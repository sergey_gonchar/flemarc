<?php echo $header; ?>
<?php $config = $this->registry->get('config');?>
<?php $lang = (int)$config->get('config_language_id');?>
<div class="container">
   <ul class="breadcrumb">
      <?php $breadlast = array_pop($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?> 
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?> 
      <li><span><?php echo $breadlast['text']; ?></span></li>
   </ul>
   <?php if ($success) { ?>
   <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
   </div>
   <?php } ?>
   <div class="row">
      <?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-10'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>">
         <?php echo $content_top; ?>    
         <h2><?php echo $heading_title; ?></h2>

                <?php if($logged){ ?>
  <div><?php echo $share_message; ?></div>
  <br />
  <div><b><?php echo $share_link; ?></b></div>
  <br />
  <?php } ?>
            
         <div class="row">
            <?php if ($products) { ?>
            <?php foreach ($products as $product) { ?>
            <div class="product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12">
               <?php if($product['special']) { ?>
               <div class="sale">-<?php echo $product['sale']; ?>%</div>
               <?php } ?>
               <div class="product-thumb">
                  <div class="action">
                     <div class="wishlist">
                        <a href="<?php echo $product['remove']; ?>" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="no-btn"><i class="fa fa-times"></i></a>
                     </div>
                  </div>
                  <?php if ($product['thumb_swap']) { ?>
                  <div class="image hover"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  <div class="image "><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  <?php } else {?>
                  <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  <?php } ?>
                  <div>
                     <div class="caption">
                        <?php if($config->get('control_stock')== 1) { ?><span class="stock" data-toggle="tooltip" title="<?php echo $product['stock']; ?>" style="background-color:<?php echo ($product['quantity'] > 0) ? '#9BD79B' : '#F07899'; ?>;"></span><?php } ?>
                        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                        <button class="no-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');">
                        <span class="cart-icon"><?php echo $button_cart; ?></span>
                        </button>
                        <?php if ($product['price']) { ?>
                        <p class="price">
                           <?php if (!$product['special']) { ?>
                           <?php echo $product['price']; ?>
                           <?php } else { ?>
                           <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                           <?php } ?>
                        </p>
                        <?php } ?>
                     </div>
                  </div>
               </div>
               <?php foreach ($product['options'] as $option) { ?>
               <?php if ($option['type'] == 'radio') { ?>
               <?php $control_option_title = $config->get('control_option_title'); if ($option['name'] ==  $control_option_title[$lang] ) { ?>
               <div class="opsize">
                  <label class="control-label"><?php echo $option['name']; ?></label>
                  <div class="input-option">
                     <?php foreach ($option['product_option_value'] as $option_value) { ?>
                     <div class="opradio <?php if ($option_value['subtract']) { if ($option_value['quantity'] <= 0) echo 'out';} ?>"
                        data-toggle="tooltip" title="<?php if ($option_value['subtract']) {
                           if ($option_value['quantity'] > 0)
                             echo ''.$text_scl.' '.$option_value['quantity'].' '.$text_pcs;
                           else
                             echo ' '.$text_out_of_stock.' ';} ?>"><?php echo $option_value['name']; ?></div>
                     <?php } ?>
                  </div>
               </div>
               <?php } ?>               
               <?php } ?>
               <?php } ?>
            </div>
            <?php } ?>
            <?php } else { ?>
            <p class="empty"><?php echo $text_empty; ?></p>
            <?php } ?>
         </div>
         <div class="buttons clearfix">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
         </div>
         <?php echo $content_bottom; ?>
      </div>
      <?php echo $column_right; ?>
   </div>
</div>
<?php echo $footer; ?>