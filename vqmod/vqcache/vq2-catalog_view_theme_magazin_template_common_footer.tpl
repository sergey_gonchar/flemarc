<?php echo $content_footer; ?>
<footer>

		<div class="col-sm-12 footer-links">	
			<ul class="line1">		
					<?php foreach ($informations as $information) { ?>
					<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
					<?php } ?>
					<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
					<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
					<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
					<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
					<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
					<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
					<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
					<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
					<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
					<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
			</ul>
		</div>
  <div class="container">
    <div class="row">
		<div class="col-lg-3 col-sm-8">
			<a href="#" title="Universal" class="footer-logo"><span>Universal</span></a>
			<p>
		<?php global $config; $lang = (int)$config->get('config_language_id');?>
		<?php if($config->get('control_info_shop')) { $control_info_shop = $config->get('control_info_shop');  echo $control_info_shop[$lang]; }?>	 
			</p>
			<div class="data-footer">
				<p  class="df"><i class="fa fa-phone"></i> <?php echo $telephone; ?></p>
				<p  class="df"><i class="fa fa-globe"></i> <?php echo $address; ?></p>
				<p  class="df"><a target="_blank" href="mailto:kenan78@gmail.com"><i class="fa fa-envelope"></i>kenan78@gmail.com</a></p>
			</div>
		</div>
		<div class="col-sm-3">
			<h2 class="footer-block-title">Newsletter</h2>
			<form class="solid">
				<div id="close-subscription">
				  <input type="hidden" name="language_id" id="language_id" value="" />
				  <input type="hidden" name="store_id" id="store_id" value="" />
				  <label for="newsletter-296">Subscribe to the Universal mailing list to receive updates on new arrivals, offers and other discount information.</label>
				  <div class="sub-bl">
					 <input id="semail" class="input-sub" name="semail" value="" placeholder="E-Mail" type="text">
				  </div>
				  <div class="sub-bl">
					 <input type="button" id="bsubscription1" value="Подписаться" class="btn btn-primary">
				  </div>
				</div>
			</form>
		</div>
	  
		<div class="col-lg-2 col-sm-4 footer-block">
			<h2 class="footer-block-title">Quick links</h2>
			<ul class="list-unstyled line">
				  <?php foreach ($informations as $information) { ?>
				  <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
				  <?php } ?>
				  <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
			</ul>
		</div>
		<div class="col-lg-2 col-sm-4 footer-block">
			<h2 class="footer-block-title">Accessories</h2>
			<ul class="list-unstyled line">
				<?php foreach ($informations as $information) { ?>
				<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
				<?php } ?>
				<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
			</ul>
		</div>
		<div class="col-lg-2 col-sm-4 footer-block">
			<h2 class="footer-block-title">Openning hours</h2>
			<ul class="list-unstyled line">
				<?php foreach ($informations as $information) { ?>
				<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
				<?php } ?>
				<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
				<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
			</ul>
		</div>
	</div>
  </div>
  <div class="copyright"><?php echo $powered; ?></div>
  <?php if($config->get('control_callmebtn')== 1 && $config->get('control_callme')== 1) { ?>
  <div class="callmebtn panel-url hidden-xs" data-spanel="#callme">
    <div class="toolhover">
        <div class="callbtn">Перезвоним</div>
		<div class="callicon"></div>
        <div class="callfon"></div>
    <div class="toolcall">Перезвоним бесплатно</div>
	</div>   
</div>
 <?php } ?> 

              <!-- Begin Cookie  plugin -->
			<script type="text/javascript">!function(){if(!window.hasCookieConsent){window.hasCookieConsent=!0;var e="cookieconsent_options",t="update_cookieconsent_options",n="cookieconsent_dismissed",i="catalog/view/theme/default/stylesheet/cookie/";if(!(document.cookie.indexOf(n)>-1)){"function"!=typeof String.prototype.trim&&(String.prototype.trim=function(){return this.replace(/^\s+|\s+$/g,"")});var o,s={isArray:function(e){var t=Object.prototype.toString.call(e);return"[object Array]"==t},isObject:function(e){return"[object Object]"==Object.prototype.toString.call(e)},each:function(e,t,n,i){if(s.isObject(e)&&!i)for(var o in e)e.hasOwnProperty(o)&&t.call(n,e[o],o,e);else for(var r=0,c=e.length;c>r;r++)t.call(n,e[r],r,e)},merge:function(e,t){e&&s.each(t,function(t,n){s.isObject(t)&&s.isObject(e[n])?s.merge(e[n],t):e[n]=t})},bind:function(e,t){return function(){return e.apply(t,arguments)}},queryObject:function(e,t){var n,i=0,o=e;for(t=t.split(".");(n=t[i++])&&o.hasOwnProperty(n)&&(o=o[n]);)if(i===t.length)return o;return null},setCookie:function(e,t,n){var i=new Date;n=n||365,i.setDate(i.getDate()+n),document.cookie=e+"="+t+"; expires="+i.toUTCString()+"; path=/"},addEventListener:function(e,t,n){e.addEventListener?e.addEventListener(t,n):e.attachEvent("on"+t,n)}},r=function(){var e="data-cc-event",t="data-cc-if",n=function(e,t,i){return s.isArray(t)?s.each(t,function(t){n(e,t,i)}):void(e.addEventListener?e.addEventListener(t,i):e.attachEvent("on"+t,i))},i=function(e,t){return e.replace(/\{\{(.*?)\}\}/g,function(e,n){for(var i,o=n.split("||");token=o.shift();){if(token=token.trim(),'"'===token[0])return token.slice(1,token.length-1);if(i=s.queryObject(t,token))return i}return""})},o=function(e){var t=document.createElement("div");return t.innerHTML=e,t.children[0]},r=function(e,t,n){var i=e.parentNode.querySelectorAll("["+t+"]");s.each(i,function(e){var i=e.getAttribute(t);n(e,i)},window,!0)},c=function(t,i){r(t,e,function(e,t){var o=t.split(":"),r=s.queryObject(i,o[1]);n(e,o[0],s.bind(r,i))})},a=function(e,n){r(e,t,function(e,t){var i=s.queryObject(n,t);i||e.parentNode.removeChild(e)})};return{build:function(e,t){s.isArray(e)&&(e=e.join("")),e=i(e,t);var n=o(e);return c(n,t),a(n,t),n}}}(),c={options:{message:"This website uses cookies to ensure you get the best experience on our website. ",dismiss:"Got it!",learnMore:"More info",link:null,container:null,theme:"light-floating",markup:['<div class="cc_banner-wrapper {{containerClasses}}">','<div class="cc_banner cc_container cc_container--open">','<a href="#null" data-cc-event="click:dismiss" class="cc_btn cc_btn_accept_all">{{options.dismiss}}</a>','<p class="cc_message">{{options.message}} <a data-cc-if="options.link" class="cc_more_info" href="{{options.link || "#null"}}">{{options.learnMore}}</a></p>',"</div>","</div>"]},init:function(){var t=window[e];t&&this.setOptions(t),this.setContainer(),this.options.theme?this.loadTheme(this.render):this.render()},setOptionsOnTheFly:function(e){this.setOptions(e),this.render()},setOptions:function(e){s.merge(this.options,e)},setContainer:function(){this.container=this.options.container?document.querySelector(this.options.container):document.body,this.containerClasses="",navigator.appVersion.indexOf("MSIE 8")>-1&&(this.containerClasses+=" cc_ie8")},loadTheme:function(e){var t=this.options.theme;-1===t.indexOf(".css")&&(t=i+t+".css");var n=document.createElement("link");n.rel="stylesheet",n.type="text/css",n.href=t;var o=!1;n.onload=s.bind(function(){!o&&e&&(e.call(this),o=!0)},this),document.getElementsByTagName("head")[0].appendChild(n)},render:function(){this.element&&this.element.parentNode&&(this.element.parentNode.removeChild(this.element),delete this.element),this.element=r.build(this.options.markup,this),this.container.firstChild?this.container.insertBefore(this.element,this.container.firstChild):this.container.appendChild(this.element)},dismiss:function(e){e.preventDefault&&e.preventDefault(),e.returnValue=!1,this.setDismissedCookie(),this.container.removeChild(this.element)},setDismissedCookie:function(){s.setCookie(n,"yes")}},a=!1;(o=function(){a||"complete"!=document.readyState||(c.init(),a=!0,window[t]=s.bind(c.setOptionsOnTheFly,c))})(),s.addEventListener(document,"readystatechange",o)}}}();</script>
	
<script type="text/javascript">
    window.cookieconsent_options = {"message":"<?php echo $entry_cookie_msg; ?>","dismiss":"<?php echo $entry_cookie_accept; ?>","learnMore":"<?php echo $entry_cookie_more; ?>","link":"<?php echo $entry_cookie_link; ?>","theme":"<?php echo $data['config_cookie']; ?>"};
</script>

<!-- End Cookie plugin -->
                </div>
             </div>
            
</footer>
<?php if($config->get('control_back_to_top')== 1) {?> 
<a onclick="cartallitem()" class="btn btn-primary btn-lg btn-block multicartbtn" href="Javascript:void(0)">Multiple Add to Cart Prouduct</a>
  <input type='hidden' id='allids' name='allids' value=''/>
<style>
.mcart {
position: absolute;
z-index: 99;
right: 40% !important;
}
.multicartbtn {
    position: fixed;
    top: 0;
    z-index: 999;
	display:none;

}

</style>
<script>
function getselecteditem(id){

	var cartid = '#cartitem'+id;
	
	if($(cartid).is(":checked")){
		var textval = $('#allids').val();
		$('#allids').val(textval+id+',');
	}else{
	var allids = $('#allids').val();
	
	var mystring = allids.replace(id+',','');
	$('#allids').val(mystring);
	}
  	$('.multicartbtn').show();	
}
function cartallitem(){
var allids = $('#allids').val();
var res = allids.split(","); 
for(var i=0;i<res.length-1;i++) {

	var  item = res[i];
	cart.add(item);
}

}
</script>
<div class="scrollup hidden-xs">
   <div class="sup"></div>
</div>
<?php } ?>
<script src="catalog/view/theme/magazin/js/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet" type='text/css'>
<script src="catalog/view/theme/magazin/js/jquery.mmenu.min.all.js" type="text/javascript"></script>
<script src="catalog/view/theme/magazin/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="catalog/view/theme/magazin/js/jquery.elevatezoom.js" type="text/javascript"></script>
<link href="catalog/view/theme/magazin/stylesheet/jquery.mmenu.all.css" rel="stylesheet" type='text/css'>
<div id="id32" style="display:none;">
<div class="form-group">
<label for="input-remember">
<input type="checkbox" name="remember_me" id="input-remember" value="remember_checked" /> remember me
</label>
</div>
</div>
<script>$('#quicklogin #quick-login .form-group:nth-child(3)').after($('#id32').html());</script>
</body></html>