<table class="table table-bordered">
  <thead>
    <tr>

			
			<td class="text-center"><?php echo $column_delete_history; ?></td>
			
			
      <td class="text-left"><?php echo $column_date_added; ?></td>
      <td class="text-left"><?php echo $column_comment; ?></td>
      <td class="text-left"><?php echo $column_status; ?></td>
      <td class="text-left"><?php echo $column_notify; ?></td>

				<td class="text-right"><?php echo $download; ?></td>
            
    </tr>
  </thead>
  <tbody>
    <?php if ($histories) { ?>
    <?php foreach ($histories as $history) { ?>
    <tr>

			
			<td class="text-center">
				<?php if (count($histories) > 1) { ?>
					<button id="button-history-<?php echo $history['order_history_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip" title="<?php echo $text_delete_history; ?>" class="btn btn-danger delete-history"><i class="fa fa-trash-o"></i></button>
				<?php } else { ?>
					&mdash;
				<?php } ?>
			</td>
			
			
      <td class="text-left"><?php echo $history['date_added']; ?></td>
      <td class="text-left"><?php echo $history['comment']; ?></td>
      <td class="text-left"><?php echo $history['status']; ?></td>
      <td class="text-left"><?php echo $history['notify']; ?></td>

				<?php if($history['filename']) { ?>
					<td class="text-right"><a  target="_blank" href="<?php echo $history['filename']; ?>" class="button"><?php echo $download_attachment; ?></a></td>
				<?php } else { ?>
					<td class="text-right"><?php echo $no_file; ?></td>
				<?php } ?>
            
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      
                <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
            
    </tr>
    <?php } ?>
  </tbody>
</table>
<div class="row">
  <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
