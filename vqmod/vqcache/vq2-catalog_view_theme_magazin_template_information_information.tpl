<?php
function email_regex()
{
    $atom = "[-!#$%&'*+/=?^_`{|}~0-9A-Za-z]+";
    $email_half = $atom . '(?:\\.' . $atom . ')*';
    $email = $email_half . '@' . $email_half;
    $email_regex = '<(' . $email . ')>';
    return $email_regex;
}
function replaceEntities($matches)
{
    $address = html_entity_decode($matches[1]);
    $replaced = '';
    for ($i = 0; $i < strlen($address); $i++) {
        $char = $address[$i];
        $r = rand(0, 100);
        # roughly 10% raw, 45% hex, 45% dec
        if ($r > 90) {
            $replaced .= $char;
        }
        else if ($r < 45) {
            $replaced .= '&#x' . dechex(ord($char)) . ';';
        }
        else
        {
            $replaced .= '&#' . ord($char) . ';';
        }
    }
    return $replaced;
}

//Layouts --> If your template have extra layout you can add it here
$header = preg_replace_callback(email_regex(), "replaceEntities", $header);
$header = preg_replace_callback('/(mailto:)/', "replaceEntities", $header);

$column_left = preg_replace_callback(email_regex(), "replaceEntities", $column_left);
$column_left = preg_replace_callback('/(mailto:)/', "replaceEntities", $column_left);

$column_right = preg_replace_callback(email_regex(), "replaceEntities", $column_right);
$column_right = preg_replace_callback('/(mailto:)/', "replaceEntities", $column_right);

$content_top = preg_replace_callback(email_regex(), "replaceEntities", $content_top);
$content_top = preg_replace_callback('/(mailto:)/', "replaceEntities", $content_top);

$content_bottom = preg_replace_callback(email_regex(), "replaceEntities", $content_bottom);
$content_bottom = preg_replace_callback('/(mailto:)/', "replaceEntities", $content_bottom);

$description = preg_replace_callback(email_regex(), "replaceEntities", $description);
$description = preg_replace_callback('/(mailto:)/', "replaceEntities", $description);

$footer = preg_replace_callback(email_regex(), "replaceEntities", $footer);
$footer = preg_replace_callback('/(mailto:)/', "replaceEntities", $footer);

// end layouts
?>
            <?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
	   <?php $breadlast = array_pop($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?> 
	   <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	   <?php } ?> 
	   <li><span><?php echo $breadlast['text']; ?></span></li>
	</ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <?php echo $description; ?><?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>