<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';

			//Cookie
			$_['entry_cookie_link']            = 'https://en.wikipedia.org/wiki/HTTP_cookie';
			 $_['entry_cookie_more']           = 'More info';
             $_['entry_cookie_accept']         = 'Got it!';
			 $_['entry_cookie_msg']            = 'This website uses cookies to ensure you get the best experience on our website - ';
            
$_['text_page']          = 'page';
