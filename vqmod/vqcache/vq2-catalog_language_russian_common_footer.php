<?php
$_['text_information'] = 'Информация';
$_['text_service'] = 'Служба поддержки';
$_['text_extra'] = 'Дополнительно';
$_['text_contact'] = 'Связаться с нами';
$_['text_return'] = 'Возврат товара';
$_['text_sitemap'] = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher'] = 'Подарочные сертификаты';
$_['text_affiliate'] = 'Партнёры';
$_['text_special'] = 'Скидки';
$_['text_account'] = 'Личный Кабинет';
$_['text_order'] = 'История заказов';
$_['text_wishlist'] = 'Мои Закладки';
$_['text_newsletter'] = 'Рассылка новостей';

			//Cookie
			$_['entry_cookie_link']            = 'http://www.arredoclassic.com/ru/%D0%B8%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D0%B8%D1%8F-%D0%BE-%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B4%D0%B5%D0%BD%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8#cookie';
			 $_['entry_cookie_more']           = 'подробнее';
             $_['entry_cookie_accept']         = 'Ok!';
			 $_['entry_cookie_msg']            = 'Этот сайт использует cookie для того, чтобы тебе дать лучший сервис. Продолжая навигацию по сайту, ты принимаешь использование файлов cookie, с нашей стороны, нажимая: ';			 
            
$_['text_powered'] = 'Работает на <a target="_blank" href="http://myopencart.com/">ocStore</a><br /> %s &copy; %s';
$_['entry_cookie_more'] = 'Подробнее';
$_['entry_cookie_accept'] = 'Согласен!';
$_['entry_cookie_msg'] = 'Этот сайт собирает статистику.';
?>