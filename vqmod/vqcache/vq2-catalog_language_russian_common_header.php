<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Мои Закладки (%s)';
$_['text_shopping_cart'] = 'Корзина покупок';
$_['text_category']      = 'Категории';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'История платежей';
$_['text_download']      = 'Файлы для скачивания';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Показать все';

			//Cookie
			$_['entry_cookie_link']            = 'http://www.arredoclassic.com/ru/%D0%B8%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D0%B8%D1%8F-%D0%BE-%D0%BA%D0%BE%D0%BD%D1%84%D0%B8%D0%B4%D0%B5%D0%BD%D1%86%D0%B8%D0%B0%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8#cookie';
			 $_['entry_cookie_more']           = 'подробнее';
             $_['entry_cookie_accept']         = 'Ok!';
			 $_['entry_cookie_msg']            = 'Этот сайт использует cookie для того, чтобы тебе дать лучший сервис. Продолжая навигацию по сайту, ты принимаешь использование файлов cookie, с нашей стороны, нажимая: ';			 
            
$_['text_page']          = 'страница';
