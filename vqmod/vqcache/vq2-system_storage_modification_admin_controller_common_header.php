<?php
class ControllerCommonHeader extends Controller {
	public function index() {

			if (!isset($this->session->data['backup_scheduled_date'])) {
				$backup_scheduled_date = $this->config->get('backup_scheduled_date');
				if ($backup_scheduled_date && $this->config->get('backup_scheduled_status') == 1 && $this->config->get('backup_scheduled_cron') == 0) {
					$this->session->data['backup_scheduled_date'] = $this->config->get('backup_scheduled_date');
				} else {
					$this->session->data['backup_scheduled_date'] = false;
				}
			}
			
			if ($this->session->data['backup_scheduled_date'] !== false) {
				if (date('Y-m-d') >= $this->session->data['backup_scheduled_date']) {
					$frequency = $this->config->get('backup_scheduled_frequency');
					if ($frequency == 'Daily') {
						$new_date = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' + 1 day'));
					} elseif ($frequency == 'Weekly') {
						$new_date = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' + 1 week'));
					} else {
						$new_date = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' + 1 month'));
					}
					$this->load->model('tool/backup_pro');
					$this->model_tool_backup_pro->reset_next_backup($new_date);
					$this->session->data['backup_scheduled_date'] = $new_date;
					
					$url = HTTPS_CATALOG . 'index.php?route=tool/backup_pro';

					//step1
					$cSession = curl_init();

					//step2
					curl_setopt($cSession,CURLOPT_URL,$url);
					curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
					curl_setopt($cSession,CURLOPT_HEADER, false);

					//step3
					$result=curl_exec($cSession);

					//step4
					curl_close($cSession);
				}
			}
			
		$data['title'] = $this->document->getTitle();

		if ($this->request->server['HTTPS']) {
			$data['base'] = HTTPS_SERVER;
		} else {
			$data['base'] = HTTP_SERVER;
		}

		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['code'] = $this->language->get('code');
		$data['lang'] = $this->language->get('lang');
		$data['direction'] = $this->language->get('direction');

		$this->load->language('common/header');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_order'] = $this->language->get('text_order');
		$data['text_processing_status'] = $this->language->get('text_processing_status');
		$data['text_complete_status'] = $this->language->get('text_complete_status');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_online'] = $this->language->get('text_online');
		$data['text_approval'] = $this->language->get('text_approval');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_stock'] = $this->language->get('text_stock');
		$data['text_review'] = $this->language->get('text_review');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_front'] = $this->language->get('text_front');
		$data['text_help'] = $this->language->get('text_help');
		$data['text_homepage'] = $this->language->get('text_homepage');
		$data['text_documentation'] = $this->language->get('text_documentation');
		$data['text_support'] = $this->language->get('text_support');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->user->getUserName());
		$data['text_logout'] = $this->language->get('text_logout');

		if (!isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
			$data['logged'] = '';

			$data['home'] = $this->url->link('common/dashboard', '', 'SSL');
		} else {
			$data['logged'] = true;

		/****** Message System Ends *****/
		$data['link_message'] = $this->url->link('message/message','token='.$this->session->data['token'],'SSL');
		$filter_data = array(
			'filter_status' => '0',
			'filter_sender'        => 'customer'
		);
		
		$this->load->model('message/message');
		
		$data['alert_messages'] = $this->model_message_message->getTotalUnreadMessages($filter_data);
		/****** Message System Ends *****/
		

			$data['home'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL');
			$data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL');

			// Orders
			$this->load->model('sale/order');

			// Processing Orders
			$data['processing_status_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_processing_status'))));
			$data['processing_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status=' . implode(',', $this->config->get('config_processing_status')), 'SSL');

			// Complete Orders
			$data['complete_status_total'] = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_complete_status'))));
			$data['complete_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_order_status=' . implode(',', $this->config->get('config_complete_status')), 'SSL');

			// Returns
			$this->load->model('sale/return');

			$return_total = $this->model_sale_return->getTotalReturns(array('filter_return_status_id' => $this->config->get('config_return_status_id')));

			$data['return_total'] = $return_total;

			$data['return'] = $this->url->link('sale/return', 'token=' . $this->session->data['token'], 'SSL');

			// Customers
			$this->load->model('report/customer');

			$data['online_total'] = $this->model_report_customer->getTotalCustomersOnline();

			$data['online'] = $this->url->link('report/customer_online', 'token=' . $this->session->data['token'], 'SSL');

			$this->load->model('customer/customer');

			$customer_total = $this->model_customer_customer->getTotalCustomers(array('filter_approved' => false));

			$data['customer_total'] = $customer_total;
			$data['customer_approval'] = $this->url->link('customer/customer', 'token=' . $this->session->data['token'] . '&filter_approved=0', 'SSL');

			// Products
			$this->load->model('catalog/product');

			$product_total = $this->model_catalog_product->getTotalProducts(array('filter_quantity' => 0));

			$data['product_total'] = $product_total;

			$data['product'] = $this->url->link('catalog/product', 'token=' . $this->session->data['token'] . '&filter_quantity=0', 'SSL');

			// Reviews
			$this->load->model('catalog/review');

			$review_total = $this->model_catalog_review->getTotalReviews(array('filter_status' => false));

			$data['review_total'] = $review_total;

			$data['review'] = $this->url->link('catalog/review', 'token=' . $this->session->data['token'] . '&filter_status=0', 'SSL');


			// Callme
			if ($this->config->get('callme_status')== 1) {
			$this->load->model('extension/callme');

			$callme_total = $this->model_extension_callme->getTotalCallmes();

			$data['callme_total'] = $callme_total;
			$data['callme'] = $this->url->link('extension/callme', 'token=' . $this->session->data['token'], 'SSL');
            }
			// Subscription
			if ($this->config->get('subscription_status')== 1) {
			$this->load->model('extension/subscription');

			$subscription_total = $this->model_extension_subscription->getTotalSubscriptions();

			$data['subscription_total'] = $subscription_total;
			$data['subscription'] = $this->url->link('extension/subscription', 'token=' . $this->session->data['token'], 'SSL');
			}
			
			// Affliate
			$this->load->model('marketing/affiliate');

			$affiliate_total = $this->model_marketing_affiliate->getTotalAffiliates(array('filter_approved' => false));

			$data['affiliate_total'] = $affiliate_total;
			$data['affiliate_approval'] = $this->url->link('marketing/affiliate', 'token=' . $this->session->data['token'] . '&filter_approved=1', 'SSL');

			$data['alerts'] = $customer_total + $product_total + $review_total + $return_total + $affiliate_total;

			// Online Stores
			$data['stores'] = array();

			$data['stores'][] = array(
				'name' => $this->config->get('config_name'),
				'href' => HTTP_CATALOG
			);

			$this->load->model('setting/store');

			$results = $this->model_setting_store->getStores();

			foreach ($results as $result) {
				$data['stores'][] = array(
					'name' => $result['name'],
					'href' => $result['url']
				);
			}
		}

		return $this->load->view('common/header.tpl', $data);
	}
}
