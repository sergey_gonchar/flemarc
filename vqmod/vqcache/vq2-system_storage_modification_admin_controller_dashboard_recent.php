<?php
class ControllerDashboardRecent extends Controller {
	public function index() {
		$this->load->language('dashboard/recent');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_order_id'] = $this->language->get('column_order_id');
		$data['column_customer'] = $this->language->get('column_customer');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_date_added'] = $this->language->get('column_date_added');

				
    	$data['column_model'] = $this->language->get('column_model');	
    	$data['column_quantity'] = $this->language->get('column_quantity');
    	$data['column_product'] = "Product";	
    	
	
				
		$data['column_total'] = $this->language->get('column_total');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_view'] = $this->language->get('button_view');

		$data['token'] = $this->session->data['token'];

		// Last 5 Orders
		$data['orders'] = array();

		$filter_data = array(
			'sort'  => 'o.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 5
		);

		$results = $this->model_sale_order->getOrders($filter_data);

		foreach ($results as $result) {
			$data['orders'][] = array(
				'order_id'   => $result['order_id'],
				'customer'   => $result['customer'],
 
	'products'=> $this->model_sale_order->getQVPRODUCTS($result['order_id']),  
    'comment'=> $this->model_sale_order->getQVCOMMENT($result['order_id']),
    'shipping'=> $this->model_sale_order->getQVSHIPPING($result['order_id']),
    'payment'=> $this->model_sale_order->getQVPAYMENT($result['order_id']),
				
				'status'     => $result['status'],
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
				'view'       => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'], 'SSL'),
			);
		}


		$data['customers'] =  array();

		$filter_data = array(
			'sort'  => 'c.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 10
		);

		if ($this->user->hasPermission('access', 'customer/customer')) {
			$customers = $this->model_customer_customer->getCustomers($filter_data);
		} else {
			$customers = array();
			$this->load->language('error/permission');
			$data['text_no_results'] = $this->language->get('text_permission');
		}

		foreach ($customers as $customer) {
			$query_order_total = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE customer_id = '" . (int)$customer['customer_id'] . "' AND order_status_id > '0'");
			$query_order_missing = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "order` WHERE customer_id = '" . (int)$customer['customer_id'] . "' AND order_status_id = '0'");

			$data['customers'][] = array(
				'name'             => $customer['name'],
				'order_total'      => $query_order_total->row['total'],
				'order_missing'    => $query_order_missing->row['total'],
				'email'            => $customer['email'],
				'newsletter'       => $customer['newsletter'],
				'group'            => $customer['customer_group'],
				'status'           => $customer['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'approved'         => $customer['approved'] ? $this->language->get('text_yes') : $this->language->get('text_no'),
				'date_added_long'  => date($this->language->get('date_format_long'), strtotime($customer['date_added'])),
				'date_added_short' => date($this->language->get('date_format_short'), strtotime($customer['date_added'])),
				'date_added_time'  => date($this->language->get('time_format'), strtotime($customer['date_added'])),
				'href_edit'        => $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $customer['customer_id'], 'SSL'),
				'href_order'       => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&filter_customer=' . $customer['name'], 'SSL'),
				'href_contact'     => $this->url->link('marketing/contact', 'token=' . $this->session->data['token'], 'SSL')
			);
		}
		
		return $this->load->view('dashboard/recent.tpl', $data);
	}
}