<?php
class ControllerCommonHeader extends Controller {
	public function index() {

			if (!isset($this->session->data['backup_scheduled_date'])) {
				$backup_scheduled_date = $this->config->get('backup_scheduled_date');
				if ($backup_scheduled_date && $this->config->get('backup_scheduled_status') == 1 && $this->config->get('backup_scheduled_cron') == 0) {
					$this->session->data['backup_scheduled_date'] = $this->config->get('backup_scheduled_date');
				} else {
					$this->session->data['backup_scheduled_date'] = false;
				}
			}
			
			if ($this->session->data['backup_scheduled_date'] !== false) {
				if (date('Y-m-d') >= $this->session->data['backup_scheduled_date']) {
					$frequency = $this->config->get('backup_scheduled_frequency');
					if ($frequency == 'Daily') {
						$new_date = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' + 1 day'));
					} elseif ($frequency == 'Weekly') {
						$new_date = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' + 1 week'));
					} else {
						$new_date = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' + 1 month'));
					}
					$this->load->model('tool/backup_pro');
					$this->model_tool_backup_pro->reset_next_backup($new_date);
					$this->session->data['backup_scheduled_date'] = $new_date;
					
					$url = HTTPS_SERVER . 'index.php?route=tool/backup_pro';

					//step1
					$cSession = curl_init();

					//step2
					curl_setopt($cSession,CURLOPT_URL,$url);
					curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
					curl_setopt($cSession,CURLOPT_HEADER, false);

					//step3
					$result=curl_exec($cSession);

					//step4
					curl_close($cSession);	
				}
			}
			
if ($this->customer->isLogged()) { $data['welcome_message'] = sprintf("Logged in as %s", $this->customer->getFirstName(), $this->customer->getLastName());}
	/*	if( (!isset($this->request->get['route']) || $this->request->get['route'] != 'account/dealer' ) && ($this->request->get['route'] != 'account/forgotten' ) && ($this->request->get['route'] != 'account/register' ) && (!$this->customer->isLogged()) ){
         $this->response->redirect($this->url->link('account/dealer', '', 'SSL'));
      }*/
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('analytics/' . $analytic['code']);
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}


				$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/contact_social.css');
				
		
		$data['title'] = $this->document->getTitle();


		/****** Message System Starts *****/
		$this->language->load('account/message');
		$data['text_message_system'] = $this->language->get('text_message_system');
		$data['message_system'] = $this->url->link('account/message');
		$filter_data =array(
			'filter_sender'   => 'user',
			'filter_status'   => '0',
			'customer_id'     => $this->customer->getId(),
		);
		$this->load->model('account/message');
		$data['alert_messages'] = $this->model_account_message->getTotalUnreadMessages($filter_data);
		
		/****** Message System Ends *****/
		
		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE."")) {
			$data['logo'] = $server . 'image/' . "";
		} else {
			$data['logo'] = '';
		}

if (is_file(DIR_IMAGE . $this->config->get('config_background_image'))) {
            $data['background_image'] = $server . 'image/' . $this->config->get('config_background_image');
            } else {
                $data['background_image'] = '';
            }
            if ($this->config->get('config_background_image_position')) {
                $data['config_background_image_position'] = $this->config->get('config_background_image_position');
            } else {
                $data['config_background_image_position'] = '';
            }
            if(isset($this->request->get['path']) AND !isset($this->request->get['product_id'])) {
                $parts = explode('_', (string)$this->request->get['path']);
                $category_id = (int)array_pop($parts);
                $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND c2s.store_id =0 AND c.status = '1'");
                if(isset($query->row['config_background_image']) AND !empty($query->row['config_background_image']) ){
                    $data['background_image'] = $server . 'image/' . $query->row['config_background_image'];
                }
                if( isset($query->row['config_background_image_position']) AND !empty($query->row['config_background_image_position']) ){
                    $data['config_background_image_position'] = $query->row['config_background_image_position'];
                }       
            }
            if(isset($this->request->get['manufacturer_id'])) {
                $manufacturer_id = $this->request->get['manufacturer_id'];
                $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "manufacturer m LEFT JOIN " . DB_PREFIX . "manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) WHERE m.manufacturer_id = '" . (int)$manufacturer_id . "' AND m2s.store_id =0 ");
                if(isset($query->row['config_background_image']) AND !empty($query->row['config_background_image']) ){
                    $data['background_image'] = $server . 'image/' . $query->row['config_background_image'];
                }
                if( isset($query->row['config_background_image_position']) AND !empty($query->row['config_background_image_position']) ){
                    $data['config_background_image_position'] = $query->row['config_background_image_position'];
                }
            }
            if(isset($this->request->get['product_id'])) {
                $product_id = $this->request->get['product_id'];
                $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p LEFT JOIN " .DB_PREFIX. "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.product_id = '" .(int) $product_id. "' AND p2s.store_id = 0 AND p.status='1'");
                if(isset($query->row['config_background_image']) AND !empty($query->row['config_background_image']) ){
                    $data['background_image'] = $server . 'image/' . $query->row['config_background_image'];
                }
                if( isset($query->row['config_background_image_position']) AND !empty($query->row['config_background_image_position']) ){
                    $data['config_background_image_position'] = $query->row['config_background_image_position'];
                }
            }
           if(isset($this->request->get['information_id'])) {
                $information_id = $this->request->get['information_id'];
                $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "information_description m LEFT JOIN " . DB_PREFIX . "information_to_store m2s ON (m.information_id = m2s.information_id) WHERE m.information_id = '" . (int)$information_id . "' AND m2s.store_id =0 ");
                if(isset($query->row['config_background_image']) AND !empty($query->row['config_background_image']) ){
                    $data['background_image'] = $server . 'image/' . $query->row['config_background_image'];
                }
                if( isset($query->row['config_background_image_position']) AND !empty($query->row['config_background_image_position']) ){
                    $data['config_background_image_position'] = $query->row['config_background_image_position'];
                }
            }
            

			
			$logos = $this->config->get('config_logo');

			if (is_array($logos)) {
				if (is_file(DIR_IMAGE.$logos[$this->session->data['language']])) {
					$data['logo'] = $server."image/".$logos[$this->session->data['language']];
				} else {
					$data['logo'] = "";
				}
			} else {
				$data['logo'] = $server."image/".$logos;
			}
		
			
		$this->load->language('common/header');
$this->load->language('theme');
		 $data['quicksignup'] = $this->load->controller('common/quicksignup');
         $data['callme'] = $this->load->controller('common/callme');
		 $data['content_slide'] = $this->load->controller('common/content_slide');
		 $data['text_search_theme'] = $this->language->get('text_search_theme');
         $data['regim'] = $this->language->get('text_regim');
		 $data['heading_title_callme'] = $this->language->get('heading_title_callme');		 
		 $data['text_brand'] = $this->language->get('text_brand');
		 $data['name_customer'] = $this->customer->getFirstName();
		 $data['manufacturer'] = $this->url->link('product/manufacturer');
		 $data['text_special'] = $this->language->get('text_special');
		 $data['special'] = $this->url->link('product/special');
		 $this->load->model('catalog/manufacturer');
		$this->load->model('tool/image');
		$results = $this->model_catalog_manufacturer->getManufacturers();
		foreach ($results as $result) {	
			if ($result['image']) {
						$image = $result['image'];
					} else {
						$image = 'no_image.jpg';
					}			
			$data['manufacturers'][] = array(
				'name' => $result['name'],
				'image' => $this->model_tool_image->resize($image, 150, 50),
				'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
			);
		}
		$data['og_url'] = (isset($this->request->server['HTTPS']) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
		$data['og_image'] = $this->document->getOgImage();

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', 'SSL'), $this->customer->getFirstName(), $this->url->link('account/logout', '', 'SSL'));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_page'] = $this->language->get('text_page');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['register'] = $this->url->link('account/register', '', 'SSL');
		$data['login'] = $this->url->link('account/login', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['transaction'] = $this->url->link('account/transaction', '', 'SSL');
		$data['download'] = $this->url->link('account/download', '', 'SSL');
		$data['logout'] = $this->url->link('account/logout', '', 'SSL');
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['email'] = $this->config->get('config_email');

		$status = true;

		if (isset($this->request->server['HTTP_USER_AGENT'])) {
			$robots = explode("\n", str_replace(array("\r\n", "\r"), "\n", trim($this->config->get('config_robots'))));

			foreach ($robots as $robot) {
				if ($robot && strpos($this->request->server['HTTP_USER_AGENT'], trim($robot)) !== false) {
					$status = false;

					break;
				}
			}
		}

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

$this->load->model('tool/image');
				$subchildren_data = array();
				$subchildren = $this->model_catalog_category->getCategories($child['category_id']);
				foreach ($subchildren as $subchild) {
				$filter_data = array(
				'filter_category_id'  => $subchild['category_id'],
				'filter_sub_category' => true
				);
				$subchildren_data[] = array(
				'name'  => $subchild['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
				'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $subchild['category_id']),
				'image' => $this->model_tool_image->resize($subchild['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'))
				);
			}
			$filter_data = array(
				'filter_category_id'  => $child['category_id'],
				'filter_sub_category' => true
			);
			
					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
'image' => $this->model_tool_image->resize($child['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height')),
		'subchildren' => $subchildren_data,
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/header.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/header.tpl', $data);
		} else {
			return $this->load->view('default/template/common/header.tpl', $data);
		}
	}
}
