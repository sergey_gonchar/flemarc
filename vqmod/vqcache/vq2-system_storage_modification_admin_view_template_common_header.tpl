<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $code; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
<script type="text/javascript" src="view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap.min.js"></script>
<link href="view/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/font-awesome/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<script src="view/javascript/summernote/lang/summernote-<?php echo $lang; ?>.js"></script>
<script src="view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="view/javascript/jquery/datetimepicker/locale/<?php echo $code; ?>.js" type="text/javascript"></script>
<script src="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<link href="view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
<link type="text/css" href="view/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
<?php foreach ($styles as $style) { ?>
<link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>

        <link type="text/css" href="view/stylesheet/custom/expand-well-component.css" rel="stylesheet" media="screen" />
      
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<script src="view/javascript/common.js" type="text/javascript"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>

  			<script type="text/javascript" src="view/javascript/custom/expand-well-component.js"></script>
  	  

<!-- Início gerador automatico de tags de produto com base no nome Ariel (veteranodf) -->
<script type="text/javascript">
$(document).ready(function(){
$('input[id^="input-name"]').on('blur', function() {
language_id = $(this).attr('name').replace(/[^0-9]+/g, '');
tags        = "";
$($('input[id="input-name'+language_id+'"]').val().split(' ')).each(function(){
if(this.length >= "5"){
tags       += this+', ';
}
});
$('#input-tag'+language_id).val(tags.substring(0,(tags.length - 2)));
});
});
</script>
<!-- Fim gerador automatico de tags de produto com base no nome Ariel (veteranodf) -->
</script>
		

				
					<script type="text/javascript" src="view/javascript/stringToSlug/jquery.stringToSlug.min.js"></script>
					<script type="text/javascript">		
					$(document).ready( function() {
						$("#title-slug").stringToSlug({
							setEvents: 'keyup keydown blur',
							getPut: '#slug-result',
							AND: 'e'
						});
						$("#title-slug").stringToSlug({
							setEvents: 'keyup keydown blur',
							space: ' ',
							callback: function(str) {
								$('.cMeta').val(str.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); })+' - FLEMARC');
							}
						});
					});
					</script>
				
			
</head>
<body>
<div id="container">
<header id="header" class="navbar navbar-static-top">
  <div class="navbar-header">
    <?php if ($logged) { ?>
    <a type="button" id="button-menu" class="pull-left"><i class="fa fa-indent fa-lg"></i></a>
    <?php } ?>
    <a href="<?php echo $home; ?>" class="navbar-brand"><img src="view/image/logo.png" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" /></a></div>
  <?php if ($logged) { ?>
  <ul class="nav pull-right">

		<!-- /****** Message System Starts *****/ -->
    <li>
			<a href="<?php echo $link_message ?>" >
				<?php if($alert_messages) { ?>
				<span class="label label-danger pull-left" id="alert_messages"><?php echo $alert_messages; ?></span>
				<?php } ?>
				<i class="fa fa-envelope fa-lg"></i>
			</a>
		</li>
		<!-- /****** Message System Ends *****/ -->
		

			<?php if (isset($callme_total)) { ?><li><a href="<?php echo $callme; ?>"><span class="label label-danger pull-left"><?php echo $callme_total; ?></span> <i class="fa fa-phone fa-lg"></i></a><?php } ?>
			<?php if (isset($subscription_total)) { ?><li><a href="<?php echo $subscription; ?>"><span class="label label-danger pull-left"><?php echo $subscription_total; ?></span> <i class="fa fa-envelope-o fa-lg"></i></a><?php } ?>
			
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><span class="label label-danger pull-left"><?php echo $alerts; ?></span> <i class="fa fa-bell fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right alerts-dropdown">
        <li class="dropdown-header"><?php echo $text_order; ?></li>
        <li><a href="<?php echo $processing_status; ?>" style="display: block; overflow: auto;"><span class="label label-warning pull-right"><?php echo $processing_status_total; ?></span><?php echo $text_processing_status; ?></a></li>
        <li><a href="<?php echo $complete_status; ?>"><span class="label label-success pull-right"><?php echo $complete_status_total; ?></span><?php echo $text_complete_status; ?></a></li>
        <li><a href="<?php echo $return; ?>"><span class="label label-danger pull-right"><?php echo $return_total; ?></span><?php echo $text_return; ?></a></li>
        <li class="divider"></li>
        <li class="dropdown-header"><?php echo $text_customer; ?></li>
        <li><a href="<?php echo $online; ?>"><span class="label label-success pull-right"><?php echo $online_total; ?></span><?php echo $text_online; ?></a></li>
        <li><a href="<?php echo $customer_approval; ?>"><span class="label label-danger pull-right"><?php echo $customer_total; ?></span><?php echo $text_approval; ?></a></li>
        <li class="divider"></li>
        <li class="dropdown-header"><?php echo $text_product; ?></li>
        <li><a href="<?php echo $product; ?>"><span class="label label-danger pull-right"><?php echo $product_total; ?></span><?php echo $text_stock; ?></a></li>
        <li><a href="<?php echo $review; ?>"><span class="label label-danger pull-right"><?php echo $review_total; ?></span><?php echo $text_review; ?></a></li>
        <li class="divider"></li>
        <li class="dropdown-header"><?php echo $text_affiliate; ?></li>
        <li><a href="<?php echo $affiliate_approval; ?>"><span class="label label-danger pull-right"><?php echo $affiliate_total; ?></span><?php echo $text_approval; ?></a></li>
      </ul>
    </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-life-ring fa-lg"></i></a>
      <ul class="dropdown-menu dropdown-menu-right">
        <li class="dropdown-header"><?php echo $text_store; ?> <i class="fa fa-shopping-cart"></i></li>
        <?php foreach ($stores as $store) { ?>
        <li><a href="<?php echo $store['href']; ?>" target="_blank"><?php echo $store['name']; ?></a></li>
        <?php } ?>
        <li class="divider"></li>
        <li class="dropdown-header"><?php echo $text_help; ?> <i class="fa fa-life-ring"></i></li>
        <li><a href="http://myopencart.com" target="_blank"><?php echo $text_homepage; ?></a></li>
        <li><a href="http://docs.myopencart.com" target="_blank"><?php echo $text_documentation; ?></a></li>
        <li><a href="https://opencartforum.com" target="_blank"><?php echo $text_support; ?></a></li>
      </ul>
    </li>
    <li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
  </ul>
  <?php } ?>
</header>
