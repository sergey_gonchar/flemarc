<?php
class ControllerModulePivaNegozio extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('module/piva_negozio');
		$this->document->setTitle($this->language->get('piva_title'));
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$arr_piva = array('piva_required' => $this->request->post['piva_required'], 'piva_length' => (int)$this->request->post['piva_length']);
			$this->model_setting_setting->editSetting('piva_negozio', $arr_piva);
			$this->session->data['success'] = $this->language->get('piva_success_text');
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else if (isset($this->session->data['error']) ) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		}
		else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
	
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('piva_title'),
			'href'      => $this->url->link('module/piva_negozio', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$data['button_save'] = $this->language->get('piva_save');
		$data['button_cancel'] = $this->language->get('piva_cancel');

		if (isset($this->request->post['piva_required'])) {
			$data['piva_required'] = $this->request->post['piva_required'];
		} else {
			$data['piva_required'] = $this->config->get('piva_required');
		}
		
		if (isset($this->request->post['piva_length'])) {
			$data['piva_length'] = $this->request->post['piva_length'];
		} else {
			$data['piva_length'] = $this->config->get('piva_length');
		}
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['action'] = $this->url->link('module/piva_negozio', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$data['heading_title'] = $this->language->get('piva_title');
		$data['description'] = $this->language->get('piva_description');
		$data['entry_required'] = $this->language->get('piva_entry_required');
		$data['entry_required_description'] = $this->language->get('piva_entry_required_description');
		$data['entry_length'] = $this->language->get('piva_entry_length');
		$data['entry_length_description'] = $this->language->get('piva_entry_length_description');
		$data['yes'] = $this->language->get('piva_yes');
		$data['no'] = $this->language->get('piva_no');

		$data['modules'] = array();
		
		if (isset($this->request->post['piva_negozio_module'])) {
			$data['modules'] = $this->request->post['piva_negozio_module'];
		} else { 
			$data['modules'] = $this->config->get('piva_negozio_module');
		}

		$this->load->model('design/layout');
		
		$data['layouts'] = $this->model_design_layout->getLayouts();
								
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
				
		$this->response->setOutput($this->load->view('module/piva_negozio.tpl', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/piva_negozio')) {
			$this->error['warning'] = $this->language->get('piva_error_access');
		}
		
		if (isset($this->request->post['piva_negozio_module'])) {
			foreach ($this->request->post['piva_negozio_module'] as $key => $value) {				
				
			}
		}	
				
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>
