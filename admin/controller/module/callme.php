<?php
class ControllerModuleCallme extends Controller {
	private $error = array();

	public function index() {
        
		$this->load->language('module/callme');
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('callme', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/callme', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/callme', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['callme_status'])) {
			$data['callme_status'] = $this->request->post['callme_status'];
		} else {
			$data['callme_status'] = $this->config->get('callme_status');
		}

		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/callme.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/control')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	public function install(){
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "callme`
						 (`callme_id` INT(11) NOT NULL AUTO_INCREMENT, 
						 `customer_phone` VARCHAR(200) NULL DEFAULT NULL,
						 `customer_name` VARCHAR(100) NULL DEFAULT NULL,
						 `customer_comment` VARCHAR(100) NULL DEFAULT NULL,
						 `manager_comment` VARCHAR(100) NULL DEFAULT NULL,						 						 
						 `time` VARCHAR(100) NULL DEFAULT NULL,						 
						 `store_id` VARCHAR(100) NULL DEFAULT NULL,						 
						 `date_created` DATETIME  NOT NULL DEFAULT '0000-00-00 00:00:00', 
					 	 `language_id` VARCHAR(100) NULL DEFAULT '".$this->config->get('config_language_id')."',
						  PRIMARY KEY (`callme_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
				  
	}
		
	public function uninstall()	{
		  $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "callme`");
	}
	
}