<?php
class ControllerModuleSubscription extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/subscription');
        $this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_cupon'] = $this->language->get('entry_cupon');
		$data['entry_status'] = $this->language->get('entry_status');
        $data['entry_text'] = $this->language->get('entry_text');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('subscription', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('module/subscription', 'token=' . $this->session->data['token'], 'SSL'));
		}

		

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['newsletter_cupon'])) {
			$data['error_cupon'] = $this->error['newsletter_cupon'];
		} else {
			$data['error_cupon'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/subscription', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/subscription', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		 $config_data = array(	
		'subscription_cupon',
		'subscription_content_tab',
		'subscription_status'
        );
        
        foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $data[$conf] = $this->request->post[$conf];
            } else {
                $data[$conf] = $this->config->get($conf);
            }
        }
				
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/subscription.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/control')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	public function install(){
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "subscription`
						 (`subscribe_id` INT(11) NOT NULL AUTO_INCREMENT, 
						 `customer_email` VARCHAR(200) NULL DEFAULT NULL,
						 `customer_name` VARCHAR(100) NULL DEFAULT NULL,
						 `store_id` VARCHAR(100) NULL DEFAULT NULL,
						 `date_created` DATETIME  NOT NULL DEFAULT '0000-00-00 00:00:00', 
					 	 `language_id` VARCHAR(100) NULL DEFAULT '".$this->config->get('config_language_id')."',
						  PRIMARY KEY (`subscribe_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
				  
	}
		
	public function uninstall()	{
		  $this->load->model('setting/setting');
		  $this->model_setting_setting->deleteSetting('subscription',0);
		  $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "subscription`");
	}
	
}