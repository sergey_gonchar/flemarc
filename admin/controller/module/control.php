<?php
class ControllerModuleControl extends Controller {
    private $error = array(); 
    
    public function index() {   
	
		$language = $this->load->language('module/control');
        $data = array_merge($language);
		

        $this->document->setTitle($this->language->get('heading_title'));
     
        $this->load->model('setting/setting');
        
        $this->load->model('tool/image');  
        
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('control', $this->request->post);    

            $this->session->data['success'] = $this->language->get('text_success');
        
            $this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }
		
            $data['text_image_manager'] = 'Image manager';
            $data['token'] = $this->session->data['token'];       
        
        $text_strings = array(
                'heading_title',
                'text_enabled',
                'text_disabled',
                'text_content_top',
                'text_content_bottom',
                'text_column_left',
                'text_column_right',
                'entry_status',
                'entry_sort_order',
                'button_save',
                'button_cancel',				
        );
        
        foreach ($text_strings as $text) {
            $data[$text] = $this->language->get($text);
        }
        

        // store config data
        
        $config_data = array(

        //Form
		'control_thema',
		'control_logo',
		'control_callme',
		'control_quicklogin',
		'control_lastname',
		'control_curlang',
		'control_telephone',
		'control_brand_menu',
		'control_wishlist_menu',
		'control_adress_menu',		
		'control_lavel1',
		'control_lavel2',
		'control_lavel3',
        'control_menutext',
        'control_phone_mask',		
		'control_custom_info_status',
		'control_title_html',
		'control_content_info',		
		'control_custom_account_status',
		'control_content_account',				
		'control_menu_title',
		'control_menu_hidden',
		'control_promo',
		'control_promo_url',
		'control_filter',
		'control_view',
		'control_imgcategory',
		'control_stock',
		'control_compare',
		'control_wishlist',
		'control_quickview',
		'control_option_title',
		'control_back_to_top',
		'control_minprice',
		'control_fastorder',
		'control_outstock',
		'control_quantity',
		'control_additional',
		'control_zoom_image',		
		'control_description',
		'control_review',
		'control_autoprice',
		'control_firstoption',
		'control_size_title',
		'control_custom_tab_status',
		'control_title_tab',
		'control_content_tab',
		'control_custom_tab2_status',
		'control_title_tab2',
		'control_content_tab2',
		'control_custom_tab4_status',
		'control_title_tab4',
		'control_content_tab4',
		'control_custom_css',
		'control_map_adress',
		'control_map_width',
		'control_map_height',
		'control_map_zoom',
		'control_islands_info',
		'control_islands_color',
		'control_callmebtn',
		'control_info_shop',
		'control_vk_url',
		'control_ok_url',
		'control_fb_url',
		'control_tw_url',
		'control_ig_url',
		'control_google_url',
		'control_menu_urltitle1',
		'control_menu_urltitle2',
		'control_menu_urltitle3',
		'control_menu_urltitle4',
		'control_menu_url1',
		'control_menu_url2',
		'control_menu_url3',
		'control_menu_url4',
		'control_menu_top_urltitle1',
		'control_menu_top_urltitle2',
		'control_menu_top_urltitle3',
		'control_menu_top_urltitle4',		
		'control_menu_top_url1',
		'control_menu_top_url2',
		'control_menu_top_url3',
		'control_menu_top_url4',
        );
        
        foreach ($config_data as $conf) {
            if (isset($this->request->post[$conf])) {
                $data[$conf] = $this->request->post[$conf];
            } else {
                $data[$conf] = $this->config->get($conf);
            }
        }
    
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        
        $data['breadcrumbs'] = array();
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
        );
        
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('module/control', 'token=' . $this->session->data['token'], 'SSL')
        );
        
        $data['action'] = $this->url->link('module/control', 'token=' . $this->session->data['token'], 'SSL');
        
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

    
        //This code handles the situation where you have multiple instances of this module, for different layouts.
        if (isset($this->request->post['control_module'])) {
            $modules = explode(',', $this->request->post['control_module']);
        } elseif ($this->config->get('control_module') != '') {
            $modules = explode(',', $this->config->get('control_module'));
        } else {
            $modules = array();
        }           
                
        
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
        
        $data['modules'] = $modules;
        
        if (isset($this->request->post['control_module'])) {
            $data['control_module'] = $this->request->post['control_module'];
        } else {
            $data['control_module'] = $this->config->get('control_module');

		}
		
		$data['control_modules'] = array();


		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/control.tpl', $data));

    }
    
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/control')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
 