<?php

require (DIR_SYSTEM . 'fpdf/fpdf.php');

require (DIR_SYSTEM . 'phpqrcode/qrlib.php');

class PDF extends FPDF

{
	var $footer_name;var $footer_name_color;
	var $footer_email;var $footer_email_color;
	var $footer_phone;var $footer_phone_color;
	var $istitul_page = false;
	var $add_footer = false;
	var $add_footer_titul = false;
	
	//опции вывода полей
	var $name_f_out;
	var $name_text_field;
	var $name_text_color;
	var $name_znach_color;
	
	var $model_f_out;
	var $model_text_field;
	var $model_text_color;
	var $model_znach_color;
	
	var $category_f_out;
	var $category_text_field;
	var $category_text_color;
	var $category_znach_color;
	
	var $manufacture_f_out;
	var $manufacture_text_field;
	var $manufacture_text_color;
	var $manufacture_znach_color;
	
	var $price_f_out;
	var $price_text_field;
	var $price_text_color;
	var $price_znach_color;
	var $price_old_out;
	var $price_discount_out;
	var $price_old_znach_color;
	var $price_discount_f_out;
	var $price_discount_text_field;
	var $price_discount_text_color;
	var $price_discount_znach_color;

	
	var $description_f_out;
	var $description_text_field;
	var $description_text_color;
	var $description_znach_color;
	
	var $url_f_out;
	var $url_text_field;
	var $url_text_color;
	var $url_znach_color;
	
	//опции вывода содержания
	
	var $content_text_field;
	var	$content_text_color;
	var	$content_fon;
	var	$content_element_color;
	var	$content_element_fon;
	var	$content_razdelitel_color;
	var	$content_razdelitel_fon;
	var	$add_content;
	

	// var $ftp_server="ftp.han.16mb.com";
	// var	$ftp_user_name="u674126994";
	// var	$ftp_user_pass="kalkakalka";

	var $ftp_server;
	var $ftp_user_name;
	var $ftp_user_pass;
	var $ftp_success;
	/*
	function Header()
	{
	$this->AddFont('arial','','arial.php');

	// $this->Image('logo.png',10,8,33);

	$this->SetFontSize(10);
	$this->SetFont('arial');
	$this->SetXY(50, 10);
	$this->Cell(0,10,'This is a header',1,0,'C');
	}

	*/
	function Footer()
	{

		// $this->AddFont('arial','','arial.php');
		// $this->SetFontSize(10);
		// $this->SetFont('arial');
		// $this->SetXY(50,-15);
		// $this->SetTextColor(0,0,255);
		// $this->Write (5, 'Мы вконтакте https://vk.com/club79146351','https://vk.com/club79146351');

		if ($this->add_footer == true) {
			if ($this->istitul_page == false) {

				// Позиционирование в  1.5 см от нижней границы

				$this->SetY(-15);

				// arial italic 8

				$this->SetFont('arial', '', 8);

				// Номер страницы
				
				
				list($r, $g, $b) = sscanf($this->footer_name_color, "#%02x%02x%02x");
				$this->SetTextColor($r, $g, $b);
				$this->Cell(60, 0, $this->footer_name, 0, 0, 'L');
				
				list($r, $g, $b) = sscanf($this->footer_email_color, "#%02x%02x%02x");
				$this->SetTextColor($r, $g, $b);
				$this->Cell(60, 0, '   ' . $this->footer_email, 0, 0, 'C');
				
				list($r, $g, $b) = sscanf($this->footer_phone_color, "#%02x%02x%02x");
				$this->SetTextColor($r, $g, $b);
				$this->Cell(60, 0, '   ' . $this->footer_phone, 0, 0, 'R');
				
				$this->Ln(4);
				$this->SetTextColor(0, 0, 0);
				
				$this->Cell(0, 0, mb_convert_encoding('Cтраница ' . $this->PageNo() , "windows-1251", "UTF-8") , 0, 0, 'C');
				$this->SetTextColor(0, 0, 0);
			}
			else {
				if ($this->add_footer_titul == true) {

					// Позиционирование в  1.5 см от нижней границы

					$this->SetY(-15);

					// arial italic 8

					$this->SetFont('arial', '', 8);

					// Номер страницы

					$this->SetTextColor(241, 168, 15);
					list($r, $g, $b) = sscanf($this->footer_name_color, "#%02x%02x%02x");
					$this->SetTextColor($r, $g, $b);
					$this->Cell(60, 0, $this->footer_name, 0, 0, 'L');
					
					list($r, $g, $b) = sscanf($this->footer_email_color, "#%02x%02x%02x");
					$this->SetTextColor($r, $g, $b);
					$this->Cell(60, 0, '   ' . $this->footer_email, 0, 0, 'C');
					
				list($r, $g, $b) = sscanf($this->footer_phone_color, "#%02x%02x%02x");
				$this->SetTextColor($r, $g, $b);
				$this->Cell(60, 0, '   ' . $this->footer_phone, 0, 0, 'R');
				
					$this->Ln(4);
					$this->SetTextColor(0, 0, 0);
					$this->Cell(0, 0, mb_convert_encoding('Cтраница ' . $this->PageNo() , "windows-1251", "UTF-8") , 0, 0, 'C');
					$this->SetTextColor(0, 0, 0);
				}
				else {
				}
			}
		}
	}

	function Output_ftp()
	{
		$filename = uniqid('php_') . ".pdf";
		$file = DIR_DOWNLOAD . $filename;
		$this->Output($file, 'F');
		$remote_file = "system/storage/download/" . $filename;

		// connecting

		$conn_id = @ftp_connect($this->ftp_server);
		if ($conn_id == FALSE) return array(
			"error" => "Нет соединения с FTP cервером! Проверьте правильность заполнения имени FTP сервера.\n",
			"success" => ""
		);

		// auth

		$login_result = @ftp_login($conn_id, $this->ftp_user_name, $this->ftp_user_pass);
		if ($login_result == FALSE) {
			@unlink($file);
			return array(
				"error" => "Ошибка авторизации FTP! Неправильно указаны имя пользователя и/или пароль FTP аккаунта! Проверьте правильность заполнения указанных полей.\n",
				"success" => ""
			);
		}

		@ftp_pasv($conn_id, true);

		// put file

		if (@ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) {
			@unlink($file);
			@ftp_close($conn_id);
			return array(
				"error" => "",
				"success" => "PDF каталог успешно загружен на FTP-cервер!\n"
			);
		}
		else {
			@unlink($file);
			@ftp_close($conn_id);
			return array(
				"error" => "PDF каталог не загружен на FTP-cервер! Попробуйте еще раз!\n",
				"success" => ""
			);
		}
	}
}

class ControllerModuleTopdf extends Controller

{
	private $error = array();
	private $default_limits = array(10,50,100,200,300,400,500,600, 700, 800, 900, 1000);
	public function index()
	{   
		$this->document->addStyle('view/javascript/jquery/jquery-ui/jquery-ui.min.css');
		//$this->document->addScript('/view/javascript/jquery/jquery.maskedinput.min.js');
		$this->language->load('module/topdf');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('module/topdf');
		$this->getList();
	}

	protected function getList()
	{
			
		
		$this->document->addStyle('view/javascript/jquery/jquery-ui/jquery-ui.min.css');
		
		$this->load->model('setting/setting');
				
		
		if (isset($this->request->post['FTPServer1']) && !empty($this->request->post['FTPServer1'])) {
			$data['ftp_server'] = $this->request->post['FTPServer1'];
		}
		else {
			
			$data['ftp_server'] = $this->config->get('topdf_ftp_server');
			
		}

		if (isset($this->request->post['FTPUser1']) && !empty($this->request->post['FTPUser1'])) {
			$data['ftp_user_name'] = $this->request->post['FTPUser1'];
		}
		else {
			$data['ftp_user_name'] = $this->config->get('topdf_ftp_user_name');
		}

		if (isset($this->request->post['FTPPassword1']) && !empty($this->request->post['FTPPassword1'])) {
			$data['ftp_user_pass'] = $this->request->post['FTPPassword1'];
					}
		else {
			$data['ftp_user_pass'] = $this->config->get('topdf_ftp_user_pass');
		}
		
		
		
		$pdf= new PDF(); 
		
		if (isset($this->request->post['field_option'])&& !empty($this->request->post['field_option']) ) {
			$this->set_option_field($pdf,$this->request->post['field_option']); 
		}
		
		else   {
		 
		 $this->get_option_field($pdf);
		 				
		}
		
		
		 if (isset($this->request->post['content_option'])&& !empty($this->request->post['content_option']) ) {
			$this->set_option_content($pdf,$this->request->post['content_option']); 
		}
		
		else   {
		 
		$this->get_option_content($pdf); 
		 				
		}
		 
		 	
		 	$data['name_f_out']=$pdf->name_f_out;	
			$data['name_text_field']=$pdf->name_text_field;
			$data['name_text_color']=$pdf->name_text_color;
			$data['name_znach_color']=$pdf->name_znach_color;
						
			$data['model_f_out']=$pdf->model_f_out;
			$data['model_text_field']=$pdf->model_text_field;
			$data['model_text_color']=$pdf->model_text_color;
			$data['model_znach_color']=$pdf->model_znach_color;
						
			$data['category_f_out']=$pdf->category_f_out;
			$data['category_text_field']=$pdf->category_text_field;
			$data['category_text_color']=$pdf->category_text_color;
			$data['category_znach_color']=$pdf->category_znach_color;
						
			$data['manufacture_f_out']=$pdf->manufacture_f_out;
			$data['manufacture_text_field']=$pdf->manufacture_text_field;
			$data['manufacture_text_color']=$pdf->manufacture_text_color;
			$data['manufacture_znach_color']=$pdf->manufacture_znach_color;
			
			
			$data['price_f_out']=$pdf->price_f_out;
			$data['price_text_field']=$pdf->price_text_field;
			$data['price_text_color']=$pdf->price_text_color;
			$data['price_znach_color']=$pdf->price_znach_color;
			
			$data['price_old_out']=$pdf->price_old_out;
			$data['price_discount_out']=$pdf->price_discount_out;
			$data['price_old_znach_color']=$pdf->price_old_znach_color;
			$data['price_discount_f_out']=$pdf->price_discount_f_out;
			$data['price_discount_text_field']=$pdf->price_discount_text_field;
			$data['price_discount_text_color']=$pdf->price_discount_text_color;
			$data['price_discount_znach_color']=$pdf->price_discount_znach_color;
			
			$data['description_f_out']=$pdf->description_f_out;
			$data['description_text_field']=$pdf->description_text_field;
			$data['description_text_color']=$pdf->description_text_color;
			$data['description_znach_color']=$pdf->description_znach_color;
						
			$data['url_f_out']=$pdf->url_f_out;
			$data['url_text_field']=$pdf->url_text_field;
			$data['url_text_color']=$pdf->url_text_color;
			$data['url_znach_color']=$pdf->url_znach_color;
			
			
			$data['content_text_field']=$pdf->content_text_field;
			$data['content_text_color']=$pdf->content_text_color;
			$data['content_fon']=$pdf->content_fon;
			$data['content_element_color']=$pdf->content_element_color;
			$data['content_element_fon']=$pdf->content_element_fon;
			$data['content_razdelitel_color']=$pdf->content_razdelitel_color;
			$data['content_razdelitel_fon']=$pdf->content_razdelitel_fon;
					
			
					
		 
		 $data['InputName'] = $this->config->get('topdf_footer_InputName');
		 $data['InputEmail'] = $this->config->get('topdf_footer_InputEmail');
		  $data['InputPhone'] = $this->config->get('topdf_footer_InputPhone');
		 $data['InputNameColor'] = $this->config->get('topdf_footer_InputNameColor');
		 $data['InputEmailColor'] = $this->config->get('topdf_footer_InputEmailColor');
		 $data['InputPhoneColor'] = $this->config->get('topdf_footer_InputPhoneColor');
		 		
		
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		}
		else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		}
		else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		}
		else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		}
		else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		}
		else {
			$filter_status = null;
		}
		
		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = null;
		}
		

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		}
		else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		}
		else {
			$order = 'ASC';
		}

		 if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
		 $limit = $this->default_limits[0];
		}

        if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		

		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url.= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url.= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url.= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url.= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url.= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

		if (isset($this->request->get['sort'])) {
			$url.= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url.= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
		

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home') ,
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module') ,
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title') ,
			'href' => $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);
		$data['clear'] = $this->url->link('module/topdf/clear', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['add'] = $this->url->link('module/topdf/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['savepdf'] = $this->url->link('module/topdf/savepdf', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['pdf_download'] = $this->url->link('module/topdf/pdf_download', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['pdf_ftp'] = $this->url->link('module/topdf/pdf_ftp', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data['pdf_delete'] = $this->url->link('module/topdf/pdf_delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');
		$data['column_category'] = $this->language->get('column_category');
		$data['column_manufacturer'] = $this->language->get('column_manufacturer');
		$data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_model'] = $this->language->get('column_model');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');
		$data['column_description'] = $this->language->get('column_description');
		$data['column_url'] = $this->language->get('column_url');
		$data['column_qrcode'] = $this->language->get('column_qrcode');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['button_edit'] = $this->language->get('button_edit');
		/*		$data['button_copy'] = $this->language->get('button_copy');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');
		*/
		$data['button_add'] = $this->language->get('button_add');
		$data['button_clear'] = $this->language->get('button_clear');
		$data['button_savepdf'] = $this->language->get('button_savepdf');
		$data['button_filter'] = $this->language->get('button_filter');
		$data['text_add_titul'] = $this->language->get('text_add_titul');
		$data['button_sbros'] = $this->language->get('button_sbros');
		$data['head1'] = $this->language->get('head1');
		$data['head1_1'] = $this->language->get('head1_1');
		$data['head1_2'] = $this->language->get('head1_2');
		$data['head2'] = $this->language->get('head2');
		$data['head2_1'] = $this->language->get('head2_1');
		$data['head2_2'] = $this->language->get('head2_2');
		$data['head3'] = $this->language->get('head3');
		$data['token'] = $this->session->data['token'];
		
		
		if (file_exists(DIR_DOWNLOAD . 'catalog.pdf')) {
    $data['pdf_fileinfo']="В последний раз каталог был изменен: " . date ("d.m.Y в H:i.", filemtime(DIR_DOWNLOAD.'catalog.pdf'));
		}
		else $data['pdf_fileinfo']='';
		
		
		$this->load->model('tool/image');
		$this->load->model('module/topdf');
		
		//////////////////////////////////////////////////
		
		    if (! function_exists('array_column')) {
        function array_column(array $input, $columnKey, $indexKey = null) {
            $array = array();
            foreach ($input as $value) {
                if ( ! isset($value[$columnKey])) {
                    trigger_error("Key \"$columnKey\" does not exist in array");
                    return false;
                }
                if (is_null($indexKey)) {
                    $array[] = $value[$columnKey];
                }
                else {
                    if ( ! isset($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not exist in array");
                        return false;
                    }
                    if ( ! is_scalar($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not contain scalar value");
                        return false;
                    }
                    $array[$value[$indexKey]] = $value[$columnKey];
                }
            }
            return $array;
        }
    }
    
		//////////////////////////////////////////////////
		
		
		
		
		
		
		
		
		
		
		
		
		
		$data['products'] = array();
		$tmp = $this->model_module_topdf->getCategories();
		$data['uni_categories']= array_column($tmp, 'name');
		foreach($data['uni_categories'] as &$value) {
			$value= str_replace ( ",", "_*_", $value );
			
		}
		
		$data ['start'] = ($page - 1) * $limit;
        $data ['limit'] = $limit;
        	
		$filter_data = array(
			'filter_name' => $filter_name,
			'filter_model' => $filter_model,
			'filter_price' => $filter_price,
			'filter_quantity' => $filter_quantity,
			'filter_status' => $filter_status,
			'filter_category' => $filter_category,
			'sort' => $sort,
			'order' => $order,
			'start' => ($page - 1) * $limit ,
			'limit' => $limit
		);
	
		$product_total = $this->model_module_topdf->getTotalProducts($filter_data);
		$results = $this->model_module_topdf->getProducts($filter_data);
		foreach($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			}
			else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}

			$special = false;
			$product_specials = $this->model_module_topdf->getProductSpecials($result['product_id']);
			foreach($product_specials as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
					$special = $product_special['price'];
					break;
				}
			}

			$data['products'][] = array(
				'product_id' => $result['product_id'],
				'category' => $this->model_module_topdf->getProductCatNames($result['product_id']) ,
				'image' => $image,
				'name' => $result['name'],
				'model' => $result['model'],
				'price' => $result['price'],
				'special' => $special,
				'quantity' => $result['quantity'],
				'status' => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled') ,
				'edit' => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
			);
		}



		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		}
		else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		}
		else {
			$data['success'] = '';
		}

		if (isset($this->session->data['warning'])) {
			$data['error_warning'] = $data['error_warning'] . $this->session->data['warning'];
			unset($this->session->data['warning']);
		}
		else {
			$data['error_warning'] = $data['error_warning'] . '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		}
		else {
			$data['selected'] = array();
		}

		if (isset($this->session->data['arr_topdf'])) {
			$data['arr_topdf'] = $this->session->data['arr_topdf'];

			// unset($this->session->data['success']);

		}
		else {
			$data['arr_topdf'] = null;
		}

		$url = '';
		
		
		 $data['limits'] = array();

        foreach($this->default_limits as $default_limit) {
            $data['limits'][] = array(
    			'value' => $default_limit,
    			'href'  => $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url . '&limit='. $default_limit, 'SSL')
    		);
        }
		
		
		if (isset($this->request->get['filter_name'])) {
			$url.= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url.= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url.= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url.= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url.= '&filter_status=' . $this->request->get['filter_status'];
			
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		

		if ($order == 'ASC') {
			$url.= '&order=DESC';
		}
		else {
			$url.= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url.= '&page=' . $this->request->get['page'];
			
			
		}
		
		
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
		

		$data['sort_name'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$data['sort_model'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
		$data['sort_price'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
		$data['sort_quantity'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, 'SSL');
		$data['sort_status'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		$data['sort_order'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');
		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url.= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url.= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url.= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url.= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url.= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		

		if (isset($this->request->get['sort'])) {
			$url.= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url.= '&order=' . $this->request->get['order'];
			
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			
		}
		
	
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination') , ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit) , $product_total, ceil($product_total / $limit));
		$data['filter_name'] = $filter_name;
		$data['filter_model'] = $filter_model;
		$data['filter_price'] = $filter_price;
		$data['filter_quantity'] = $filter_quantity;
		$data['filter_status'] = $filter_status;
		$data['filter_category'] = $filter_category;
		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('module/topdf.tpl', $data));
	}

	public function savepdf1()
	{

		// $this->load->library('customer');
		//	$this->load->library('tax');

$count_arr_topdf=count($this->session->data['arr_topdf']);

 $fp = fopen(DIR_DOWNLOAD.'pdf_time.txt', 'ab');
 
 fputs($fp,'
');
 fputs($fp,$count_arr_topdf.' товаров (поля: '.implode(",",$this->request->post['sort_hidden']).') - '.date("H:i:s"));
 //$current = file_get_contents(DIR_DOWNLOAD."pdf_time.txt");

//$current .= date("H:i:s");

		$progress_shag = 1 / $count_arr_topdf;
		$this->session->data['progress_value'] = 0;
		$this->registry->set('customer', new Customer($this->registry));
		$mytax = new Tax($this->registry);
		$pdf = new PDF();
		$pdf->AddFont('arial', '', 'arial.php');
		$pdf->AddFont('arialbd', '', 'arialbd.php');
		
		
					
		//$this->language->load('catalog/product');
		//$this->document->setTitle($this->language->get('heading_title'));
		
		$this->language->load('module/topdf');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('module/topdf');
		

		$flag_nach_tov = 0; // флаг первой страницы
		if (isset($this->request->post['add_footer'])) {
			$pdf->add_footer = true;
		}

		if (isset($this->request->post['footer_titul'])) {
			$pdf->add_footer_titul = true;
		}
		
		if (isset($this->request->post['add_content'])) {
			$pdf->add_content = true;
		}
		

		if (isset($this->request->post['FTPServer'])) {
			$pdf->ftp_server = $this->request->post['FTPServer'];
		}

		if (isset($this->request->post['FTPUser'])) {
			$pdf->ftp_user_name = $this->request->post['FTPUser'];
		}

		if (isset($this->request->post['FTPPassword'])) {
			$pdf->ftp_user_pass = $this->request->post['FTPPassword'];
		}
		
		$pdf->footer_name = mb_convert_encoding($this->request->post['InputName'] , "windows-1251", "UTF-8");
		$pdf->footer_email = mb_convert_encoding($this->request->post['InputEmail'] , "windows-1251", "UTF-8");
		$pdf->footer_phone = mb_convert_encoding($this->request->post['InputPhone'] , "windows-1251", "UTF-8");
		$pdf->footer_name_color=$this->request->post['InputNameColor'];
		$pdf->footer_email_color=$this->request->post['InputEmailColor'];
		$pdf->footer_phone_color=$this->request->post['InputPhoneColor'];
		
		
		
		
		
		if (isset($this->request->post['field_option'])&& !empty($this->request->post['field_option']) ) {
			$this->set_option_field($pdf,$this->request->post['field_option']); 
		}
		
		else   {
		 
		 $this->get_option_field($pdf);
		 				
		}
		
		 if (isset($this->request->post['content_option'])&& !empty($this->request->post['content_option']) ) {
			$this->set_option_content($pdf,$this->request->post['content_option']); 
		}
		
		else   {
		 
		$this->get_option_content($pdf); 
		 				
		}
		
		
		if (isset($this->request->files) && count($this->request->files) != 0) {
			$pdf->AddPage();
			$pdf->SetFontSize(14);
			$pdf->SetFont('arial');
			$pdf->istitul_page = true;
			$uploadfile = DIR_UPLOAD . basename($this->request->files['picture']['name']);
			move_uploaded_file($this->request->files['picture']['tmp_name'], $uploadfile);
			$pdf->Image($uploadfile, 0, 0, 210);
			unlink($uploadfile);
		}
		

		
		if (isset($this->session->data['arr_topdf']) && $this->validateSavepdf()) {
			
	
$cur_cat="";
$cat_start=0;	
	
	 if (isset($this->request->post['add_content'])){
			
		
		
			//cортировка по категориям
		
		
		
		
		//////////////////////////////////////////////////
		
		    if (! function_exists('array_column')) {
        function array_column(array $input, $columnKey, $indexKey = null) {
            $array = array();
            foreach ($input as $value) {
                if ( ! isset($value[$columnKey])) {
                    trigger_error("Key \"$columnKey\" does not exist in array");
                    return false;
                }
                if (is_null($indexKey)) {
                    $array[] = $value[$columnKey];
                }
                else {
                    if ( ! isset($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not exist in array");
                        return false;
                    }
                    if ( ! is_scalar($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not contain scalar value");
                        return false;
                    }
                    $array[$value[$indexKey]] = $value[$columnKey];
                }
            }
            return $array;
        }
    }
    
		//////////////////////////////////////////////////
		
		
		
		
		
		
		
		
		
		
		
		foreach($this->session->data['arr_topdf'] as $id) {
		$contents[]=array_column($this->model_module_topdf->getProductCatNames($id), 'name');
			
	}
	

	foreach($contents as &$value) {  $value=$value[0];    }
	
	$contents=array_unique($contents);
		
	$contents = array_values($contents);
	
	asort($contents);
		
	//print_r($contents3);
		//echo $contents3[0];echo $contents3[1];
	
	$pdf->AddPage();
	$pdf->istitul_page = false;
	$pdf->SetFontSize(14);
$pdf->SetFont('arial');
	

$r_shrift_zag=18;
$r_shrift_polya=10;


$w1=64;
$w2=80;
$w3=48;
$w=$w1+$w2+$w3;
$h1=76;
	
		
		 $pdf->SetFont('arial','',$r_shrift_zag);
		 $pdf->SetFontSize($r_shrift_zag);
		 $pdf->SetFillColor(69, 139, 196);
		 $pdf->SetFillColor( 37, 118, 185);
		 $pdf->SetTextColor(256,256,256);
		 
		 
		 
		list($r, $g, $b) = sscanf($pdf->content_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		list($r, $g, $b) = sscanf($pdf->content_fon, "#%02x%02x%02x");
		$pdf->SetFillColor($r, $g, $b);
		
		
		 $pdf->MultiCell($w1*3,20,mb_convert_encoding($pdf->content_text_field.': ', "windows-1251","UTF-8"),0, 'C',true);
		 $pdf->Ln(5);
		 $pdf->SetTextColor(0,0,0);
	
	
	$pdf->SetTextColor(29, 101, 159);
	$pdf->SetTextColor(228, 88, 43);
	
	foreach($contents as $cat) { $link[$cat]=$pdf->AddLink();
									$str_tmp = html_entity_decode($cat, ENT_QUOTES, 'UTF-8');
									$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
									list($r, $g, $b) = sscanf($pdf->content_element_color, "#%02x%02x%02x");
									$pdf->SetTextColor($r, $g, $b);
									list($r, $g, $b) = sscanf($pdf->content_element_fon, "#%02x%02x%02x");
									$pdf->SetFillColor($r, $g, $b);
									//$pdf->Write(5,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),$link[$cat]);
									// $pdf->MultiCell(,,mb_convert_encoding($pdf->content_text_field.': ', "windows-1251","UTF-8"),0, 'C',true);
									 $pdf->Cell(0,5,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0,0,'L',1,$link[$cat]);
									 $pdf->SetTextColor(0,0,0);
									$pdf->Ln(10);
	   }
	$pdf->SetTextColor(0,0,0);
	

$kol_tovarov=$count_arr_topdf;



 $mas_prod3=array();

for($v=0;$v<$kol_tovarov;$v++){
	
	
$tmp_mas_id=$this->model_module_topdf->getProductCatNames($this->session->data['arr_topdf'][$v]);
	
 $mas_prod3[$this->session->data['arr_topdf'][$v]]= $tmp_mas_id[0]['name'];

}

 asort($mas_prod3);
 $mas_prod3=array_keys ($mas_prod3);
			
		
		
	$link_content=$pdf->AddLink($pdf->PageNo());
	$pdf->SetLink($link_content,0 ,1);
	//cортировка по категориям конец	
		
		
			
			
		}
		
		else   {
		 
		$mas_prod3=$this->session->data['arr_topdf'];
		$kol_tovarov=count($mas_prod3);
		 				
		}
	
	
	
	
	
	
	

	
	
	
	for($i=0;$i<$kol_tovarov;$i++){
	
	$mas_prod=$this->model_module_topdf->getProduct2($mas_prod3[$i]);	
	$mas_prod['category']=$this->model_module_topdf->getProductCatNames($mas_prod3[$i]);		
		

if (isset($this->request->post['add_content'])){

if($cur_cat!=$mas_prod['category'][0]['name']) {
		
		
		 $pdf->AddPage();
		
		 $pdf->SetXY($pdf->GetX(),$pdf->GetY()+100);
		 $pdf->SetLink($link[$mas_prod['category'][0]['name']]);
		 $pdf->SetFont('arial','',$r_shrift_zag);
		 $pdf->SetFontSize($r_shrift_zag);
		 $pdf->SetFillColor(76, 179, 86);
		 $pdf->SetTextColor(256,256,256);
		 
		 							list($r, $g, $b) = sscanf($pdf->content_razdelitel_color, "#%02x%02x%02x");
									$pdf->SetTextColor($r, $g, $b);
									list($r, $g, $b) = sscanf($pdf->content_razdelitel_fon, "#%02x%02x%02x");
									$pdf->SetFillColor($r, $g, $b);
		 
		 $str_tmp = html_entity_decode($mas_prod['category'][0]['name'], ENT_QUOTES, 'UTF-8');
		$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
			
		
		 $pdf->MultiCell($w1*3,28,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0, 'C',true);
		 $pdf->Image(DIR_IMAGE."/back_2.png", 0, 0, 10, 10, 'png', $link_content);
		// $pdf->Write(5,mb_convert_encoding($mas_prod['category'][0]['name'], "windows-1251","UTF-8"));
		$cur_cat=$mas_prod['category'][0]['name'];
		$cat_start=1;
		$kol_na_str=0;
	}
	else { $cat_start=0;
		
	}	
	
	// if ($cat_start==1) {  /*$pdf->AddPage(); $x1=$pdf->GetX(); $y1=$pdf->GetY(); $h1=0; */ $flag_nach_tov=1;}
		/*	foreach($this->session->data['arr_topdf'] as $product_id) {
				
				$mas_prod = $this->model_module_topdf->getProduct2($product_id);
				$mas_prod['category'] = $this->model_module_topdf->getProductCatNames($product_id);*/
	}
	
	else {
		
	}			
			if ($flag_nach_tov == 0 || $cat_start==1) {
					$flag_nach_tov = 1;
					$pdf->AddPage();
					$pdf->istitul_page = false;

					// $this->footer($pdf,'InputName','InputEmail');

					$pdf->SetFontSize(14);
					$pdf->SetFont('arial');
				}
				else
				if (isset($this->request->post['razryv'])) {
					$pdf->AddPage();
					$pdf->istitul_page = false;
					$pdf->SetFontSize(14);
					$pdf->SetFont('arial');
				} else $pdf->Ln(10);

				$this->session->data['progress_value']+= $progress_shag;

				// echo'<br />'. $this->session->data['progress_value'];

				if (isset($this->request->post['sort_hidden'])) {
					foreach($this->request->post['sort_hidden'] as $value) {
						$value = substr($value, 5);
						switch ($value) {
						case "price":
							{$func = 'func_' . $value;
								$this->$func($pdf, $mas_prod[$value], $mas_prod3[$i], $mas_prod['special'], $mas_prod['tax_class_id'], $mytax);
								break;
							}

						case "image": {
								$func = 'func_' . $value;
								$this->$func($pdf, $mas_prod[$value], $mas_prod3[$i]);
								break;
							}

						case "url": {
								$func = 'func_' . $value;
								$this->$func($pdf, $mas_prod3[$i]);
								break;
							}

						case "qrcode": {
								$func = 'func_' . $value;
								$this->$func($pdf, $mas_prod3[$i]);
								break;
							}

						default: {
								$func = 'func_' . $value;
								$this->$func($pdf, $mas_prod[$value]);
								break;
							}
						}
					}
				}
			}

			$data['pdf'] = $pdf;

			// echo strlen($pdf->buffer)."-".$pdf->buffer;
			// $pdf->Output(DIR_SYSTEM.'storage/download/ftp_exampledfg.pdf','F');

			if (isset($this->request->post['ftp'])) {

				// $this->error['warning']=$pdf->Output_ftp();

				$mas_result_out = $pdf->Output_ftp();
				$this->error['warning'] = $mas_result_out['error'];
				$this->session->data['success'] = $mas_result_out['success'];
			}

			$pdf->Output(DIR_DOWNLOAD . 'catalog.pdf', 'F'); //D - в файл I- в браузер

			// $this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			 fputs($fp,'----'.date("H:i:s"));
			/*
			if (isset($this->request->get['filter_category_id'])) {
				$url.= '&filter_category_id=' . (int)$this->request->get['filter_category_id'];
			}
			*/
			if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

			if (isset($this->request->get['filter_manufacturer_id'])) {
				$url.= '&filter_manufacturer_id=' . (int)$this->request->get['filter_manufacturer_id'];
			}

			if (isset($this->request->get['filter_name'])) {
				$url.= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url.= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url.= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['sort'])) {
				$url.= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url.= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url.= '&page=' . $this->request->get['page'];
			}

			// $this->redirect($this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url, 'SSL'));

		}

		$this->getList();
	}



public function savepdf2()
	{
       
    
		// $this->load->library('customer');
		//	$this->load->library('tax');

		$progress_shag = 1 / count($this->session->data['arr_topdf']);
		$this->session->data['progress_value'] = 0;
		$this->registry->set('customer', new Customer($this->registry));
		$mytax = new Tax($this->registry);
		$pdf = new PDF();
		$pdf->AddFont('arial', '', 'arial.php');
		$pdf->AddFont('arialbd', '', 'arialbd.php');
		
				
		$this->language->load('module/topdf');
		$this->document->setTitle($this->language->get('heading_title'));
		
		//$this->language->load('catalog/product');
		//$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('module/topdf');
		

		$flag_nach_tov = 0; // флаг первой страницы
		if (isset($this->request->post['add_footer'])) {
			$pdf->add_footer = true;
		}

		if (isset($this->request->post['footer_titul'])) {
			$pdf->add_footer_titul = true;
		}
		
		if (isset($this->request->post['add_content'])) {
			$pdf->add_content = true;
		}
		

		if (isset($this->request->post['FTPServer'])) {
			$pdf->ftp_server = $this->request->post['FTPServer'];
		}

		if (isset($this->request->post['FTPUser'])) {
			$pdf->ftp_user_name = $this->request->post['FTPUser'];
		}

		if (isset($this->request->post['FTPPassword'])) {
			$pdf->ftp_user_pass = $this->request->post['FTPPassword'];
		}
		
		$pdf->footer_name = mb_convert_encoding($this->request->post['InputName'] , "windows-1251", "UTF-8");
		$pdf->footer_email = mb_convert_encoding($this->request->post['InputEmail'] , "windows-1251", "UTF-8");
		$pdf->footer_phone = mb_convert_encoding($this->request->post['InputPhone'] , "windows-1251", "UTF-8");
		$pdf->footer_name_color=$this->request->post['InputNameColor'];
		$pdf->footer_email_color=$this->request->post['InputEmailColor'];
		$pdf->footer_phone_color=$this->request->post['InputPhoneColor'];
		
		
		
		
		
		if (isset($this->request->post['field_option'])&& !empty($this->request->post['field_option']) ) {
			$this->set_option_field($pdf,$this->request->post['field_option']); 
		}
		
		else   {
		 
		 $this->get_option_field($pdf);
		 				
		}
		
		 if (isset($this->request->post['content_option'])&& !empty($this->request->post['content_option']) ) {
			$this->set_option_content($pdf,$this->request->post['content_option']); 
		}
		
		else   {
		 
		$this->get_option_content($pdf); 
		 				
		}
		
		
		if (isset($this->request->files) && count($this->request->files) != 0) {
			$pdf->AddPage();//echo 'AddPage2'; 
			$pdf->SetFontSize(14);
			$pdf->SetFont('arial');
			$pdf->istitul_page = true;
			$uploadfile = DIR_UPLOAD . basename($this->request->files['picture']['name']);
			move_uploaded_file($this->request->files['picture']['tmp_name'], $uploadfile);
			$pdf->Image($uploadfile, 0, 0, 210);
			unlink($uploadfile);
		}
		
	
$link_content=$pdf->AddLink();
$ssilka_page_nazad=1;
$pdf->SetLink($link_content,0 ,$ssilka_page_nazad);	
			
		
		if (isset($this->session->data['arr_topdf']) && $this->validateSavepdf()) {
			
	

	//$pdf->AddPage();echo 'AddPage1'; 
	//$pdf->istitul_page = false;
	$pdf->SetFontSize(14);
$pdf->SetFont('arial');
	

$cur_cat="";
$cat_start=0;
$r_shrift_zag=18;
$r_shrift_polya=10;	
$w1=64;
$w2=80;
$w3=48;
$w=$w1+$w2+$w3;
$h1=74;

	
	 if (isset($this->request->post['add_content'])){
			
	$pdf->AddPage();
	$pdf->istitul_page = false;	
	$ssilka_page_nazad=$pdf->PageNo();	
	$pdf->SetLink($link_content,0 ,$ssilka_page_nazad);	
			//cортировка по категориям
		
		
		
		
		//////////////////////////////////////////////////
		
		    if (! function_exists('array_column')) {
        function array_column(array $input, $columnKey, $indexKey = null) {
            $array = array();
            foreach ($input as $value) {
                if ( ! isset($value[$columnKey])) {
                    trigger_error("Key \"$columnKey\" does not exist in array");
                    return false;
                }
                if (is_null($indexKey)) {
                    $array[] = $value[$columnKey];
                }
                else {
                    if ( ! isset($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not exist in array");
                        return false;
                    }
                    if ( ! is_scalar($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not contain scalar value");
                        return false;
                    }
                    $array[$value[$indexKey]] = $value[$columnKey];
                }
            }
            return $array;
        }
    }
    
		//////////////////////////////////////////////////
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		foreach($this->session->data['arr_topdf'] as $id) {
		$contents[]=array_column($this->model_module_topdf->getProductCatNames($id), 'name');
			
	}
	

	foreach($contents as &$value) {  $value=$value[0];    }
	
	$contents=array_unique($contents);
		
	$contents = array_values($contents);
	
	asort($contents);
		
	
		


	
		
		 $pdf->SetFont('arial','',$r_shrift_zag);
		 $pdf->SetFontSize($r_shrift_zag);
		 $pdf->SetFillColor(69, 139, 196);
		 $pdf->SetFillColor( 37, 118, 185);
		 $pdf->SetTextColor(256,256,256);
		 
		 
		 
		list($r, $g, $b) = sscanf($pdf->content_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		list($r, $g, $b) = sscanf($pdf->content_fon, "#%02x%02x%02x");
		$pdf->SetFillColor($r, $g, $b);
		
		
		 $pdf->MultiCell($w1*3,20,mb_convert_encoding($pdf->content_text_field.': ', "windows-1251","UTF-8"),0, 'C',true);
		 $pdf->Ln(5);
		 $pdf->SetTextColor(0,0,0);
	
	
	$pdf->SetTextColor(29, 101, 159);
	$pdf->SetTextColor(228, 88, 43);
	
	foreach($contents as $cat) { $link[$cat]=$pdf->AddLink();
									$str_tmp = html_entity_decode($cat, ENT_QUOTES, 'UTF-8');
									$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
									list($r, $g, $b) = sscanf($pdf->content_element_color, "#%02x%02x%02x");
									$pdf->SetTextColor($r, $g, $b);
									list($r, $g, $b) = sscanf($pdf->content_element_fon, "#%02x%02x%02x");
									$pdf->SetFillColor($r, $g, $b);
									//$pdf->Write(5,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),$link[$cat]);
									// $pdf->MultiCell(,,mb_convert_encoding($pdf->content_text_field.': ', "windows-1251","UTF-8"),0, 'C',true);
									 $pdf->Cell(0,5,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0,0,'L',1,$link[$cat]);
									 $pdf->SetTextColor(0,0,0);
									$pdf->Ln(10);
	   }
	$pdf->SetTextColor(0,0,0);
	

$kol_tovarov=count($this->session->data['arr_topdf']);



 $mas_prod3=array();

for($v=0;$v<$kol_tovarov;$v++){
	
	
$tmp_mas_id=$this->model_module_topdf->getProductCatNames($this->session->data['arr_topdf'][$v]);
	
 $mas_prod3[$this->session->data['arr_topdf'][$v]]= $tmp_mas_id[0]['name'];

}

 asort($mas_prod3);
 $mas_prod3=array_keys ($mas_prod3);
			
		
		
//	$link_content=$pdf->AddLink($pdf->PageNo());
//	$pdf->SetLink($link_content,0 ,1);
	//cортировка по категориям конец	
		
		
			
			
		}
		
		else   {
		 
		$mas_prod3=$this->session->data['arr_topdf'];
		$kol_tovarov=count($mas_prod3);
		 				
		}
	
	
	
	
$kol_na_str=0;
	
	
	
	for($i=0;$i<$kol_tovarov;$i++){
	
	$h1=74;	
	
	$mas_prod=$this->model_module_topdf->getProduct2($mas_prod3[$i]);	
	$mas_prod['category']=$this->model_module_topdf->getProductCatNames($mas_prod3[$i]);		
		

if (isset($this->request->post['add_content'])){

if($cur_cat!=$mas_prod['category'][0]['name']) {
		
		
		 $pdf->AddPage();//echo 'AddPagecat'; 
		
		 $pdf->SetXY($pdf->GetX(),$pdf->GetY()+100);
		 $pdf->SetLink($link[$mas_prod['category'][0]['name']]);
		 $pdf->SetFont('arial','',$r_shrift_zag);
		 $pdf->SetFontSize($r_shrift_zag);
		 $pdf->SetFillColor(76, 179, 86);
		 $pdf->SetTextColor(256,256,256);
		 
		 							list($r, $g, $b) = sscanf($pdf->content_razdelitel_color, "#%02x%02x%02x");
									$pdf->SetTextColor($r, $g, $b);
									list($r, $g, $b) = sscanf($pdf->content_razdelitel_fon, "#%02x%02x%02x");
									$pdf->SetFillColor($r, $g, $b);
		 
		 $str_tmp = html_entity_decode($mas_prod['category'][0]['name'], ENT_QUOTES, 'UTF-8');
		$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
			
		
		 $pdf->MultiCell($w1*3,28,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0, 'C',true);
		 $pdf->Image(DIR_IMAGE."/back_2.png", 0, 0, 10, 10, 'png', $link_content);
		// $pdf->Write(5,mb_convert_encoding($mas_prod['category'][0]['name'], "windows-1251","UTF-8"));
		$cur_cat=$mas_prod['category'][0]['name'];
		$cat_start=1;
		$kol_na_str=0;
	}
	else { $cat_start=0;
		
	}	
	
	
	
	// if ($cat_start==1) {  /*$pdf->AddPage(); $x1=$pdf->GetX(); $y1=$pdf->GetY(); $h1=0; */ $flag_nach_tov=1;}
		/*	foreach($this->session->data['arr_topdf'] as $product_id) {
				
				$mas_prod = $this->model_module_topdf->getProduct2($product_id);
				$mas_prod['category'] = $this->model_module_topdf->getProductCatNames($product_id);*/
	}
	
	else {
		/* $x1=$pdf->GetX(); $y1=$pdf->GetY(); $h1=76; echo '22';echo $kol_na_str;*/
	}		
	
	 if (($kol_na_str % 3) == 0 || $cat_start==1) {  $pdf->AddPage(); $x1=$pdf->GetX(); $y1=$pdf->GetY(); $h1=0;$pdf->istitul_page = false;/*echo 'kol'; */} 
	
		
			if ($flag_nach_tov == 0 || $cat_start==1) {
					$flag_nach_tov = 1;
					//$pdf->AddPage();
					$pdf->istitul_page = false;
//echo '6';
					// $this->footer($pdf,'InputName','InputEmail');

					$pdf->SetFontSize(14);
					$pdf->SetFont('arial');
				}
				else
				if (isset($this->request->post['razryv']) && ($kol_na_str % 3) != 0) {/*echo '7';*/
					$pdf->AddPage();$x1=$pdf->GetX(); $y1=$pdf->GetY(); $h1=0;
					$pdf->istitul_page = false;
					$pdf->SetFontSize(14);
					$pdf->SetFont('arial');
				}

				$this->session->data['progress_value']+= $progress_shag;

				// echo'<br />'. $this->session->data['progress_value'];

$pdf->istitul_page = false;			
			$kol_na_str++;
	/////////////////////////начало вывода1
$x1=$x1;
$y1=$y1+$h1;
$pdf->SetXY($x1,$y1);
$h1=74;

$pdf->SetFont('arial','',$r_shrift_zag);
$pdf->SetFontSize($r_shrift_zag);

//$pdf->AddPage();


//$str_tmp=html_entity_decode($meta_h1['1']['meta_h1'], ENT_QUOTES,'UTF-8');
//$str_tmp=html_entity_decode($str_tmp, ENT_QUOTES,'UTF-8');

$pdf->SetFillColor(241,168,15);
$pdf->SetTextColor(256,256,256);

$str_tmp = html_entity_decode($mas_prod['name'], ENT_QUOTES, 'UTF-8');
$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');

$pdf->MultiCell($w1*3,8,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0, 'L',true);
//$pdf->Cell($w1*3, 8,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"), 0, 1, 'L',true);

$pdf->SetTextColor(0,0,0);

//$pdf->Write(1,mb_convert_encoding('Название товара', "windows-1251","UTF-8"));
$pdf->Ln(1);
$x1=$pdf->GetX();
$y1=$pdf->GetY();
 
$x2=$x1+$w1;
$x3=$x2+$w2;

$pdf->SetFontSize($r_shrift_polya);

////////////////////////////вывод изображения
$image=$mas_prod['image'];

if(file_exists(DIR_IMAGE.$image) && !empty($image) && is_file(DIR_IMAGE.$image)) 	{
	
	list($img_width, $img_height) = getimagesize(DIR_IMAGE.$image);
 
			

if(!isset($img_width))  {
	

	 }
	
		else 	{
			
					
			$ratio_hw=$img_height/$img_width;
    	//	echo $ratio_hw;
		if($ratio_hw>1) {	$hd=$h1; $wd=$hd/$ratio_hw;	}
		else {   $wd=$w1; $hd=$wd*$ratio_hw; }

				$pdf->Image(DIR_IMAGE.$image,10,null,$wd,$hd);
				$pdf->SetXY($x2,$y1);
			
			}

}
else {	

}


////////////////вывод модели
$pdf->SetX($x2);
$str_tmp=html_entity_decode($mas_prod['model'], ENT_QUOTES,'UTF-8');
$str_tmp=html_entity_decode($str_tmp, ENT_QUOTES,'UTF-8');
$pdf->MultiCell($w2,4,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0);


////////////////////////////
$pdf->SetX($x2);

////////////////вывод цены

$pdf->Write(8,mb_convert_encoding('Цена: ', "windows-1251","UTF-8"));

$x1с=$pdf->GetX();
$y1с=$pdf->GetY()+4;
			
$pdf->Write(8,mb_convert_encoding($this->currency->format($mytax->calculate($mas_prod['price'], $mas_prod['tax_class_id'], $this->config->get('config_tax'))), "windows-1251","UTF-8"));
	$x2с=$pdf->GetX()+3;
	$y2с=$pdf->GetY()+4;
		
			
	if ((float)$mas_prod['special']) {
							$pdf->Line($x1с,  $y1с,  $x2с,  $y2с);
							
							$pdf->Write(8,"  ");
							$pdf->Write(8,mb_convert_encoding($this->currency->format($mytax->calculate($mas_prod['special'], $mas_prod['tax_class_id'], $this->config->get('config_tax'))), "windows-1251","UTF-8"));
						} 

///////////////
//$pdf->MultiCell($w2,$h1/4,mb_convert_encoding('Цена: 525р.', "windows-1251","UTF-8"),1);

$pdf->SetX($x2);

//////////////вывод опций
$pdf->Ln();

$data['options'] = array();

foreach(($this->model_module_topdf->getProductOptions($mas_prod3[$i])) as $option) 
			{ //print_r($option);
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($mytax->calculate($option_value['price'], $mas_prod['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

//$str_tmp="";
//$str_tmp.='<br>'.$this->session->data['arr_topdf'][$i]."<br>";
foreach($data['options'] as $option){
	
	//$str_tmp.='<br>';
	$str_tmp="";
	if (!empty($option['product_option_value'])){
    
	   $str_tmp.= $option['name'].': ';
		foreach($option['product_option_value'] as $value) 
		
			{ $str_tmp.= $value['name'].' '; }
	
 } else{ }
	
	if(!empty($str_tmp)){
		
	$pdf->SetX($x2);
	$pdf->MultiCell($w2,4,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0);
	}
	
}
//$pdf->SetX($x2);
	//echo $str_tmp;
//$pdf->MultiCell($w2,$h1/4,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),1);
////////////////////////




///////вывод описания 
$pdf->Ln();
$pdf->SetX($x2);

$str_tmp=html_entity_decode($mas_prod['description'], ENT_QUOTES,'UTF-8');
$str_tmp=html_entity_decode($str_tmp, ENT_QUOTES,'UTF-8');
$str_tmp=trim(strip_tags($str_tmp));
$str_tmp = preg_replace("/[\t]+/",'',$str_tmp);
$str_tmp = preg_replace("/(\r\n)+/i", "\r\n", $str_tmp);


$str_tmp=substr ( $str_tmp , 0 , 350 ).'...';

$pdf->MultiCell($w2,4,mb_convert_encoding('Описание:'.$str_tmp, "windows-1251","UTF-8"),0);

//////////////////////

$y_max=$pdf->GetY();
			
///////////вывод qr-кода 

$tempDir = DIR_UPLOAD."qrcode_image";

if (file_exists($tempDir)) {} else {mkdir($tempDir, 0755);}
		
		$fileName = $mas_prod3[$i].'.png';
		$pngAbsoluteFilePath = $tempDir . '/' .$fileName;
		
	
		if (file_exists($pngAbsoluteFilePath) && !empty($pngAbsoluteFilePath) && is_file($pngAbsoluteFilePath)) {
			
			$pdf->Image($pngAbsoluteFilePath, $x3+($w3-$w3/2)/2,$y1,$w3/2);
					}

		    else {
				
			$codeContents = HTTP_CATALOG . "index.php?route=product/product&product_id=" . $mas_prod3[$i];
			QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L);
			$pdf->Image($pngAbsoluteFilePath, $x3+($w3-$w3/2)/2,$y1,$w3/2);
				
			}


////////////////////////////
$pdf->SetXY($x3+1,$y1+$w3/2);
//$pdf->Write(1,mb_convert_encoding('Ссылка на товар в интернет-магазине:', "windows-1251","UTF-8"));
//$pdf->MultiCell($w3,5,mb_convert_encoding('Ссылка на товар в интернет-магазине:', "windows-1251","UTF-8"),0,'C');

$pdf->SetFillColor(255,255,255);

//$pdf->SetLineWidth($w3);

$pdf->Cell($w3,5,mb_convert_encoding('Ссылка на товар', "windows-1251","UTF-8"), 0, 1, 'C',true, HTTP_CATALOG."index.php?route=product/product&product_id=".$mas_prod3[$i]);
$pdf->SetXY($x3+1,$y1+$w3/2+4);
$pdf->Cell($w3,5,mb_convert_encoding('в интернет-магазине:', "windows-1251","UTF-8"), 0, 1, 'C',true, HTTP_CATALOG."index.php?route=product/product&product_id=".$mas_prod3[$i]);
/////////////////////////конец вывода1 

			
				
			}

			$data['pdf'] = $pdf;

			// echo strlen($pdf->buffer)."-".$pdf->buffer;
			// $pdf->Output(DIR_SYSTEM.'storage/download/ftp_exampledfg.pdf','F');

			if (isset($this->request->post['ftp'])) {

				// $this->error['warning']=$pdf->Output_ftp();

				$mas_result_out = $pdf->Output_ftp();
				$this->error['warning'] = $mas_result_out['error'];
				$this->session->data['success'] = $mas_result_out['success'];
			}

			$pdf->Output(DIR_DOWNLOAD . 'catalog.pdf', 'F'); //D - в файл I- в браузер

			// $this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			//$current .= "---".date("H:i:s");   
			/*
			if (isset($this->request->get['filter_category_id'])) {
				$url.= '&filter_category_id=' . (int)$this->request->get['filter_category_id'];
			}
			*/
			if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

			if (isset($this->request->get['filter_manufacturer_id'])) {
				$url.= '&filter_manufacturer_id=' . (int)$this->request->get['filter_manufacturer_id'];
			}

			if (isset($this->request->get['filter_name'])) {
				$url.= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url.= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url.= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['sort'])) {
				$url.= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url.= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url.= '&page=' . $this->request->get['page'];
			}

			// $this->redirect($this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url, 'SSL'));

		}

		$this->getList();
	}


public function savepdf22(){

		
	//$this->load->library('customer');
//	$this->load->library('tax');

		
	$this->registry->set('customer', new Customer($this->registry));
	$mytax = new Tax($this->registry);
	

		
		$pdf=new FPDF();
			
		$pdf->AddFont('arial','','arial.php');
		$pdf->AddFont('arialbd','','arialbd.php');
		
    	$this->language->load('catalog/product');

    	$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('module/topdf');
		
		

if (isset($this->request->files) && count($this->request->files)!=0){


$pdf->AddPage();
$pdf->SetFontSize(14);
$pdf->SetFont('arial');

$uploadfile = DIR_UPLOAD.basename($this->request->files['picture']['name']);
move_uploaded_file($this->request->files['picture']['tmp_name'], $uploadfile);

	$pdf->Image($uploadfile,0,0,210);
unlink($uploadfile);
	
}
		
		
		
		
		
		
		
					
		if (isset($this->session->data['arr_topdf']) && $this->validateSavepdf()) {
			
	
	
	//////////////////////////////////////////////////
		
		    if (! function_exists('array_column')) {
        function array_column(array $input, $columnKey, $indexKey = null) {
            $array = array();
            foreach ($input as $value) {
                if ( ! isset($value[$columnKey])) {
                    trigger_error("Key \"$columnKey\" does not exist in array");
                    return false;
                }
                if (is_null($indexKey)) {
                    $array[] = $value[$columnKey];
                }
                else {
                    if ( ! isset($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not exist in array");
                        return false;
                    }
                    if ( ! is_scalar($value[$indexKey])) {
                        trigger_error("Key \"$indexKey\" does not contain scalar value");
                        return false;
                    }
                    $array[$value[$indexKey]] = $value[$columnKey];
                }
            }
            return $array;
        }
    }
    
		//////////////////////////////////////////////////
	
	
	
	
	
	
	
	
	
	
	foreach($this->session->data['arr_topdf'] as $id) {
		$contents[]=array_column($this->model_module_topdf->getProductCatNames($id), 'name');
			
	}
	

	foreach($contents as &$value) {  $value=$value[0];    }
	
	$contents=array_unique($contents);
		
	$contents = array_values($contents);
	
	asort($contents);
		
	//print_r($contents3);
		//echo $contents3[0];echo $contents3[1];
		
	$pdf->AddPage();
	$pdf->SetFontSize(14);
$pdf->SetFont('arial');
	

$r_shrift_zag=18;
$r_shrift_polya=10;


$w1=64;
$w2=80;
$w3=48;
$w=$w1+$w2+$w3;
$h1=76;
	//вывод логотипа 
	if(file_exists(DIR_IMAGE.'logo.png') && is_file(DIR_IMAGE.'logo.png')) 	{
	
	list($img_w, $img_h) = getimagesize(DIR_IMAGE.'logo.png');
 
 	if(isset($img_w))  {
		
		$pdf->Image(DIR_IMAGE.'logo.png',10);//вывод логотипа непосредственно  

	 }
	
		else 	{ }
 
 }
			
	
	
		 $pdf->SetFont('arial','',$r_shrift_zag);
		 $pdf->SetFontSize($r_shrift_zag);
		 $pdf->SetFillColor(69, 139, 196);
		 $pdf->SetFillColor( 37, 118, 185);
		 $pdf->SetTextColor(256,256,256);
		 $pdf->MultiCell($w1*3,20,mb_convert_encoding('Cодержание:', "windows-1251","UTF-8"),0, 'C',true);
		 $pdf->Ln(5);
		 $pdf->SetTextColor(0,0,0);
	
	
	$pdf->SetTextColor(29, 101, 159);
	$pdf->SetTextColor(228, 88, 43);
	
	foreach($contents as $cat) { $link[$cat]=$pdf->AddLink();
									$pdf->Write(5,mb_convert_encoding($cat, "windows-1251","UTF-8"),$link[$cat]);
									$pdf->Ln(10);
	   }
	$pdf->SetTextColor(0,0,0);
	

$kol_tovarov=count($this->session->data['arr_topdf']);

$cur_cat="";
$cat_start=0;

 $mas_prod3=array();

for($v=0;$v<$kol_tovarov;$v++){
	
	
$tmp_mas_id=$this->model_module_topdf->getProductCatNames($this->session->data['arr_topdf'][$v]);
	
 $mas_prod3[$this->session->data['arr_topdf'][$v]]= $tmp_mas_id[0]['name'];

}

 asort($mas_prod3);
 $mas_prod3=array_keys ($mas_prod3);
		
$kol_na_str=0;
		
	for($i=0;$i<$kol_tovarov;$i++){
	
	$mas_prod=$this->model_module_topdf->getProduct2($mas_prod3[$i]);	
	$mas_prod['category']=$this->model_module_topdf->getProductCatNames($mas_prod3[$i]);
	
	if($cur_cat!=$mas_prod['category'][0]['name']) {
		
		 $pdf->AddPage();
		 //echo 'добавить страницу для заголовка категории<br>';
		 $pdf->SetXY($pdf->GetX(),$pdf->GetY()+100);
		 $pdf->SetLink($link[$mas_prod['category'][0]['name']]);
		 $pdf->SetFont('arial','',$r_shrift_zag);
		 $pdf->SetFontSize($r_shrift_zag);
		 $pdf->SetFillColor(76, 179, 86);
		 $pdf->SetTextColor(256,256,256);
		 $pdf->MultiCell($w1*3,28,mb_convert_encoding($mas_prod['category'][0]['name'], "windows-1251","UTF-8"),0, 'C',true);
		// $pdf->Write(5,mb_convert_encoding($mas_prod['category'][0]['name'], "windows-1251","UTF-8"));
		$cur_cat=$mas_prod['category'][0]['name'];
		$cat_start=1;
		$kol_na_str=0;
	}
	else { $cat_start=0;
		
	}
	
	//$meta_h1=$this->model_module_topdf->getProductDescriptions2($this->session->data['arr_topdf'][$i]);		
				
	//$pdf->Write(5,mb_convert_encoding($mas_prod['category'][0]['name'], "windows-1251","UTF-8"));
	 if (($kol_na_str % 3) == 0 || $cat_start==1) { /*echo 'добавить страницу<br>';*/ $pdf->AddPage(); $x1=$pdf->GetX(); $y1=$pdf->GetY(); $h1=0; }

/*echo 'товар'.$i.'<br>';	*/
$kol_na_str++;
	/////////////////////////начало вывода1
$x1=$x1;
$y1=$y1+$h1+2;
$pdf->SetXY($x1,$y1);
$h1=76;

$pdf->SetFont('arial','',$r_shrift_zag);
$pdf->SetFontSize($r_shrift_zag);

//$pdf->AddPage();


//$str_tmp=html_entity_decode($meta_h1['1']['meta_h1'], ENT_QUOTES,'UTF-8');
//$str_tmp=html_entity_decode($str_tmp, ENT_QUOTES,'UTF-8');

$pdf->SetFillColor(241,168,15);
$pdf->SetTextColor(256,256,256);

$pdf->MultiCell($w1*3,8,mb_convert_encoding($mas_prod['name'], "windows-1251","UTF-8"),0, 'L',true);
//$pdf->Cell($w1*3, 8,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"), 0, 1, 'L',true);

$pdf->SetTextColor(0,0,0);

//$pdf->Write(1,mb_convert_encoding('Название товара', "windows-1251","UTF-8"));
$pdf->Ln(1);
$x1=$pdf->GetX();
$y1=$pdf->GetY();
 
$x2=$x1+$w1;
$x3=$x2+$w2;

$pdf->SetFontSize($r_shrift_polya);

////////////////////////////вывод изображения
$image=$mas_prod['image'];

if(file_exists(DIR_IMAGE.$image) && !empty($image) && is_file(DIR_IMAGE.$image)) 	{
	
	list($img_width, $img_height) = getimagesize(DIR_IMAGE.$image);
 
			

if(!isset($img_width))  {
	

	 }
	
		else 	{
			
					
			$ratio_hw=$img_height/$img_width;
    	//	echo $ratio_hw;
		if($ratio_hw>1) {	$hd=$h1; $wd=$hd/$ratio_hw;	}
		else {   $wd=$w1; $hd=$wd*$ratio_hw; }

				$pdf->Image(DIR_IMAGE.$image,10,null,$wd,$hd);
				$pdf->SetXY($x2,$y1);
			
			}

}
else {	

}


////////////////вывод модели
$pdf->SetX($x2);
$str_tmp=html_entity_decode($mas_prod['model'], ENT_QUOTES,'UTF-8');
$str_tmp=html_entity_decode($str_tmp, ENT_QUOTES,'UTF-8');
$pdf->MultiCell($w2,4,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0);


////////////////////////////
$pdf->SetX($x2);

////////////////вывод цены

$pdf->Write(8,mb_convert_encoding('Цена: ', "windows-1251","UTF-8"));

$x1с=$pdf->GetX();
$y1с=$pdf->GetY()+4;
			
$pdf->Write(8,mb_convert_encoding($this->currency->format($mytax->calculate($mas_prod['price'], $mas_prod['tax_class_id'], $this->config->get('config_tax'))), "windows-1251","UTF-8"));
	$x2с=$pdf->GetX()+3;
	$y2с=$pdf->GetY()+4;
		
			
	if ((float)$mas_prod['special']) {
							$pdf->Line($x1с,  $y1с,  $x2с,  $y2с);
							
							$pdf->Write(8,"  ");
							$pdf->Write(8,mb_convert_encoding($this->currency->format($mytax->calculate($mas_prod['special'], $mas_prod['tax_class_id'], $this->config->get('config_tax'))), "windows-1251","UTF-8"));
						} 

///////////////
//$pdf->MultiCell($w2,$h1/4,mb_convert_encoding('Цена: 525р.', "windows-1251","UTF-8"),1);

$pdf->SetX($x2);

//////////////вывод опций
$pdf->Ln();

$data['options'] = array();

foreach(($this->model_module_topdf->getProductOptions($mas_prod3[$i])) as $option) 
			{ //print_r($option);
				$product_option_value_data = array();

				foreach ($option['product_option_value'] as $option_value) {
					if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
						if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
							$price = $this->currency->format($mytax->calculate($option_value['price'], $mas_prod['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
						} else {
							$price = false;
						}

						$product_option_value_data[] = array(
							'product_option_value_id' => $option_value['product_option_value_id'],
							'option_value_id'         => $option_value['option_value_id'],
							'name'                    => $option_value['name'],
							'price'                   => $price,
							'price_prefix'            => $option_value['price_prefix']
						);
					}
				}

				$data['options'][] = array(
					'product_option_id'    => $option['product_option_id'],
					'product_option_value' => $product_option_value_data,
					'option_id'            => $option['option_id'],
					'name'                 => $option['name'],
					'type'                 => $option['type'],
					'value'                => $option['value'],
					'required'             => $option['required']
				);
			}

//$str_tmp="";
//$str_tmp.='<br>'.$this->session->data['arr_topdf'][$i]."<br>";
foreach($data['options'] as $option){
	
	//$str_tmp.='<br>';
	$str_tmp="";
	if (!empty($option['product_option_value'])){
    
	   $str_tmp.= $option['name'].': ';
		foreach($option['product_option_value'] as $value) 
		
			{ $str_tmp.= $value['name'].' '; }
	
 } else{ }
	
	if(!empty($str_tmp)){
		
	$pdf->SetX($x2);
	$pdf->MultiCell($w2,4,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),0);
	}
	
}
//$pdf->SetX($x2);
	//echo $str_tmp;
//$pdf->MultiCell($w2,$h1/4,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"),1);
////////////////////////




///////вывод описания 
$pdf->Ln();
$pdf->SetX($x2);

$str_tmp=html_entity_decode($mas_prod['description'], ENT_QUOTES,'UTF-8');
$str_tmp=html_entity_decode($str_tmp, ENT_QUOTES,'UTF-8');
$str_tmp=trim(strip_tags($str_tmp));
$str_tmp = preg_replace("/[\t]+/",'',$str_tmp);
$str_tmp = preg_replace("/(\r\n)+/i", "\r\n", $str_tmp);


$str_tmp=substr ( $str_tmp , 0 , 350 ).'...';

$pdf->MultiCell($w2,4,mb_convert_encoding('Описание:'.$str_tmp, "windows-1251","UTF-8"),0);

//////////////////////

$y_max=$pdf->GetY();
			
///////////вывод qr-кода 




 $tempDir = DIR_UPLOAD; 
     
    $codeContents =  HTTP_CATALOG."index.php?route=product/product&product_id=".$mas_prod3[$i]; 
     
    $fileName = 'file_'.md5($codeContents).'.png'; 
    $pngAbsoluteFilePath = $tempDir.$fileName; 
    
    if (!file_exists($pngAbsoluteFilePath)) { 
        QRcode::png($codeContents, $pngAbsoluteFilePath); 
      
    } else {   } 
    
 if(file_exists($pngAbsoluteFilePath) && !empty($pngAbsoluteFilePath) && is_file($pngAbsoluteFilePath)) 	{   
  
     $pdf->Image($pngAbsoluteFilePath,$x3+($w3-$w3/2)/2,$y1,$w3/2);
 unlink($pngAbsoluteFilePath);
		
		} 
		 
$files = glob(DIR_SYSTEM.'phpqrcode/cache/*'); // get all file names
foreach($files as $file){ // iterate files
  if(is_file($file))
    unlink($file); // delete file
}


////////////////////////////
$pdf->SetXY($x3+1,$y1+$w3/2);
//$pdf->Write(1,mb_convert_encoding('Ссылка на товар в интернет-магазине:', "windows-1251","UTF-8"));
//$pdf->MultiCell($w3,5,mb_convert_encoding('Ссылка на товар в интернет-магазине:', "windows-1251","UTF-8"),0,'C');

$pdf->SetFillColor(255,255,255);

//$pdf->SetLineWidth($w3);

$pdf->Cell($w3,5,mb_convert_encoding('Ссылка на товар', "windows-1251","UTF-8"), 0, 1, 'C',true, HTTP_CATALOG."index.php?route=product/product&product_id=".$mas_prod3[$i]);
$pdf->SetXY($x3+1,$y1+$w3/2+4);
$pdf->Cell($w3,5,mb_convert_encoding('в интернет-магазине:', "windows-1251","UTF-8"), 0, 1, 'C',true, HTTP_CATALOG."index.php?route=product/product&product_id=".$mas_prod3[$i]);
/////////////////////////конец вывода1 

}		
			
			  		  
	  		  
	  		$data['pdf'] = $pdf;
	  		$pdf->Output(DIR_DOWNLOAD . 'catalog.pdf', 'F'); //D - в файл I- в браузер

			$this->session->data['success'] = $this->language->get('text_success');
			
			$url = '';
			
			/*
			if (isset($this->request->get['filter_category_id'])) {
				$url.= '&filter_category_id=' . (int)$this->request->get['filter_category_id'];
			}
			*/
			if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}

			if (isset($this->request->get['filter_manufacturer_id'])) {
				$url.= '&filter_manufacturer_id=' . (int)$this->request->get['filter_manufacturer_id'];
			}

			if (isset($this->request->get['filter_name'])) {
				$url.= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url.= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url.= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['sort'])) {
				$url.= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url.= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url.= '&page=' . $this->request->get['page'];
			}

			// $this->redirect($this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url, 'SSL'));

		}

		
		$this->getList();
  	}

	


	public function pdf_ftp()
	{
		
		$this->load->model('setting/setting');
		$mas=array('tpdf_asd'=> '11111111111','tpdf_zxc'=> '222222222222222');

	  
			
		$flag_zapolneno = true;
		$this->session->data['warning'] = '';
		if (isset($this->request->post['FTPServer1']) && !empty($this->request->post['FTPServer1'])) {
			$ftp_server = $this->request->post['FTPServer1'];
		}
		else {
			$this->session->data['warning'].= "Не указан FTP-cервер!";
			$flag_zapolneno = false;
		}

		if (isset($this->request->post['FTPUser1']) && !empty($this->request->post['FTPUser1'])) {
			$ftp_user_name = $this->request->post['FTPUser1'];
		}
		else {
			$this->session->data['warning'].= "Не указан FTP-пользователь!";
			$flag_zapolneno = false;
		}

		if (isset($this->request->post['FTPPassword1']) && !empty($this->request->post['FTPPassword1'])) {
			$ftp_user_pass = $this->request->post['FTPPassword1'];
		}
		else {
			$this->session->data['warning'].= "Не указан пароль подключения к FTP!";
			$flag_zapolneno = false;
		}

  $this->model_setting_setting->editSetting("topdf", array('topdf_ftp_server'=>$ftp_server,'topdf_ftp_user_name'=>$ftp_user_name,'topdf_ftp_user_pass'=>$ftp_user_pass));		

		if ($flag_zapolneno == true) {
			
			
			
			$filename = 'catalog.pdf';
			$file = DIR_DOWNLOAD . $filename;
			$remote_file = $filename;

			// connecting

			$conn_id = @ftp_connect($ftp_server);
			if ($conn_id == FALSE) $this->session->data['warning'].= "Нет соединения с FTP cервером! Проверьте правильность заполнения имени FTP сервера.\n";
			else {

				// auth

				$login_result = @ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
				if ($login_result == FALSE) $this->session->data['warning'].= "Ошибка авторизации FTP! Неправильно указаны имя пользователя и/или пароль FTP аккаунта! Проверьте правильность заполнения указанных полей.\n";
				else {
					@ftp_pasv($conn_id, true);

					// put file

					if (@ftp_put($conn_id, $remote_file, $file, FTP_BINARY)) $this->session->data['success'] = "PDF каталог успешно загружен на FTP-cервер!\n";
					else $this->session->data['warning'].= "PDF каталог не загружен на FTP-cервер! Попробуйте еще раз!\n";
				}
			}

			// @unlink($file);

			@ftp_close($conn_id);
		}

			$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) { 
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}
		
				
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		// echo $url;

		$this->response->redirect($this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url, 'SSL'));
	}

	public function pdf_download()
	{
		$file = DIR_DOWNLOAD . 'catalog.pdf';
		if (file_exists($file)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="' . basename($file) . '"');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
			readfile($file);
			exit;
		}
	}
	
	public function pdf_delete()
	{
		
		@unlink(DIR_DOWNLOAD . 'catalog.pdf');
		
			$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) { 
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}
		
				
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->response->redirect($this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		
		
	}

	public function add()
	{
		$this->language->load('module/topdf');
		$this->document->setTitle($this->language->get('heading_title'));
		
		
		
	/*	echo '<pre>';
print_r($_POST['selected']);
echo '</pre>';*/

		if (!isset($this->session->data['arr_topdf'])) $this->session->data['arr_topdf'] = array();
		if (isset($this->request->post['forma_add_selected']) && $this->validateAdd()) {
			 
			$tmp = explode(",", $this->request->post['forma_add_selected']);
			
			foreach($tmp as $value) {
				array_push($this->session->data['arr_topdf'], $value);
			}

			$data['arr_topdf'] = $this->session->data['arr_topdf'];
			
			$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) { 
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}
		
				
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			
		}
		
	
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
		
		
		
		
/*
		$data['sort_name'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$data['sort_model'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
		$data['sort_category'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=cd.name' . $url, 'SSL');
		
		$data['sort_price'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
		$data['sort_quantity'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, 'SSL');
		$data['sort_status'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		$data['sort_order'] = $this->url->link('module/topdf', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');
*/
			
			$this->response->redirect($this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function clear()
	{
		$this->session->data['arr_topdf'] = null;
		
			$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		
		if (isset($this->request->get['filter_category'])) { 
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}
		
				
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$this->response->redirect($this->url->link('module/topdf', 'token=' . $this->session->data['token'] . $url, 'SSL'));
	}


public function get_option_field(&$pdf)
{
	
	  $str=$this->config->get('topdf_field_name_f_out');
	  $pdf->name_f_out = (isset($str)&& !empty($str)) ? $str : 'option1'; 
	  $str=$this->config->get('topdf_field_name_text_field');
	  $pdf->name_text_field = (isset($str)&& !empty($str)) ? $str : 'Наименование';
	  $str=$this->config->get('topdf_field_name_text_color');
	  $pdf->name_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_name_znach_color');
	  $pdf->name_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  
	  
	  
	  $str=$this->config->get('topdf_field_model_f_out');
	  $pdf->model_f_out = (isset($str)&& !empty($str)) ? $str : 'option1';
	  $str=$this->config->get('topdf_field_model_text_field');
	  $pdf->model_text_field = (isset($str)&& !empty($str)) ? $str : 'Модель';
	  $str=$this->config->get('topdf_field_model_text_color');
	  $pdf->model_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_model_znach_color');
	  $pdf->model_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  
	  
	  $str=$this->config->get('topdf_field_category_f_out');
	  $pdf->category_f_out = (isset($str)&& !empty($str)) ? $str : 'option1';
	  $str=$this->config->get('topdf_field_category_text_field');
	  $pdf->category_text_field = (isset($str)&& !empty($str)) ? $str : 'Категория';
	  $str=$this->config->get('topdf_field_category_text_color');
	  $pdf->category_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_category_znach_color');
	  $pdf->category_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  
	  
	  $str=$this->config->get('topdf_field_manufacture_f_out');
	  $pdf->manufacture_f_out = (isset($str)&& !empty($str)) ? $str : 'option1';
	  $str=$this->config->get('topdf_field_manufacture_text_field');
	  $pdf->manufacture_text_field = (isset($str)&& !empty($str)) ? $str : 'Производитель';
	  $str=$this->config->get('topdf_field_manufacture_text_color');
	  $pdf->manufacture_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_manufacture_znach_color');
	  $pdf->manufacture_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  
	  
	  $str=$this->config->get('topdf_field_price_f_out');
	  $pdf->price_f_out = (isset($str)&& !empty($str)) ? $str : 'option1';
	  $str=$this->config->get('topdf_field_price_text_field');
	  $pdf->price_text_field = (isset($str)&& !empty($str)) ? $str : 'Цена';
	  $str=$this->config->get('topdf_field_price_text_color');
	  $pdf->price_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_price_znach_color');
	  $pdf->price_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  
	  
	  $str=$this->config->get('topdf_field_price_old_znach_color');
	  $pdf->price_old_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_price_discount_text_color');
	  $pdf->price_discount_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_price_discount_znach_color');
	  $pdf->price_discount_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  
	   
	  $str=$this->config->get('topdf_field_price_discount_f_out');
	  $pdf->price_discount_f_out = (isset($str)&& !empty($str)) ? $str : 'option1';
	  $str=$this->config->get('topdf_field_price_discount_text_field');
	  $pdf->price_discount_text_field = (isset($str)&& !empty($str)) ? $str : 'Дисконт';
	  
	  $str=$this->config->get('topdf_field_price_old_out');
	  $pdf->price_old_out = (isset($str)&& !empty($str)) ? $str : 'false';
	  $str=$this->config->get('topdf_field_price_discount_out');
	  $pdf->price_discount_out = (isset($str)&& !empty($str)) ? $str : 'false';
	  
	  
	  
	  $str=$this->config->get('topdf_field_description_f_out');
	  $pdf->description_f_out = (isset($str)&& !empty($str)) ? $str : 'option1';
	  $str=$this->config->get('topdf_field_description_text_field');
	  $pdf->description_text_field = (isset($str)&& !empty($str)) ? $str : 'Описание';
	  $str=$this->config->get('topdf_field_description_text_color');
	  $pdf->description_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_description_znach_color');
	  $pdf->description_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  
	  
	  $str=$this->config->get('topdf_field_url_f_out');
	  $pdf->url_f_out = (isset($str)&& !empty($str)) ? $str : 'option1';
	  $str=$this->config->get('topdf_field_url_text_field');
	  $pdf->url_text_field = (isset($str)&& !empty($str)) ? $str : 'Ссылка на товар';
	  $str=$this->config->get('topdf_field_url_text_color');
	  $pdf->url_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_field_url_znach_color');
	  $pdf->url_znach_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  
	  
	  
	  /*
	  
	  
	  $pdf->model_f_out = $this->config->get('topdf_field_model_f_out');
	  
	  $pdf->model_text_field = $this->config->get('topdf_field_model_text_field');
	  $pdf->model_text_color = $this->config->get('topdf_field_model_text_color');
	  $pdf->model_znach_color = $this->config->get('topdf_field_model_znach_color');
	  
	  
	   $pdf->category_f_out = $this->config->get('topdf_field_category_f_out');
	   
	  $pdf->category_text_field = $this->config->get('topdf_field_category_text_field');
	  $pdf->category_text_color = $this->config->get('topdf_field_category_text_color');
	  $pdf->category_znach_color = $this->config->get('topdf_field_category_znach_color');
	  
	  
	  
	   $pdf->manufacture_f_out = $this->config->get('topdf_field_manufacture_f_out');
	   
	  $pdf->manufacture_text_field = $this->config->get('topdf_field_manufacture_text_field');
	  $pdf->manufacture_text_color = $this->config->get('topdf_field_manufacture_text_color');
	  $pdf->manufacture_znach_color = $this->config->get('topdf_field_manufacture_znach_color');
	  
	  
	  
	   $pdf->price_f_out = $this->config->get('topdf_field_price_f_out');
	   
	  $pdf->price_text_field = $this->config->get('topdf_field_price_text_field');
	  $pdf->price_text_color = $this->config->get('topdf_field_price_text_color');
	  $pdf->price_znach_color = $this->config->get('topdf_field_price_znach_color');
	  
	  
	  
	   $pdf->description_f_out = $this->config->get('topdf_field_description_f_out');
	   
	  $pdf->description_text_field = $this->config->get('topdf_field_description_text_field');
	  $pdf->description_text_color = $this->config->get('topdf_field_description_text_color');
	  $pdf->description_znach_color = $this->config->get('topdf_field_description_znach_color');
	  
	  
	   $pdf->url_f_out = $this->config->get('topdf_field_url_f_out');
	   
	   
	  $pdf->url_text_field = $this->config->get('topdf_field_url_text_field');
	  $pdf->url_text_color = $this->config->get('topdf_field_url_text_color');
	  $pdf->url_znach_color = $this->config->get('topdf_field_url_znach_color');
			*/
	
}

public function get_option_content(&$pdf)
{
		  
	  $str=$this->config->get('topdf_content_text_field');
	  $pdf->content_text_field = (isset($str)&& !empty($str)) ? $str : 'Содержание';
	  $str=$this->config->get('topdf_content_text_color');
	  $pdf->content_text_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_content_fon');
	  $pdf->content_fon = (isset($str)&& !empty($str)) ? $str : '#ffffff';
	  
	  $str=$this->config->get('topdf_content_element_color');
	  $pdf->content_element_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_content_element_fon');
	  $pdf->content_element_fon = (isset($str)&& !empty($str)) ? $str : '#ffffff';
	  
	  $str=$this->config->get('topdf_content_razdelitel_color');
	  $pdf->content_razdelitel_color = (isset($str)&& !empty($str)) ? $str : '#000000';
	  $str=$this->config->get('topdf_content_razdelitel_fon');
	  $pdf->content_razdelitel_fon = (isset($str)&& !empty($str)) ? $str : '#ffffff';
	  
	 
}
	
public function save_option_field()
{
		
		
		$field=json_decode((html_entity_decode($this->request->post['save_data'], ENT_QUOTES, 'UTF-8')));
		$this->load->model('setting/setting');
	
		$this->model_setting_setting->editSetting("topdf_field", 
		array(
			  
			  'topdf_field_name_f_out'=>$field->name_f_out,
			  'topdf_field_name_text_field'=>$field->name_text_field,
			  'topdf_field_name_text_color'=>$field->name_text_color,
			  'topdf_field_name_znach_color'=>$field->name_znach_color,
			  
			  'topdf_field_model_f_out'=>$field->model_f_out,
			  'topdf_field_model_text_field'=>$field->model_text_field,
			  'topdf_field_model_text_color'=>$field->model_text_color,
			  'topdf_field_model_znach_color'=>$field->model_znach_color,
			  
			  'topdf_field_category_f_out'=>$field->category_f_out,
			  'topdf_field_category_text_field'=>$field->category_text_field,
			  'topdf_field_category_text_color'=>$field->category_text_color,
			  'topdf_field_category_znach_color'=>$field->category_znach_color,
			  
			  'topdf_field_manufacture_f_out'=>$field->manufacture_f_out,
			  'topdf_field_manufacture_text_field'=>$field->manufacture_text_field,
			  'topdf_field_manufacture_text_color'=>$field->manufacture_text_color,
			  'topdf_field_manufacture_znach_color'=>$field->manufacture_znach_color,
			  
			  'topdf_field_price_f_out'=>$field->price_f_out,
			  'topdf_field_price_text_field'=>$field->price_text_field,
			  'topdf_field_price_text_color'=>$field->price_text_color,
			  'topdf_field_price_znach_color'=>$field->price_znach_color,
			  
			 
			  
			   'topdf_field_price_old_out'=>(isset($field->price_old_out)&& !empty($field->price_old_out)) ? $field->price_old_out : 'false',
			   'topdf_field_price_discount_out'=>(isset($field->price_discount_out)&& !empty($field->price_discount_out)) ? $field->price_discount_out : 'false',
			   'topdf_field_price_old_znach_color'=>$field->price_old_znach_color,
			   'topdf_field_price_discount_f_out'=>$field->price_discount_f_out,
			   'topdf_field_price_discount_text_field'=>$field->price_discount_text_field,
			   'topdf_field_price_discount_text_color'=>$field->price_discount_text_color,
			   'topdf_field_price_discount_znach_color'=>$field->price_discount_znach_color,
	  
			  
			  
			   'topdf_field_description_f_out'=>$field->description_f_out,
			  'topdf_field_description_text_field'=>$field->description_text_field,
			  'topdf_field_description_text_color'=>$field->description_text_color,
			  'topdf_field_description_znach_color'=>$field->description_znach_color,
			  
			   'topdf_field_url_f_out'=>$field->url_f_out,
			  'topdf_field_url_text_field'=>$field->url_text_field,
			  'topdf_field_url_text_color'=>$field->url_text_color,
			  'topdf_field_url_znach_color'=>$field->url_znach_color
			  
			  )
			  );
	//var_dump($field);
	echo 'ok';
	
}



public function save_option_content()
{
		
		
		$field=json_decode((html_entity_decode($this->request->post['save_data'], ENT_QUOTES, 'UTF-8')));
		
		
		$this->load->model('setting/setting');
			
		
		$this->model_setting_setting->editSetting("topdf_content", 
		array(
			  
			  'topdf_content_text_field'=>$field->content_text_field,
			  'topdf_content_text_color'=>$field->content_text_color,
			  'topdf_content_fon'=>$field->content_fon,
			  'topdf_content_element_color'=>$field->content_element_color,
			  'topdf_content_element_fon'=>$field->content_element_fon,
			  'topdf_content_razdelitel_color'=>$field->content_razdelitel_color,
			  'topdf_content_razdelitel_fon'=>$field->content_razdelitel_fon,
								  
			  )
			  );
	//var_dump($field);
	echo 'ok';
	
}


public function save_option_footer()
{
		
		
		$field=json_decode((html_entity_decode($this->request->post['save_data'], ENT_QUOTES, 'UTF-8')));
		
		$this->load->model('setting/setting');
			
		
		$this->model_setting_setting->editSetting("topdf_footer", 
		array(
			  
			  'topdf_footer_InputName'=>$field->NameDlg,
			  'topdf_footer_InputEmail'=>$field->EmailDlg,
			   'topdf_footer_InputPhone'=>$field->PhoneDlg,
			   'topdf_footer_InputNameColor'=>$field->NameColorDlg,
			   'topdf_footer_InputEmailColor'=>$field->EmailColorDlg,
			   'topdf_footer_InputPhoneColor'=>$field->PhoneColorDlg
			 
			  )
			  );
	//var_dump($field);
	echo 'ok';
	
}


public function set_option_field(&$pdf,$post)
	{
		
		$field=json_decode(urldecode($post));
		
		
		
		
		$pdf->name_f_out=$field->name_f_out;
		$pdf->name_text_field=$field->name_text_field;
		$pdf->name_text_color=$field->name_text_color;
		$pdf->name_znach_color=$field->name_znach_color;
		
		$pdf->model_f_out=$field->model_f_out;
		$pdf->model_text_field=$field->model_text_field;
		$pdf->model_text_color=$field->model_text_color;
		$pdf->model_znach_color=$field->model_znach_color;
		
		$pdf->category_f_out=$field->category_f_out;
		$pdf->category_text_field=$field->category_text_field;
		$pdf->category_text_color=$field->category_text_color;
		$pdf->category_znach_color=$field->category_znach_color;
		
		$pdf->manufacture_f_out=$field->manufacture_f_out;
		$pdf->manufacture_text_field=$field->manufacture_text_field;
		$pdf->manufacture_text_color=$field->manufacture_text_color;
		$pdf->manufacture_znach_color=$field->manufacture_znach_color;
		
		$pdf->price_f_out=$field->price_f_out;
		$pdf->price_text_field=$field->price_text_field;
		$pdf->price_text_color=$field->price_text_color;
		$pdf->price_znach_color=$field->price_znach_color;
		
		$pdf->price_old_out=(isset($field->price_old_out)&& !empty($field->price_old_out)) ? $field->price_old_out : 'false';
		$pdf->price_discount_out=(isset($field->price_discount_out)&& !empty($field->price_discount_out)) ? $field->price_discount_out : 'false';
		$pdf->price_old_znach_color=$field->price_old_znach_color;
		$pdf->price_discount_f_out=$field->price_discount_f_out;
		$pdf->price_discount_text_field=$field->price_discount_text_field;
		$pdf->price_discount_text_color=$field->price_discount_text_color;
		$pdf->price_discount_znach_color=$field->price_discount_znach_color;
		
		$pdf->description_f_out=$field->description_f_out;
		$pdf->description_text_field=$field->description_text_field;
		$pdf->description_text_color=$field->description_text_color;
		$pdf->description_znach_color=$field->description_znach_color;
		
		$pdf->url_f_out=$field->url_f_out;
		$pdf->url_text_field=$field->url_text_field;
		$pdf->url_text_color=$field->url_text_color;
		$pdf->url_znach_color=$field->url_znach_color;
		
		
		
					
		//echo $pdf->name_text_field;
		//echo $pdf->name_text_color;
		//echo $pdf->name_znach_color;
		
	}

public function set_option_content(&$pdf,$post)
	{
		
		$field=json_decode(urldecode($post));
		
		
		$pdf->content_text_field=$field->content_text_field;
		$pdf->content_text_color=$field->content_text_color;
		$pdf->content_fon=$field->content_fon;
		$pdf->content_element_color=$field->content_element_color;
		$pdf->content_element_fon=$field->content_element_fon;
		$pdf->content_razdelitel_color=$field->content_razdelitel_color;
		$pdf->content_razdelitel_fon=$field->content_razdelitel_fon;
		
	
			
	}

	protected
	function func_name(&$pdf, $name)
	{
		$this->language->load('module/topdf');
		
		
		
		
		if(strcmp($pdf->name_f_out, "option1")==0){
			
			
		$pdf->SetFont('arialbd');	
		list($r, $g, $b) = sscanf($pdf->name_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		
		$pdf->Write(10, mb_convert_encoding($pdf->name_text_field.': ' , "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
			
		}
		
		
		$pdf->SetFont('arial');
		list($r, $g, $b) = sscanf($pdf->name_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		$str_tmp = html_entity_decode($name, ENT_QUOTES, 'UTF-8');
		$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
		$pdf->Write(10, mb_convert_encoding($str_tmp, "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
		
		$pdf->Ln(10);
		
	}

	protected function func_model(&$pdf, $model)
	{
		$this->language->load('module/topdf');
		
		
		if(strcmp($pdf->model_f_out, "option1")==0){
			
		$pdf->SetFont('arialbd');
		list($r, $g, $b) = sscanf($pdf->model_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		
		$pdf->Write(10, mb_convert_encoding($pdf->model_text_field.': ' , "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
			
			
			}
			
			
		
		
		$pdf->SetFont('arial');
		list($r, $g, $b) = sscanf($pdf->model_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		$str_tmp = html_entity_decode($model, ENT_QUOTES, 'UTF-8');
		$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
		$pdf->Write(10, mb_convert_encoding($str_tmp, "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
		$pdf->Ln(10);
	}

	protected function func_category(&$pdf, $category)
	{
		$this->language->load('module/topdf');
		
		
		if(strcmp($pdf->category_f_out, "option1")==0){
			
		$pdf->SetFont('arialbd');
		list($r, $g, $b) = sscanf($pdf->category_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		
		$pdf->Write(10, mb_convert_encoding($pdf->category_text_field.': ' , "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
		
		}
		
		$str = "";
		$pdf->SetFont('arial');
		list($r, $g, $b) = sscanf($pdf->category_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		
		foreach($category as $value) {
			$str.= mb_convert_encoding($value['name'], "windows-1251", "UTF-8") . ',';
		}

		$str = substr($str, 0, -1);
		$str_tmp = html_entity_decode($str, ENT_QUOTES, 'UTF-8');
		$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
		$pdf->Write(10, $str_tmp);
		$pdf->SetTextColor(0,0,0);
		
		
		$pdf->Ln(10);

		// $pdf->Write(10,mb_convert_encoding($str_tmp, "windows-1251","UTF-8"));

	}

	protected function func_manufacturer(&$pdf, $manufacturer)
	{
		$this->language->load('module/topdf');
		
		
		if(strcmp($pdf->manufacture_f_out, "option1")==0){
			
		$pdf->SetFont('arialbd');
		list($r, $g, $b) = sscanf($pdf->manufacture_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		
		$pdf->Write(10, mb_convert_encoding($pdf->manufacture_text_field.': ' , "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
		
		}
				
		$pdf->SetFont('arial');
		list($r, $g, $b) = sscanf($pdf->manufacture_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		$str_tmp = html_entity_decode($manufacturer, ENT_QUOTES, 'UTF-8');
		$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
		$pdf->Write(10, mb_convert_encoding($str_tmp, "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
		
		$pdf->Ln(10);
		
	}

	protected function func_url(&$pdf, $product_id)
	{
		$this->language->load('module/topdf');
		
		
		if(strcmp($pdf->url_f_out, "option1")==0){
		
		$pdf->SetFont('arialbd');
		list($r, $g, $b) = sscanf($pdf->url_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);	
		
		$pdf->Write(10, mb_convert_encoding($pdf->url_text_field.': ' , "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
		
		}
		
		$pdf->SetFont('arial');	
		list($r, $g, $b) = sscanf($pdf->url_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);			
		
		$pdf->Write(10, mb_convert_encoding($this->language->get('pdf_url') , "windows-1251", "UTF-8") , HTTP_CATALOG . "index.php?route=product/product&product_id=" . $product_id);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Ln(10);
	}

	protected function func_qrcode2(&$pdf, $product_id)
	{
		
		
		
		$tempDir = DIR_UPLOAD;
		$codeContents = HTTP_CATALOG . "index.php?route=product/product&product_id=" . $product_id;
		$fileName = 'file_' . md5($codeContents) . '.png';
		$pngAbsoluteFilePath = $tempDir . $fileName;
		if (!file_exists($pngAbsoluteFilePath)) {
			QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L);
		}
		else {
		}

		if (file_exists($pngAbsoluteFilePath) && !empty($pngAbsoluteFilePath) && is_file($pngAbsoluteFilePath)) {
			$pdf->Image($pngAbsoluteFilePath, $pdf->GetX() + 5);
			unlink($pngAbsoluteFilePath);
		}

		/*$files = glob(DIR_SYSTEM . 'phpqrcode/cache/*'); // get all file names
		foreach($files as $file) { // iterate files
			if (is_file($file)) unlink($file); // delete file
		}*/

		//	$pdf->SetTextColor(0,0,0);

	}
	

	protected function func_qrcode(&$pdf, $product_id)
	{
		
		
		
		$tempDir = DIR_UPLOAD."qrcode_image";
		
		if (file_exists($tempDir)) {} else {mkdir($tempDir, 0755);}
		
		$fileName = $product_id.'.png';
		$pngAbsoluteFilePath = $tempDir . '/' .$fileName;
		
	
		if (file_exists($pngAbsoluteFilePath) && !empty($pngAbsoluteFilePath) && is_file($pngAbsoluteFilePath)) {
			
			$pdf->Image($pngAbsoluteFilePath, $pdf->GetX() + 5);
					}

		    else {
				
			$codeContents = HTTP_CATALOG . "index.php?route=product/product&product_id=" . $product_id;
			QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L);
			$pdf->Image($pngAbsoluteFilePath, $pdf->GetX() + 5);
				
			}
	//$pdf->Ln();
	}
	
	
	public function qrgen()
	{
		
	
	
	$tempDir = DIR_UPLOAD."qrcode_image";

			if (file_exists($tempDir)) {} else {mkdir($tempDir, 0755);}
	
	
	if (file_exists($tempDir . '/' .$this->request->post['save_data']. '.png')) {  } 
	else {
			
			
			$codeContents = HTTP_CATALOG . "index.php?route=product/product&product_id=" . $this->request->post['save_data'];
			$fileName = $this->request->post['save_data']. '.png';
			$pngAbsoluteFilePath = $tempDir . '/' .$fileName;
			if (!file_exists($pngAbsoluteFilePath)) { QRcode::png($codeContents, $pngAbsoluteFilePath, QR_ECLEVEL_L);}
			else {}

				
			
			}
	
	
	
	
		
		
	}
	
	
	protected function func_image(&$pdf, $image, $product_id)
	{
		$iter = 0;
		$wd = 40;$hd=40;
		$perenos=40;
		
		$x = $pdf->GetX();
		$y = $pdf->GetY();

		

		// echo '<br />---'.DIR_IMAGE.'|'.$image.'|';

		if (file_exists(DIR_IMAGE . $image) && !empty($image) && is_file(DIR_IMAGE . $image)) {
			list($img_width, $img_height) = getimagesize(DIR_IMAGE . $image);
			if (!isset($img_width)) {

				// $pdf->Write(10,"no image read");
				// $pdf->Ln(10);
				// $y=$pdf->GetY();

			}
			else {
				
				
				
				
								
				
				
				
				$ratio_hw = $img_height / $img_width;
				$hd = $wd * $ratio_hw;
				
				
				if (($y + $hd) > 287) {
					$pdf->AddPage();
					$y = $pdf->GetY();
					$pdf->istitul_page = false;
				
				}
				else {

					//	$pdf->Ln($hd);

					$y = $pdf->GetY();
				
				}	
				
				
				//echo '<br />'.$y.'-'.$image;
				$pdf->Image(DIR_IMAGE . $image, null, null, $wd, $hd);
				$y = $pdf->GetY()-$hd;
				$iter++;
			}
		}
		else { //$pdf->Write(10,"no image file");

			// $pdf->Ln(10);
			// $y=$pdf->GetY();

		}

		//	$pdf->Write(10,"\r\n");

$hd=40;
		$results = $this->model_module_topdf->getProductImages($product_id);
		foreach($results as $result) {
			if ($iter > 3) {
				if (($y + 2*$hd) > 287) {
					$pdf->AddPage();
					$y = $pdf->GetY();
					$pdf->istitul_page = false;
					$iter = 0;
				}
				else {

					//	$pdf->Ln($hd);

					$y = $pdf->GetY();
					$iter = 0;
				}
			}

			if (file_exists(DIR_IMAGE . $result['image']) && !empty($result['image']) && is_file(DIR_IMAGE . $result['image'])) {
				list($img_width, $img_height) = getimagesize(DIR_IMAGE . $result['image']);
				if (!isset($img_width)) {

					// $pdf->Write(10,"no image read");
					// $pdf->Ln(10);
					// $y=$pdf->GetY();

				}
				else {
					$ratio_hw = $img_height / $img_width;
					$hd = $wd * $ratio_hw;
					if($hd>$perenos) $perenos=$hd;
					//echo '<br />'.$y.'-'.$image;
					$pdf->Image(DIR_IMAGE . $result['image'], $x + $iter * $wd, $y, $wd, $hd);
					$iter++;
				}
			}
			else {

				// $pdf->Write(10,"no image file");
				// $pdf->Ln(10);
				// $y=$pdf->GetY();

			}
		}

		$pdf->SetY($y);
		$pdf->Ln($perenos);
	}

	protected function func_price(&$pdf, $price, $product_id, $special, $tax_class_id, &$mytax)
	{
		$this->language->load('module/topdf');
		
		
		if(strcmp($pdf->price_f_out, "option1")==0){
			
		$pdf->SetFont('arialbd');
		list($r, $g, $b) = sscanf($pdf->price_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);	
		
		$pdf->Write(10, mb_convert_encoding($pdf->price_text_field.': ', "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0, 0, 0);
		
		}
		
	
		
	if(strcmp($pdf->price_old_out, "on")==0){
			
		$x1 = $pdf->GetX();
		$y1 = $pdf->GetY() + 5;		
		$pdf->SetFont('arial');
		
		if((float)$special) {
		list($r, $g, $b) = sscanf($pdf->price_old_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);	}
		
		else { list($r, $g, $b) = sscanf($pdf->price_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);	
			
		}
		$pdf->Write(10, mb_convert_encoding($this->currency->format($mytax->calculate($price, $tax_class_id, $this->config->get('config_tax'))) , "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0, 0, 0);
		$x2 = $pdf->GetX() + 3;
		$y2 = $pdf->GetY() + 5;
		
		if ((float)$special) {
			$pdf->Line($x1, $y1, $x2, $y2);
			$pdf->Write(10, "  ");
			list($r, $g, $b) = sscanf($pdf->price_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);	
			$pdf->Write(10, mb_convert_encoding($this->currency->format($mytax->calculate($special, $tax_class_id, $this->config->get('config_tax'))) , "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0, 0, 0);
		
		}
		
	}	
	
	else {  $pdf->SetFont('arial');
	list($r, $g, $b) = sscanf($pdf->price_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);	
		$pdf->Write(10, mb_convert_encoding($this->currency->format($mytax->calculate($price, $tax_class_id, $this->config->get('config_tax'))) , "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0, 0, 0);
		
		
	}
		
		
		

if(strcmp($pdf->price_discount_out, "on")==0){

$pdf->Ln(10);
		$discounts = $this->model_module_topdf->getProductDiscounts($product_id);
		
		
		if(strcmp($pdf->price_discount_f_out, "option1")==0){
			
		$pdf->SetFont('arialbd');
		list($r, $g, $b) = sscanf($pdf->price_discount_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);	
		
		$pdf->Write(10, mb_convert_encoding($pdf->price_discount_text_field.': ', "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0, 0, 0);
		
		}
		
		$x = $pdf->GetX();
		$pdf->SetFont('arial');
		
		list($r, $g, $b) = sscanf($pdf->price_discount_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);	
		
		foreach($discounts as $discount) {
			$pdf->SetX($x + 5);
			$pdf->Write(10, mb_convert_encoding($this->currency->format($mytax->calculate($discount['price'], $tax_class_id, $this->config->get('config_tax'))) , "windows-1251", "UTF-8"));
			$pdf->Write(10, mb_convert_encoding($this->language->get('pri') . $discount['quantity'] . $this->language->get('sht') , "windows-1251", "UTF-8"));
			$pdf->Ln(10);
		}
		if(count($discounts)>0) $pdf->SetY($pdf->GetY()-10);
		$pdf->SetTextColor(0, 0, 0);
		
		}
	$pdf->Ln(10);
		
	}

	protected function func_description(&$pdf, $description)
	{
		$this->language->load('module/topdf');
		$str_tmp = html_entity_decode($description, ENT_QUOTES, 'UTF-8');
		$str_tmp = html_entity_decode($str_tmp, ENT_QUOTES, 'UTF-8');
		$str_tmp = trim(strip_tags($str_tmp));
		$str_tmp = preg_replace("/[\t]+/", '', $str_tmp);
		$str_tmp = preg_replace("/(\r\n)+/i", "\r\n", $str_tmp);

		// $str_tmp = preg_replace("/[\t\r\n]+/",' ',$str_tmp);
		// $str_tmp = preg_replace("/\n{2,}/",'\n',$str_tmp);

		
		
		if(strcmp($pdf->description_f_out, "option1")==0){
			
		$pdf->SetFont('arialbd');
		list($r, $g, $b) = sscanf($pdf->description_text_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		
		$pdf->Write(10, mb_convert_encoding($pdf->description_text_field.': ', "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
		
		}
					
		$pdf->SetFont('arial');
		list($r, $g, $b) = sscanf($pdf->description_znach_color, "#%02x%02x%02x");
		$pdf->SetTextColor($r, $g, $b);
		
		$pdf->Write(10, mb_convert_encoding($str_tmp, "windows-1251", "UTF-8"));
		$pdf->SetTextColor(0,0,0);
		$pdf->Ln(10);
		
	}

	protected function validateSavepdf()
	{
		if (!$this->user->hasPermission('modify', 'module/topdf')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		}
		else {
			return false;
		}
	}

	protected function validateAdd()
	{
		if (!$this->user->hasPermission('modify', 'module/topdf')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		}
		else {
			return false;
		}
	}

	protected function validate()
	{
		if (!$this->user->hasPermission('modify', 'module/topdf')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		}
		else {
			return false;
		}
	}

	public function progressing()
	{
		if (isset($this->session->data['progress_value'])) echo 100 * ($this->session->data['progress_value']);
		else echo null;
	}

	public function autocomplete()
	{
		$json = array();
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
			$this->load->model('catalog/product');
			$this->load->model('catalog/option');
			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			}
			else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			}
			else {
				$filter_model = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			}
			else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name' => $filter_name,
				'filter_model' => $filter_model,
				'start' => 0,
				'limit' => $limit
			);
			$results = $this->model_catalog_product->getProducts($filter_data);
			foreach($results as $result) {
				$option_data = array();
				$product_options = $this->model_catalog_product->getProductOptions($result['product_id']);
				foreach($product_options as $product_option) {
					$option_info = $this->model_catalog_option->getOption($product_option['option_id']);
					if ($option_info) {
						$product_option_value_data = array();
						foreach($product_option['product_option_value'] as $product_option_value) {
							$option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);
							if ($option_value_info) {
								$product_option_value_data[] = array(
									'product_option_value_id' => $product_option_value['product_option_value_id'],
									'option_value_id' => $product_option_value['option_value_id'],
									'name' => $option_value_info['name'],
									'price' => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
									'price_prefix' => $product_option_value['price_prefix']
								);
							}
						}

						$option_data[] = array(
							'product_option_id' => $product_option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id' => $product_option['option_id'],
							'name' => $option_info['name'],
							'type' => $option_info['type'],
							'value' => $product_option['value'],
							'required' => $product_option['required']
						);
					}
				}

				$json[] = array(
					'product_id' => $result['product_id'],
					'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')) ,
					'model' => $result['model'],
					'option' => $option_data,
					'price' => $result['price']
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}

?>