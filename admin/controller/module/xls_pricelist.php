<?php
class ControllerModuleXlsPricelist extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/xls_pricelist');
		$this->document->addScript('view/javascript/jquery/sortable/jquery-sortable.js');
		$this->document->addStyle('view/javascript/jquery/sortable/sortable.css');
		//$this->document->addScript('view/javascript/jquery/jpicker/jpicker-1.1.6.min.js');
		//$this->document->addStyle('view/javascript/jquery/jpicker/css/jPicker-1.1.6.min.css');
		//////$this->document->addStyle('view/javascript/jquery/jpicker/jPicker.css');
		$this->document->addScript('view/javascript/bootstrap/js/bootstrap-colorpicker.min.js');
		$this->document->addStyle('view/javascript/bootstrap/css/bootstrap-colorpicker.min.css');
		
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('catalog/category');
		$this->load->model('catalog/attribute_group');
		
		if(version_compare(VERSION, '2.1.0.0', '<')) {
			$this->load->model('sale/customer_group');
		}else{
			$this->load->model('customer/customer_group');
		}
		$this->load->model('localisation/language');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			if(isset($this->request->post['xls_pricelist_store'])){
				$this->request->post['xls_pricelist_store'] = implode("_", $this->request->post['xls_pricelist_store']);
			}else{
				$this->request->post['xls_pricelist_store'] = '';
			}
		
			if(isset($this->request->post['xls_pricelist_category'])){
				$this->request->post['xls_pricelist_category'] = implode("_", $this->request->post['xls_pricelist_category']);
			}else{
				$this->request->post['xls_pricelist_category'] = '';
			}
			
			if(isset($this->request->post['xls_pricelist_customer_group'])){
				$this->request->post['xls_pricelist_customer_group'] = implode("_", $this->request->post['xls_pricelist_customer_group']);
			}else{
				$this->request->post['xls_pricelist_customer_group'] = '';
			}
			
			if(isset($this->request->post['xls_pricelist_attribute_group'])){
				$this->request->post['xls_pricelist_attribute_group'] = implode("_", $this->request->post['xls_pricelist_attribute_group']);
			}else{
				$this->request->post['xls_pricelist_attribute_group'] = '';
			}
			
			
			
			$this->model_setting_setting->editSetting('xls_pricelist', $this->request->post);		
					
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));

		}
		
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_option'] = $this->language->get('tab_option');
		$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_design'] = $this->language->get('tab_design');
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_save'] = $this->language->get('text_save');
		$data['text_select_all'] = $this->language->get('text_select_all');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_browse'] = $this->language->get('text_browse');
		$data['text_clear'] = $this->language->get('text_clear');	
		$data['text_image_manager'] = $this->language->get('text_image_manager');
		$data['text_xls_pricelist'] = $this->language->get('text_xls_pricelist');
		$data['text_xls_success'] = $this->language->get('text_xls_success');
		
		$data['entry_nodubles'] = $this->language->get('entry_nodubles');
		$data['entry_logo_dimensions'] = $this->language->get('entry_logo_dimensions');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_stock'] = $this->language->get('entry_stock');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_special'] = $this->language->get('entry_special');
		$data['entry_dimensions'] = $this->language->get('entry_dimensions');
		$data['entry_code'] = $this->language->get('entry_code');
		
		$data['entry_use_cache'] = $this->language->get('entry_use_cache');
		$data['entry_use_collapse'] = $this->language->get('entry_use_collapse');
		$data['entry_use_protection'] = $this->language->get('entry_use_protection');
		$data['entry_use_password'] = $this->language->get('entry_use_password');
		$data['entry_category'] = $this->language->get('entry_category');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_use_options'] = $this->language->get('entry_use_options');
		$data['entry_use_attributes'] = $this->language->get('entry_use_attributes');
		$data['entry_use_quantity'] = $this->language->get('entry_use_quantity');
		$data['entry_use_notinstock'] = $this->language->get('entry_use_notinstock');
		$data['entry_attribute_groups'] = $this->language->get('entry_attribute_groups');
		$data['entry_image_dimensions'] = $this->language->get('entry_image_dimensions');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_adress'] = $this->language->get('entry_adress');
		$data['entry_phone'] = $this->language->get('entry_phone');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_link'] = $this->language->get('entry_link');
		$data['entry_logo'] = $this->language->get('entry_logo');
		$data['entry_custom_text'] = $this->language->get('entry_custom_text');
		$data['entry_currency'] = $this->language->get('entry_currency');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_text_list'] = $this->language->get('entry_text_list');
		$data['entry_view'] = sprintf($this->language->get('entry_view'), HTTP_CATALOG.'index.php?route=xls/xls_pricelist');
		$data['entry_for_all'] = $this->language->get('entry_for_all');
		$data['entry_memcache_warning'] = $this->language->get('entry_memcache_warning');
		$data['entry_colors'] = $this->language->get('entry_colors');
		$data['entry_color_text'] = $this->language->get('entry_color_text');
		$data['entry_color_bg'] = $this->language->get('entry_color_bg');
		$data['entry_thead_color'] = $this->language->get('entry_thead_color');
		$data['entry_underthead_color'] = $this->language->get('entry_underthead_color');
		$data['entry_category0_color'] = $this->language->get('entry_category0_color');
		$data['entry_category1_color'] = $this->language->get('entry_category1_color');
		$data['entry_category2_color'] = $this->language->get('entry_category2_color');
		$data['entry_image_color'] = $this->language->get('entry_image_color');
		$data['entry_model_color'] = $this->language->get('entry_model_color');
		$data['entry_name_color'] = $this->language->get('entry_name_color');
		$data['entry_stock_color'] = $this->language->get('entry_stock_color');
		$data['entry_price_color'] = $this->language->get('entry_price_color');
		$data['entry_special_color'] = $this->language->get('entry_special_color');
		
		$data['entry_poles'] = $this->language->get('entry_poles');
		$data['entry_pole_type'] = $this->language->get('entry_pole_type');
		$data['entry_pole_name'] = $this->language->get('entry_pole_name');
		$data['entry_category_link'] = $this->language->get('entry_category_link');
		$data['entry_product_link'] = $this->language->get('entry_product_link');
		$data['entry_alignment'] = $this->language->get('entry_alignment');
		
		
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['button_view'] = $this->language->get('button_view');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_remove'] = $this->language->get('button_remove');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();

		$this->load->model('setting/store');
		
		$data['stores'] = $this->model_setting_store->getStores();
		
		if (isset($this->request->post['product_store'])) {
			$data['product_store'] = $this->request->post['product_store'];
		} elseif (isset($this->request->get['product_id'])) {
			$data['product_store'] = $this->model_catalog_product->getProductStores($this->request->get['product_id']);
		} else {
			$data['product_store'] = array(0);
		}	
		
		$categories = $this->getAllCategories();
		$data['categories'] = $this->getAllCategories1($categories);
		
		
		$datas = array(
			'start' => 0,
			'limit' => 100
		);
		
		if(version_compare(VERSION, '2.1.0.0', '<')) {
			$data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups($datas);
		}else{
			$data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups($datas);
		}
		
		
		$data['attribute_groups'] = $this->model_catalog_attribute_group->getAttributeGroups(null);
		
		$data['poletypes'] = array(
			'product_id',
			'name',
			'description',
			'model',
			'sku',
			'upc',
			'location',
			'quantity',
			'image',
			'manufacturer',
			'price',
			'special',
			'date_available',
			'weight',
			'length',
			'width',
			'height'
		);
		
		$data['alignments'] = array(
			'left'	=> $this->language->get('text_left'),
			'right'	=> $this->language->get('text_right'),
			'center'	=> $this->language->get('text_center'),
		);
		
		$data['sorts'] = array();
			
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC'
			);
			
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC'
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC'
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC'
			); 

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC'
			); 
			
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_desc'),
				'value' => 'rating-DESC'
			); 
			
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_asc'),
				'value' => 'rating-ASC'
			);
			
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC'
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC'
			);
			
			$data['currencies']=array();
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency");
			
			foreach ($query->rows as $result) {
				$data['currencies'][] = array(
					'code'			=> $result['code'],
					'currency_id'   => $result['currency_id'],
					'title'         => $result['title'],
					'symbol_left'   => $result['symbol_left'],
					'symbol_right'  => $result['symbol_right'],
					'decimal_place' => $result['decimal_place'],
					'value'         => $result['value']
				); 
			}
			
			
		
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->error['xls_pricelist_category'])) {
			$data['error_xls_pricelist_category'] = $this->error['xls_pricelist_category'];
		} else {
			$data['error_xls_pricelist_category'] = '';
		}
		
		if (isset($this->error['xls_pricelist_store'])) {
			$data['error_xls_pricelist_store'] = $this->error['xls_pricelist_store'];
		} else {
			$data['error_xls_pricelist_store'] = '';
		}
		
		if (isset($this->error['xls_pricelist_customer_group'])) {
			$data['error_xls_pricelist_customer_group'] = $this->error['xls_pricelist_customer_group'];
		} else {
			$data['error_xls_pricelist_customer_group'] = '';
		}
		
		if (isset($this->error['xls_pricelist_dimensions1'])) {
			$data['error_xls_pricelist_dimensions1'] = $this->error['xls_pricelist_dimensions1'];
		} else {
			$data['error_xls_pricelist_dimensions1'] = '';
		}
		
		if (isset($this->error['xls_pricelist_dimensions'])) {
			$data['error_xls_pricelist_dimensions'] = $this->error['xls_pricelist_dimensions'];
		} else {
			$data['error_xls_pricelist_dimensions'] = '';
		}
		
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/xls_pricelist', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$data['action'] = $this->url->link('module/xls_pricelist', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['view'] = HTTP_CATALOG.'index.php?route=xls/xls_pricelist/view';
		
		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$data['xls_pricelist_store'] = array();
		if (isset($this->request->post['xls_pricelist_store'])) {
			$data['xls_pricelist_store'] =  $this->request->post['xls_pricelist_store'];
		} else {
			$data['xls_pricelist_store'] = explode("_", $this->config->get('xls_pricelist_store'));
		}
		
		$data['xls_pricelist_category'] = array();
		if (isset($this->request->post['xls_pricelist_category'])) {
			$data['xls_pricelist_category'] =  $this->request->post['xls_pricelist_category'];
		} else {
			$data['xls_pricelist_category'] = explode("_", $this->config->get('xls_pricelist_category'));
		}

		$data['xls_pricelist_customer_group'] = array();
		if (isset($this->request->post['xls_pricelist_customer_group'])) {
			$data['xls_pricelist_customer_group'] = $this->request->post['xls_pricelist_customer_group'];
		} else {
			$data['xls_pricelist_customer_group'] = explode("_", $this->config->get('xls_pricelist_customer_group'));
		}
		
		$data['xls_pricelist_attribute_group'] = array();
		if (isset($this->request->post['xls_pricelist_attribute_group'])) {
			$data['xls_pricelist_attribute_group'] = $this->request->post['xls_pricelist_attribute_group'];
		} else {
			$data['xls_pricelist_attribute_group'] = explode("_", $this->config->get('xls_pricelist_attribute_group'));
		}
		
		$data['xls_pricelist_poles'] = array();
		if (isset($this->request->post['xls_pricelist_poles'])) {
			$data['xls_pricelist_poles'] = $this->request->post['xls_pricelist_poles'];
		} else {
			$data['xls_pricelist_poles'] = is_array($this->config->get('xls_pricelist_poles'))?$this->config->get('xls_pricelist_poles'):array();
		}
		
		if(!$data['xls_pricelist_poles']){
			$arr1=array();
			$arr2=array();
			$arr3=array();
			$arr4=array();
			$arr5=array();
			$arr6=array();
		
			foreach ($data['languages'] as $language) {
				switch ($language['code']) {
					case 'ru':
						$arr1[$language['language_id']]='изображение';
						$arr2[$language['language_id']]='наименование';
						$arr3[$language['language_id']]='модель';
						$arr4[$language['language_id']]='кол-во';
						$arr5[$language['language_id']]='цена';
						$arr6[$language['language_id']]='акция';
						break;
					case 'en':
						$arr1[$language['language_id']]='image';
						$arr2[$language['language_id']]='name';
						$arr3[$language['language_id']]='model';
						$arr4[$language['language_id']]='quantity';
						$arr5[$language['language_id']]='price';
						$arr6[$language['language_id']]='special';
						break;
					 default:
						$arr1[$language['language_id']]='';
						$arr2[$language['language_id']]='';
						$arr3[$language['language_id']]='';
						$arr4[$language['language_id']]='';
						$arr5[$language['language_id']]='';
						$arr6[$language['language_id']]='';
				}
				
			}
			
		
			$data['xls_pricelist_poles'] = Array (
				Array ( 'type' => 'image', 'name' => $arr1, 'dlina' => 15, 'alignment' => 'left', 'textcolor' => '000000', 'bgcolor' => ''),
				Array ( 'type' => 'name', 'name' => $arr2, 'dlina' => 75, 'alignment' => 'left', 'textcolor' => '000000', 'bgcolor' => ''),
				Array ( 'type' => 'model', 'name' => $arr3, 'dlina' => 15, 'alignment' => 'center', 'textcolor' => '000000', 'bgcolor' => ''),
				Array ( 'type' => 'quantity', 'name' => $arr4, 'dlina' => 15, 'alignment' => 'center', 'textcolor' => '000000', 'bgcolor' => ''),
				Array ( 'type' => 'price', 'name' => $arr5, 'dlina' => 15, 'alignment' => 'right', 'textcolor' => '007f00', 'bgcolor' => ''),
				Array ( 'type' => 'special', 'name' => $arr6, 'dlina' => 15, 'alignment' => 'right', 'textcolor' => 'ff0000', 'bgcolor' => '')
			);
		}
	
		
		if (isset($this->request->post['xls_pricelist_sort_order'])) {
			$data['xls_pricelist_sort_order'] = $this->request->post['xls_pricelist_sort_order'];
		} else {
			$data['xls_pricelist_sort_order'] = $this->config->get('xls_pricelist_sort_order');
		}
		
		
		
		if (isset($this->request->post['xls_pricelist_use_options'])) {
			$data['xls_pricelist_use_options'] = $this->request->post['xls_pricelist_use_options'];
		} else {
			$data['xls_pricelist_use_options'] = $this->config->get('xls_pricelist_use_options');
		}

		if (isset($this->request->post['xls_pricelist_use_attributes'])) {
			$data['xls_pricelist_use_attributes'] = $this->request->post['xls_pricelist_use_attributes'];
		} else {
			$data['xls_pricelist_use_attributes'] = $this->config->get('xls_pricelist_use_attributes');
		}
		
		if (isset($this->request->post['xls_pricelist_nodubles'])) {
			$data['xls_pricelist_nodubles'] = $this->request->post['xls_pricelist_nodubles'];
		} else {
			$data['xls_pricelist_nodubles'] = $this->config->get('xls_pricelist_nodubles');
		}
		
		if (isset($this->request->post['xls_pricelist_use_quantity'])) {
			$data['xls_pricelist_use_quantity'] = $this->request->post['xls_pricelist_use_quantity'];
		} else {
			$data['xls_pricelist_use_quantity'] = $this->config->get('xls_pricelist_use_quantity');
		}
		
		if (isset($this->request->post['xls_pricelist_use_notinstock'])) {
			$data['xls_pricelist_use_notinstock'] = $this->request->post['xls_pricelist_use_notinstock'];
		} else {
			$data['xls_pricelist_use_notinstock'] = $this->config->get('xls_pricelist_use_notinstock');
		}
		
		if (isset($this->request->post['xls_pricelist_image_width'])) {
			$data['xls_pricelist_image_width'] = $this->request->post['xls_pricelist_image_width'];
		} else {
			$data['xls_pricelist_image_width'] = $this->config->get('xls_pricelist_image_width');
		}
		
		if (isset($this->request->post['xls_pricelist_image_height'])) {
			$data['xls_pricelist_image_height'] = $this->request->post['xls_pricelist_image_height'];
		} else {
			$data['xls_pricelist_image_height'] = $this->config->get('xls_pricelist_image_height');
		}
		
		if (isset($this->request->post['xls_pricelist_logo_width'])) {
			$data['xls_pricelist_logo_width'] = $this->request->post['xls_pricelist_logo_width'];
		} else {
			$data['xls_pricelist_logo_width'] = $this->config->get('xls_pricelist_logo_width');
		}
		
		if (isset($this->request->post['xls_pricelist_logo_height'])) {
			$data['xls_pricelist_logo_height'] = $this->request->post['xls_pricelist_logo_height'];
		} else {
			$data['xls_pricelist_logo_height'] = $this->config->get('xls_pricelist_logo_height');
		}
		
		if (isset($this->request->post['xls_pricelist_description'])) {
			$data['xls_pricelist_description'] = $this->request->post['xls_pricelist_description'];
		} else {
			$data['xls_pricelist_description'] = $this->config->get('xls_pricelist_description');
		}
		
		if (isset($this->request->post['xls_pricelist_category_link'])) {
			$data['xls_pricelist_category_link'] = $this->request->post['xls_pricelist_category_link'];
		} else {
			$data['xls_pricelist_category_link'] = $this->config->get('xls_pricelist_category_link');
		}
		if (isset($this->request->post['xls_pricelist_product_link'])) {
			$data['xls_pricelist_product_link'] = $this->request->post['xls_pricelist_product_link'];
		} else {
			$data['xls_pricelist_product_link'] = $this->config->get('xls_pricelist_product_link');
		}
		
		if (isset($this->request->post['xls_pricelist_view'])) {
			$data['xls_pricelist_view'] = $this->request->post['xls_pricelist_view'];
		} else {
			$data['xls_pricelist_view'] = $this->config->get('xls_pricelist_view');
		}
		
		if (isset($this->request->post['xls_pricelist_usecache'])) {
			$data['xls_pricelist_usecache'] = $this->request->post['xls_pricelist_usecache'];
		} else {
			$data['xls_pricelist_usecache'] = $this->config->get('xls_pricelist_usecache');
		}
		
		if (isset($this->request->post['xls_pricelist_memcacheServer'])) {
			$data['xls_pricelist_memcacheServer'] = $this->request->post['xls_pricelist_memcacheServer'];
		} else {
			$data['xls_pricelist_memcacheServer'] = $this->config->get('xls_pricelist_memcacheServer')?$this->config->get('xls_pricelist_memcacheServer'):'localhost';
		}
		
		if (isset($this->request->post['xls_pricelist_memcachePort'])) {
			$data['xls_pricelist_memcachePort'] = (int)$this->request->post['xls_pricelist_memcachePort'];
		} else {
			$data['xls_pricelist_memcachePort'] = $this->config->get('xls_pricelist_memcachePort')?$this->config->get('xls_pricelist_memcachePort'):'11211';
		}
		
		if (isset($this->request->post['xls_pricelist_cacheTime'])) {
			$data['xls_pricelist_cacheTime'] = (int)$this->request->post['xls_pricelist_cacheTime'];
		} else {
			$data['xls_pricelist_cacheTime'] = $this->config->get('xls_pricelist_cacheTime')?$this->config->get('xls_pricelist_cacheTime'):'600';
		}
		
		
		
		if (isset($this->request->post['xls_pricelist_use_collapse'])) {
			$data['xls_pricelist_use_collapse'] = $this->request->post['xls_pricelist_use_collapse'];
		} else {
			$data['xls_pricelist_use_collapse'] = $this->config->get('xls_pricelist_use_collapse');
		}
		if (isset($this->request->post['xls_pricelist_use_protection'])) {
			$data['xls_pricelist_use_protection'] = $this->request->post['xls_pricelist_use_protection'];
		} else {
			$data['xls_pricelist_use_protection'] = $this->config->get('xls_pricelist_use_protection');
		}
		if (isset($this->request->post['xls_pricelist_use_password'])) {
			$data['xls_pricelist_use_password'] = $this->request->post['xls_pricelist_use_password'];
		} else {
			$data['xls_pricelist_use_password'] = $this->config->get('xls_pricelist_use_password');
		}
		if (isset($this->request->post['xls_pricelist_code'])) {
			$data['xls_pricelist_code'] = $this->request->post['xls_pricelist_code'];
		} else {
			$data['xls_pricelist_code'] = $this->config->get('xls_pricelist_code');
		}
		
		
		
		if (isset($this->request->post['xls_pricelist_model_width'])) {
			$data['xls_pricelist_model_width'] = $this->request->post['xls_pricelist_model_width'];
		} else {
			$data['xls_pricelist_model_width'] = ($this->config->get('xls_pricelist_model_width')!='')?$this->config->get('xls_pricelist_model_width'):15;
		}
		
		if (isset($this->request->post['xls_pricelist_name_width'])) {
			$data['xls_pricelist_name_width'] = $this->request->post['xls_pricelist_name_width'];
		} else {
			$data['xls_pricelist_name_width'] = ($this->config->get('xls_pricelist_name_width')!='')?$this->config->get('xls_pricelist_name_width'):75;
		}
		
		if (isset($this->request->post['xls_pricelist_stock_width'])) {
			$data['xls_pricelist_stock_width'] = $this->request->post['xls_pricelist_stock_width'];
		} else {
			$data['xls_pricelist_stock_width'] = ($this->config->get('xls_pricelist_stock_width')!='')?$this->config->get('xls_pricelist_stock_width'):15;
		}
		
		if (isset($this->request->post['xls_pricelist_price_width'])) {
			$data['xls_pricelist_price_width'] = $this->request->post['xls_pricelist_price_width'];
		} else {
			$data['xls_pricelist_price_width'] = ($this->config->get('xls_pricelist_price_width')!='')?$this->config->get('xls_pricelist_price_width'):15;
		}
		
		if (isset($this->request->post['xls_pricelist_special_width'])) {
			$data['xls_pricelist_special_width'] = $this->request->post['xls_pricelist_special_width'];
		} else {
			$data['xls_pricelist_special_width'] = ($this->config->get('xls_pricelist_special_width')!='')?$this->config->get('xls_pricelist_special_width'):15;
		}
		
		if (isset($this->request->post['xls_pricelist_thead_color'])) {
			$data['xls_pricelist_thead_color'] = $this->request->post['xls_pricelist_thead_color'];
		} else {
			$data['xls_pricelist_thead_color'] = $this->config->get('xls_pricelist_thead_color');
		}
		if (isset($this->request->post['xls_pricelist_colors'])) {
			$data['xls_pricelist_colors'] = $this->request->post['xls_pricelist_colors'];
		} else {
			$data['xls_pricelist_colors'] = $this->config->get('xls_pricelist_colors');
		}
		if(!$data['xls_pricelist_colors']){
			$data['xls_pricelist_colors'] = array(
				'thead'			=> '000000',
				'thead_bg'		=> '',
				'underthead_bg'	=> '808080',
				'category0'		=> 'FFFFFF',
				'category0_bg'	=> '000000',
				'category1'		=> '000000',
				'category1_bg'	=> 'CCFFCC',
				'category2'		=> '000000',
				'category2_bg'	=> 'FFFF99',
				'image_bg'		=> '',
				'model'		=> '000000',
				'model_bg'		=> '',
				'name'		=> '000000',
				'name_bg'		=> '',
				'stock'		=> '000000',
				'stock_bg'		=> '',
				'price'		=> '000000',
				'price_bg'		=> '',
				'special'		=> 'FF0000',
				'special_bg'		=> ''
			);
		}
		
		
		$this->load->model('tool/image');
		
		if (isset($this->request->post['xls_pricelist_logo'])) {
			$data['xls_pricelist_logo'] = $this->request->post['xls_pricelist_logo'];
		} else {
			$data['xls_pricelist_logo'] = $this->config->get('xls_pricelist_logo');			
		}

		if ($this->config->get('xls_pricelist_logo') && file_exists(DIR_IMAGE . $this->config->get('xls_pricelist_logo')) && is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $this->model_tool_image->resize($this->config->get('xls_pricelist_logo'), 100, 100);		
		} else {
			$data['logo'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}
		
		$data['no_image'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		
		$data['token'] = $this->session->data['token'];
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
				
		$this->response->setOutput($this->load->view('module/xls_pricelist.tpl', $data));
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/xls_pricelist')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!isset($this->request->post['xls_pricelist_store'])||!$this->request->post['xls_pricelist_store']) {
			$this->error['xls_pricelist_store'] = $this->language->get('error_xls_pricelist_store');
		}
		
		if (!isset($this->request->post['xls_pricelist_category'])||!$this->request->post['xls_pricelist_category']) {
			$this->error['xls_pricelist_category'] = $this->language->get('error_xls_pricelist_category');
		}
		
		if (!isset($this->request->post['xls_pricelist_customer_group'])||!$this->request->post['xls_pricelist_customer_group']) {
			$this->error['xls_pricelist_customer_group'] = $this->language->get('error_xls_pricelist_customer_group');
		}
		
		if(isset($this->request->post['xls_pricelist_image_width'])&&(int)$this->request->post['xls_pricelist_image_width']>200){
			$this->error['xls_pricelist_dimensions'] = $this->language->get('error_xls_pricelist_dimensions');
		}
		
		if(isset($this->request->post['xls_pricelist_image_height'])&&(int)$this->request->post['xls_pricelist_image_height']>200){
			$this->error['xls_pricelist_dimensions'] = $this->language->get('error_xls_pricelist_dimensions');
		}
		
		if(isset($this->request->post['xls_pricelist_logo_width'])&&(int)$this->request->post['xls_pricelist_logo_width']>200){
			$this->error['xls_pricelist_dimensions1'] = $this->language->get('error_xls_pricelist_dimensions');
		}
		
		if(isset($this->request->post['xls_pricelist_logo_height'])&&(int)$this->request->post['xls_pricelist_logo_height']>200){
			$this->error['xls_pricelist_dimensions1'] = $this->language->get('error_xls_pricelist_dimensions');
		}
		
		if (isset($this->request->post['xls_pricelist_memcacheServer'])&&$this->request->post['xls_pricelist_memcacheServer']=='') {
			$this->request->post['xls_pricelist_memcacheServer'] = 'localhost';
		} 
		
		if (isset($this->request->post['xls_pricelist_memcachePort'])&&(int)$this->request->post['xls_pricelist_memcachePort']==0) {
			$this->request->post['xls_pricelist_memcachePort'] = 11211;
		}
		
		if (isset($this->request->post['xls_pricelist_cacheTime'])&&(int)$this->request->post['xls_pricelist_cacheTime']==0) {
			$this->request->post['xls_pricelist_cacheTime'] = 600;
		}
		
		if (isset($this->request->post['xls_pricelist_model_width'])&&(int)$this->request->post['xls_pricelist_model_width']<0) {
			$this->request->post['xls_pricelist_model_width'] = 15;
		}
		
		if (isset($this->request->post['xls_pricelist_name_width'])&&(int)$this->request->post['xls_pricelist_name_width']<0) {
			$this->request->post['xls_pricelist_name_width'] = 75;
		}
		
		if (isset($this->request->post['xls_pricelist_stock_width'])&&(int)$this->request->post['xls_pricelist_stock_width']<0) {
			$this->request->post['xls_pricelist_stock_width'] = 15;
		}
		
		if (isset($this->request->post['xls_pricelist_price_width'])&&(int)$this->request->post['xls_pricelist_price_width']<0) {
			$this->request->post['xls_pricelist_price_width'] = 15;
		}
		
		if (isset($this->request->post['xls_pricelist_special_width'])&&(int)$this->request->post['xls_pricelist_special_width']<0) {
			$this->request->post['xls_pricelist_special_width'] = 15;
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
	private function getAllCategories1($categories, $parent_id = 0, $parent_name = '') {
		$output = array();

		if (array_key_exists($parent_id, $categories)) {
			if ($parent_name != '') {
				$parent_name .= '>';
			}

			foreach ($categories[$parent_id] as $category) {
				$output[$category['category_id']] = array(
					'category_id' => $category['category_id'],
					'name'        => $parent_name . $category['name']
				);

				$output += $this->getAllCategories1($categories, $category['category_id'], $parent_name . $category['name']);
			}
		}

		return $output;
	}
	
	private function getAllCategories() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category c LEFT JOIN " . DB_PREFIX . "category_description cd ON (c.category_id = cd.category_id) LEFT JOIN " . DB_PREFIX . "category_to_store c2s ON (c.category_id = c2s.category_id) WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  ORDER BY c.parent_id, c.sort_order, cd.name");

		$category_data = array();
		foreach ($query->rows as $row) {
			$category_data[$row['parent_id']][$row['category_id']] = $row;
		}

		return $category_data;
	}
}
?>