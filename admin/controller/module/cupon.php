<?php
class ControllerModuleCupon extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/cupon');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('cupon', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_sale'] = $this->language->get('entry_sale');
		$data['entry_cupon'] = $this->language->get('entry_cupon');
		$data['entry_info'] = $this->language->get('entry_info');
		$data['entry_status'] = $this->language->get('entry_status');


		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['sale'])) {
			$data['error_sale'] = $this->error['code'];
		} else {
			$data['error_sale'] = '';
		}
		
		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}
		
		if (isset($this->error['info'])) {
			$data['error_info'] = $this->error['code'];
		} else {
			$data['error_info'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/cupon', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('module/cupon', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['cupon_sale'])) {
			$data['cupon_sale'] = $this->request->post['cupon_sale'];
		} else {
			$data['cupon_sale'] = $this->config->get('cupon_sale');
		}
		
		if (isset($this->request->post['cupon_code'])) {
			$data['cupon_code'] = $this->request->post['cupon_code'];
		} else {
			$data['cupon_code'] = $this->config->get('cupon_code');
		}
		
		if (isset($this->request->post['cupon_info'])) {
			$data['cupon_info'] = $this->request->post['cupon_info'];
		} else {
			$data['cupon_info'] = $this->config->get('cupon_info');
		}

		if (isset($this->request->post['cupon_status'])) {
			$data['cupon_status'] = $this->request->post['cupon_status'];
		} else {
			$data['cupon_status'] = $this->config->get('cupon_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/cupon.tpl', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'module/cupon')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['cupon_sale']) {
			$this->error['sale'] = $this->language->get('error_code');
		}
		
		if (!$this->request->post['cupon_code']) {
			$this->error['code'] = $this->language->get('error_code');
		}
		
		if (!$this->request->post['cupon_info']) {
			$this->error['info'] = $this->language->get('error_code');
		}

		return !$this->error;
	}
}