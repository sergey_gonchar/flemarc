<?php
class ControllerModuleWishlistDiscounts extends Controller {

	public function index() {
		$this->document->addStyle($this->getCatalogURL() . 'admin/view/stylesheet/wishlistdiscounts.css');

        $this->load->model('localisation/language');
		
		$this->load->model('module/wishlistdiscounts');
		$this->load->model('setting/setting');
    
	    $this->load->model('setting/store');
		
		if (isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id']; 
        } elseif (isset($this->request->post['store_id'])) {
			$store_id = $this->request->post['store_id']; 
		} else {
			$store_id = 0;
		}

		$data['store_id'] = $store_id;
		
	 	$store = $this->getCurrentStore($store_id);
		
		$this->language->load('module/wishlistdiscounts');
		
		$this->document->setTitle($this->language->get('heading_title'));
		$data['error_warning'] = '';
		
		$languages = $this->model_localisation_language->getLanguages();
        $data['languages'] = $languages;
        $firstLanguage = array_shift($languages);
        $data['firstLanguageCode'] = $firstLanguage['code'];

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if (!$this->user->hasPermission('modify', 'module/wishlistdiscounts')) {
					$this->response->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
			}
			if (!empty($_POST['OaXRyb1BhY2sgLSBDb21'])) {
					$this->request->post['WishlistDiscounts']['LicensedOn'] = $_POST['OaXRyb1BhY2sgLSBDb21'];
			}
			if (!empty($_POST['cHRpbWl6YXRpb24ef4fe'])) {
					$this->request->post['WishlistDiscounts']['License'] = json_decode(base64_decode($_POST['cHRpbWl6YXRpb24ef4fe']), true);
			}
			$this->model_setting_setting->editSetting('WishlistDiscounts', $this->request->post, $store_id);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('module/wishlistdiscounts', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$languageVariables = array(
			'heading_title',
			'entry_code',
			'coupon_validity',
			'user_email',
			'default_subject',
			'default_message',
			'admin_notification',
			'admin_notification_help',
			'message_has_been_sent',
			'select_date_format',
			'message_to_customer_heading',
			'message_to_customer_help',
			'default_discount_message',
			'text_no_results',
			'text_default',
			'default_discount_message',
			
		);
 
		foreach ($languageVariables as $languageVariable) {
				$data[$languageVariable] = $this->language->get($languageVariable);
		}
		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}
		if (isset($this->session->data['success'])) {     
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        
        if (isset($this->error['warning'])) { 
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
		$data['breadcrumbs']   = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('module/wishlistdiscounts', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
		$data['currency'] = $this->config->get('config_currency');
		$data['action']        = $this->url->link('module/wishlistdiscounts', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel']        = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		$data['data']          = $this->model_setting_setting->getSetting('WishlistDiscounts', $store_id);
        $data['token']         = $this->session->data['token'];

		$data['email']	=	$this->config->get('config_email');

 		$data['stores'] = array_merge(array(0 => array('store_id' => '0', 'name' => $this->config->get('config_name') . ' ' . $data['text_default'], 'url' => HTTP_SERVER, 'ssl' => HTTPS_SERVER)), $this->model_setting_store->getStores());
        $data['store']                  = $store;

		 if ($this->config->get('WishlistDiscounts_status')) { 
            $data['WishlistDiscounts_status'] = $this->config->get('WishlistDiscounts_status'); 
        } else {
            $data['WishlistDiscounts_status'] = '0';
        }
		
        $data['header']                 = $this->load->controller('common/header');
        $data['column_left']            = $this->load->controller('common/column_left');
        $data['footer']                 = $this->load->controller('common/footer');
		
		$storeURL = $this->getCatalogURL();

		$this->response->setOutput($this->load->view('module/wishlistdiscounts.tpl', $data)); 
		
		$data = array();		
		
		
		$data['storeURL']       = $storeURL;
		$data['unsubscribeURL'] = $storeURL . 'index.php?route=account/newsletter';
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template'). '/module/wishlistdiscounts/discount_template.tpl')) {
			return $this->load->view($this->config->get('config_template'). '/module/wishlistdiscounts/discount_template.tpl', $data);
		 } else {
			return $this->load->view('/module/wishlistdiscounts/discount_template.tpl', $data);
		 }  
		
	}
	
	public function customerList($action) {
		
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('module/wishlistdiscounts');
		$this->load->language('module/wishlistdiscounts');
		
		if (isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id']; 
        } elseif (isset($this->request->post['store_id'])) {
			$store_id = $this->request->post['store_id']; 
		} else {
			$store_id = 0;
		}
		$data['store_id'] = $store_id;

		if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
		} else {
				$sort = 'firstname, lastname';
		}
		if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
		} else {
				$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
		} else {
				$page = 1;
		} 

		$url = '';
		if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
		}

		$data   = array(
				'sort' => $sort,
				'order' => $order,
				'start' => ($page - 1) * $this->config->get('config_limit_admin'),
				'limit' => $this->config->get('config_limit_admin')
		);		
		
		$languageVariables = array (
			'column_customer_name',
			'column_date_added',
			'column_customer_wishlist',
			'column_customer_email',
			'show_wishlist',
			'button_send_discounts_to_selected',
			'button_send_discount_to_all',
			'text_no_results',
			'wishlist_heading'
		);
 
		foreach ($languageVariables as $languageVariable) {
				$data[$languageVariable] = $this->language->get($languageVariable);
		}

		$url = '';
		if ($order == 'ASC') {
				$url .= '&order=DESC';
		} else {
				$url .= '&order=ASC';
		}
		$data['sort_name']       = 'index.php?route=module/wishlistdiscounts/customerList&token=' . $this->session->data['token'] . '&sort=name' . $url;
		$data['sort_date_added'] = 'index.php?route=module/wishlistdiscounts/customerList&token=' . $this->session->data['token'] . '&sort=c.date_added' . $url;
		$data['sort_email']      = 'index.php?route=module/wishlistdiscounts/customerList&token=' . $this->session->data['token'] . '&sort=c.email' . $url;
		$data['continue'] = $this->url->link('account/account', '', 'SSL');
	
		$url = '';
		if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) { 
				$url .= '&order=' . $this->request->get['order'];
		} 
		
		if($action == 'archivedWishlists') {
			$data['customers']  = $this->model_module_wishlistdiscounts->gethWishListArchive($data, $store_id);  
			$total_customers = $this->model_module_wishlistdiscounts->getTotalWishListArchive($store_id);
		
		} else { 
			$data['customers']  = $this->model_module_wishlistdiscounts->getCustomersWithWishList($data, $store_id);  
			$total_customers  = $this->model_module_wishlistdiscounts->getTotalCustomersWithWishList($store_id);
		}

	
		$pagination               = new Pagination();
		$pagination->total        = $total_customers;
		$pagination->page         = $page;
		$pagination->limit        = $this->config->get('config_limit_admin');
		$pagination->text         = $this->language->get('text_pagination');
		$pagination->url          = 'index.php?route=module/wishlistdiscounts/customerList&token=' . $this->session->data['token'] . $url . '&page={page}';
		$data['pagination'] = $pagination->render();
	 	$data['sort']       = $sort;
		$data['order']      = $order;
		 

		$this->response->setOutput($this->load->view('module/wishlistdiscounts/customerList.tpl', $data)); 
	}

	public function wishlist() {
	
		$this->load->model('catalog/product');
		$this->load->model('tool/image');
		$this->load->model('module/wishlistdiscounts');
		$this->load->language('module/wishlistdiscounts');
		
		if (isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id']; 
        } elseif (isset($this->request->post['store_id'])) {
			$store_id = $this->request->post['store_id']; 
		} else {
			$store_id = 0;
		}
		$data['store_id'] = $store_id;
		
		$languageVariables = array (
			'heading_title', 
			'text_empty',
			'column_image',  
			'column_name',   
			'column_model',  
			'column_stock',  
			'column_price',  
			'column_action', 
			'button_continue',
			'button_cart',   
			'button_remove', 
			'discount_text' 
		);
		foreach ($languageVariables as $languageVariable) {
			$data[$languageVariable] = $this->language->get($languageVariable);
		}

		$data['currency']                     = $this->config->get('config_currency');
		$data['products']                     = array(); 
		$data['customer_info']['name']        = $this->request->get['customer_name'];
		$data['customer_info']['customer_id'] = $this->request->get['customer_id'];
		$customer_id                          = $this->request->get['customer_id'];
		$data['currencyLeft']  = $this->currency->getSymbolLeft($this->config->get('config_currency'));
		$data['currencyRight'] = $this->currency->getSymbolRight($this->config->get('config_currency'));

		$wishList                                   = $this->model_module_wishlistdiscounts->getCustomerWishlist($customer_id, $store_id);
		
		foreach ($wishList as $product_info) {
			
			if (isset($product_info)) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_wishlist_width'), $this->config->get('config_image_wishlist_height'));
					} else {
						$image = false;
					}
					if ($product_info['quantity'] <= 0) { 
						$stock = $product_info['stock_status'];
					} elseif ($this->config->get('config_stock_display')) {
						$stock = $product_info['quantity'];
					} else {
						$stock = $this->language->get('text_instock');
					}
					if (!empty($product_info['regular_price'])) {
						$price = $product_info['regular_price'];
					} else {
						$price = false;
					}
					if ((float) $product_info['special_price']) {
						$special = $product_info['special_price'];
					} else {
						$special = false;
					}
					$data['products'][] = array(
						'product_id' => $product_info['wishlist_product_id'],
						'thumb' => $image,
						'name' => $product_info['product_name'],
						'model' => $product_info['model'],
						'stock' => $stock,
						'price' => $price,
						'special' => $special,
						'href' => $this->getCatalogURL() . 'index.php?route=product/product&product_id=' . $product_info['wishlist_product_id'],
						'remove' => $this->url->link('account/wishlist', 'remove=' . $product_info['wishlist_product_id'])
					);
			} else {
				unset($this->session->data['wishlist'][$key]);
			}
		}

		$this->response->setOutput($this->load->view('module/wishlistdiscounts/wishlist.tpl', $data)); 
		
	}
	
	public function mailForm() {

		$this->load->model('setting/setting');
		$this->load->model('module/wishlistdiscounts');
		$this->load->language('module/wishlistdiscounts');
		
		if (isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id']; 
        } elseif (isset($this->request->post['store_id'])) {
			$store_id = $this->request->post['store_id']; 
		} else {
			$store_id = 0;
		}
		$data['store_id'] = $store_id;
  		
		$this->session->data['sendToAllCustomers'] = NULL;
		$customers =  array();
		if(!empty($this->request->get['sendToAllCustomers']) && $this->request->get['sendToAllCustomers'] ) {
			$this->session->data['sendToAllCustomers'] = $this->request->get['sendToAllCustomers'];
			$customers = $this->model_module_wishlistdiscounts->getCustomersWithWishList($data=array(), $store_id);
		}   
		if(!empty($this->request->get["customers"])) { 
			$customers =  $this->model_module_wishlistdiscounts->getCustomers($this->request->get["customers"], $store_id);
		}
		
		$data = array();
		$storeURL = $data['server'] = $this->getCatalogURL();
		
		$languageVariables = array(
			'user_email',
			'default_discount_message',
			'subject_text',
			'default_subject',
			'total_amount',
			'discount_code_text',
			'text_subject',
			'text_type',
			'text_discount',
			'text_duration',
			'text_days',
			'text_send',
			'text_percentage',
			'text_fixed_amount'
		);

		foreach ($languageVariables as $languageVariable) {
			$data[$languageVariable] = $this->language->get($languageVariable);
		}
			
		$data['currencyLeft']  = $this->currency->getSymbolLeft($this->config->get('config_currency'));
		$data['currencyRight'] = $this->currency->getSymbolRight($this->config->get('config_currency'));
		$data['storeURL'] = $this->getCatalogURL();
		$data['unsubscribeURL'] = $this->getCatalogURL() . 'index.php?route=account/newsletter';
		$this->session->data['customers'] = $customers;

		$data['data'] = $this->model_setting_setting->getSetting('WishlistDiscounts', $store_id);  
		$data['currency'] = $this->config->get('config_currency');		
		$data['customers'] = $customers; 
		$data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');
		
		$languages = $this->model_localisation_language->getLanguages();
        $data['languages'] = $languages;
        $firstLanguage = array_shift($languages);
        $data['firstLanguageCode'] = $firstLanguage['code'];
		
		$data['defaultMessage'] = $this->load->view('module/wishlistdiscounts/discount_template.tpl', $data);
		
		$this->response->setOutput($this->load->view('module/wishlistdiscounts/mailForm.tpl', $data)); 
	}
	
	public function sendMail() {

		$this->load->model('module/wishlistdiscounts');
		$this->load->model('sale/customer');
		$this->load->model('tool/image');
		$this->load->model('marketing/coupon');
		
		if (isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id']; 
        } elseif (isset($this->request->post['store_id'])) {
			$store_id = $this->request->post['store_id']; 
		} else {
			$store_id = 0;
		}
		$data['store_id'] = $store_id;
		
		$data['token'] = $this->session->data['token'];
		
		$this->load->model('localisation/language');
		
		$languages = $this->model_localisation_language->getLanguages();
        $data['languages'] = $languages;
        $firstLanguage = array_shift($languages);
        $data['firstLanguageCode'] = $firstLanguage['code'];
		
		$data = array();
		$discount = $this->request->post['discount'];
		$discount_type = $this->request->post['discount_type'];
		$duration  = $this->request->post['duration'];

		$customers = $this->model_module_wishlistdiscounts->getCustomers($this->request->post['customer_id'], $store_id); 

		foreach($customers as $customer) {
			$products = $this->model_module_wishlistdiscounts->getCustomerWishlist($customer['customer_id'], $store_id);
			$coupon_product = array();
			foreach ($products as $product) {
				 $coupon_product[] = $product['wishlist_product_id'];
			}
			$images   = array();
			foreach ($products as $product) {
				$images[$product['wishlist_product_id']] = $this->model_tool_image->resize($product['image'], 200, 200);
			}
			
				$discountCode =  $this->generateUniqueRandomVoucherCode();
				$dateEnd = date('Y-m-d', time() + ((int) $duration * 24 * 60 * 60));
				$couponInfo = array(
					'name' => 'WishlistDiscount [' . $customer['email'] . ']',
					'code' => $discountCode,
					'discount' => (int)$discount,
					'type' => $discount_type,
					'total' => '0',
					'logged' => '1',
					'shipping' => '0',
					'date_start' => date('Y-m-d', time()),
					'date_end' => $dateEnd,
					'uses_total' => '1',
					'uses_customer' => '1',
					'status' => '1',
					'coupon_product' => $coupon_product
				); 
				$coupon_id = $this->model_marketing_coupon->addCoupon($couponInfo); 
				//$coupon_id = $this->db->getLastId();
				$wordTemplates = array(
					"{firstname}", 
					"{lastname}", 
					"{discount_code}", 
					"{discount_value}", 
					"{date_end}", 
					"{customer_wishlist}"
				); 
				
				$data['products'] = $products;
				$data['images'] = $images;
				$data['currencyLeft']  = $this->currency->getSymbolLeft($this->config->get('config_currency'));
				$data['currencyRight'] = $this->currency->getSymbolRight($this->config->get('config_currency'));
				$data['storeURL'] = $this->getCatalogURL();
				$words = array(
					$customer['firstname'], 
					$customer['lastname'], 
					$discountCode, 
					$discount, 
					$dateEnd, 
					$this->load->view('module/wishlistdiscounts/customer_wishlist_template.tpl', $data)
				); 

				$data['storeURL'] = $this->getCatalogURL();
				if(!empty($this->request->post['message'])){
					$message = 	str_replace($wordTemplates, $words, $this->request->post['message']);
				} else {
					$message = str_replace($wordTemplates, $words, $this->load->view('module/wishlistdiscounts/discount_template.tpl',$data));
				}
			
				$this->model_module_wishlistdiscounts->logDiscount(array('customer_id' => $customer['customer_id'], 'wishlist' => $customer['wishlist'], 'coupon_id' => $coupon_id), $store_id);
				
				
				$mailToUser            = new Mail($this->config->get('config_mail'));
				$mailToUser->setTo($customer['email']);
				$mailToUser->setFrom($this->config->get('config_email'));
				$mailToUser->setSender($this->config->get('config_email'));
				$mailToUser->setSubject(html_entity_decode($this->request->post['subject'], ENT_QUOTES, 'UTF-8'));
				$mailToUser->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mailToUser->send();
		}
	}
	
	public function givenCoupons() {		
		$this->load->model('module/wishlistdiscounts');
		$action='givenCoupons';
		$this->listCoupons($action);	
	}
	
	public function usedCoupons() {		
		$this->load->model('module/wishlistdiscounts');
		$action='usedCoupons';
		$this->listCoupons($action);	
	}
	
	public function currentWishlists() {		
		$this->load->model('module/wishlistdiscounts');
		$action='currentWishlists';
		$this->customerList($action);	
	}
	
	public function archivedWishlists() {		
		$this->load->model('module/wishlistdiscounts');
		$action='archivedWishlists';
		$this->customerList($action);	
	}
	
	public function install() {
		$this->load->model('module/wishlistdiscounts');
		$this->model_module_wishlistdiscounts->install();
	}	

	public function uninstall() {
		$this->load->model('setting/setting');	
		$this->load->model('setting/store');
		$this->model_setting_setting->deleteSetting('WishlistDiscounts', 0);
		$stores=$this->model_setting_store->getStores();
		foreach ($stores as $store) {
			$this->model_setting_setting->deleteSetting('WishlistDiscounts', $store['store_id']);
		}
		$this->load->model('module/wishlistdiscounts');
		$this->model_module_wishlistdiscounts->uninstall();
	}

	private function listCoupons($action) { 
	
		$this->load->language('module/wishlistdiscounts');
		
		if (isset($this->request->get['store_id'])) {
			$store_id = $this->request->get['store_id']; 
        } elseif (isset($this->request->post['store_id'])) {
			$store_id = $this->request->post['store_id']; 
		} else {
			$store_id = 0;
		}
		$data['store_id'] = $store_id;
	
		if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
		} else {
				$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
		} else {
				$page = 1;
		}

		$url = '';
		if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
		}

		$data['givenCoupons']       = array();

		$data                        = array(
				'sort' => $sort,
				'order' => $order,
				'start' => ($page - 1) * $this->config->get('config_limit_admin'),
				'limit' => $this->config->get('config_limit_admin')
		);
		if($action == 'usedCoupons') {
			$coupon_total = $this->model_module_wishlistdiscounts->getTotalUsedCoupons($store_id); 
			$coupons  = $this->model_module_wishlistdiscounts->getUsedCoupons($data, $store_id);
		
		} else { 
			$coupon_total  = $this->model_module_wishlistdiscounts->getTotalGivenCoupons($store_id);
			$coupons  =      $this->model_module_wishlistdiscounts->getGivenCoupons($data, $store_id);
		}
		if(!empty($coupons)) {
			foreach ($coupons as $coupon) {
				$data['coupons'][] = array(
					'coupon_id' => $coupon['coupon_id'],
					'name' => $coupon['name'],
					'code' => $coupon['code'],
					'discount' => $coupon['discount'],
					'date_start' => date($this->language->get('date_format_short'), strtotime($coupon['date_start'])),
					'date_end' => date($this->language->get('date_format_short'), strtotime($coupon['date_end'])),
					'status' => ($coupon['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
					'date_added' => $coupon['date_added']
				);
			}
		}

		$languageVariables = array(
			'heading_title',  
			'text_no_results',
			'column_coupon_name',
			'column_code',   
			'column_discount',  
			'column_date_start',
			'column_date_end',  
			'column_status',  
			'button_insert',    
			'button_delete',    
			'column_email',    
			'column_date_added'
			);
		foreach ($languageVariables as $languageVariable) {
			$data[$languageVariable] = $this->language->get($languageVariable);
		}

		if (isset($this->error['warning'])) {
				$data['error_warning'] = $this->error['warning'];
		} else {
				$data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];
				unset($this->session->data['success']);
		} else {
				$data['success'] = '';
		}

		$url = '';
		if ($order == 'ASC') {
				$url .= '&order=DESC';
		} else {
				$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['sort_name']       = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=name' . $url;
		$data['sort_code']       = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=code' . $url;
		$data['sort_discount']   = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=discount' . $url;
		$data['sort_date_start'] = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=date_start' . $url;
		$data['sort_date_end']   = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=date_end' . $url;
		$data['sort_status']     = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=status' . $url;
		$data['sort_email']      = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=email' . $url;
		$data['sort_discount_type'] = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=discount_type' . $url;
		$data['sort_date_added']   = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . '&sort=date_added' . $url;
		$url = '';
		if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
		}
		$pagination               = new Pagination();
		$pagination->total        = $coupon_total;
		$pagination->page         = $page;
		$pagination->limit        = $this->config->get('config_limit_admin');
		$pagination->text         = $this->language->get('text_pagination');
		$pagination->url          = 'index.php?route=module/wishlistdiscounts/'.$action.'&token=' . $this->session->data['token'] . $url . '&page={page}';
		$data['pagination'] = $pagination->render();
		$data['sort']       = $sort;
		$data['order']      = $order;

		$this->response->setOutput($this->load->view('module/wishlistdiscounts/coupon.tpl', $data)); 

	}
	
	private function generateUniqueRandomVoucherCode() {
		$this->load->model('module/wishlistdiscounts');
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$couponCode = '';
		for ($i = 0; $i < 10; $i++) {
				$couponCode .= $characters[rand(0, strlen($characters) - 1)];
		}
		if ($this->model_module_wishlistdiscounts->isUniqueCode($couponCode)) {
				return $couponCode;
		} else {
				return $this->generateUniqueRandomVoucherCode();
		}
	}
	
    private function getCatalogURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $storeURL = HTTPS_CATALOG;
        } else {
            $storeURL = HTTP_CATALOG;
        }
        return $storeURL;   
    }

    private function getServerURL() {
        if (isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) {
            $serverURL = HTTPS_SERVER;
        } else {
            $serverURL = HTTP_SERVER;
        }
        return $serverURL;   
    }		
		
	private function getCurrentStore($store_id) {    
        if($store_id && $store_id != 0) {
            $store = $this->model_setting_store->getStore($store_id);
        } else {
            $store['store_id'] = 0;
            $store['name'] = $this->config->get('config_name');
            $store['url'] = $this->getCatalogURL(); 
        }
        return $store;
    }
}
?>