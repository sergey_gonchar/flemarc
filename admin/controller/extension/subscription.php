<?php
class ControllerExtensionSubscription extends Controller {
	private $error = array();
	
	public function index() {
		$this->language->load('module/subscription');
		
		$this->load->model('extension/subscription');
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_lang'] = $this->language->get('text_lang');
		$data['text_del'] = $this->language->get('text_del');
		$data['text_warning'] = $this->language->get('text_warning');
		$data['text_search'] = $this->language->get('text_search');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/news', 'token=' . $this->session->data['token'] . $url, 'SSL')
   		);
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		
			unset($this->error['warning']);
		} else {
			$data['error'] = '';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}
		
		$url = '';
		
		$filter_data = array(
			'page' => $page,
			'limit' => $this->config->get('config_limit_admin'),
			'start' => $this->config->get('config_limit_admin') * ($page - 1),
		);
		
		$total = $this->model_extension_subscription->getTotalSubscriptions();
		
		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/subscription', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));
        $data['token'] = $this->session->data['token'];

		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
		$data['all_viewsubscribers'] = array();
		
		$all_viewsubscribers = $this->model_extension_subscription->getViewSubscribers($filter_data);

		foreach ($all_viewsubscribers as $getViewSubscribers) {
			$data['all_viewsubscribers'][] = array (				
				'subscribe_id' 		=> $getViewSubscribers['subscribe_id'],
				'customer_name' 	=> $getViewSubscribers['customer_name'],
				'customer_email' 	=> $getViewSubscribers['customer_email'],
				'date_created' 		=> date($this->language->get('date_format_short'), strtotime($getViewSubscribers['date_created'])),
                'store_id' 			=> $getViewSubscribers['store_id'],
                'language_id' 		=> $getViewSubscribers['language_id'],			
			);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/subscriptions_list.tpl', $data));
	
	}
	
	 public function deleteSubscriber() {
		if (isset($_POST['subscribe_id'])) {
			$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "subscription` WHERE `subscribe_id`=".(int)$_POST['subscribe_id']);
			if ($run_query) echo "Success!";
		}
	}
}