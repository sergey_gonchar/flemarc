<?php
class ControllerExtensionCallme extends Controller {
	private $error = array();
	
	public function index() {
		$this->language->load('module/callme');
		
		$this->load->model('extension/callme');
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_phone'] = $this->language->get('text_phone');
		$data['text_comment'] = $this->language->get('text_comment');
		$data['text_time'] = $this->language->get('text_time');
		$data['text_name'] = $this->language->get('text_name');
		$data['text_date'] = $this->language->get('text_date');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_lang'] = $this->language->get('text_lang');
		$data['text_del'] = $this->language->get('text_del');
		$data['text_warning'] = $this->language->get('text_warning');
		$data['text_search'] = $this->language->get('text_search');
		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/callme', 'token=' . $this->session->data['token'] . $url, 'SSL')
   		);
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->error['warning'])) {
			$data['error'] = $this->error['warning'];
		
			unset($this->error['warning']);
		} else {
			$data['error'] = '';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}
		
		$url = '';
		
		$filter_data = array(
			'page' => $page,
			'limit' => $this->config->get('config_limit_admin'),
			'start' => $this->config->get('config_limit_admin') * ($page - 1),
		);
		
		$total = $this->model_extension_callme->getTotalCallmes();
		
		$pagination = new Pagination();
		$pagination->total = $total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('extension/callme', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));
        $data['token'] = $this->session->data['token'];

		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				
		$data['all_callmes'] = array();
		
		$all_callmes = $this->model_extension_callme->getViewCallmes($filter_data);

		foreach ($all_callmes as $getViewCallmes) {
			$data['all_callmes'][] = array (				
				'callme_id' 		=> $getViewCallmes['callme_id'],
				'customer_name' 	=> $getViewCallmes['customer_name'],
				'customer_phone' 	=> $getViewCallmes['customer_phone'],
				'customer_comment' 	=> $getViewCallmes['customer_comment'],
				'manager_comment' 	=> $getViewCallmes['manager_comment'],				
				'time' 	            => $getViewCallmes['time'],
				'date_created' 		=> date($this->language->get('date_format_short'), strtotime($getViewCallmes['date_created'])),
                'store_id' 			=> $getViewCallmes['store_id'],
                'language_id' 		=> $getViewCallmes['language_id'],			
			);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/callme_list.tpl', $data));
	
	}
	
	 public function deleteCallme() {
		if (isset($_POST['callme_id'])) {
			$run_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "callme` WHERE `callme_id`=".(int)$_POST['callme_id']);
			if ($run_query) echo "Success!";
		}
	}
	
	public function posting() {
		
		if (isset($_POST['callme_id'])) {
		$comment = mysql_real_escape_string($_POST['manager_comment']);
		
			$run_query = $this->db->query("UPDATE `" . DB_PREFIX . "callme` SET `manager_comment`= '". $_POST['manager_comment'] . "' WHERE `callme_id`= '".(int)$_POST['callme_id'] ."'");
		}
	}
}