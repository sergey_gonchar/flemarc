<?php
class ControllerDboptimMain extends Controller
{
	public function index() {
		$data = array();
		
		$result = $this->db->query('show tables');
		$data['tables'] = array();
		
		foreach ($result->rows as $row) {
			$data['tables'][ $row[key($row)] ] = null;
		}
		
// 		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($_POST['optimize']) && is_array($_POST['optimize']) && ! empty($_POST['optimize'])) {
// 			foreach($_POST['optimize'] as $table) {
				foreach ($data['tables'] as $table => $value) {
					$result = $this->db->query('optimize table '. $table);
					
					$data['tables'][$table] = $result->rows[0]['Msg_text'];
				}
// 			}
// 		}
		
		$this->load->language('dboptim/main');
		
		$languages = array('text_optimize_selected_tables');
		
		foreach($languages as $lang) {
			$data[$lang] = $this->language->get($lang);
		}
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		
		$this->response->setOutput($this->load->view('dboptim/main.tpl', $data));
	}
}