<?php 
class ControllerMarketingContacts extends Controller {
	private $error = array();
	 
	public function index() {
		$this->load->language('marketing/contacts');
		$this->load->model('marketing/contacts');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_default'] = $this->language->get('text_default');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_customer_all'] = $this->language->get('text_customer_all');	
		$data['text_customer'] = $this->language->get('text_customer');
		$data['text_client_all'] = $this->language->get('text_client_all');
		$data['text_customer_group'] = $this->language->get('text_customer_group');
		$data['text_affiliate_all'] = $this->language->get('text_affiliate_all');	
		$data['text_affiliate'] = $this->language->get('text_affiliate');	
		$data['text_product'] = $this->language->get('text_product');
		$data['text_manual'] = $this->language->get('text_manual');

		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_to'] = $this->language->get('entry_to');
		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_customer'] = $this->language->get('entry_customer');
		$data['entry_affiliate'] = $this->language->get('entry_affiliate');
		$data['entry_product'] = $this->language->get('entry_product');
		$data['entry_manual'] = $this->language->get('entry_manual');
		$data['entry_subject'] = $this->language->get('entry_subject');
		$data['entry_message'] = $this->language->get('entry_message');
		
		$data['button_send'] = $this->language->get('button_send');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['text_no_lic'] = $this->language->get('text_no_lic');
		$data['button_bta'] = $this->language->get('button_bta');
		
		$data['tab_send'] = $this->language->get('tab_send');
		$data['tab_template'] = $this->language->get('tab_template');
		$data['tab_log'] = $this->language->get('tab_log');
		$data['tab_setting'] = $this->language->get('tab_setting');
		
		$data['token'] = $this->session->data['token'];

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('marketing/contacts', 'token=' . $this->session->data['token'], 'SSL')
   		);

    	$data['cancel'] = $this->url->link('marketing/contacts', 'token=' . $this->session->data['token'], 'SSL');

		$data['stores'] = $this->model_marketing_contacts->getShopStores();
		$data['customer_groups'] = $this->model_marketing_contacts->getCustomersGroups();
		
		$data['lic'] = false;
		$check_lic = $this->model_marketing_contacts->checkLicense();
		if ($check_lic) {
			$data['lic'] = true;
		}
		
		// Send //
		$data['text_wait'] = $this->language->get('text_wait');
		$data['text_tegi'] = $this->language->get('text_tegi');
		$data['spravka_tegi'] = $this->language->get('spravka_tegi');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_region'] = $this->language->get('text_region');
		$data['entry_template'] = $this->language->get('entry_template');
		$data['text_save_template'] = $this->language->get('text_save_template');
		$data['text_template_name'] = $this->language->get('text_template_name');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_unsubscribe'] = $this->language->get('entry_unsubscribe');
		$data['entry_insert_products'] = $this->language->get('entry_insert_products');
		$data['entry_special'] = $this->language->get('entry_special');
		$data['entry_bestseller'] = $this->language->get('entry_bestseller');
		$data['entry_featured'] = $this->language->get('entry_featured');
		$data['entry_latest'] = $this->language->get('entry_latest');
		$data['entry_selected'] = $this->language->get('entry_selected');
		$data['entry_pselected'] = $this->language->get('entry_pselected');
		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_limit'] = $this->language->get('entry_limit');
		
		$data['help_product'] = $this->language->get('help_product');
		$data['help_manual'] = $this->language->get('help_manual');
		$data['help_customer'] = $this->language->get('help_customer');
		$data['help_affiliate'] = $this->language->get('help_affiliate');
		$data['help_selproduct'] = $this->language->get('help_selproduct');
		$data['help_subject'] = $this->language->get('help_subject');
		$data['help_message'] = $this->language->get('help_message');
		$data['error_close'] = $this->language->get('error_close');
		
		$data['templates'] = array();
		
		$data['templates'] = $this->model_marketing_contacts->getTemplates();
		
		if (isset($this->request->post['template_id'])) {
			$data['template_id'] = $this->request->post['template_id'];
		} else {
			$data['template_id'] = '';
		}
		
		if (isset($this->request->post['set_region'])) {
			$data['set_region'] = $this->request->post['set_region'];
		} else {
			$data['set_region'] = '';
		}
		
		if (isset($this->request->post['country_id'])) {
			$data['country_id'] = $this->request->post['country_id'];
		} else {
			$data['country_id'] = '';
		}
		
		$data['countries'] = $this->model_marketing_contacts->getShopCountries();
		
		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = $this->request->post['zone_id'];
		} else {
			$data['zone_id'] = '';
		}
		
		if (isset($this->request->post['set_unsubscribe'])) {
			$data['set_unsubscribe'] = $this->request->post['set_unsubscribe'];
		} else {
			$data['set_unsubscribe'] = '';
		}
		// Send //
		
		// Templates //
		$data['column_template_name'] = $this->language->get('column_template_name');
		$data['column_action'] = $this->language->get('column_action');
		$data['text_view'] = $this->language->get('text_view');
		$data['text_delete'] = $this->language->get('text_delete');
		$data['text_no_template'] = $this->language->get('text_no_template');
		$data['text_save'] = $this->language->get('text_save');
		$data['text_new_template'] = $this->language->get('text_new_template');
		$data['editor_mode_alert'] = $this->language->get('editor_mode_alert');
		// Templates //
		
		// log //
		$data['button_clearlog'] = $this->language->get('button_clearlog');
		$data['button_updatelog'] = $this->language->get('button_updatelog');
		$data['text_delete_log'] = $this->language->get('text_delete_log');
		$data['text_update_log'] = $this->language->get('text_update_log');
		
		$file = DIR_LOGS . 'contacts.log';
		
		if (file_exists($file)) {
			$data['log'] = file_get_contents($file, FILE_USE_INCLUDE_PATH, null);
		} else {
			$data['log'] = '';
		}
		// log //
		
		// Settintg //
		$data['text_mail'] = $this->language->get('text_mail');
		$data['text_smtp'] = $this->language->get('text_smtp');
		$data['entry_protocol'] = $this->language->get('entry_mail_protocol');
		$data['entry_parameter'] = $this->language->get('entry_mail_parameter');
		$data['entry_smtp_hostname'] = $this->language->get('entry_smtp_host');
		$data['entry_smtp_username'] = $this->language->get('entry_smtp_username');
		$data['entry_smtp_password'] = $this->language->get('entry_smtp_password');
		$data['entry_smtp_port'] = $this->language->get('entry_smtp_port');
		$data['entry_smtp_timeout'] = $this->language->get('entry_smtp_timeout');
		$data['entry_count_message'] = $this->language->get('entry_mail_count_message');
		$data['entry_sleep_time'] = $this->language->get('entry_mail_sleep_time');
		$data['entry_from'] = $this->language->get('entry_mail_from');
		$data['entry_product_currency'] = $this->language->get('entry_product_currency');
		$data['help_protocol'] = $this->language->get('help_protocol');
		$data['help_from'] = $this->language->get('help_from');
		$data['help_parameter'] = $this->language->get('help_parameter');
		
		$this->load->model('localisation/currency');
		$data['currencies'] = $this->model_localisation_currency->getCurrencies();
		
		if (isset($this->request->post['contacts_protocol'])) {
			$data['contacts_protocol'] = $this->request->post['contacts_protocol'];
		} elseif ($this->config->get('contacts_protocol')) {
			$data['contacts_protocol'] = $this->config->get('contacts_protocol');
		} else {
			$data['contacts_protocol'] = 'mail';
		}
		
		if (isset($this->request->post['contacts_from'])) {
			$data['contacts_from'] = $this->request->post['contacts_from'];
		} elseif ($this->config->get('contacts_from')) {
			$data['contacts_from'] = $this->config->get('contacts_from');
		} else {
			$data['contacts_from'] = $this->config->get('config_email');
		}
		
		if (isset($this->request->post['contacts_parameter'])) {
			$data['contacts_parameter'] = $this->request->post['contacts_parameter'];
		} elseif ($this->config->get('contacts_parameter')) {
			$data['contacts_parameter'] = $this->config->get('contacts_parameter');
		} else {
			$data['contacts_parameter'] = '';
		}

		if (isset($this->request->post['contacts_smtp_hostname'])) {
			$data['contacts_smtp_hostname'] = $this->request->post['contacts_smtp_hostname'];
		} elseif ($this->config->get('contacts_smtp_hostname')) {
			$data['contacts_smtp_hostname'] = $this->config->get('contacts_smtp_hostname');
		} else {
			$data['contacts_smtp_hostname'] = '';
		}

		if (isset($this->request->post['contacts_smtp_username'])) {
			$data['contacts_smtp_username'] = $this->request->post['contacts_smtp_username'];
		} elseif ($this->config->get('contacts_smtp_username')) {
			$data['contacts_smtp_username'] = $this->config->get('contacts_smtp_username');
		} else {
			$data['contacts_smtp_username'] = '';
		}
		
		if (isset($this->request->post['contacts_smtp_password'])) {
			$data['contacts_smtp_password'] = $this->request->post['contacts_smtp_password'];
		} elseif ($this->config->get('contacts_smtp_password')) {
			$data['contacts_smtp_password'] = $this->config->get('contacts_smtp_password');
		} else {
			$data['contacts_smtp_password'] = '';
		}
		
		if (isset($this->request->post['contacts_smtp_port'])) {
			$data['contacts_smtp_port'] = $this->request->post['contacts_smtp_port'];
		} elseif ($this->config->get('contacts_smtp_port')) {
			$data['contacts_smtp_port'] = $this->config->get('contacts_smtp_port');
		} else {
			$data['contacts_smtp_port'] = 25;
		}
		
		if (isset($this->request->post['contacts_smtp_timeout'])) {
			$data['contacts_smtp_timeout'] = $this->request->post['contacts_smtp_timeout'];
		} elseif ($this->config->get('contacts_smtp_timeout')) {
			$data['contacts_smtp_timeout'] = $this->config->get('contacts_smtp_timeout');
		} else {
			$data['contacts_smtp_timeout'] = 5;	
		}
		
		if (isset($this->request->post['contacts_count_message'])) {
			$data['contacts_count_message'] = $this->request->post['contacts_count_message'];
		} elseif ($this->config->get('contacts_count_message')) {
			$data['contacts_count_message'] = $this->config->get('contacts_count_message');
		} else {
			$data['contacts_count_message'] = 3;
		}
		
		if (isset($this->request->post['contacts_sleep_time'])) {
			$data['contacts_sleep_time'] = $this->request->post['contacts_sleep_time'];
		} elseif ($this->config->get('contacts_sleep_time')) {
			$data['contacts_sleep_time'] = $this->config->get('contacts_sleep_time');
		} else {
			$data['contacts_sleep_time'] = 7;
		}
		
		if (isset($this->request->post['contacts_product_currency'])) {
			$data['contacts_product_currency'] = $this->request->post['contacts_product_currency'];
		} elseif ($this->config->get('contacts_product_currency')) {
			$data['contacts_product_currency'] = $this->config->get('contacts_product_currency');
		} else {
			$data['contacts_product_currency'] = $this->config->get('config_currency');
		}
		// Settintg //

		$this->cache->delete('contacts');

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('marketing/contacts.tpl', $data));
	}
	
	public function send() {
		$this->load->language('marketing/contacts');
		$contacts_log = new Log('contacts.log');
		require_once(DIR_SYSTEM . 'library/mail_cs.php');
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if (!$this->user->hasPermission('modify', 'marketing/contacts')) {
				$json['error']['warning'] = $this->language->get('error_permission');
			}
					
			if (!$this->request->post['subject']) {
				$json['error']['subject'] = $this->language->get('error_subject');
			}
	
			if (!$this->request->post['message']) {
				$json['error']['message'] = $this->language->get('error_message');
			}
			
			if (!$json) {
				$this->load->model('marketing/contacts');
			
				$store_info = $this->model_marketing_contacts->getShopStore($this->request->post['store_id']);			
				
				if ($store_info) {
					$store_name = $store_info['name'];
				} else {
					$store_name = $this->config->get('config_name');
				}
	
				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$this->cache->delete('contacts');
					$page = 1;
					$contacts_log->write($this->language->get('start_send'));
				}
				
				if (($this->config->get('contacts_count_message')) && ($this->config->get('contacts_count_message') > 0)) {
					$contacts_count_message = $this->config->get('contacts_count_message');
				} else {
					$contacts_count_message = 3;
				}
				
				if (($this->config->get('contacts_sleep_time')) && ($this->config->get('contacts_sleep_time') > 0)) {
					$contacts_sleep_time = $this->config->get('contacts_sleep_time');
				} else {
					$contacts_sleep_time = 7;
				}
				
				if ($this->request->post['set_region']) {
					$set_region = $this->request->post['set_region'];
				} else {
					$set_region = false;
				}
				
				if ($this->request->post['country_id']) {
					$country_id = $this->request->post['country_id'];
				} else {
					$country_id = false;
				}
				
				if ($this->request->post['zone_id']) {
					$zone_id = $this->request->post['zone_id'];
				} else {
					$zone_id = false;
				}
				
				if ($this->request->post['set_unsubscribe']) {
					$set_unsubscribe = $this->request->post['set_unsubscribe'];
				} else {
					$set_unsubscribe = false;
				}
				
				$shop_country = $this->model_marketing_contacts->getCountry($this->config->get('config_country_id'));
				$shop_zone = $this->model_marketing_contacts->getZone($this->config->get('config_zone_id'));
				
				if ($this->request->post['special']) {
					if ($this->request->post['special_limit'] && ($this->request->post['special_limit'] > 0)) {
						$special_limit = $this->request->post['special_limit'];
					} else {
						$special_limit = 4;
					}
					$special_products = $this->model_marketing_contacts->getSpecialsProducts($special_limit);

					if ($special_products) {
					
						if ($this->request->post['special_title']) {
							$sdata['title'] = $this->request->post['special_title'];
						} else {
							$sdata['title'] = $this->language->get('special_title');
						}
					
						$sdata['products'] = $this->getMailProducts($special_products);

						$special = $this->load->view('mail/contacts_products.tpl', $sdata);
					
					} else {
						$special = '';
					}

				} else {
					$special = '';
				}
				
				if ($this->request->post['bestseller']) {
					if ($this->request->post['bestseller_limit'] && ($this->request->post['bestseller_limit'] > 0)) {
						$bestseller_limit = $this->request->post['bestseller_limit'];
					} else {
						$bestseller_limit = 4;
					}
					$bestseller_products = $this->model_marketing_contacts->getBestsellerProducts($bestseller_limit);
					
					if ($bestseller_products) {
						
						if ($this->request->post['bestseller_title']) {
							$bdata['title'] = $this->request->post['bestseller_title'];
						} else {
							$bdata['title'] = $this->language->get('bestseller_title');
						}
					
						$bdata['products'] = $this->getMailProducts($bestseller_products);
					
						$bestseller = $this->load->view('mail/contacts_products.tpl', $bdata);
					
					} else {
						$bestseller = '';
					}
					
				} else {
					$bestseller = '';
				}
				
				if ($this->request->post['latest']) {
					if ($this->request->post['latest_limit'] && ($this->request->post['latest_limit'] > 0)) {
						$latest_limit = $this->request->post['latest_limit'];
					} else {
						$latest_limit = 4;
					}
					$latest_products = $this->model_marketing_contacts->getLatestProducts($latest_limit);
					
					if ($latest_products) {
						
						if ($this->request->post['latest_title']) {
							$ldata['title'] = $this->request->post['latest_title'];
						} else {
							$ldata['title'] = $this->language->get('latest_title');
						}
					
						$ldata['products'] = $this->getMailProducts($latest_products);
					
						$latest = $this->load->view('mail/contacts_products.tpl', $ldata);
					
					} else {
						$latest = '';
					}

				} else {
					$latest = '';
				}
				
				if ($this->request->post['featured']) {
					if ($this->request->post['featured_limit'] && ($this->request->post['featured_limit'] > 0)) {
						$featured_limit = $this->request->post['featured_limit'];
					} else {
						$featured_limit = 4;
					}
					$featured_products = $this->model_marketing_contacts->getFeaturedProducts($featured_limit);

					if ($featured_products) {
						
						if ($this->request->post['featured_title']) {
							$fdata['title'] = $this->request->post['featured_title'];
						} else {
							$fdata['title'] = $this->language->get('featured_title');
						}
					
						$fdata['products'] = $this->getMailProducts($featured_products);
					
						$featured = $this->load->view('mail/contacts_products.tpl', $fdata);
					
					} else {
						$featured = '';
					}
					
				} else {
					$featured = '';
				}
				
				if (($this->request->post['selectproducts']) && isset($this->request->post['selproducts']) && is_array($this->request->post['selproducts'])) {

					$selproducts_products = $this->model_marketing_contacts->getSelectProducts($this->request->post['selproducts']);

					if ($selproducts_products) {
						
						if ($this->request->post['selproducts_title']) {
							$sldata['title'] = $this->request->post['selproducts_title'];
						} else {
							$sldata['title'] = $this->language->get('selproducts_title');
						}
					
						$sldata['products'] = $this->getMailProducts($selproducts_products);
					
						$selproducts = $this->load->view('mail/contacts_products.tpl', $sldata);
					
					} else {
						$selproducts = '';
					}
					
				} else {
					$selproducts = '';
				}
				
				$category_products = '';

				$email_total = 0;
				$emails = array();
				$emails_cache = array();
				
				$emails_cache = $this->cache->get('contacts.emails');
				
				if ($emails_cache) {

					$emails = $emails_cache['emails'];
					$email_total = $emails_cache['email_total'];

				} else {
				
				  switch ($this->request->post['to']) {
					case 'newsletter':
						$customer_data = array(
							'filter_newsletter' => 1,
							'filter_country_id' => $country_id,
							'filter_zone_id'    => $zone_id
						);

						$results = $this->model_marketing_contacts->getEmailCustomers($customer_data);
					
						foreach ($results as $result) {
								$emails[$result['email']] = array(
									'customer_id'   => $result['customer_id'],
									'firstname'     => mb_convert_case($result['firstname'], MB_CASE_TITLE, 'UTF-8'),
									'lastname'      => mb_convert_case($result['lastname'], MB_CASE_TITLE, 'UTF-8'),
									'country'       => $result['country'],
									'zone'          => $result['zone'],
									'date_added'    => $result['date_added']
								);
						}
						
						$email_total = count($emails);
						$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));
						
						$emails_cache['emails'] = $emails;
						$emails_cache['email_total'] = $email_total;
						$this->cache->set('contacts.emails', $emails_cache);

						break;
					case 'customer_all':
						$customer_data = array(
						    'filter_country_id' => $country_id,
							'filter_zone_id'    => $zone_id
						);

						$results = $this->model_marketing_contacts->getEmailCustomers($customer_data);
				
						foreach ($results as $result) {
								$emails[$result['email']] = array(
									'customer_id'   => $result['customer_id'],
									'firstname'     => mb_convert_case($result['firstname'], MB_CASE_TITLE, 'UTF-8'),
									'lastname'      => mb_convert_case($result['lastname'], MB_CASE_TITLE, 'UTF-8'),
									'country'       => $result['country'],
									'zone'          => $result['zone'],
									'date_added'    => $result['date_added']
								);
						}
						
						$email_total = count($emails);
						$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));
						
						$emails_cache['emails'] = $emails;
						$emails_cache['email_total'] = $email_total;
						$this->cache->set('contacts.emails', $emails_cache);

						break;
					case 'client_all':
						$customer_data = array(
							'filter_country_id'        => $country_id,
							'filter_zone_id'           => $zone_id
						);
							
						$results = $this->model_marketing_contacts->getEmailsByOrdereds($customer_data);
				
						foreach ($results as $result) {
								$emails[$result['email']] = array(
									'customer_id'   => $result['customer_id'],
									'firstname'     => mb_convert_case($result['firstname'], MB_CASE_TITLE, 'UTF-8'),
									'lastname'      => mb_convert_case($result['lastname'], MB_CASE_TITLE, 'UTF-8'),
									'country'       => $result['country'],
									'zone'          => $result['zone'],
									'date_added'    => $result['date_added']
								);
						}
						
						$email_total = count($emails);
						$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));

						$emails_cache['emails'] = $emails;
						$emails_cache['email_total'] = $email_total;
						$this->cache->set('contacts.emails', $emails_cache);

						break;
					case 'customer_group':
						$customer_data = array(
							'filter_customer_group_id' => $this->request->post['customer_group_id'],
							'filter_country_id'        => $country_id,
							'filter_zone_id'           => $zone_id
						);

						$results = $this->model_marketing_contacts->getEmailCustomers($customer_data);
				
						foreach ($results as $result) {
								$emails[$result['email']] = array(
									'customer_id'   => $result['customer_id'],
									'firstname'     => mb_convert_case($result['firstname'], MB_CASE_TITLE, 'UTF-8'),
									'lastname'      => mb_convert_case($result['lastname'], MB_CASE_TITLE, 'UTF-8'),
									'country'       => $result['country'],
									'zone'          => $result['zone'],
									'date_added'    => $result['date_added']
								);
						}
						
						$email_total = count($emails);
						$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));

						$emails_cache['emails'] = $emails;
						$emails_cache['email_total'] = $email_total;
						$this->cache->set('contacts.emails', $emails_cache);

						break;
					case 'customer':
						if (!empty($this->request->post['customer'])) {
							foreach ($this->request->post['customer'] as $customer_id) {
								$customer_info = $this->model_marketing_contacts->getCustomerData($customer_id);
								
								if ($customer_info) {
									$emails[$customer_info['email']] = array(
										'customer_id'   => $customer_info['customer_id'],
										'firstname'     => mb_convert_case($customer_info['firstname'], MB_CASE_TITLE, 'UTF-8'),
										'lastname'      => mb_convert_case($customer_info['lastname'], MB_CASE_TITLE, 'UTF-8'),
										'country'       => $customer_info['country'],
										'zone'          => $customer_info['zone'],
										'date_added'    => $customer_info['date_added']
									);
								}
							}

							$email_total = count($emails);
							$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));
							
							$emails_cache['emails'] = $emails;
							$emails_cache['email_total'] = $email_total;
							$this->cache->set('contacts.emails', $emails_cache);
						}
						break;
					case 'affiliate_all':
						$affiliate_data = array(
							'filter_country_id'   => $country_id,
							'filter_zone_id'      => $zone_id
						);
						
						$results = $this->model_marketing_contacts->getEmailAffiliates($affiliate_data);
				
						foreach ($results as $result) {
								$emails[$result['email']] = array(
										'customer_id'   => '',
										'firstname'     => mb_convert_case($result['firstname'], MB_CASE_TITLE, 'UTF-8'),
										'lastname'      => mb_convert_case($result['lastname'], MB_CASE_TITLE, 'UTF-8'),
										'country'       => $result['country'],
										'zone'          => $result['zone'],
										'date_added'    => ''
								);
						}
						
						$email_total = count($emails);
						$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));

						$emails_cache['emails'] = $emails;
						$emails_cache['email_total'] = $email_total;
						$this->cache->set('contacts.emails', $emails_cache);

						break;
					case 'affiliate':
						if (!empty($this->request->post['affiliate'])) {
							foreach ($this->request->post['affiliate'] as $affiliate_id) {
								$affiliate_info = $this->model_marketing_contacts->getAffiliateData($affiliate_id);
								
								if ($affiliate_info) {
									$emails[$affiliate_info['email']] = array(
										'customer_id'   => '',
										'firstname'     => mb_convert_case($affiliate_info['firstname'], MB_CASE_TITLE, 'UTF-8'),
										'lastname'      => mb_convert_case($affiliate_info['lastname'], MB_CASE_TITLE, 'UTF-8'),
										'country'       => $affiliate_info['country'],
										'zone'          => $affiliate_info['zone'],
										'date_added'    => ''
									);
								}
							}
							
							$email_total = count($emails);
							$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));
							
							$emails_cache['emails'] = $emails;
							$emails_cache['email_total'] = $email_total;
							$this->cache->set('contacts.emails', $emails_cache);

						}
						break;
					case 'product':
						if (!empty($this->request->post['product'])) {
							
							$data = array(
								'filter_products'     => $this->request->post['product'],
								'filter_country_id'   => $country_id,
								'filter_zone_id'      => $zone_id
							);
							
							$results = $this->model_marketing_contacts->getEmailsByOrdereds($data);

							foreach ($results as $result) {
								$emails[$result['email']] = array(
									'customer_id'   => $result['customer_id'],
									'firstname'     => mb_convert_case($result['firstname'], MB_CASE_TITLE, 'UTF-8'),
									'lastname'      => mb_convert_case($result['lastname'], MB_CASE_TITLE, 'UTF-8'),
									'country'       => $result['country'],
									'zone'          => $result['zone'],
									'date_added'    => $result['date_added']
								);
							}
							
							$email_total = count($emails);
							$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));

							$emails_cache['emails'] = $emails;
							$emails_cache['email_total'] = $email_total;
							$this->cache->set('contacts.emails', $emails_cache);

						}
						break;
					case 'manual':
						if (!empty($this->request->post['manual'])) {
							$post_manuals = explode(',', preg_replace('/\s/', '', $this->request->post['manual']));
							$manuals = array_unique($post_manuals);
							foreach ($manuals as $manual) {
								if (trim($manual) != '') {
									$results[] = array(
										'email'   => $manual
									);
								}
							}
							foreach ($results as $result) {
								$emails[$result['email']] = array(
										'customer_id'   => '',
										'firstname'     => '',
										'lastname'      => '',
										'country'       => '',
										'zone'          => '',
										'date_added'    => ''
								);
							}

							$email_total = count($emails);
							$contacts_log->write(sprintf($this->language->get('count_email'), $email_total));
							
							$emails_cache['emails'] = $emails;
							$emails_cache['email_total'] = $email_total;
							$this->cache->set('contacts.emails', $emails_cache);

						}
						break;
				  }
				}

				if ($emails) {
					sleep($contacts_sleep_time);
					$start = ($page - 1) * $contacts_count_message;
					$end = $start + $contacts_count_message;
					$lastsend = 0;
					
					if ($end < $email_total) {
						$json['success'] = sprintf($this->language->get('text_sent'), $end, $email_total);
					} else {
						$json['success'] = $this->language->get('text_success');
						$lastsend = 1;
					}
						
					if ($end < $email_total) {
						$json['next'] = str_replace('&amp;', '&', $this->url->link('marketing/contacts/send', 'token=' . $this->session->data['token'] . '&page=' . ($page + 1), 'SSL'));
					} else {
						$json['next'] = '';
					}
					
					if (($this->config->get('contacts_from')) && ($this->config->get('contacts_from') != '')) {
						$senders = explode(',', preg_replace('/\s/', '', $this->config->get('contacts_from')));
					} else {
						$senders = array($this->config->get('config_email'));
					}
					
					$semails = array_slice($emails, $start, $contacts_count_message);

					foreach ($semails as $email => $customer) {
						if ($this->checkValidEmail($email)) {
							
						if ($customer['firstname']) {
							$firstname = $customer['firstname'];
						} else {
							$firstname = '';
						}
						if ($customer['lastname']) {
							$lastname = $customer['lastname'];
						} else {
							$lastname = '';
						}
						if ($customer['firstname'] && $customer['lastname']) {
							$name = $customer['firstname'] . ' ' . $customer['lastname'];
						} elseif ($customer['firstname'] && !$customer['lastname']) {
							$name = $customer['firstname'];
						} elseif (!$customer['firstname'] && $customer['lastname']) {
							$name = $customer['lastname'];
						} else {
							$name = $this->language->get('text_client');
						}
						if ($customer['country']) {
							$country = $customer['country'];
						} else {
							$country = $shop_country;
						}
						if ($customer['zone']) {
							$zone = $customer['zone'];
						} else {
							$zone = $shop_zone;
						}
						$shopname = $store_name;
						$shopurl = '<a href="' . HTTP_CATALOG . '">' . $store_name . '</a>';
						
						$tegi_subject = array('{firstname}','{lastname}','{name}','{country}','{zone}','{shopname}');
						$tegi_message = array('{firstname}','{lastname}','{name}','{country}','{zone}','{shopname}','{shopurl}','{special}','{bestseller}','{latest}','{featured}','{selproducts}','{category_products}');
						
						$replace_subject = array($firstname, $lastname, $name, $country, $zone, $shopname);
						$replace_message = array($firstname, $lastname, $name, $country, $zone, $shopname, $shopurl, $special, $bestseller, $latest, $featured, $selproducts, $category_products);
						
						$orig_subject = $this->request->post['subject'];
						$orig_message = $this->request->post['message'];
					
						$subject_to_send = str_replace($tegi_subject, $replace_subject, $orig_subject);
						$message_to_send = str_replace($tegi_message, $replace_message, $orig_message);

						$message  = '<html dir="ltr" lang="en">' . "\n";
						$message .= '  <head>' . "\n";
						$message .= '    <title>' . $subject_to_send . '</title>' . "\n";
						$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
						$message .= '  </head>' . "\n";
						$message .= '  <body>' . html_entity_decode($message_to_send, ENT_QUOTES, 'UTF-8') . "\n\n";
						
						if ($set_unsubscribe && $customer['customer_id']) {
							$unsubscribe_url = HTTP_CATALOG . 'index.php?route=account/success&customer_id=' . $customer['customer_id'] . '&check=' . preg_replace("/\D/","",$customer['date_added']);
							$message .= '    <table style="width:100%;background:#F5F2F2;font-size:12px;"><tr><td style="padding:5px;text-align:center;">' . sprintf($this->language->get('text_unsubscribe'), $unsubscribe_url) . '</td></tr></table>' . "\n";
						}

						$message .= '  </body>' . "\n";
						$message .= '</html>' . "\n";

						if (count($senders) > 1) {
							$number = mt_rand(0, count($senders) - 1);
							$sender = $senders[$number];
						} else {
							$sender = $senders[0];
						}

						$mail = new Mail_CS();
						$mail->protocol = $this->config->get('contacts_protocol');
						$mail->parameter = $this->config->get('contacts_parameter');
						$mail->smtp_hostname = $this->config->get('contacts_smtp_hostname');
						$mail->smtp_username = $this->config->get('contacts_smtp_username');
						$mail->smtp_password = html_entity_decode($this->config->get('contacts_smtp_password'), ENT_QUOTES, 'UTF-8');
						$mail->smtp_port = $this->config->get('contacts_smtp_port');
						$mail->smtp_timeout = $this->config->get('contacts_smtp_timeout');
						
						$mail->setTo($email);
						$mail->setFrom($sender);
						$mail->setSender(html_entity_decode($store_name, ENT_QUOTES, 'UTF-8'));
						if ($set_unsubscribe && $customer['customer_id']) {
							$mail->setUnsubscribe($unsubscribe_url);
						}
						$mail->setSubject(html_entity_decode($subject_to_send, ENT_QUOTES, 'UTF-8'));
						$mail->setHtml($message);
						$contacts_log->write($this->language->get('send_email') . $email);
						$mail->send();
					
						} else {
							$contacts_log->write($this->language->get('bad_email') . $email);
						}
					}
				
					if($lastsend == 1) {
						$this->cache->delete('contacts');
						$contacts_log->write($this->language->get('end_send'));
					}
				}
			}
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function checkValidEmail($email) {
		$pattern = '/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i';
		if (preg_match($pattern, $email)) {
				return true;
		} else {
				return false;
		}
	}
	
	public function updatelog() {
		$json = array();
		$this->load->language('marketing/contacts');
		
		if ($this->user->hasPermission('modify', 'marketing/contacts')) {
		
			$file = DIR_LOGS . 'contacts.log';
			
			if (file_exists($file)) {
				$json['log'] = file_get_contents($file, FILE_USE_INCLUDE_PATH, null);
			} else {
				$json['log'] = '';
			}
		
			$json['success'] = $this->language->get('text_update_log');
		
		} else {

			$json['error'] = $this->language->get('error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function clearlog() {
		$json = array();
		$this->load->language('marketing/contacts');
		
		if ($this->user->hasPermission('modify', 'marketing/contacts')) {
		
			$file = DIR_LOGS . 'contacts.log';
			$handle = fopen($file, 'w+');
			fclose($handle); 			
		
			$json['success'] = $this->language->get('text_delete_log');
		
		} else {

			$json['error'] = $this->language->get('error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getMailProducts($results) {
		$products = array();
		
		if ($this->config->get('contacts_product_currency')) {
			$currency = $this->config->get('contacts_product_currency');
		} else {
			$currency = $this->config->get('config_currency');
		}
		
		$this->load->model('tool/image');
		
		foreach ($results as $result) {
			if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			} else {
				$image = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			}

			$price = $this->currency->format($result['price'], $currency);

			if ((float)$result['special']) {
				$special = $this->currency->format($result['special'], $currency);
				$discount = floor((($result['price']-$result['special'])/$result['price'])*100);
			} else {
				$special = false;
				$discount = false;
			}
			
			if ($this->config->get('config_review_status')) {
				$rating = (int)$result['rating'];
			} else {
				$rating = false;
			}

			$products[] = array(
				'product_id' => $result['product_id'],
				'thumb'   	 => $image,
				'discount'   => isset($discount)?'-'.$discount.'%':'',
				'name'    	 => $result['name'],
				'price'   	 => $price,
				'model'    	 => $result['model'],
				'sku'    	 => $result['sku'],
				'special' 	 => $special,
				'rating'     => $rating,
				'href'    	 => str_replace(HTTP_SERVER, HTTP_CATALOG, $this->url->link('product/product', 'product_id=' . $result['product_id']))
			);
		}
			
		return $products;
	}

	public function gettemplate() {
		$json = array();

		$this->load->model('marketing/contacts');
		$this->load->language('marketing/contacts');

		if (isset($this->request->get['template_id'])) {

			$template_id = $this->request->get['template_id'];
			$message_data = $this->model_marketing_contacts->getTemplate($template_id);

			$json['template_id'] = $message_data['template_id'];
			$json['name'] = $message_data['name'];
			$json['message'] = html_entity_decode($message_data['message'], ENT_QUOTES, 'UTF-8');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function deltemplate() {
		$json = array();
		
		$this->load->model('marketing/contacts');
		$this->load->language('marketing/contacts');
		
		if ($this->user->hasPermission('modify', 'marketing/contacts')) {
			
			if (!isset($this->request->get['template_id'])) {
			
				$json['error'] = $this->language->get('error_del_template');
			
			} else {

				$this->model_marketing_contacts->delTemplate($this->request->get['template_id']);
				$json['success'] = $this->language->get('text_delete_template');
			}

		} else {

			$json['error'] = $this->language->get('error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function edittemplate() {
		$json = array();
		
		$this->load->model('marketing/contacts');
		$this->load->language('marketing/contacts');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			if ((utf8_strlen($this->request->post['temp_name']) < 1) || (utf8_strlen($this->request->post['temp_name']) > 255)) {
			
				$json['error'] = $this->language->get('error_name_template');
			
			} else {
				
				if (!isset($this->request->get['template_id'])) {
			
					$json['error'] = $this->language->get('error_save_template');
			
				} else {
					$data = array(
						'name'    => $this->request->post['temp_name'],
						'message' => $this->request->post['message2']
					);
					$template_id = $this->request->get['template_id'];
					$this->model_marketing_contacts->editTemplate($template_id, $data);
					$json['success'] = $this->language->get('text_success_template');
				}
			}

		} else {

			$json['error'] = $this->language->get('error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function addtemplate() {
		$json = array();
		
		$this->load->model('marketing/contacts');
		$this->load->language('marketing/contacts');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			if ((utf8_strlen($this->request->post['new_temp_name']) < 1) || (utf8_strlen($this->request->post['new_temp_name']) > 255)) {
			
				$json['error'] = $this->language->get('error_name_template');
			
			} else {
				$data = array(
					'name'    => $this->request->post['new_temp_name'],
					'message' => $this->request->post['message']
				);
				$json['template_id'] = $this->model_marketing_contacts->addTemplate($data);
				$json['success'] = $this->language->get('text_success_template');
			}

		} else {

			$json['error'] = $this->language->get('error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function addnewtemplate() {
		$json = array();
		
		$this->load->model('marketing/contacts');
		$this->load->language('marketing/contacts');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			if ((utf8_strlen($this->request->post['temp_name']) < 1) || (utf8_strlen($this->request->post['temp_name']) > 255)) {
			
				$json['error'] = $this->language->get('error_name_template');
			
			} else {
				$data = array(
					'name'    => $this->request->post['temp_name'],
					'message' => $this->request->post['message2']
				);
				$json['template_id'] = $this->model_marketing_contacts->addTemplate($data);
				$json['success'] = $this->language->get('text_success_template');
			}

		} else {

			$json['error'] = $this->language->get('error_permission');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function savesetting() {
		$json = array();
		
		$this->load->model('marketing/contacts');
		$this->load->language('marketing/contacts');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$set_data = $this->request->post;
			$save_ok = $this->model_marketing_contacts->saveSetting($set_data);
			
			if($save_ok) {
				$json['success'] = $this->language->get('text_success_setting');
			} else {
				$json['error'] = $this->language->get('error_save_setting');
			}
			
		} else {
			$json['error'] = $this->language->get('error_save_setting');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	public function getlic() {
		$json = array();

		$this->load->model('marketing/contacts');
		$this->load->language('marketing/contacts');
		
		if ($this->user->hasPermission('modify', 'marketing/contacts')) {
			$lic_ok = $this->model_marketing_contacts->getLicense();
			
			if ($lic_ok) {
				$json['success'] = $this->language->get('text_ok_lic');
			} else {
				$json['error'] = $this->language->get('text_error_lic');
			}

		} else {
			$json['error'] = $this->language->get('text_error_lic');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
	protected function validate() {
		$this->load->language('marketing/contacts');
		
		if (!$this->user->hasPermission('modify', 'marketing/contacts')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	public function country() {
		$json = array();

		$this->load->model('marketing/contacts');

		$country_name = $this->model_marketing_contacts->getCountry($this->request->get['country_id']);

		if ($country_name) {
			$json = array(
				'name'   => $country_name,
				'zone'   => $this->model_marketing_contacts->getZonesCountryId($this->request->get['country_id']),
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
	
}
?>
