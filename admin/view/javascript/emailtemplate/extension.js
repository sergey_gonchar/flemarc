(function($){
	$(document).ready(function() {
		
		/**
		 * Form Filter
		 */		
		$('input[name=filter_type]').on('change', function() {
			var $el = $('#form-emailtemplate');
			var url = 'index.php?route=module/emailtemplate&token=' + $.getUrlParam('token') + '&filter_type=' + encodeURIComponent($(this).val());
						
			$el.find('.ajax-filter-type').addClass('ajax-loading').html('<i class="fa fa-spinner fa-spin fa-5x" style="color:#009afd"></i>')
			
			$(this).parent().addClass('active').siblings().removeClass('active');
			
			$.ajax({
				url: url,
				type: 'GET',
				dataType: 'html',
				success: function(html) {
					if(html){
						$el.find('.ajax-filter-type').removeClass('ajax-loading').html(
							$(html).find('.ajax-filter-type').html()
						);
					}
				}
			});
		});
		
	});		
})(jQuery);