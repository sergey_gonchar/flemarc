$(document).ready(function() {
	//Mostra botão para mostrar tudo no components well
	$('.well').each(function() {
		var element = $(this);
		var innerHeight = $(this).innerHeight();
		var initialHeight = element.css('height');
		element.css('min-height', '150px');

		var button = $('<a href="#show-all" class="button-show-all-items btn btn-primary btn-xs"><span class="fa fa-expand"></span></a>');
		
		var hasButton = false;

		//Quando houver mudanças no dom do elemento
		element.bind("DOMSubtreeModified", function() {
		    if (element[0].scrollHeight > innerHeight) {
		    	if (!hasButton) {
					hasButton = true;
					
					element.prepend(button);
				}
				
		    	button.show();
		    } else {
		    	button.hide();
		    }
		});

		element.on('mouseover', function () {
			if (!hasButton) {
				hasButton = true;

				element.prepend(button);
			}
		});

		var open = false;

		button.on('click', function () {
			if (open) {
				open = false;

				element.removeClass('show-all');
				button.find('span').addClass('fa-expand').removeClass('fa-compress');
			} else {
				open = true;

				element.addClass('show-all');
				button.find('span').addClass('fa-compress').removeClass('fa-expand');
			}

			return false;
		});
	});
});