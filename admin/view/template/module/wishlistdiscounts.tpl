<?php echo $header;?><?php echo $column_left; ?>
<div id="content">
   <div class="page-header">
    <div class="container-fluid">
      <h1><i class="fa fa-gift"></i>&nbsp;<?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
<?php echo (empty($data['WishlistDiscounts']['LicensedOn'])) ? base64_decode('ICAgIDxkaXYgY2xhc3M9ImFsZXJ0IGFsZXJ0LWRhbmdlciBmYWRlIGluIj4NCiAgICAgICAgPGJ1dHRvbiB0eXBlPSJidXR0b24iIGNsYXNzPSJjbG9zZSIgZGF0YS1kaXNtaXNzPSJhbGVydCIgYXJpYS1oaWRkZW49InRydWUiPsOXPC9idXR0b24+DQogICAgICAgIDxoND5XYXJuaW5nISBVbmxpY2Vuc2VkIHZlcnNpb24gb2YgdGhlIG1vZHVsZSE8L2g0Pg0KICAgICAgICA8cD5Zb3UgYXJlIHJ1bm5pbmcgYW4gdW5saWNlbnNlZCB2ZXJzaW9uIG9mIHRoaXMgbW9kdWxlISBZb3UgbmVlZCB0byBlbnRlciB5b3VyIGxpY2Vuc2UgY29kZSB0byBlbnN1cmUgcHJvcGVyIGZ1bmN0aW9uaW5nLCBhY2Nlc3MgdG8gc3VwcG9ydCBhbmQgdXBkYXRlcy48L3A+PGRpdiBzdHlsZT0iaGVpZ2h0OjVweDsiPjwvZGl2Pg0KICAgICAgICA8YSBjbGFzcz0iYnRuIGJ0bi1kYW5nZXIiIGhyZWY9ImphdmFzY3JpcHQ6dm9pZCgwKSIgb25jbGljaz0iJCgnYVtocmVmPSNpc2Vuc2Utc3VwcG9ydF0nKS50cmlnZ2VyKCdjbGljaycpIj5FbnRlciB5b3VyIGxpY2Vuc2UgY29kZTwvYT4NCiAgICA8L2Rpdj4=') : '' ?>
   <?php if ($error_warning) { ?>
            <div class="alert alert-danger autoSlideUp"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
             <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success autoSlideUp"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <script>$('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
 		<?php } ?>    
 	<div class="panel panel-default">
        <div class="panel-heading">
                     <h3 class="panel-title"><i class="fa fa-list"></i>&nbsp;<span style="vertical-align:middle;font-weight:bold;">Module Settings</span></h3>
            <div class="storeSwitcherWidget">
            	<div class="form-group">
                	<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-pushpin"></span>&nbsp;<?php echo $store['name']; if($store['store_id'] == 0) echo $text_default; ?>&nbsp;<span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                	<ul class="dropdown-menu" role="menu">
                    	<?php foreach ($stores  as $st) { ?>
                    		<li><a href="index.php?route=module/wishlistdiscounts&store_id=<?php echo $st['store_id'];?>&token=<?php echo $token; ?>"><?php echo $st['name']; ?></a></li>
                    	<?php } ?> 
                	</ul>
            	</div>
            </div>
        </div>
        <div class="panel-body">
      		<form method="post" enctype="multipart/form-data" id="form">
                <input type="hidden" name="store_id" value="<?php echo $store['store_id']; ?>" />
                <input type="hidden" name="WishlistDiscounts_status" value="1" />
                    <div class="tabbable">

        <div class="tab-navigation">
          <ul class="nav nav-tabs mainMenuTabs">
            <li><a href="#settings" data-toggle="tab"><i class="icon-wrench"></i> Settings</a></li>
             <li class="dropdown">
              <a href="#"  data-toggle="dropdown" class="dropdown-toggle"><i class="icon-gift"></i>  Customer Wishlists<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#customers" data-toggle="tab"><i class="icon-list-alt"></i> Wishlists</a></li>
                <li><a href="#archived_wishlists" data-toggle="tab"><i class="icon-list-alt"></i> Archived Wishlists</a></li>
              </ul>
            </li>
            <li><a href="#template" data-toggle="tab"><i class="icon-envelope"></i> Mail</a></li>

            <li class="dropdown">
              <a href="#"  data-toggle="dropdown" class="dropdown-toggle"><i class="icon-gift"></i> Coupons<b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#givenCoupons" data-toggle="tab"/><i class="icon-gift"></i>Given Coupons</a></li>
                <li><a href="#usedCoupons" data-toggle="tab"/><i class="icon-ok"></i>Used Coupons</a></li>
              </ul>
            </li>

            <li><a href="#isense-support" data-toggle="tab"><i class="icon-share"></i> Support</a></li>
          </ul>
          <div class="tab-buttons">
            <button type="submit" class="btn btn-primary save-changes"><i class="icon-ok"></i> Save changes</button>
            <a onclick="location = '<?php echo $cancel; ?>'" class="btn" a>Cancel</a> 
          </div>
        </div>
        <div class="tab-content">
         
         <?php
            if (!function_exists('modification_vqmod')) {
                function modification_vqmod($file) {
                    if (class_exists('VQMod')) {
                        return VQMod::modCheck(modification($file), $file);
                    } else {
                        return modification($file);
                    }
                }
            }
            ?>
        	<div id="settings" class="tab-pane active">
              <?php require_once(DIR_APPLICATION.'view/template/module/wishlistdiscounts/tab_settings.php'); ?>                        
            </div>   
          <div id="customers" class="tab-pane"></div>
          <div id="archived_wishlists" class="tab-pane"></div>
          <div id="template" class="tab-pane">
            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/module/wishlistdiscounts/tab_template.php'); ?>
          </div>
          <div id="givenCoupons" class="tab-pane"></div>
          <div id="usedCoupons" class="tab-pane"></div>
          <div id="isense-support" class="tab-pane">
            <?php require_once modification_vqmod(DIR_APPLICATION.'view/template/module/wishlistdiscounts/tab_support.php'); ?>
          </div>
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.tabbable -->
    </form>
  </div>
</div>
<script>
var store_id = '<?php if(isset($store_id)) {echo $store_id; } else {echo "0"; } ?>';

  if (window.localStorage && window.localStorage['currentTab']) {
  	$('.mainMenuTabs a[href='+window.localStorage['currentTab']+']').trigger('click');  
  }
  if (window.localStorage && window.localStorage['currentSubTab']) {
  	$('a[href='+window.localStorage['currentSubTab']+']').trigger('click');  

  }

  $('.fadeInOnLoad').css('visibility','visible');

  $('.mainMenuTabs a[data-toggle="tab"]').click(function() {
  	if (window.localStorage) {
  		window.localStorage['currentTab'] = $(this).attr('href');
		}
  });
  $('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"])').click(function() {
  	if (window.localStorage) {
  		window.localStorage['currentSubTab'] = $(this).attr('href');
	 	}
  });
	function validateNumber(input, theEvent) { 
    var regex = /[0-9]|\./;
    if(!regex.test(input.val())) {
      input.css({'background':'#f2dede'});
			theEvent.preventDefault();
			$('.errorForm').show(200);

		}
  }

$(document).ready(refreshData());
  function refreshData(){
		$.ajax({
			 url: "index.php?route=module/wishlistdiscounts/currentWishlists&token=" + getURLVar('token') + '&store_id=' + store_id,
			 type: 'get',
			 dataType: 'html',
			 success: function(data) { 
				  $('#customers.tab-pane').html(data);
		  }
		});
	 $.ajax({
         url: "index.php?route=module/wishlistdiscounts/archivedWishlists&token=<?php echo $token; ?>" + '&store_id=' + store_id,
         type: 'get',
         dataType: 'html',
         success: function(data) { 
          $('#archived_wishlists.tab-pane').html(data);
         }
      });
      $.ajax({
         url: "index.php?route=module/wishlistdiscounts/givenCoupons&token=<?php echo $token; ?>" + '&store_id=' + store_id,
         type: 'get',
         dataType: 'html',
         success: function(data) { 
          $('#givenCoupons').html(data);
         }
      });
      $.ajax({
         url: "index.php?route=module/wishlistdiscounts/usedCoupons&token=<?php echo $token; ?>" + '&store_id=' + store_id,
         type: 'get',
         dataType: 'html',
         success: function(data) { 
          $('#usedCoupons').html(data);
         }
      });
    }
</script> 
<?php echo $footer; ?>