<table class="table">
	<tr>
        <td class="col-xs-2"><h5><strong><span class="required">* </span><?php echo $entry_code; ?></strong></h5>
        </td>
    	<td>
          <div class="col-xs-3">
            <select name="WishlistDiscounts[Enabled]" class="WishlistDiscountsEnabled form-control">
                <option value="yes" <?php echo (!empty($data['WishlistDiscounts']['Enabled']) && $data['WishlistDiscounts']['Enabled'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
                <option value="no"  <?php echo (empty($data['WishlistDiscounts']['Enabled']) || $data['WishlistDiscounts']['Enabled']== 'no') ? 'selected=selected' : '' ?>>Disabled</option>
             </select>
           </div>
      	</td>
  </tr>
<tr>
        <td class="col-xs-2">
        	<h5><strong><span class="required">* </span><?php echo $admin_notification; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?php echo str_replace('{email}', $email, $admin_notification_help); ?></span>
        </td>
    	<td>
          <div class="col-xs-3">
            <select name="WishlistDiscounts[admin_notification]" class="form-control" >
                <option value="yes" <?php echo (!empty($data['WishlistDiscounts']['admin_notification']) && $data['WishlistDiscounts']['admin_notification'] == 'yes') ? 'selected=selected' : '' ?>>Enabled</option>
                <option value="no"  <?php echo (empty($data['WishlistDiscounts']['admin_notification']) || $data['WishlistDiscounts']['admin_notification'] == 'no') ? 'selected=selected' : '' ?>>Disabled</option>
              </select>
           </div>
        </td>
  </tr>
  <tr>
</table>
