	<div class="pull-left" style="width:50%;">
		<h4><strong><?php echo $wishlist_heading; ?></strong></h4>
	</div>
	<div class="pull-right">
	 	<a class="btn btn-success sendToSelected" style="display:none;">
			<?php echo $button_send_discounts_to_selected; ?>
		</a>
		<a class="btn btn-success sendToAll" >
			<?php echo $button_send_discount_to_all; ?>
		</a>
	</div>
	
	<div class="clearfix"></div>
	
	<table class="table list">
		<thead>
			<tr>
				<td><input type="checkbox" name="selectAllCustomers"/></td>
				<td class="left">
					<?php if ($sort == 'name') { ?>
						<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer_name; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_name; ?>"><?php echo $column_customer_name; ?></a>
					<?php } ?>
				</td>

				<td class="left">
					<?php if ($sort == 'email') { ?>
						<a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer_email; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_email; ?>"><?php echo $column_customer_email; ?></a>
					<?php } ?>
				</td>

				<td class="left">
					<?php if ($sort == 'date_added') { ?>
						<a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
					<?php } ?>
				</td>

				<td class="left" width="105px"><?php echo $column_customer_wishlist;  ?></td>	
			</tr>
		</thead>
		 
		<tbody>
			<?php if(!empty($customers)) { ?>
			 <?php foreach($customers as $customer) { ?>
			   <tr id="<?php echo $customer['customer_id'] ?>">
			     <td width="10px"><input type="checkbox" value="<?php echo $customer['customer_id']; ?>" id="customer<?php echo $customer['customer_id']; ?>"/></td>
			     <td><?php echo $customer['firstname'] .  " " . $customer['lastname']; ?></td>
			     <td><?php echo $customer['email']?></td>
			     <td><?php echo date('d.m.Y', strtotime($customer['date_added'])); ?></td>
			     <td><a id='<?php echo $customer['customer_id']; ?>' class="btn btn-info btn-mini showWishList"><?php echo $show_wishlist; ?></a></td>
			   </tr>
			 <?php }?>
			<?php } else { ?>
		      <tr><td class="center" colspan="5"><?php echo $text_no_results; ?></td></tr>
      <?php } ?>
		</tbody>
	</table>
	<div class="pagination"><?php echo $pagination; ?></div>
    
 <!--ShowWishlistModal -->
<div class="modal fade" id="show_wishlist_modal" tabindex="-1" role="dialog" aria-labelledby="show_wishlist_ModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Products in Wishlist</h4>
      </div>
      <div class="modal-body" id="show_wishlist_modal_body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>   
</div>

 <!--SendDiscountModal -->
<div class="modal fade" id="send_discount_modal" tabindex="-1" role="dialog" aria-labelledby="send_discount_ModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="cronModalLabel">Send discount to all</h4>
      </div>
      <div class="modal-body" id="discount_modal_body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>   
</div>

 <!--SendDiscountModalToSelected -->
<div class="modal fade" id="send_selected_discount_modal" tabindex="-1" role="dialog" aria-labelledby="send_selected_discount_modal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="cronModalLabel">Send discount to selected customers</h4>
      </div>
      <div class="modal-body" id="selected_discount_modal_body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>   
</div>

<script>
var store_id = '<?php if(isset($store_id)) {echo $store_id; } else {echo "0"; } ?>';

$(document).on('click','.showWishList', function(e){
	var cust_id = this.id;
	var cust_name = '';
	var modal = '';
	if($('#customers .table.list tr[id="' + this.id + '"] td:nth-child(2)').text()) { 
		cust_name = $('#customers .table.list tr[id="' + this.id + '"] td:nth-child(2)').text();
		modal = $('#customers #show_wishlist_modal'), modalBody = $('#customers #show_wishlist_modal .modal-body');
	} else { 
		cust_name = $('#archived_wishlists .table.list tr[id="' + this.id + '"] td:nth-child(2)').text();
		modal = $('#archived_wishlists #show_wishlist_modal'), modalBody = $('#archived_wishlists #show_wishlist_modal .modal-body');
	}
	
	
	modal
        .on('show.bs.modal', function () {  console.log('index.php?route=module/wishlistdiscounts/wishlist&customer_name='+ encodeURIComponent(cust_name) +'&customer_id='+cust_id+'&token=' + getURLVar('token') + '&store_id=' + store_id);
            modalBody.load('index.php?route=module/wishlistdiscounts/wishlist&customer_name='+ encodeURIComponent(cust_name) +'&customer_id='+cust_id+'&token=' + getURLVar('token') + '&store_id=' + store_id);
        })
        .modal();
});

$(document).on('click','.sendToAll', function(e){
	
	var selected_customer = '';
	
	if($(this).parent().parent().attr("id") == 'customers') {
		var modal = $('#customers #send_discount_modal'), modalBody = $('#customers #send_discount_modal .modal-body');
		selected_customer = '#customers input[id^=\'customer\']';
	} else {
		var modal = $('#archived_wishlists #send_discount_modal'), modalBody = $('#archived_wishlists #send_discount_modal .modal-body');
		selected_customer = '#archived_wishlists input[id^=\'customer\']';
	}
	
	if($(modal).length){
		modal
			.on('show.bs.modal', function () { 
				modalBody.load('index.php?route=module/wishlistdiscounts/mailForm&token=' + getURLVar('token') + '&sendToAllCustomers=' + 1 + '&store_id=' + store_id)
			})
			.modal();
		e.preventDefault();
	} else {
		alert("There are no customers with wishlist!");
	}
});

$(document).on('click','.sendToSelected', function(e){
	var customer=[];
	var selected_customer = '';
	if($(this).parent().parent().attr("id") == 'customers') {
		var modal = $('#customers #send_selected_discount_modal'), modalBody = $('#customers #send_selected_discount_modal .modal-body');
		selected_customer = '#customers input[type="checkbox"][id^="customer"]:checked';
	} else {
		var modal = $('#archived_wishlists #send_selected_discount_modal'), modalBody = $('#archived_wishlists #send_selected_discount_modal .modal-body');
		selected_customer = '#archived_wishlists input[type="checkbox"][id^="customer"]:checked';
	}

	$(selected_customer).each(function(){ customer.push($(this).val());});  
	
	modal
        .on('show.bs.modal', function () { 
            modalBody.load('index.php?route=module/wishlistdiscounts/mailForm&customers='+customer+'&token=' + getURLVar('token') + '&store_id=' + store_id)
        })
        .modal();
    e.preventDefault();
});

var sendToAll;
$('input[name="selectAllCustomers"]').on('click', function(e) { 
	$('input[id^=\'customer\']').attr('checked', this.checked);
	sendToAll= this.checked;
});


$('input[type="checkbox"][id^="customer"]').on('click', function(){
	if($('input[type="checkbox"][id^="customer"]:checked').length != 0) {
		$('.btn.btn-success.sendToAll').hide();
		$('.btn.btn-success.sendToSelected').show();
	} else {
		$('.btn.btn-success.sendToAll').show();
		$('.btn.btn-success.sendToSelected').hide();	
	}
});

$('input[type="checkbox"][name="selectAllCustomers"]').on('click', function(){
	if($('input[type="checkbox"][id^="customer"]:checked').length != 0) {
		$('.btn.btn-success.sendToAll').hide();
		$('.btn.btn-success.sendToSelected').show();	
	} else {
		$('.btn.btn-success.sendToAll').show();
		$('.btn.btn-success.sendToSelected').hide();
	}
});

	
$(document).ready(function(){
	$('#customers .pagination .links a').click(function(e){
		e.preventDefault();
		$.ajax({
			url: this.href,
			type: 'get',
			dataType: 'html',
			success: function(data) {				
				$('#customers.tab-pane').html(data);
			}
		});
	});
	
	$('#archived_wishlists .pagination .links a').click(function(e){
		e.preventDefault();
		$.ajax({
			url: this.href,
			type: 'get',
			dataType: 'html',
			success: function(data) {				
				$('#archived_wishlists.tab-pane').html(data);
			}
		});
	});
});

$(document).ready(function(){
	$('#customers table thead tr td a').click(function(e){
		e.preventDefault();
		$.ajax({
			url: this.href,
			type: 'get',
			dataType: 'html',
			success: function(data) {				
				$('#customers.tab-pane').html(data);
			}
		});
	});
	
	$('#archived_wishlists table thead tr td a').click(function(e){
		e.preventDefault();
		$.ajax({
			url: this.href,
			type: 'get',
			dataType: 'html',
			success: function(data) {				
				$('#archived_wishlists.tab-pane').html(data);
			}
		});
	});
});
</script>