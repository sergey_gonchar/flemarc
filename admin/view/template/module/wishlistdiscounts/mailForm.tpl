<div style="padding:10px">
	<form class="table mailForm">
        <?php  $languageCount = count($languages); ?>
		<table class="table form">
          <div class="tab-navigation">
            <ul class="nav nav-tabs mainMenuTabs">
            <?php $class="active"; foreach ($languages as $language) { ?>
              <li class="<?php  echo $class; ?>"><a href="#email_<?php echo $language['language_id'];?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>
            <?php  $class="";} ?>
          </div>
			<tr class="progressBarHolder" role="progressbar">
			 	<td colspan="2">
		  			<div id="progressBar"><div id="currentEmail" class="progress-label"></div></div>
		  		</td>
		  	</tr>
			<tr>
                    <td>		  		
                        <div class="discountOption"><label><span class="required">* </span><?php echo $text_type?>:</label>
                    </td>

		    	<td>	
                    <div class="col-xs-6">
                        <select name="discount_type" class="form-control">
                            <option value="P"><?php echo $text_percentage; ?></option>
                            <option value="F"><?php echo $text_fixed_amount; ?></option>
                        </select>
                    </div>
				</td>
			</tr>
			<tr>
				<td> 
		    		<label><span class="required">* </span><?php echo $text_discount?>:</label>
				</td>
				<td>
                    <div class="col-xs-6">
                        <div class="input-group">
                            <input name="discount" required="required" type="number" min="0" class="brSmallField form-control">
                            <span class="input-group-addon" ><p style="display:none;margin-bottom: 0px;" id="currencyAddonForm"><?php echo $currency ?></p><p style="display:none;margin-bottom: 0px;" id="percentageAddonForm">%</p></span>
                        </div> 
                    </div>
		   		 </td>
		   	</tr>
		   	<tr>
		   		<td>	
		   		 	<label><span class="required">* </span><?php echo $text_duration; ?>:</label>
		   		</td>
		   		<td>
                	<div class="col-xs-6">
                        <div class="input-group"><input required="required" type="number" min="0" name="duration" class="brSmallField form-control">
                            <span class="input-group-addon"><p style="margin-bottom:0;"><?php echo $text_days; ?></p></span></div>
                        </div>
                    </div>
			  	</td>
			 </tr>

		  	<tr>
		  		<td style="width:100px;">
	  				<label><span class="required">* </span><?php echo $text_subject; ?>:</label>
	    		</td>
	    		<td>
	    			<input placeholder="Mail subject..." class="form-control" type="text" id="subject" name="subject" style="margin-left: 15px;" value="<?php if(!empty($data['WishlistDiscounts']['discountSubject'])) echo $data['WishlistDiscounts']['discountSubject']; else echo $default_subject; ?>" />	
	    		</td>
	    	</tr>
	  		<tr>
                <input type="hidden" name="email" value="<?php  echo $data['WishlistDiscounts']['message'][$language['language_id']]; ?>" />
 				<td colspan="2" style="border-top: 0;">
				  	<textarea id="message" name="message">
				  		<?php if(!empty($data['WishlistDiscounts']['message'][$language['language_id']])) { 
                            echo $data['WishlistDiscounts']['message'][$language['language_id']]; 
				          } else { 
				            echo $defaultMessage;
				          }
				   		 ?>
				  	</textarea>
				</td>
			</tr>
			<tr>
				<td><button class="btn btn-success sendMessage" type="submit"><?php echo $text_send; ?></button></td>
			</tr>
	</table>
	</form>
</div>
<script>

var customers = <?php echo json_encode($customers); ?>;   
var durationValue, discountTypeValue, discountMessage, discountSubject, discountValue;
var customer_index = 0; 
$(document).ready(function(e) {  
	$("#message").summernote({height: 300});
});

if($('select[name="discount_type"]').val() == 'P'){
	$('#percentageAddonForm').show();
} else {
	$('#currencyAddonForm').show();
}

$('select[name="discount_type"]').on('change', function(e){  
	if($(this).val() == 'P') {
		$('#percentageAddonForm').show();
		$('input[type="number"][name="discount"]').attr('max','100');
		$('#currencyAddonForm').hide();
	} else {
		$('#currencyAddonForm').show();
		$('input[type="number"][name="discount"]').attr('max','');
		$('#percentageAddonForm').hide();
	}	
});	


$('.btn.btn-success.sendMessage').on('click', function(e) {
	e.preventDefault();
	var error= false;
	if(!validateNumber($('input[name="discount"]'), e)) {
		error = true;
	} else if($('select[name="discount_type"]').val() == 'P' && ($('input[name="discount"]').val() < 0 || $('input[name="discount"]').val() >= 100)) {
		$('input[name="discount"]').css({'background':'#f2dede'});
		error = true;
	}

	if(!validateNumber($('input[name="duration"]'), e)) {
		error = true;
	} else if($('input[name="duration"]').val() < 0) {
		$('input[name="duration"]').css({'background':'#f2dede'});
		error = true;
	}

	discountValue = $('input[type="number"][name="discount"]').val(); 
	durationValue = $('input[type="number"][name="duration"]').val();
	discountTypeValue = $('select[name="discount_type"]').val();	
	discountMessage =  $('input[type="hidden"][name="email"]').val();
	discountSubject = $('input[type="text"][name="subject"]').val();  

	if(!error) {
		sendMail();
	}

});				


function sendMail() {
	if(!customers[customer_index]){
		alert("The messages have been sent to selected customers!");
		refreshData();
		location.reload();
		return;
	}
	var customer = customers[customer_index++];

	json = {
		discount: discountValue,
		duration: durationValue,
		discount_type : discountTypeValue,
		subject: discountSubject,
		message: discountMessage,
		customer_id: customer.customer_id,
	};
	$('.progressBarHolder').fadeIn();

	$('#currentEmail').text(customer.email);
	$.ajax({
		url: 'index.php?route=module/wishlistdiscounts/sendMail&token=<?php echo $token; ?>',
		dataType:"json",
		data: json,
		type: 'POST',
		complete: function(json) {
			sendMail();
		}
	});
};

function validateNumber(input, theEvent) {
	var regex = /[0-9]|\./;
	if(!regex.test(input.val())) { 
		input.css({'background':'#f2dede'});
		return false;
	} else {
		input.css({'background':'#fff'});
		return true;
	}
}
</script>

