  <div class="customerInfo">
    <h5><?php echo $customer_info['name']; ?></h5>
  </div>
  <div style="padding:20px">
  <table class="table wishList">
    <thead>
      <tr>
        <th><?php echo $column_image; ?></th>
        <th><?php echo $column_name; ?></th>
        <th><?php echo $column_model; ?></th>
        <th><?php echo $column_stock; ?></th>
        <th><?php echo $column_price; ?></th>
      </tr>
    </thead>
    <?php foreach ($products as $product) { ?>
    <tbody id="wishlist-row<?php echo $product['product_id']; ?>">
    <td><?php if ($product['thumb']) { ?>
        <a href="<?php echo $product['href']; ?>" target="_blank"> <img class="image" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /> </a>
        <?php } ?></td>
      <td><a href="<?php echo $product['href']; ?>" target="_blank"><?php echo $product['name']; ?></a>
        <input type="hidden" name="product[]" value="<?php echo $product['product_id']; ?>" /></td>
      <td><?php echo $product['model']; ?></td>
      <td><?php echo $product['stock']; ?></td>
      <td><?php if ($product['price']) { ?>
        <div class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $currencyLeft.$product['price'].$currencyRight ?>
          <?php } else { ?>
          <s><?php echo $currencyLeft.$product['price'].$currencyRight; ?></s><br />
          <b style="color:#F00"><?php echo $currencyLeft.$product['special'].$currencyRight ?></b>
          <?php } ?>
        </div>
        <?php } ?></td>
      </td>
    </tr>
  </tbody>
    <input type="hidden" name="customer_id" value="<?php echo $customer_info['customer_id']; ?>"/>
    <?php } ?>
  </table>
</div>