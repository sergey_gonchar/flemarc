<table class="table templateForm"> 
    <?php  $languageCount = count($languages); ?>
    <tr>
      <td class="userMessageLegend"><h5><strong><span class="required">*</span> <?php echo $message_to_customer_heading; ?></strong></h5>
      	<span class="help"><i class="fa fa-info-circle"></i><?php echo $message_to_customer_help; ?></span>
	  </td>
      <td>
        <div class="tabbable">
          <div class="tab-navigation">
            <ul class="nav nav-tabs mainMenuTabs">
            <?php $class="active"; foreach ($languages as $language) { ?>
              <li class="<?php  echo $class; ?>"><a href="#email_<?php echo $language['language_id'];?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></a></li>
            <?php  $class="";} ?>
          </div>
          <div class="tab-content">
            <?php  $class="active"; foreach ($languages as $language) { ?>
                <div class="emailLanguageWrapper tab-pane <?php echo $class; ?>" id="email_<?php echo $language['language_id'];?>">
                  <label for="WishlistDiscounts[subject][<?php echo $language['language_id']; ?>]">
                    <span class="required">*</span>Subject:
                  </label>  
                 <input placeholder="Mail subject" type="text" id="subject" class="form-control" name="WishlistDiscounts[discountSubject]" value="<?php if(!empty($data['WishlistDiscounts']['discountSubject'])) echo $data['WishlistDiscounts']['discountSubject'];  else echo $default_subject;?>" />
                  <?php if(isset($error_subject[$language['language_id']])){ ?>
                      <span class="error"><?php echo $error_subject[$language['language_id']]; ?></span>
                   <?php } ?> 
                  <textarea name="WishlistDiscounts[message][<?php echo $language['language_id']; ?>]"  class="form-control" id="discount_message_<?php echo $language['language_id']; ?>">
                    <?php if(!empty($data['WishlistDiscounts']['message'][$language['language_id']])) { echo $data['WishlistDiscounts']['message'][$language['language_id']]; } else { echo $default_discount_message;  } ?>
                  </textarea>
                </div>
            <?php $class="";} ?>
          </div>
        </div>
      </td>
    </tr>
  </table>
<script type="text/javascript" >
    <?php foreach ($languages as $language) { ?>
		$("#discount_message_<?php echo $language['language_id']; ?>").summernote({height: 300});
    <?php } ?>
</script> 

