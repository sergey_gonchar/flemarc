<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
   <div class="page-header">
      <div class="container-fluid">
         <div class="pull-right">
            <button type="submit" form="form-control" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
         </div>
         <h1><?php echo $heading_title; ?></h1>
         <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
         </ul>
      </div>
   </div>
   <div class="container-fluid">
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
         <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-control" class="form-horizontal">
         <div class="panel panel-default">
            <div class="panel-heading">
               <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
               <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-status"><?php echo $thema; ?></label>
                  <div class="col-sm-10">
                     <select name="control_thema" class="form-control">
                        <option value="1"<?php if($control_thema == '1') echo ' selected="selected"';?>><?php echo $text_thema1; ?></option>
                        <option value="2"<?php if($control_thema == '2') echo ' selected="selected"';?>><?php echo $text_thema2; ?></option>
                        <option value="3"<?php if($control_thema == '3') echo ' selected="selected"';?>><?php echo $text_thema3; ?></option>
                     </select>
                  </div>
               </div>
               <br />
               <div class="tab-content">
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#tab_top" data-toggle="tab"><?php echo $tab_top; ?></a></li>
                     <li><a href="#tab_category" data-toggle="tab"><?php echo $tab_category; ?></a></li>
                     <li><a href="#tab_product" data-toggle="tab"><?php echo $tab_product; ?></a></li>
                     <li><a href="#tab_contact" data-toggle="tab"><?php echo $tab_contact; ?></a></li>
                     <li><a href="#tab_footer" data-toggle="tab"><?php echo $tab_footer; ?></a></li>
                     <li><a href="#tab_custom_css" data-toggle="tab"><?php echo $tab_css; ?></a></li>
                  </ul>
               </div>
               <div class="tab-content">
                  <div id="tab_top" class="tab-pane active">
                     <table class="table table-hover">
                        <div class="form-group">
                           <label class="col-sm-2 control-label">Расположение логотипа</label>
                           <div class="col-sm-10">
                              <select name="control_logo" class="form-control">
                                 <option value="1"<?php if($control_logo == '1') echo ' selected="selected"';?>>По центру</option>
                                 <option value="2"<?php if($control_logo == '2') echo ' selected="selected"';?>>В левой части</option>
                              </select>
                           </div>
                        </div>
						<div class="form-group">
                           <label class="col-sm-2 control-label"><?php echo $collme; ?></label>
                           <div class="col-sm-10">
                              <select name="control_callme" class="form-control">
                                 <option value="1"<?php if($control_callme == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_callme == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-2 control-label"><?php echo $quicklogin; ?></label>
                           <div class="col-sm-10">
                              <select name="control_quicklogin" class="form-control">
                                 <option value="1"<?php if($control_quicklogin == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_quicklogin == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
						<div class="form-group">
                           <label class="col-sm-2 control-label"><?php echo $lastname; ?></label>
                           <div class="col-sm-10">
                              <select name="control_lastname" class="form-control">
                                 <option value="1"<?php if($control_lastname == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_lastname == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
						 <div class="form-group">
                           <label class="col-sm-2 control-label"><?php echo $curlang; ?></label>
                           <div class="col-sm-10">
                              <select name="control_curlang" class="form-control">
                                 <option value="1"<?php if($control_curlang == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_curlang == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
						<div class="form-group">
                           <label class="col-sm-2 control-label">Телефон вверху</label>
                           <div class="col-sm-10">
                              <select name="control_telephone" class="form-control">
                                 <option value="1"<?php if($control_telephone == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_telephone == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
						<div class="form-group">
                           <label class="col-sm-2 control-label"><?php echo $brand_menu; ?></label>
                           <div class="col-sm-10">
                              <select name="control_brand_menu" class="form-control">
                                 <option value="1"<?php if($control_brand_menu == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_brand_menu == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
						<div class="form-group">
                           <label class="col-sm-2 control-label">Кнопка в меню</label>
                           <div class="col-sm-10">
                              <select name="control_wishlist_menu" class="form-control">
                                 <option value="1"<?php if($control_wishlist_menu == '1') echo ' selected="selected"';?>>Кнопка скидки</option>
                                 <option value="2"<?php if($control_wishlist_menu == '2') echo ' selected="selected"';?>>Кнопка закладки</option>
								 <option value="3"<?php if($control_wishlist_menu == '3') echo ' selected="selected"';?>>Нет кнопки</option>
                              </select>
                           </div>
                        </div>
						<div class="form-group">
                           <label class="col-sm-2 control-label"><?php echo $adress_menu; ?></label>
                           <div class="col-sm-10">
                              <select name="control_adress_menu" class="form-control">
                                 <option value="1"<?php if($control_adress_menu == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_adress_menu == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
						
						
						<div class="form-group">
                           <label class="col-sm-2 control-label">Кликабельная главная категория</label>
                           <div class="col-sm-10">
                              <select name="control_lavel1" class="form-control">
                                 <option value="1"<?php if($control_lavel1 == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_lavel1 == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>						
						<div class="form-group">
                           <label class="col-sm-2 control-label">Выводить категории второго уровня</label>
                           <div class="col-sm-10">
                              <select name="control_lavel2" class="form-control">
                                 <option value="1"<?php if($control_lavel2 == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_lavel2 == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>						
						<div class="form-group">
                           <label class="col-sm-2 control-label">Выводить категории третьего уровня</label>
                           <div class="col-sm-10">
                              <select name="control_lavel3" class="form-control">
                                 <option value="1"<?php if($control_lavel3 == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_lavel3 == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
						<div class="form-group">
                           <label class="col-sm-2 control-label">Выводить моб. меню текстом</label>
                           <div class="col-sm-10">
                              <select name="control_menutext" class="form-control">
                                 <option value="1"<?php if($control_menutext == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_menutext == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
						<div class="form-group">
						    <label class="col-sm-2 control-label">Маска для телефона</label>
                              <div class="col-sm-10">
							  <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_phone_mask[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_phone_mask[$language['language_id']]) ? $control_phone_mask[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                             </div>
						</div>
                        <hr>
						<!--promo info-->
						<h2 style="margin-bottom:15px;"><?php echo $custom_info; ?></h2>
                     <hr>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" ><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                           <select class="form-control" name="control_custom_info_status">
                              <option value="0"<?php if($control_custom_info_status == '0') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              <option value="1"<?php if($control_custom_info_status == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                           </select>
                        </div>
                     </div>
                     <div class="tab-content">
                        <ul id="language3" class="nav nav-tabs">
                           <?php foreach ($languages as $language) { ?>
                           <li><a data-toggle="tab" href="#tab3-language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                           <?php } ?>
                        </ul>
                        <div class="tab-content">
                           <?php foreach ($languages as $language) { ?>
                           <div class="tab-pane" id="tab3-language-<?php echo $language['language_id']; ?>">
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $custom_head_info; ?></label>
                                 <div class="col-sm-10">
                                    <input class="form-control" type="text" name="control_title_html[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_title_html[$language['language_id']]) ? $control_title_html[$language['language_id']] : ''; ?>" />
                                 </div>
                              </div>
							  <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $content_info; ?></label>
                                 <div class="col-sm-10">
                                    <textarea id="input-content-info<?php echo $language['language_id']; ?>" name="control_content_info[<?php echo $language['language_id']; ?>]" cols="50" rows="8" class="form-control"><?php echo isset($control_content_info[$language['language_id']]) ? $control_content_info[$language['language_id']] : ''; ?></textarea>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
					 <hr>
						<!--promo info-->
						<h2 style="margin-bottom:15px;">Html вкладка в панели личного кабинета</h2>
                     <hr>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" ><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                           <select class="form-control" name="control_custom_account_status">
                              <option value="0"<?php if($control_custom_account_status == '0') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              <option value="1"<?php if($control_custom_account_status == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                           </select>
                        </div>
                     </div>
                     <div class="tab-content">
                        <ul id="language5" class="nav nav-tabs">
                           <?php foreach ($languages as $language) { ?>
                           <li><a data-toggle="tab" href="#tab5-language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                           <?php } ?>
                        </ul>
                        <div class="tab-content">
                           <?php foreach ($languages as $language) { ?>
                           <div class="tab-pane" id="tab5-language-<?php echo $language['language_id']; ?>">
							  <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $content_info; ?></label>
                                 <div class="col-sm-10">
                                    <textarea id="input-content-account<?php echo $language['language_id']; ?>" name="control_content_account[<?php echo $language['language_id']; ?>]" cols="50" rows="8" class="form-control"><?php echo isset($control_content_account[$language['language_id']]) ? $control_content_account[$language['language_id']] : ''; ?></textarea>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                     <br><!--promo info-->
						<h3><?php echo $heading_top_url; ?></h3>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $urltitle1; ?></label>
                           <div class="col-sm-4">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_top_urltitle1[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_top_urltitle1[$language['language_id']]) ? $control_menu_top_urltitle1[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $url; ?></label>
                           <?php foreach ($languages as $language) { ?>
						   <div class="col-sm-1"></div>
						   <div class="col-sm-6">
						   <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input class="form-control" type="text" name="control_menu_top_url1[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_top_url1[$language['language_id']]) ? $control_menu_top_url1[$language['language_id']] : ''; ?>" />
                           </div>
						   </div>
						   <?php } ?>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $urltitle2; ?></label>
                           <div class="col-sm-4">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_top_urltitle2[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_top_urltitle2[$language['language_id']]) ? $control_menu_top_urltitle2[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $url; ?></label>
                           <?php foreach ($languages as $language) { ?>
						   <div class="col-sm-1"></div>
						   <div class="col-sm-6">
						   <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input class="form-control" type="text" name="control_menu_top_url2[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_top_url2[$language['language_id']]) ? $control_menu_top_url2[$language['language_id']] : ''; ?>" />
                           </div>
						   </div>
						   <?php } ?>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $urltitle3; ?></label>
                           <div class="col-sm-4">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_top_urltitle3[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_top_urltitle3[$language['language_id']]) ? $control_menu_top_urltitle3[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $url; ?></label>
                          <?php foreach ($languages as $language) { ?>
						   <div class="col-sm-1"></div>
						   <div class="col-sm-6">
						   <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input class="form-control" type="text" name="control_menu_top_url3[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_top_url3[$language['language_id']]) ? $control_menu_top_url3[$language['language_id']] : ''; ?>" />
                           </div>
						   </div>
						   <?php } ?>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $urltitle4; ?></label>
                           <div class="col-sm-4">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_top_urltitle4[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_top_urltitle4[$language['language_id']]) ? $control_menu_top_urltitle4[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $url; ?></label>
                          <?php foreach ($languages as $language) { ?>
						   <div class="col-sm-1"></div>
						   <div class="col-sm-6">
						   <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input class="form-control" type="text" name="control_menu_top_url4[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_top_url4[$language['language_id']]) ? $control_menu_top_url4[$language['language_id']] : ''; ?>" />
                           </div>
						   </div>
						   <?php } ?>
                        </div>
						<hr>												
						<h3><?php echo $heading_url; ?></h3>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $urltitle1; ?></label>
                           <div class="col-sm-4">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_urltitle1[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_urltitle1[$language['language_id']]) ? $control_menu_urltitle1[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $url; ?></label>
                           <?php foreach ($languages as $language) { ?>
						   <div class="col-sm-1"></div>
						   <div class="col-sm-6">
						   <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input class="form-control" type="text" name="control_menu_url1[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_url1[$language['language_id']]) ? $control_menu_url1[$language['language_id']] : ''; ?>" />
                           </div>
						   </div>
						   <?php } ?>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $urltitle2; ?></label>
                           <div class="col-sm-4">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_urltitle2[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_urltitle2[$language['language_id']]) ? $control_menu_urltitle2[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $url; ?></label>
                           <?php foreach ($languages as $language) { ?>
						   <div class="col-sm-1"></div>
						   <div class="col-sm-6">
						   <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input class="form-control" type="text" name="control_menu_url2[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_url2[$language['language_id']]) ? $control_menu_url2[$language['language_id']] : ''; ?>" />
                           </div>
						   </div>
						   <?php } ?>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $urltitle3; ?></label>
                           <div class="col-sm-4">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_urltitle3[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_urltitle3[$language['language_id']]) ? $control_menu_urltitle3[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $url; ?></label>
                          <?php foreach ($languages as $language) { ?>
						   <div class="col-sm-1"></div>
						   <div class="col-sm-6">
						   <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input class="form-control" type="text" name="control_menu_url3[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_url3[$language['language_id']]) ? $control_menu_url3[$language['language_id']] : ''; ?>" />
                           </div>
						   </div>
						   <?php } ?>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $urltitle4; ?></label>
                           <div class="col-sm-4">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_urltitle4[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_urltitle4[$language['language_id']]) ? $control_menu_urltitle4[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $url; ?></label>
                          <?php foreach ($languages as $language) { ?>
						   <div class="col-sm-1"></div>
						   <div class="col-sm-6">
						   <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                              <input class="form-control" type="text" name="control_menu_url4[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_url4[$language['language_id']]) ? $control_menu_url4[$language['language_id']] : ''; ?>" />
                           </div>
						   </div>
						   <?php } ?>
                        </div>
						
                        <hr>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $text_menu; ?></label>
                           <div class="col-sm-5">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_menu_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_menu_title[$language['language_id']]) ? $control_menu_title[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $hidden_menu; ?></label>
                           <div class="col-sm-5">
                              <select name="control_menu_hidden" class="form-control">
                                 <option value="1"<?php if($control_menu_hidden == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_menu_hidden == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-sm-1 control-label" ><?php echo $promo_text; ?></label>
                           <div class="col-sm-6">
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_promo[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_promo[$language['language_id']]) ? $control_promo[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </div>
                           <label class="col-sm-1 control-label" ><?php echo $promo_url; ?></label>
                           <div class="col-sm-4">
                              <input class="form-control" type="text" name="control_promo_url" value="<?php echo $control_promo_url; ?>" />
                           </div>
                        </div>
                     </table>
                  </div>
                  <div id="tab_category" class="tab-pane" >
                     <table class="table table-hover">
                        <tr>
                           <td style="width:30%;"><?php echo $hidden_filter; ?></td>
                           <td>
                              <select name="control_filter" class="form-control">
                                 <option value="1"<?php if($control_filter == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_filter == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td style="width:30%;"><?php echo $hidden_view; ?></td>
                           <td>
                              <select name="control_view" class="form-control">
                                 <option value="1"<?php if($control_view == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_view == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td style="width:30%;"><?php echo $hidden_imgcategory; ?></td>
                           <td>
                              <select name="control_imgcategory" class="form-control">
                                 <option value="1"<?php if($control_imgcategory == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_imgcategory == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td style="width:30%;"><?php echo $hidden_stock; ?></td>
                           <td>
                              <select name="control_stock" class="form-control">
                                 <option value="1"<?php if($control_stock == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_stock == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td style="width:30%;"><?php echo $hidden_compare; ?></td>
                           <td>
                              <select name="control_compare" class="form-control">
                                 <option value="1"<?php if($control_compare == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_compare == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td style="width:30%;">Кнопка закладки</td>
                           <td>
                              <select name="control_wishlist" class="form-control">
                                 <option value="1"<?php if($control_wishlist == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_wishlist == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td style="width:30%;"><?php echo $hidden_quickview; ?></td>
                           <td>
                              <select name="control_quickview" class="form-control">
                                 <option value="1"<?php if($control_quickview == '1') echo ' selected="selected"';?>>Сбоку</option>
                                 <option value="2"<?php if($control_quickview == '2') echo ' selected="selected"';?>>Внизу</option>
								 <option value="3"<?php if($control_quickview == '3') echo ' selected="selected"';?>>Отключить</option>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><?php echo $back_to_top; ?></td>
                           <td>
                              <select name="control_back_to_top" class="form-control">
                                 <option value="1"<?php if($control_back_to_top == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_back_to_top == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><?php echo $category_option; ?></td>
                           <td>
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_option_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_option_title[$language['language_id']]) ? $control_option_title[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </td>
                        </tr>
                     </table>
                  </div>
                  <div class="tab-pane" id="tab_product">
                     <table class="table table-hover">
                        <tr>
                           <td style="width:30%;"><?php echo $minprice; ?></td>
                           <td>
                              <select name="control_minprice" class="form-control">
                                 <option value="1"<?php if($control_minprice == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_minprice == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><?php echo $fastorder; ?></td>
                           <td>
                              <select name="control_fastorder" class="form-control">
                                 <option value="1"<?php if($control_fastorder == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_fastorder == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><?php echo $preorder; ?></td>
                           <td>
                              <select name="control_outstock" class="form-control">
                                 <option value="1"<?php if($control_outstock == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_outstock == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><?php echo $quantity; ?></td>
                           <td>
                              <select name="control_quantity" class="form-control">
                                 <option value="1"<?php if($control_quantity == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_quantity == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						 <tr>
                           <td><?php echo $additional; ?></td>
                           <td>
                              <select name="control_additional" class="form-control">
                                 <option value="1"<?php if($control_additional == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_additional == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td>Уведичение фото при наведении</td>
                           <td>
                              <select name="control_zoom_image" class="form-control">
                                 <option value="1"<?php if($control_zoom_image == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_zoom_image == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td><?php echo $description; ?></td>
                           <td>
                              <select name="control_description" class="form-control">
                                 <option value="1"<?php if($control_description == '1') echo ' selected="selected"';?>>Выше кнопки купить</option>
                                 <option value="2"<?php if($control_description == '2') echo ' selected="selected"';?>>Ниже кнопки купить</option>
								 <option value="3"<?php if($control_description == '3') echo ' selected="selected"';?>>Внизу на всю ширину</option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td><?php echo $review; ?></td>
                           <td>
                              <select name="control_review" class="form-control">
                                 <option value="1"<?php if($control_review == '1') echo ' selected="selected"';?>>Под ценой</option>
                                 <option value="2"<?php if($control_review == '2') echo ' selected="selected"';?>>Под кнопкой купить</option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td>Автопересчет цены</td>
                           <td>
                              <select name="control_autoprice" class="form-control">
                                 <option value="1"<?php if($control_autoprice == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_autoprice == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
						<tr>
                           <td>Активная первая опция</td>
                           <td>
                              <select name="control_firstoption" class="form-control">
                                 <option value="1"<?php if($control_firstoption == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_firstoption == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><?php echo $help_size; ?></td>
                           <td>
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_size_title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_size_title[$language['language_id']]) ? $control_size_title[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </td>
                        </tr>
                     </table>
                     <h2 style="margin-bottom:15px;"><?php echo $custom_tab; ?></h2>
                     <hr>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" ><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                           <select class="form-control" name="control_custom_tab_status">
                              <option value="0"<?php if($control_custom_tab_status == '0') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              <option value="1"<?php if($control_custom_tab_status == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                           </select>
                        </div>
                     </div>
                     <div class="tab-content">
                        <ul id="language" class="nav nav-tabs">
                           <?php foreach ($languages as $language) { ?>
                           <li><a data-toggle="tab" href="#tab-language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                           <?php } ?>
                        </ul>
                        <div class="tab-content">
                           <?php foreach ($languages as $language) { ?>
                           <div class="tab-pane" id="tab-language-<?php echo $language['language_id']; ?>">
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $title_tab; ?></label>
                                 <div class="col-sm-10">
                                    <input class="form-control" type="text" name="control_title_tab[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_title_tab[$language['language_id']]) ? $control_title_tab[$language['language_id']] : ''; ?>" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $content_tab; ?></label>
                                 <div class="col-sm-10">
                                    <textarea id="input-description<?php echo $language['language_id']; ?>" name="control_content_tab[<?php echo $language['language_id']; ?>]" cols="50" rows="8" class="form-control"><?php echo isset($control_content_tab[$language['language_id']]) ? $control_content_tab[$language['language_id']] : ''; ?></textarea>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
					 <br>
					 <h2 style="margin-bottom:15px;"><?php echo $custom_tab3; ?></h2>
                     <hr>
					 <div class="form-group">
                        <label class="col-sm-2 control-label" ><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                           <select class="form-control" name="control_custom_tab4_status">
                              <option value="0"<?php if($control_custom_tab4_status == '0') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              <option value="1"<?php if($control_custom_tab4_status == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                           </select>
                        </div>
                     </div>
                     <div class="tab-content">
                        <ul id="language4" class="nav nav-tabs">
                           <?php foreach ($languages as $language) { ?>
                           <li><a data-toggle="tab" href="#tab4-language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                           <?php } ?>
                        </ul>
                        <div class="tab-content">
                           <?php foreach ($languages as $language) { ?>
                           <div class="tab-pane" id="tab4-language-<?php echo $language['language_id']; ?>">
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $title_tab; ?></label>
                                 <div class="col-sm-10">
                                    <input class="form-control" type="text" name="control_title_tab4[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_title_tab4[$language['language_id']]) ? $control_title_tab4[$language['language_id']] : ''; ?>" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $content_tab; ?></label>
                                 <div class="col-sm-10">
                                    <textarea id="input-description<?php echo $language['language_id']; ?>" name="control_content_tab4[<?php echo $language['language_id']; ?>]" cols="50" rows="8" class="form-control"><?php echo isset($control_content_tab4[$language['language_id']]) ? $control_content_tab4[$language['language_id']] : ''; ?></textarea>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                     <br>
                     <h2 style="margin-bottom:15px;"><?php echo $custom_tab2; ?></h2>
                     <hr>
                     <div class="form-group">
                        <label class="col-sm-2 control-label" ><?php echo $entry_status; ?></label>
                        <div class="col-sm-10">
                           <select class="form-control" name="control_custom_tab2_status">
                              <option value="0"<?php if($control_custom_tab2_status == '0') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              <option value="1"<?php if($control_custom_tab2_status == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                           </select>
                        </div>
                     </div>
                     <div class="tab-content">
                        <ul id="language2" class="nav nav-tabs">
                           <?php foreach ($languages as $language) { ?>
                           <li><a data-toggle="tab" href="#tab2-language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                           <?php } ?>
                        </ul>
                        <div class="tab-content">
                           <?php foreach ($languages as $language) { ?>
                           <div class="tab-pane" id="tab2-language-<?php echo $language['language_id']; ?>">
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $title_tab2; ?></label>
                                 <div class="col-sm-10">
                                    <input class="form-control" type="text" name="control_title_tab2[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_title_tab2[$language['language_id']]) ? $control_title_tab2[$language['language_id']] : ''; ?>" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label" ><?php echo $content_tab2; ?></label>
                                 <div class="col-sm-10">
                                    <textarea id="input-description2<?php echo $language['language_id']; ?>" name="control_content_tab2[<?php echo $language['language_id']; ?>]" cols="50" rows="8" class="form-control"><?php echo isset($control_content_tab2[$language['language_id']]) ? $control_content_tab2[$language['language_id']] : ''; ?></textarea>
                                 </div>
                              </div>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                     <br>
                     <script type="text/javascript"><!--
                        <?php foreach ($languages as $language) { ?>
                        $('#input-content-info<?php echo $language['language_id']; ?>,#input-content-account<?php echo $language['language_id']; ?>,#input-description<?php echo $language['language_id']; ?>, #input-description2<?php echo $language['language_id']; ?>').summernote({height: 300});
                        <?php } ?>
                        $('#language3 a:first, #language4 a:first, #language5 a:first, #language a:first, #language2 a:first').tab('show');
                        //--></script>					 
                  </div>
                  <div class="tab-pane" id="tab_contact">
                     <table class="table table-hover">
                        <tr>
                           <td style="width:30%;"><?php echo $adress; ?></td>
                           <td><input class="form-control" type="text" name="control_map_adress" value="<?php echo $control_map_adress; ?>" /> <i>&nbsp;<?php echo $adress_info; ?></i></td>
                        </tr>
                        <tr>
                           <td><?php echo $width; ?></td>
                           <td><input class="form-control" type="text" name="control_map_width" value="<?php echo $control_map_width; ?>" /> %</td>
                           <td></td>
                        </tr>
                        <tr>
                           <td><?php echo $height; ?></td>
                           <td><input class="form-control" type="text" name="control_map_height" value="<?php echo $control_map_height; ?>" /> px</td>
                           <td></td>
                        </tr>
                        <tr>
                           <td><?php echo $map_size; ?></td>
                           <td>
                              <select class="form-control" name="control_map_zoom" size="1">
                                 <option value="1"<?php if($control_map_zoom == '1') echo ' selected="selected"';?>>1</option>
                                 <option value="2"<?php if($control_map_zoom == '2') echo ' selected="selected"';?>>2-<?php echo $z1; ?></option>
                                 <option value="3"<?php if($control_map_zoom == '3') echo ' selected="selected"';?>>3</option>
                                 <option value="4"<?php if($control_map_zoom == '4') echo ' selected="selected"';?>>4</option>
                                 <option value="5"<?php if($control_map_zoom == '5') echo ' selected="selected"';?>>5-<?php echo $z2; ?></option>
                                 <option value="6"<?php if($control_map_zoom == '6') echo ' selected="selected"';?>>6</option>
                                 <option value="7"<?php if($control_map_zoom == '7') echo ' selected="selected"';?>>7</option>
                                 <option value="8"<?php if($control_map_zoom == '8') echo ' selected="selected"';?>>8</option>
                                 <option value="9"<?php if($control_map_zoom == '9') echo ' selected="selected"';?>>9-<?php echo $z3; ?></option>
                                 <option value="10"<?php if($control_map_zoom == '10') echo ' selected="selected"';?>>10</option>
                                 <option value="11"<?php if($control_map_zoom == '11') echo ' selected="selected"';?>>11</option>
                                 <option value="12"<?php if($control_map_zoom == '12') echo ' selected="selected"';?>>12</option>
                                 <option value="13"<?php if($control_map_zoom == '13') echo ' selected="selected"';?>>13</option>
                                 <option value="14"<?php if($control_map_zoom == '14') echo ' selected="selected"';?>>14-<?php echo $z4; ?></option>
                                 <option value="15"<?php if($control_map_zoom == '15') echo ' selected="selected"';?>>15</option>
                                 <option value="16"<?php if($control_map_zoom == '16') echo ' selected="selected"';?>>16</option>
                                 <option value="17"<?php if($control_map_zoom == '17') echo ' selected="selected"';?>>17-<?php echo $z5; ?></option>
                                 <option value="18"<?php if($control_map_zoom == '18') echo ' selected="selected"';?>>18</option>
                              </select>
                           </td>
                        </tr>
                        <tr>
                           <td><?php echo $islands; ?></td>
                           <td>
                              <?php foreach ($languages as $language) { ?>
                              <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
                                 <input class="form-control" type="text" name="control_islands_info[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_islands_info[$language['language_id']]) ? $control_islands_info[$language['language_id']] : ''; ?>" />
                              </div>
                              <?php } ?>
                           </td>
                        </tr>
                        <tr>
                           <td><?php echo $islands_color; ?></td>
                           <td>
                              <select class="form-control" name="control_islands_color" size="1">
                                 <option value="blue"<?php if($control_islands_color == 'blue') echo ' selected="selected"';?>><?php echo $blue; ?></option>
                                 <option value="red"<?php if($control_islands_color == 'red') echo ' selected="selected"';?>><?php echo $red; ?></option>
                                 <option value="darkOrange"<?php if($control_islands_color == 'darkOrange') echo ' selected="selected"';?>><?php echo $orange; ?></option>
                                 <option value="pink"<?php if($control_islands_color == 'pink') echo ' selected="selected"';?>><?php echo $pink; ?></option>
                                 <option value="gray"<?php if($control_islands_color == 'gray') echo ' selected="selected"';?>><?php echo $gray; ?></option>
                                 <option value="brown"<?php if($control_islands_color == 'brown') echo ' selected="selected"';?>><?php echo $brown; ?></option>
                              </select>
                           </td>
                        </tr>
                     </table>
                  </div>
                  <div class="tab-pane" id="tab_footer">
                     <br />
					 <table class="table table-hover">
					 <tr>
                           <td style="width:30%;"><?php echo $hidden_callmebtn; ?></td>
                           <td>
                              <select name="control_callmebtn" class="form-control">
                                 <option value="1"<?php if($control_callmebtn == '1') echo ' selected="selected"';?>><?php echo $text_enabled; ?></option>
                                 <option value="2"<?php if($control_callmebtn == '2') echo ' selected="selected"';?>><?php echo $text_disabled; ?></option>
                              </select>
                           </td>
                        </tr>
					 <tr>
					   <td style="width:30%;"><?php echo $info_shop; ?></td>
					   <td>
						  <?php foreach ($languages as $language) { ?>
						  <div class="input-group"><span class="input-group-addon"> <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span>
							 <input class="form-control" type="text" name="control_info_shop[<?php echo $language['language_id']; ?>]" value="<?php echo isset($control_info_shop[$language['language_id']]) ? $control_info_shop[$language['language_id']] : ''; ?>" />
						  </div>
						  <?php } ?>
					   </td>
					 </tr>
                     </table>
                     <h2><?php echo $follow; ?></h2>
                     <div class="info-help" style="text-align: left;"><?php echo $follow_url; ?></div>
                     <hr />
                     <div class="form-group">
                        <label class="col-sm-3 control-label" ><img style="margin-left:10px; float:right; margin-top: -7px;" src="../catalog/view/theme/magazin/image/vk.png" alt=""><?php echo $control_vk_url; ?></label>
                        <div class="col-sm-9">
                           <input class="form-control" type="text" name="control_vk_url" value="<?php echo $control_vk_url; ?>" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label" ><img style="margin-left:10px; float:right; margin-top: -7px;" src="../catalog/view/theme/magazin/image/ok.png" alt=""><?php echo $control_ok_url; ?></label>
                        <div class="col-sm-9">
                           <input class="form-control" type="text" name="control_ok_url" value="<?php echo $control_ok_url; ?>" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label" ><img style="margin-left:10px; float:right; margin-top: -7px;" src="../catalog/view/theme/magazin/image/fb.png" alt=""><?php echo $control_fb_url; ?></label>
                        <div class="col-sm-9">
                           <input class="form-control" type="text" name="control_fb_url" value="<?php echo $control_fb_url; ?>" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label" ><img style="margin-left:10px; float:right; margin-top: -7px;" src="../catalog/view/theme/magazin/image/tw.png" alt=""><?php echo $control_tw_url; ?></label>
                        <div class="col-sm-9">
                           <input class="form-control" type="text" name="control_tw_url" value="<?php echo $control_tw_url; ?>" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label" ><img style="margin-left:10px; float:right; margin-top: -7px;" src="../catalog/view/theme/magazin/image/ig.png" alt=""><?php echo $control_ig_url; ?></label>
                        <div class="col-sm-9">
                           <input class="form-control" type="text" name="control_ig_url" value="<?php echo $control_ig_url; ?>" />
                        </div>
                     </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label" ><img style="margin-left:10px; float:right; margin-top: -7px;" src="../catalog/view/theme/magazin/image/google.png" alt=""><?php echo $control_google_url; ?></label>
                        <div class="col-sm-9">
                           <input class="form-control" type="text" name="control_google_url" value="<?php echo $control_google_url; ?>" />
                        </div>
                     </div>
                  </div>
                  <div class="tab-pane" id="tab_custom_css">
                     <div class="form-group">
                        <label class="col-sm-2 control-label" ><?php echo $entry_custom_css; ?></label>
                        <div class="col-sm-10">
                           <textarea class="form-control" name="control_custom_css" cols="52" rows="16"><?php echo $control_custom_css; ?></textarea>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>
<?php echo $footer; ?>