<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	<div id="alert-category"></div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-maxy-category" class="form-horizontal">
		  <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
            <li class="<?php echo $tab_status; ?>"><a href="#tab-design" data-toggle="tab"><?php echo $tab_design_module; ?></a></li>
			<li id="design-quick-view" class="<?php echo $tab_status; ?>"><a href="#tab-design-quick-view" data-toggle="tab"><?php echo $tab_design_quick_view; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-data">
			  <div class="panel panel-default">
			    <div class="panel-body">	
				  <input type="hidden" name="module_id" value="<?php echo $module_id; ?>" />
				  <div class="form-group required">
					<label class="col-sm-3 control-label" for="input-name"><?php echo $entry_name; ?></label>
					<div class="col-sm-9">
					  <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
					  <?php if ($error_name) { ?>
					  <div class="text-danger"><?php echo $error_name; ?></div>
					  <?php } ?>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-3 control-label"><?php echo $entry_title; ?></label>
					<div class="col-sm-9">
					  <?php foreach ($languages as $language) { ?>
						<div class="input-group"><span class="input-group-addon"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /></span><input placeholder="<?php echo $entry_title; ?>" type="text" name="title[<?php echo $language['language_id']; ?>]" value="<?php echo isset($title[$language['language_id']]) ? $title[$language['language_id']] : ''; ?>" class="form-control" /></div>
					  <?php } ?>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-3 control-label"><?php echo $entry_categories; ?></label>
					<div class="col-sm-9">
					  <div class="table-responsive">
						<table id="category" class="table table-striped table-bordered table-hover">
						  <thead>
							<tr>
							  <td class="text-left"><?php echo $entry_category; ?></td>
							  <td class="text-left" width="20%"><?php echo $entry_limit_name; ?></td>
							  <td class="text-left" width="20%"><?php echo $entry_limit_name_product; ?></td>
							  <td class="text-left" width="15%"><?php echo $entry_limit_product; ?></td>
							  <td width="1%"></td>
							</tr>
						  </thead>
						  <tbody>
							<?php $category_row = 0; if ($select_categories != '') { ?>
							  <?php foreach ($select_categories as $select_category) { ?>
								<tr id="category-row<?php echo $category_row; ?>">
								  <td class="text-left">
									<input id="category-name<?php echo $category_row; ?>" type="text" name="select_categories[<?php echo $category_row; ?>][category]" value="<?php if(isset($select_category['category'])) { echo $select_category['category']; } ?>" class="form-control" />
									<input type="hidden" name="select_categories[<?php echo $category_row; ?>][category_id]" value="<?php if(isset($select_category['category_id'])) { echo $select_category['category_id']; } ?>" />
								  </td>
								  <td class="text-left">
									<input type="text" name="select_categories[<?php echo $category_row; ?>][limit_name]" value="<?php if(isset($select_category['limit_name'])) { echo $select_category['limit_name']; } ?>" class="form-control" />
								  </td>
								  <td class="text-left">
									<input type="text" name="select_categories[<?php echo $category_row; ?>][limit_name_product]" value="<?php if(isset($select_category['limit_name_product'])) { echo $select_category['limit_name_product']; } ?>" class="form-control" />
								  </td>
								  <td class="text-left">
									<input type="text" name="select_categories[<?php echo $category_row; ?>][limit_product]" value="<?php if(isset($select_category['limit_product'])) { echo $select_category['limit_product']; } ?>" class="form-control" />
								  </td>
								  <td class="text-right"><button type="button" onclick="$('#category-row<?php echo $category_row; ?>').remove();" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
								</tr>
								<?php $category_row++; ?>
							  <?php } ?>
							<?php } ?>
						  </tbody>
						  <tfoot>
							<tr>
							  <td colspan="4"></td>
							  <td class="text-right"><button type="button" onclick="addCategory();" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
							</tr>
						  </tfoot>
						</table>
					  </div>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-3 control-label"><?php echo $entry_product_name; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_product_name) { ?>
						  <input type="checkbox" name="status_product_name" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_product_name"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
					<label class="col-sm-3 control-label"><?php echo $entry_cart; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_cart) { ?>
						  <input type="checkbox" name="status_cart" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_cart"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
					<label class="col-sm-3 control-label"><?php echo $entry_carousel; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_carousel) { ?>
						  <input type="checkbox" name="status_carousel" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_carousel"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-3 control-label"><?php echo $entry_wishlist; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_wishlist) { ?>
						  <input type="checkbox" name="status_wishlist" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_wishlist"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
					<label class="col-sm-3 control-label"><?php echo $entry_compare; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_compare) { ?>
						  <input type="checkbox" name="status_compare" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_compare"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
					<label class="col-sm-3 control-label"><?php echo $entry_quick_view; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_quick_view) { ?>
						  <input type="checkbox" name="status_quick_view" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_quick_view"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-3 control-label"><?php echo $entry_price; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_price) { ?>
						  <input type="checkbox" name="status_price" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_price"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
					<label class="col-sm-3 control-label"><?php echo $entry_rating; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_rating) { ?>
						  <input type="checkbox" name="status_rating" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_rating"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
					<label class="col-sm-3 control-label"><?php echo $entry_google_font; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_google_font) { ?>
						  <input type="checkbox" name="status_google_font" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_google_font"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>
				  </div>
				  <div class="form-group">
					<label class="col-sm-3 control-label"><?php echo $entry_banner; ?></label>
					<div class="col-sm-1">
					  <div class="checkbox">
						<?php if ($status_banner) { ?>
						  <input type="checkbox" name="status_banner" checked="checked"  value="1" />
						  <span class="checkbox-label"></span>
						  <?php } else { ?>
						  <input type="checkbox" name="status_banner"  value="1" />
						  <span class="checkbox-label"></span>
						<?php } ?>
					  </div>
					</div>					
					<div id="banner-image">
					  <label class="col-sm-2 control-label" for="input-image-banner"><span data-toggle="tooltip" title="<?php echo $help_banner; ?>"><?php echo $entry_image_banner; ?></span></label>
					  <div class="col-sm-2">
						<a href="" id="module-image-banner" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_banner; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
						<input type="hidden" name="image_banner" value="<?php echo $image_banner; ?>"  id="input-image-banner" />
					  </div>
					  <label class="col-sm-2 control-label" for="input-link-banner"><?php echo $entry_link_banner; ?></label>
					  <div class="col-sm-2">
					    <input type="text" name="link_banner" value="<?php echo $link_banner; ?>" placeholder="<?php echo $entry_link_banner; ?>" id="input-link-banner" class="form-control" />
					  </div>
					</div>
				  </div>
				  <div class="form-group">
				    <label class="col-sm-3 control-label"><?php echo $entry_image_product; ?></label>
					<div class="col-sm-4">
					  <label class="control-label"><?php echo $entry_width; ?></label>
					  <input type="text" name="image_product_width" value="<?php echo $image_product_width; ?>" placeholder="<?php echo $entry_width; ?>" class="form-control" />
					</div>
					<div class="col-sm-1"></div>
					<div class="col-sm-4">
					  <label class="control-label"><?php echo $entry_height; ?></label>
					  <input type="text" name="image_product_height" value="<?php echo $image_product_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
					</div>
				  </div>
				  <fieldset id="quick-view-data">
					<legend><?php echo $entry_quick_view; ?></legend>
				    <div class="form-group">
				      <label class="col-sm-3 control-label"><?php echo $entry_manufacturer; ?></label>
					  <div class="col-sm-1">
					    <div class="checkbox">
						  <?php if ($status_manufacturer) { ?>
						    <input type="checkbox" name="status_manufacturer" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_manufacturer"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					    </div>
					  </div>	
					  <label class="col-sm-3 control-label"><?php echo $entry_model; ?></label>
					  <div class="col-sm-1">
					    <div class="checkbox">
						  <?php if ($status_model) { ?>
						    <input type="checkbox" name="status_model" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_model"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					    </div>
					  </div>	
					  <label class="col-sm-3 control-label"><?php echo $entry_sku; ?></label>
					  <div class="col-sm-1">
					    <div class="checkbox">
						 <?php if ($status_sku) { ?>
						    <input type="checkbox" name="status_sku" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_sku"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					    </div>
					  </div>	
				    </div>
				    <div class="form-group">
				      <label class="col-sm-3 control-label"><?php echo $entry_weight_product; ?></label>
					  <div class="col-sm-1">
					    <div class="checkbox">
						  <?php if ($status_weight) { ?>
						    <input type="checkbox" name="status_weight" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_weight"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					    </div>
					  </div>
					  <label class="col-sm-3 control-label"><?php echo $entry_reward; ?></label>
					  <div class="col-sm-1">
					    <div class="checkbox">
						  <?php if ($status_reward) { ?>
						    <input type="checkbox" name="status_reward" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_reward"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					    </div>
					  </div>
					  <label class="col-sm-3 control-label"><?php echo $entry_stock; ?></label>
					  <div class="col-sm-1">
					    <div class="checkbox">
					  	  <?php if ($status_stock) { ?>
						    <input type="checkbox" name="status_stock" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_stock"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					   </div>
					  </div>
				    </div>
				    <div class="form-group">
				      <label class="col-sm-3 control-label"><?php echo $entry_wishlist; ?></label>
					  <div class="col-sm-1">
					   <div class="checkbox">
						  <?php if ($status_quick_wishlist) { ?>
						    <input type="checkbox" name="status_quick_wishlist" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_quick_wishlist"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					    </div>
					  </div>
					  <label class="col-sm-3 control-label"><?php echo $entry_compare; ?></label>
					  <div class="col-sm-1">
					    <div class="checkbox">
						  <?php if ($status_quick_compare) { ?>
						    <input type="checkbox" name="status_quick_compare" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_quick_compare"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					    </div>
					  </div>
					  <label class="col-sm-3 control-label"><?php echo $entry_more; ?></label>
					  <div class="col-sm-1">
					    <div class="checkbox">
						  <?php if ($status_quick_more) { ?>
						    <input type="checkbox" name="status_quick_more" checked="checked"  value="1" />
						    <span class="checkbox-label"></span>
						    <?php } else { ?>
						    <input type="checkbox" name="status_quick_more"  value="1" />
						    <span class="checkbox-label"></span>
						  <?php } ?>
					    </div>
					  </div>
				    </div>
					<div class="form-group">
				      <label class="col-sm-3 control-label"><?php echo $entry_image_product; ?></label>
					  <div class="col-sm-4">
					    <label class="control-label"><?php echo $entry_width; ?></label>
					    <input type="text" name="image_quick_width" value="<?php echo $image_quick_width; ?>" placeholder="<?php echo $entry_width; ?>" class="form-control" />
					  </div>
					  <div class="col-sm-1"></div>
					  <div class="col-sm-4">
					    <label class="control-label"><?php echo $entry_height; ?></label>
					    <input type="text" name="image_quick_height" value="<?php echo $image_quick_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
					  </div>
				    </div>
					<div class="form-group">
				      <label class="col-sm-3 control-label"><?php echo $entry_image_aditional; ?></label>
					  <div class="col-sm-4">
					    <label class="control-label"><?php echo $entry_width; ?></label>
					    <input type="text" name="image_quick_additional_width" value="<?php echo $image_quick_additional_width; ?>" placeholder="<?php echo $entry_width; ?>" class="form-control" />
					  </div>
					  <div class="col-sm-1"></div>
					  <div class="col-sm-4">
					    <label class="control-label"><?php echo $entry_height; ?></label>
					    <input type="text" name="image_quick_additional_height" value="<?php echo $image_quick_additional_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
					  </div>
				    </div>
					<legend></legend>
				  </fieldset>
				  <div class="form-group">
					<label class="col-sm-3 control-label" for="input-icon-status"><?php echo $entry_icon_status; ?></label>
					<div class="col-sm-3">
					  <select name="icon_status" id="input-icon-status" class="form-control">
						<?php if ($icon_status) { ?>
						<option value="1" selected="selected"><?php echo $text_rotate; ?></option>
						<option value="0"><?php echo $text_no_rotate; ?></option>
						<?php } else { ?>
						<option value="1"><?php echo $text_rotate; ?></option>
						<option value="0" selected="selected"><?php echo $text_no_rotate; ?></option>
						<?php } ?>
					  </select>
					</div>
					<label class="col-sm-3 control-label" for="input-status"><?php echo $entry_status; ?></label>
					<div class="col-sm-3">
					  <select name="status" id="input-status" class="form-control">
						<?php if ($status) { ?>
						<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
						<option value="0"><?php echo $text_disabled; ?></option>
						<?php } else { ?>
						<option value="1"><?php echo $text_enabled; ?></option>
						<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
						<?php } ?>
					  </select>
					</div>
				  </div>
			    </div>				  
			    <div class="panel-footer">
				  <div class="text-right">
				    <div class="btn-group">
					  <a id="save-setting-general" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
					  <a id="clear-setting-general" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
				      <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
					</div>
				  </div>
			    </div>
			  </div>
		    </div>
		    <div class="tab-pane" id="tab-design">
			  <div class="panel panel-default">
			    <div class="panel-body">
				  <div class="col-md-3">
					<ul class="nav nav-pills nav-stacked">
					  <li class="active"><a href="#tab-module-box-design" data-toggle="tab"><?php echo $entry_box_module_design; ?></a></li>
					  <li><a href="#tab-module-title-design" data-toggle="tab"><?php echo $entry_title_design; ?></a></li>
					  <li><a href="#tab-module-tabs-design" data-toggle="tab"><?php echo $entry_tabs_design; ?></a></li>
					  <li><a href="#tab-module-tabs-active-design" data-toggle="tab"><?php echo $entry_tabs_active_design; ?></a></li>
					  <li><a href="#tab-box-product-design" data-toggle="tab"><?php echo $entry_box_product_design; ?></a></li>
					  <li><a href="#tab-box-product-hover-design" data-toggle="tab"><?php echo $entry_box_product_hover_design; ?></a></li>
					  <li><a href="#tab-box-product-name-design" data-toggle="tab"><?php echo $entry_box_product_name_design; ?></a></li>
					  <li><a href="#tab-box-product-name-hover-design" data-toggle="tab"><?php echo $entry_box_product_name_hover_design; ?></a></li>
					  <li><a href="#tab-box-product-price-design" data-toggle="tab"><?php echo $entry_box_product_price_design; ?></a></li>
					  <li><a href="#tab-box-product-price-old-design" data-toggle="tab"><?php echo $entry_box_product_price_old_design; ?></a></li>
					  <li><a href="#tab-box-product-cart-design" data-toggle="tab"><?php echo $entry_cart_design; ?></a></li>
					  <li><a href="#tab-box-product-cart-hover-design" data-toggle="tab"><?php echo $entry_cart_hover_design; ?></a></li>
					  <li><a href="#tab-buttons-design" data-toggle="tab"><?php echo $entry_buttons_design; ?></a></li>
					  <li><a href="#tab-buttons-hover-design" data-toggle="tab"><?php echo $entry_buttons_hover_design; ?></a></li>
					  <li><a href="#tab-btn-carousel-design" data-toggle="tab"><?php echo $entry_btn_carousel_design; ?></a></li>
					  <li><a href="#tab-btn-carousel-hover-design" data-toggle="tab"><?php echo $entry_btn_carousel_hover_design; ?></a></li>
					</ul>
				  </div>
				  <div class="col-md-9">
					<div class="tab-content">
					  <div class="tab-pane active" id="tab-module-box-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-box"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-box" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_box; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="module_image_box" value="<?php echo $module_image_box; ?>"  id="input-image-box" />
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="bg_box_color" value="<?php echo $bg_box_color; ?>" class="form-control" />
							    </div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="box_border_weight" class="form-control">
								  <?php if ($box_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($box_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($box_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($box_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($box_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($box_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="box_border_style" class="form-control">
								  <?php if ($box_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($box_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($box_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_border_color" value="<?php echo $box_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_margin; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_margin_top" value="<?php echo $box_margin_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_margin_right" value="<?php echo $box_margin_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_margin_bottom" value="<?php echo $box_margin_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							   <label class="control-label"><?php echo $entry_left; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_margin_left" value="<?php echo $box_margin_left; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
						</div>											
					  </div>
					  <div class="tab-pane" id="tab-module-title-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-header"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-header" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_header; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="module_image_header" value="<?php echo $module_image_header; ?>"  id="input-image-header" />
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_bg_top; ?></label>
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="bg_title_top" value="<?php echo $bg_title_top; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_bg_bottom; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="bg_title_bottom" value="<?php echo $bg_title_bottom; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="title_padding_top" value="<?php echo $title_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="title_padding_right" value="<?php echo $title_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="title_padding_bottom" value="<?php echo $title_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left; ?></label>
							   <div class="input-group">
								 <input type="text" name="title_padding_left" value="<?php echo $title_padding_left; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_title_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="title_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($title_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($title_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($title_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($title_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($title_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($title_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="title_text_color" value="<?php echo $title_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="title_text_shadow" value="<?php echo $title_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="title_font_size" value="<?php echo $title_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="title_font_weight" class="form-control">
								  <?php if ($title_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($title_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="title_font_style" class="form-control">
								  <?php if ($title_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($title_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_transform; ?></label>
							  <div class="col-sm-3">
							    <select name="title_text_transform" class="form-control">
								  <?php if ($title_text_transform == 'none') { ?>
								    <option value="none" selected="selected"><?php echo $text_normal_text; ?></option>
								    <?php } else { ?>
								    <option value="none"><?php echo $text_normal_text; ?></option>
								    <?php } ?>
								    <?php if ($title_text_transform == 'capitalize') { ?>
								    <option value="capitalize" selected="selected"><?php echo $text_capitalize; ?></option>
								    <?php } else { ?>
								    <option value="capitalize"><?php echo $text_capitalize; ?></option>
									<?php } ?>
									<?php if ($title_text_transform == 'uppercase') { ?>
								    <option value="uppercase" selected="selected"><?php echo $text_uppercase; ?></option>
								    <?php } else { ?>
								    <option value="uppercase"><?php echo $text_uppercase; ?></option>
									 <?php } ?>
								    <?php if ($title_text_transform == 'lowercase') { ?>
								    <option value="lowercase" selected="selected"><?php echo $text_lowercase; ?></option>
								    <?php } else { ?>
								    <option value="lowercase"><?php echo $text_lowercase; ?></option>
								  <?php } ?>
							    </select>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-title" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-title" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-module-tabs-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_tabs_width; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group">
								  <input type="text" name="tabs_width" value="<?php echo $tabs_width; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_padding_top; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group">
								  <input type="text" name="box_tabs_padding_top" value="<?php echo $box_tabs_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_tabs_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="tabs_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($tabs_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($tabs_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($tabs_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($tabs_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($tabs_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($tabs_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="tabs_text_color" value="<?php echo $tabs_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="tabs_text_shadow" value="<?php echo $tabs_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_font_size" value="<?php echo $tabs_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="tabs_font_weight" class="form-control">
								  <?php if ($tabs_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($tabs_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="tabs_font_style" class="form-control">
								  <?php if ($tabs_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($tabs_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_margin; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_margin_top" value="<?php echo $tabs_margin_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_margin_right" value="<?php echo $tabs_margin_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_margin_bottom" value="<?php echo $tabs_margin_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							   <label class="control-label"><?php echo $entry_left; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_margin_left" value="<?php echo $tabs_margin_left; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_padding_top" value="<?php echo $tabs_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_padding_right" value="<?php echo $tabs_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_padding_bottom" value="<?php echo $tabs_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							   <label class="control-label"><?php echo $entry_left; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_padding_left" value="<?php echo $tabs_padding_left; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-tabs" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-tabs" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-module-tabs-active-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-tabs"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-tabs" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_tabs; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="image_bg_tabs" value="<?php echo $image_bg_tabs; ?>"  id="input-image-tabs" />
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="bg_box_color_tabs" value="<?php echo $bg_box_color_tabs; ?>" class="form-control" />
							    </div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_tabs_active_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="tabs_active_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($tabs_active_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($tabs_active_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($tabs_active_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($tabs_active_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($tabs_active_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($tabs_active_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="tabs_active_text_color" value="<?php echo $tabs_active_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="tabs_active_text_shadow" value="<?php echo $tabs_active_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_active_font_size" value="<?php echo $tabs_active_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="tabs_active_font_weight" class="form-control">
								  <?php if ($tabs_active_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($tabs_active_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="tabs_active_font_style" class="form-control">
								  <?php if ($tabs_active_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($tabs_active_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_active_padding_top" value="<?php echo $tabs_active_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_active_padding_right" value="<?php echo $tabs_active_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_active_padding_bottom" value="<?php echo $tabs_active_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							   <label class="control-label"><?php echo $entry_left; ?></label>
							    <div class="input-group">
								  <input type="text" name="tabs_active_padding_left" value="<?php echo $tabs_active_padding_left; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="tabs_border_weight" class="form-control">
								  <?php if ($tabs_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($tabs_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($tabs_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($tabs_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($tabs_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($tabs_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="tabs_border_style" class="form-control">
								  <?php if ($tabs_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($tabs_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($tabs_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="tabs_border_color" value="<?php echo $tabs_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-tabs-active" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-tabs-active" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-box-product-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-box-product"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-box-product" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_box_product; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="image_bg_box_product" value="<?php echo $image_bg_box_product; ?>"  id="input-image-box-product" />
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="bg_box_product_color" value="<?php echo $bg_box_product_color; ?>" class="form-control" />
							    </div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_margin; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_margin_top" value="<?php echo $box_product_margin_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_margin_right" value="<?php echo $box_product_margin_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_margin_bottom" value="<?php echo $box_product_margin_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							   <label class="control-label"><?php echo $entry_left; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_margin_left" value="<?php echo $box_product_margin_left; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_padding_top" value="<?php echo $box_product_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_padding_right" value="<?php echo $box_product_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_padding_bottom" value="<?php echo $box_product_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							   <label class="control-label"><?php echo $entry_left; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_padding_left" value="<?php echo $box_product_padding_left; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="box_product_border_weight" class="form-control">
								  <?php if ($box_product_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($box_product_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($box_product_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($box_product_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($box_product_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($box_product_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="box_product_border_style" class="form-control">
								  <?php if ($box_product_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($box_product_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($box_product_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_border_color" value="<?php echo $box_product_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-box-product-hover-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-box-product-hover"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-box-product-hover" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_box_product_hover; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="image_bg_box_product_hover" value="<?php echo $image_bg_box_product_hover; ?>"  id="input-image-box-product-hover" />
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="bg_box_product_hover_color" value="<?php echo $bg_box_product_hover_color; ?>" class="form-control" />
							    </div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="box_product_hover_border_weight" class="form-control">
								  <?php if ($box_product_hover_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($box_product_hover_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($box_product_hover_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($box_product_hover_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($box_product_hover_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($box_product_hover_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="box_product_hover_border_style" class="form-control">
								  <?php if ($box_product_hover_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($box_product_hover_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($box_product_hover_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_hover_border_color" value="<?php echo $box_product_hover_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-hover" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-hover" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-box-product-name-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_name_padding_top" value="<?php echo $box_product_name_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_name_padding_right" value="<?php echo $box_product_name_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_name_padding_bottom" value="<?php echo $box_product_name_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left; ?></label>
							   <div class="input-group">
								 <input type="text" name="box_product_name_padding_left" value="<?php echo $box_product_name_padding_left; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_name_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="box_product_name_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($box_product_name_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($box_product_name_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($box_product_name_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($box_product_name_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($box_product_name_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($box_product_name_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_name_text_color" value="<?php echo $box_product_name_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_name_text_shadow" value="<?php echo $box_product_name_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_name_font_size" value="<?php echo $box_product_name_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="box_product_name_font_weight" class="form-control">
								  <?php if ($box_product_name_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($box_product_name_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="box_product_name_font_style" class="form-control">
								  <?php if ($box_product_name_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($box_product_name_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-name" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-name" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-box-product-name-hover-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_name_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="box_product_name_hover_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($box_product_name_hover_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($box_product_name_hover_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($box_product_name_hover_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($box_product_name_hover_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($box_product_name_hover_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($box_product_name_hover_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_name_hover_text_color" value="<?php echo $box_product_name_hover_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_name_hover_text_shadow" value="<?php echo $box_product_name_hover_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_name_hover_font_size" value="<?php echo $box_product_name_hover_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="box_product_name_hover_font_weight" class="form-control">
								  <?php if ($box_product_name_hover_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($box_product_name_hover_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="box_product_name_hover_font_style" class="form-control">
								  <?php if ($box_product_name_hover_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($box_product_name_hover_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-name-hover" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-name-hover" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-box-product-price-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_price_padding_top" value="<?php echo $box_product_price_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_price_padding_right" value="<?php echo $box_product_price_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_price_padding_bottom" value="<?php echo $box_product_price_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left; ?></label>
							   <div class="input-group">
								 <input type="text" name="box_product_price_padding_left" value="<?php echo $box_product_price_padding_left; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_price_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="box_product_price_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($box_product_price_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($box_product_price_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($box_product_price_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($box_product_price_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($box_product_price_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($box_product_price_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_price_text_color" value="<?php echo $box_product_price_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_price_text_shadow" value="<?php echo $box_product_price_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_price_font_size" value="<?php echo $box_product_price_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="box_product_price_font_weight" class="form-control">
								  <?php if ($box_product_price_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($box_product_price_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="box_product_price_font_style" class="form-control">
								  <?php if ($box_product_price_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($box_product_price_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-price" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-price" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-box-product-price-old-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_price_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="box_product_price_old_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($box_product_price_old_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($box_product_price_old_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($box_product_price_old_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($box_product_price_old_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($box_product_price_old_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($box_product_price_old_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_price_old_text_color" value="<?php echo $box_product_price_old_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_price_old_text_shadow" value="<?php echo $box_product_price_old_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_price_old_font_size" value="<?php echo $box_product_price_old_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="box_product_price_old_font_weight" class="form-control">
								  <?php if ($box_product_price_old_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($box_product_price_old_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="box_product_price_old_font_style" class="form-control">
								  <?php if ($box_product_price_old_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($box_product_price_old_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-price-old" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-price-old" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-box-product-cart-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-cart"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-cart" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_box_product_cart; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="box_product_cart_image" value="<?php echo $box_product_cart_image; ?>"  id="input-image-cart" />
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_cart_bg_color_top" value="<?php echo $box_product_cart_bg_color_top; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_cart_padding_top" value="<?php echo $box_product_cart_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_cart_padding_right" value="<?php echo $box_product_cart_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_cart_padding_bottom" value="<?php echo $box_product_cart_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left; ?></label>
							   <div class="input-group">
								 <input type="text" name="box_product_cart_padding_left" value="<?php echo $box_product_cart_padding_left; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_cart_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="box_product_cart_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($box_product_cart_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($box_product_cart_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($box_product_cart_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($box_product_cart_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($box_product_cart_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($box_product_cart_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_cart_text_color" value="<?php echo $box_product_cart_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_cart_text_shadow" value="<?php echo $box_product_cart_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_cart_font_size" value="<?php echo $box_product_cart_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="box_product_cart_font_weight" class="form-control">
								  <?php if ($box_product_cart_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($box_product_cart_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="box_product_cart_font_style" class="form-control">
								  <?php if ($box_product_cart_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($box_product_cart_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="box_product_cart_border_weight" class="form-control">
								  <?php if ($box_product_cart_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="box_product_cart_border_style" class="form-control">
								  <?php if ($box_product_cart_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($box_product_cart_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($box_product_cart_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_cart_border_color" value="<?php echo $box_product_cart_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="cart_border_radius_left_top" value="<?php echo $cart_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="cart_border_radius_right_top" value="<?php echo $cart_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="cart_border_radius_right_bottom" value="<?php echo $cart_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							   <div class="input-group">
								 <input type="text" name="cart_border_radius_left_bottom" value="<?php echo $cart_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_margin_bottom; ?></label>
							  <div class="col-sm-3">
							   <div class="input-group">
								 <input type="text" name="cart_margin_bottom" value="<?php echo $cart_margin_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-cart" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-cart" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-box-product-cart-hover-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-cart-hover"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-cart-hover" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_box_product_cart_hover; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="box_product_cart_hover_image" value="<?php echo $box_product_cart_hover_image; ?>"  id="input-image-cart-hover" />
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_cart_hover_bg_color_top" value="<?php echo $box_product_cart_hover_bg_color_top; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_cart_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="box_product_cart_hover_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($box_product_cart_hover_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($box_product_cart_hover_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($box_product_cart_hover_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($box_product_cart_hover_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($box_product_cart_hover_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($box_product_cart_hover_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_cart_hover_text_color" value="<?php echo $box_product_cart_hover_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_cart_hover_text_shadow" value="<?php echo $box_product_cart_hover_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="box_product_cart_hover_font_size" value="<?php echo $box_product_cart_hover_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="box_product_cart_hover_font_weight" class="form-control">
								  <?php if ($box_product_cart_hover_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($box_product_cart_hover_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="box_product_cart_hover_font_style" class="form-control">
								  <?php if ($box_product_cart_hover_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($box_product_cart_hover_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="box_product_cart_hover_border_weight" class="form-control">
								  <?php if ($box_product_cart_hover_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_hover_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_hover_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_hover_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_hover_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($box_product_cart_hover_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="box_product_cart_hover_border_style" class="form-control">
								  <?php if ($box_product_cart_hover_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($box_product_cart_hover_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($box_product_cart_hover_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_cart_hover_border_color" value="<?php echo $box_product_cart_hover_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="cart_hover_border_radius_left_top" value="<?php echo $cart_hover_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="cart_hover_border_radius_right_top" value="<?php echo $cart_hover_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="cart_hover_border_radius_right_bottom" value="<?php echo $cart_hover_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							   <div class="input-group">
								 <input type="text" name="cart_hover_border_radius_left_bottom" value="<?php echo $cart_hover_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-cart-hover" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-cart-hover" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-buttons-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_buttons_bg_color" value="<?php echo $box_product_buttons_bg_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <label class="col-sm-1 control-label"><?php echo $entry_color; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_buttons_text_color" value="<?php echo $box_product_buttons_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <label class="col-sm-1 control-label"><?php echo $entry_shadow; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_buttons_text_shadow" value="<?php echo $box_product_buttons_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="box_product_buttons_border_weight" class="form-control">
								  <?php if ($box_product_buttons_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="box_product_buttons_border_style" class="form-control">
								  <?php if ($box_product_buttons_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($box_product_buttons_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($box_product_buttons_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_buttons_border_color" value="<?php echo $box_product_buttons_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="buttons_border_radius_left_top" value="<?php echo $buttons_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="buttons_border_radius_right_top" value="<?php echo $buttons_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="buttons_border_radius_right_bottom" value="<?php echo $buttons_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							    <div class="input-group">
								 <input type="text" name="buttons_border_radius_left_bottom" value="<?php echo $buttons_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-buttons" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-buttons" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-buttons-hover-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_buttons_hover_bg_color" value="<?php echo $box_product_buttons_hover_bg_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <label class="col-sm-1 control-label"><?php echo $entry_color; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_buttons_hover_text_color" value="<?php echo $box_product_buttons_hover_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <label class="col-sm-1 control-label"><?php echo $entry_shadow; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_buttons_hover_text_shadow" value="<?php echo $box_product_buttons_hover_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="box_product_buttons_hover_border_weight" class="form-control">
								  <?php if ($box_product_buttons_hover_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_hover_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_hover_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_hover_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_hover_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($box_product_buttons_hover_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="box_product_buttons_hover_border_style" class="form-control">
								  <?php if ($box_product_buttons_hover_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($box_product_buttons_hover_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($box_product_buttons_hover_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="box_product_buttons_hover_border_color" value="<?php echo $box_product_buttons_hover_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="buttons_hover_border_radius_left_top" value="<?php echo $buttons_hover_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="buttons_hover_border_radius_right_top" value="<?php echo $buttons_hover_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="buttons_hover_border_radius_right_bottom" value="<?php echo $buttons_hover_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							    <div class="input-group">
								 <input type="text" name="buttons_hover_border_radius_left_bottom" value="<?php echo $buttons_hover_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-box-product-buttons-hover" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-box-product-buttons-hover" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-btn-carousel-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="btn_carousel_bg_color" value="<?php echo $btn_carousel_bg_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <label class="col-sm-1 control-label"><?php echo $entry_color; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="btn_carousel_text_color" value="<?php echo $btn_carousel_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <label class="col-sm-1 control-label"><?php echo $entry_shadow; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="btn_carousel_text_shadow" value="<?php echo $btn_carousel_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="btn_carousel_border_weight" class="form-control">
								  <?php if ($btn_carousel_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="btn_carousel_border_style" class="form-control">
								  <?php if ($btn_carousel_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($btn_carousel_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($btn_carousel_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="btn_carousel_border_color" value="<?php echo $btn_carousel_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius_left; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_l_border_radius_left_top" value="<?php echo $btn_carousel_l_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_l_border_radius_right_top" value="<?php echo $btn_carousel_l_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_l_border_radius_right_bottom" value="<?php echo $btn_carousel_l_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							    <div class="input-group">
								 <input type="text" name="btn_carousel_l_border_radius_left_bottom" value="<?php echo $btn_carousel_l_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius_right; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_r_border_radius_left_top" value="<?php echo $btn_carousel_r_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_r_border_radius_right_top" value="<?php echo $btn_carousel_r_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_r_border_radius_right_bottom" value="<?php echo $btn_carousel_r_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							    <div class="input-group">
								 <input type="text" name="btn_carousel_r_border_radius_left_bottom" value="<?php echo $btn_carousel_r_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-btn-carousel" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-btn-carousel" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-btn-carousel-hover-design">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="btn_carousel_hover_bg_color" value="<?php echo $btn_carousel_hover_bg_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <label class="col-sm-1 control-label"><?php echo $entry_color; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="btn_carousel_hover_text_color" value="<?php echo $btn_carousel_hover_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <label class="col-sm-1 control-label"><?php echo $entry_shadow; ?></label>
							  <div class="col-sm-2">
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="btn_carousel_hover_text_shadow" value="<?php echo $btn_carousel_hover_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="btn_carousel_hover_border_weight" class="form-control">
								  <?php if ($btn_carousel_hover_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_hover_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_hover_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_hover_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_hover_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($btn_carousel_hover_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="btn_carousel_hover_border_style" class="form-control">
								  <?php if ($btn_carousel_hover_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($btn_carousel_hover_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($btn_carousel_hover_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="btn_carousel_hover_border_color" value="<?php echo $btn_carousel_hover_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius_left; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_hover_l_border_radius_left_top" value="<?php echo $btn_carousel_hover_l_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_hover_l_border_radius_right_top" value="<?php echo $btn_carousel_hover_l_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_hover_l_border_radius_right_bottom" value="<?php echo $btn_carousel_hover_l_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							    <div class="input-group">
								 <input type="text" name="btn_carousel_hover_l_border_radius_left_bottom" value="<?php echo $btn_carousel_hover_l_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius_right; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_hover_r_border_radius_left_top" value="<?php echo $btn_carousel_hover_r_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_hover_r_border_radius_right_top" value="<?php echo $btn_carousel_hover_r_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="btn_carousel_hover_r_border_radius_right_bottom" value="<?php echo $btn_carousel_hover_r_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							    <div class="input-group">
								 <input type="text" name="btn_carousel_hover_r_border_radius_left_bottom" value="<?php echo $btn_carousel_hover_r_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-btn-carousel-hover" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-btn-carousel-hover" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
		    </div>
			<div class="tab-pane" id="tab-design-quick-view">
			  <div class="panel panel-default">
			    <div class="panel-body">
				  <div class="col-md-3">
					<ul class="nav nav-pills nav-stacked">
					  <li class="active"><a href="#tab-quick-product-name" data-toggle="tab"><?php echo $entry_box_product_name_design; ?></a></li>
					  <li><a href="#tab-quick-product-name-data" data-toggle="tab"><?php echo $entry_name_data_design; ?></a></li>
					  <li><a href="#tab-quick-product-data-value" data-toggle="tab"><?php echo $entry_value_data_design; ?></a></li>
					  <li><a href="#tab-quick-product-price" data-toggle="tab"><?php echo $entry_box_product_price_design; ?></a></li>
					  <li><a href="#tab-quick-product-price-old" data-toggle="tab"><?php echo $entry_box_product_price_old_design; ?></a></li>
					  <li><a href="#tab-quick-product-price-other" data-toggle="tab"><?php echo $entry_box_product_price_other; ?></a></li>
					  <li><a href="#tab-quick-buttons" data-toggle="tab"><?php echo $entry_quick_buttons; ?></a></li>
					  <li><a href="#tab-quick-buttons-hover" data-toggle="tab"><?php echo $entry_quick_buttons_hover; ?></a></li>
					</ul>
				  </div>
				  <div class="col-md-9">
					<div class="tab-content">
					  <div class="tab-pane active" id="tab-quick-product-name">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="product_name_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($product_name_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($product_name_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($product_name_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($product_name_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($product_name_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($product_name_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="product_name_text_color" value="<?php echo $product_name_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="product_name_text_shadow" value="<?php echo $product_name_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="product_name_font_size" value="<?php echo $product_name_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="product_name_font_weight" class="form-control">
								  <?php if ($product_name_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($product_name_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="product_name_font_style" class="form-control">
								  <?php if ($product_name_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($product_name_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-quick-product-name" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-quick-product-name" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-quick-product-name-data">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="product_name_data_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($product_name_data_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($product_name_data_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($product_name_data_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($product_name_data_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($product_name_data_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($product_name_data_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="product_name_data_text_color" value="<?php echo $product_name_data_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="product_name_data_text_shadow" value="<?php echo $product_name_data_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="product_name_data_font_size" value="<?php echo $product_name_data_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="product_name_data_font_weight" class="form-control">
								  <?php if ($product_name_data_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($product_name_data_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="product_name_data_font_style" class="form-control">
								  <?php if ($product_name_data_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($product_name_data_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-quick-product-name-data" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-quick-product-name-data" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-quick-product-data-value">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="product_name_value_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($product_name_value_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($product_name_value_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($product_name_value_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($product_name_value_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($product_name_value_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($product_name_value_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="product_name_value_text_color" value="<?php echo $product_name_value_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="product_name_value_text_shadow" value="<?php echo $product_name_value_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="product_name_value_font_size" value="<?php echo $product_name_value_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="product_name_value_font_weight" class="form-control">
								  <?php if ($product_name_value_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($product_name_value_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="product_name_value_font_style" class="form-control">
								  <?php if ($product_name_value_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($product_name_value_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-quick-product-data-value" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-quick-product-data-value" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-quick-product-price">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_price_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="quick_product_price_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($quick_product_price_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($quick_product_price_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($quick_product_price_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($quick_product_price_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($quick_product_price_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($quick_product_price_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="quick_product_price_text_color" value="<?php echo $quick_product_price_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="quick_product_price_text_shadow" value="<?php echo $quick_product_price_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="quick_product_price_font_size" value="<?php echo $quick_product_price_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="quick_product_price_font_weight" class="form-control">
								  <?php if ($quick_product_price_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($quick_product_price_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="quick_product_price_font_style" class="form-control">
								  <?php if ($quick_product_price_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($quick_product_price_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-quick-product-price" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-quick-product-price" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-quick-product-price-old">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_price_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="quick_product_price_old_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($quick_product_price_old_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($quick_product_price_old_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($quick_product_price_old_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($quick_product_price_old_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($quick_product_price_old_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($quick_product_price_old_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="quick_product_price_old_text_color" value="<?php echo $quick_product_price_old_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="quick_product_price_old_text_shadow" value="<?php echo $quick_product_price_old_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="quick_product_price_old_font_size" value="<?php echo $quick_product_price_old_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="quick_product_price_old_font_weight" class="form-control">
								  <?php if ($quick_product_price_old_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($quick_product_price_old_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="quick_product_price_old_font_style" class="form-control">
								  <?php if ($quick_product_price_old_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($quick_product_price_old_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-quick-product-price-old" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-quick-product-price-old" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-quick-product-price-other">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_price_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="quick_product_price_other_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($quick_product_price_other_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($quick_product_price_other_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($quick_product_price_other_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($quick_product_price_other_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($quick_product_price_other_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($quick_product_price_other_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="quick_product_price_other_text_color" value="<?php echo $quick_product_price_other_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="quick_product_price_other_text_shadow" value="<?php echo $quick_product_price_other_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="quick_product_price_other_font_size" value="<?php echo $quick_product_price_other_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="quick_product_price_other_font_weight" class="form-control">
								  <?php if ($quick_product_price_other_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($quick_product_price_other_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="quick_product_price_other_font_style" class="form-control">
								  <?php if ($quick_product_price_other_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($quick_product_price_other_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-quick-product-price-other" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-quick-product-price-other" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-quick-buttons">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-quick-buttons"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-quick-buttons" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_modal_buttons; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="modal_buttons_image" value="<?php echo $modal_buttons_image; ?>"  id="input-image-quick-buttons" />
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="modal_buttons_bg_color" value="<?php echo $modal_buttons_bg_color; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_cart_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="modal_buttons_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($modal_buttons_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($modal_buttons_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($modal_buttons_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($modal_buttons_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($modal_buttons_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($modal_buttons_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="modal_buttons_text_color" value="<?php echo $modal_buttons_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="modal_buttons_text_shadow" value="<?php echo $modal_buttons_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_font_size" value="<?php echo $modal_buttons_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="modal_buttons_font_weight" class="form-control">
								  <?php if ($modal_buttons_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($modal_buttons_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="modal_buttons_font_style" class="form-control">
								  <?php if ($modal_buttons_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($modal_buttons_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="modal_buttons_border_weight" class="form-control">
								  <?php if ($modal_buttons_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="modal_buttons_border_style" class="form-control">
								  <?php if ($modal_buttons_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($modal_buttons_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($modal_buttons_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="modal_buttons_border_color" value="<?php echo $modal_buttons_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_padding_top" value="<?php echo $modal_buttons_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_padding_right" value="<?php echo $modal_buttons_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_padding_bottom" value="<?php echo $modal_buttons_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left; ?></label>
							   <div class="input-group">
								 <input type="text" name="modal_buttons_padding_left" value="<?php echo $modal_buttons_padding_left; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_border_radius_left_top" value="<?php echo $modal_buttons_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_border_radius_right_top" value="<?php echo $modal_buttons_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_border_radius_right_bottom" value="<?php echo $modal_buttons_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							   <div class="input-group">
								 <input type="text" name="modal_buttons_border_radius_left_bottom" value="<?php echo $modal_buttons_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-quick-buttons" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-quick-buttons" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					  <div class="tab-pane" id="tab-quick-buttons-hover">
					    <div class="panel panel-default">
						  <div class="panel-body">
							<div class="form-group">
							  <label class="col-sm-3 control-label" for="input-image-quick-buttons-hover"><?php echo $entry_bg_image; ?></label>
							  <div class="col-sm-3">
							    <a href="" id="module-image-quick-buttons-hover" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb_modal_buttons_hover; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" style="width: 50px; height: 50px;" /></a>
							    <input type="hidden" name="modal_buttons_hover_image" value="<?php echo $modal_buttons_hover_image; ?>"  id="input-image-quick-buttons-hover" />
							  </div>
							  <label class="col-sm-3 control-label"><?php echo $entry_bg_color; ?></label>
							  <div class="col-sm-3">
							    <div class="input-group setting-colorpicker">
								 <span class="input-group-addon"><i></i></span>
								  <input type="text" name="modal_buttons_hover_bg_color" value="<?php echo $modal_buttons_hover_bg_color; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_product_cart_text; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font; ?></label>
							    <select name="modal_buttons_hover_text_font" class="form-control">
								  <optgroup label="<?php echo $text_google_font; ?>">
								    <?php if ($modal_buttons_hover_text_font == 'Jura') { ?>
									  <option value="Jura" selected="selected">Jura</option>
									  <?php } else { ?>
									  <option value="Jura">Jura</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Ubuntu') { ?>
									  <option value="Ubuntu" selected="selected">Ubuntu</option>
									  <?php } else { ?>
									  <option value="Ubuntu">Ubuntu</option>
									  <?php } ?>	
									  <?php if ($modal_buttons_hover_text_font == 'Lobster') { ?>
									  <option value="Lobster" selected="selected">Lobster</option>
									  <?php } else { ?>
									  <option value="Lobster">Lobster</option>
									  <?php } ?>							
									  <?php if ($modal_buttons_hover_text_font == 'Cuprum') { ?>
									  <option value="Cuprum" selected="selected">Cuprum</option>
									  <?php } else { ?>
									  <option value="Cuprum">Cuprum</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Open Sans') { ?>
									  <option value="Open Sans" selected="selected">Open Sans</option>
									  <?php } else { ?>
									  <option value="Open Sans">Open Sans</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Kelly Slab') { ?>
									  <option value="Kelly Slab" selected="selected">Kelly Slab</option>
									  <?php } else { ?>
									  <option value="Kelly Slab">Kelly Slab</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Comfortaa') { ?>
									  <option value="Comfortaa" selected="selected">Comfortaa</option>
									  <?php } else { ?>
									  <option value="Comfortaa">Comfortaa</option>
									  <?php } ?>	
									  <?php if ($modal_buttons_hover_text_font == 'Russo One') { ?>
									  <option value="Russo One" selected="selected">Russo One</option>
									  <?php } else { ?>
									  <option value="Russo One">Russo One</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Yeseva One') { ?>
									  <option value="Yeseva One" selected="selected">Yeseva One</option>
									  <?php } else { ?>
									  <option value="Yeseva One">Yeseva One</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Stalinist One') { ?>
									  <option value="Stalinist One" selected="selected">Stalinist One</option>
									  <?php } else { ?>
									  <option value="Stalinist One">Stalinist One</option>
									<?php } ?>
								  </optgroup>
								  <optgroup label="<?php echo $text_standart_font; ?>">
								    <?php if ($modal_buttons_hover_text_font == 'Arial') { ?>
									  <option value="Arial" selected="selected">Arial</option>
									  <?php } else { ?>
									  <option value="Arial">Arial</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Verdana') { ?>
									  <option value="Verdana" selected="selected">Verdana</option>
									  <?php } else { ?>
									  <option value="Verdana">Verdana</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Helvetica') { ?>
									  <option value="Helvetica" selected="selected">Helvetica</option>
									  <?php } else { ?>
									  <option value="Helvetica">Helvetica</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Lucida Grande') { ?>
									  <option value="Lucida Grande" selected="selected">Lucida Grande</option>
									  <?php } else { ?>
									  <option value="Lucida Grande">Lucida Grande</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Trebuchet MS') { ?>
									  <option value="Trebuchet MS" selected="selected">Trebuchet MS</option>
									  <?php } else { ?>
									  <option value="Trebuchet MS">Trebuchet MS</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Times New Roman') { ?>
									  <option value="Times New Roman" selected="selected">Times New Roman</option>
									  <?php } else { ?>
									  <option value="Times New Roman">Times New Roman</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Tahoma') { ?>
									  <option value="Tahoma" selected="selected">Tahoma</option>
									  <?php } else { ?>
									  <option value="Tahoma">Tahoma</option>
									  <?php } ?>
									  <?php if ($modal_buttons_hover_text_font == 'Georgia') { ?>
									  <option value="Georgia" selected="selected">Georgia</option>
									  <?php } else { ?>
									  <option value="Georgia">Georgia</option>
									<?php } ?>
								 </optgroup>
							    </select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_color; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="modal_buttons_hover_text_color" value="<?php echo $modal_buttons_hover_text_color; ?>" class="form-control" />
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_shadow; ?></label>
							    <div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="modal_buttons_hover_text_shadow" value="<?php echo $modal_buttons_hover_text_shadow; ?>" class="form-control" />
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_text_style; ?></label>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_size; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_hover_font_size" value="<?php echo $modal_buttons_hover_font_size; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_weight; ?></label>
							    <select name="modal_buttons_hover_font_weight" class="form-control">
								  <?php if ($modal_buttons_hover_font_weight == 'normal') { ?>
									<option value="normal" selected="selected"><?php echo $text_normal; ?></option>
									<?php } else { ?>
									<option value="normal"><?php echo $text_normal; ?></option>
									<?php } ?>
									<?php if ($modal_buttons_hover_font_weight == 'bold') { ?>
									<option value="bold" selected="selected"><?php echo $text_bold; ?></option>
									<?php } else { ?>
									<option value="bold"><?php echo $text_bold; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
							    <label class="control-label"><?php echo $entry_font_style; ?></label>
							    <select name="modal_buttons_hover_font_style" class="form-control">
								  <?php if ($modal_buttons_hover_font_style == 'normal') { ?>
								    <option value="normal" selected="selected"><?php echo $text_normal; ?></option>
								    <?php } else { ?>
								    <option value="normal"><?php echo $text_normal; ?></option>
								    <?php } ?>
								    <?php if ($modal_buttons_hover_font_style == 'italic') { ?>
								    <option value="italic" selected="selected"><?php echo $text_italic; ?></option>
								    <?php } else { ?>
								    <option value="italic"><?php echo $text_italic; ?></option>
								  <?php } ?>
							    </select>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_box_border_weight; ?></label>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_weight; ?></label>					  
								<select name="modal_buttons_hover_border_weight" class="form-control">
								  <?php if ($modal_buttons_hover_border_weight == '0') { ?>
								  <option value="0" selected="selected">0px</option>
								  <?php } else { ?>
								  <option value="0">0px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_hover_border_weight == '1') { ?>
								  <option value="1" selected="selected">1px</option>
								  <?php } else { ?>
								  <option value="1">1px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_hover_border_weight == '2') { ?>
								  <option value="2" selected="selected">2px</option>
								  <?php } else { ?>
								  <option value="2">2px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_hover_border_weight == '3') { ?>
								  <option value="3" selected="selected">3px</option>
								  <?php } else { ?>
								  <option value="3">3px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_hover_border_weight == '4') { ?>
								  <option value="4" selected="selected">4px</option>
								  <?php } else { ?>
								  <option value="4">4px</option>
								  <?php } ?>
								  <?php if ($modal_buttons_hover_border_weight == '5') { ?>
								  <option value="5" selected="selected">5px</option>
								  <?php } else { ?>
								  <option value="5">5px</option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_style; ?></label>
								<select name="modal_buttons_hover_border_style" class="form-control">
								  <?php if ($modal_buttons_hover_border_style == 'solid') { ?>
								  <option value="solid" selected="selected"><?php echo $text_solid; ?></option>
								  <?php } else { ?>
								  <option value="solid"><?php echo $text_solid; ?></option>
								  <?php } ?>
								  <?php if ($modal_buttons_hover_border_style == 'dotted') { ?>
								  <option value="dotted" selected="selected"><?php echo $text_dotted; ?></option>
								  <?php } else { ?>
								  <option value="dotted"><?php echo $text_dotted; ?></option>
								  <?php } ?>
								  <?php if ($modal_buttons_hover_border_style == 'dashed') { ?>
								  <option value="dashed" selected="selected"><?php echo $text_dashed; ?></option>
								  <?php } else { ?>
								  <option value="dashed"><?php echo $text_dashed; ?></option>
								  <?php } ?>
								</select>
							  </div>
							  <div class="col-sm-3">
								<label class="control-label"><?php echo $entry_color; ?></label>
								<div class="input-group setting-colorpicker">
								  <span class="input-group-addon"><i></i></span>
								  <input type="text" name="modal_buttons_hover_border_color" value="<?php echo $modal_buttons_hover_border_color; ?>" class="form-control" />
								</div>
							  </div>
							</div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_padding; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_hover_padding_top" value="<?php echo $modal_buttons_hover_padding_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_hover_padding_right" value="<?php echo $modal_buttons_hover_padding_right; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_hover_padding_bottom" value="<?php echo $modal_buttons_hover_padding_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left; ?></label>
							   <div class="input-group">
								 <input type="text" name="modal_buttons_hover_padding_left" value="<?php echo $modal_buttons_hover_padding_left; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
							<div class="form-group">
							  <label class="col-sm-3 control-label"><?php echo $entry_border_radius; ?></label>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_hover_border_radius_left_top" value="<?php echo $modal_buttons_hover_border_radius_left_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_top; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_hover_border_radius_right_top" value="<?php echo $modal_buttons_hover_border_radius_right_top; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							    </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_right_bottom; ?></label>
							    <div class="input-group">
								  <input type="text" name="modal_buttons_hover_border_radius_right_bottom" value="<?php echo $modal_buttons_hover_border_radius_right_bottom; ?>" class="form-control" />
								  <span class="input-group-addon">px;</span>
							   </div>
							  </div>
							  <div class="col-sm-2">
							    <label class="control-label"><?php echo $entry_left_bottom; ?></label>
							   <div class="input-group">
								 <input type="text" name="modal_buttons_hover_border_radius_left_bottom" value="<?php echo $modal_buttons_hover_border_radius_left_bottom; ?>" class="form-control" />
								 <span class="input-group-addon">px;</span>
							    </div>
							  </div>
						    </div>
						  </div>
						  <div class="panel-footer">
						    <div class="text-right">
							  <div class="btn-group">
							    <a id="save-design-quick-buttons-hover" data-form="form-maxy-category" class="button-save btn btn-primary btn-lg" data-toggle="tooltip" title="<?php echo $button_save; ?>"><i class="fa fa-save"></i></a>
							    <a id="clear-design-quick-buttons-hover" class="button-save btn btn-danger btn-lg"  data-toggle="tooltip" title="<?php echo $button_clear_setting; ?>"><i class="fa fa-eraser"></i></a>
							    <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default btn-lg"><i class="fa fa-reply"></i></a>
							  </div>
						    </div>
						  </div>
					    </div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
		    </div>
		  </div>
		</form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('#save-setting-general, #save-design-box, #save-design-title, #save-design-tabs, #save-design-tabs-active, #save-design-box-product, #save-design-box-product-hover, #save-design-box-product-name, #save-design-box-product-name-hover, #save-design-box-product-price, #save-design-box-product-price-old, #save-design-box-product-cart, #save-design-box-product-cart-hover, #save-design-box-product-buttons, #save-design-box-product-buttons-hover, #save-design-btn-carousel, #save-design-btn-carousel-hover, #save-design-quick-product-name, #save-design-quick-product-name-data, #save-design-quick-product-data-value, #save-design-quick-product-price, #save-design-quick-product-price-old, #save-design-quick-product-price-other, #save-design-quick-buttons, #save-design-quick-buttons-hover').on('click', function(){	
	var module_id = $('input[name=\'module_id\']');
	var url = 'index.php?route=extension/module&token=<?php echo $token; ?>';
		
	$.ajax({
		url: $('#' + $(this).data('form')).attr('action'),
		type: 'post',
		dataType: 'json',
		data: $('#' + $(this).data('form') + ' input[type=\'checkbox\']:checked, #' + $(this).data('form') + ' input[type=\'text\'], #' + $(this).data('form') + ' input[type=\'hidden\'], #' + $(this).data('form') + ' select'),
		success: function(json) {
			$('.alert-success, .alert-danger').remove();
										
			if (json['error']) {
				$('#alert-category').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
			}
										
			if (json['success']) {
				$('#alert-category').before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');				
			
				$('.alert-success').delay(1500).fadeOut(500);
				
				if (module_id.val() == '') {
					setTimeout(function(){	
						location = url;
					}, 500)
				}
			}
		}
	});
});  
//--></script>
<script type="text/javascript"><!--
$('#clear-setting-general').on('click', function(){
	$('#tab-data #module-image-banner').find('img').attr('src', $('#tab-data #module-image-banner').find('img').attr('data-placeholder'));
	$('#tab-data #module-image-banner').parent().find('input').attr('value', '');	
	$('#tab-data input').val('');
	$('#tab-data input[type=\'checkbox\']').prop('checked', true);
	$('#tab-data #category tbody').find('tr').remove();
	$('#tab-data select[name=\'icon_status\']').val('1');
	$('#tab-data select[name=\'status\']').val('1');
	$('#tab-data input[name=\'image_product_width\']').val('210');
	$('#tab-data input[name=\'image_product_height\']').val('210');
	$('#design-quick-view').removeClass('hide');	
	$('#tab-design-quick-view').removeClass('hide');
	$('#quick-view-data').removeClass('hide');
	$('#tab-data input[name=\'image_quick_width\']').val('350');
	$('#tab-data input[name=\'image_quick_height\']').val('350');
	$('#tab-data input[name=\'image_quick_additional_width\']').val('50');
	$('#tab-data input[name=\'image_quick_additional_height\']').val('50');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box').on('click', function(){
	$('#tab-module-box-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-module-box-design #module-image-box').find('img').attr('src', $('#tab-module-box-design #module-image-box').find('img').attr('data-placeholder'));
	$('#tab-module-box-design #module-image-box').parent().find('input').attr('value', '');	
	$('#tab-module-box-design input[name=\'bg_box_color\']').val('#f8f8f8');
	$('#tab-module-box-design select[name=\'box_border_weight\']').val('1');
	$('#tab-module-box-design select[name=\'box_border_style\']').val('solid');
	$('#tab-module-box-design input[name=\'box_border_color\']').val('#dddddd');
	$('#tab-module-box-design input[name=\'box_margin_top\']').val('0');
	$('#tab-module-box-design input[name=\'box_margin_right\']').val('0');
	$('#tab-module-box-design input[name=\'box_margin_bottom\']').val('20');
	$('#tab-module-box-design input[name=\'box_margin_left\']').val('0');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-title').on('click', function(){
	$('#tab-module-title-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-module-title-design #module-image-header').find('img').attr('src', $('#tab-module-title-design #module-image-header').find('img').attr('data-placeholder'));
	$('#tab-module-title-design #module-image-header').parent().find('input').attr('value', '');	
	$('#tab-module-title-design input[name=\'bg_title_top\']').val('#42a1d7');
	$('#tab-module-title-design input[name=\'bg_title_bottom\']').val('#3991c4');	
	$('#tab-module-title-design input[name=\'title_padding_top\']').val('30');
	$('#tab-module-title-design input[name=\'title_padding_right\']').val('10');
	$('#tab-module-title-design input[name=\'title_padding_bottom\']').val('30');
	$('#tab-module-title-design input[name=\'title_padding_left\']').val('20');
	$('#tab-module-title-design select[name=\'title_text_font\']').val('Jura');
	$('#tab-module-title-design input[name=\'title_text_color\']').val('#ffffff');
	$('#tab-module-title-design input[name=\'title_text_shadow\']').val('#333333');
	$('#tab-module-title-design input[name=\'title_font_size\']').val('16');
	$('#tab-module-title-design select[name=\'title_font_weight\']').val('normal');
	$('#tab-module-title-design select[name=\'title_font_style\']').val('normal');
	$('#tab-module-title-design select[name=\'title_text_transform\']').val('uppercase');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-tabs').on('click', function(){
	$('#tab-module-tabs-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-module-tabs-design input[name=\'tabs_width\']').val('250');
	$('#tab-module-tabs-design input[name=\'box_tabs_padding_top\']').val('15');
	$('#tab-module-tabs-design select[name=\'tabs_text_font\']').val('Comfortaa');
	$('#tab-module-tabs-design input[name=\'tabs_text_color\']').val('#888888');
	$('#tab-module-tabs-design input[name=\'tabs_text_shadow\']').val('#ffffff');
	$('#tab-module-tabs-design input[name=\'tabs_font_size\']').val('11');
	$('#tab-module-tabs-design select[name=\'tabs_font_weight\']').val('normal');
	$('#tab-module-tabs-design select[name=\'tabs_font_style\']').val('normal');
	$('#tab-module-tabs-design input[name=\'tabs_margin_top\']').val('0');
	$('#tab-module-tabs-design input[name=\'tabs_margin_right\']').val('0');
	$('#tab-module-tabs-design input[name=\'tabs_margin_bottom\']').val('0');
	$('#tab-module-tabs-design input[name=\'tabs_margin_left\']').val('5');
	$('#tab-module-tabs-design input[name=\'tabs_padding_top\']').val('7');
	$('#tab-module-tabs-design input[name=\'tabs_padding_right\']').val('15');
	$('#tab-module-tabs-design input[name=\'tabs_padding_bottom\']').val('7');
	$('#tab-module-tabs-design input[name=\'tabs_padding_left\']').val('15');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-tabs-active').on('click', function(){
	$('#tab-module-tabs-active-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-module-tabs-active-design #module-image-tabs').find('img').attr('src', $('#tab-module-tabs-active-design #module-image-tabs').find('img').attr('data-placeholder'));
	$('#tab-module-tabs-active-design #module-image-tabs').parent().find('input').attr('value', '');
	$('#tab-module-tabs-active-design input[name=\'bg_box_color_tabs\']').val('#ffffff');
	$('#tab-module-tabs-active-design select[name=\'tabs_active_text_font\']').val('Comfortaa');
	$('#tab-module-tabs-active-design input[name=\'tabs_active_text_color\']').val('#3991c4');
	$('#tab-module-tabs-active-design input[name=\'tabs_active_text_shadow\']').val('#ffffff');
	$('#tab-module-tabs-active-design input[name=\'tabs_active_font_size\']').val('11');
	$('#tab-module-tabs-active-design select[name=\'tabs_active_font_weight\']').val('normal');
	$('#tab-module-tabs-active-design select[name=\'tabs_active_font_style\']').val('normal');
	$('#tab-module-tabs-active-design input[name=\'tabs_active_padding_top\']').val('6');
	$('#tab-module-tabs-active-design input[name=\'tabs_active_padding_right\']').val('14');
	$('#tab-module-tabs-active-design input[name=\'tabs_active_padding_bottom\']').val('6');
	$('#tab-module-tabs-active-design input[name=\'tabs_active_padding_left\']').val('14');
	$('#tab-module-tabs-active-design select[name=\'tabs_border_weight\']').val('1');
	$('#tab-module-tabs-active-design select[name=\'tabs_border_style\']').val('solid');
	$('#tab-module-tabs-active-design input[name=\'tabs_border_color\']').val('#dddddd');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product').on('click', function(){
	$('#tab-box-product-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-box-product-design #module-image-box-product').find('img').attr('src', $('#tab-box-product-design #module-image-box-product').find('img').attr('data-placeholder'));
	$('#tab-box-product-design #module-image-box-product').parent().find('input').attr('value', '');	
	$('#tab-box-product-design input[name=\'bg_box_product_color\']').val('#ffffff');
	$('#tab-box-product-design input[name=\'box_product_margin_top\']').val('0');
	$('#tab-box-product-design input[name=\'box_product_margin_right\']').val('0');
	$('#tab-box-product-design input[name=\'box_product_margin_bottom\']').val('0');
	$('#tab-box-product-design input[name=\'box_product_margin_left\']').val('0');
	$('#tab-box-product-design input[name=\'box_product_padding_top\']').val('5');
	$('#tab-box-product-design input[name=\'box_product_padding_right\']').val('5');
	$('#tab-box-product-design input[name=\'box_product_padding_bottom\']').val('5');
	$('#tab-box-product-design input[name=\'box_product_padding_left\']').val('5');
	$('#tab-box-product-design select[name=\'box_product_border_weight\']').val('0');
	$('#tab-box-product-design select[name=\'box_product_border_style\']').val('solid');
	$('#tab-box-product-design input[name=\'box_product_border_color\']').val('#ffffff');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-hover').on('click', function(){
	$('#tab-box-product-hover-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-box-product-hover-design #module-image-box-product-hover').find('img').attr('src', $('#tab-box-product-hover-design #module-image-box-product-hover').find('img').attr('data-placeholder'));
	$('#tab-box-product-hover-design #module-image-box-product-hover').parent().find('input').attr('value', '');	
	$('#tab-box-product-hover-design input[name=\'bg_box_product_hover_color\']').val('#ffffff');
	$('#tab-box-product-hover-design select[name=\'box_product_hover_border_weight\']').val('0');
	$('#tab-box-product-hover-design select[name=\'box_product_hover_border_style\']').val('solid');
	$('#tab-box-product-hover-design input[name=\'box_product_hover_border_color\']').val('#ffffff');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-name').on('click', function(){
	$('#tab-box-product-name-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-box-product-name-design input[name=\'box_product_name_padding_top\']').val('5');
	$('#tab-box-product-name-design input[name=\'box_product_name_padding_right\']').val('5');
	$('#tab-box-product-name-design input[name=\'box_product_name_padding_bottom\']').val('5');
	$('#tab-box-product-name-design input[name=\'box_product_name_padding_left\']').val('5');
	$('#tab-box-product-name-design select[name=\'box_product_name_text_font\']').val('Comfortaa');
	$('#tab-box-product-name-design input[name=\'box_product_name_text_color\']').val('#333333');
	$('#tab-box-product-name-design input[name=\'box_product_name_text_shadow\']').val('#ffffff');
	$('#tab-box-product-name-design input[name=\'box_product_name_font_size\']').val('12');
	$('#tab-box-product-name-design select[name=\'box_product_name_font_weight\']').val('normal');
	$('#tab-box-product-name-design select[name=\'box_product_name_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-name-hover').on('click', function(){
	$('#tab-box-product-name-hover-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-box-product-name-hover-design select[name=\'box_product_name_hover_text_font\']').val('Comfortaa');
	$('#tab-box-product-name-hover-design input[name=\'box_product_name_hover_text_color\']').val('#000000');
	$('#tab-box-product-name-hover-design input[name=\'box_product_name_hover_text_shadow\']').val('#ffffff');
	$('#tab-box-product-name-hover-design input[name=\'box_product_name_hover_font_size\']').val('12');
	$('#tab-box-product-name-hover-design select[name=\'box_product_name_hover_font_weight\']').val('normal');
	$('#tab-box-product-name-hover-design select[name=\'box_product_name_hover_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-price').on('click', function(){
	$('#tab-box-product-price-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-box-product-price-design input[name=\'box_product_price_padding_top\']').val('0');
	$('#tab-box-product-price-design input[name=\'box_product_price_padding_right\']').val('0');
	$('#tab-box-product-price-design input[name=\'box_product_price_padding_bottom\']').val('0');
	$('#tab-box-product-price-design input[name=\'box_product_price_padding_left\']').val('0');
	$('#tab-box-product-price-design select[name=\'box_product_price_text_font\']').val('Open Sans');
	$('#tab-box-product-price-design input[name=\'box_product_price_text_color\']').val('#c95034');
	$('#tab-box-product-price-design input[name=\'box_product_price_text_shadow\']').val('#ffffff');
	$('#tab-box-product-price-design input[name=\'box_product_price_font_size\']').val('18');
	$('#tab-box-product-price-design select[name=\'box_product_price_font_weight\']').val('normal');
	$('#tab-box-product-price-design select[name=\'box_product_price_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-price-old').on('click', function(){
	$('#tab-box-product-price-old-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-box-product-price-old-design select[name=\'box_product_price_old_text_font\']').val('Open Sans');
	$('#tab-box-product-price-old-design input[name=\'box_product_price_old_text_color\']').val('#999999');
	$('#tab-box-product-price-old-design input[name=\'box_product_price_old_text_shadow\']').val('#ffffff');
	$('#tab-box-product-price-old-design input[name=\'box_product_price_old_font_size\']').val('15');
	$('#tab-box-product-price-old-design select[name=\'box_product_price_old_font_weight\']').val('normal');
	$('#tab-box-product-price-old-design select[name=\'box_product_price_old_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-cart').on('click', function(){
	$('#tab-box-product-cart-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-box-product-cart-design #module-image-cart').find('img').attr('src', $('#tab-box-product-cart-design #module-image-cart').find('img').attr('data-placeholder'));
	$('#tab-box-product-cart-design #module-image-cart').parent().find('input').attr('value', '');	
	$('#tab-box-product-cart-design input[name=\'box_product_cart_bg_color_top\']').val('#ffffff');
	$('#tab-box-product-cart-design input[name=\'box_product_cart_padding_top\']').val('10');
	$('#tab-box-product-cart-design input[name=\'box_product_cart_padding_right\']').val('10');
	$('#tab-box-product-cart-design input[name=\'box_product_cart_padding_bottom\']').val('10');
	$('#tab-box-product-cart-design input[name=\'box_product_cart_padding_left\']').val('10');
	$('#tab-box-product-cart-design select[name=\'box_product_cart_text_font\']').val('Open Sans');
	$('#tab-box-product-cart-design input[name=\'box_product_cart_text_color\']').val('#c95034');
	$('#tab-box-product-cart-design input[name=\'box_product_cart_text_shadow\']').val('#ffffff');
	$('#tab-box-product-cart-design input[name=\'box_product_cart_font_size\']').val('12');
	$('#tab-box-product-cart-design select[name=\'box_product_cart_font_weight\']').val('normal');
	$('#tab-box-product-cart-design select[name=\'box_product_cart_font_style\']').val('normal');
	$('#tab-box-product-cart-design select[name=\'box_product_cart_border_weight\']').val('1');
	$('#tab-box-product-cart-design select[name=\'box_product_cart_border_style\']').val('solid');
	$('#tab-box-product-cart-design input[name=\'box_product_cart_border_color\']').val('#dddddd');
	$('#tab-box-product-cart-design input[name=\'cart_border_radius_left_top\']').val('0');
	$('#tab-box-product-cart-design input[name=\'cart_border_radius_right_top\']').val('0');
	$('#tab-box-product-cart-design input[name=\'cart_border_radius_right_bottom\']').val('0');
	$('#tab-box-product-cart-design input[name=\'cart_border_radius_left_bottom\']').val('0');
	$('#tab-box-product-cart-design input[name=\'cart_margin_bottom\']').val('15');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-cart-hover').on('click', function(){
	$('#tab-box-product-cart-hover-design .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-box-product-cart-hover-design #module-image-cart-hover').find('img').attr('src', $('#tab-box-product-cart-hover-design #module-image-cart-hover').find('img').attr('data-placeholder'));
	$('#tab-box-product-cart-hover-design #module-image-cart-hover').parent().find('input').attr('value', '');	
	$('#tab-box-product-cart-hover-design input[name=\'box_product_cart_hover_bg_color_top\']').val('#1f90bb');
	$('#tab-box-product-cart-hover-design select[name=\'box_product_cart_hover_text_font\']').val('Open Sans');
	$('#tab-box-product-cart-hover-design input[name=\'box_product_cart_hover_text_color\']').val('#ffffff');
	$('#tab-box-product-cart-hover-design input[name=\'box_product_cart_hover_text_shadow\']').val('#333333');
	$('#tab-box-product-cart-hover-design input[name=\'box_product_cart_hover_font_size\']').val('12');
	$('#tab-box-product-cart-hover-design select[name=\'box_product_cart_hover_font_weight\']').val('normal');
	$('#tab-box-product-cart-hover-design select[name=\'box_product_cart_hover_font_style\']').val('normal');
	$('#tab-box-product-cart-hover-design select[name=\'box_product_cart_hover_border_weight\']').val('1');
	$('#tab-box-product-cart-hover-design select[name=\'box_product_cart_hover_border_style\']').val('solid');
	$('#tab-box-product-cart-hover-design input[name=\'box_product_cart_hover_border_color\']').val('#156888');
	$('#tab-box-product-cart-hover-design input[name=\'cart_hover_border_radius_left_top\']').val('0');
	$('#tab-box-product-cart-hover-design input[name=\'cart_hover_border_radius_right_top\']').val('0');
	$('#tab-box-product-cart-hover-design input[name=\'cart_hover_border_radius_right_bottom\']').val('0');
	$('#tab-box-product-cart-hover-design input[name=\'cart_hover_border_radius_left_bottom\']').val('0');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-buttons').on('click', function(){
	$('#tab-buttons-design .input-group-addon').find('i').addClass().css('background-color', '#000');	
	$('#tab-buttons-design input[name=\'box_product_buttons_bg_color\']').val('#888888');
	$('#tab-buttons-design input[name=\'box_product_buttons_text_color\']').val('#ffffff');
	$('#tab-buttons-design input[name=\'box_product_buttons_text_shadow\']').val('#333333');
	$('#tab-buttons-design select[name=\'box_product_buttons_border_weight\']').val('1');
	$('#tab-buttons-design select[name=\'box_product_buttons_border_style\']').val('solid');
	$('#tab-buttons-design input[name=\'box_product_buttons_border_color\']').val('#666666');
	$('#tab-buttons-design input[name=\'buttons_border_radius_left_top\']').val('25');
	$('#tab-buttons-design input[name=\'buttons_border_radius_right_top\']').val('25');
	$('#tab-buttons-design input[name=\'buttons_border_radius_right_bottom\']').val('25');
	$('#tab-buttons-design input[name=\'buttons_border_radius_left_bottom\']').val('25');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-box-product-buttons-hover').on('click', function(){
	$('#tab-buttons-hover-design .input-group-addon').find('i').addClass().css('background-color', '#000');	
	$('#tab-buttons-hover-design input[name=\'box_product_buttons_hover_bg_color\']').val('#1f90bb');
	$('#tab-buttons-hover-design input[name=\'box_product_buttons_hover_text_color\']').val('#ffffff');
	$('#tab-buttons-hover-design input[name=\'box_product_buttons_hover_text_shadow\']').val('#333333');
	$('#tab-buttons-hover-design select[name=\'box_product_buttons_hover_border_weight\']').val('1');
	$('#tab-buttons-hover-design select[name=\'box_product_buttons_hover_border_style\']').val('solid');
	$('#tab-buttons-hover-design input[name=\'box_product_buttons_hover_border_color\']').val('#156888');
	$('#tab-buttons-hover-design input[name=\'buttons_hover_border_radius_left_top\']').val('25');
	$('#tab-buttons-hover-design input[name=\'buttons_hover_border_radius_right_top\']').val('25');
	$('#tab-buttons-hover-design input[name=\'buttons_hover_border_radius_right_bottom\']').val('25');
	$('#tab-buttons-hover-design input[name=\'buttons_hover_border_radius_left_bottom\']').val('25');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-btn-carousel').on('click', function(){
	$('#tab-btn-carousel-design .input-group-addon').find('i').addClass().css('background-color', '#000');	
	$('#tab-btn-carousel-design input[name=\'btn_carousel_bg_color\']').val('#1f90bb');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_text_color\']').val('#ffffff');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_text_shadow\']').val('#333333');
	$('#tab-btn-carousel-design select[name=\'btn_carousel_border_weight\']').val('1');
	$('#tab-btn-carousel-design select[name=\'btn_carousel_border_style\']').val('solid');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_border_color\']').val('#156888');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_l_border_radius_left_top\']').val('0');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_l_border_radius_right_top\']').val('25');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_l_border_radius_right_bottom\']').val('25');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_l_border_radius_left_bottom\']').val('0');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_r_border_radius_left_top\']').val('25');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_r_border_radius_right_top\']').val('0');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_r_border_radius_right_bottom\']').val('0');
	$('#tab-btn-carousel-design input[name=\'btn_carousel_r_border_radius_left_bottom\']').val('25');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-btn-carousel-hover').on('click', function(){
	$('#tab-btn-carousel-hover-design .input-group-addon').find('i').addClass().css('background-color', '#000');	
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_bg_color\']').val('#d61359');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_text_color\']').val('#ffffff');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_text_shadow\']').val('#333333');
	$('#tab-btn-carousel-hover-design select[name=\'btn_carousel_hover_border_weight\']').val('1');
	$('#tab-btn-carousel-hover-design select[name=\'btn_carousel_hover_border_style\']').val('solid');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_border_color\']').val('#ad0b45');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_l_border_radius_left_top\']').val('0');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_l_border_radius_right_top\']').val('25');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_l_border_radius_right_bottom\']').val('25');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_l_border_radius_left_bottom\']').val('0');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_r_border_radius_left_top\']').val('25');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_r_border_radius_right_top\']').val('0');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_r_border_radius_right_bottom\']').val('0');
	$('#tab-btn-carousel-hover-design input[name=\'btn_carousel_hover_r_border_radius_left_bottom\']').val('25');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-quick-product-name').on('click', function(){
	$('#tab-quick-product-name .input-group-addon').find('i').addClass().css('background-color', '#000');	
	$('#tab-quick-product-name select[name=\'product_name_text_font\']').val('Open Sans');
	$('#tab-quick-product-name input[name=\'product_name_text_color\']').val('#666666');
	$('#tab-quick-product-name input[name=\'product_name_text_shadow\']').val('#ffffff');
	$('#tab-quick-product-name input[name=\'product_name_font_size\']').val('14');
	$('#tab-quick-product-name select[name=\'product_name_font_weight\']').val('normal');
	$('#tab-quick-product-name select[name=\'product_name_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-quick-product-name-data').on('click', function(){
	$('#tab-quick-product-name-data .input-group-addon').find('i').addClass().css('background-color', '#000');	
	$('#tab-quick-product-name-data select[name=\'product_name_data_text_font\']').val('Jura');
	$('#tab-quick-product-name-data input[name=\'product_name_data_text_color\']').val('#888888');
	$('#tab-quick-product-name-data input[name=\'product_name_data_text_shadow\']').val('#ffffff');
	$('#tab-quick-product-name-data input[name=\'product_name_data_font_size\']').val('16');
	$('#tab-quick-product-name-data select[name=\'product_name_data_font_weight\']').val('normal');
	$('#tab-quick-product-name-data select[name=\'product_name_data_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-quick-product-data-value').on('click', function(){
	$('#tab-quick-product-data-value .input-group-addon').find('i').addClass().css('background-color', '#000');	
	$('#tab-quick-product-data-value select[name=\'product_name_value_text_font\']').val('Jura');
	$('#tab-quick-product-data-value input[name=\'product_name_value_text_color\']').val('#666666');
	$('#tab-quick-product-data-value input[name=\'product_name_value_text_shadow\']').val('#ffffff');
	$('#tab-quick-product-data-value input[name=\'product_name_value_font_size\']').val('16');
	$('#tab-quick-product-data-value select[name=\'product_name_value_font_weight\']').val('bold');
	$('#tab-quick-product-data-value select[name=\'product_name_value_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-quick-product-price').on('click', function(){
	$('#tab-quick-product-price .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-quick-product-price select[name=\'quick_product_price_text_font\']').val('Open Sans');
	$('#tab-quick-product-price input[name=\'quick_product_price_text_color\']').val('#c95034');
	$('#tab-quick-product-price input[name=\'quick_product_price_text_shadow\']').val('#ffffff');
	$('#tab-quick-product-price input[name=\'quick_product_price_font_size\']').val('32');
	$('#tab-quick-product-price select[name=\'quick_product_price_font_weight\']').val('normal');
	$('#tab-quick-product-price select[name=\'quick_product_price_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-quick-product-price-old').on('click', function(){
	$('#tab-quick-product-price-old .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-quick-product-price-old select[name=\'quick_product_price_old_text_font\']').val('Open Sans');
	$('#tab-quick-product-price-old input[name=\'quick_product_price_old_text_color\']').val('#888888');
	$('#tab-quick-product-price-old input[name=\'quick_product_price_old_text_shadow\']').val('#ffffff');
	$('#tab-quick-product-price-old input[name=\'quick_product_price_old_font_size\']').val('28');
	$('#tab-quick-product-price-old select[name=\'quick_product_price_old_font_weight\']').val('normal');
	$('#tab-quick-product-price-old select[name=\'quick_product_price_old_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-quick-product-price-other').on('click', function(){
	$('#tab-quick-product-price-other .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-quick-product-price-other select[name=\'quick_product_price_other_text_font\']').val('Open Sans');
	$('#tab-quick-product-price-other input[name=\'quick_product_price_other_text_color\']').val('#888888');
	$('#tab-quick-product-price-other input[name=\'quick_product_price_other_text_shadow\']').val('#ffffff');
	$('#tab-quick-product-price-other input[name=\'quick_product_price_other_font_size\']').val('11');
	$('#tab-quick-product-price-other select[name=\'quick_product_price_other_font_weight\']').val('normal');
	$('#tab-quick-product-price-other select[name=\'quick_product_price_other_font_style\']').val('normal');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-quick-buttons').on('click', function(){
	$('#tab-quick-buttons .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-quick-buttons #module-image-quick-buttons').find('img').attr('src', $('#tab-quick-buttons #module-image-quick-buttons').find('img').attr('data-placeholder'));
	$('#tab-quick-buttons #module-image-quick-buttons').parent().find('input').attr('value', '');		
	$('#tab-quick-buttons input[name=\'modal_buttons_bg_color\']').val('#ffffff');
	$('#tab-quick-buttons select[name=\'modal_buttons_text_font\']').val('Open Sans');
	$('#tab-quick-buttons input[name=\'modal_buttons_text_color\']').val('#c95034');
	$('#tab-quick-buttons input[name=\'modal_buttons_text_shadow\']').val('#ffffff');
	$('#tab-quick-buttons input[name=\'modal_buttons_font_size\']').val('12');
	$('#tab-quick-buttons select[name=\'modal_buttons_font_weight\']').val('normal');
	$('#tab-quick-buttons select[name=\'modal_buttons_font_style\']').val('normal');
	$('#tab-quick-buttons select[name=\'modal_buttons_border_weight\']').val('1');
	$('#tab-quick-buttons select[name=\'modal_buttons_border_style\']').val('solid');
	$('#tab-quick-buttons input[name=\'modal_buttons_border_color\']').val('#c95034');
	$('#tab-quick-buttons input[name=\'modal_buttons_padding_top\']').val('10');
	$('#tab-quick-buttons input[name=\'modal_buttons_padding_right\']').val('20');
	$('#tab-quick-buttons input[name=\'modal_buttons_padding_bottom\']').val('10');
	$('#tab-quick-buttons input[name=\'modal_buttons_padding_left\']').val('20');	
	$('#tab-quick-buttons input[name=\'modal_buttons_border_radius_left_top\']').val('0');
	$('#tab-quick-buttons input[name=\'modal_buttons_border_radius_right_top\']').val('0');
	$('#tab-quick-buttons input[name=\'modal_buttons_border_radius_right_bottom\']').val('0');
	$('#tab-quick-buttons input[name=\'modal_buttons_border_radius_left_bottom\']').val('0');
}); 
//--></script>
<script type="text/javascript"><!--
$('#clear-design-quick-buttons-hover').on('click', function(){
	$('#tab-quick-buttons-hover .input-group-addon').find('i').addClass().css('background-color', '#000');
	$('#tab-quick-buttons-hover #module-image-quick-buttons-hover').find('img').attr('src', $('#tab-quick-buttons-hover #module-image-quick-buttons-hover').find('img').attr('data-placeholder'));
	$('#tab-quick-buttons-hover #module-image-quick-buttons-hover').parent().find('input').attr('value', '');		
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_bg_color\']').val('#1f90bb');
	$('#tab-quick-buttons-hover select[name=\'modal_buttons_hover_text_font\']').val('Open Sans');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_text_color\']').val('#ffffff');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_text_shadow\']').val('#333333');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_font_size\']').val('12');
	$('#tab-quick-buttons-hover select[name=\'modal_buttons_hover_font_weight\']').val('normal');
	$('#tab-quick-buttons-hover select[name=\'modal_buttons_hover_font_style\']').val('normal');
	$('#tab-quick-buttons-hover select[name=\'modal_buttons_hover_border_weight\']').val('1');
	$('#tab-quick-buttons-hover select[name=\'modal_buttons_hover_border_style\']').val('solid');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_border_color\']').val('#156888');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_padding_top\']').val('10');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_padding_right\']').val('20');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_padding_bottom\']').val('10');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_padding_left\']').val('20');	
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_border_radius_left_top\']').val('0');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_border_radius_right_top\']').val('0');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_border_radius_right_bottom\']').val('0');
	$('#tab-quick-buttons-hover input[name=\'modal_buttons_hover_border_radius_left_bottom\']').val('0');
}); 
//--></script>
<script type="text/javascript"><!--
var category_row = <?php echo $category_row; ?>;

function addCategory() {
    html  = '<tr id="category-row' + category_row + '">';
	html += '  <td class="text-left"><input type="text" name="select_categories[' + category_row + '][category]" value="" class="form-control" /><input type="hidden" value="" name="select_categories[' + category_row + '][category_id]" /></td>';
	html += '  <td class="text-left"><input type="text" name="select_categories[' + category_row + '][limit_name]" value="25" class="form-control" /></td>';
	html += '  <td class="text-left"><input type="text" name="select_categories[' + category_row + '][limit_name_product]" value="25" class="form-control" /></td>';
	html += '  <td class="text-left"><input type="text" name="select_categories[' + category_row + '][limit_product]" value="20" class="form-control" /></td>';	
	html += '  <td class="text-right"><button type="button" onclick="$(\'#category-row' + category_row + '\').remove();" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
	html += '</tr>';
	
	$('#category tbody').append(html);
	
	categoryautocomplete(category_row);
	
	category_row++;
}

function categoryautocomplete(category_row) {
	$('input[name=\'select_categories[' + category_row + '][category]\']').autocomplete({
		'source': function(request, response) {
			$.ajax({
				url: 'index.php?route=module/product_categories/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',			
				success: function(json) {
					response($.map(json, function(item) {
						return {
							label: item.name,
							value: item.category_id
						}
					}));
				}
			});
		},
		'select': function(item) {
			$('input[name=\'select_categories[' + category_row + '][category]\']').val(item['label']);
			$('input[name=\'select_categories[' + category_row + '][category_id]\']').val(item['value']);
		}
	});
}

$('#category tbody tr').each(function(index, element) {
	categoryautocomplete(index);
});
//--></script>
<script type="text/javascript"><!--
$('#tab-data input[name=\'status_banner\']').on('change', function() {
	if ($('#tab-data input[name=\'status_banner\']:checked').val()) {
		$('#banner-image').removeClass('hide');	
	} else {
		$('#banner-image').addClass('hide');
	}
});	
$('#tab-data input[name=\'status_banner\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('#tab-data input[name=\'status_quick_view\']').on('change', function() {
	if ($('#tab-data input[name=\'status_quick_view\']:checked').val()) {
		$('#quick-view-data').removeClass('hide');
		$('#design-quick-view').removeClass('hide');	
		$('#tab-design-quick-view').removeClass('hide');	
	} else {
		$('#quick-view-data').addClass('hide');
		$('#design-quick-view').addClass('hide');
		$('#tab-design-quick-view').addClass('hide');
	}
});	
$('#tab-data input[name=\'status_quick_view\']').trigger('change');
//--></script>
<script type="text/javascript" src="view/javascript/bootstrap/js/bootstrap-colorpicker.min.js"></script>
<link href="view/javascript/bootstrap/css/bootstrap-colorpicker.css" type="text/css" rel="stylesheet" media="screen" />
<script type="text/javascript"><!--
$('.setting-colorpicker').colorpicker();
//--></script>
<?php echo $footer; ?>