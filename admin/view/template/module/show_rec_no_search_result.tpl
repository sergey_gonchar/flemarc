<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-srpwnsr" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
	  </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($update) { ?>
    <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $update; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>  
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-srpwnsr" class="form-horizontal">
			<ul class="nav nav-tabs" id="tabs">
				<li class="active"><a href="#tab-setting" data-toggle="tab"><i class="fa fa-fw fa-wrench"></i> <?php echo $tab_setting; ?></a></li>
				<li><a href="#tab-help" data-toggle="tab"><i class="fa fa-fw fa-question"></i> <?php echo $tab_help; ?></a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="tab-setting">  
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
							<select name="show_rec_no_search_result_status" id="input-status" class="form-control">
								<?php if ($show_rec_no_search_result_status) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>	
					
					<fieldset>
						<legend class="small"><?php echo $legend_products; ?></legend>
						<div class="form-group required">
							<label class="col-sm-2 control-label" for="input-product"><?php echo $entry_product; ?></label>
							<div class="col-sm-10">
								<input type="text" name="product" value="" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
								<div id="show-rec-no-search-result-product" class="well well-sm" style="height: 150px; overflow: auto;">
									<?php foreach ($products as $product) { ?>
									<div id="show-rec-no-search-result-product<?php echo $product['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product['name']; ?>
										<input type="hidden" name="show_rec_no_search_result_product[]" value="<?php echo $product['product_id']; ?>" />
									</div>
									<?php } ?>
								</div>
								<?php if ($error_product) { ?>
								<div class="text-danger"><?php echo $error_product; ?></div>
								<?php } ?>
							</div>
						</div>
					</fieldset>		

					<fieldset>
						<legend class="small"><?php echo $legend_title; ?></legend>
						<ul class="nav nav-tabs" id="languages">
							<?php foreach ($languages as $language) { ?>
							<li><a data-toggle="tab" href="#language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
							<?php } ?>
						</ul>
						
						<div class="tab-content">	
							<?php foreach ($languages as $language) { ?>
							<div id="language-<?php echo $language['language_id']; ?>" class="tab-pane">
								<div class="form-group required">
									<label class="col-sm-2 control-label" for="input-title-<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" data-html="true" title="<?php echo $help_title; ?>"><?php echo $entry_title; ?></span></label>
									<div class="col-sm-10">
										<input name="show_rec_no_search_result_title[<?php echo $language['language_id']; ?>][title]" placeholder="<?php echo $entry_title; ?>" id="input-title-<?php echo $language['language_id']; ?>" value="<?php echo isset($show_rec_no_search_result_title[$language['language_id']]) ? $show_rec_no_search_result_title[$language['language_id']]['title'] : ''; ?>" class="form-control" />
										<?php if (isset($error_title[$language['language_id']])) { ?>
										<div class="text-danger"><?php echo $error_title[$language['language_id']]; ?></div>
										<?php } ?>									
									</div>
								</div>						
							</div>
							<?php } ?>
						</div>
					</fieldset>
					
				</div>			
				
				<div class="tab-pane" id="tab-help">
					<div class="tab-content">
						Change Log and HELP Guide is available : <a href="http://www.oc-extensions.com/Show-Recommended-Products-When-No-Search-Result" target="blank">HERE</a><br /><br />
						If you need support email us at <strong>support@oc-extensions.com</strong> (Please first read help guide) 				
					</div>
				</div>
			</div>
		</form>	
    </div>
  </div>
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	select: function(item) {
		$('input[name=\'product\']').val('');
		
		$('#show-rec-no-search-result-product' + item['value']).remove();
		
		$('#show-rec-no-search-result-product').append('<div id="show-rec-no-search-result-product' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="show_rec_no_search_result_product[]" value="' + item['value'] + '" /></div>');	
	}
});
	
$('#show-rec-no-search-result-product').delegate('.fa-minus-circle', 'click', function() {
	$(this).parent().remove();
});

$('#languages li:first-child a').tab('show');
//--></script></div>
<?php echo $footer; ?>