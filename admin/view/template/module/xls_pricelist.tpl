<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
 <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" onclick="$('#form').removeAttr('target'); $('#form').attr('action', '<?php echo $action; ?>');" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
 <style>
    .colorpicker-2x .colorpicker-saturation {
        width: 200px;
        height: 200px;
    }
    .colorpicker-2x .colorpicker-hue,
    .colorpicker-2x .colorpicker-alpha {
        width: 30px;
        height: 200px;
    }
    .colorpicker-2x .colorpicker-color,
    .colorpicker-2x .colorpicker-color div{
        height: 30px;
    }
</style> 
<div class="container-fluid">
<?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
  <div class="panel panel-default">
  <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
	<div class="panel-body">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
		<ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
			<li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
			<li><a href="#tab-option" data-toggle="tab"><?php echo $tab_option; ?></a></li>
			<li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
			<li><a href="#tab-design" data-toggle="tab"><?php echo $tab_design; ?></a></li>
          </ul>
		<div class="tab-content">  
		<div class="tab-pane active"  id="tab-general">
			<div class="form-group required">
				  <label class="col-sm-2 control-label" ><?php echo $entry_store; ?></label>
				  <div class="col-sm-10">
					<div class="well well-sm" style="height: 150px; overflow: auto;">
                     <div class="checkbox">
                      <label>
                        <?php if (in_array(0, $xls_pricelist_store)) { ?>
                        <input type="checkbox" name="xls_pricelist_store[]" value="0" checked="checked" />
                        <?php echo $text_default; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="xls_pricelist_store[]" value="0" />
                        <?php echo $text_default; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php foreach ($stores as $store) { ?>
                    <div class="checkbox">
                      <label>
                        <?php if (in_array($store['store_id'], $xls_pricelist_store)) { ?>
                        <input type="checkbox" name="xls_pricelist_store[]" value="<?php echo $store['store_id']; ?>" checked="checked" />
                        <?php echo $store['name']; ?>
                        <?php } else { ?>
                        <input type="checkbox" name="xls_pricelist_store[]" value="<?php echo $store['store_id']; ?>" />
                        <?php echo $store['name']; ?>
                        <?php } ?>
                      </label>
                    </div>
                    <?php } ?>
					</div>
					<?php if ($error_xls_pricelist_store) { ?>
					  <div class="text-danger"><?php echo $error_xls_pricelist_store; ?></div>
					  <?php } ?>
					</div>
			</div>
			<div class="form-group required">
				<label class="col-sm-2 control-label" ><?php echo $entry_category; ?></label>
				<div class="col-sm-10">
					<div class="well well-sm" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach ($categories as $category) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<div class="checkbox">
                      <label>
					  <?php if (in_array($category['category_id'], $xls_pricelist_category)) { ?>
					  <input class="xls_pricelist_category" type="checkbox" name="xls_pricelist_category[]" value="<?php echo $category['category_id']; ?>" checked="checked" />
					  <?php echo $category['name']; ?>
					  <?php } else { ?>
					  <input class="xls_pricelist_category" type="checkbox" name="xls_pricelist_category[]" value="<?php echo $category['category_id']; ?>" />
					  <?php echo $category['name']; ?>
					  <?php } ?>
					  </label>
					</div>
					<?php } ?>
				  </div>
				  <br>
				  
				  <a href="#" onclick="$('.well .xls_pricelist_category').prop('checked', !($('.well .xls_pricelist_category').is(':checked'))); return false;" class="btn btn-default"><i class="fa fa-check"></i></a> 
				 
				  <?php if ($error_xls_pricelist_category) { ?>
					  <div class="text-danger"><?php echo $error_xls_pricelist_category; ?></div>
					  <?php } ?>
				</div>
			</div>
			<div class="form-group" style="display:none;">
				<label class="col-sm-2 control-label" ><?php echo $entry_nodubles; ?></label>
				<div class="col-sm-10">
				<select name="xls_pricelist_nodubles" class="form-control">
					<?php if($xls_pricelist_nodubles){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_sort_order; ?></label>
				<div class="col-sm-10">
				<select name="xls_pricelist_sort_order" class="form-control">
					<?php foreach ($sorts as $sorts) { ?>
						<?php if ($sorts['value'] == $xls_pricelist_sort_order) { ?>
						<option value="<?php echo $sorts['value']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $sorts['value']; ?>"><?php echo $sorts['text']; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
				</div>
			</div>
			
			
			<div class="form-group required">
				<label class="col-sm-2 control-label" ><?php echo $entry_customer_group; ?></label>
				<div class="col-sm-10">
					<div class="well well-sm" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach ($customer_groups as $customer_group) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<div class="checkbox">
					<label>
					  <?php if (in_array($customer_group['customer_group_id'], $xls_pricelist_customer_group)) { ?>
					  <input class="xls_pricelist_customer_group" type="checkbox" name="xls_pricelist_customer_group[]" value="<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
					  <?php echo $customer_group['name']; ?>
					  <?php } else { ?>
					  <input class="xls_pricelist_customer_group" type="checkbox" name="xls_pricelist_customer_group[]" value="<?php echo $customer_group['customer_group_id']; ?>" />
					  <?php echo $customer_group['name']; ?>
					  <?php } ?>
					  </label>
					</div>
					<?php } ?>
				  </div>
				  <br>
				  
				  <a href="#" onclick="$('.well .xls_pricelist_customer_group').prop('checked', !($('.well .xls_pricelist_customer_group').is(':checked'))); return false;" class="btn btn-default"><i class="fa fa-check"></i></a> 
				 
				  <?php if ($error_xls_pricelist_customer_group) { ?>
					  <div class="text-danger"><?php echo $error_xls_pricelist_customer_group; ?></div>
					  <?php } ?>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_attribute_groups; ?></label>
				<div class="col-sm-10">
					<div class="well well-sm" style="height: 150px; overflow: auto;">
					<?php $class = 'odd'; ?>
					<?php foreach ($attribute_groups as $attribute_group) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<div class="checkbox">
					<label>
					  <?php if (in_array($attribute_group['attribute_group_id'], $xls_pricelist_attribute_group)) { ?>
					  <input class="xls_pricelist_attribute_group" type="checkbox" name="xls_pricelist_attribute_group[]" value="<?php echo $attribute_group['attribute_group_id']; ?>" checked="checked" />
					  <?php echo $attribute_group['name']; ?>
					  <?php } else { ?>
					  <input class="xls_pricelist_attribute_group" type="checkbox" name="xls_pricelist_attribute_group[]" value="<?php echo $attribute_group['attribute_group_id']; ?>" />
					  <?php echo $attribute_group['name']; ?>
					  <?php } ?>
					  </label>
					</div>
					<?php } ?>
				  </div>
				  <br>
				  
				  <a href="#" onclick="$('.well .xls_pricelist_attribute_group').prop('checked', !($('.well .xls_pricelist_attribute_group').is(':checked'))); return false;" class="btn btn-default"><i class="fa fa-check"></i></a> 
				  
				</div>
			</div>
			
			
		</div>
		
		<div class="tab-pane"  id="tab-data">
			<ul class="nav nav-tabs" id="language">
                <?php foreach ($languages as $language) { ?>
                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                <?php } ?>
              </ul>
			<div class="tab-content">  
			<?php foreach ($languages as $language) { ?>
			  <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
		  
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_title; ?></label>
				<div class="col-sm-10">
				<input type="text" name="xls_pricelist_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]) ? $xls_pricelist_description[$language['language_id']]['title'] : ''; ?>"  class="form-control"/>
				&nbsp;&nbsp;&nbsp;
				<div class="input-group jpicker">
					<input type="text" class="form-control"  name="xls_pricelist_description[<?php echo $language['language_id']; ?>][title_color]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]['title_color'])&&$xls_pricelist_description[$language['language_id']]['title_color'] ? $xls_pricelist_description[$language['language_id']]['title_color'] : '000000'; ?>" />
					<span class="input-group-addon"><i></i></span>
				</div>
				
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_adress; ?></label>
				<div class="col-sm-10">
				<input type="text" name="xls_pricelist_description[<?php echo $language['language_id']; ?>][adress]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]) ? $xls_pricelist_description[$language['language_id']]['adress'] : ''; ?>" class="form-control"/>
				&nbsp;&nbsp;&nbsp;
				<div class="input-group jpicker">
					<input type="text" class="form-control"  name="xls_pricelist_description[<?php echo $language['language_id']; ?>][adress_color]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]['adress_color'])&&$xls_pricelist_description[$language['language_id']]['adress_color'] ? $xls_pricelist_description[$language['language_id']]['adress_color'] : '000000'; ?>" />
					<span class="input-group-addon"><i></i></span>
				</div>				
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_phone; ?></label>
				<div class="col-sm-10">
				<input type="text" name="xls_pricelist_description[<?php echo $language['language_id']; ?>][phone]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]) ? $xls_pricelist_description[$language['language_id']]['phone'] : ''; ?>" class="form-control"/>
				&nbsp;&nbsp;&nbsp;
				<div class="input-group jpicker">
					<input type="text" class="form-control"  name="xls_pricelist_description[<?php echo $language['language_id']; ?>][phone_color]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]['phone_color'])&&$xls_pricelist_description[$language['language_id']]['phone_color'] ? $xls_pricelist_description[$language['language_id']]['phone_color'] : '000000'; ?>" />
					<span class="input-group-addon"><i></i></span>
				</div>				
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_email; ?></label>
				<div class="col-sm-10">
				<input type="text" name="xls_pricelist_description[<?php echo $language['language_id']; ?>][email]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]) ? $xls_pricelist_description[$language['language_id']]['email'] : ''; ?>" class="form-control"/>
				&nbsp;&nbsp;&nbsp;
				<div class="input-group jpicker">
					<input type="text" class="form-control"  name="xls_pricelist_description[<?php echo $language['language_id']; ?>][email_color]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]['email_color'])&&$xls_pricelist_description[$language['language_id']]['email_color'] ? $xls_pricelist_description[$language['language_id']]['email_color'] : '339966'; ?>" />
					<span class="input-group-addon"><i></i></span>
				</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_link; ?></label>
				<div class="col-sm-10">
				<input type="text" name="xls_pricelist_description[<?php echo $language['language_id']; ?>][link]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]) ? $xls_pricelist_description[$language['language_id']]['link'] : ''; ?>" class="form-control"/>
				&nbsp;&nbsp;&nbsp;
				<div class="input-group jpicker">
					<input type="text" class="form-control"  name="xls_pricelist_description[<?php echo $language['language_id']; ?>][link_color]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]['link_color'])&&$xls_pricelist_description[$language['language_id']]['link_color'] ? $xls_pricelist_description[$language['language_id']]['link_color'] : '0000FF'; ?>" />
					<span class="input-group-addon"><i></i></span>
				</div>
				
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_custom_text; ?></label>
				<div class="col-sm-10">
				<textarea name="xls_pricelist_description[<?php echo $language['language_id']; ?>][custom_text]" class="form-control" rows="5"><?php echo isset($xls_pricelist_description[$language['language_id']]) ? $xls_pricelist_description[$language['language_id']]['custom_text'] : ''; ?></textarea>
				&nbsp;&nbsp;&nbsp;
				<div class="input-group jpicker">
					<input type="text" class="form-control"  name="xls_pricelist_description[<?php echo $language['language_id']; ?>][custom_color]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]['custom_color'])&&$xls_pricelist_description[$language['language_id']]['custom_color'] ? $xls_pricelist_description[$language['language_id']]['custom_color'] : '000000'; ?>" />
					<span class="input-group-addon"><i></i></span>
				</div>
				
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_text_list; ?></label>
				<div class="col-sm-10">
				<input type="text" name="xls_pricelist_description[<?php echo $language['language_id']; ?>][listname]" value="<?php echo isset($xls_pricelist_description[$language['language_id']]) ? $xls_pricelist_description[$language['language_id']]['listname'] : ''; ?>" class="form-control"/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_currency; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_description[<?php echo $language['language_id']; ?>][currency]">
					<?php foreach ($currencies as $currency) { ?>
						<?php if ($currency['code'] == $xls_pricelist_description[$language['language_id']]['currency']) { ?>
						<option value="<?php echo $currency['code']; ?>" selected="selected"><?php echo $currency['title']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $currency['code']; ?>"><?php echo $currency['title']; ?></option>
						<?php } ?>
					<?php } ?>
				</select>
				</div>
			</div>
			
			</div>
			<?php } ?>
			</div>
		</div>
		<div class="tab-pane" id="tab-option">
			<div class="tab-content">  
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_use_attributes; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_use_attributes">
					<?php if($xls_pricelist_use_attributes){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_use_options; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_use_options">
					<?php if($xls_pricelist_use_options){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_use_notinstock; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_use_notinstock">
					<?php if($xls_pricelist_use_notinstock){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_use_quantity; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_use_quantity">
					<?php if($xls_pricelist_use_quantity){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_view; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_view">
					<?php if($xls_pricelist_view){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_category_link; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_category_link">
					<?php if($xls_pricelist_category_link){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_product_link; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_product_link">
					<?php if($xls_pricelist_product_link){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_use_cache; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_usecache">
					<option <?php if($xls_pricelist_usecache=="no")echo 'selected'; ?> value="no"><?php echo $text_no; ?></option>
					<option <?php if($xls_pricelist_usecache=="file")echo 'selected'; ?> value="file">file</option>
					<option <?php if($xls_pricelist_usecache=="memcache")echo 'selected'; ?> value="memcache">memcache *</option>
				</select>
				<div class="memcache_params"<?php if($xls_pricelist_usecache!='memcache')echo 'style="display:none;"'; ?>>
				<br />
				<b><?php echo $entry_memcache_warning; ?></b><br /><br />
				memcacheServer: <input class="form-control" type="text" name="xls_pricelist_memcacheServer" value="<?php echo $xls_pricelist_memcacheServer; ?>" /><br />
				memcachePort: <input class="form-control" type="text" name="xls_pricelist_memcachePort" value="<?php echo $xls_pricelist_memcachePort; ?>" /><br />
				cacheTime: <input class="form-control" type="text" name="xls_pricelist_cacheTime" value="<?php echo $xls_pricelist_cacheTime; ?>" />
				
				</div>
				</div>
			</div>
			
			</div>
		</div>
		<div class="tab-pane" id="tab-image">
			<div class="tab-content">
				<div class="form-group">
					<label class="col-sm-2 control-label"><?php echo $entry_logo; ?></label>
					<div class="col-sm-10"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $logo; ?>" alt="" title="" data-placeholder="<?php echo $no_image; ?>" /></a>
					  <input type="hidden" name="xls_pricelist_logo" value="<?php echo $xls_pricelist_logo; ?>" id="input-image" />
					</div>
              </div>
				
				<div class="form-group form-inline">
					<label class="col-sm-2 control-label" ><?php echo $entry_logo_dimensions; ?></label>
					<div class="col-sm-10">
					<input class="form-control" type="text" name="xls_pricelist_logo_width" value="<?php echo $xls_pricelist_logo_width; ?>" />
					 x 
					<input class="form-control" type="text" name="xls_pricelist_logo_height" value="<?php echo $xls_pricelist_logo_height; ?>" />
					<?php if (isset($error_xls_pricelist_dimensions1)) { ?>
						  <span class="error"><?php echo $error_xls_pricelist_dimensions1; ?></span>
						  <?php } ?>
					</div>
				</div>
				
				<div class="form-group form-inline">
					<label class="col-sm-2 control-label" ><?php echo $entry_image_dimensions; ?></label>
					<div class="col-sm-10">
					<input type="text" name="xls_pricelist_image_width" value="<?php echo $xls_pricelist_image_width; ?>" class="form-control"/>
					 x 
					<input type="text" name="xls_pricelist_image_height" value="<?php echo $xls_pricelist_image_height; ?>" class="form-control"/>
					<?php if (isset($error_xls_pricelist_dimensions)) { ?>
						  <span class="error"><?php echo $error_xls_pricelist_dimensions; ?></span>
						  <?php } ?>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tab-design">
			<div class="tab-content">
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_poles; ?></label>
				<div class="col-sm-10">
					<ul id="sortable">
						<?php $pole_row = 0; ?>
							<?php foreach($xls_pricelist_poles as $pole){ ?>
							<li id="pole-row<?php echo $pole_row; ?>" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
								<table  class="list">
								<tbody >
								  <tr>
									<td class="left">
										<?php echo $entry_pole_type; ?><br />
										<select name="xls_pricelist_poles[<?php echo $pole_row; ?>][type]">
										<?php foreach ($poletypes as $poletype) { ?>
										<?php if ($poletype == $pole['type']) { ?>
										<option value="<?php echo $poletype; ?>" selected="selected"><?php echo $poletype; ?></option>
										<?php } else { ?>
										<option value="<?php echo $poletype; ?>"><?php echo $poletype; ?></option>
										<?php } ?>
										<?php } ?>
									  </select></td>
									<td class="left">
									<?php echo $entry_pole_name; ?><br />
									<?php foreach ($languages as $language) { ?>
									<input type="text" name="xls_pricelist_poles[<?php echo $pole_row; ?>][name][<?php echo $language['language_id']; ?>]" value="<?php echo $pole['name'][$language['language_id']]; ?>" size="20" />
									<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />
									<?php } ?>
									</td>
									<td class="left">
									<?php echo $entry_dimensions; ?><br />
									<input type="text" name="xls_pricelist_poles[<?php echo $pole_row; ?>][dlina]" value="<?php echo $pole['dlina']; ?>" size="3" />
									</td>
									<td class="left">
									<?php echo $entry_alignment; ?><br />
									<select name="xls_pricelist_poles[<?php echo $pole_row; ?>][alignment]">
										<?php foreach ($alignments as $key=>$alignment) { ?>
										<?php if ($key == $pole['alignment']) { ?>
										<option value="<?php echo $key; ?>" selected="selected"><?php echo $alignment; ?></option>
										<?php } else { ?>
										<option value="<?php echo $key; ?>"><?php echo $alignment; ?></option>
										<?php } ?>
										<?php } ?>
									  </select>
									</td>
									<td class="left">
										<?php echo $entry_color_text; ?>&nbsp;
										<div class="input-group jpicker">
											<input type="text" class="form-control"  name="xls_pricelist_poles[<?php echo $pole_row; ?>][textcolor]" value="<?php echo $pole['textcolor']; ?>" />
											<span class="input-group-addon"><i></i></span>
										</div>
										<?php echo $entry_color_bg; ?>&nbsp;
										<div class="input-group jpicker">
											<input type="text" class="form-control"  name="xls_pricelist_poles[<?php echo $pole_row; ?>][bgcolor]" value="<?php echo $pole['bgcolor']; ?>" />
											<span class="input-group-addon"><i></i></span>
										</div>
										
									</td>
									<td class="left"><a onclick="removePole(<?php echo $pole_row; ?>);" class="btn btn-danger">x</a></td>
								  </tr>
								</tbody>
								</table>
								</li>
							<?php $pole_row++; ?>
							<?php } ?>
						</ul>
							<table id="poles" class="list">
							
							  <tr>
								<td  class="right"><a onclick="addPole();" class="btn btn-primary">+</a></td>
							  </tr>
							
							</table>
						
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_use_collapse; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_use_collapse">
					<?php if($xls_pricelist_use_collapse){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_use_protection; ?></label>
				<div class="col-sm-10">
				<select class="form-control" name="xls_pricelist_use_protection" onchange="lavascript:if($(this).val()==1){$('#use_password').fadeIn('slow');}else{$('#use_password').fadeOut('slow');}">
					<?php if($xls_pricelist_use_protection){ ?>
					<option value="0"><?php echo $text_no; ?></option>
					<option value="1" selected="selected"><?php echo $text_yes; ?></option>
					<?php }else{ ?>
					<option value="0" selected="selected"><?php echo $text_no; ?></option>
					<option value="1"><?php echo $text_yes; ?></option>
					<?php } ?>
				</select>
				<span id="use_password" <?php if(!$xls_pricelist_use_protection) echo'style="display:none;"'; ?>><br/>&nbsp;&nbsp;&nbsp;<b><?php echo $entry_use_password; ?></b>&nbsp;<input type="text" name="xls_pricelist_use_password" value="<?php echo $xls_pricelist_use_password; ?>" class="form-control" /></span>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10"><?php echo $entry_colors; ?></div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_thead_color; ?></label>
				<div class="col-sm-10">
					<?php echo $entry_color_text; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[thead]" value="<?php echo $xls_pricelist_colors['thead']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					<?php echo $entry_color_bg; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[thead_bg]" value="<?php echo $xls_pricelist_colors['thead_bg']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_underthead_color; ?></label>
				<div class="col-sm-10">
					<?php echo $entry_color_bg; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[underthead_bg]" value="<?php echo $xls_pricelist_colors['underthead_bg']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_category0_color; ?></label>
				<div class="col-sm-10">
					<?php echo $entry_color_text; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[category0]" value="<?php echo $xls_pricelist_colors['category0']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					<?php echo $entry_color_bg; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[category0_bg]" value="<?php echo $xls_pricelist_colors['category0_bg']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_category1_color; ?></label>
				<div class="col-sm-10">
					<?php echo $entry_color_text; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[category1]" value="<?php echo $xls_pricelist_colors['category1']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					<?php echo $entry_color_bg; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[category1_bg]" value="<?php echo $xls_pricelist_colors['category1_bg']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" ><?php echo $entry_category2_color; ?></label>
				<div class="col-sm-10">
					<?php echo $entry_color_text; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[category2]" value="<?php echo $xls_pricelist_colors['category2']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					<?php echo $entry_color_bg; ?>&nbsp;
					<div class="input-group jpicker">
						<input type="text" class="form-control"  name="xls_pricelist_colors[category2_bg]" value="<?php echo $xls_pricelist_colors['category2_bg']; ?>" />
						<span class="input-group-addon"><i></i></span>
					</div>
					
				</div>
			</div>
			
				
			</div>
		</div>
		<div class="form-group ">
			<div class="col-sm-10">
				<input type="hidden" name="action" value="generate" />
				<a onclick="$('#form').attr('target', '_blank'); $('#form').attr('action', '<?php echo $view; ?>'); $('#form').submit();"  class="btn btn-primary"><?php echo $button_view; ?></a>
				
				<a id="a_xls_pricelist" class="btn btn-primary" onclick="gen_price();"><?php echo $text_xls_pricelist; ?></a>
			</div>
		</div>
	</div> 
	</form>	
	</div> 
    </div> 
  </div>
</div>

<script type="text/javascript"><!--

function show_alert(a, t){
	$('#a_xls_pricelist').after('<div class="alert alert-'+t+'"><i class="fa fa-exclamation-circle"></i>'+a+'<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
	}


function gen_price() {
	$('#form .alert').remove();
	$('#a_xls_pricelist').attr('onclick', "").html('<img src="view/image/loading.gif" />');
	$.ajax({
		url: '<?php echo HTTP_CATALOG; ?>index.php?route=xls/xls_pricelist',
		type: 'post',
		data: 'action=generate',
		dataType: 'json',
		success: function(json) {
			$('#a_xls_pricelist').attr('onclick', "gen_price();").html('<?php echo $text_xls_pricelist; ?>');
			
			if (json['redirect']) {
				location = json['redirect'];
			}
			
			if (json['error']) {
				if (json['error']['warning']) {
					show_alert(json['error']['warning'], 'danger');
				}
			}	 
						
			if (json['success']) {
				show_alert('<?php echo $text_xls_success; ?>', 'success');
			}	
		},
		error: function(x, t, m) {
			if(t==="timeout") {
				show_alert('timeout exceeded', 'warning');
			} else {
				show_alert(t, 'warning');
			}
		},
		timeout: 300000
	});
	return false;
};


function a_jpicker(a){
	/*
	$('.jpicker').jPicker({
			window:
			{
				expandable: true,
				position:
				{
					x: '10px', // acceptable values "left", "center", "right", "screenCenter", or relative px value
					y: '10px', // acceptable values "top", "bottom", "center", or relative px value
				}
			},
			images:
			{
				clientPath: 'view/javascript/jquery/jpicker/images/' // Path to image files
			},
			
		});
	*/
	if(a==1){
		$('.jpicker').colorpicker('destroy');
	}
	else{
		$('.jpicker').colorpicker({
			customClass: 'colorpicker-2x',
			//format:'hex',
            sliders: {
                saturation: {
                    maxLeft: 200,
                    maxTop: 200
                },
                hue: {
                    maxTop: 200
                },
                alpha: {
                    maxTop: 200
                }
            }
        }).on('destroy.jpicker', function(event){
			$('.jpicker').colorpicker({
				customClass: 'colorpicker-2x',
				format:'hex',
				sliders: {
					saturation: {
						maxLeft: 200,
						maxTop: 200
					},
					hue: {
						maxTop: 200
					},
					alpha: {
						maxTop: 200
					}
				}
			});
		});
	}	
}


var pole_row = <?php echo $pole_row; ?>;

function addPole() {	
	html  = '<li id="pole-row' + pole_row + '" class="ui-state-default"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span>';
	html += '<table  class="list">';
	html += '<tbody >';
	html += '  <tr>';
	html += '    <td class="left"><?php echo $entry_pole_type; ?><br /><select name="xls_pricelist_poles[' + pole_row + '][type]">';
	<?php foreach ($poletypes as $poletype) { ?>
	html += '      <option value="<?php echo $poletype; ?>"><?php echo $poletype; ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><?php echo $entry_pole_name; ?><br />';
	<?php foreach ($languages as $language) { ?>
	html += '<input type="text" name="xls_pricelist_poles[' + pole_row + '][name][<?php echo $language['language_id']; ?>]" value="" size="20" />';
	html += '<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /><br />';
	<?php } ?>
	html += '</td>';
	html += '<td class="left"><?php echo $entry_dimensions; ?><br />';
	html += '<input type="text" name="xls_pricelist_poles[' + pole_row + '][dlina]" value="15" size="3" />';
	html += '</td>';
	html += '<td class="left">';
	html += '<?php echo $entry_alignment; ?><br />';
	html += '<select name="xls_pricelist_poles[' + pole_row + '][alignment]">';
	<?php foreach ($alignments as $key=>$alignment) { ?>
	html += '<option value="<?php echo $key; ?>"><?php echo $alignment; ?></option>';
	<?php } ?>
	html += '</select>';
	html += '</td>';
	html += '	 <td class="left">';
	html += '	<?php echo $entry_color_text; ?>&nbsp;';
	html += '<div class="input-group jpicker">';
	html += '<input type="text" class="form-control"  name="xls_pricelist_poles[' + pole_row + '][textcolor]" value="000000" />';
	html += '<span class="input-group-addon"><i></i></span>';
	html += '</div>';
	html += '	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	html += '	<?php echo $entry_color_bg; ?>&nbsp;';
	html += '<div class="input-group jpicker">';
	html += '<input type="text" class="form-control"  name="xls_pricelist_poles[' + pole_row + '][bgcolor]" value="" />';
	html += '<span class="input-group-addon"><i></i></span>';
	html += '</div>';
	html += '	</td>';
	html += '    <td class="left"><a onclick="removePole('+pole_row+');" class="btn btn-danger">x</a></td>';
	html += '  </tr>';
	html += '</tbody>';
	html += '</table>';
	html += '</li>';
	
	
	$('#sortable').append(html);
	

	a_jpicker(1);
	
	pole_row++;
}

function removePole(i){
	
	
	$('#pole-row' + i).remove();
	a_jpicker(1);
}

//--></script>

<script type="text/javascript"><!--

	$(document).ready(function(){
		$( "#sortable" ).sortable();
		a_jpicker(0);
	});
	
	
//--></script> 
<script type="text/javascript"><!--
$('input, select').change(function(){
	$('#xls_pricelist').html('<a class="top" style="color:red;" ><?php echo $text_save; ?></a>');
								
});

$('select[name="xls_pricelist_usecache"]').change(function(){
	if($(this).val()=='memcache')$('div.memcache_params').fadeIn('slow');
	else $('div.memcache_params').fadeOut('slow');							
});



//--></script> 
  <script type="text/javascript"><!--
$('#language a:first').tab('show');
//--></script>

<?php echo $footer; ?>