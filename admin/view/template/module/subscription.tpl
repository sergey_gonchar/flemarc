<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
   <div class="page-header">
      <div class="container-fluid">
         <div class="pull-right">
            <button type="submit" form="form-subscription" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
         </div>
         <h1><?php echo $heading_title; ?></h1>
         <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
         </ul>
      </div>
   </div>
   <div class="container-fluid">
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
         <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <div class="panel panel-default">
         <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
         </div>
         <div class="panel-body">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-subscription" class="form-horizontal">
               <div class="form-group">
                  <label class="col-sm-2 control-label" for="newsletter-status"><?php echo $entry_status; ?></label>
                  <div class="col-sm-10">
                     <select name="subscription_status" id="newsletter-status" class="form-control">
                        <?php if ($subscription_status) { ?>
                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <hr>              			   
			   <h3><?php echo $entry_cupon; ?></h3>
               <br>
			   <div class="col-xs-12">
                  <ul id="language" class="nav nav-tabs">
                     <?php foreach ($languages as $language) { ?>
                     <li><a data-toggle="tab" href="#tab-language-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                     <?php } ?>
                  </ul>
                  <div class="tab-content">
                     <?php foreach ($languages as $language) { ?>
                     <div class="tab-pane" id="tab-language-<?php echo $language['language_id']; ?>">
                        <div class="form-group">
                           <div class="col-sm-10">
                              <textarea id="text-content<?php echo $language['language_id']; ?>" name="subscription_cupon[<?php echo $language['language_id']; ?>]" cols="50" rows="8" class="form-control"><?php echo isset($subscription_cupon[$language['language_id']]) ? $subscription_cupon[$language['language_id']] : ''; ?></textarea>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                  </div>
               </div>			   
               <br><hr>
			   <h3><?php echo $entry_text; ?></h3>
               <br>
			   <div class="col-xs-12">
                  <ul id="language2" class="nav nav-tabs">
                     <?php foreach ($languages as $language) { ?>
                     <li><a data-toggle="tab" href="#tab-language2-<?php echo $language['language_id']; ?>"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                     <?php } ?>
                  </ul>
                  <div class="tab-content">
                     <?php foreach ($languages as $language) { ?>
                     <div class="tab-pane" id="tab-language2-<?php echo $language['language_id']; ?>">
                        <div class="form-group">
                           <div class="col-sm-10">
                              <textarea id="email-content<?php echo $language['language_id']; ?>" name="subscription_content_tab[<?php echo $language['language_id']; ?>]" cols="50" rows="8" class="form-control"><?php echo isset($subscription_content_tab[$language['language_id']]) ? $subscription_content_tab[$language['language_id']] : ''; ?></textarea>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                  </div>
               </div>        
         </form>
      </div>
	  </div>
   </div>
</div>
<script type="text/javascript"><!--
   <?php foreach ($languages as $language) { ?>
   $('#email-content<?php echo $language['language_id']; ?>, #text-content<?php echo $language['language_id']; ?>').summernote({height: 300});
   <?php } ?>
   $('#language a:first, #language2 a:first').tab('show');
   //--></script>	
<?php echo $footer; ?>