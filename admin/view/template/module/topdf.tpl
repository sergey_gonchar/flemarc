<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
 
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
      
      <form action="<?php echo $savepdf; ?>" method="post" enctype="multipart/form-data" id="form-product">   
      
 <table style="text-align: center;" align="center" border="1">
<tr style="border: 1px solid;" align="center">
<th style="border: 1px solid; width: 15%; font-size: 12pt; font-weight: bold; padding: 5px"><?php echo $head1; ?>
</th>
<th style="border: 1px solid; font-size: 12pt; font-weight: bold;width: 900px;padding: 5px">

<label class="control-label">2. Выберите шаблон:</label>


<label class="radio-inline">
  <input type="radio" name="templateRadioOptions" id="templateRadio1" value="option1" checked="true">Последовательный вывод
</label>
<label class="radio-inline">
  <input type="radio" name="templateRadioOptions" id="templateRadio2" value="option2"> Табличный
</label>
<br />


<input type="text" name="InputName" id="idInputName" style="display: none"/>
    <input type="email" name="InputEmail" id="idInputEmail" style="display: none"/>
     <input type="text" name="InputNameColor" id="idInputNameColor" style="display: none"/>
     <input type="text" name="InputEmailColor" id="idInputEmailColor" style="display: none"/>
     
     <input type="text" name="InputPhone" id="idInputPhone" style="display: none"/>
     <input type="text" name="InputPhoneColor" id="idInputPhoneColor" style="display: none"/>
     
    <input type="checkbox" name="footer_titul" id="idfooter_titul" style="display: none"/>
    <input type="text" name="FTPServer" id="idFTPServer" style="display: none"/>
    <input type="text" name="FTPUser" id="idFTPUser" style="display: none"/>
    <input type="password" name="FTPPassword" id="idFTPPassword" style="display: none"/>

    <input type="text" name="field_option" id="idfield_option" style="display: none"/>
    <input type="text" name="content_option" id="idcontent_option" style="display: none"/>

    


<div class="form-group">
<label class="control-label">3. Общие опции:</label>	
	<label class="checkbox-inline"><input type="checkbox" name="titul" id="idtitul"/><?php echo $text_add_titul; ?><span id="str_add"></span></label>
	<label class="checkbox-inline"><input type="checkbox" name="razryv" id="idrazryv"/>разрыв страницы после каждого товара</label>
	<label class="checkbox-inline"><input type="checkbox" name="add_footer" id="idfooter"/>Добавить нижний колонтитул (футер)</label>
	<label class="checkbox-inline"><input type="checkbox" name="add_content" id="idadd_content"/>Добавить содержание</label>
	<button type="button" id="id_option_field" class="btn btn-default">Настроить формат вывода полей </button>
	</div>

</th>
<th style="border: 1px solid; font-size: 12pt; font-weight: bold;width: 20%; padding: 5px"><?php echo $head3; ?>
</th>
</tr>
<tr>
	<td>
	
	
	
<table align="center">
<tr>
<td>
<!--<button type="button" id="button-add" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-default" onclick="$('#form-product').attr('action', '<?php echo $add; ?>').submit()"><?php echo $button_add; ?></button>-->
<button type="button" id="button-add" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-default"><?php echo $button_add; ?></button>
</td>
<td rowspan=2 style="height: 128px;  width: 128px;background-position: center, center; background-repeat: no-repeat;background-image: url('view/image/pdf_logo.png'); align="center" valign="center"><p style="font-size: 11pt; font-weight: bold;"><?php echo $head1_1; ?></p>
		<p style="font-size: 14pt; font-weight: bold;" id="pdf_basket">  <?php if ($arr_topdf) echo count($arr_topdf); else echo 0; ?> </p>
		<p style="font-size: 11pt; font-weight: bold;"><?php echo $head1_2; ?></p>
</td>
</tr>
<tr>
<td>
<button type="button" data-toggle="tooltip" title="<?php echo $button_clear; ?>" class="btn btn-default" onclick="$('#form-product').attr('action', '<?php echo $clear; ?>').submit()"><?php echo $button_clear; ?></button>
</td>
</tr>


	</table>	
	
<br/>	<br/>

<button type="button" id="qrgen" data-toggle="tooltip" title="" class="btn btn-default">Сгенерировать qr-коды</button><br/>
<p id="idsgenerir"></p></td>	

	<td>

<table align="center" id="tmpl_option1" border="0">
		<th style="font-size: 12pt; font-weight: bold;width: 100%;padding: 5px"><?php echo $head2; ?>
			<br/>
			<div style="text-align: left; font-size: 10pt; font-weight: normal;"><?php echo $head2_1; ?></div>
			<div style="text-align: left; font-size: 10pt; font-weight: normal;"><?php echo $head2_2; ?></div>
			</th>
			<tr>
			<td id="ish_td" style="height: 64px; ">

	
	
	<a class="button" id="sort_name"><?php echo $column_name; ?></a>
	<a class="button" id="sort_model"><?php echo $column_model; ?></a>
	<a class="button" id="sort_category"><?php echo $column_category; ?></a>
	<a class="button" id="sort_manufacturer"><?php echo $column_manufacturer; ?></a>
	<a class="button" id="sort_price"><?php echo $column_price; ?></a>
	<a class="button" id="sort_image"><?php echo $column_image;?></a>
	<a class="button" id="sort_description"><?php echo $column_description; ?></a>
	<a class="button" id="sort_url"><?php echo $column_url; ?></a>
	<a class="button" id="sort_qrcode"><?php echo $column_qrcode; ?></a>
	
	
</td>
				
			</tr>
			<td id="movi" style="height: 64px; background-color: #fdfdce">
			</td>
			<tr>
			
			<tr>
				<td><div class="buttons"><a class="button" id="sbros" ><?php echo $button_sbros; ?></a>
					</div>
				
				<div id="text_sort"></div>
				<div id="input_sort"></div>
			</td>
				

			</tr>	
				
			</tr>
			
			
		</table>
<div id="tmpl_option2" style="display: none;">Шаблон №2. Полностью статический. Все выводимые поля фиксированные. Настраивать можно только общие опции(титульный, разрыв, колонтитул, содержание).</div>	
	</td>
	<td>
	
	<div class="buttons">

<!--<label><input type="checkbox" name="ftp" id="idftp"/>Ftp</label><br /> -->
<button type="button" id="btn_savepdf" data-toggle="tooltip" title="<?php echo $button_savepdf; ?>" class="btn btn-success" onclick="$('#form-product').submit()"><?php echo $button_savepdf; ?></button> <br/>
<progress id="idprogress" value="0" max="100"></progress><br /> 
<div id="crud_catalog" style="display: none">
<a href="<?php echo $pdf_download; ?>" class="btn btn-info">Скачать</a>
<a href="<?php echo $pdf_ftp; ?>" id="idftp2" class="btn btn-info">Закачать на FTP</a>
<a href="<?php echo $pdf_delete; ?>" class="btn btn-danger">Удалить</a><br /> 
</div>
<p id="pdf_fileinfo"><?php echo $pdf_fileinfo; ?></p>

</div>

	</td>
	
</tr>
</table>     
      
  
      
      
      
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
            
            
            <div class="form-group">
                <label class="control-label" for="input-cat">Категория</label>
                <select name="filter_category" id="input-cat" class="form-control" multiple>
                 <option value="*"></option>
                 <?php 
                 
                 $tmp_cat = explode(",", $filter_category);
                
                 foreach($uni_categories as $value) {
                 	
                 echo '<option value="'.$value.'"';
                
                 foreach($tmp_cat as $cat_filter) {
                 	
                 	if($cat_filter==$value){ echo ' selected';
						
					}
                 	
                 }
                 
                 echo '>'.$value.'</option>';
                 }
                  ?>                  
                </select>
              </div>
            
            
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-model"><?php echo $entry_model; ?></label>
                <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-price"><?php echo $entry_price; ?></label>
                <input type="text" name="filter_price" value="<?php echo $filter_price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_status && !is_null($filter_status)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        
        
        
       <div class="fright">
          <strong><?php echo 'Количество товаров'?></strong> <select name="limit" onchange="">
            <?php foreach($limits as $limit_value) { ?>
            <option value="<?php echo $limit_value['href']; ?>"<?php if($limit == $limit_value['value']) { ?> selected="selected"<?php } ?>><?php echo $limit_value['value']; ?></option>
            <?php } ?>
          </select>
        </div>
        
        
        
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-center"><?php echo $column_image; ?></td>
                  <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.model') { ?>
                    <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'p.price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'p.quantity') { ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($product['product_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" class="myselected" value="<?php echo $product['product_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" class="myselected" value="<?php echo $product['product_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-center"><?php if ($product['image']) { ?>
                    <img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" />
                    <?php } else { ?>
                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                    <?php } ?></td>
                  <td class="text-left"><?php echo $product['name']; ?></td>
                  <td class="text-left"><?php echo $product['model']; ?></td>
                  <td class="text-right"><?php if ($product['special']) { ?>
                    <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                    <div class="text-danger"><?php echo $product['special']; ?></div>
                    <?php } else { ?>
                    <?php echo $product['price']; ?>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($product['quantity'] <= 0) { ?>
                    <span class="label label-warning"><?php echo $product['quantity']; ?></span>
                    <?php } elseif ($product['quantity'] <= 5) { ?>
                    <span class="label label-danger"><?php echo $product['quantity']; ?></span>
                    <?php } else { ?>
                    <span class="label label-success"><?php echo $product['quantity']; ?></span>
                    <?php } ?></td>
                  <td class="text-left"><?php echo $product['status']; ?></td>
                  <td class="text-right"><a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>


<div id="dialog_titul"></div>

<div id="dialog_footer">
	<form enctype="multipart/form-data">
  <div class="form-group">
    <label for="idNameDlg">Название интернет-магазина</label>
    <input type="text" class="form-control" name="NameDlg" id="idNameDlg" value="<?php echo $InputName;?>" maxlength="50">
    <label for="idNameDlgColor">Цвет поля "Название интернет-магазина" </label>
   <input type="color" id="idNameDlgColor" name="NameColorDlg" value="<?php echo $InputNameColor;?>" style="width:15%;">
  </div>
  <div class="form-group">
    <label for="idEmailDlg">Email</label>
    <input type="email" class="form-control" id="idEmailDlg" name="EmailDlg" value="<?php echo $InputEmail;?>" maxlength="30">
     <label for="idEmailDlgColor">Цвет поля "Email" </label>
   <input type="color" id="idEmailDlgColor" name="EmailColorDlg" value="<?php echo $InputEmailColor;?>" style="width:15%;">
  </div>
 <div class="form-group">
    <label for="idPhoneDlg">Телефон</label>
    <input type="text" class="form-control" id="idPhoneDlg" name="PhoneDlg">
     <label for="idPhoneDlgColor">Цвет поля "Телефон" </label>
   <input type="color" id="idPhoneDlgColor" name="PhoneColorDlg" value="<?php echo $InputPhoneColor;?>" style="width:15%;">
  </div>
  
  <div class="form-group">
    <input type="checkbox" id="idfootituldlg"/>Выводить колонтитул на титульном листе 
    
  </div>
   

 </form>
 <div align="right"><button type="button" id="idsbros_option_footer" class="btn btn-default">Конфигурация по умолчанию</button></div>	
</div> 


  
  <div id="dialog_ftp">
	<form class="form-inline" action="<?echo $pdf_ftp?>" id="idform_ftp" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="idFtp_server">Server</label>
    <input type="text" name="FTPServer1" class="form-control" id="idFTPServer_Dlg" placeholder="FTP Server" value="<?echo $ftp_server?>">
  </div>
  <div class="form-group">
    <label for="idFtp_User">FTP User</label>
    <input type="text" name="FTPUser1" class="form-control" id="idFTPUser_Dlg" placeholder="FTP User" value="<?echo $ftp_user_name?>">
  </div>
  <div class="form-group">
    <label for="idFtp_PSWD">FTP Password</label>
    <input type="password" name="FTPPassword1" class="form-control" id="idFTPPassword_Dlg" placeholder="FTP Password" value="<?echo $ftp_user_pass?>">
  </div>
  </form>
  
 </div> 
  
<div id="dialog_field">
	
<form>
 <table class="table table-bordered">
 
 <thead>
 	<td>Поле</td>
 	<td>Формат вывода</td>
 	
 </thead>
 <tr>
 <td>Название товара</td>
 <td>
 
 <div class="form-group">
 <label class="radio-inline">
  <input type="radio" name="name_f_out" id="idname_f_out1" value="option1" <?if($name_f_out==='option1') echo 'checked="true"';?>>Название поля: значение поля 
</label>
<label class="radio-inline">
  <input type="radio" name="name_f_out" id="idname_f_out2" value="option2" <?if($name_f_out==='option2') echo 'checked="true"';?>> Значение поля
</label>
</div>

 <div class="form-group" id="name_format1">
<label>Название поля</label>
<input type="text" name="name_text_field" value="<?php echo $name_text_field;?>">
<label>Цвет поля</label><input type="color" name="name_text_color" value="<?php echo $name_text_color;?>" style="width:5%;">
</div>

 <div class="form-group" id="name_format2">
<label>Цвет значения поля</label>
<input type="color" name="name_znach_color" value="<?php echo $name_znach_color;?>" style="width:5%;">
</div>
</td>
</tr>

 <tr>
 <td>Модель</td>
 <td>
 
 <div class="form-group">
 <label class="radio-inline">
  <input type="radio" name="model_f_out" id="idmodel_f_out1" value="option1" <?if($model_f_out==='option1') echo 'checked="true"';?>>Название поля: значение поля 
</label>
<label class="radio-inline">
  <input type="radio" name="model_f_out" id="idmodel_f_out2" value="option2" <?if($model_f_out==='option2') echo 'checked="true"';?>> Значение поля
</label>
</div>

 <div class="form-group" id="model_format1">
<label>Название поля</label>
<input type="text" value="<?php echo $model_text_field;?>" name="model_text_field">
<label>Цвет поля</label><input type="color" name="model_text_color" value="<?php echo $model_text_color;?>" style="width:5%;">
</div>

 <div class="form-group" id="model_format2">
<label>Цвет значения поля</label>
<input type="color" name="model_znach_color" value="<?php echo $model_znach_color;?>" style="width:5%;">
</div>

</td>
 
 </tr>

 <tr>
 <td>Категория</td>
 <td>
 
 <div class="form-group">
 <label class="radio-inline">
  <input type="radio" name="category_f_out" id="idcategory_f_out1" value="option1" <?if($category_f_out==='option1') echo 'checked="true"';?>>Название поля: значение поля 
</label>
<label class="radio-inline">
  <input type="radio" name="category_f_out" id="idcategory_f_out2" value="option2" <?if($category_f_out==='option2') echo 'checked="true"';?>> Значение поля
</label>
</div>

 <div class="form-group" id="category_format1">
<label>Название поля</label>
<input type="text" value="<?php echo $category_text_field;?>" name="category_text_field">
<label>Цвет поля</label><input type="color" name="category_text_color" value="<?php echo $category_text_color;?>" style="width:5%;">
</div>

 <div class="form-group" id="category_format2">
<label>Цвет значения поля</label>
<input type="color" name="category_znach_color" value="<?php echo $category_znach_color;?>" style="width:5%;">
</div>

</td>
 
 </tr>

 <tr>
 <td>Производитель</td>
 <td>
 
 <div class="form-group">
 <label class="radio-inline">
  <input type="radio" name="manufacture_f_out" id="idmanufacture_f_out1" value="option1" <?if($manufacture_f_out==='option1') echo 'checked="true"';?>>Название поля: значение поля 
</label>
<label class="radio-inline">
  <input type="radio" name="manufacture_f_out" id="idmanufacture_f_out2" value="option2" <?if($manufacture_f_out==='option2') echo 'checked="true"';?>> Значение поля
</label>
</div>

 <div class="form-group" id="manufacture_format1">
<label>Название поля</label>
<input type="text" value="<?php echo $manufacture_text_field;?>" name="manufacture_text_field">
<label>Цвет поля</label><input type="color" name="manufacture_text_color" value="<?php echo $manufacture_text_color;?>" style="width:5%;">
</div>

 <div class="form-group" id="manufacture_format2">
<label>Цвет значения поля</label>
<input type="color" name="manufacture_znach_color" value="<?php echo $manufacture_znach_color;?>" style="width:5%;">
</div>

</td>
 
 </tr>




 <tr>
 <td>Цена</td>
 <td>


 <div class="form-group">
 Цена
 <label class="radio-inline">
  <input type="radio" name="price_f_out" id="idprice_f_out1" value="option1" <?if($price_f_out==='option1') echo 'checked="true"';?>>Название поля: значение поля 
</label>
<label class="radio-inline">
  <input type="radio" name="price_f_out" id="idprice_f_out2" value="option2" <?if($price_f_out==='option2') echo 'checked="true"';?>> Значение поля
</label>
</div>

<div class="form-group" id="price_format1">
<label>Название поля</label>
<input type="text" value="<?php echo $price_text_field;?>" name="price_text_field">
<label>Цвет поля</label><input type="color" name="price_text_color" value="<?php echo $price_text_color;?>" style="width:5%;">
</div>

 <div class="form-group" id="price_format2">
<label>Цвет значения поля</label>
<input type="color" name="price_znach_color" value="<?php echo $price_znach_color;?>" style="width:5%;">
</div>

<div class="form-group">
    <input type="checkbox" name="price_old_out" id="idprice_old_out" <?if($price_old_out==='on') echo 'checked="true"';?>/>Выводить старую цену
</div>

 <div class="form-group" id="idprice_old_format2" style="display: none">
<label>Цвет значения поля для старой цены</label>
<input type="color" name="price_old_znach_color" value="<?php echo $price_old_znach_color;?>" style="width:5%;">
</div>


<div class="form-group">
    <input type="checkbox" name="price_discount_out" id="idprice_discount_out" <?if($price_discount_out==='on') echo 'checked="true"';?>/>Выводить Дисконт
</div>

 <div class="form-group" style="display: none" id="iddiscount_out">
Дисконт
 <div class="form-group">
 
 <label class="radio-inline">
  <input type="radio" name="price_discount_f_out" id="idprice_discount_f_out1" value="option1" <?if($price_discount_f_out==='option1') echo 'checked="true"';?>>Название поля: значение поля 
</label>
<label class="radio-inline">
  <input type="radio" name="price_discount_f_out" id="idprice_discount_f_out2" value="option2" <?if($price_discount_f_out==='option2') echo 'checked="true"';?>> Значение поля
</label>
</div>

<div class="form-group" id="price_discount_format1">
<label>Название поля</label>
<input type="text" value="<?php echo $price_discount_text_field;?>" name="price_discount_text_field">
<label>Цвет поля</label><input type="color" name="price_discount_text_color" value="<?php echo $price_discount_text_color;?>" style="width:5%;">
</div>

 <div class="form-group" id="price_format2">
<label>Цвет значения поля</label>
<input type="color" name="price_discount_znach_color" value="<?php echo $price_discount_znach_color;?>" style="width:5%;">
</div>

</div>

</td>
 
 </tr>

 <tr>
 <td>Описание</td>
 <td>
 
 <div class="form-group">
 <label class="radio-inline">
  <input type="radio" name="description_f_out" id="iddescription_f_out1" value="option1" <?if($description_f_out==='option1') echo 'checked="true"';?>>Название поля: значение поля 
</label>
<label class="radio-inline">
  <input type="radio" name="description_f_out" id="iddescription_f_out2" value="option2" <?if($description_f_out==='option2') echo 'checked="true"';?>> Значение поля
</label>
</div>

 <div class="form-group" id="description_format1">
<label>Название поля</label>
<input type="text" value="<?php echo $description_text_field;?>" name="description_text_field">
<label>Цвет поля</label><input type="color" name="description_text_color" value="<?php echo $description_text_color;?>" style="width:5%;">
</div>

 <div class="form-group" id="description_format2">
<label>Цвет значения поля</label>
<input type="color" name="description_znach_color" value="<?php echo $description_znach_color;?>" style="width:5%;">
</div>

</td>
 
 </tr>

 <tr>
 <td>URL-cсылка на товар</td>
 <td>
 
 <div class="form-group">
 <label class="radio-inline">
  <input type="radio" name="url_f_out" id="idurl_f_out1" value="option1" <?if($url_f_out==='option1') echo 'checked="true"';?>>Название поля: значение поля 
</label>
<label class="radio-inline">
  <input type="radio" name="url_f_out" id="idurl_f_out2" value="option2" <?if($url_f_out==='option2') echo 'checked="true"';?>> Значение поля
</label>
</div>

 <div class="form-group" id="url_format1">
<label>Название поля</label>
<input type="text" value="<?php echo $url_text_field;?>" name="url_text_field">
<label>Цвет поля</label><input type="color" name="url_text_color" value="<?php echo $url_text_color;?>" style="width:5%;">
</div>

 <div class="form-group" id="url_format2">
<label>Цвет значения поля</label>
<input type="color" name="url_znach_color" value="<?php echo $url_znach_color;?>" style="width:5%;">
</div>

</td>
 
 </tr>
 
</table>

<label><input type="checkbox" name="save_option_field" id="idsave_option_field"/>Cохранить конфигурацию</label>
<img id="loadImg" src="/image/ajax-loader.gif" style="display: none" />
<img id="loadOk" src="/image/ajax-ok.ico" style="display: none" />
<div align="right"><button type="button" id="idsbros_option_field" class="btn btn-default">Конфигурация по умолчанию</button></div>
</form>	 
</div> 

<div id="dialog_content">

<pre>При выводе содержания товары сортируются по категориям. 
В содержании выводятся названия категорий в виде ссылок,
по которым можно переходить на страницу-разделитель категорий.</pre>

 
<form enctype="multipart/form-data">
  
   <div class="form-group">
<label>Название поля "Содержание"</label>
<input type="text" name="content_text_field" value="<?php echo $content_text_field;?>"><br>
<label>Цвет поля</label><input type="color" name="content_text_color" value="<?php echo $content_text_color;?>" style="width:10%;">
<label>Фон поля</label>
<input type="color" name="content_fon" value="<?php echo $content_fon;?>" style="width:10%;">
</div>

<div class="form-group">
<label>Элементы содержания</label><br />
<label>Цвет текста</label><input type="color" name="content_element_color" value="<?php echo $content_element_color;?>" style="width:10%;">
<label>Фон</label>
<input type="color" name="content_element_fon" value="<?php echo $content_element_fon;?>" style="width:10%;">
</div>

<div class="form-group">
<label>Разделители страниц</label><br />
<pre>(вывод отдельных страниц-разделителей с элементами содержания)</pre><br />
<label>Цвет текста</label><input type="color" name="content_razdelitel_color" value="<?php echo $content_razdelitel_color;?>" style="width:10%;">
<label>Фон</label>
<input type="color" name="content_razdelitel_fon" value="<?php echo $content_razdelitel_fon;?>" style="width:10%;">
</div>
<!--</td>
</tr>-->

<div class="form-group">

<label><input type="checkbox" name="save_option_content" id="idsave_option_content"/>Cохранить конфигурацию</label>
<img id="content_loadImg" src="/image/ajax-loader.gif" style="display: none" />
<img id="content_loadOk" src="/image/ajax-ok.ico" style="display: none" />

</div>

 </form>
 

 <div align="right"><button type="button" id="idsbros_option_content" class="btn btn-default">Конфигурация по умолчанию</button></div>	
</div> 

 <form id="forma_add" method="post" enctype="multipart/form-data" >
 	<input type="hidden" id="idforma_add_selected" name="forma_add_selected"></input>
 	
 </form>
 
<script src="view/javascript/jquery/jquery.maskedinput.min.js" type="text/javascript"></script>


<?php if(isset($arr_topdf)) $arr_topdf=implode(',',$arr_topdf);?>
  
  <script type="text/javascript"><!--
  
  var str_sel="";
  
  var arr_pdf = '<?php echo $arr_topdf; ?>';

  
 /* <?php if ($arr_topdf) echo count($arr_topdf); else echo 0; ?> */
  
 if($('#pdf_basket').html()==0) $('#btn_savepdf').attr("disabled","true"); else $('#btn_savepdf').removeAttr('disabled');
  
$('#button-filter').on('click', function() {
	var url = 'index.php?route=module/topdf&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').val();

	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	var filter_category = $('select[name=\'filter_category\']').val();
	
	/*
	if (filter_category != '*' && filter_category!=null) {
		
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}
	*/
	if (filter_category != '*') {
		
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}
	
	url += '&sort=<?php echo $sort; ?>&order=<?php echo $order; ?>';

	location = url;
});

$('select[name=\'limit\']').on('change', function(){
	
	var url=$(this).val();
	
	url=filter_sort(url);
	
	//location = url;
	$('#form-product').attr('action', url).submit();
	
	
});


$('#qrgen').on('click', function() {
	
	
	var arr = arr_pdf.split(',');
	
	var index;
	var arr_kol=arr.length;
		
for (index = 0; index < arr.length; ++index) {



 
  
$.ajax({
  type: 'POST',
  async: false,
  url: 'index.php?route=module/topdf/qrgen&token=<?php echo $token; ?>',
  data: "save_data="+arr[index],
  success: function(data){
  
  	$('#idsgenerir').html('Cгенерировано '+(index+1)+' товаров из'+arr_kol);  
  
  }
 
});	

}

	
	
});

function filter_sort(str){
	
	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		str += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		str += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		str += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').val();

	if (filter_quantity) {
		str += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		str += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	var filter_category = $('select[name=\'filter_category\']').val();
		
	/*
	if (filter_category != '*' && filter_category!=null) {
		
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}
	*/
	if (filter_category != '*' && filter_category!=null) {
		
		str += '&filter_category=' + encodeURIComponent(filter_category);
	}
	
	str += '&sort=<?php echo $sort; ?>&order=<?php echo $order; ?>';
	
	
	return str;
	
}




$('#button-add').on('click', function() {
	var url = 'index.php?route=module/topdf/add&token=<?php echo $token; ?>';
/*
	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').val();

	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	var filter_category = $('select[name=\'filter_category\']').val();
	
	
	if (filter_category != '*' && filter_category!=null) {
		
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}
	
	url += '&sort=<?php echo $sort; ?>&order=<?php echo $order; ?>';
*/
   url=filter_sort(url);
   
  // alert($('select[name=\'limit\'] option:selected').text());
   url += '&limit='+$('select[name=\'limit\'] option:selected').text();
	//location = url;
	str_sel="";
	$('input[name="selected[]"]').each(function(){
		 if($(this).prop("checked")){ str_sel+=$(this).val()+',';}
		});
		
		str_sel=(str_sel.substr(0, str_sel.length - 1));
			$('#idforma_add_selected').val(str_sel);
			
	
	
	//$('#form-product').attr('action', url).submit();
	
	$('#forma_add').attr('action', url).submit();
	
});
//--></script>
   <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=module/topdf/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_model\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=module/topdf/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['model'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_model\']').val(item['label']);
	}
});
//--></script>

<script type="text/javascript" src="view/javascript/jquery/jquery-ui/jquery-ui.min.js"></script>
  
<script type="text/javascript"><!--
	
$(document).ready(function(){
	
function myprogress(){
	
$('#idprogress').val('0');

var intervalID =setInterval(function(){ 


$.ajax({
		    url: 'index.php?route=module/topdf/progressing&token=<?php echo $token; ?>',
			type: 'POST',
			async: 'false',
			success: function(data) {
				
				$('#idprogress').val(data);
				console.log(data);
				if(data<100) myprogress();	
				else clearInterval(intervalID);		
				 } 
		});


} , 300);	
	
}


 $("#idPhoneDlg").mask("8(999)999-9999");
$("#idPhoneDlg").val("<?php echo $InputPhone;?>");
	
$('#idsave_option_field').change(function(){ 
   if($(this).prop("checked")){ 
 $("#loadImg").show();
 $("#loadOk").hide();
 
  
$.ajax({
  type: 'POST',
  url: 'index.php?route=module/topdf/save_option_field&token=<?php echo $token; ?>',
  data: "save_data="+(JSON.stringify($(this).closest("form").serializeObject())),
  success: function(data){$("#loadImg").hide();$("#loadOk").show();}
});
 
 
 
  }else{ 
  
  
  } 
});

$('#idsave_option_content').change(function(){ 
   if($(this).prop("checked")){ 
 $("#content_loadImg").show();
 $("#content_loadOk").hide();
 
  
$.ajax({
  type: 'POST',
  url: 'index.php?route=module/topdf/save_option_content&token=<?php echo $token; ?>',
  data: "save_data="+(JSON.stringify($(this).closest("form").serializeObject())),
  success: function(data){$("#content_loadImg").hide();$("#content_loadOk").show();}
});
 
 
 
  }else{ 
  
  
  } 
});




$('#idsbros_option_field').on("click", function(){
	
	
	$('input[name="name_text_field"]').val('Наименование');
	$('input[name="name_text_color"]').val('#000000');
	$('input[name="name_znach_color"]').val('#000000');
	
	$('input[name="model_text_field"]').val('Модель');
	$('input[name="model_text_color"]').val('#000000');
	$('input[name="model_znach_color"]').val('#000000');
	
	$('input[name="category_text_field"]').val('Категория');
	$('input[name="category_text_color"]').val('#000000');
	$('input[name="category_znach_color"]').val('#000000');
	
	$('input[name="manufacture_text_field"]').val('Производитель');
	$('input[name="manufacture_text_color"]').val('#000000');
	$('input[name="manufacture_znach_color"]').val('#000000');
	
	$('input[name="price_text_field"]').val('Цена');
	$('input[name="price_text_color"]').val('#000000');
	$('input[name="price_znach_color"]').val('#000000');
	
	
	
	$('input[name="price_old_znach_color"]').val('#000000');
	
	$('input[name="price_discount_text_field"]').val('Дисконт');
	$('input[name="price_discount_text_color"]').val('#000000');
	$('input[name="price_discount_znach_color"]').val('#000000');
	
	$('input[name="description_text_field"]').val('Описание');
	$('input[name="description_text_color"]').val('#000000');
	$('input[name="description_znach_color"]').val('#000000');
	
	$('input[name="url_text_field"]').val('Ссылка на товар');
	$('input[name="url_text_color"]').val('#000000');
	$('input[name="url_znach_color"]').val('#000000');
	
	
	$('[id*=format1]').show();
	
	$("#idprice_old_out").removeAttr("checked").change();
	$("#idprice_discount_out").removeAttr("checked").change();
	
	$('#dialog_field input:radio').each(function(inx,element){
		
		
		
	if($(element).val()=="option1") $(element).prop("checked", "true");
			
		
	});
	
	
	
	
	
});


$('#idsbros_option_footer').on("click", function(){
	
	
	$('input[name="NameDlg"]').val('');
	$('input[name="NameColorDlg"]').val('#000000');
	$('input[name="EmailDlg"]').val('');
	$('input[name="EmailColorDlg"]').val('#000000');
	$('input[name="PhoneDlg"]').val('');
	$('input[name="PhoneColorDlg"]').val('#000000');

	
	
});

$('#idsbros_option_content').on("click", function(){
	
	
	$('input[name="content_text_field"]').val('Содержание');
	$('input[name="content_text_color"]').val('#000000');
	$('input[name="content_fon"]').val('#ffffff');
	$('input[name="content_element_color"]').val('#000000');
	$('input[name="content_element_fon"]').val('#ffffff');
	$('input[name="content_razdelitel_color"]').val('#000000');
	$('input[name="content_razdelitel_fon"]').val('#ffffff');
	
		
});


$('#form-product').attr("action",'index.php?route=module/topdf/savepdf1&token=<?php echo $token; ?>');

$('input[name=templateRadioOptions]').on('change' , 
function() {  

switch (this.value) {

   case "option1": 
   	
   $('#tmpl_option2').hide();
   $('#tmpl_option1').show();
   $('#id_option_field').removeAttr("disabled");
     
 	$('#form-product').attr('action', filter_sort('index.php?route=module/topdf/savepdf1&token=<?php echo $token; ?>'));
   	
   	break;
   	
   

   case "option2": 
   	
   $('#id_option_field').attr("disabled","true");
   	$('#tmpl_option1').hide();
   $('#tmpl_option2').show();
   	
   	  $('#form-product').attr("action",filter_sort('index.php?route=module/topdf/savepdf2&token=<?php echo $token; ?>'));
   	  
   	break;
   	
   
     

   default: 	alert("опция не выбрана");break;

} 
}
);

if($('#pdf_fileinfo').text().length!=0) {
	
	 $('#crud_catalog').show();
}	
	
$('#btn_savepdf').on("click", myprogress);
	
	
 $('#dialog_footer').dialog({ 
	
	position: { my: "center top+100", at: 'center top+100', of: window }, 
	height: "auto",
	width: "auto", 
	modal: true,
	autoOpen: false,
	buttons: {"Cохранить":function(){ 
	
			$.ajax({
		  type: 'POST',
		  url: 'index.php?route=module/topdf/save_option_footer&token=<?php echo $token; ?>',
		  data: "save_data="+(JSON.stringify($('form', this).serializeObject())),
		  success: function(data){}
		});
	
	$("#idInputName").val($("#idNameDlg").val());
	$("#idInputEmail").val($("#idEmailDlg").val());
	$("#idInputPhone").val($("#idPhoneDlg").val());
	
	
	$("#idInputNameColor").val($("#idNameDlgColor").val());
	$("#idInputEmailColor").val($("#idEmailDlgColor").val());
	$("#idInputPhoneColor").val($("#idPhoneDlgColor").val());
	
		
	if($("#idfootituldlg").prop("checked")){$("#idfooter_titul").prop("checked",true); }
	else {$("#idfooter_titul").prop("checked",false);}
	
	$(this).dialog( "close" );
	
	}, "Отмена":function(){ 
		
		$(this).dialog( "close" );	
		$("#idfooter").removeAttr("checked");
		$("#idfooter_titul").prop("checked",false);
							
	}}});
		
$("#idfooter").change(function(e){ 
  if($(this).prop("checked")){ 

 $('#dialog_footer').dialog("open"); }
 }); 	
	

function onchcheck()
{
		
  if($(this).prop("checked")){ 

 $('#dialog_titul').dialog({ 
	
	position: { my: "center top+100", at: 'center top+100', of: window }, 
	height: "auto",
	width: "auto", 
	modal: true,
	open: function( event, ui ) {$('#dialog_titul').append(
"<p>Для вставки графического файла на титульном листе каталога воспользуйтесь кнопкой снизу.</br>"
+"Файл выравнивается по левому верхнему углу. Чтобы расположить файл по цетру просто создайте</br>"
+"в любом графическом редакторе лист размером а4, поместите на него картинку и тогда</br>"
+"на титульном листе рисунок будет распологаться так, как у Вас на листе.</br></br>"
+"<input type='file' id='idpicture' name='picture'/>");},
	buttons: {"Cохранить":function(){ 
	
	
	$("#ish_td #idpicture").remove();
	$("#ish_td").append($('#idpicture'));
	$('#idpicture').css('display','none');
	$('#str_add').html($('#idpicture').val());
	
	
	$(this).dialog( "close" );
	}, "Отмена":function(){ 
									$(idtitul).removeAttr("checked");
									$("#idpicture").remove();
									$(this).dialog( "close" );
									$('#str_add').html('');
	}}});
 
   }else{ 
 
 $("#idpicture").remove();
$('#str_add').html('');
  } 
	
}
$("#idtitul").change(onchcheck);	
	
 $('#dialog_ftp').dialog({ 
	
	position: { my: "center top+100", at: 'center top+100', of: window }, 
	height: "auto",
	width: "auto", 
	modal: true,
	autoOpen: false,
	buttons: {"Cохранить":function(){ 
	
	$("#idFTPServer").val($("#idFTPServer_Dlg").val());
	$("#idFTPUser").val($("#idFTPUser_Dlg").val());
	$("#idFTPPassword").val($("#idFTPPassword_Dlg").val());

		$("#idform_ftp").submit();	
	//$(this).dialog( "close" );
	
	}, "Отмена":function(){ 
		
		$(this).dialog( "close" );	
		$("#idftp").removeAttr("checked");
		
							
	}}});
	
	
		
$("#idftp").change(function(e){ 
  if($(this).prop("checked")){ 

 $('#dialog_ftp').dialog("open"); }
 }); 
 
$("#idftp2").click(function(e){ 
 
 e.preventDefault();
 $('#dialog_ftp').dialog("open"); 
 
 }); 
 
 
 
  $('#dialog_content').dialog({ 
	
	position: { my: "center top+100", at: 'center top+100', of: window }, 
	height: "auto",
	width: "auto", 
	modal: true,
	autoOpen: false,
	buttons: {"Ok":function(){ 
	
 $('#idcontent_option').val(encodeURIComponent(JSON.stringify($('form',this).serializeObject())));
	$(this).dialog( "close" );
	
	}
	}});
 
 
$("#idadd_content").change(function(e){ 
  if($(this).prop("checked")){ 

 $('#dialog_content').dialog("open"); }
 }); 
 
 
 
 
 
 
 
 
  $('#dialog_field').dialog({ 
	
	position: { my: "center top+100", at: 'center top+100', of: window }, 
	height: "auto",
	width: 800, 
	modal: true,
	autoOpen: false,
	open: function( event, ui ) {
		
		if($("#idprice_old_out", event.target).attr("checked")=='checked'){
			
			$('#idprice_old_format2').show();
			
		} 
		
		if($("#idprice_discount_out", event.target).attr("checked")=='checked'){
			
			$('#iddiscount_out').show();
			
		} 
		
	
		$("input:radio", event.target).each(function(indx, element){
			
			
var str = $(element).attr("name");str=(str.substr(0, str.length - 6));		
	
/*
if($(element).prop("checked")){ $('#'+str+'_format1').show();}
else {
	$('#'+str+'_format1').hide();
}	*/

	
switch ($(element).val()) {
   case "option1": if($(element).prop("checked")){ $('#'+str+'_format1').show();} break;
   case "option2": if($(element).prop("checked")){ $('#'+str+'_format1').hide();} break;
   default: 	alert("опция не выбрана");break;
}

			});
		
	},
	buttons: {"Ok":function(){ 
	
	 $('#idfield_option').val(encodeURIComponent(JSON.stringify($('form',this).serializeObject())));
	 
	//console.log(JSON.stringify($('form',this).serializeObject()));
	//alert(JSON.stringify($('form',this).serializeObject()));
	$(this).dialog( "close" );
	
	}}});
	
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
		
$("#id_option_field").click(function(e){ 
$('#dialog_field').dialog("open"); 
 }); 
 
 
$("#idprice_old_out").change(function(e){ 
  if($(this).prop("checked")){ 

$('#idprice_old_format2').show(); }

else {
	$('#idprice_old_format2').hide(); 
	
	}
 
 }); 
 
 $("#idprice_discount_out").change(function(e){ 
  if($(this).prop("checked")){ 

$('#iddiscount_out').show(); }

else {
	$('#iddiscount_out').hide(); 
	
	}
 
 });  

$('input[name=name_f_out]').on('change' , 
function() {  

switch (this.value) {
   case "option1": $('#name_format1').show();break;
   case "option2": $('#name_format1').hide();break;
   default: 	alert("опция не выбрана");break;
}
		    }
 
 );
 
 $('input[name=model_f_out]').on('change' , 
function() {  

switch (this.value) {
   case "option1": $('#model_format1').show();break;
   case "option2": $('#model_format1').hide();break;
   default: 	alert("опция не выбрана");break;
}
		    }
 
 );
 
  $('input[name=category_f_out]').on('change' , 
function() {  

switch (this.value) {
   case "option1": $('#category_format1').show();break;
   case "option2": $('#category_format1').hide();break;
   default: 	alert("опция не выбрана");break;
}
		    }
 
 );
 
  

 
 $('input[name=manufacture_f_out]').on('change' , 
function() {  

switch (this.value) {
   case "option1": $('#manufacture_format1').show();break;
   case "option2": $('#manufacture_format1').hide();break;
   default: 	alert("опция не выбрана");break;
}
		    }
 
 );
 
  
 $('input[name=price_f_out]').on('change' , 
function() {  

switch (this.value) {
   case "option1": $('#price_format1').show();break;
   case "option2": $('#price_format1').hide();break;
   default: 	alert("опция не выбрана");break;
}
		    }
 
 );
 
 
 
 
  $('input[name=price_discount_f_out]').on('change' , 
function() {  

switch (this.value) {
   case "option1": $('#price_discount_format1').show();break;
   case "option2": $('#price_discount_format1').hide();break;
   default: 	alert("опция не выбрана");break;
}
		    }
 
 );
 
 
 $('input[name=description_f_out]').on('change' , 
function() {  

switch (this.value) {
   case "option1": $('#description_format1').show();break;
   case "option2": $('#description_format1').hide();break;
   default: 	alert("опция не выбрана");break;
}
		    }
 
 );
 
 $('input[name=url_f_out]').on('change' , 
function() {  

switch (this.value) {
   case "option1": $('#url_format1').show();break;
   case "option2": $('#url_format1').hide();break;
   default: 	alert("опция не выбрана");break;
}
		    }
 
 );
 

$(".button").button();
	
var tmp;
var ev;
var td=$("#ish_td").html();



	$("#sbros").on("click", function(){
	
		
	$("#ish_td").html(td);
	$("#idtitul").change(onchcheck);
	$("#movi").empty();
	$("#input_sort").empty();	
		
		$("#sort_name").draggable({
	
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
  stop: function(event, ui){
  	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
     $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  
  }
});

$("#sort_model").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
  stop: function(event, ui){
  	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});

$("#sort_category").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
 stop: function(event, ui){
 	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});
$("#sort_manufacturer").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
   stop: function(event, ui){
   	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});
$("#sort_price").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
   stop: function(event, ui){
   	ev=$(event.target).text();
    tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});
$("#sort_image").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
   stop: function(event, ui){
   	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});
$("#sort_description").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
   stop: function(event, ui){
   	ev=$(event.target).text();
    tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});

$("#sort_url").draggable({
	
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
  stop: function(event, ui){
  	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
     $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  
  }
});

$("#sort_qrcode").draggable({
	
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
  stop: function(event, ui){
  	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
     $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  
  }
});
		
	
		
	});
	
$("#sort_name").draggable({
	
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
  stop: function(event, ui){
   ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   
     $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  
  }
});

$("#sort_model").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
  stop: function(event, ui){
  	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});

$("#sort_category").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
 stop: function(event, ui){
 	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});
$("#sort_manufacturer").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
   stop: function(event, ui){
   	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});
$("#sort_price").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
   stop: function(event, ui){
   	ev=$(event.target).text();
    tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});
$("#sort_image").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
   stop: function(event, ui){
   	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});
$("#sort_description").draggable({
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
   stop: function(event, ui){
   	ev=$(event.target).text();
    tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
   $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  }
});

$("#sort_url").draggable({
	
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
  stop: function(event, ui){
  	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
     $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  
  }
});

$("#sort_qrcode").draggable({
	
  connectToSortable:"#movi",
  helper: "clone",
  revert:"invalid",
  stop: function(event, ui){
  	ev=$(event.target).text();
   tmp=$('#ish_td a:contains("'+$(event.target).text()+'")').attr("id");
     $('#ish_td a:contains("'+$(event.target).text()+'")').remove();
  
  }
});


$( "#movi" ).sortable({
	 
	revert:true,
	receive: function(event, ui) { 
 

	$('#movi a:contains("'+ev+'")').attr("id",tmp);
   
   $('#input_sort').append('<input type="hidden" name="sort_hidden[]" value="'+tmp+'" />');
 
	},
   update: function() { 

   $('#text_sort').empty();
   $('#movi a').each(function(index){	$('#text_sort').append($(this).text()); 
   										$('[name*=sort_hidden]').eq(index).val($(this).attr('id'));	
   										
   										
   									 });
   
    
    }
    
    });

	
});	
//--></script>  
</div>
<?php echo $footer; ?>