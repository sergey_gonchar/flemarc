<!DOCTYPE html>
<html>
  <head>
  <title><?php echo $text_login; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap -->
	<link href="view/admin-login/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="view/admin-login/css/custom.css" rel="stylesheet" media="screen">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:700,400,300,300italic' rel='stylesheet' type='text/css'>
	<!-- Font-Awesome -->
	<link rel="stylesheet" type="text/css" href="view/admin-login/css/font-awesome.css" media="screen" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="view/admin-login/css/font-awesome-ie7.css" media="screen" /><![endif]-->

	<!-- Load jQuery -->
	<script src="view/admin-login/js/jquery.v2.0.3.js"></script>

</head>
<body>
<!-- 100% Width & Height container  -->
<div class="login-fullwidith">

<!-- Login Wrap  -->
<div class="login-wrap"><div class="login-image"></div>
<?php if ($success) { ?>
      <div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
      <div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<div class="login-c1">
				<div class="cpadding50">
					<input type="text" class="form-control logpadding"  name="username" value="<?php echo $username; ?>" placeholder="<?php echo $entry_username; ?>" >
					<br/>
					<input type="password" class="form-control logpadding" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>">

				</div>
			</div>
			<div class="login-c2">
				<div class="logmargfix">
					<div class="chpadding50">
							<div class="alignbottom">
								<button class="bluebtn"  type="submit" onclick="errorMessage()"><?php echo $button_login; ?></button>
							</div>
					</div>
				</div>
			</div>

	  <div class="login-c3">
				<div class="left"><a href="../" class="whitelink"><span></span></a></div>
				<?php if ($forgotten) { ?>
				<div class="right"><a href="<?php echo $forgotten; ?>" class="whitelink"><?php echo $text_forgotten; ?></a></div>
				<?php } ?>
	  </div>
	  <!-- End of Login Wrap  --><!-- End of Container  -->

	    <!-- Javascript  -->
        <script src="view/admin-login/js/initialize-loginpage.js"></script>
        <script src="view/admin-login/js/jquery.easing.js"></script>
        <!-- Load Animo -->
        <script src="view/admin-login/js/animo.js"></script>
        <script>
        function errorMessage(){
            $('.login-wrap').animo( { animation: 'tada' } );
        }
        </script>

        <?php if ($redirect) { ?>
        	<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        <?php } ?>
	</form>
  </div>
</div>
<script type="text/javascript">
<!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#form').submit();
	}
});
//-->
</script>
</body>
</html>