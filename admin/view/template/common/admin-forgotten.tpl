<!DOCTYPE html>
<html>
  <head>
  <title><?php echo $heading_title; ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap -->
	<link href="view/admin-login/css/bootstrap.css" rel="stylesheet" media="screen">
	<link href="view/admin-login/css/custom.css" rel="stylesheet" media="screen">
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:700,400,300,300italic' rel='stylesheet' type='text/css'>
	<!-- Font-Awesome -->
	<link rel="stylesheet" type="text/css" href="view/admin-login/css/font-awesome.css" media="screen" />
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="view/admin-login/css/font-awesome-ie7.css" media="screen" /><![endif]-->

	<!-- Load jQuery -->
	<script src="view/admin-login/js/jquery.v2.0.3.js"></script>

</head>
<body>
<!-- 100% Width & Height container  -->
<div class="login-fullwidith">

<!-- Login Wrap  -->
<div class="login-wrap"><div class="login-image"></div>
<?php if ($error_warning) { ?>
      <div class="warning long"><?php echo $error_warning; ?></div>
<?php } ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<div class="login-c1">
				<div class="cpadding50">
				<label for="input-email"><?php echo $heading_title; ?></label>
					<input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
				</div>
			</div>
			<div class="login-c2">
							<div class="alignbottom">
                			<button type="submit" class="bluebtn"><i class="fa fa-check"></i> <?php echo $button_reset; ?></button>
							</div>
			</div>
			
			<div class="login-c3">
				<div class="left"><a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="whitelink"><?php echo $button_cancel; ?></a></div>
	  </div>

	  <!-- End of Login Wrap  --><!-- End of Container  -->

	    <!-- Javascript  -->
        <script src="view/admin-login/js/initialize-loginpage.js"></script>
        <script src="view/admin-login/js/jquery.easing.js"></script>
        <!-- Load Animo -->
        <script src="view/admin-login/js/animo.js"></script>
        <script>
        function errorMessage(){
            $('.login-wrap').animo( { animation: 'tada' } );
        }
        </script>
	</form>
  </div>
</div>
<script type="text/javascript">
<!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#form').submit();
	}
});
//-->
</script>
</body>
</html>