<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
  </div>
  <div class="container-fluid">
  	<p>Whenever you access this page the database optimizer runs, <b>OK</b> means the table has just been optimized</p>
  	<table class="table table-striped table-hover">
  		<thead>
  			<tr>
  				<th>Table</th>
  			</tr>
  		</thead>
  		<tbody>
  			<?php foreach($tables as $table => $status) : ?>
  				<tr>
  					<td><?php echo $table; ?></td>
  					<td><?php echo $status; ?></td>
  				</tr>
  			<?php endforeach; ?>
  		</tbody>
  	</table>
  </div>
</div>
<?php echo $footer; ?>