<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="contacts-content">
<style>
.panel-body {min-height:460px;}
#tab-send {opacity:0;}
#tab-send input[type="checkbox"] {margin-top:9px;}
#select-region, #tab-send .to {display:none;}
#select-region {padding:0;}
.products-block, .selproducts-block, .products-body {display:none;}
.text-button {padding-left:5px;}
#selproduct .fa {color:#CE5F5F;cursor:pointer;}
.wait {display:block;width:100%;height:100%;padding:8px 0;font-size:14px;text-align:center;background-color:#60C174;color:#fff;border-radius:3px;}
.well .alert, .save-log .alert, .status-log .alert {margin-bottom:0;}
.contacts-content .well {padding:9px;}
.alert-danger {border-color:#F9B5C0;color:#E02727;}
.spravka_tegi span {width:100px;display:inline-block;}
.form-control.small-input {text-align:center;padding:8px 5px;}
.form-horizontal .form-group > label {text-align:left;padding-left:25px;}
.alert-warning .fa {padding-right:3px;}
.contacts-heading {overflow:hidden;}
#bta {padding: 4px 13px;}
.text-lic {color: #FF0039;}
.text-lic .alert {margin-bottom:0;padding:4px 10px;}
#input-manual {min-height:100px;height:auto;resize:vertical;}
.td-button {width:230px;}
@media (min-width: 992px) {#temp-load {width:120px;}}
@media (min-width: 768px) {.nopadding {padding:0;}}
@media (max-width: 1200px) {
#select-region .col-lg-6 {padding:0;}
#select-region label {padding-left:0;padding-right:0;}
}
@media (max-width: 991px) {
.col-sm-1 {padding-right:0;}
#select-region .col-lg-6 {padding: 2px 0;}
.form-horizontal .form-group {margin-bottom:2px;padding-top:6px;padding-bottom:6px;background:#FCFCFC;border-top: 1px solid #EAEAEA;border-bottom: 1px solid #EAEAEA;}
.products-body .products-block {padding: 2px 0;}
.form-message {padding-top:5px;}
.status-log.save-log {padding: 5px 0 0 0;}
}
@media (max-width: 767px) {
#column-left.active + #content {margin-left:235px;left:0;}
#column-left.active + #content + #footer {margin-left:235px;left:0;}
#column-left.active + #content .well .col-xs-6 {width:100%;padding-top:2px;padding-bottom:2px;}
#column-left.active + #content .text-button {display:none;}
.form-horizontal .form-group {margin-left: -15px;margin-right: -15px;background: #FCFCFC;padding-top: 10px;padding-bottom: 15px;margin-bottom: 2px;border-top: 1px solid #ddd;border-bottom: 1px solid #ddd;}
#temp-load {width:100%;padding-top:5px;}
.button-block {text-align:center;padding-top:5px;}
#select-region .col-lg-6 {padding: inherit;padding-left:15px;padding-right:15px;}
#select-region label {padding-left:15px;padding-right:15px;}
.td-button {width:inherit;}
.lic-active {text-align:center;}
.lic-active > div {padding: 2px 0;}
}
@media (max-width: 480px) {.text-button {display:none;}}
</style>
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <a id="button-send" data-toggle="tooltip" title="<?php echo $button_send; ?>" class="btn btn-primary" onclick="send('index.php?route=marketing/contacts/send&token=<?php echo $token; ?>');"><i class="fa fa-envelope"></i><span class="text-button"><?php echo $button_send; ?></span></a>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  
  <div class="container-fluid">
    <div class="panel panel-default">
  
    <div class="panel-heading contacts-heading">
	  <div class="col-sm-4 col-md-3">
		<h3 class="panel-title"><i class="fa fa-envelope"></i> <?php echo $heading_title; ?></h3>
	  </div>
	  <?php if (!$lic) { ?>
	  <div class="col-sm-8 col-md-9 lic-active">
		<div class="col-sm-12 text-lic"><?php echo $text_no_lic; ?></div>
	  </div>
	  <?php } ?>
    </div>
	
    <div class="panel-body">
	<form class="form-horizontal">
	
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-send" data-toggle="tab"><?php echo $tab_send; ?></a></li>
            <li><a href="#tab-template" data-toggle="tab"><?php echo $tab_template; ?></a></li>
            <li><a href="#tab-log" data-toggle="tab"><?php echo $tab_log; ?></a></li>
			<li><a href="#tab-setting" data-toggle="tab"><?php echo $tab_setting; ?></a></li>
        </ul>

		<div class="tab-content">

			<div class="tab-pane active" id="tab-send">
				
				  <div class="form-group required">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-store"><?php echo $entry_store; ?></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					<div class="col-sm-9 col-md-8 col-lg-6">
					  <select name="store_id" id="input-store" class="form-control">
						<option value="0"><?php echo $text_default; ?></option>
						<?php foreach ($stores as $store) { ?>
						<option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
						<?php } ?>
					  </select>
					</div>
					</div>
				  </div>
				  
				  <div class="form-group required">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-to"><?php echo $entry_to; ?></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					  <div class="col-sm-9 col-md-8 col-lg-6 select-block">
						<select name="to" id="input-to" class="form-control">
						<option value="newsletter"><?php echo $text_newsletter; ?></option>
						<option value="customer_all"><?php echo $text_customer_all; ?></option>
						<option value="client_all"><?php echo $text_client_all; ?></option>
						<option value="customer_group"><?php echo $text_customer_group; ?></option>
						<option value="customer"><?php echo $text_customer; ?></option>
						<option value="affiliate_all"><?php echo $text_affiliate_all; ?></option>
						<option value="affiliate"><?php echo $text_affiliate; ?></option>
						<option value="product"><?php echo $text_product; ?></option>
						<option value="manual"><?php echo $text_manual; ?></option>
						</select>
					  </div>
					</div>
				  </div>
				  
				  <div class="form-group to" id="to-customer-group">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-customer-group"><?php echo $entry_customer_group; ?></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					<div class="col-sm-9 col-md-8 col-lg-6">
					  <select name="customer_group_id" id="input-customer-group" class="form-control">
						<?php foreach ($customer_groups as $customer_group) { ?>
						<option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
						<?php } ?>
					  </select>
					</div>
					</div>
				  </div>
				  
				  <div class="form-group to" id="to-customer">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-customer"><span data-toggle="tooltip" title="<?php echo $help_customer; ?>"><?php echo $entry_customer; ?></span></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					<div class="col-sm-9 col-md-8 col-lg-6">
					  <input type="text" name="customers" value="" placeholder="<?php echo $entry_customer; ?>" id="input-customer" class="form-control" />
					  <div id="block_customer" class="well well-sm" style="min-height:50px;height:auto;max-height:250px;overflow:auto;"></div>
					</div>
					</div>
				  </div>
				  
				  <div class="form-group to" id="to-affiliate">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-affiliate"><span data-toggle="tooltip" title="<?php echo $help_affiliate; ?>"><?php echo $entry_affiliate; ?></span></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					<div class="col-sm-9 col-md-8 col-lg-6">
					  <input type="text" name="affiliates" value="" placeholder="<?php echo $entry_affiliate; ?>" id="input-affiliate" class="form-control" />
					  <div id="block_affiliate" class="well well-sm" style="min-height:50px;height:auto;max-height:250px;overflow:auto;"></div>
					</div>
					</div>
				  </div>
				  
				  <div class="form-group to" id="to-product">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-product"><span data-toggle="tooltip" title="<?php echo $help_product; ?>"><?php echo $entry_product; ?></span></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					<div class="col-sm-9 col-md-8 col-lg-6">
					  <input type="text" name="products" value="" placeholder="<?php echo $entry_product; ?>" id="input-product" class="form-control" />
					  <div id="block_product" class="well well-sm" style="min-height:50px;height:auto;max-height:250px;overflow:auto;"></div>
					</div>
					</div>
				  </div>
				  
				  <div class="form-group to" id="to-manual">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-manual"><span data-toggle="tooltip" title="<?php echo $help_manual; ?>"><?php echo $entry_manual; ?></span></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					<div class="col-sm-9 col-md-8 col-lg-6">
					  <textarea name="manual" value="" placeholder="<?php echo $help_manual; ?>" id="input-manual" class="form-control"></textarea>
					</div>
					</div>
				  </div>
				  
				  <div class="form-group form-region">
						<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="region"><?php echo $text_region; ?></label>
						<div class="col-sm-9 col-md-9 col-lg-10">
							<div class="col-sm-1">
							<?php if ($set_region) { ?>
								<input id="region" class="form-control" type="checkbox" name="set_region" value="1" checked="checked" />
							<?php } else { ?>
								<input id="region" class="form-control" type="checkbox" name="set_region" value="0" />
							<?php } ?>
							</div>
					
							<div class="col-sm-11 col-md-11 col-lg-11" id="select-region">
						
							<div class="col-sm-12 col-md-6 col-lg-6">
								<label class="col-sm-4 col-md-3 col-lg-3 control-label" for="input-country"><?php echo $entry_country; ?></label>
								<div class="col-sm-8 col-md-9 col-lg-9">
									<select name="country_id" id="input-country" class="form-control">
									<option value=""><?php echo $text_select; ?></option>
									<?php foreach ($countries as $country) { ?>
									<?php if ($country['country_id'] == $country_id) { ?>
										<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
									<?php } else { ?>
										<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
									<?php } ?>
									<?php } ?>
									</select>
								</div>
							</div>
						
							<div class="col-sm-12 col-md-6 col-lg-6">
								<label class="col-sm-4 col-md-3 col-lg-3 control-label" for="input-zone"><?php echo $entry_zone; ?></label>
								<div class="col-sm-8 col-md-9 col-lg-9">
									<select name="zone_id" id="input-zone" class="form-control"></select>
								</div>
							</div>
						  
							</div>
						</div>
				  </div>

				  <div class="form-group">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="insert_products"><?php echo $entry_insert_products; ?></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
						<div class="col-sm-1">
							<input id="insert_products" class="form-control" type="checkbox" value="0" />
						</div>
					</div>
				  </div>
				  
					<div class="form-group products-body" id="special-product">
					  <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="entry_special"><?php echo $entry_special; ?></label>
					  <div class="col-sm-9 col-md-9 col-lg-10">
						<div class="col-sm-1 col-lg-1">
							<input id="entry_special" name="special" type="checkbox" value="0" class="form-control products-checkbox" />
						</div>
						<div class="col-sm-11 col-lg-11 nopadding">
						<div class="col-sm-12 col-md-8 col-lg-9 nopadding products-block">
							<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="special-title"><?php echo $entry_title; ?></label>
							<div class="col-sm-9 col-md-9 col-lg-10"><input type="text" class="form-control" id="special-title" name="special_title" value="" /></div>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-3 nopadding products-block">
							<label class="col-sm-8 col-md-7 col-lg-7 control-label" for="special-limit"><?php echo $entry_limit; ?></label>
							<div class="col-sm-4 col-md-5 col-lg-5"><input type="text" class="form-control small-input" id="special-limit" name="special_limit" value="" /></div>
						</div>
						</div>
					  </div>
					</div>
					
					<div class="form-group products-body" id="bestseller-product">
					  <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="entry_bestseller"><?php echo $entry_bestseller; ?></label>
					  <div class="col-sm-9 col-md-9 col-lg-10">
						<div class="col-sm-1 col-lg-1">
							<input id="entry_bestseller" name="bestseller" type="checkbox" value="0" class="form-control products-checkbox" />
						</div>
						<div class="col-sm-11 col-lg-11 nopadding">
						<div class="col-sm-12 col-md-8 col-lg-9 nopadding products-block">
							<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="bestseller-title"><?php echo $entry_title; ?></label>
							<div class="col-sm-9 col-md-9 col-lg-10"><input type="text" id="bestseller-title" class="form-control" name="bestseller_title" value="" /></div>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-3 nopadding products-block">
							<label class="col-sm-8 col-md-7 col-lg-7 control-label" for="bestseller-limit"><?php echo $entry_limit; ?></label>
							<div class="col-sm-4 col-md-5 col-lg-5"><input type="text" id="bestseller-limit" class="form-control small-input" name="bestseller_limit" value="" /></div>
						</div>
						</div>
					  </div>
					</div>
					
					<div class="form-group products-body" id="featured-product">
					  <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="entry_featured"><?php echo $entry_featured; ?></label>
					  <div class="col-sm-9 col-md-9 col-lg-10">
						<div class="col-sm-1 col-lg-1">
							<input id="entry_featured" name="featured" type="checkbox" value="0" class="form-control products-checkbox" />
						</div>
						<div class="col-sm-11 col-lg-11 nopadding">
						<div class="col-sm-12 col-md-8 col-lg-9 nopadding products-block">
							<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="featured-title"><?php echo $entry_title; ?></label>
							<div class="col-sm-9 col-md-9 col-lg-10"><input type="text" id="featured-title" class="form-control" name="featured_title" value="" /></div>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-3 nopadding products-block">
							<label class="col-sm-8 col-md-7 col-lg-7 control-label" for="featured-limit"><?php echo $entry_limit; ?></label>
							<div class="col-sm-4 col-md-5 col-lg-5"><input type="text" id="featured-limit" class="form-control small-input" name="featured_limit" value="" /></div>
						</div>
						</div>
					  </div>
					</div>
					
					<div class="form-group products-body" id="latest-product">
					  <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="entry_latest"><?php echo $entry_latest; ?></label>
					  <div class="col-sm-9 col-md-9 col-lg-10">
						<div class="col-sm-1 col-lg-1">
							<input id="entry_latest" name="latest" type="checkbox" value="0" class="form-control products-checkbox" />
						</div>
						<div class="col-sm-11 col-lg-11 nopadding">
						<div class="col-sm-12 col-md-8 col-lg-9 nopadding products-block">
							<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="latest-title"><?php echo $entry_title; ?></label>
							<div class="col-sm-9 col-md-9 col-lg-10"><input type="text" id="latest-title" class="form-control" name="latest_title" value="" /></div>
						</div>
						<div class="col-sm-12 col-md-4 col-lg-3 nopadding products-block">
							<label class="col-sm-8 col-md-7 col-lg-7 control-label" for="latest-limit"><?php echo $entry_limit; ?></label>
							<div class="col-sm-4 col-md-5 col-lg-5"><input type="text" id="latest-limit" class="form-control small-input" name="latest_limit" value="" /></div>
						</div>
						</div>
					  </div>
					</div>
					
					<div class="form-group products-body" id="selectproducts-product">
					  <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="entry_selected"><?php echo $entry_selected; ?></label>
					  <div class="col-sm-9 col-md-9 col-lg-10">
						<div class="col-sm-1 col-lg-1">
							<input id="entry_selected" name="selectproducts" type="checkbox" value="0" class="form-control sproducts-checkbox" />
						</div>
						<div class="col-sm-11 col-lg-11 products-block selproducts-block">
							<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="selproducts-title"><?php echo $entry_title; ?></label>
							<div class="col-sm-9 col-md-9 col-lg-10"><input type="text" id="selproducts-title" class="form-control" name="selproducts_title" value="" placeholder="<?php echo $entry_title; ?>" /></div>
						</div>
					  </div>
					</div>
					
					<div class="form-group selproducts-block" id="showsel">
					  <label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-sproducts"><span data-toggle="tooltip" title="<?php echo $help_selproduct; ?>"><?php echo $entry_pselected; ?></span></label>
					  <div class="col-sm-9 col-md-9 col-lg-10">
					  <div class="col-sm-12">
						<input type="text" id="input-sproducts" class="form-control" name="sproducts" value="" placeholder="<?php echo $entry_pselected; ?>" />
						<div id="block_selproduct" class="well well-sm" style="min-height:50px;height:auto;max-height:250px;overflow:auto;"></div>
					  </div>
					  </div>
					</div>
				  
				  <div class="form-group">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-template"><?php echo $entry_template; ?></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
						<div class="col-xs-12 col-sm-9 col-md-8 col-lg-6">
						<select name="template_id" id="input-template" class="form-control">
						  <option value="0" selected="selected"><?php echo $text_none; ?></option>
						  <?php foreach ($templates as $template) { ?>
						  <?php if ($template['template_id'] == $template_id) { ?>
						  <option value="<?php echo $template['template_id']; ?>" selected="selected"><?php echo $template['name']; ?></option>
						  <?php } else { ?>
						  <option value="<?php echo $template['template_id']; ?>"><?php echo $template['name']; ?></option>
						  <?php } ?>
						  <?php } ?>
						</select>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-4 col-lg-4" id="temp-load">
						</div>
					</div>
				  </div>
						
				  <div class="form-group form-unsubscribe">
					<label class="col-xs-9 col-sm-4 col-md-3 col-lg-2 control-label" for="unsubscribe"><?php echo $entry_unsubscribe; ?></label>
					<div class="col-xs-3 col-sm-8 col-md-9 col-lg-10 select-block">
						<div class="col-sm-12 col-md-12 col-lg-12">
							<?php if ($set_unsubscribe) { ?>
								<input id="unsubscribe" type="checkbox" class="form-control" name="set_unsubscribe" value="1" checked="checked" />
							<?php } else { ?>
								<input id="unsubscribe" type="checkbox" class="form-control" name="set_unsubscribe" value="0" />
							<?php } ?>
						</div>
					</div>
				  </div>
				  
				  <div class="form-group required">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-subject"><span data-toggle="tooltip" title="<?php echo $help_subject; ?>"><?php echo $entry_subject; ?></span></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
						<div class="col-sm-12 col-md-12 col-lg-12 select-block">
							<input type="text" name="subject" value="" placeholder="<?php echo $entry_subject; ?>" id="input-subject" class="form-control" />
						</div>
					</div>
				  </div>
				  
				  <div class="form-group required">
					<label class="col-sm-12 col-md-3 col-lg-2 control-label" for="input-message"><span data-toggle="tooltip" title="<?php echo $help_message; ?>"><?php echo $entry_message; ?></span></label>
					<div class="col-sm-12 col-md-9 col-lg-10">
					<div class="col-sm-12 form-message">
					  <textarea name="message" id="input-message" class="form-control"></textarea>
					</div>
					</div>
				  </div>
				  
				  <div class="form-group">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="new-temp-name"><?php echo $text_save_template; ?></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					  <div class="col-sm-12">
					  <div class="col-sm-8 col-md-5 col-lg-5 nopadding">
						<input id="new-temp-name" type="text" name="new_temp_name" value="" class="form-control" />
					  </div>
					  <div class="col-sm-4 col-md-3 col-lg-2 button-block">
						<a class="btn btn-success" onclick="addtemplate();" id="savetempl"><i class="fa fa-save"></i><span class="text-button"><?php echo $text_save; ?></span></a>
					  </div>
					  <div class="col-sm-12 col-md-4 col-lg-5 status-log save-log"></div>
					  </div>
					</div>
				  </div>
				  
				  <div class="form-group">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label"><?php echo $text_tegi; ?></label>
					<div class="col-sm-9 col-md-9 col-lg-10 spravka_tegi"><div class="col-sm-12"><?php echo $spravka_tegi; ?></div></div>
				  </div>
				  
			</div>

			<div class="tab-pane" id="tab-template">
			
				<div class="well">
					<div class="row">
						<div class="col-xs-6 col-sm-8 status-log"></div>
						<div class="col-xs-6 col-sm-4 text-right"><a onclick="newtemplate();" class="btn btn-primary"><i class="fa fa-plus"></i><span class="text-button"><?php echo $text_new_template; ?></span></a></div>
					</div>
				</div>
				
				<div class="table-responsive">
				<table id="templates" class="table table-bordered table-hover">
				<thead>
					<tr>
						<td class="text-center"><?php echo $column_template_name; ?></td>
						<td class="text-center td-button"><?php echo $column_action; ?></td>
					</tr>
				</thead>
				<tbody>
				<?php if ($templates) { ?>
					<?php foreach ($templates as $template) { ?>
					<tr id="template_<?php echo $template['template_id']; ?>">
						<td class="text-left"><?php echo $template['name']; ?></td>
						<td class="text-right">
							<a onclick="viewtemplate('<?php echo $template['template_id']; ?>');" class="btn btn-primary button-view"><i class="fa fa-search"></i><span class="text-button"><?php echo $text_view; ?></span></a>
							<a onclick="deltemplate('<?php echo $template['template_id']; ?>');" class="btn btn-danger button-delete"><i class="fa fa-trash-o"></i><span class="text-button"><?php echo $text_delete; ?></span></a>
						</td>
					</tr>
					<?php } ?>
					<tr>
					  <td class="text-right" colspan="2"><a onclick="newtemplate();" class="btn btn-primary"><i class="fa fa-plus"></i><span class="text-button"><?php echo $text_new_template; ?></span></a></td>
					</tr>
				<?php } else { ?>
					<tr class="noresults">
					  <td class="text-center" colspan="2"><?php echo $text_no_template; ?></td>
					</tr>
					<tr>
					  <td class="text-right" colspan="2"><a onclick="newtemplate();" class="btn btn-primary"><i class="fa fa-plus"></i><span class="text-button"><?php echo $text_new_template; ?></span></a></td>
					</tr>
				<?php } ?>
				</tbody>
				</table>
				</div>

				<div id="template">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="temp-name"><?php echo $column_template_name; ?></label>
					<div class="col-sm-10">
					  <input type="text" name="temp_name" placeholder="<?php echo $column_template_name; ?>" id="temp-name" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-message2"><?php echo $entry_message; ?></label>
					<div class="col-sm-10">
					  <textarea name="message2" placeholder="<?php echo $entry_message; ?>" id="input-message2" class="form-control"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 col-md-3 col-lg-2 control-label" for="input-message"><?php echo $text_save_template; ?></label>
					<div class="col-sm-9 col-md-9 col-lg-10">
					  <div class="col-xs-4 col-sm-3"><a class="btn btn-success button-save"><i class="fa fa-save"></i><span class="text-button"><?php echo $text_save; ?></span></a></div>
					  <div class="col-xs-8 col-sm-9 status-log"></div>
					</div>
				</div>
				</div>
			</div>

			<div class="tab-pane" id="tab-log">
			
				<div class="well">
					<div class="row">
						<div class="col-xs-6 col-sm-5 col-md-7 col-lg-8 status-log"></div>
						<div class="col-xs-6 col-sm-7 col-md-5 col-lg-4 text-right">
							<a id="button-updatelog" onclick="updatelog();" class="btn btn-success"><i class="fa fa-refresh"></i><span class="text-button"><?php echo $button_updatelog; ?></span></a>
							<a id="button-clearlog" onclick="clearlog();" class="btn btn-danger"><i class="fa fa-eraser"></i><span class="text-button"><?php echo $button_clearlog; ?></span></a>
						</div>
					</div>
				</div>

				<textarea wrap="off" id="logarea" rows="15" class="form-control" style="resize:vertical;"><?php echo $log; ?></textarea>
			</div>

			<div class="tab-pane" id="tab-setting">
				<div class="well">
					<div class="row">
						<div class="col-xs-6 col-sm-7 col-md-8 status-log"></div>
						<div class="col-xs-6 col-sm-5 col-md-4 text-right">
							<a id="button-savesetting" class="btn btn-success"><i class="fa fa-save"></i><span class="text-button"><?php echo $button_save; ?></span></a>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-5 col-md-4 control-label" for="input-currency"><?php echo $entry_product_currency; ?></label>
					<div class="col-sm-7 col-md-5">
						<select name="contacts_product_currency" id="input-currency" class="form-control">
						  <?php foreach ($currencies as $currency) { ?>
						  <?php if ($currency['code'] == $contacts_product_currency) { ?>
						  <option value="<?php echo $currency['code']; ?>" selected="selected"><?php echo $currency['title']; ?></option>
						  <?php } else { ?>
						  <option value="<?php echo $currency['code']; ?>"><?php echo $currency['title']; ?></option>
						  <?php } ?>
						  <?php } ?>
						</select>
					</div>
				</div>
				
				<div class="form-group required">
					<label class="col-sm-5 col-md-4 control-label" for="input-protocol"><span data-toggle="tooltip" title="<?php echo $help_protocol; ?>"><?php echo $entry_protocol; ?></span></label>
					<div class="col-sm-7 col-md-5">
						<select name="contacts_protocol" id="input-protocol" class="form-control">
						  <?php if ($contacts_protocol == 'mail') { ?>
						  <option value="mail" selected="selected"><?php echo $text_mail; ?></option>
						  <?php } else { ?>
						  <option value="mail"><?php echo $text_mail; ?></option>
						  <?php } ?>
						  <?php if ($contacts_protocol == 'smtp') { ?>
						  <option value="smtp" selected="selected"><?php echo $text_smtp; ?></option>
						  <?php } else { ?>
						  <option value="smtp"><?php echo $text_smtp; ?></option>
						  <?php } ?>
						</select>
					</div>
				</div>
				
				<div class="form-group required">
					<label class="col-sm-5 col-md-4 control-label" for="count-message"><?php echo $entry_count_message; ?></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="count-message" name="contacts_count_message" class="form-control" value="<?php echo $contacts_count_message; ?>" />
					</div>
				</div>
				
				<div class="form-group required">
					<label class="col-sm-5 col-md-4 control-label" for="sleep-time"><?php echo $entry_sleep_time; ?></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="sleep-time" name="contacts_sleep_time" class="form-control" value="<?php echo $contacts_sleep_time; ?>" />
					</div>
				</div>
				
				<div class="form-group required">
					<label class="col-sm-5 col-md-4 control-label" for="mail-from"><span data-toggle="tooltip" title="<?php echo $help_from; ?>"><?php echo $entry_from; ?></span></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="mail-from" name="contacts_from" class="form-control" value="<?php echo $contacts_from; ?>" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-5 col-md-4 control-label" for="mail-parameter"><span data-toggle="tooltip" title="<?php echo $help_parameter; ?>"><?php echo $entry_parameter; ?></span></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="mail-parameter" name="contacts_parameter" class="form-control" value="<?php echo $contacts_parameter; ?>" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-5 col-md-4 control-label" for="smtp-host"><?php echo $entry_smtp_hostname; ?></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="smtp-host" name="contacts_smtp_hostname" class="form-control" value="<?php echo $contacts_smtp_hostname; ?>" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-5 col-md-4 control-label" for="smtp-username"><?php echo $entry_smtp_username; ?></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="smtp-username" name="contacts_smtp_username" class="form-control" value="<?php echo $contacts_smtp_username; ?>" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-5 col-md-4 control-label" for="smtp-password"><?php echo $entry_smtp_password; ?></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="smtp-password" name="contacts_smtp_password" class="form-control" value="<?php echo $contacts_smtp_password; ?>" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-5 col-md-4 control-label" for="smtp-port"><?php echo $entry_smtp_port; ?></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="smtp-port" name="contacts_smtp_port" class="form-control" value="<?php echo $contacts_smtp_port; ?>" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-5 col-md-4 control-label" for="smtp-timeout"><?php echo $entry_smtp_timeout; ?></label>
					<div class="col-sm-7 col-md-5">
						<input type="text" id="smtp-timeout" name="contacts_smtp_timeout" class="form-control" value="<?php echo $contacts_smtp_timeout; ?>" />
					</div>
				</div>

			</div>

		</div>
	
	</form>
	</div>
    
	</div>
  </div>
<script type="text/javascript"><!--
$('#input-message').summernote({height: 300});$('#input-message2').summernote({height: 300});$(document).ready(function() {$('#template').fadeOut('slow');$('#tab-send').animate({'opacity': '1'}, 'slow');});
$(".products-checkbox").on("click",function(){$(this).prop("checked")?($(this).val("1"),$(this).parent().parent().find(".products-block").show("slow").animate({opacity:"1"},"slow")):($(this).val("0"),$(this).parent().parent().find(".products-block").animate({opacity:"0"},"slow").hide("slow"))});
$("#entry_selected").on("click",function(){$(this).prop("checked")?($(this).val("1"),$(".selproducts-block").show("slow").animate({opacity:"1"},"slow")):($(this).val("0"),$(".selproducts-block").animate({opacity:"0"},"slow").hide("slow"))});
$("#insert_products").on("click",function(){$(this).prop("checked")?($(".products-body").show("slow"),$("#insert_products").val("1")):($(".products-body").hide("slow"),$("#insert_products").val("0"),$(".products-body input[type='checkbox']").val("0").attr("checked",!1),$(".products-block").animate({opacity:"0"},"slow").hide("slow"),$(".selproducts-block").animate({opacity:"0"},"slow").hide("slow"))});
$("#region").on("click",function(){$("#region").prop("checked")?($("#select-region").show("slow").animate({opacity:"1"},"slow"),$("input[name='set_region']").val("1")):($("#select-region").animate({opacity:"0"},"slow").hide("slow"),$("select[name='country_id']").val("0"),$("select[name='zone_id']").val("0"),$("input[name='set_region']").val("0"))});
$("#unsubscribe").on("click",function(){$("input[name='set_unsubscribe']").val($("#unsubscribe").prop("checked")?"1":"0")});
//--></script>
<script type="text/javascript"><!--
function updatelog(){$.ajax({url:"index.php?route=marketing/contacts/updatelog&token=<?php echo $token; ?>",dataType:"json",beforeSend:function(){$("#button-updatelog").button("loading")},complete:function(){$("#button-updatelog").button("reset")},success:function(a){$(".alert, .text-danger").remove();a.error&&($("#tab-log .status-log").append('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+a.error+"</div>"),$(".alert").fadeIn("slow"));a.success&&
($("#logarea").val(""),a.log&&$("#logarea").val(a.log),$("#tab-log .status-log").append('<div class="alert alert-success" style="display: none;"><i class="fa fa-check-circle"></i> '+a.success+"</div>"),$(".alert").fadeIn("slow"),setTimeout("$('.alert').fadeOut('slow');",3E3),setTimeout("$('.alert').remove();",4E3))}})};
function clearlog(){$.ajax({url:"index.php?route=marketing/contacts/clearlog&token=<?php echo $token; ?>",dataType:"json",beforeSend:function(){$("#button-clearlog").button("loading")},complete:function(){$("#button-clearlog").button("reset")},success:function(a){$(".alert, .text-danger").remove();a.error&&($("#tab-log .status-log").append('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+a.error+"</div>"),$(".alert").fadeIn("slow"));a.success&&($("#logarea").val(""),
$("#tab-log .status-log").append('<div class="alert alert-success" style="display: none;"><i class="fa fa-check-circle"></i> '+a.success+"</div>"),$(".alert").fadeIn("slow"),setTimeout("$('.alert').fadeOut('slow');",3E3),setTimeout("$('.alert').remove();",4E3))}})};
//--></script>
<script type="text/javascript"><!--
var message=$("#input-message"),message2=$("#input-message2");function newtemplate(){$("#temp-name").val("");message2.val("").code("");$("#template").fadeIn("slow");$("#template .button-save").attr("onclick","addnewtemplate();")}
function addnewtemplate(){var b=$("#temp-name").val();$("textarea[name='message2']").val($("#input-message2").code());$.ajax({url:"index.php?route=marketing/contacts/addnewtemplate&token=<?php echo $token; ?>",type:"post",data:$("#temp-name, #input-message2"),dataType:"json",beforeSend:function(){$("#template .button-save").button("loading")},complete:function(){$("#template .button-save").button("reset");message2.val("")},success:function(a){$(".alert, .text-danger").remove();a.error&&($("#tab-template .status-log").append('<div class="alert alert-danger alert-addnew" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+
a.error+"</div>"),$(".alert").fadeIn("slow"));a.success&&($("#template .button-save").attr("onclick",""),$(".noresults").remove(),$("#templates tbody").prepend('<tr class="newtemp" id="template_'+a.template_id+'"><td class="text-left">'+b+'</td><td class="text-right"><a onclick="viewtemplate('+a.template_id+');" class="btn btn-primary button-view" style="margin-right:3px;"><i class="fa fa-search"></i><span class="text-button"><?php echo $text_view; ?></span></a><a onclick="deltemplate('+a.template_id+
');" class="btn btn-danger button-delete"><i class="fa fa-trash-o"></i><span class="text-button"><?php echo $text_delete; ?></span></a></td></tr>'),$("#tab-template .status-log").append('<div class="alert alert-success alert-addnew" style="display: none;"><i class="fa fa-check-circle"></i> '+a.success+"</div>"),$(".alert").fadeIn("slow"),setTimeout("$('.alert-addnew').fadeOut('slow');",3E3),setTimeout("$('.alert-addnew').remove();",4E3))}})}
function addtemplate(){var b=$("#new-temp-name").val();$("textarea[name='message']").val($("#input-message").code());$.ajax({url:"index.php?route=marketing/contacts/addtemplate&token=<?php echo $token; ?>",type:"post",data:$("#new-temp-name, #input-message"),dataType:"json",beforeSend:function(){$("#savetempl").button("loading")},complete:function(){$("#savetempl").button("reset");message.val("")},success:function(a){$(".alert, .text-danger").remove();a.error&&($("#tab-send .save-log").append('<div class="alert alert-danger alert-addnew" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+
a.error+"</div>"),$(".alert").fadeIn("slow"));a.success&&($(".noresults").remove(),$("#templates tbody").prepend('<tr class="newtemp" id="template_'+a.template_id+'"><td class="text-left">'+b+'</td><td class="text-right"><a onclick="viewtemplate('+a.template_id+');" class="btn btn-primary button-view" style="margin-right:3px;"><i class="fa fa-search"></i><span class="text-button"><?php echo $text_view; ?></span></a><a onclick="deltemplate('+a.template_id+');" class="btn btn-danger button-delete"><i class="fa fa-trash-o"></i><span class="text-button"><?php echo $text_delete; ?></span></a></td></tr>'),
$("#tab-send .save-log").append('<div class="alert alert-success alert-addnew" style="display: none;"><i class="fa fa-check-circle"></i> '+a.success+"</div>"),$(".alert").fadeIn("slow"))}})}
function savetemplate(b){$("textarea[name='message2']").val($("#input-message2").code());var a=$("#temp-name").val();$.ajax({url:"index.php?route=marketing/contacts/edittemplate&template_id="+b+"&token=<?php echo $token; ?>",type:"post",data:$("#temp-name, #input-message2"),dataType:"json",beforeSend:function(){$("#template .button-save").button("loading")},complete:function(){$("#template .button-save").button("reset");message2.val("")},success:function(c){$(".alert, .text-danger").remove();c.error&&
($("#template .status-log").append('<div class="alert alert-danger alert-addnew" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+c.error+"</div>"),$(".alert").fadeIn("slow"));c.success&&($("#template_"+b+" .text-left").empty().append(a),$("#template .status-log").append('<div class="alert alert-success alert-addnew" style="display: none;"><i class="fa fa-check-circle"></i> '+c.success+"</div>"),$(".alert").fadeIn("slow"),setTimeout("$('.alert-addnew').fadeOut('slow');",3E3),setTimeout("$('.alert-addnew').remove();",4E3))}})}
function deltemplate(b){$.ajax({url:"index.php?route=marketing/contacts/deltemplate&template_id="+b+"&token=<?php echo $token; ?>",dataType:"json",beforeSend:function(){$("#temp-name").val("");message2.val("").code("");$("#template .button-save").attr("onclick","");$("#template").fadeOut("slow")},complete:function(){},success:function(a){$(".alert, .text-danger").remove();a.error&&($("#template .status-log").append('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+a.error+
"</div>"),$(".alert").fadeIn("slow"));a.success&&($("#templates #template_"+b).fadeOut("slow").remove(),$("#template .status-log").append('<div class="alert alert-success" style="display: none;"><i class="fa fa-check-circle"></i> '+a.success+"</div>"),$(".alert").fadeIn("slow"),setTimeout("$('.alert').fadeOut('slow');",3E3),setTimeout("$('.alert').remove();",4E3))}})}
function viewtemplate(b){var a=$("#templates #template_"+b+" .button-view");$.ajax({url:"index.php?route=marketing/contacts/gettemplate&template_id="+b+"&token=<?php echo $token; ?>",dataType:"json",beforeSend:function(){$(".alert, .text-danger").remove();a.button("loading");$("#template").fadeIn("slow");$("#temp-name").val("");message2.val("").code("")},complete:function(){a.button("reset")},success:function(a){$("#temp-name").val(a.name);a.message&&message2.code(a.message);$("#template .button-save").attr("onclick","savetemplate("+a.template_id+");")}})}
function loadtemplate(b){$.ajax({url:"index.php?route=marketing/contacts/gettemplate&template_id="+b+"&token=<?php echo $token; ?>",dataType:"json",beforeSend:function(){$(".alert, .text-danger").remove();$("#temp-load").append('<span class="wait" style="display:none;">Loading...</span>');$(".wait").fadeIn("slow");message.val("").code("")},complete:function(){$(".wait").fadeOut("slow",function(){$(".wait").remove()})},success:function(a){a.message&&message.code(a.message)}})};
//--></script>
<script type="text/javascript"><!--
$("#bta").on("click",function(){$.ajax({url:"index.php?route=marketing/contacts/getlic&token=<?php echo $token; ?>",dataType:"json",beforeSend:function(){$("#bta").button("loading")},complete:function(){$("#bta").button("reset")},success:function(a){$(".alert, .text-danger").remove();a.error&&($(".lic-active .text-lic").empty().append('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+a.error+"</div>"),$(".alert").fadeIn("slow"));a.success&&($(".lic-active .text-lic").empty().append('<div class="alert alert-success" style="display: none;"><i class="fa fa-check-circle"></i> '+
a.success+"</div>"),$(".alert").fadeIn("slow"),$("#bta").hide("slow").remove(),setTimeout("$('.alert').fadeOut('slow');",3E3))}})});
$("#button-savesetting").on("click",function(){$.ajax({url:"index.php?route=marketing/contacts/savesetting&token=<?php echo $token; ?>",type:"post",data:$("#tab-setting select, #tab-setting input"),dataType:"json",beforeSend:function(){$("#button-savesetting").button("loading")},complete:function(){$("#button-savesetting").button("reset")},success:function(a){$(".alert, .text-danger").remove();a.error&&($("#tab-setting .status-log").append('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+
a.error+"</div>"),$(".alert").fadeIn("slow"));a.success&&($("#tab-setting .status-log").append('<div class="alert alert-success" style="display: none;"><i class="fa fa-check-circle"></i> '+a.success+"</div>"),$(".alert").fadeIn("slow"),setTimeout("$('.alert').fadeOut('slow');",3E3))}})});
$("select[name='to']").on("change",function(){$("#tab-send .to").hide(300);$("#to-"+this.value.replace("_","-")).show("slow");"affiliate"==this.value||"affiliate_all"==this.value||"manual"==this.value?($(".form-unsubscribe").hide("slow"),$("input[name='set_unsubscribe']").attr("checked",!1).val("0")):$(".form-unsubscribe").show("slow");"affiliate"==this.value||"customer"==this.value||"manual"==this.value?($(".form-region").hide(300),$("#select-region").hide(300),$("input[name='set_region']").attr("checked",!1).val("0")):$(".form-region").show("slow")});$('select[name=\'to\']').trigger('change');
$("select[name='template_id']").on("change",function(){this.value>0?loadtemplate(this.value):message.val("").code("")});$('select[name=\'template_id\']').trigger('change');
$("select[name='country_id']").on("change",function(){0<this.value&&$.ajax({url:"index.php?route=marketing/contacts/country&token=<?php echo $token; ?>&country_id="+this.value,dataType:"json",beforeSend:function(){},complete:function(){},success:function(a){html='<option value=""><?php echo $text_select; ?></option>';if(a.zone&&""!=a.zone)for(i=0;i<a.zone.length;i++)html+='<option value="'+a.zone[i].zone_id+'"',"<?php echo $zone_id; ?>"==a.zone[i].zone_id&&(html+=' selected="selected"'),html+=">"+a.zone[i].name+
"</option>";else html+='<option value="0" selected="selected"><?php echo $text_none; ?></option>';$("select[name='zone_id']").html(html)},error:function(a,c,b){alert(b+"\r\n"+a.statusText+"\r\n"+a.responseText)}})});$('select[name=\'country_id\']').trigger('change');
//--></script>
<script type="text/javascript"><!--
$("input[name='customers']").autocomplete({source:function(a,b){$.ajax({url:"index.php?route=customer/customer/autocomplete&token=<?php echo $token; ?>&filter_name="+encodeURIComponent(a),dataType:"json",success:function(a){b($.map(a,function(a){return{label:a.name,value:a.customer_id}}))}})},select:function(a){$("input[name='customers']").val("");$("#customer"+a.value).remove();$("#block_customer").append('<div id="customer'+a.value+'"><i class="fa fa-minus-circle"></i> '+a.label+'<input type="hidden" name="customer[]" value="'+a.value+'" /></div>')}});$("#block_customer").delegate(".fa-minus-circle","click",function(){$(this).parent().remove()});
$("input[name='affiliates']").autocomplete({source:function(a,b){$.ajax({url:"index.php?route=marketing/affiliate/autocomplete&token=<?php echo $token; ?>&filter_name="+encodeURIComponent(a),dataType:"json",success:function(a){b($.map(a,function(a){return{label:a.name,value:a.affiliate_id}}))}})},select:function(a){$("input[name='affiliates']").val("");$("#affiliate"+a.value).remove();$("#block_affiliate").append('<div id="affiliate'+a.value+'"><i class="fa fa-minus-circle"></i> '+a.label+'<input type="hidden" name="affiliate[]" value="'+a.value+'" /></div>')}});$("#block_affiliate").delegate(".fa-minus-circle","click",function(){$(this).parent().remove()});
$("input[name='products']").autocomplete({source:function(a,b){$.ajax({url:"index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name="+encodeURIComponent(a),dataType:"json",success:function(a){b($.map(a,function(a){return{label:a.name,value:a.product_id}}))}})},select:function(a){$("input[name='products']").val("");$("#product"+a.value).remove();$("#block_product").append('<div id="product'+a.value+'"><i class="fa fa-minus-circle"></i> '+a.label+'<input type="hidden" name="product[]" value="'+a.value+'" /></div>')}});$("#block_product").delegate(".fa-minus-circle","click",function(){$(this).parent().remove()});
$("input[name='sproducts']").autocomplete({source:function(a,b){$.ajax({url:"index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name="+encodeURIComponent(a),dataType:"json",success:function(a){b($.map(a,function(a){return{label:a.name,value:a.product_id}}))}})},select:function(a){$("input[name='sproducts']").val("");$("#selproduct"+a.value).remove();$("#block_selproduct").append('<div id="selproduct'+a.value+'"><i class="fa fa-minus-circle"></i> '+a.label+'<input type="hidden" name="selproducts[]" value="'+a.value+'" /></div>')}});$("#block_selproduct").delegate(".fa-minus-circle","click",function(){$(this).parent().remove()});
//--></script>
<script type="text/javascript"><!--
function send(b){$("textarea[name='message']").val($("#input-message").code());$.ajax({url:b,type:"post",data:$("#tab-send select, #tab-send input, #tab-send textarea"),dataType:"json",beforeSend:function(){$("#button-send").button("loading");$("#content > .container-fluid").prepend('<div class="alert alert-warning" style="display: none;"><i class="fa fa-exclamation-circle"></i><?php echo $text_wait; ?></div>');$(".alert").fadeIn("slow")},complete:function(){$("#button-send").button("reset");$(".alert.alert-warning").remove()},
success:function(a){$(".alert, .text-danger").remove();a.error&&(a.error.warning&&($("#content > .container-fluid").prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+a.error.warning+"</div>"),$("html, body").animate({scrollTop:0},"slow"),$(".alert").fadeIn("slow")),a.error.subject&&($("#content > .container-fluid").prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+a.error.subject+"</div>"),
$("html, body").animate({scrollTop:0},"slow"),$(".alert").fadeIn("slow"),$("input[name='subject']").after('<div class="text-danger">'+a.error.subject+"</div>")),a.error.message&&($("#content > .container-fluid").prepend('<div class="alert alert-danger" style="display: none;"><i class="fa fa-exclamation-circle"></i> '+a.error.message+"</div>"),$("html, body").animate({scrollTop:0},"slow"),$(".alert").fadeIn("slow"),$("textarea[name='message']").parent().append('<div class="text-danger">'+a.error.message+
"</div>")));a.next?a.success&&($("#content > .container-fluid").prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  '+a.success+"</div>"),$("html, body").animate({scrollTop:0},"slow"),send(a.next)):a.success&&($("#content > .container-fluid").prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i>  '+a.success+"</div>"),$("html, body").animate({scrollTop:0},"slow"))}})};
window.onbeforeunload = function(evt) {evt = evt || window.event;evt.returnValue = '<?php echo $error_close; ?>';}
//--></script></div>
<?php echo $footer; ?>