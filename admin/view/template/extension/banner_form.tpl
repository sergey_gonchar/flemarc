<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
   <div class="page-header">
      <div class="container-fluid">
         <div class="pull-right">
            <button type="submit" form="form-banner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
            <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
         </div>
         <h1><?php echo $heading_title; ?></h1>
         <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
         </ul>
      </div>
   </div>
   <div class="container-fluid">
      <?php if ($error_warning) { ?>
      <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
         <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
      <?php } ?>
      <div class="panel panel-default">
         <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
         </div>
         <div class="panel-body">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-banner" class="form-horizontal">
               <div class="form-group required">
                  <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
                  <div class="col-sm-10">
                     <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
                     <?php if ($error_name) { ?>
                     <div class="text-danger"><?php echo $error_name; ?></div>
                     <?php } ?>
                  </div>
               </div>
               <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                  <div class="col-sm-10">
                     <select name="status" id="input-status" class="form-control">
                        <?php if ($status) { ?>
                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                        <option value="0"><?php echo $text_disabled; ?></option>
                        <?php } else { ?>
                        <option value="1"><?php echo $text_enabled; ?></option>
                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                        <?php } ?>
                     </select>
                  </div>
               </div>
               <hr>
               <div class="row">
                  <div class="col-sm-2">
                     <ul class="nav nav-pills nav-stacked" id="section">
                        <?php $section_row = 0; ?>
                        <?php foreach ($banner_images as $banner_image) { ?> 		 
                        <li class=""><a href="#tab-section-<?php echo $section_row; ?>" data-toggle="tab" aria-expanded="false"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-section-<?php echo $section_row; ?>\']').parent().remove(); $('#tab-section-<?php echo $section_row; ?>').remove(); $('#section a:first').tab('show');"></i> Слайдер<?php echo $section_row + 1; ?></a></li>
                        <?php $section_row++; ?>
                        <?php } ?>
                        <li id="section-add" style="cursor:pointer"><a onclick="addSection();"><i class="fa fa-plus-circle"></i> Добавить слайдер</a></li>
                     </ul>
                  </div>
                  <div class="col-sm-10">
                     <div class="tab-content first">
                        <?php $section_row = 0; ?>
                        <?php foreach ($banner_images as $banner_image) { ?>
                        <div class="tab-pane" id="tab-section-<?php echo $section_row; ?>">
                           <div class="tab-content">
                              <ul class="nav nav-tabs" id="language<?php echo $section_row; ?>">
                                 <?php foreach ($languages as $language) { ?> 
                                 <li class=""><a href="#tab-section-<?php echo $section_row; ?>-<?php echo $language['language_id']; ?>" data-toggle="tab" aria-expanded="false"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                 <?php } ?>
                              </ul>
                              <?php foreach ($languages as $language) { ?>
                              <div class="tab-pane" id="tab-section-<?php echo $section_row; ?>-<?php echo $language['language_id']; ?>">
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Заголовок</label>
                                    <div class="col-sm-10"><input type="text" name="banner_image[<?php echo $section_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['title'] : ''; ?>" class="form-control"></div>
                                 </div>
                                 <div class="form-group">
                                    <label class="col-sm-2 control-label">Контент</label>
                                    <div class="col-sm-10"><textarea name="banner_image[<?php echo $section_row; ?>][banner_image_description][<?php echo $language['language_id']; ?>][description]" id="description-<?php echo $section_row; ?>-<?php echo $language['language_id']; ?>" class="form-control custom-control summernote" cols="3"><?php echo isset($banner_image['banner_image_description'][$language['language_id']]) ? $banner_image['banner_image_description'][$language['language_id']]['description'] : ''; ?></textarea></div>
                                 </div>
                              </div>
                              <?php } ?>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Ссылка</label>
                                 <div class="col-sm-10"><input type="text" name="banner_image[<?php echo $section_row; ?>][link]" value="<?php echo $banner_image['link']; ?>" placeholder="<?php echo $entry_link; ?>" class="form-control" /></div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Изображение слайда</label>
                                 <div class="col-sm-10">
                                    <a href="" id="thumb-image<?php echo $section_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $banner_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>
                                    <input type="hidden" name="banner_image[<?php echo $section_row; ?>][image]" value="<?php echo $banner_image['image']; ?>" id="input-image<?php echo $section_row; ?>" />
                                 </div>
                              </div>
                              <div class="form-group">
                                 <label class="col-sm-2 control-label">Порядок сортировки</label>
                                 <div class="col-sm-10"><input type="text" name="banner_image[<?php echo $section_row; ?>][sort_order]" value="<?php echo $banner_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></div>
                              </div>
                           </div>
                        </div>
                        <?php $section_row++; ?>
                        <?php } ?>		 
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <script><!--
             
			 var section_row = <?php echo $section_row; ?>;
         
             function addSection() {	         
             
			 html  = '<div class="tab-pane" id="tab-section-' + section_row + '">';
             html  += '                <div class="tab-content">';           
             html  += '                  <ul class="nav nav-tabs" id="language' + section_row + '">';
             <?php foreach ($languages as $language) { ?> 
             html  += '                     <li class=""><a href="#tab-section-' + section_row + '-<?php echo $language['language_id']; ?>" data-toggle="tab" aria-expanded="false"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>';
             <?php } ?>
             html  += '                  </ul>';			
             html  += '                  <div class="tab-content">';
             <?php foreach ($languages as $language) { ?>
             html  += '                    <div class="tab-pane" id="tab-section-' + section_row + '-<?php echo $language['language_id']; ?>">';
             html  += '                        <div class="form-group">';
             html  += '                          <label class="col-sm-2 control-label">Заголовок</label>';
             html  += '                          <div class="col-sm-10"><input type="text" name="banner_image[' + section_row + '][banner_image_description][<?php echo $language['language_id']; ?>][title]" class="form-control"></div>';
             html  += '                       </div>';
             html  += '                       <div class="form-group">';
             html  += '                          <label class="col-sm-2 control-label">Контент</label>';
             html  += '                          <div class="col-sm-10"><textarea name="banner_image[' + section_row + '][banner_image_description][<?php echo $language['language_id']; ?>][description]" id="description-' + section_row + '-<?php echo $language['language_id']; ?>" class="form-control custom-control summernote" cols="3"></textarea></div>';
             html  += '                       </div>';
             html  += '                     </div>';
             <?php } ?>
             html  += '                     <div class="form-group">';
             html  += '                       <label class="col-sm-2 control-label">Ссылка</label>';
             html  += '                       <div class="col-sm-10"><input type="text" name="banner_image[' + section_row + '][link]" value="" placeholder="<?php echo $entry_link; ?>" class="form-control" /></div>';
             html  += '                    </div>';
             html  += '                    <div class="form-group">';
             html  += '                       <label class="col-sm-2 control-label">Изображение слайда</label>';
             html  += '                       <div class="col-sm-10">';
             html  += '                          <a href="" id="thumb-image' + section_row + '" data-toggle="image" class="img-thumbnail"><img src="<?php echo $banner_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>';
             html  += '                          <input type="hidden" name="banner_image[' + section_row + '][image]" value="<?php echo $banner_image['image']; ?>" id="input-image' + section_row + '" />';
             html  += '                       </div>';
             html  += '                    </div>';
             html  += '                    <div class="form-group">';
             html  += '                       <label class="col-sm-2 control-label">Порядок сортировки</label>';
             html  += '                      <div class="col-sm-10"><input type="text" name="banner_image[' + section_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></div>';
             html  += '                    </div>';
             html  += '                 </div>';
             html  += '              </div>';
             html  += '           </div>  ';     
         
         	$('.tab-content.first').append(html);
         
            $('#section-add').before('<li><a href="#tab-section-' + section_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$(\'a[href=\\\'#tab-section-' + section_row + '\\\']\').parent().remove(); $(\'#tab-section-' + section_row + '\').remove(); $(\'#section a:first\').tab(\'show\');"></i> Редактирование слайда</a></li>');
           
            $('#section a[href=\'#tab-section-' + section_row + '\']').tab('show');
         
            $('#language' + section_row + ' li:first-child a').tab('show');
         	
         	$('#description-' + section_row + '-1' ).summernote({ 
         	 height: 200,
         	});
         	$('#description-' + section_row + '-2' ).summernote({ 
         	 height: 200,
         	});
			$('#description-' + section_row + '-3' ).summernote({ 
         	 height: 200,
         	});
         		
         	section_row++;
         }
         
           $( '#language1 li:first-child a' ).tab( 'show' );
           $( '#language2 li:first-child a' ).tab( 'show' );
           $( '#language3 li:first-child a' ).tab( 'show' );
         //-->
   </script>
   <script type="text/javascript">
      $('.summernote').summernote({
      	height: 200
      });
      $('#section li:first-child a').tab('show');
      $('#language0 li:first-child a').tab('show');
      $('#language1 li:first-child a').tab('show');
      $('#language2 li:first-child a').tab('show');
      $('#language3 li:first-child a').tab('show');
      $('#language4 li:first-child a').tab('show');
   </script>
</div>
<?php echo $footer; ?>