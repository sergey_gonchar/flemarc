<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bars"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
	 <div class="form-group"> 

	 <input id="search" class="form-control" type="text" placeholder="<?php echo $text_search; ?>"></input></div>
        <form method="post" enctype="multipart/form-data" id="form-callme" class="form-horizontal">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
			  <thead>
				<tr class="guide"> 
				  <td width="2%" class="text-left">№</td>
				  <td width="20%" class="text-left"><?php echo $text_name; ?></td>
				  <td width="10%" class="text-left"><?php echo $text_phone; ?></td>
				  <td width="20%" class="text-left"><?php echo $text_time; ?></td>
				  <td width="30%" class="text-left">Комментарий менеджера</td>
				  <td width="30%" class="text-left"><?php echo $text_comment; ?></td>				  
				  <td width="10%" class="text-left"><?php echo $text_date; ?></td>
				  <td width="10%" style="text-align: center;"><?php echo $text_store; ?></td>
				  <td width="2%" class="text-right"><?php echo $text_lang; ?></td>
				  <td width="5%" class="text-right"><?php echo $text_del; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($all_callmes) { ?>
				  <?php foreach($all_callmes as $getViewCallmes) { ?>
					<tr>
					<td><?php echo $getViewCallmes['callme_id']; ?></td>
					<td><?php echo $getViewCallmes['customer_name']; ?></td>
					<td><?php echo $getViewCallmes['customer_phone']; ?></td>					
					<td><?php echo $getViewCallmes['time']; ?></td>
					<td><textarea name="comm" placeholder="Комментарий" id="manager-comment<?php echo $getViewCallmes['callme_id']; ?>" onblur="posting(<?php echo $getViewCallmes['callme_id']; ?>)" class="form-control"><?php echo $getViewCallmes['manager_comment']; ?></textarea></td>
					<td><?php echo $getViewCallmes['customer_comment']; ?></td>
					<td><?php echo $getViewCallmes['date_created']; ?></td>
					<td><?php echo $getViewCallmes['store_id']; ?></td>
					<td><?php echo $getViewCallmes['language_id']; ?></td>
					<td><button type="button" data-toggle="tooltip" title="" class="btn btn-danger" onclick="deleteCallme('<?php echo $getViewCallmes['callme_id']; ?>')"><i class="fa fa-trash-o"></i></button></td>
					</tr>     
                  <?php } ?>
				<?php } else { ?>
				  <tr>
					<td colspan="10" class="text-center"><?php echo $text_no_results; ?></td>
				  </tr>
				<?php } ?>
			  </tbody>
			</table>
          </div>
        </form>
		<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

$(function() {
    $("input#search").on("input", function() {
        var text = this.value.toLowerCase();
        $("tr:not(.guide) td").removeClass("hot").each(function(i, obj) {
            if ($(obj).text().toLowerCase().indexOf(text) > -1 && text) $(obj).addClass("hot")
        });
        $("tr:not(.guide)").show().filter(function() {
            return text && !$(".hot", this).length
        }).hide()
    })
});
</script>
<script>
	function deleteCallme(callmeID) {      
				var r=confirm("<?php echo $text_warning; ?>");
				if (r==true) {
					$.ajax({
						url: 'index.php?route=extension/callme/deletecallme&token=<?php echo $token; ?>',
						type: 'post',
						data: {'callme_id': callmeID},
						success: function(response) {
						location.reload();
					}
				});
			 }
			}
	function posting(callmeID) { 
                    var com = $('#manager-comment'+callmeID).val();					
					$.ajax({
						url: 'index.php?route=extension/callme/posting&token=<?php echo $token; ?>',
						type: 'post',						
						data: ({ 'manager_comment': com,'callme_id': callmeID }),
				});
			}		
	
</script> 
<?php echo $footer; ?>