<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bars"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
	 <div class="form-group"> 

	 <input id="search" class="form-control" type="text" placeholder="<?php echo $text_search; ?>"></input></div>
        <form method="post" enctype="multipart/form-data" id="form-news" class="form-horizontal">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover">
			  <thead>
				<tr class="guide"> 
				  <td width="2%" class="text-left">№</td>
				  <td width="30%" class="text-left"><?php echo $text_email; ?></td>
				  <td width="30%" class="text-left"><?php echo $text_name; ?></td>
				  <td width="10%" class="text-left"><?php echo $text_date; ?></td>
				  <td width="10%" style="text-align: center;"><?php echo $text_store; ?></td>
				  <td width="5%" class="text-right"><?php echo $text_lang; ?></td>
				  <td width="5%" class="text-right"><?php echo $text_del; ?></td>
				</tr>
			  </thead>
			  <tbody>
				<?php if ($all_viewsubscribers) { ?>
				  <?php foreach($all_viewsubscribers as $getViewSubscribers) { ?>
					<tr>
					<td><?php echo $getViewSubscribers['subscribe_id']; ?></td>
					<td><?php echo $getViewSubscribers['customer_email']; ?></td>
					<td><?php echo $getViewSubscribers['customer_name']; ?></td>
					<td><?php echo $getViewSubscribers['date_created']; ?></td>
					<td><?php echo $getViewSubscribers['store_id']; ?></td>
					<td><?php echo $getViewSubscribers['language_id']; ?></td>
					<td><button type="button" data-toggle="tooltip" title="" class="btn btn-danger" onclick="deleteSubscriber('<?php echo $getViewSubscribers['subscribe_id']; ?>')"><i class="fa fa-trash-o"></i></button></td>
					</tr>     
                  <?php } ?>
				<?php } else { ?>
				  <tr>
					<td colspan="7" class="text-center"><?php echo $text_no_results; ?></td>
				  </tr>
				<?php } ?>
			  </tbody>
			</table>
          </div>
        </form>
		<div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

$(function() {
    $("input#search").on("input", function() {
        var text = this.value.toLowerCase();
        $("tr:not(.guide) td").removeClass("hot").each(function(i, obj) {
            if ($(obj).text().toLowerCase().indexOf(text) > -1 && text) $(obj).addClass("hot")
        });
        $("tr:not(.guide)").show().filter(function() {
            return text && !$(".hot", this).length
        }).hide()
    })
});
</script>
<script>
	function deleteSubscriber(subscribeID) {      
				var r=confirm("<?php echo $text_warning; ?>");
				if (r==true) {
					$.ajax({
						url: 'index.php?route=extension/subscription/deletesubscriber&token=<?php echo $token; ?>',
						type: 'post',
						data: {'subscribe_id': subscribeID},
						success: function(response) {
						location.reload();
					}
				});
			 }
			}	
	
</script> 
<?php echo $footer; ?>