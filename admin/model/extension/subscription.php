<?php 
class ModelExtensionSubscription extends Model {
	
	public function getViewSubscribers($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "subscription ORDER BY date_created DESC";
		
		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
 
		return $query->rows;
	}
	

    public function getTotalSubscriptions() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "subscription");
	
		return $query->row['total'];
	}
}
?>