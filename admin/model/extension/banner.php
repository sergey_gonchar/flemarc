<?php
class ModelExtensionBanner extends Model {
	public function install() {
		$sql = " SHOW TABLES LIKE '".DB_PREFIX."slideshowmag'";
		$query = $this->db->query( $sql );
		if( count($query->rows) >0 )
			return true;
		$sql = array();

		$sql[] = "CREATE TABLE IF NOT EXISTS `".DB_PREFIX."slideshowmag` (
				  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(64) DEFAULT NULL,
				  `status` tinyint(4) DEFAULT NULL,
				  PRIMARY KEY (`banner_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;";
				
		$sql[] = "CREATE TABLE `".DB_PREFIX."slideshowmag_image` (
				  `banner_image_id` int(11) NOT NULL AUTO_INCREMENT,
				  `banner_id` int(11) NOT NULL,
				  `link` varchar(255) DEFAULT NULL,
				  `image` varchar(255) DEFAULT NULL,
				  `sort_order` int(3) NOT NULL DEFAULT '0',
				  PRIMARY KEY (`banner_image_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;";
				
		$sql[] = "CREATE TABLE `".DB_PREFIX."slideshowmag_image_description` (
				  `banner_image_id` int(11) NOT NULL,
				  `language_id` int(11) NOT NULL,
				  `banner_id` int(11) DEFAULT NULL,
				  `title` varchar(100) DEFAULT NULL,
				  `description` text(0) DEFAULT NULL,
				  PRIMARY KEY (`banner_image_id`,`language_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;";

		foreach( $sql as $q ){
			$query = $this->db->query( $q );
		}

	}
	public function uninstall() {		
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "slideshowmag`");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "slideshowmag_image`");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "slideshowmag_image_description`");
	}
				
	public function addBanner($data) {
		$this->event->trigger('pre.admin.banner.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "slideshowmag SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "'");

		$banner_id = $this->db->getLastId();

		if (isset($data['banner_image'])) {
			foreach ($data['banner_image'] as $banner_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "slideshowmag_image SET banner_id = '" . (int)$banner_id . "', link = '" .  $this->db->escape($banner_image['link']) . "', image = '" .  $this->db->escape($banner_image['image']) . "', sort_order = '" . (int)$banner_image['sort_order'] . "'");

				$banner_image_id = $this->db->getLastId();

				foreach ($banner_image['banner_image_description'] as $language_id => $banner_image_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "slideshowmag_image_description SET banner_image_id = '" . (int)$banner_image_id . "', language_id = '" . (int)$language_id . "', banner_id = '" . (int)$banner_id . "', description = '" .  $this->db->escape($banner_image_description['description']) . "', title = '" .  $this->db->escape($banner_image_description['title']) . "'");
				}
			}
		}

		$this->event->trigger('post.admin.banner.add', $banner_id);

		return $banner_id;
	}

	public function editBanner($banner_id, $data) {
		$this->event->trigger('pre.admin.banner.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "slideshowmag SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "' WHERE banner_id = '" . (int)$banner_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "slideshowmag_image WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "slideshowmag_image_description WHERE banner_id = '" . (int)$banner_id . "'");

		if (isset($data['banner_image'])) {
			foreach ($data['banner_image'] as $banner_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "slideshowmag_image SET banner_id = '" . (int)$banner_id . "', link = '" .  $this->db->escape($banner_image['link']) . "', image = '" .  $this->db->escape($banner_image['image']) . "', sort_order = '" . (int)$banner_image['sort_order'] . "'");

				$banner_image_id = $this->db->getLastId();

				foreach ($banner_image['banner_image_description'] as $language_id => $banner_image_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "slideshowmag_image_description SET banner_image_id = '" . (int)$banner_image_id . "', language_id = '" . (int)$language_id . "', banner_id = '" . (int)$banner_id . "', description = '" .  $this->db->escape($banner_image_description['description']) . "', title = '" .  $this->db->escape($banner_image_description['title']) . "'");
				}
			}
		}

		$this->event->trigger('post.admin.banner.edit', $banner_id);
	}

	public function deleteBanner($banner_id) {
		$this->event->trigger('pre.admin.banner.delete', $banner_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "slideshowmag WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "slideshowmag_image WHERE banner_id = '" . (int)$banner_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "slideshowmag_image_description WHERE banner_id = '" . (int)$banner_id . "'");

		$this->event->trigger('post.admin.banner.delete', $banner_id);
	}

	public function getBanner($banner_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "slideshowmag WHERE banner_id = '" . (int)$banner_id . "'");

		return $query->row;
	}

	public function getBanners($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "slideshowmag";

		$sort_data = array(
			'name',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getBannerImages($banner_id) {
		$banner_image_data = array();

		$banner_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "slideshowmag_image WHERE banner_id = '" . (int)$banner_id . "' ORDER BY sort_order ASC");

		foreach ($banner_image_query->rows as $banner_image) {
			$banner_image_description_data = array();

			$banner_image_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "slideshowmag_image_description WHERE banner_image_id = '" . (int)$banner_image['banner_image_id'] . "' AND banner_id = '" . (int)$banner_id . "'");

			foreach ($banner_image_description_query->rows as $banner_image_description) {
				$banner_image_description_data[$banner_image_description['language_id']] = array('title' => $banner_image_description['title'], 'description' => $banner_image_description['description']);
			}

			$banner_image_data[] = array(
				'banner_image_description' => $banner_image_description_data,
				'link'                     => $banner_image['link'],
				'image'                    => $banner_image['image'],
				'sort_order'               => $banner_image['sort_order']
			);
		}

		return $banner_image_data;
	}

	public function getTotalBanners() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "slideshowmag");

		return $query->row['total'];
	}
}