<?php 
class ModelExtensionCallme extends Model {
	
	public function getViewCallmes($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "callme ORDER BY date_created DESC";
		
		if (isset($data['start']) && isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	
		
		$query = $this->db->query($sql);
 
		return $query->rows;
	}
	

    public function getTotalCallmes() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "callme");
	
		return $query->row['total'];
	}
}
?>