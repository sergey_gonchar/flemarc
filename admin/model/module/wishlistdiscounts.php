<?php
class ModelModuleWishlistDiscounts extends Model {
	
	public function getCustomersWithWishList($data=array(), $store_id) {
		
		if($data){
			if($data['sort'] == 'name') {
				$data['sort'] = 'c.firstname ' . $data['order'] . ', c.lastname';
			}
			$orderClause = "ORDER BY " . $data['sort'] . " ". $data['order'] . " LIMIT " . $data['start'] . ", " . $data['limit'];
		} else {
			$orderClause = '';
		}
		$sql = "SELECT  *, c.date_added as date_added, c.customer_id AS customer_id 
				FROM " . DB_PREFIX . "customer AS c
				LEFT JOIN " . DB_PREFIX . "wishlistdiscounts AS wd ON c.customer_id=wd.customer_id
				WHERE c.wishlist<>'' 
				AND c.wishlist<>'a:0:{}' 
				AND c.wishlist IS NOT NULL 
				AND c.store_id ='" . $store_id . "'
				AND (c.wishlist<>wd.wishlist OR wd.wishlist IS NULL)" . $orderClause;
		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function gethWishListArchive($data=array(), $store_id) {
		
		if($data){
			if($data['sort'] == 'name') {
				$data['sort'] = 'c.firstname ' . $data['order'] . ', c.lastname';
			}
			$orderClause = "ORDER BY " . $data['sort'] . " ". $data['order'] . " LIMIT " . $data['start'] . ", " . $data['limit'];
		} else {
			$orderClause = '';
		}
		$sql = "SELECT  *, c.date_added as date_added, c.customer_id AS customer_id 
				FROM " . DB_PREFIX . "customer AS c
				LEFT JOIN " . DB_PREFIX . "wishlistdiscounts AS wd ON c.customer_id=wd.customer_id
				WHERE c.wishlist<>'' 
				AND c.wishlist<>'a:0:{}' 
				AND c.wishlist IS NOT NULL 
				AND c.store_id ='" . $store_id . "'
				AND (c.wishlist=wd.wishlist)" . $orderClause;
		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getTotalCustomersWithWishList($store_id) {
		$query = $this->db->query("	SELECT count(*) as count 
									FROM " . DB_PREFIX . "customer AS c
									LEFT JOIN " . DB_PREFIX . "wishlistdiscounts AS wd ON c.customer_id=wd.customer_id
									WHERE c.wishlist<>'' 
									AND c.wishlist<>'a:0:{}' 
									AND c.wishlist IS NOT NULL 
									AND c.store_id ='" . $store_id . "'
									AND (c.wishlist<>wd.wishlist OR wd.wishlist IS NULL)");
		return $query->row['count'];
	}
	
	public function getTotalWishListArchive($store_id) {
		$query = $this->db->query("	SELECT count(*) as count 
									FROM " . DB_PREFIX . "customer AS c
									LEFT JOIN " . DB_PREFIX . "wishlistdiscounts AS wd ON c.customer_id=wd.customer_id
									WHERE c.wishlist<>'' 
									AND c.wishlist<>'a:0:{}' 
									AND c.wishlist IS NOT NULL 
									AND c.store_id ='" . $store_id . "'
									AND (c.wishlist=wd.wishlist)");
		return $query->row['count'];
	}

	public function getCustomerWishlist($customer_id, $store_id) {
		$query = $this->db->query("SELECT wishlist FROM " . DB_PREFIX . "customer WHERE customer_id='".(int)$customer_id . "' AND store_id ='" . $store_id . "'")->row; 
		$wishlist =  unserialize($query['wishlist']);
		$implode     = array();
		$whereClause = '';
		if (isset($wishlist)) {
			foreach ($wishlist as $product_id) {
					$implode[] = "p.product_id = '" . $product_id . "'";
			} 

			$whereClause = "WHERE (" . implode(" OR ", $implode) . ")";
			$query =  $this->db->query("SELECT *, ss.name AS stock_status, pd.name AS product_name, p.price AS regular_price, ps.price AS special_price, p.product_id AS wishlist_product_id
										FROM " . DB_PREFIX . "product AS p 
										JOIN " . DB_PREFIX . "product_description  AS pd ON p.product_id=pd.product_id 
										LEFT JOIN " . DB_PREFIX . "stock_status AS ss ON p.stock_status_id=ss.stock_status_id 
										LEFT JOIN " . DB_PREFIX . "product_special AS ps ON p.product_id=ps.product_id 
										LEFT JOIN " . DB_PREFIX . "product_to_store AS pts ON p.product_id=pts.product_id
										 ". $whereClause . " AND pd.language_id='" . $this->config->get('config_language_id') . "' AND pts.store_id ='" . $store_id . "' GROUP BY p.product_id " );
			return $query->rows;
		} 
		return array();
	}

	public function getCustomers($customers, $store_id) {
		if(!empty($customers)) {  
			$whereClause = "WHERE customer_id IN ( $customers ) AND store_id ='" . $store_id . "'";  
			$query =  $this->db->query("SELECT * FROM " . DB_PREFIX . "customer " . $whereClause);
			return $query->rows;  
		} else {
			return array();
		}
	}

	public function isUniqueCode($randomCode) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "coupon` WHERE code='" . $this->db->escape($randomCode) . "'");
		if ($query->num_rows == 0) {
			return true;
		} else {
			return false;
		}			
 	}
	
	public function getGivenCoupons($data=array(), $store_id) {
		$givenCoupons = $this->db->query("SELECT *
											FROM " . DB_PREFIX . "coupon as c
											JOIN `" . DB_PREFIX . "wishlistdiscounts` as wd ON wd.coupon_id = c.coupon_id
											WHERE c.name LIKE  '%WishlistDiscount [%'											
											 AND wd.store_id ='" . (int)$store_id . "'
											ORDER BY c." . $data['sort'] . " ". $data['order'] . " 
											LIMIT " . $data['start'].", " . $data['limit'] );										 
		return $givenCoupons->rows;
	}

	public function getTotalGivenCoupons($store_id) {
		$givenCoupons = $this->db->query("SELECT COUNT(*) as count FROM " . DB_PREFIX . "coupon as c
										  JOIN `" . DB_PREFIX . "wishlistdiscounts` as wd ON wd.coupon_id = c.coupon_id
										   AND wd.store_id ='" . $store_id . "'
										  WHERE c.name LIKE '%WishlistDiscount [%'"); 
		return $givenCoupons->row['count'];
	}
	
	public function getUsedCoupons($data=array(), $store_id ) {
		$usedCoupons = $this->db->query("SELECT *
		 								  FROM `" . DB_PREFIX . "coupon` AS c
										  JOIN `" . DB_PREFIX . "coupon_history` AS ch ON c.coupon_id=ch.coupon_id
   										  JOIN `" . DB_PREFIX . "wishlistdiscounts` as wd ON wd.coupon_id = c.coupon_id
										  WHERE name LIKE  '%WishlistDiscount [%'
										   AND wd.store_id ='" . $store_id . "'
										  ORDER BY " . $data['sort'] . " ". $data['order'] . " 
										  LIMIT " . $data['start'].", " . $data['limit'] );
		return $usedCoupons->rows;
	}
	
	public function getTotalUsedCoupons($store_id ) {
		$givenCoupons = $this->db->query("SELECT COUNT(*) as count FROM `" . DB_PREFIX . "coupon` as c
											JOIN `" . DB_PREFIX . "coupon_history` AS ch ON c.coupon_id=ch.coupon_id
											JOIN `" . DB_PREFIX . "wishlistdiscounts` AS wd ON c.coupon_id=wd.coupon_id
											WHERE name LIKE  '%WishlistDiscount [%'  
											AND wd.store_id ='" . $store_id . "'"); 
		return $givenCoupons->row['count'];
	}

	public function logDiscount($data, $store_id) {
		
		if($this->db->query("SELECT * FROM " . DB_PREFIX . "wishlistdiscounts WHERE customer_id = '" .  (int)$data['customer_id']  . "'")->num_rows > 0){
			$this->db->query("UPDATE " . DB_PREFIX . "wishlistdiscounts 
								SET wishlist = '" . $this->db->escape($data['wishlist']) . "', 
								coupon_id = '" . (int)$data['coupon_id'] . "', 
								date_added = NOW()
								WHERE customer_id='" . (int)$data['customer_id'] . "' AND store_id ='" . $store_id . "'"); 
		} else {
			$this->db->query("INSERT INTO " . DB_PREFIX . "wishlistdiscounts 
								SET 
								customer_id = '" . (int)$data['customer_id'] . "',	
								coupon_id ='" . (int)$data['coupon_id'] .  "', 
								date_added = NOW(),
								store_id = '". $store_id. "',
								wishlist='" . $this->db->escape($data['wishlist']) . "'");
		}
	}

	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "wishlistdiscounts (
							wishlist_discount_id INT(11) NOT NULL AUTO_INCREMENT,
							customer_id INT(11) NOT NULL DEFAULT 0, 
							coupon_id INT(11) NULL DEFAULT NULL, 
							date_added DATE NULL DEFAULT NULL,
							wishlist TEXT NULL DEFAULT NULL,
							store_id INT(11) NOT NULL,
							PRIMARY KEY (wishlist_discount_id))");
	}

	public function uninstall() {
		$this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "wishlistdiscounts");
	}

	public function moveWishlistToCart($customer_id, $store_id) {
		$query = $this->db->query("SELECT wishlist, cart FROM "  . DB_PREFIX  . "customer WHERE customer_id='" . (int)$customer_id."' AND store_id ='" . $store_id . "'");
		$wishlist = $query->row['wishlist'];
		$cart = $query->row['cart'];
		$query = $this->db->query("UPDATE "  . DB_PREFIX  . "customer SET cart='"  . $wishlist . "' WHERE customer_id='" . (int)$customer_id."' AND store_id ='" . $store_id . "'");	
	}
}
?>