<?php 
class ModelModuleErrorlogManager extends Model {
    public function install() {
        $res = $this->db->query("SHOW TABLES LIKE '%errorlog_manager%'");
        if (!$res->num_rows) {
            $this->db->query("CREATE TABLE " . DB_PREFIX . "errorlog_manager (
                `id` INT NOT NULL AUTO_INCREMENT,
                PRIMARY KEY (`id`),
                `filename` VARCHAR(255) NOT NULL,
                `row_hash` VARCHAR(32) NOT NULL,
                `message_hash` VARCHAR(32) NOT NULL,
                `message` TEXT NOT NULL,
                `timestamp` varchar(32)
            )");
        }
    }

    public function uninstall() {
        $res = $this->db->query("SHOW TABLES LIKE '%errorlog_manager%'");
        if ($res->num_rows) {
            $this->db->query("DROP TABLE " . DB_PREFIX . "errorlog_manager");
        }
    }

    public function truncate($file) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "errorlog_manager WHERE `filename`='" . $this->db->escape($file) . "'");
    }

    public function get_last_error($file) {
        $db_last_error = $this->db->query("SELECT * FROM " . DB_PREFIX . "errorlog_manager WHERE filename='$file' ORDER BY id DESC LIMIT 1");
        return $db_last_error->row;
    }

    public function get_pages_count($filters) {
        $limit = 10;
        $table = DB_PREFIX . "errorlog_manager";

        if (!empty($filters['from']) || !empty($filters['to'])) {
            $conditions = array();

            if (!empty($filters['from']) && preg_match('/\d{4}-\d{2}-\d{2}/', $filters['from'])) {
                $conditions[] = "timestamp > '" . $filters['from'] . "'";
            }

            if (!empty($filters['to']) && preg_match('/\d{4}-\d{2}-\d{2}/', $filters['to'])) {
                $conditions[] = "timestamp < '" . $filters['to'] . "'";
            }

            $table = "(SELECT * FROM " . DB_PREFIX . "errorlog_manager WHERE " . implode(' AND ', $conditions) . ") AS tmp";
        }
        $conditions = array();

        if (!empty($filters['file'])) {
            $conditions[] = "filename = '" . $filters['file'] . "'";
        }

        if (!empty($filters['extension'])) {
            $conditions[] = "message LIKE '%" . $filters['extension'] . "%'";
        }

        if (!empty($filters['search'])) {
            $conditions[] = "message LIKE '%" . $filters['search'] . "%'";
        }

        $result = $this->db->query("SELECT COUNT(*) AS rows_total FROM (SELECT * FROM $table WHERE " . implode(' AND ', $conditions) . " GROUP BY message_hash) as tmp_2");

        return ceil((int)$result->row['rows_total'] / $limit);
    }

    public function get_errors($filters = array(), $page = 1) {
        $limit = 10;
        $start = ($page-1) * $limit;
        $table = DB_PREFIX . "errorlog_manager";

        if (!empty($filters['from']) || !empty($filters['to'])) {
            $conditions = array();

            if (!empty($filters['from']) && preg_match('/\d{4}-\d{2}-\d{2}/', $filters['from'])) {
                $conditions[] = "timestamp > '" . $filters['from'] . "'";
            }

            if (!empty($filters['to']) && preg_match('/\d{4}-\d{2}-\d{2}/', $filters['to'])) {
                $conditions[] = "timestamp < '" . $filters['to'] . "'";
            }

            $table = "(SELECT * FROM " . DB_PREFIX . "errorlog_manager WHERE " . implode(' AND ', $conditions) . ") AS tmp";

        }
        $conditions = array();

        if (!empty($filters['file'])) {
            $conditions[] = "filename = '" . $filters['file'] . "'";
        }

        if (!empty($filters['extension'])) {
            $conditions[] = "message LIKE '%" . $filters['extension'] . "%'";
        }

        if (!empty($filters['search'])) {
            $conditions[] = "message LIKE '%" . $filters['search'] . "%'";
        }

        $result = $this->db->query("SELECT *, COUNT(*) AS popularity, MIN(timestamp) AS first_appeared, MAX(timestamp) AS last_appeared FROM $table WHERE " . implode(' AND ', $conditions) . " GROUP BY message_hash ORDER BY popularity DESC LIMIT $start, $limit");

        return $result->rows;
    }

    public function get_first_timestamp($file, $hash) {
        $result = $this->db->query("SELECT MIN(timestamp) as timestamp FROM " . DB_PREFIX . "errorlog_manager WHERE filename='$file' AND message_hash='$hash' LIMIT 1");
        return $result->row['timestamp'];
    }

    public function get_error_message($file, $hash) {
        $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "errorlog_manager WHERE filename='$file' AND message_hash='$hash' LIMIT 1");
        return $result->row['message'];
    }

    public function clear_error($filename, $message_hash) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "errorlog_manager WHERE `filename`='" . $this->db->escape($filename) . "' AND `message_hash`='" . $this->db->escape($message_hash) . "'");
    }
}
