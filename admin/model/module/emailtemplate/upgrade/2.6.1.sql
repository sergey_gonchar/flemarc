ALTER TABLE `oc_emailtemplate_config` ADD `emailtemplate_config_body_bg_image` VARCHAR(255) NOT NULL;
ALTER TABLE `oc_emailtemplate_config` ADD `emailtemplate_config_body_bg_image_position` VARCHAR(32) NOT NULL;
ALTER TABLE `oc_emailtemplate_config` ADD `emailtemplate_config_body_bg_image_repeat` ENUM('no-repeat', 'repeat', 'repeat-y', 'repeat-x') NOT NULL;