ALTER TABLE `oc_emailtemplate` CHANGE `emailtemplate_plain_text` `emailtemplate_mail_plain_text` TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `oc_emailtemplate` ADD `emailtemplate_mail_html` TINYINT(1) NOT NULL DEFAULT '1' AFTER `emailtemplate_mail_plain_text`;

ALTER TABLE `oc_modification` CHANGE `xml` `xml` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;