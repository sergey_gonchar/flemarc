ALTER TABLE `oc_emailtemplate` ADD `_emailtemplate_status` TINYINT(1) NOT NULL AFTER `emailtemplate_status`; 
UPDATE `oc_emailtemplate` SET `_emailtemplate_status` = 0 WHERE `emailtemplate_status` = 'DISABLED';
UPDATE `oc_emailtemplate` SET `_emailtemplate_status` = 1 WHERE `emailtemplate_status` = 'ENABLED';
ALTER TABLE `oc_emailtemplate` DROP `emailtemplate_status`;
ALTER TABLE `oc_emailtemplate` CHANGE `_emailtemplate_status` `emailtemplate_status` TINYINT(1) NOT NULL;

ALTER TABLE `oc_emailtemplate_config` ADD `emailtemplate_config_replace_language` TINYINT(1) NOT NULL DEFAULT '1' ;

UPDATE `oc_emailtemplate` SET `emailtemplate_template` = '' WHERE `emailtemplate_key` IN('order.update_admin', 'admin.return_history');

UPDATE `oc_emailtemplate` SET `emailtemplate_key` = 'customer.return', `emailtemplate_template` = '' WHERE `emailtemplate_key` = 'account.return';