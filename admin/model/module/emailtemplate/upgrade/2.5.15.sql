ALTER TABLE `oc_emailtemplate` ADD `emailtemplate_event` VARCHAR(128) NOT NULL;

ALTER TABLE `oc_emailtemplate` ADD `event_id` INT(11) NOT NULL;

DROP TABLE IF EXISTS `oc_emailtemplate_events`;
CREATE TABLE IF NOT EXISTS `oc_emailtemplate_events` (
  `emailtemplate_event_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `emailtemplate_event_key` varchar(128) NOT NULL,
  `emailtemplate_event_type` varchar(64) NOT NULL,
  PRIMARY KEY (`emailtemplate_event_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO `oc_emailtemplate_events` (`emailtemplate_event_id`, `emailtemplate_event_key`, `emailtemplate_event_type`) VALUES
(NULL, 'post.customer.login', 'customer'),
(NULL, 'post.customer.logout', 'customer'),
(NULL, 'post.customer.add.address', 'customer'),
(NULL, 'post.customer.edit.address', 'customer'),
(NULL, 'post.customer.add', 'customer'),
(NULL, 'post.customer.edit', 'customer'),
(NULL, 'post.customer.edit.password', 'customer'),
(NULL, 'post.customer.edit.newsletter', 'customer'),
(NULL, 'post.return.add', 'return'),
(NULL, 'post.affiliate.add', 'affiliate'),
(NULL, 'post.affiliate.edit', 'affiliate'),
(NULL, 'post.affiliate.edit.payment', 'affiliate'),
(NULL, 'post.affiliate.edit.password', 'affiliate'),
(NULL, 'post.order.add', 'order'),
(NULL, 'post.order.edit', 'order'),
(NULL, 'post.order.history.add', 'order');