ALTER TABLE `oc_emailtemplate` ADD `emailtemplate_type` enum('','customer','affiliate','order','admin') NOT NULL AFTER `emailtemplate_label`;

-- DELETE FROM `oc_emailtemplate_descripton` WHERE `emailtemplate_id` IN (SELECT `emailtemplate_id` FROM `oc_emailtemplate` WHERE `emailtemplate_key` = 'order.update_admin');
-- DELETE FROM `oc_emailtemplate_shortcode` WHERE `emailtemplate_id` IN (SELECT `emailtemplate_id` FROM `oc_emailtemplate` WHERE `emailtemplate_key` = 'order.update_admin');
-- DELETE FROM `oc_emailtemplate` WHERE `emailtemplate_key` = 'order.update_admin';
UPDATE `oc_emailtemplate` SET `emailtemplate_status` = 0 WHERE `emailtemplate_key` = 'customer.return';

UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'affiliate' WHERE `emailtemplate_key` = 'admin.affiliate_approve';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'affiliate' WHERE `emailtemplate_key` = 'admin.affiliate_transaction';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'affiliate' WHERE `emailtemplate_key` = 'affiliate.forgotten';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'affiliate' WHERE `emailtemplate_key` = 'affiliate.register';

UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'admin' WHERE `emailtemplate_key` = 'affiliate.register_admin';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'admin' WHERE `emailtemplate_key` = 'customer.register_admin';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'admin' WHERE `emailtemplate_key` = 'information.contact';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'admin' WHERE `emailtemplate_key` = 'product.review';

UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'customer' WHERE `emailtemplate_key` = 'admin.customer_approve';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'customer' WHERE `emailtemplate_key` = 'admin.customer_create';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'customer' WHERE `emailtemplate_key` = 'admin.customer_reward';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'customer' WHERE `emailtemplate_key` = 'admin.customer_transaction';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'customer' WHERE `emailtemplate_key` = 'admin.newsletter';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'customer' WHERE `emailtemplate_key` = 'customer.forgotten';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'customer' WHERE `emailtemplate_key` = 'customer.register';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'customer' WHERE `emailtemplate_key` = 'information.contact_customer';

UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'order' WHERE `emailtemplate_key` = 'order.customer';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'order' WHERE `emailtemplate_key` = 'order.admin';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'order' WHERE `emailtemplate_key` = 'order.update';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'order' WHERE `emailtemplate_key` = 'order.voucher';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'order' WHERE `emailtemplate_key` = 'order.return';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'order' WHERE `emailtemplate_key` = 'admin.return_history';
UPDATE `oc_emailtemplate` SET `emailtemplate_type` = 'order' WHERE `emailtemplate_key` = 'admin.voucher';