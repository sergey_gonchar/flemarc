<?php
// Heading
$_['heading_title']               = 'WishlistDiscounts 2.1[<a href="http://TheNulledClub.com" target="_blank">TheNulledClub.com</a>]';
$_['wishlist_text']               = 'Wishlist';

// Text
$_['text_module']                 = 'Modules';
$_['text_success']                = ' Success: You have modified module WishlistDiscounts!';
$_['entry_code']                  = 'WishlistManager status:';
$_['entry_highlightcolor']        = 'Highlight color:<br /><span class="help">This is the color the keyword in the results highlights in.<br/><br/><em>Examples: red, blue, #F7FF8C</em></span>';
$_['error_permission']            = 'Warning: You do not have permission to modify module BirthdayReminder!';
$_['subject_text']                = 'Subject';
$_['select_coupon_text']          = 'Select an existing coupon: <span class="help">Select among existing OpenCart coupons.</span>';
$_['select_coupon']               = 'Select coupon';
$_['select_order_status_text']    = 'Select order status: <br /> <span class="help">Select order status that will generate an unique code for every customer</span>';
$_['select_order_status']         = 'Select order status';
$_['text_yes']                    = 'Yes';
$_['text_no']                     = 'No';
$_['text_enabled']                = 'Enabled';
$_['text_disabled']               = 'Disabled';
$_['text_default']                = 'Default';

$_['message_to_customer_heading'] = 'Message to customer';
$_['message_to_customer_help']    = ' Message with discount code that will be sent to customer. 
									<p>You can use the following short-codes:<br />
									{firstname} - fist name<br />
									{lastname} - last name<br />
									{discount_code} - the code of discount coupon<br />
									{customer_wishlist} - wish list of to customer<br />  
									{date_end} - end date of coupon validity</p>';
$_['discount_code_text']          = 'Discount code';
$_['default_subject']             = 'Wishlist Subject';
$_['admin_notification']          = 'Send BCC to store owner:';
$_['error_subject']               = 'Subject must be between 3 and 128 characters!';
$_['days_before_birthday']        = 'Days before birthday <span class="help">How many days before birthday to be sent wish</span>';
$_['admin_notification_help']     = 'Enabling this option will add {email} as BCC recepient';
$_['coupon_validity']             = ' Coupon validity: <span class="help">Select how many days the coupon to be valid after recieved</span>';
$_['text_account']                = 'Account';
$_['text_instock']                = 'In Stock';
$_['text_wishlist']               = 'Wish List (%s)';
$_['column_image']                = 'Image';
$_['column_name']                 = 'Product Name';
$_['column_model']                = 'Model';
$_['column_stock']                = 'Stock';
$_['column_price']                = 'Price';
$_['column_action']               = 'Action';
$_['discount_text']               = 'Discount';
$_['column_coupon_name']         = 'Coupon Name';
$_['column_code']         		= 'Code';
$_['column_discount']     = 'Discount';
$_['column_date_start']   = 'Date Start';
$_['column_date_end']     = 'Date End';
$_['column_status']       = 'Status';
$_['column_order_id']     = 'Order ID';
$_['column_customer']     = 'Customer';
$_['column_amount']       = 'Amount';
$_['column_date_added']   = 'Date Added';
$_['column_action']       = 'Action';

$_['column_customer_name'] = 'Customer Name';
$_['column_customer_email'] = 'Customer Email';
$_['column_customer_wishlist'] = 'Customer Wishlist';
$_['show_wishlist'] = "Show Wishlist";

$_['button_send_discounts_to_selected'] = 'Send discount to selected';
$_['button_send_discount_to_all']	= 'Send discount to all';
$_['text_no_results']				= 'No Results';
$_['text_subject']					= 'Subject';
$_['text_type']						= 'Type';
$_['text_discount']					= 'Discount';
$_['text_duration']					= 'Duration';
$_['text_days']						= 'days';
$_['text_send']						= 'Send';
$_['wishlist_heading']				= 'Customers with products in wishlist';
$_['text_fixed_amount']				= 'Fixed amount';
$_['text_percentage']				= 'Percentage';
$_['default_discount_message']		= 'Hello {firstname} {lastname} </br></br> We see that you have products in your wish list. Now you can buy each of them with {discount_value}% OFF. The coupon code is {discount_code}. The coupon expires on {date_end}. </br></br>{customer_wishlist} </br></br> Best Regards,'
?>