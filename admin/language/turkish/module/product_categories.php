<?php
// Heading
$_['heading_title']    						= 'Products by Category';
$_['heading_license']                   	= 'License Module';

// Text
$_['text_module']      						= 'Modules';
$_['text_success']     						= 'Module settings updated!';
$_['text_success_license']      			= 'The license for the right to use the module is successfully activated!';
$_['text_edit']        						= 'Edit module "Products by Category"';
$_['text_license_activate']             	= 'License activation module "Products by Category"';
$_['text_license_info'] 					= 'Type in the appropriate fields the following data:<br /><ol><li>Order ID;</li><li>Product code;</li><li>License key;</li></ol>Where and how to obtain the data for activation, in detail in the article "<a href="http://maxystore.com/license-product-info.html" target="_blanck"><ins>Rules for buying licensed products</ins></a>"<br />In case you have any questions on obtaining data to activate, write on mail:<ol><li>support@maxystore.com</li><li>maxystore.com@gmail.com</li></ol>Remember that module will only work on the domain that you specified when the license key!';
$_['text_secret_key']              			= 'Copy this secret key and when data is received to activate paste it into the appropriate field.';
$_['text_clipboard']                 		= 'Copy Clipboard';
$_['text_rotate']      						= 'With Rotation';
$_['text_no_rotate']      					= 'Without Rotation';

// Text Design
$_['text_google_font']   					= 'Google Fonts';
$_['text_standart_font']   					= 'Standard Fonts';
$_['text_solid']   							= 'Solid';
$_['text_dotted']   						= 'Dotted';
$_['text_dashed']   						= 'Dashed';
$_['text_normal']   						= 'Normal';
$_['text_bold']   							= 'Bold';
$_['text_italic']   						= 'Italic';
$_['text_normal_text']   					= 'None';
$_['text_capitalize']   					= 'Capitalize';
$_['text_uppercase']   						= 'Uppercase';
$_['text_lowercase']   						= 'Lowercase';

// Entry
$_['entry_secret_key']        				= 'Secret Key';
$_['entry_license_data']        			= 'Data for activation';
$_['entry_order_id']        				= 'Order ID';
$_['entry_product_license_code']        	= 'Product Code';
$_['entry_license_key']        				= 'License Key';
$_['entry_name']       						= 'Name for Administrator';
$_['entry_title']       					= 'Name for Customers';
$_['entry_position']       					= 'Position';
$_['entry_categories']       				= 'Categories';
$_['entry_category']       					= 'Category';
$_['entry_limit_name']       				= 'Limit Name Tabs';
$_['entry_limit_name_product']       		= 'Limit Product Name';
$_['entry_limit_product']       			= 'Limit Products';
$_['entry_product_name']       				= 'Product Name';
$_['entry_cart']       						= 'Button "Cart"';
$_['entry_carousel']       					= 'Scroll Products';
$_['entry_wishlist']       					= 'Wishlist';
$_['entry_compare']       					= 'Compare';
$_['entry_quick_view']       				= 'Quik View';
$_['entry_banner']       					= 'Show Banner';
$_['entry_image_banner']       				= 'Banner Image';
$_['entry_link_banner']       				= 'Link to Banner';
$_['entry_price']       					= 'Product Price';
$_['entry_rating']       					= 'Rating';
$_['entry_google_font']       				= 'Google Fonts';
$_['entry_image_product']       			= 'Image Size Product';
$_['entry_width']       					= 'Width';
$_['entry_height']       					= 'Height';
$_['entry_manufacturer']       				= 'Brand';
$_['entry_model']       					= 'Product Code';
$_['entry_sku']       						= 'SKU';
$_['entry_weight_product']       			= 'Weight';
$_['entry_reward']       					= 'Reward Points';
$_['entry_stock']       					= 'Availability';
$_['entry_more']       						= 'Button "More"';
$_['entry_image_aditional']       			= 'Additional Product Image Size';
$_['entry_icon_status']       				= 'Icons on Buttons';
$_['entry_status']       					= 'Status';

// Entry Design
$_['entry_text']    						= 'Text';
$_['entry_font']     						= 'Font';
$_['entry_bg_image']     					= 'Background Image';
$_['entry_bg_color']       					= 'Background Color';
$_['entry_weight']     						= 'Thickness';
$_['entry_style']     						= 'Style';
$_['entry_color']     						= 'Color';
$_['entry_shadow']     						= 'Shadow';
$_['entry_margin']     						= 'Margin';
$_['entry_padding']     					= 'Padding';
$_['entry_top']     						= 'Top';
$_['entry_right']     						= 'Right';
$_['entry_bottom']     						= 'Bottom';
$_['entry_left']     						= 'Left';
$_['entry_text_style']     					= 'Style Text';
$_['entry_font_size']     					= 'Font Size';
$_['entry_font_style']     					= 'Style';
$_['entry_text_transform']     				= 'Text Transform';

// Entry Box Module Design
$_['entry_box_module_design']       		= 'Box Module';
$_['entry_box_border_weight']       		= 'Border';

// Entry Design Title
$_['entry_title_design']     				= 'Module Title';
$_['entry_bg_top']     						= 'Gradient fill from top';
$_['entry_bg_bottom']     					= 'Gradient fill from bottom';
$_['entry_title_text']     					= 'Title Text';

// Entry Design Tabs
$_['entry_tabs_design']     				= 'Module Tabs';
$_['entry_tabs_active_design']     			= 'Active Tab';
$_['entry_tabs_width']     					= 'Width Block Tabs';
$_['entry_padding_top']     				= 'Indent-tabs on Top';
$_['entry_tabs_text']     					= 'Text Tabs';
$_['entry_tabs_active_text']       			= 'Text Tab';

// Entry Design Box Product
$_['entry_box_product_design']       		= 'Box Product';
$_['entry_box_product_hover_design']       	= 'Box Hover';

// Design Box Product Name
$_['entry_box_product_name_design']       	= 'Product Name';
$_['entry_box_product_name_text']       	= 'Text Name';
$_['entry_box_product_name_hover_design']   = 'Name Hover';

// Design Box Product Price
$_['entry_box_product_price_design']       	= 'Price Product';
$_['entry_box_product_price_text']       	= 'Text Price';
$_['entry_box_product_price_old_design']    = 'Price Old';

// Design Box Product Cart
$_['entry_cart_design']    					= 'Button "Cart"';
$_['entry_box_product_cart_text']    		= 'Text Button';
$_['entry_border_radius']    				= 'Border Radius';
$_['entry_left_top']    					= 'Top Left';
$_['entry_right_top']    					= 'Top Right';
$_['entry_right_bottom']    				= 'Bottom Right';
$_['entry_left_bottom']    					= 'Bottom Left';
$_['entry_margin_bottom']    				= 'Padding bottom';
$_['entry_cart_hover_design']    			= 'Button "Cart" Hover';

// Design Buttons
$_['entry_buttons_design']    				= 'Other Buttons';
$_['entry_buttons_hover_design']    		= 'Other Buttons Hover';

// Design Button Carousel
$_['entry_btn_carousel_design']    			= 'Scroll Buttons';
$_['entry_border_radius_left']    			= 'Border Radius Left Button';
$_['entry_border_radius_right']    			= 'Border Radius Right Button';
$_['entry_btn_carousel_hover_design']    	= 'Scroll Buttons Hover';

// Design Quik View
$_['entry_name_data_design']    			= 'Name Data Product';
$_['entry_value_data_design']    			= 'Value Data Product';
$_['entry_box_product_price_other']  		= 'Other Prices';
$_['entry_quick_buttons']  					= 'Buttons';
$_['entry_quick_buttons_hover']  			= 'Buttons Hover';

// Tab
$_['tab_design_module']       				= 'Module Design';
$_['tab_design_quick_view']       			= 'Design Quick View';

// Button
$_['button_clear_setting']					= 'Reset Settings';

// Help
$_['help_banner']       					= 'Banner size should be: Width - 250px, Height - 350px';

// Error
$_['error_permission'] 						= 'Warning: You do not have sufficient rights to run this module!';
$_['error_name']       						= 'Module Name must be between 3 and 64 characters!';
$_['error_license']    						= 'The license key does not belong to this domain or incorrect order ID or product code or secret key!';