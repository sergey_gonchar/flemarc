<?php
// Heading
$_['heading_title']    = 'Auto Invoice Number';

$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified Auto Invoice Number module!';
$_['text_edit']        = 'Edit Auto Invoice Number Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Auto Invoice Number module!';