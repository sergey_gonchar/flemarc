<?php
// Heading
$_['heading_title']      = 'Anti-Fraude IP';

// Text
$_['text_fraud']         = 'Anti-Fraude';
$_['text_success']       = 'Succes: Instellingen gewijzigd!';
$_['text_edit']          = 'Anti-Fraude IP wijzigen';
$_['text_ip_add']        = 'IP-adres toevoegen';
$_['text_ip_list']       = 'Lijst';

// Column
$_['column_ip']          = 'IP-adres';
$_['column_total']       = 'Totaal Accounts';
$_['column_date_added']  = 'Datum toegevoegd';
$_['column_action']      = 'Aktion';

// Entry
$_['entry_ip']           = 'IP';
$_['entry_status']       = 'Status';
$_['entry_order_status'] = 'Bestelstatus';

// Help
$_['help_order_status']  = 'Klanten met een geblokkerd IP-adres krijgen deze bestelstatus en kunnen hun bestelling niet automatisch afronden.';

// Error
$_['error_permission']   = 'Waarschuwing: U heeft geen rechten deze instellingen te wijzigen!';
