<?php  
// Heading
$_['heading_title']         = 'New Mail';

$_['start_send']            = 'Newsletter launched.';
$_['end_send']              = 'Sending over.';
$_['count_email']           = 'Just to send %s emails.';
$_['send_email']            = 'Sending on: ';
$_['bad_email']             = 'Wrong email: ';

// Text
$_['text_success']             = 'All messages sent!';
$_['text_success_setting']     = 'The settings are saved!';
$_['text_sent']                = 'Your message has been successfully sent to <span style="color:red;">%s</span> of <span style="color:red;">%s</span> recipients! <span style="color:red;">Do not leave the page until the end of the distribution!</span>';
$_['text_default']             = 'Default';
$_['text_newsletter']          = 'All Newsletter Subscribers';
$_['text_customer_all']        = 'All Customers';
$_['text_customer_group']      = 'Customer Group';
$_['text_customer']            = 'Selected Customers';
$_['text_client_all']          = 'All buyers';
$_['text_affiliate_all']       = 'All Affiliates';
$_['text_affiliate']           = 'Selected Affiliates';
$_['text_product']             = 'Bought the product';
$_['text_manual']              = 'Manual list';
$_['text_save']                = 'Save';
$_['text_save_template']       = 'Save as template';
$_['text_new_template']        = 'Add template';
$_['text_template_name']       = 'Name of the template';
$_['text_success_template']    = 'Template is saved!';
$_['text_delete_template']     = 'Template is deleted!';
$_['text_unsubscribe']         = 'You can unsubscribe by clicking on the link: <a href="%s">Unsubscribe</a>';
$_['text_wait']                = 'Wait! We are collecting data!';

$_['text_no_lic']              = 'Module not activated! The functionality is limited!';
$_['text_ok_lic']              = 'Successful activation! Thank you for your purchase!';
$_['text_error_lic']           = 'Activation error! Please contact the developer: shchs@ya.ru';
$_['button_bta']               = 'Activate';

$_['text_tegi']                = 'Help tags:';
$_['spravka_tegi']             = 'It is possible to use the following spec.tags:<br /><span>{name}</span>-  Firstname Lastname (or what you find of them, if both are absent, the "Client")<br /><span>{firstname}</span>-  Firstname (if not, remove the tag)<br /><span>{lastname}</span>-  Lastname (if not, remove the tag)<br /><span>{shopname}</span>-  Store name (from the settings)<br /><span>{shopurl}</span>-  Store name link (on the main page)<br /><span>{country}</span>-  Country (data from the registration of the customer; if mailing "All buyers" or "Bought the product", of shipping address)<br /><span>{zone}</span>-  Zone (data from the registration of the customer; if mailing "All buyers" or "Bought the product", of shipping address)<br /><span>{special}</span>-  Products with the action (if not, remove the tag)<br /><span>{bestseller}</span>-  Bestsellers (if not, remove the tag)<br /><span>{latest}</span>-  Latest (if not, remove the tag)<br /><span>{featured}</span>-  Featured products (from a standard module "Featured", if not, remove the tag)<br /><span>{selproducts}</span>-  Selected products (if not, remove the tag)';

$_['text_mail']                = 'Mail';
$_['text_smtp']                = 'SMTP';

$_['tab_send']                = 'Sending';
$_['tab_template']            = 'Templates';
$_['tab_newsletter']          = 'Subscribers';
$_['tab_newsgroup']           = 'Distribution group';
$_['tab_log']                 = 'Sending log';
$_['tab_setting']             = 'Settings';

// Entry
$_['entry_store']              = 'From:';
$_['entry_to']                 = 'To:';
$_['entry_customer_group']     = 'Customer group:';
$_['entry_customer']           = 'Customer:';
$_['help_customer']            = 'Autocomplete';
$_['entry_affiliate']          = 'Affiliate:';
$_['help_affiliate']           = 'Autocomplete';
$_['entry_product']            = 'Products:';
$_['help_product']             = 'Send to customers who have ordered goods from the list.';
$_['entry_manual']             = 'E-mails:';
$_['help_manual']              = 'Enter e-mails separated by commas.';
$_['entry_subject']            = 'Subject:';
$_['help_subject']             = 'Allowed tags:<br />{name},{firstname},{lastname},{shopname},{country},{zone}';
$_['entry_message']            = 'Message:';
$_['help_message']             = 'Allowed tags:<br />{name},{firstname},{lastname},{shopname},{country},{zone},{shopurl},{special},{bestseller},{latest},{featured},{selproducts}';
$_['entry_template']           = 'Use the template:';

$_['entry_mail_count_message'] = 'The number of messages in one transfer:';
$_['entry_mail_sleep_time']    = 'Delay sending in seconds:';
$_['entry_mail_from']          = 'Sender E-mail address:';
$_['help_from']                = 'Be comma-separated. If not specified, the address is taken from store settings.';
$_['entry_mail_protocol']      = 'Mail Protocol:';
$_['help_protocol']            = 'Only choose \'Mail\' unless your host has disabled the php mail function.';
$_['entry_mail_parameter']     = 'Mail Parameters:';
$_['help_parameter']           = 'When using \'Mail\', additional mail parameters can be added here (e.g. "-femail@storeaddress.com").';
$_['entry_smtp_host']          = 'SMTP Host:';
$_['entry_smtp_username']      = 'SMTP Username:';
$_['entry_smtp_password']      = 'SMTP Password:';
$_['entry_smtp_port']          = 'SMTP Port:';
$_['entry_smtp_timeout']       = 'SMTP Timeout:';
$_['entry_product_currency']   = 'Products currency:';

$_['column_action']            = 'Action';
$_['column_template_name']     = 'Name of the template';
$_['text_view']                = 'View';
$_['text_delete']              = 'Delete';
$_['text_no_template']         = 'No saved templates!';

$_['text_region']          = 'To consider address';
$_['entry_country']        = 'Country';
$_['entry_zone']           = 'Zone';
$_['entry_unsubscribe']    = 'Add a link "unsubscribe"';
$_['text_client']          = 'Client';

$_['button_clearlog']          = 'Clear log';
$_['text_delete_log']          = 'Log deleted!';
$_['text_update_log']          = 'Log updated!';
$_['button_updatelog']         = 'Update log';

$_['entry_insert_products']    = 'To attach the goods';
$_['entry_special']            = 'Products with the action';
$_['entry_bestseller']         = 'Bestsellers';
$_['entry_featured']           = 'Featured products';
$_['entry_latest']             = 'Latest';
$_['entry_selected']           = 'Selected products';
$_['entry_pselected']          = 'Select products';
$_['help_selproduct']          = 'Autocomplete';
$_['entry_title']         = 'Header';
$_['entry_limit']         = 'Quantity';

$_['featured_title']        = 'Featured products';
$_['bestseller_title']      = 'Bestsellers';
$_['special_title']         = 'Special';
$_['latest_title']          = 'Latest';
$_['selproducts_title']       = 'Recommend';
$_['category_products_title'] = 'Our products';

// Error
$_['error_permission']         = 'You do not have permission to edit messages!';
$_['error_save_setting']       = 'You have no rights to save the settings!';
$_['error_save_template']      = 'Failed to save template!';
$_['error_del_template']       = 'Failed to delete the template!';
$_['error_name_template']      = 'Enter the name of the template!';
$_['error_subject']            = 'E-Mail Subject required!';
$_['error_message']            = 'E-Mail Message required!';
$_['editor_mode_alert']        = 'Switch the editor to visual mode!';
$_['error_close']              = 'Do not leave the page if newsletter launched!\nThis will lead to her termination!';
?>