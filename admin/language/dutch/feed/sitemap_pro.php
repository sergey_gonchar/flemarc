<?php
// *	@copyright	modules.pro

// Heading
$_['heading_title']    = 'Sitemap Pro';

// Text
$_['text_feed']        = 'Feeds';
$_['text_success']     = 'Success: You have modified Sitemap feed!';
$_['text_edit']        = 'Edit Sitemap';

// Entry
$_['entry_status']     = 'Status';
$_['entry_data_feed']  = 'Google feed';
$_['entry_data_yandex_feed']  = 'Yandex feed';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Sitemap feed!';