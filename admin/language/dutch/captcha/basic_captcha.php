<?php
$_['heading_title']    = 'Basic Captcha';

// Text
$_['text_captcha']     = 'Captcha';
$_['text_success']	   = 'Succes: Instellingen gewijzigd!';
$_['text_edit']        = 'Basic Captcha wijzigen';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Waarschuwing: U heeft geen rechten deze instellingen te wijzigen!';
