<?php
$_['heading_title']    = 'Google reCAPTCHA';

// Text
$_['text_captcha']     = 'Captcha';
$_['text_success']	   = 'Succes: Instellingen gewijzigd!';
$_['text_edit']        = 'Google reCAPTCHA wijzigen';
$_['text_signup']      = 'Ga naar <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank"><u>Google reCAPTCHA page</u></a> and registeer uw website.';

// Entry
$_['entry_key']        = 'Site key';
$_['entry_secret']     = 'Secret key';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Waarschuwing: U heeft geen rechten deze instellingen te wijzigen!';
$_['error_key']	       = 'Waarschuwing: Key verplicht!';
$_['error_secret']	   = 'Waarschuwing: Secret verplicht!';
