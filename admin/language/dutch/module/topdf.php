<?php
// Heading
$_['heading_title']          = 'Export to PDF'; 
$_['text_module']         = 'Modules';
$_['text_add_titul']         = 'To add the title page ';

// Column
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';
$_['column_category']        = 'Category';
$_['column_manufacturer']    = 'Manufacturer';
$_['column_description']    = 'Description';
$_['column_url']    = 'URL';
$_['column_qrcode']    = 'QR-code';


//PDF
$_['pdf_name']            = 'Name: ';
$_['pdf_model']            = 'Model: ';
$_['pdf_category']        = 'Category: ';
$_['pdf_manufacturer']    = 'Manufacturer: ';
$_['pdf_price']            = 'Price: ';
$_['pdf_special']            = 'Special: ';
$_['pdf_discount']            = 'Discount: ';
$_['pdf_url']            = 'The link to goods in online store';
$_['pdf_description']    = 'Description: ';
$_['pri']    = ' at purchase ';
$_['sht']    = ' pieces and more ';

// Error
$_['error_permission']       = 'Warning: You do not have permission to modify products!';

// Button
$_['button_clear']           = 'Clear';
$_['button_savepdf']           = 'Save to PDF';
$_['button_add']             = 'Add';
$_['button_cancel']           = 'Cancel';
$_['button_sbros']           = 'Сlearing of settings';
$_['button_filter']                 = 'Filter';
$_['button_edit']           = 'Edit';



// Table head
$_['head1']           = '1. To add or remove goods from a PDF-basket';
$_['head1_1']           = 'In a basket';
$_['head1_2']           = 'goods';
$_['head2']           = '2. To adjust fields, for this purpose:';
$_['head2_1'] 		 = '- move the necessary fields to yellow area';
$_['head2_2'] 		 = '- if necessary change sequence of fields in yellow area by dragging to the left and to the right (in PDF the file of a field register in the sequence presented in yellow area)';
$_['head3']             = '3. To write down PDF-basket contents in the file';
$_['text_list']          = 'Export to PDF'; 





$_['entry_name']             = 'Product Name';
$_['entry_description']      = 'Description';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_model']            = 'Model';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Location';
$_['entry_shipping']         = 'Requires Shipping';
$_['entry_manufacturer']     = 'Manufacturer';
$_['entry_store']            = 'Stores';
$_['entry_date_available']   = 'Date Available';
$_['entry_quantity']         = 'Quantity';
$_['entry_minimum']          = 'Minimum Quantity';
$_['entry_stock_status']     = 'Out Of Stock Status';
$_['entry_price']            = 'Price';
$_['entry_tax_class']        = 'Tax Class';
$_['entry_points']           = 'Points';
$_['entry_option_points']    = 'Points';
$_['entry_subtract']         = 'Subtract Stock';
$_['entry_weight_class']     = 'Weight Class';
$_['entry_weight']           = 'Weight';
$_['entry_dimension']        = 'Dimensions (L x W x H)';
$_['entry_length_class']     = 'Length Class';
$_['entry_length']           = 'Length';
$_['entry_width']            = 'Width';
$_['entry_height']           = 'Height';
$_['entry_image']            = 'Image';
$_['entry_customer_group']   = 'Customer Group';
$_['entry_date_start']       = 'Date Start';
$_['entry_date_end']         = 'Date End';
$_['entry_priority']         = 'Priority';
$_['entry_attribute']        = 'Attribute';
$_['entry_attribute_group']  = 'Attribute Group';
$_['entry_text']             = 'Text';
$_['entry_option']           = 'Option';
$_['entry_option_value']     = 'Option Value';
$_['entry_required']         = 'Required';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_category']         = 'Categories';
$_['entry_filter']           = 'Filters';
$_['entry_download']         = 'Downloads';
$_['entry_related']          = 'Related Products';
$_['entry_tag']          	 = 'Product Tags';
$_['entry_reward']           = 'Reward Points';
$_['entry_layout']           = 'Layout Override';
$_['entry_recurring']        = 'Recurring Profile';


?>