<?php
// Heading
$_['heading_title']        = 'Show Recommended Products When No Search Result ';

// Text
$_['text_module']          = 'Modules';
$_['text_success']         = 'Success: You have modified module Show Recommended Products When No Search Result!';
$_['text_edit']            = 'Edit Show Recommended Products When No Search Result!';

// Tab
$_['tab_setting']          = 'Settings';
$_['tab_help']             = 'Help';

// Entry
$_['entry_status']         = 'Status';
$_['entry_title']          = 'Title';
$_['entry_product']        = 'Products';

// Legend
$_['legend_products']      = 'Choose recommended products';
$_['legend_title']         = 'Title for \'No search results found, but we recommend\'';

// Help
$_['help_title']           = 'Ex: \'No products found, but we recommend\'';

// Error 
$_['error_permission']     = 'Warning: You do not have permission to modify module Show Recommended Products When No Search Result!';
$_['error_product']        = 'Error: Please choose at least one product!';
$_['error_title']          = 'Error: Please add Title text for all available languages';
$_['error_title_required'] = 'Title - required!';
?>