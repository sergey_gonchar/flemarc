<?php
// Heading
$_['heading_title']       = '<span style="color:#597AB2;font-weight:700;">"МАГАЗИН"</span> карусель товаров';

// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Настройки успешно изменены!';
$_['text_edit']           = 'Настройки модуля';

// Entry
$_['entry_name']       	  = 'Название модуля';
$_['entry_limit']      	  = 'Лимит';
$_['entry_width']         = 'Ширина';
$_['entry_height']        = 'Высота';
$_['entry_status']        = 'Статус';
$_['featured_module']     = 'Модуль рекомендуемых';

// Error
$_['error_permission']    = 'У Вас нет прав для управления данным модулем!';
$_['error_name']          = 'Название модуля должно быть от 3 до 64 символов!';
$_['error_width']         = 'Необходимо указать ширину!';
$_['error_height']        = 'Необходимо указать высоту!';