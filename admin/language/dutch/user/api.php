<?php
// Heading
$_['heading_title']        = 'API\'s';

// Text
$_['text_success']         = 'Succes: Instellingen gewijzigd!';
$_['text_list']            = 'Lijst';
$_['text_add']             = 'Toevoegen';
$_['text_edit']            = 'Wijzigen';
$_['text_ip']              = 'Hieronder kunt u een lijst aanmaken met IP-adressen welke toegang mogen hebben tot de API. Uw IP-adres is %s';

// Column
$_['column_name']      	   = 'Naam';
$_['column_status']        = 'Status';
$_['column_date_added']    = 'Datum toegevoegd';
$_['column_date_modified'] = 'Datum gewijzigd';
$_['column_token']         = 'Token';
$_['column_ip']            = 'IP-adres';
$_['column_action']        = 'Aktie';

// Entry
$_['entry_name']	= 'Naam';
$_['entry_key']		= 'API key';
$_['entry_status']	= 'Status';
$_['entry_ip']		= 'IP-adres';

// Error
$_['error_permission']  = 'Waarschuwing: U heeft geen rechten deze instellingen te wijzigen';
$_['error_name']       	= 'Gebruikersnaam dient tussen de 3 en 20 tekens lang te zijn!';
$_['error_key']       	= 'API-key dient tussen de 64 en 256 tekens lang te zijn!';