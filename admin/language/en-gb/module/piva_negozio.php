<?php
// Heading
$_['heading_title']      = 'Company ID of the Store';
$_['piva_title']  		 = 'Company ID of the Store';

// Text
$_['text_edit']  		 = 'Edit Company ID of the Store';
$_['piva_description']		= '';
$_['piva_success_text']		= 'Success! You have modified Company ID of the Store!';
$_['piva_save']				= 'Save';
$_['piva_cancel']			= 'Cancel';
$_['text_module'] 	    	= 'Modules';
$_['piva_yes']				= 'Yes';
$_['piva_no']			    = 'No';

// Entry
$_['piva_entry_required'] 	= 'Company ID is required';
$_['piva_entry_required_description'] 	= 'Company ID is a required field';
$_['piva_entry_length'] 	= 'Length of company ID';
$_['piva_entry_length_description'] 	= 'Length is verified while saving the data of the store.';

// Error
$_['piva_error_access'] = 'You do not have permission to modify Company ID of the Store module.';
?>
