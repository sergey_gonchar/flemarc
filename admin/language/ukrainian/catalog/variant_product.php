<?php
// Entry
$_['entry_related_variant_name']          	= 'Группа вариантов';
$_['entry_related_variant_type']          	= 'Группа вариантов';
$_['entry_related_variant']          		= 'Товары для варианта';
$_['help_related_variant_name']          	= 'Название для варианта (например: цвет)';
$_['help_related_variant']          		= 'Продукты для варианта.';
$_['tab_variant']          		            = 'Вариант';
$_['text_variant']          		        = 'Варианты';
$_['head_news']          		            = 'Создать новость';
