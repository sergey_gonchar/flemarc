<?php
// Text
$_['text_column_homel']   = '2/3';
$_['text_column_homer']   = '1/3';
$_['text_content_slide']  = 'Верх на всю ширину';
$_['text_content_footer'] = 'Низ на всю ширину';

// subscription
$_['url_subscription']    = 'Передплатники';

//variant
$_['url_variant']         = 'Варіанти';
$_['url_news']            = 'Створити новину';
