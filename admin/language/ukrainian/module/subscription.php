<?php
// Heading
$_['heading_title']    = '<span style="color:#597AB2;font-weight:700;">"МАГАЗИН"</span> подписка на новости';

// Text
$_['text_edit']        = 'Настройка модуля';
$_['text_module']      = 'модули';
$_['text_success']     = 'Настройки модуля успешно применены!';
$_['text_email']       = 'Почта';
$_['text_name']        = 'Имя';
$_['text_date']        = 'Дата';
$_['text_store']       = 'Магазин';
$_['text_lang']        = 'Язык';
$_['text_del']         = 'Удалить';
$_['text_list']		   = 'Список подписчиков';
$_['text_warning']	   = 'Вы действительно хотите удалить запись?';
$_['text_search']	   = 'Поиск на текущей странице';
$_['text_enabled']     = 'Включено';
$_['text_disabled']    = 'Отключено';

// Entry
$_['entry_cupon']      = 'Текст возле формы:';
$_['entry_text']       = 'Текст письма для подписчика:';
$_['entry_status']     = 'Статус:';

// Error
$_['error_permission'] = 'У вас нет прав на редактирование этого модуля!';
$_['error_cupon']      = 'не ввели купон!';
?>