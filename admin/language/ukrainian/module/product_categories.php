<?php
// Heading
$_['heading_title']    						= 'Товары по категориям';
$_['heading_license']                   	= 'Лицензирование модуля';

// Text
$_['text_module']      						= 'Модули';
$_['text_success']     						= 'Настройки модуля успешно обновлены!';
$_['text_success_license']      			= 'Лицензия на право пользования модулем - успешно активирована!';
$_['text_edit']        						= 'Редактирование модуля "Товары по категориям"';
$_['text_license_activate']             	= 'Активация лицензии модуля "Товары по категориям"';
$_['text_license_info'] 					= 'Введите в соответствующие поля следующие данные:<br /><ol><li>№ заказа;</li><li>Код товара;</li><li>Ключ активации;</li></ol>Где и как получить данные для активации, подробно написано в статье "<a href="http://maxystore.com/license-product-info.html" target="_blanck"><ins>Правила покупки лицензионных товаров</ins></a>"<br />В случае возникновения вопросов по получению данных для активации, пишите непосредственно на почту:<ol><li>support@maxystore.com</li><li>maxystore.com@gmail.com</li></ol>Помните, что модуль будет работать только на том домене, который вы указали при получении ключа активации!';
$_['text_secret_key']              			= 'Скопируйте данный секретный ключ и при получении данных для активации вставьте его в соответствующее поле.';
$_['text_clipboard']                		= 'Скопировать в буфер обмена';
$_['text_rotate']      						= 'С вращением';
$_['text_no_rotate']      					= 'Без вращения';

// Text Design
$_['text_google_font']   					= 'Google шрифты';
$_['text_standart_font']   					= 'Стандартные шрифты';
$_['text_solid']   							= 'Сплошная линия';
$_['text_dotted']   						= 'Мелкий пунктир';
$_['text_dashed']   						= 'Крупный пунктир';
$_['text_normal']   						= 'Нормальный';
$_['text_bold']   							= 'Жирный';
$_['text_italic']   						= 'Курсив';
$_['text_normal_text']   					= 'Обычный текст';
$_['text_capitalize']   					= 'Каждое слово с заглавной буквы';
$_['text_uppercase']   						= 'Только большие буквы';
$_['text_lowercase']   						= 'Только маленькие буквы';

// Entry
$_['entry_secret_key']     					= 'Секретный ключ';
$_['entry_license_data']        			= 'Данные для активации';
$_['entry_order_id']        				= '№ заказа';
$_['entry_product_license_code']    		= 'Код товара';
$_['entry_license_key']        				= 'Ключ активации';
$_['entry_name']       						= 'Название для администратора';
$_['entry_title']       					= 'Название для покупателей';
$_['entry_position']       					= 'Расположение';
$_['entry_categories']       				= 'Категории';
$_['entry_category']       					= 'Категория';
$_['entry_limit_name']       				= 'Лимит названия вкладки';
$_['entry_limit_name_product']       		= 'Лимит названия товаров';
$_['entry_limit_product']       			= 'Лимит товаров';
$_['entry_product_name']       				= 'Название товара';
$_['entry_cart']       						= 'Копка купить';
$_['entry_carousel']       					= 'Прокрутка товаров';
$_['entry_wishlist']       					= 'В заметки';
$_['entry_compare']       					= 'В сравнения';
$_['entry_quick_view']       				= 'Быстрый просмотр';
$_['entry_banner']       					= 'Показывать баннер';
$_['entry_image_banner']       				= 'Изображение баннера';
$_['entry_link_banner']       				= 'Ссылка на баннер';
$_['entry_price']       					= 'Цена товара';
$_['entry_rating']       					= 'Рейтинг';
$_['entry_google_font']       				= 'Google шрифты';
$_['entry_image_product']       			= 'Размер изображения товара';
$_['entry_width']       					= 'Ширина';
$_['entry_height']       					= 'Высота';
$_['entry_manufacturer']       				= 'Производитель';
$_['entry_model']       					= 'Модель';
$_['entry_sku']       						= 'Артикул';
$_['entry_weight_product']       			= 'Вес';
$_['entry_reward']       					= 'Бонусные баллы';
$_['entry_stock']       					= 'Наличие';
$_['entry_more']       						= 'Кнопка подробнее';
$_['entry_image_aditional']       			= 'Размер дополнительных изображений';
$_['entry_icon_status']       				= 'Иконки на кнопках';
$_['entry_status']       					= 'Статус';

// Entry Design
$_['entry_text']    						= 'Текст';
$_['entry_font']     						= 'Шрифт';
$_['entry_bg_image']     					= 'Фоновое изображение';
$_['entry_bg_color']       					= 'Фоновый цвет';
$_['entry_weight']     						= 'Толщина';
$_['entry_style']     						= 'Стиль';
$_['entry_color']     						= 'Цвет';
$_['entry_shadow']     						= 'Тень';
$_['entry_margin']     						= 'Наружние отступы';
$_['entry_padding']     					= 'Внутренние отступы';
$_['entry_top']     						= 'Сверху';
$_['entry_right']     						= 'Справа';
$_['entry_bottom']     						= 'Снизу';
$_['entry_left']     						= 'Слева';
$_['entry_text_style']     					= 'Стиль текста';
$_['entry_font_size']     					= 'Размер шрифта';
$_['entry_font_style']     					= 'Стиль';
$_['entry_text_transform']     				= 'Трансформация текста';

// Entry Box Module Design
$_['entry_box_module_design']       		= 'Бокс модуля';
$_['entry_box_border_weight']       		= 'Наружняя граница';

// Entry Design Title
$_['entry_title_design']     				= 'Заголовок модуля';
$_['entry_bg_top']     						= 'Градиент заливки сверху';
$_['entry_bg_bottom']     					= 'Градиент заливки снизу';
$_['entry_title_text']     					= 'Текст заголовка';

// Entry Design Tabs
$_['entry_tabs_design']     				= 'Вкладки модуля';
$_['entry_tabs_active_design']     			= 'Активная вкладка';
$_['entry_tabs_width']     					= 'Ширина блока вкладок';
$_['entry_padding_top']     				= 'Отступ вкладок сверху';
$_['entry_tabs_text']     					= 'Текст вкладок';
$_['entry_tabs_active_text']       			= 'Текст вкладки';

// Entry Design Box Product
$_['entry_box_product_design']       		= 'Бокс товара';
$_['entry_box_product_hover_design']       	= 'Бокс при наведении';

// Design Box Product Name
$_['entry_box_product_name_design']       	= 'Название товара';
$_['entry_box_product_name_text']       	= 'Текст названия';
$_['entry_box_product_name_hover_design']   = 'Название при наведении';

// Design Box Product Price
$_['entry_box_product_price_design']       	= 'Цена товара';
$_['entry_box_product_price_text']       	= 'Текст цены';
$_['entry_box_product_price_old_design']    = 'Старая цена';

// Design Box Product Cart
$_['entry_cart_design']    					= 'Кнопка "Купить"';
$_['entry_box_product_cart_text']    		= 'Текст кнопки';
$_['entry_border_radius']    				= 'Скругление углов';
$_['entry_left_top']    					= 'Сверху слева';
$_['entry_right_top']    					= 'Сверху справа';
$_['entry_right_bottom']    				= 'Снизу справа';
$_['entry_left_bottom']    					= 'Снизу слева';
$_['entry_margin_bottom']    				= 'Отступ снизу';
$_['entry_cart_hover_design']    			= 'Кнопка "Купить" при наведении';

// Design Buttons
$_['entry_buttons_design']    				= 'Прочие кнопки';
$_['entry_buttons_hover_design']    		= 'Прочие кнопки при наведении';

// Design Button Carousel
$_['entry_btn_carousel_design']    			= 'Кнопки прокрутки';
$_['entry_border_radius_left']    			= 'Скругление левой кнопки';
$_['entry_border_radius_right']    			= 'Скругление правой кнопки';
$_['entry_btn_carousel_hover_design']    	= 'Кнопки прокрутки при наведении';

// Design Quik View
$_['entry_name_data_design']    			= 'Название данных товара';
$_['entry_value_data_design']    			= 'Значение данных товара';
$_['entry_box_product_price_other']  		= 'Остальные цены';
$_['entry_quick_buttons']  					= 'Кнопки';
$_['entry_quick_buttons_hover']  			= 'Кнопки при наведении';

// Tab
$_['tab_design_module']       				= 'Дизайн модуля';
$_['tab_design_quick_view']       			= 'Дизайн быстрого просмотра';

// Button
$_['button_clear_setting']					= 'Сбросить настройки';

// Help
$_['help_banner']       					= 'Размер баннера должен быть: Ширина - 250px, Высота - 350px';

// Error
$_['error_permission'] 						= 'Внимание: У Вас недостаточно прав для управления данным модулем!';
$_['error_name']       						= 'Название модуля должно быть от 3 до 64 символов!';
$_['error_license']    						= 'Ключ активации не принадлежит этому домену или неверный № заказа, код товара либо секретный ключ!';