<?php
// Heading
$_['heading_title']          = 'FAQ Configuration';

// Text
$_['text_success']           = 'Success: You have modified faq configuration!';
$_['text_edit']              = 'Edit FAQ Configuration';

// Entry
$_['entry_title']            = 'FAQ Title';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_coc_faq_seo_keyword']  = 'SEO Keyword';
$_['entry_coc_faq_license_id']   = 'License Code';
$_['entry_coc_faq_template']   = 'Template';

//Tab
$_['tab_license']            = 'License';

// Help
$_['help_coc_faq_seo_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the keyword is globally unique.';
$_['help_coc_faq_license_id']            = 'The Order ID you bought this extension at OpenCart.com';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify faq configuration!';
$_['error_title']            = 'FAQ Title must be between 3 and 64 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_coc_faq_seo_keyword']          = 'SEO keyword already in use!';
