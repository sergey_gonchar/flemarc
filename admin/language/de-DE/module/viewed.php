<?php
// Heading
$_['heading_title']    = '<span style="color:#597AB2;font-weight:700;">"МАГАЗИН"</span> просмотренные товары';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Настройки модуля';

// Entry
$_['entry_limit']      = 'Лимит';
$_['entry_name']       = 'Имя модуля';
$_['entry_image']      = 'Картинка (Ш x В)';
$_['entry_width']      = 'Ширина';
$_['entry_height']     = 'Высота';
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У Вас нет прав для управления данным модулем!';
$_['error_height']     = 'Необходимо указать высоту!';
$_['error_width']      = 'Необходимо указать ширину!';
$_['error_name']       = 'Необходимо указать имя модуля!';