<?php
// Heading
$_['heading_title']    = '<span style="color:#597AB2;font-weight:700;">"МАГАЗИН"</span> купон';

// Text
$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактировать Купон модуль';

// Entry
$_['entry_sale']       = 'Скидка';
$_['entry_cupon']      = 'Код купона';
$_['entry_info']       = 'Информация';
$_['entry_status']     = 'Статус';


// Error
$_['error_permission'] = 'У вас нет прав для изменения данного модуля!';
$_['error_code']       = 'Обязательное заполнение';