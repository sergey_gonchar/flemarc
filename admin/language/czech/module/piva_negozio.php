<?php
// Heading
$_['heading_title']      = 'IČO obchodu';
$_['piva_title']  		 = 'IČO obchodu';

// Text
$_['text_edit']  		 = 'IČO obchodu';
$_['piva_description']		= '';
$_['piva_success_text']		= 'Úspěch: Upravili jste IČO obchodu!';
$_['piva_save']				= 'Uložit';
$_['piva_cancel']			= 'Zrušit';
$_['text_module'] 	    	= 'Moduly';
$_['piva_yes']				= 'Ano';
$_['piva_no']			    	= 'Ne';

// Entry
$_['piva_entry_required'] 	= 'Je nutné uvést IČO';
$_['piva_entry_required_description'] 	= '';
$_['piva_entry_length'] 	= 'Délka IČO';
$_['piva_entry_length_description'] 	= 'Délka se ověřuje při ukládání dat do systému.';

// Error
$_['piva_error_access'] = 'Nemáte oprávnění upravovat IČO tohoto modulu.';
?>
