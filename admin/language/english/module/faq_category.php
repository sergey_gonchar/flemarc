<?php
// Heading
$_['heading_title']    = 'FAQ Category';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified faq category module!';
$_['text_edit']        = 'Edit FAQ Category Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify faq category module!';