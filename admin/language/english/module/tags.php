<?php
// Heading
$_['heading_title']    = 'Tags Cloud [<a href="http://feofan.net" target="_blank">feofan.net</a>]';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified tags cloud module!';
$_['text_edit']        = 'Edit Tags Cloud Module';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify tags cloud module!';