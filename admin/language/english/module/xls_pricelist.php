<?php
// Heading
$_['heading_title']       = 'XLS Price';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module XLS Price!';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Price (Low > High)';
$_['text_price_desc']   = 'Price (High > Low)';
$_['text_rating_asc']   = 'Rating (Highest)';
$_['text_rating_desc']  = 'Rating (Lowest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_save']   = 'You must save changes!';
//$_['text_select_all']   = 'select/unselect all';
$_['text_browse']              = 'Browse Files';
$_['text_clear']               = 'Clear Image';
$_['text_image_manager']       = 'Image Manager';
$_['text_xls_pricelist']       = 'Generate pricelist';
$_['text_xls_success']         = 'Successfull';
// Entry
$_['entry_nodubles']        = '<span title="It is useful, if product is placed more than in one category."  data-toggle="tooltip">Show products only in main category</span>';
$_['entry_logo_dimensions'] = '<span title="50х50px by default" data-toggle="tooltip">Logo image size</span>';
$_['entry_model']        = 'Model';
$_['entry_name']        = 'Name';
$_['entry_stock']        = 'In stock';
$_['entry_price']        = 'Price';
$_['entry_special']        = 'Special';
$_['entry_dimensions']            = '<span title="select with experimental" data-toggle="tooltip">Column width:</span>';
$_['entry_use_code']            = 'Show Model column:';
$_['entry_code']            = 'Model:';
$_['entry_use_special']            = 'Show specials column:';
$_['entry_memcache_warning']     = '* You must have a memcache server running, and have enabled memcache for your PHP to use this option';
$_['entry_use_cache']            = '<span title="Use it when you get memory limitation problems" data-toggle="tooltip">Use cache:</span>';
$_['entry_store']            = 'Stores:';
$_['entry_sort_order']    = 'Products sort order:';
$_['entry_category'] = 'Categories for show in price:';
$_['entry_use_image'] = 'Product thumbnails:';
$_['entry_use_options'] = 'Show products with options:';
$_['entry_attribute_groups'] = 'Atttribute groups:';
$_['entry_for_all'] = '<h2>Settings, similar for all languages</h2>';
$_['entry_use_attributes'] = '<span title="It takes effect, if at least one of attribute groups is selected." data-toggle="tooltip">Show attribute group names:</span>';
$_['entry_image_dimensions'] = '<span title="It takes effect, when \'Product thumbnails\' parameter is on<br>50х50px by default" data-toggle="tooltip">Product thumbnail size:</span>';
$_['entry_title'] = '<span title="Used shop name by default" data-toggle="tooltip">Tittle:</span>';
$_['entry_adress'] = '<span title="Used shop address by default" data-toggle="tooltip">Address:</span>';
$_['entry_phone'] = '<span title="Used shop phone by default" data-toggle="tooltip">Phone:</span>';
$_['entry_email'] = '<span title="Used shop e-mail by default" data-toggle="tooltip">E-mail:</span>';
$_['entry_link'] = '<span title="Used shop domain by default" data-toggle="tooltip">Text for url to index page:</span>';
$_['entry_custom_text'] = '<span title="Here you can provide your comments, bank details, etc." data-toggle="tooltip">Custom text:</span>';
$_['entry_currency'] = 'Price currency:';
$_['entry_logo'] = 'Logo image for price:';
$_['entry_customer_group'] = 'Generate price for customer groups:';
$_['entry_text_list'] = '<span title="Customer group name used by default.<br>It takes effect, if at least one customer group is selected." data-toggle="tooltip">Worksheet name:</span>';
$_['entry_view'] = '<span title="Attention! If you have a lot of products and high customer traffic - this option must be disabled!" data-toggle="tooltip">Generate price, when wisit a <b>%s</b> link</span>';
$_['entry_use_quantity']    = 'Show numeric product quantity:';
$_['entry_use_notinstock']    = 'Show products, that not in stock:';
$_['entry_use_collapse']    = '<span class="Not recommended, if \'Product thumbnails\' is set to \'Yes\'." data-toggle="tooltip">Compact view:</span>';
$_['entry_use_protection']    = '<span title="Should not use, if \'Compact view\' is set to \'Yes\'." data-toggle="tooltip">Protect Sheet from editing:</span>';
$_['entry_use_password']    = 'Protection password:';
$_['entry_colors']    = '<h2>Colors customization:</h2>';
$_['entry_color_text']    = 'text:';
$_['entry_color_bg']    = 'background:';
$_['entry_thead_color']    = 'Table head:';
$_['entry_underthead_color']    = 'Table underhead:';
$_['entry_category0_color']    = 'Category top level:';
$_['entry_category1_color']    = 'Category level=1:';
$_['entry_category2_color']    = 'Category level>=2:';
$_['entry_image_color']		= 'Product image';
$_['entry_model_color']		= 'Product model';
$_['entry_name_color']		= 'Product name';
$_['entry_stock_color']		= 'Product stock';
$_['entry_price_color']		= 'Product price';
$_['entry_special_color']	= 'Product special';
$_['entry_category_link']	= 'Open page on category click';
$_['entry_product_link']	= 'Open page on product name click';
$_['entry_alignment']	= 'Alignment';

$_['text_left']	= 'left';
$_['text_right']	= 'right';
$_['text_center']	= 'center';

$_['entry_poles']		= '<span title="Add fields to show in pricelist" data-toggle="tooltip">Fields:</span>';
$_['entry_pole_type']		= 'Field:';
$_['entry_pole_name']		= 'Name:';
// Error
$_['error_permission']    = 'Warning: You do not have permission to modify XLS Price!';
$_['error_xls_pricelist_store']    = 'At least one store must be selected!';
$_['error_xls_pricelist_category']    = 'At least one category must be selected!';
$_['error_xls_pricelist_customer_group']    = 'At least one customer group must be selected!';
$_['error_xls_pricelist_dimensions']    = 'Image size cannot be large than 200х200px !';

$_['button_view']    = 'Preview';
$_['button_add']    = 'Add field';
?>