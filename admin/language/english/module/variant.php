<?php
// Heading
$_['heading_title']              = '<span style="color:#597AB2;font-weight:700;">"МАГАЗИН"</span> варианты';

// Text
$_['text_success']               = 'Настройки успешно изменены!';
$_['text_list']                  = 'Список вариантов';
$_['text_add']                   = 'Добавить вариант';
$_['text_edit']                  = 'Редактировать модуль';

// Column
$_['column_variant']             = 'Вариант';
$_['column_action']              = 'Действие';

// Entry
$_['entry_status']               = 'Статус';
$_['entry_description']          = 'Описание варианта';
$_['entry_code']        	     = 'Код варианта';

$_['entry_related_variant_type'] = 'Группа варианотов';
$_['entry_related_variant_name'] = 'Товары для варианта';
$_['entry_related_variant']      = 'Товары для варианта';

$_['help_related_variant_name']  = 'Название для варианта (например: цвет)';
$_['help_related_variant']       = 'Выберите продукты для этого варианта.';

// Error
$_['error_permission']           = 'У Вас нет прав для управления данным модулем!';
