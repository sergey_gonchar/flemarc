<?php
// Heading
$_['heading_title']      = 'Partita IVA del Negozio';
$_['piva_title']  		 = 'Partita IVA del Negozio';

// Text
$_['text_edit']  		 = 'Modifica modulo Partita IVA del Negozio';
$_['piva_description']		= '';
$_['piva_success_text']		= 'Il modulo Partita IVA del Negozio &egrave; stato modificato con successo!';
$_['piva_save']				= 'Salva';
$_['piva_cancel']			= 'Annulla';
$_['text_module'] 	    	= 'Moduli';
$_['piva_yes']				= 'Si';
$_['piva_no']			    = 'No';

// Entry
$_['piva_entry_required']	= 'Partita IVA richiesta';
$_['piva_entry_required_description'] 	= '&Egrave; obbligatorio l\'inserimento della Partita IVA?';
$_['piva_entry_length'] 	= 'Lunghezza Partita IVA';
$_['piva_entry_length_description'] 	= 'La lunghezza viene verificata in fase di salvataggio dei dati del negozio.';

// Error
$_['piva_error_access'] = 'Attenzione: non hai il permesso di modificare il modulo Partita IVA del Negozio.';
?>
