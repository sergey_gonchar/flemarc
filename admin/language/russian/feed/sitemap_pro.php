<?php
// *	@copyright	modules.pro

// Heading
$_['heading_title']    = 'Sitemap Pro';

// Text
$_['text_feed']        = 'Каналы продвижения';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактирование';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_data_feed']  = 'Google feed';
$_['entry_data_yandex_feed']  = 'Yandex feed';

// Error
$_['error_permission'] = 'У вас недостаточно прав для внесения изменений!';