<?php
// Heading
$_['heading_title']    = '<span style="color:#597AB2;font-weight:700;">"МАГАЗИН"</span> новости';

$_['text_module']      = 'Модули';
$_['text_success']     = 'Настройки успешно изменены!';
$_['text_edit']        = 'Редактировать Новости модуль';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission'] = 'У вас нет прав для изменения данного модуля!';