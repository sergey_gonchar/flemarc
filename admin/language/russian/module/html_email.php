<?php
// Heading
$_['heading_title']       		   = 'HTML Email';

// Tab
$_['tab_template']           	   = 'Email Template Settings';
$_['tab_email_page']               = 'Page';
$_['tab_email_top_bar']            = 'Top Bar';
$_['tab_email_header']             = 'Header';
$_['tab_email_featured']           = 'Featured';
$_['tab_email_main']               = 'Main Message';
$_['tab_email_footer']             = 'Footer';
$_['tab_email_subfooter']          = 'Sub-Footer';
$_['tab_language_settings']        = 'Language Settings';
$_['tab_help']           		   = 'Help';

// Button
$_['button_send_test']             = 'Send Test Email';
$_['button_send_now']              = 'Send Now';

// Text
$_['text_module']         		   = 'Modules';
$_['text_success']        		   = 'Success: You have modified module HTML Email!';
$_['text_edit']        		       = 'Edit HTML Email';

$_['text_test_email']              = 'Send Test Email';
$_['text_subject']                 = '[Test] HTML Email Extension - Test mail';
$_['text_success_test_email']      = 'Success: Test mail was sent.';

// Entry
$_['entry_background_color']       = 'Background Color';
$_['entry_text_color']             = 'Text Color';
$_['entry_link_color']             = 'Link Color';
$_['entry_table_border_color']     = 'Table Border Color';
$_['entry_table_header_bg']        = 'Table Header Background Color';
$_['entry_table_header_text_color']= 'Table Header Text Color';
$_['entry_table_body_bg']          = 'Table Body Background Color';
$_['entry_table_body_text_color']  = 'Table Body Text Color';
$_['entry_logo']                   = 'Logo';
$_['entry_logo_width']             = 'Logo width';
$_['entry_logo_height']            = 'Logo height';
$_['entry_icon']                   = 'Icon';
$_['entry_link']                   = 'Link';
$_['entry_show_featured']          = 'Show Featured';
$_['entry_show_footer']            = 'Show Footer';

$_['entry_view_store']             = 'View Store Text';
$_['entry_contact_us']             = 'Contact Us Text';
$_['entry_featured_title']         = 'Featured title';
$_['entry_featured_description']   = 'Featured description';
$_['entry_featured_link']          = 'Featured Link';
$_['entry_featured_find_more_info']= 'Find More Info Text';
$_['entry_my_account']             = 'My account';
$_['entry_footer_description']     = 'Footer description';
$_['entry_copyright']              = 'Copyright';

$_['entry_email']                  = 'Email address';

// Legend
$_['legend_logo']                  = 'Logo Image & dimensions';
$_['legend_facebook']              = 'Facebook';
$_['legend_twitter']               = 'Twitter';
$_['legend_youtube']               = 'Youtube';

// Help
$_['help_choose_color']            = 'Click inside input and choose color';
$_['help_layout_preview_title']    = 'LAYOUT PREVIEW';
$_['help_layout_preview_info']     = 'This only helps you to choose colors. Use \'Send test email\' to see exactly how looks your email template';
$_['help_logo_dimension']          = 'Use this option to resize logo ONLY if logo size is bigger than size you want.<br />Leave blank = logo is loaded at full size';
$_['help_px']                      = 'px';
$_['help_facebook_link']           = 'Full link to your fan page<br />Ex: http://www.facebook.com/ocextensions';
$_['help_twitter_link']            = 'Full link to your twitter page<br />Ex: https://twitter.com/ocextensionscom';
$_['help_youtube_link']            = 'Full link to your channel<br />Ex: http://www.youtube.com/channel/UC-g1jC0H_HAKS-RPOY2aqtQ';
$_['help_32x32']                   = '32 x 32 pixels';
$_['help_show_featured']           = 'If is set to Disabled will hide \'FEATURED/PROMOTED MESSAGE AREA\' (see layout preview to identify area)';
$_['help_show_footer']             = 'If is set to Disabled will hide \'FOOTER AREA\' (see layout preview to identify area)';
$_['help_featured_title']          = 'Title for \'FEATURED/PROMOTED MESSAGE AREA\' (see layout preview to identify area)';
$_['help_featured_description']    = 'Description for \'FEATURED/PROMOTED MESSAGE AREA\' (see layout preview to identify area)';
$_['help_featured_link']           = 'Add link to page where customer can find more info<br />ex: http://www.oc-extensions.com <br />required for all languages because some store use multi language keyworkds => different link for each language';
$_['help_featured_find_more_info'] = 'Translate words \'Find more info\' for current language';
$_['help_my_account']              = 'Translate words \'My Account\' for current language';
$_['help_footer_description']      = 'Here you can add your store info or other message';
$_['help_view_store']              = 'Translate words \'View Store\' for current language';
$_['help_contact_us']              = 'Translate words \'Contact Us\' for current language';
$_['help_copyright']               = 'Ex: Copyright &copy; 2015 - Your store All rights reserved';   
$_['help_email']                   = 'For multiple addresses use separator comma. Ex: admin@yourstore.com, sales@yourstore.com, etc@yourstore.com';

// Error
$_['error_permission']    		   = 'Warning: You do not have permission to modify module HTML Email!';
$_['error_in_language']            = 'Found errors in tab \'Language Settings\'. Please fill required fields for all available languages';
$_['error_required']    	       = 'This field is required!';
$_['error_min_chars_required']     = 'This field require at least 20 chars';
$_['error_email_required']         = 'Email address is required! For multiple addresses use coma as separator. Ex: admin@yourstore.com, sales@yourstore.com, etc@yourstore.com';
$_['error_email_wrong_format']     = 'One (or more)E-Mail Addresses does not appear to be valid!';
?>