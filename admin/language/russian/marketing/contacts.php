<?php  
// Heading
$_['heading_title']         = 'Новая Почта';

$_['start_send']            = 'Рассылка начата.';
$_['end_send']              = 'Рассылка окончена.';
$_['count_email']           = 'Всего к отправке %s писем.';
$_['send_email']            = 'Отправка на: ';
$_['bad_email']             = 'Неправильный email: ';

// Text
$_['text_success']             = 'Все сообщения отправлены!';
$_['text_success_setting']     = 'Настройки сохранены!';
$_['text_sent']                = 'Ваше сообщение успешно отправлено <span style="color:red;">%s</span> из <span style="color:red;">%s</span> получателей! <span style="color:red;">Не закрывайте страницу до окончания рассылки!</span>';
$_['text_default']             = 'По умолчанию';
$_['text_newsletter']          = 'Подписчикам на новости';
$_['text_customer_all']        = 'Всем зарегистрированным';
$_['text_customer_group']      = 'Группе покупателей';
$_['text_customer']            = 'Выбранным Покупателям';
$_['text_client_all']          = 'Всем покупателям';
$_['text_affiliate_all']       = 'Всем партнёрам';
$_['text_affiliate']           = 'Выбранным Партнёрам';
$_['text_product']             = 'Купившим товары';
$_['text_manual']              = 'Произвольный список';
$_['text_save']                = 'Сохранить';
$_['text_save_template']       = 'Сохранить как шаблон';
$_['text_new_template']        = 'Добавить новый';
$_['text_template_name']       = 'Название шаблона';
$_['text_success_template']    = 'Шаблон сохранен!';
$_['text_delete_template']     = 'Шаблон удален!';
$_['text_unsubscribe']         = 'Вы можете отписаться от рассылки, перейдя по ссылке: <a href="%s">Отписаться</a>';
$_['text_wait']                = 'Ждите! Идет сбор данных!';

$_['text_no_lic']              = 'Модуль не активирован! Функциональность ограниченна!';
$_['text_ok_lic']              = 'Успешная активация! Спасибо за покупку!';
$_['text_error_lic']           = 'Ошибка активации! Обратитесь к разработчику: shchs@ya.ru';
$_['button_bta']               = 'Активировать';

$_['text_tegi']                = 'Справка по тегам:';
$_['spravka_tegi']             = 'Возможно использовать следующие спец.теги:<br /><span>{name}</span>-  Имя Фамилия (или что найдет из них, если оба отсутствуют, то "Клиент")<br /><span>{firstname}</span>-  Имя (если не найдет, то удалит тег)<br /><span>{lastname}</span>-  Фамилия (если не найдет, то удалит тег)<br /><span>{shopname}</span>-  Название магазина (из настроек)<br /><span>{shopurl}</span>-  Название магазина в виде ссылки (на главную страницу)<br /><span>{country}</span>-  Страна (из данных регистрации покупателя; если рассылка "Всем покупателям" или "Купившим товары", то из адреса доставки)<br /><span>{zone}</span>-  Регион (из данных регистрации покупателя, если рассылка "Всем покупателям" или "Купившим товары", то из адреса доставки)<br /><span>{special}</span>-  Товары с акцией (если не найдет, то удалит тег)<br /><span>{bestseller}</span>-  Хиты продаж (если не найдет, то удалит тег)<br /><span>{latest}</span>-  Последние (если не найдет, то удалит тег)<br /><span>{featured}</span>-  Рекомендуемые товары (из стандартного модуля "Рекомендуемые", если не найдет, то удалит тег)<br /><span>{selproducts}</span>-  Выбранные товары (если не найдет, то удалит тег)';

$_['text_mail']                = 'Mail';
$_['text_smtp']                = 'SMTP';

$_['tab_send']                = 'Отправка';
$_['tab_template']            = 'Шаблоны';
$_['tab_newsletter']          = 'Подписчики';
$_['tab_newsgroup']           = 'Группы рассылки';
$_['tab_log']                 = 'Лог отправки';
$_['tab_setting']             = 'Настройки';

// Entry
$_['entry_store']              = 'От:';
$_['entry_to']                 = 'Кому:';
$_['entry_customer_group']     = 'Группа покупателя:';
$_['entry_customer']           = 'Покупатель:';
$_['help_customer']            = 'Autocomplete';
$_['entry_affiliate']          = 'Партнёр:';
$_['help_affiliate']           = 'Autocomplete';
$_['entry_product']            = 'Товары:';
$_['help_product']             = 'Отправить покупателям, которые уже заказали товары из списка.';
$_['entry_manual']             = 'E-mails:';
$_['help_manual']              = 'Введите адреса через запятую.';
$_['entry_subject']            = 'Тема:';
$_['help_subject']             = 'Разрешенные теги:<br />{name},{firstname},{lastname},{shopname},{country},{zone}';
$_['entry_message']            = 'Сообщение:';
$_['help_message']             = 'Разрешенные теги:<br />{name},{firstname},{lastname},{shopname},{country},{zone},{shopurl},{special},{bestseller},{latest},{featured},{selproducts}';
$_['entry_template']           = 'Загрузить шаблон:';

$_['entry_mail_count_message'] = 'Количество писем за одну отправку:';
$_['entry_mail_sleep_time']    = 'Задержка отправки в секундах:';
$_['entry_mail_from']          = 'E-mail адреса отправителя:';
$_['help_from']                = 'Указывать через запятую. Если не указаны, то адрес берется из настроек магазина.';
$_['entry_mail_protocol']      = 'Почтовый протокол:';
$_['help_protocol']            = 'Выбирайте "Mail", и только в случае, когда этот способ не работает &mdash; SMTP.';
$_['entry_mail_parameter']     = 'Параметры функции почты:';
$_['help_parameter']           = 'ОСТОРОЖНО. Не заполняйте поле, если не знаете, для чего оно. Когда используется "Mail", здесь могут быть указаны дополнительные параметры для sendmail (напр. "-femail@storeaddress.com").';
$_['entry_smtp_host']          = 'SMTP хост:';
$_['entry_smtp_username']      = 'SMTP логин:';
$_['entry_smtp_password']      = 'SMTP пароль:';
$_['entry_smtp_port']          = 'SMTP порт:';
$_['entry_smtp_timeout']       = 'SMTP таймаут:';
$_['entry_product_currency']   = 'Валюта товаров:';

$_['column_action']            = 'Действие';
$_['column_template_name']     = 'Название шаблона';
$_['text_view']                = 'Открыть';
$_['text_delete']              = 'Удалить';
$_['text_no_template']         = 'Нет сохраненных шаблонов!';

$_['text_region']          = 'Учитывать адрес';
$_['entry_country']        = 'Страна';
$_['entry_zone']           = 'Регион';
$_['entry_unsubscribe']    = 'Добавить ссылку "отписаться"';
$_['text_client']          = 'Клиент';

$_['button_clearlog']          = 'Очистить';
$_['text_delete_log']          = 'Лог удален!';
$_['text_update_log']          = 'Лог обновлен!';
$_['button_updatelog']         = 'Обновить';

$_['entry_insert_products']    = 'Прикрепить товары';
$_['entry_special']            = 'Товары с акцией';
$_['entry_bestseller']         = 'Хиты продаж';
$_['entry_featured']           = 'Рекомендуемые';
$_['entry_latest']             = 'Последние';
$_['entry_selected']           = 'Выбранные товары';
$_['entry_pselected']          = 'Укажите товары';
$_['help_selproduct']          = 'Autocomplete';
$_['entry_title']         = 'Заголовок';
$_['entry_limit']         = 'Кол-во';

$_['featured_title']        = 'Рекомендуем';
$_['bestseller_title']      = 'Хиты продаж';
$_['special_title']         = 'Специальное предложение';
$_['latest_title']          = 'Последнее поступление';
$_['selproducts_title']       = 'Обратите внимание';
$_['category_products_title'] = 'Наши продукты';

// Error
$_['error_permission']         = 'У Вас нет прав для изменения сообщений!';
$_['error_save_setting']       = 'У Вас нет прав для сохранения настроек!';
$_['error_save_template']      = 'Не удалось сохранить шаблон!';
$_['error_del_template']       = 'Не удалось удалить шаблон!';
$_['error_name_template']      = 'Укажите название шаблона!';
$_['error_subject']            = 'Необходима тема письма!';
$_['error_message']            = 'Необходим текст сообщения!';
$_['editor_mode_alert']        = 'Переключите редактор в визуальный режим!';
$_['error_close']              = 'Не закрывайте страницу если запущенна рассылка!\nЭто приведет к её прерыванию!\n\n';
?>