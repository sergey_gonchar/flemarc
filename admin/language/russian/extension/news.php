<?php
// Heading 
$_['heading_title']     		= 'Новости';

// Text
$_['text_list']     		    = 'Список новостей';
$_['text_image']				= 'Изображение';
$_['text_title']				= 'Заголовок';
$_['text_description']			= 'Полное описание';
$_['text_short_description']	= 'Краткое описание';
$_['text_date']					= 'Дата добавления';
$_['text_action']				= 'Действие';
$_['text_status']				= 'Статус';
$_['text_keyword']				= 'SEO Keyword';
$_['text_no_results']			= 'Пусто.';
$_['text_browse']				= 'Обзор';
$_['text_clear']				= 'Очистить';
$_['text_image_manager']		= 'Загрузить изобрадение';
$_['text_success']				= 'Настройки успешно изменены!';
$_['text_confirm']				= 'Вы действительно хотите удалить модуль?';
// Error
$_['error_permission'] 			= 'У Вас нет прав для управления данным модулем!';
$_['error_title']               = 'Заголовок должен быть заполнен!';
$_['error_short_description']   = 'Короткое описание должно быть заполнено!';
?>