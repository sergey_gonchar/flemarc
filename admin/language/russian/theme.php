<?php
// Text
$_['text_column_homel']           = '2/3';
$_['text_column_homer']           = '1/3';
$_['text_content_slide']          = 'Верх на всю ширину';
$_['text_content_footer']         = 'Низ на всю ширину';

// subscription
$_['url_subscription']            = 'Подписчики';

//variant
$_['url_variant']                 = 'Варианты';
$_['url_news']                    = 'Создать новость';

$_['entry_related_variant_name']  = 'Группа вариантов';
$_['entry_related_variant_type']  = 'Группа вариантов';
$_['entry_related_variant']       = 'Товары для варианта';
$_['help_related_variant_name']   = 'Название для варианта (например: цвет)';
$_['help_related_variant']        = 'Продукты для варианта.';
$_['tab_variant']          		  = 'Вариант';
$_['text_variant']          	  = 'Варианты';


//size
$_['url_size']                    = 'Создать таблицу';
$_['entry_size_table']            = 'Таблица размеров';

$_['url_callme']                  = 'Перезвонить';
$_['url_bannermag']               = 'Создать группу слайдов';