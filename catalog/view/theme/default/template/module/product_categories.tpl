<?php if ($status_google_font) { ?>
<link href="//fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Kelly+Slab&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Jura&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Russo+One&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Exo&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Yeseva+One&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Stalinist+One&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Ubuntu&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<link href="//fonts.googleapis.com/css?family=Comfortaa&subset=latin,cyrillic,latin,cyrillic" rel="stylesheet" type="text/css" />
<?php } ?>
<style type="text/css">
.product-module-box-<?php echo $id; ?> {
	position: relative;
	background-color: <?php echo $bg_box_color; ?>;
	<?php if ($module_image_box) { ?>background: url('image/<?php echo $module_image_box; ?>') center center repeat;<?php } ?>
	border: <?php echo $box_border_weight; ?>px <?php echo $box_border_style; ?> <?php echo $box_border_color; ?>;
	margin: <?php echo $box_margin_top; ?>px <?php echo $box_margin_right; ?>px <?php echo $box_margin_bottom; ?>px <?php echo $box_margin_left; ?>px;
}
.product-module-box-<?php echo $id; ?> .banner-image {
	height: 350px;
	float: left;
}
.product-module-box-<?php echo $id; ?> .product-module-box-<?php echo $id; ?>-heading {
	background-color: <?php echo $bg_title_top; ?>;
	background-image: linear-gradient(to bottom, <?php echo $bg_title_top; ?> 0%, <?php echo $bg_title_bottom; ?> 100%);
	background-repeat: repeat-x;
	<?php if ($image_header) { ?>background: url('image/<?php echo $image_header; ?>') center center repeat;<?php } ?>
	padding: <?php echo $title_padding_top; ?>px <?php echo $title_padding_right; ?>px <?php echo $title_padding_bottom; ?>px <?php echo $title_padding_left; ?>px;
	font-family: '<?php echo $title_text_font; ?>', Arial, sans-serif;
	color: <?php echo $title_text_color; ?>;
	text-shadow: 0 1px 0 <?php echo $title_text_shadow; ?>;
	font-size: <?php echo $title_font_size; ?>px;
	font-weight: <?php echo $title_font_weight; ?>;
	font-style: <?php echo $title_font_style; ?>;
	text-transform: <?php echo $title_text_transform; ?>;
}
.product-module-box-<?php echo $id; ?> .product-module-column-left-<?php echo $id; ?> {
	float: left;
	height: 350px;
	border-right: <?php echo $tabs_border_weight; ?>px <?php echo $tabs_border_style; ?> <?php echo $tabs_border_color; ?>;
}
.product-module-box-<?php echo $id; ?> .vtabs-<?php echo $id; ?> {
	width: <?php echo $tabs_width; ?>px;
	padding-top: <?php echo $tabs_padding_top; ?>px;
	display: block;
}
.product-module-box-<?php echo $id; ?> .vtabs-<?php echo $id; ?> a {
	position: relative;
	display: block;
	text-align: left;
	text-decoration: none;
	font-family: '<?php echo $tabs_text_font; ?>', Arial, sans-serif;
	color: <?php echo $tabs_text_color; ?>;
	text-shadow: 0 1px 0 <?php echo $tabs_text_shadow; ?>;
	font-size: <?php echo $tabs_font_size; ?>px;
	font-weight: <?php echo $tabs_font_weight; ?>;
	font-style: <?php echo $tabs_font_style; ?>;
	margin: <?php echo $tabs_margin_top; ?>px <?php echo $tabs_margin_right; ?>px <?php echo $tabs_margin_bottom; ?>px <?php echo $tabs_margin_left; ?>px;
	padding: <?php echo $tabs_padding_top; ?>px <?php echo $tabs_padding_right; ?>px <?php echo $tabs_padding_bottom; ?>px <?php echo $tabs_padding_left; ?>px;
	z-index: 1;
}
.product-module-box-<?php echo $id; ?> .vtabs-<?php echo $id; ?> a.selected {
	margin-right: -<?php echo $tabs_border_weight; ?>px;
	background-color: <?php echo $bg_box_color_tabs; ?>;
	<?php if ($image_bg_tabs) { ?>background: url('image/<?php echo $image_bg_tabs; ?>') center center repeat;<?php } ?>
	font-family: '<?php echo $tabs_active_text_font; ?>', Arial, sans-serif;
	color: <?php echo $tabs_active_text_color; ?>;
	text-shadow: 0 1px 0 <?php echo $tabs_active_text_shadow; ?>;
	font-size: <?php echo $tabs_active_font_size; ?>px;
	font-weight: <?php echo $tabs_active_font_weight; ?>;
	font-style: <?php echo $tabs_active_font_style; ?>;
	padding: <?php echo $tabs_active_padding_top; ?>px <?php echo $tabs_active_padding_right; ?>px <?php echo $tabs_active_padding_bottom; ?>px <?php echo $tabs_active_padding_left; ?>px;
	border-left: <?php echo $tabs_border_weight; ?>px <?php echo $tabs_border_style; ?> <?php echo $tabs_border_color; ?>;
	border-top: <?php echo $tabs_border_weight; ?>px <?php echo $tabs_border_style; ?> <?php echo $tabs_border_color; ?>;
	border-right: <?php echo $tabs_border_weight; ?>px solid <?php echo $bg_box_color_tabs; ?>;
	border-bottom: <?php echo $tabs_border_weight; ?>px <?php echo $tabs_border_style; ?> <?php echo $tabs_border_color; ?>;
}
.product-module-box-<?php echo $id; ?> .vtabs-content-<?php echo $id; ?> {
	padding: 0px;
	margin: 0px;
	height: 350px;
	overflow: hidden;
}
.product-module-box-<?php echo $id; ?> .product-module-box-<?php echo $id; ?>-content {
	background-color: <?php echo $bg_box_color_tabs; ?>;
	<?php if ($image_bg_tabs) { ?>background: url('image/<?php echo $image_bg_tabs; ?>') center center repeat;<?php } ?>
}
.product-module-box-<?php echo $id; ?>-content div.boxes-product-<?php echo $id; ?> {
	position: relative;
	height: <?php echo $box_product_height; ?>px;
	text-align: center;
	background-color: <?php echo $bg_box_product_color; ?>;
	<?php if ($image_bg_box_product) { ?>background: url('image/<?php echo $image_bg_box_product; ?>') center center repeat;<?php } ?>
	border: <?php echo $box_product_border_weight; ?>px <?php echo $box_product_border_style; ?> <?php echo $box_product_border_color; ?>;
	margin: <?php echo $box_product_margin_top; ?>px <?php echo $box_product_margin_right; ?>px <?php echo $box_product_margin_bottom; ?>px <?php echo $box_product_margin_left; ?>px;
	padding: <?php echo $box_product_padding_top; ?>px <?php echo $box_product_padding_right; ?>px <?php echo $box_product_padding_bottom; ?>px <?php echo $box_product_padding_left; ?>px;
	-webkit-transition: all 0.3s linear;
	-moz-transition: all 0.3s linear;
	-o-transition: all 0.3s linear;
	-ms-transition: all 0.3s linear;
	transition: all 0.3s linear;
}
.product-module-box-<?php echo $id; ?>-content div.boxes-product-<?php echo $id; ?>:hover {
	background-color: <?php echo $bg_box_product_hover_color; ?>;
	<?php if ($image_bg_box_product_hover) { ?>background: url('image/<?php echo $image_bg_box_product_hover; ?>') center center repeat;<?php } ?>
	border: <?php echo $box_product_hover_border_weight; ?>px <?php echo $box_product_hover_border_style; ?> <?php echo $box_product_hover_border_color; ?>;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> img {
	margin-left: auto;
	margin-right: auto;
}
.product-module-box-<?php echo $id; ?>-content .name-<?php echo $id; ?> {
	padding: <?php echo $box_product_name_padding_top; ?>px <?php echo $box_product_name_padding_right; ?>px <?php echo $box_product_name_padding_bottom; ?>px <?php echo $box_product_name_padding_left; ?>px;
}
.product-module-box-<?php echo $id; ?>-content .name-<?php echo $id; ?> a {
	font-family: '<?php echo $box_product_name_text_font; ?>', Arial, sans-serif;
	color: <?php echo $box_product_name_text_color; ?>;
	<?php if ($box_product_name_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_name_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $box_product_name_font_size; ?>px;
	font-weight: <?php echo $box_product_name_font_weight; ?>;
	font-style: <?php echo $box_product_name_font_style; ?>;
	-webkit-transition: all 0.3s linear;
	-moz-transition: all 0.3s linear;
	-o-transition: all 0.3s linear;
	-ms-transition: all 0.3s linear;
	transition: all 0.3s linear;
}
.product-module-box-<?php echo $id; ?>-content .name-<?php echo $id; ?> a:hover {
	font-family: '<?php echo $box_product_name_hover_text_font; ?>', Arial, sans-serif;
	color: <?php echo $box_product_name_hover_text_color; ?>;
	<?php if ($box_product_name_hover_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_name_hover_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $box_product_name_hover_font_size; ?>px;
	font-weight: <?php echo $box_product_name_hover_font_weight; ?>;
	font-style: <?php echo $box_product_name_hover_font_style; ?>;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .rating {
	padding-bottom: 5px;
	text-align: center;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .rating .fa-stack {
	font-size: 6px;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .rating .fa-star-o {
	color: #999;
	font-size: 12px;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .rating .fa-star {
	color: #F00;
	font-size: 12px;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .rating .fa-star + .fa-star-o {
	color: #8f0606;
}
.product-module-box-<?php echo $id; ?>-content .price {
	padding: <?php echo $box_product_price_padding_top; ?>px <?php echo $box_product_price_padding_right; ?>px <?php echo $box_product_price_padding_bottom; ?>px <?php echo $box_product_price_padding_left; ?>px;
	font-family: '<?php echo $box_product_price_text_font; ?>', Arial, sans-serif;
	color: <?php echo $box_product_price_text_color; ?>;
	<?php if ($box_product_price_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_price_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $box_product_price_font_size; ?>px;
	font-weight: <?php echo $box_product_price_font_weight; ?>;
	font-style: <?php echo $box_product_price_font_style; ?>;
	overflow: hidden;
}
.product-module-box-<?php echo $id; ?>-content .price-old {
	font-family: '<?php echo $box_product_price_old_text_font; ?>', Arial, sans-serif;
	color: <?php echo $box_product_price_old_text_color; ?>;
	<?php if ($box_product_price_old_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_price_old_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $box_product_price_old_font_size; ?>px;
	font-weight: <?php echo $box_product_price_old_font_weight; ?>;
	font-style: <?php echo $box_product_price_old_font_style; ?>;
	text-decoration: line-through;
	margin-left: 10px;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .button-cart-<?php echo $id; ?> {
	position: absolute;
	width: 95%;
	text-align: center;
	bottom: <?php echo $cart_margin_bottom; ?>px;
}
.product-module-box-<?php echo $id; ?>-content .button-cart-<?php echo $id; ?> a {
	background-color: <?php echo $box_product_cart_bg_color_top; ?>;
	<?php if ($box_product_cart_image) { ?>background: url('image/<?php echo $box_product_cart_image; ?>') center center repeat;<?php } ?>
	padding: <?php echo $box_product_cart_padding_top; ?>px <?php echo $box_product_cart_padding_right; ?>px <?php echo $box_product_cart_padding_bottom; ?>px <?php echo $box_product_cart_padding_left; ?>px;
	font-family: '<?php echo $box_product_cart_text_font; ?>', Arial, sans-serif;
	color: <?php echo $box_product_cart_text_color; ?>;
	<?php if ($box_product_cart_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_cart_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $box_product_cart_font_size; ?>px;
	font-weight: <?php echo $box_product_cart_font_weight; ?>;
	font-style: <?php echo $box_product_cart_font_style; ?>;
	border: <?php echo $box_product_cart_border_weight; ?>px <?php echo $box_product_cart_border_style; ?> <?php echo $box_product_cart_border_color; ?>;
	-webkit-border-radius: <?php echo $cart_border_radius_left_top; ?>px <?php echo $cart_border_radius_right_top; ?>px <?php echo $cart_border_radius_right_bottom; ?>px <?php echo $cart_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $cart_border_radius_left_top; ?>px <?php echo $cart_border_radius_right_top; ?>px <?php echo $cart_border_radius_right_bottom; ?>px <?php echo $cart_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $cart_border_radius_left_top; ?>px <?php echo $cart_border_radius_right_top; ?>px <?php echo $cart_border_radius_right_bottom; ?>px <?php echo $cart_border_radius_left_bottom; ?>px;
	cursor: pointer;	
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?>:hover .button-cart-<?php echo $id; ?> a {
	background-color: <?php echo $box_product_cart_hover_bg_color_top; ?>;
	<?php if ($box_product_cart_hover_image) { ?>background: url('image/<?php echo $box_product_cart_hover_image; ?>') center center repeat;<?php } ?>
	font-family: '<?php echo $box_product_cart_hover_text_font; ?>', Arial, sans-serif;
	color: <?php echo $box_product_cart_hover_text_color; ?>;
	<?php if ($box_product_cart_hover_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_cart_hover_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $box_product_cart_hover_font_size; ?>px;
	font-weight: <?php echo $box_product_cart_hover_font_weight; ?>;
	font-style: <?php echo $box_product_cart_hover_font_style; ?>;
	border: <?php echo $box_product_cart_hover_border_weight; ?>px <?php echo $box_product_cart_hover_border_style; ?> <?php echo $box_product_cart_hover_border_color; ?>;
	-webkit-border-radius: <?php echo $cart_hover_border_radius_left_top; ?>px <?php echo $cart_hover_border_radius_right_top; ?>px <?php echo $cart_hover_border_radius_right_bottom; ?>px <?php echo $cart_hover_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $cart_hover_border_radius_left_top; ?>px <?php echo $cart_hover_border_radius_right_top; ?>px <?php echo $cart_hover_border_radius_right_bottom; ?>px <?php echo $cart_hover_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $cart_hover_border_radius_left_top; ?>px <?php echo $cart_hover_border_radius_right_top; ?>px <?php echo $cart_hover_border_radius_right_bottom; ?>px <?php echo $cart_hover_border_radius_left_bottom; ?>px;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-<?php echo $left_button; ?>-<?php echo $id; ?>,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-<?php echo $right_button; ?>-<?php echo $id; ?> {
	position: absolute;
	width: 30px;
	height: 30px;
	padding: 5px;
	top: 40%;
	margin: 18px 30px 0px;
	opacity: 0;
	filter: alpha(opacity=0);
	visibility: hidden;
	color: <?php echo $box_product_buttons_text_color; ?>;
	<?php if ($box_product_buttons_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_buttons_text_shadow; ?>;<?php } ?>
	background-color: <?php echo $box_product_buttons_bg_color; ?>;
	border: <?php echo $box_product_buttons_border_weight; ?>px <?php echo $box_product_buttons_border_style; ?> <?php echo $box_product_buttons_border_color; ?>;
	-webkit-border-radius: <?php echo $buttons_border_radius_left_top; ?>px <?php echo $buttons_border_radius_right_top; ?>px <?php echo $buttons_border_radius_right_bottom; ?>px <?php echo $buttons_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $buttons_border_radius_left_top; ?>px <?php echo $buttons_border_radius_right_top; ?>px <?php echo $buttons_border_radius_right_bottom; ?>px <?php echo $buttons_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $buttons_border_radius_left_top; ?>px <?php echo $buttons_border_radius_right_top; ?>px <?php echo $buttons_border_radius_right_bottom; ?>px <?php echo $buttons_border_radius_left_bottom; ?>px;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-<?php echo $center_button; ?>-<?php echo $id; ?> {
	position: absolute;
	width: 30px;
	height: 30px;
	padding: 5px;
	bottom: 30%;
	margin: 0px -16px 0px;
	opacity: 0;
	filter: alpha(opacity=0);
	visibility: hidden;
	color: <?php echo $box_product_buttons_text_color; ?>;
	<?php if ($box_product_buttons_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_buttons_text_shadow; ?>;<?php } ?>
	background-color: <?php echo $box_product_buttons_bg_color; ?>;
	border: <?php echo $box_product_buttons_border_weight; ?>px <?php echo $box_product_buttons_border_style; ?> <?php echo $box_product_buttons_border_color; ?>;
	-webkit-border-radius: <?php echo $buttons_border_radius_left_top; ?>px <?php echo $buttons_border_radius_right_top; ?>px <?php echo $buttons_border_radius_right_bottom; ?>px <?php echo $buttons_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $buttons_border_radius_left_top; ?>px <?php echo $buttons_border_radius_right_top; ?>px <?php echo $buttons_border_radius_right_bottom; ?>px <?php echo $buttons_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $buttons_border_radius_left_top; ?>px <?php echo $buttons_border_radius_right_top; ?>px <?php echo $buttons_border_radius_right_bottom; ?>px <?php echo $buttons_border_radius_left_bottom; ?>px;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?>:hover .module-button-<?php echo $center_button; ?>-<?php echo $id; ?> {
	margin: 0px -16px 60px;
	opacity: 1;
	filter: alpha(opacity=100);
	visibility: visible;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?>:hover .module-button-<?php echo $left_button; ?>-<?php echo $id; ?>,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?>:hover .module-button-<?php echo $right_button; ?>-<?php echo $id; ?> {
	margin: -18px 5px 0px;
	opacity: 1;
	filter: alpha(opacity=100);
	visibility: visible;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-<?php echo $left_button; ?>-<?php echo $id; ?> {
	right: 50%;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-<?php echo $right_button; ?>-<?php echo $id; ?> {
	left: 50%;
}
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-wishlist-<?php echo $id; ?>:hover,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-compare-<?php echo $id; ?>:hover,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-quick-view-<?php echo $id; ?>:hover {
	color: <?php echo $box_product_buttons_hover_text_color; ?>;
	<?php if ($box_product_buttons_hover_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $box_product_buttons_hover_text_shadow; ?>;<?php } ?>
	background-color: <?php echo $box_product_buttons_hover_bg_color; ?>;
	border: <?php echo $box_product_buttons_hover_border_weight; ?>px <?php echo $box_product_buttons_hover_border_style; ?> <?php echo $box_product_buttons_hover_border_color; ?>;
	-webkit-border-radius: <?php echo $buttons_hover_border_radius_left_top; ?>px <?php echo $buttons_hover_border_radius_right_top; ?>px <?php echo $buttons_hover_border_radius_right_bottom; ?>px <?php echo $buttons_hover_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $buttons_hover_border_radius_left_top; ?>px <?php echo $buttons_hover_border_radius_right_top; ?>px <?php echo $buttons_hover_border_radius_right_bottom; ?>px <?php echo $buttons_hover_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $buttons_hover_border_radius_left_top; ?>px <?php echo $buttons_hover_border_radius_right_top; ?>px <?php echo $buttons_hover_border_radius_right_bottom; ?>px <?php echo $buttons_hover_border_radius_left_bottom; ?>px;
}
.product-module-carousel .owl-buttons div i.btn-<?php echo $id; ?> {
	font-size: 15px !important;
	margin: -20px 0 0 !important;
	color: <?php echo $btn_carousel_text_color; ?>;
	<?php if ($btn_carousel_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $btn_carousel_text_shadow; ?> !important;<?php } ?>
	background-color: <?php echo $btn_carousel_bg_color; ?> !important;
	border: <?php echo $btn_carousel_border_weight; ?>px <?php echo $btn_carousel_border_style; ?> <?php echo $btn_carousel_border_color; ?> !important;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.product-module-carousel .owl-buttons .owl-prev i.btn-<?php echo $id; ?> {
	-webkit-border-radius: <?php echo $btn_carousel_l_border_radius_left_top; ?>px <?php echo $btn_carousel_l_border_radius_right_top; ?>px <?php echo $btn_carousel_l_border_radius_right_bottom; ?>px <?php echo $btn_carousel_l_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $btn_carousel_l_border_radius_left_top; ?>px <?php echo $btn_carousel_l_border_radius_right_top; ?>px <?php echo $btn_carousel_l_border_radius_right_bottom; ?>px <?php echo $btn_carousel_l_border_radius_left_bottom; ?>px;
	-o-border-radius: <?php echo $btn_carousel_l_border_radius_left_top; ?>px <?php echo $btn_carousel_l_border_radius_right_top; ?>px <?php echo $btn_carousel_l_border_radius_right_bottom; ?>px <?php echo $btn_carousel_l_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $btn_carousel_l_border_radius_left_top; ?>px <?php echo $btn_carousel_l_border_radius_right_top; ?>px <?php echo $btn_carousel_l_border_radius_right_bottom; ?>px <?php echo $btn_carousel_l_border_radius_left_bottom; ?>px;
	padding: 10px !important;
}
.product-module-carousel .owl-buttons .owl-next i.btn-<?php echo $id; ?> {
	-webkit-border-radius: <?php echo $btn_carousel_r_border_radius_left_top; ?>px <?php echo $btn_carousel_r_border_radius_right_top; ?>px <?php echo $btn_carousel_r_border_radius_right_bottom; ?>px <?php echo $btn_carousel_r_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $btn_carousel_r_border_radius_left_top; ?>px <?php echo $btn_carousel_r_border_radius_right_top; ?>px <?php echo $btn_carousel_r_border_radius_right_bottom; ?>px <?php echo $btn_carousel_r_border_radius_left_bottom; ?>px;
	-o-border-radius: <?php echo $btn_carousel_r_border_radius_left_top; ?>px <?php echo $btn_carousel_r_border_radius_right_top; ?>px <?php echo $btn_carousel_r_border_radius_right_bottom; ?>px <?php echo $btn_carousel_r_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $btn_carousel_r_border_radius_left_top; ?>px <?php echo $btn_carousel_r_border_radius_right_top; ?>px <?php echo $btn_carousel_r_border_radius_right_bottom; ?>px <?php echo $btn_carousel_r_border_radius_left_bottom; ?>px;
	padding: 10px !important;
}
.product-module-carousel .owl-buttons div:hover i.btn-<?php echo $id; ?> {
	color: <?php echo $btn_carousel_hover_text_color; ?>;
	<?php if ($btn_carousel_hover_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $btn_carousel_hover_text_shadow; ?> !important;<?php } ?>
	background-color: <?php echo $btn_carousel_hover_bg_color; ?> !important;
	border: <?php echo $btn_carousel_hover_border_weight; ?>px <?php echo $btn_carousel_hover_border_style; ?> <?php echo $btn_carousel_hover_border_color; ?> !important;
}
.product-module-carousel .owl-buttons .owl-prev:hover i.btn-<?php echo $id; ?> {
	-webkit-border-radius: <?php echo $btn_carousel_hover_l_border_radius_left_top; ?>px <?php echo $btn_carousel_hover_l_border_radius_right_top; ?>px <?php echo $btn_carousel_hover_l_border_radius_right_bottom; ?>px <?php echo $btn_carousel_hover_l_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $btn_carousel_hover_l_border_radius_left_top; ?>px <?php echo $btn_carousel_hover_l_border_radius_right_top; ?>px <?php echo $btn_carousel_hover_l_border_radius_right_bottom; ?>px <?php echo $btn_carousel_hover_l_border_radius_left_bottom; ?>px;
	-o-border-radius: <?php echo $btn_carousel_hover_l_border_radius_left_top; ?>px <?php echo $btn_carousel_hover_l_border_radius_right_top; ?>px <?php echo $btn_carousel_hover_l_border_radius_right_bottom; ?>px <?php echo $btn_carousel_hover_l_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $btn_carousel_hover_l_border_radius_left_top; ?>px <?php echo $btn_carousel_hover_l_border_radius_right_top; ?>px <?php echo $btn_carousel_hover_l_border_radius_right_bottom; ?>px <?php echo $btn_carousel_hover_l_border_radius_left_bottom; ?>px;
	padding: 10px !important;
}
.product-module-carousel .owl-buttons .owl-next:hover i.btn-<?php echo $id; ?> {
	-webkit-border-radius: <?php echo $btn_carousel_hover_r_border_radius_left_top; ?>px <?php echo $btn_carousel_hover_r_border_radius_right_top; ?>px <?php echo $btn_carousel_hover_r_border_radius_right_bottom; ?>px <?php echo $btn_carousel_hover_r_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $btn_carousel_hover_r_border_radius_left_top; ?>px <?php echo $btn_carousel_hover_r_border_radius_right_top; ?>px <?php echo $btn_carousel_hover_r_border_radius_right_bottom; ?>px <?php echo $btn_carousel_hover_r_border_radius_left_bottom; ?>px;
	-o-border-radius: <?php echo $btn_carousel_hover_r_border_radius_left_top; ?>px <?php echo $btn_carousel_hover_r_border_radius_right_top; ?>px <?php echo $btn_carousel_hover_r_border_radius_right_bottom; ?>px <?php echo $btn_carousel_hover_r_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $btn_carousel_hover_r_border_radius_left_top; ?>px <?php echo $btn_carousel_hover_r_border_radius_right_top; ?>px <?php echo $btn_carousel_hover_r_border_radius_right_bottom; ?>px <?php echo $btn_carousel_hover_r_border_radius_left_bottom; ?>px;
	padding: 10px !important;
}
.custom-modal-title-<?php echo $id; ?> {
	font-family: '<?php echo $product_name_text_font; ?>', Arial, sans-serif;
	color: <?php echo $product_name_text_color; ?>;
	<?php if ($product_name_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $product_name_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $product_name_font_size; ?>px;
	font-weight: <?php echo $product_name_font_weight; ?>;
	font-style: <?php echo $product_name_font_style; ?>;
}
.module-modal-content-<?php echo $id; ?> {
	overflow: hidden;
}
.product-module-box-<?php echo $id; ?>-thumbnails {
	overflow: hidden;
	clear: both;
	list-style: none;
	padding: 0;
	margin: 0;
	margin-bottom: 15px;
}
.product-module-box-<?php echo $id; ?>-thumbnails .product-module-carousel {
	float: left;
	width: 97%;
}
.mod-product-image-additional {
	cursor: pointer;
}
.product-module-box-<?php echo $id; ?>-thumbnails .product-module-carousel {
	-webkit-box-shadow: 0 0 0 rgba(0,0,0,.2); 
	-moz-box-shadow: 0 0 0 rgba(0,0,0,.2); 
	-o-box-shadow: 0 0 0 rgba(0,0,0,.2); 
	box-shadow: 0 0 0 rgba(0,0,0,.2);
}
.module-modal-content-<?php echo $id; ?> .module-product-data-<?php echo $id; ?> {
	line-height: 24px;
	font-family: '<?php echo $product_name_data_text_font; ?>', Arial, sans-serif;
	color: <?php echo $product_name_data_text_color; ?>;
	<?php if ($product_name_data_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $product_name_data_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $product_name_data_font_size; ?>px;
	font-weight: <?php echo $product_name_data_font_weight; ?>;
	font-style: <?php echo $product_name_data_font_style; ?>;
}
.module-modal-content-<?php echo $id; ?> .module-product-data-<?php echo $id; ?> span, .module-modal-content-<?php echo $id; ?> .module-product-data-<?php echo $id; ?> span a {
	margin-left: 10px;
	font-family: '<?php echo $product_name_value_text_font; ?>', Arial, sans-serif;
	color: <?php echo $product_name_value_text_color; ?>;
	<?php if ($product_name_value_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $product_name_value_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $product_name_value_font_size; ?>px;
	font-weight: <?php echo $product_name_value_font_weight; ?>;
	font-style: <?php echo $product_name_value_font_style; ?>;
}
.module-modal-content-<?php echo $id; ?> .module-product-data-<?php echo $id; ?> span a {
	text-decoration: underline;
}
.module-modal-content-<?php echo $id; ?> .module-product-data-<?php echo $id; ?> span a:hover {
	text-decoration: none;
}
.module-modal-content-<?php echo $id; ?> .price-new-<?php echo $id; ?> {
	display: inline-block;
	font-family: '<?php echo $quick_product_price_text_font; ?>', Arial, sans-serif;
	color: <?php echo $quick_product_price_text_color; ?>;
	<?php if ($quick_product_price_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $quick_product_price_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $quick_product_price_font_size; ?>px;
	font-weight: <?php echo $quick_product_price_font_weight; ?>;
	font-style: <?php echo $quick_product_price_font_style; ?>;
}
.module-modal-content-<?php echo $id; ?> .price-old-<?php echo $id; ?> {
	display: inline-block;
	margin-right: 15px;
	text-decoration: line-through;
	font-family: '<?php echo $quick_product_price_old_text_font; ?>', Arial, sans-serif;
	color: <?php echo $quick_product_price_old_text_color; ?>;
	<?php if ($quick_product_price_old_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $quick_product_price_old_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $quick_product_price_old_font_size; ?>px;
	font-weight: <?php echo $quick_product_price_old_font_weight; ?>;
	font-style: <?php echo $quick_product_price_old_font_style; ?>;
}
.module-modal-content-<?php echo $id; ?> .tax {
	margin-top: 15px;
	font-family: '<?php echo $quick_product_price_other_text_font; ?>', Arial, sans-serif;
	color: <?php echo $quick_product_price_other_text_color; ?>;
	<?php if ($quick_product_price_other_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $quick_product_price_other_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $quick_product_price_other_font_size; ?>px;
	font-weight: <?php echo $quick_product_price_other_font_weight; ?>;
	font-style: <?php echo $quick_product_price_other_font_style; ?>;
}
.module-modal-content-<?php echo $id; ?> .points {
	margin-top: 5px;
	font-family: '<?php echo $quick_product_price_other_text_font; ?>', Arial, sans-serif;
	color: <?php echo $quick_product_price_other_text_color; ?>;
	<?php if ($quick_product_price_other_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $quick_product_price_other_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $quick_product_price_other_font_size; ?>px;
	font-weight: <?php echo $quick_product_price_other_font_weight; ?>;
	font-style: <?php echo $quick_product_price_other_font_style; ?>;
}
.module-modal-content-<?php echo $id; ?> .discounts {
	font-family: '<?php echo $quick_product_price_other_text_font; ?>', Arial, sans-serif;
	color: <?php echo $quick_product_price_other_text_color; ?>;
	<?php if ($quick_product_price_other_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $quick_product_price_other_text_shadow; ?>;<?php } ?>
	font-size: <?php echo $quick_product_price_other_font_size; ?>px;
	font-weight: <?php echo $quick_product_price_other_font_weight; ?>;
	font-style: <?php echo $quick_product_price_other_font_style; ?>;
}
.module-modal-content-<?php echo $id; ?> .modal-btn {
	clear: both;
	overflow: hidden;
	border-top: 1px solid #ddd;
	padding: 30px 0 10px 0;
}
.module-modal-content-<?php echo $id; ?> .modal-button {
	font-family: '<?php echo $modal_buttons_text_font; ?>', Arial, sans-serif;
	font-size: <?php echo $modal_buttons_font_size; ?>px;
	font-weight: <?php echo $modal_buttons_font_weight; ?>;
	font-style: <?php echo $modal_buttons_font_style; ?>;
	color: <?php echo $modal_buttons_text_color; ?>;
	<?php if ($modal_buttons_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $modal_buttons_text_shadow; ?>;<?php } ?>
	background-color: <?php echo $modal_buttons_bg_color; ?>;
	<?php if ($modal_buttons_image) { ?>background: url('image/<?php echo $modal_buttons_image; ?>') center center repeat;<?php } ?>
	border: <?php echo $modal_buttons_border_weight; ?>px <?php echo $modal_buttons_border_style; ?> <?php echo $modal_buttons_border_color; ?>;
	padding: <?php echo $modal_buttons_padding_top; ?>px <?php echo $modal_buttons_padding_right; ?>px <?php echo $modal_buttons_padding_bottom; ?>px <?php echo $modal_buttons_padding_left; ?>px;
	-webkit-border-radius: <?php echo $modal_buttons_border_radius_left_top; ?>px <?php echo $modal_buttons_border_radius_right_top; ?>px <?php echo $modal_buttons_border_radius_right_bottom; ?>px <?php echo $modal_buttons_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $modal_buttons_border_radius_left_top; ?>px <?php echo $modal_buttons_border_radius_right_top; ?>px <?php echo $modal_buttons_border_radius_right_bottom; ?>px <?php echo $modal_buttons_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $modal_buttons_border_radius_left_top; ?>px <?php echo $modal_buttons_border_radius_right_top; ?>px <?php echo $modal_buttons_border_radius_right_bottom; ?>px <?php echo $modal_buttons_border_radius_left_bottom; ?>px;
	cursor: pointer;
	display: inline-block;
	-webkit-transition: all 0.2s linear;
	-moz-transition: all 0.2s linear;
	-o-transition: all 0.2s linear;
	-ms-transition: all 0.2s linear;
	transition: all 0.2s linear;
}
.module-modal-content-<?php echo $id; ?> .modal-button:hover {
	font-family: '<?php echo $modal_buttons_hover_text_font; ?>', Arial, sans-serif;
	font-size: <?php echo $modal_buttons_hover_font_size; ?>px;
	font-weight: <?php echo $modal_buttons_hover_font_weight; ?>;
	font-style: <?php echo $modal_buttons_hover_font_style; ?>;
	color: <?php echo $modal_buttons_hover_text_color; ?>;
	<?php if ($modal_buttons_hover_text_shadow) { ?>text-shadow: 0 1px 0 <?php echo $modal_buttons_hover_text_shadow; ?>;<?php } ?>
	background-color: <?php echo $modal_buttons_hover_bg_color; ?>;
	<?php if ($modal_buttons_hover_image) { ?>background: url('image/<?php echo $modal_buttons_hover_image; ?>') center center repeat;<?php } ?>
	border: <?php echo $modal_buttons_hover_border_weight; ?>px <?php echo $modal_buttons_hover_border_style; ?> <?php echo $modal_buttons_hover_border_color; ?>;
	padding: <?php echo $modal_buttons_hover_padding_top; ?>px <?php echo $modal_buttons_hover_padding_right; ?>px <?php echo $modal_buttons_hover_padding_bottom; ?>px <?php echo $modal_buttons_hover_padding_left; ?>px;
	-webkit-border-radius: <?php echo $modal_buttons_hover_border_radius_left_top; ?>px <?php echo $modal_buttons_hover_border_radius_right_top; ?>px <?php echo $modal_buttons_hover_border_radius_right_bottom; ?>px <?php echo $modal_buttons_hover_border_radius_left_bottom; ?>px;
	-moz-border-radius: <?php echo $modal_buttons_hover_border_radius_left_top; ?>px <?php echo $modal_buttons_hover_border_radius_right_top; ?>px <?php echo $modal_buttons_hover_border_radius_right_bottom; ?>px <?php echo $modal_buttons_hover_border_radius_left_bottom; ?>px;
	border-radius: <?php echo $modal_buttons_hover_border_radius_left_top; ?>px <?php echo $modal_buttons_hover_border_radius_right_top; ?>px <?php echo $modal_buttons_hover_border_radius_right_bottom; ?>px <?php echo $modal_buttons_hover_border_radius_left_bottom; ?>px;
}
.product-module-box-<?php echo $id; ?>-content .button-cart-<?php echo $id; ?> a i {
	font-size: <?php echo $box_product_cart_font_size; ?>px;
}
.module-modal-content-<?php echo $id; ?> .modal-button  i {
	font-size: <?php echo $modal_buttons_font_size; ?>px;
}
<?php if ($icon_status) { ?>
.product-module-box-<?php echo $id; ?>-content .button-cart-<?php echo $id; ?> a i,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-wishlist-<?php echo $id; ?> i,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-compare-<?php echo $id; ?> i,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-quick-view-<?php echo $id; ?> i,
.module-modal-content-<?php echo $id; ?> .modal-button  i {
	-webkit-transition-duration: 0.8s;
    -moz-transition-duration: 0.8s;
    -o-transition-duration: 0.8s;
    transition-duration: 0.8s;
    -webkit-transition-property: -webkit-transform;
    -moz-transition-property: -moz-transform;
    -o-transition-property: -o-transform;
    transition-property: transform;
}
.product-module-box-<?php echo $id; ?>-content .button-cart-<?php echo $id; ?> a:hover  i,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-wishlist-<?php echo $id; ?>:hover  i,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-compare-<?php echo $id; ?>:hover i,
.product-module-box-<?php echo $id; ?>-content .boxes-product-<?php echo $id; ?> .module-button-quick-view-<?php echo $id; ?>:hover i,
.module-modal-content-<?php echo $id; ?> .modal-button:hover i {
	-webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg); 
    -o-transform: rotate(360deg);
}
<?php } ?>
</style>
<div class="product-module-box-<?php echo $id; ?>">
  <?php if ($status_banner && $image_banner) { ?>
    <div class="banner-image hidden-md hidden-sm hidden-xs"><a href="<?php echo $link_banner; ?>"><img src="<?php echo $image_banner; ?>" /></a></div>
  <?php } ?>
  <div class="product-module-column-left-<?php echo $id; ?>">
    <div class="product-module-box-<?php echo $id; ?>-heading"><?php echo $heading_title; ?></div>
	<div class="vtabs-<?php echo $id; ?>" id="categories-tabs-<?php echo $id; ?>">
	  <?php foreach ($tabs as $id_tab => $tab) { ?>
		<a href="#cat-<?php echo $id_tab; ?>-<?php echo $id; ?>"><?php echo $tab['tab_name']; ?></a>
	  <?php } ?>
	</div>
  </div>
  <div id="product-module-carousel-<?php echo $id; ?>" class="product-module-box-<?php echo $id; ?>-content product-module-carousel">
	<?php foreach ($tabs as $id_tab => $tab) { ?>
	  <div id="cat-<?php echo $id_tab; ?>-<?php echo $id; ?>" class="vtabs-content-<?php echo $id; ?>">
        <div class="product-module-items-<?php echo $id; ?> text-center">
		  <?php foreach ($tab['products'] as $product) { ?>
			<div class="boxes-product-<?php echo $id; ?>">
			  <?php if ($status_quick_view) { ?>
			    <button class="module-button-quick-view-<?php echo $id; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_quick_view; ?>" id="quick-view-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>"><i class="fa fa-eye"></i></button>
			  <?php } ?>
			  <?php if ($status_wishlist) { ?>
			    <button class="module-button-wishlist-<?php echo $id; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
			  <?php } ?>
			  <?php if ($status_compare) { ?>
			    <button class="module-button-compare-<?php echo $id; ?>" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
			  <?php } ?>
			  <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
			  <?php if ($status_product_name) { ?>
			    <div class="name-<?php echo $id; ?>"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
			  <?php } ?>
			  <?php if ($status_rating) { ?>
			    <div class="rating">
				 <?php for ($i = 1; $i <= 5; $i++) { ?>
				    <?php if ($product['rating'] < $i) { ?>
					  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
					  <?php } else { ?>
					  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
				    <?php } ?>
				  <?php } ?>
			    </div>
			  <?php } ?>
			  <?php if ($status_price) { ?>
			    <?php if ($product['price']) { ?>
				  <div class="price">
				    <?php if (!$product['special']) { ?>
					  <?php echo $product['price']; ?>
					  <?php } else { ?>
					  <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
					<?php } ?>
				  </div>
				<?php } ?>
			  <?php } ?>
			  <?php if ($status_cart) { ?>
			    <div class="button-cart-<?php echo $id; ?>"><a onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></a></div>
			  <?php } ?>
			</div>
		  <?php } ?>
		</div>
	  </div>
	<?php } ?>
  </div>
</div>
<?php foreach ($tabs as $id_tab => $tab) { ?>
<?php foreach ($tab['products'] as $product) { ?>
<div class="modal fade" id="quick-view-content-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title custom-modal-title-<?php echo $id; ?>"><?php echo $product['name']; ?></h4>
	  </div>
	  <div class="modal-body module-modal-content-<?php echo $id; ?>">
	    <div id="quick-alert-<?php echo $id; ?>"></div>
		<div class="form-group">
		  <div class="col-md-5">	
			<?php if ($product['modal_thumb']) { ?>
			  <ul class="product-module-box-<?php echo $id; ?>-thumbnails">
				<?php if ($product['modal_thumb']) { ?>
				  <a class="thumbnail text-center" href="<?php echo $product['href']; ?>"><img src="<?php echo $product['modal_thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" id="images-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>" /></a>
				<?php } ?>
				<?php if ($product['images']) { ?>
				  <div id="carousel-product-image-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>" class="product-module-carousel">
					<?php foreach ($product['images'] as $image) { ?>
					  <div class="text-center product-image-additional-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>"><a clickimage="<?php echo $image['popup']; ?>" class="mod-product-image-additional"><img src="<?php echo $image['img']; ?>" class="img-responsive" /></a></div>
					<?php } ?>
				  </div>
				<?php } ?>
			  </ul>
			<?php } ?>
		  </div>
		  <div class="col-md-7">
		    <?php if (($product['manufacturer'] && $status_manufacturer) || ($status_model) || ($product['sku'] && $status_sku) || ($product['weight'] && $status_weight) || ($product['reward'] && $status_reward) || ($status_stock)) { ?>
		      <ul class="list-unstyled module-product-data-<?php echo $id; ?>">
                <?php if ($product['manufacturer'] && $status_manufacturer) { ?>
				  <li><?php echo $text_manufacturer; ?><span><a href="<?php echo $product['manufacturers']; ?>"><?php echo $product['manufacturer']; ?></a></span></li>
                <?php } ?>
			    <?php if ($status_model) { ?>
                  <li><?php echo $text_model; ?><span><?php echo $product['model']; ?></span></li>
			    <?php } ?>
			    <?php if ($product['sku'] && $status_sku) { ?>
				  <li><?php echo $text_sku; ?><span><?php echo $product['sku']; ?></span></li>
                <?php } ?>
			    <?php if ($product['weight'] && $status_weight) { ?>
				  <li><?php echo $text_weight; ?><span><?php echo $product['weight']; ?></span></li>
                <?php } ?>
                <?php if ($product['reward'] && $status_reward) { ?>
				  <li><?php echo $text_reward; ?><span><?php echo $product['reward']; ?></span></li>
                <?php } ?>
			    <?php if ($status_stock) { ?>
                  <li><?php echo $text_stock; ?><span><?php echo $product['stock']; ?></span></li>
			    <?php } ?>
			    <li><hr></li>
              </ul>
			<?php } ?>
			<?php if ($product['price']) { ?>
			  <ul class="list-unstyled">
				<?php if (!$product['special']) { ?>
				  <li class="price-new-<?php echo $id; ?>"><?php echo $product['price']; ?></li>
				  <?php } else { ?>
				  <li class="price-old-<?php echo $id; ?>"><?php echo $product['price']; ?></li>
				  <li class="price-new-<?php echo $id; ?>"><?php echo $product['special']; ?></li>
				<?php } ?>
				<?php if ($product['tax']) { ?>
				  <li class="tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></li>
				<?php } ?>
				<?php if ($product['points']) { ?>
				  <li class="points"><?php echo $text_points; ?> <?php echo $product['points']; ?></li>
				<?php } ?>
				<?php if ($product['discounts']) { ?>
				  <li>
				    <hr>
				  </li>
				  <?php foreach ($product['discounts'] as $discount) { ?>
				    <li class="discounts"><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
				  <?php } ?>
				<?php } ?>
			  </ul>
			<?php } ?>
			<div class="modal-btn text-center">
			  <div class="col-sm-12">
			    <a onclick="cart.add('<?php echo $product['product_id']; ?>');" class="modal-button" id="modal-btn-cart-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>"><i class="fa fa-shopping-cart"></i> <?php echo $button_cart; ?></a>
				<?php if ($status_quick_wishlist) { ?>
				  <a class="modal-button" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></a>
				<?php } ?>
				<?php if ($status_quick_compare) { ?>
				  <a class="modal-button" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></a>
				<?php } ?>
				<?php if ($status_quick_more) { ?>
				  <a class="modal-button" href="<?php echo $product['href']; ?>"><i class="fa fa-eye"></i> <?php echo $button_more; ?></a>
				<?php } ?>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
<script type="text/javascript"><!--
$('#quick-view-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>').on('click', function() {						
	$('#quick-view-content-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>').modal('show');	
});
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	var img = $('#images-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>').attr('src');
	if (img != undefined) {
		var imgWidth = img.substring(img.lastIndexOf('-') + 1, img.lastIndexOf('x'));
		var imgHeight = img.substring(img.lastIndexOf('x') + 1, img.lastIndexOf('.'));
	}
    $('.product-image-additional-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?> img').click(function() {
		var newsrc = $(this).parent().attr('clickimage');
	    $('#images-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>').attr({
			src: newsrc,
			title: $(this).attr('title'),
			alt: $(this).attr('alt'),
			width: imgWidth,
			height: imgHeight
	    });
	    $('#images-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>').parent().attr('clickimage', newsrc);
	});
});
$('#carousel-product-image-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>').owlCarousel({
	items: 3,
	autoPlay: 3000,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-3x"></i>', '<i class="fa fa-chevron-right fa-3x"></i>'],
	slideSpeed: 500,
	pagination: false
});
--></script>
<script type="text/javascript"><!--
$('#modal-btn-cart-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>').on('click', function() {
	$('#quick-view-content-<?php echo $product['product_id']; ?>-<?php echo $id; ?>-<?php echo $id_tab; ?>').modal('hide');		
});
//--></script>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#product-module-carousel-<?php echo $id; ?> .product-module-items-<?php echo $id; ?>').owlCarousel({
	<?php if ($status_banner && $image_banner) { ?>
	items: 3,
	<?php } else { ?>
	items: 4,
	<?php } ?>
	itemsDesktop: [1200,3],
	itemsDesktopSmall: [992,2],
	itemsTablet: [767,1],
	itemsMobile: [320,1],
	navigation: <?php echo $status_carousel; ?>,
	navigationText: ['<i class="fa fa-chevron-left btn-<?php echo $id; ?>"></i>', '<i class="fa fa-chevron-right btn-<?php echo $id; ?>"></i>'],
	slideSpeed: 500,
	pagination: false
});
//--></script>
<script type="text/javascript"><!--
$('#categories-tabs-<?php echo $id; ?> a').tabs();
//--></script>