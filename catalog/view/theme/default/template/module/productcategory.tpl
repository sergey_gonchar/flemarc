<?php $config = $this->registry->get('config');?>
<?php $lang = (int)$config->get('config_language_id'); $text_tooltip_quick = 'Быстрый просмотр';?>
<?php foreach ($categories as $category) { ?>
<div class="panel-heading" style="border-bottom: 1px solid #DADADA;height: 40px; padding-left:0;    margin-bottom: 20px;">
	<h4 class="panel-title" style="border-bottom: 1px solid #555; float:left; padding: 0 10px 14px 10px;font-size: 14px;font-weight:normal;color: #555;"><?php echo $category['name']; ?></h4><span class="pull-right"><a href="<?php echo $category['href']; ?>">View All</a></span>
</div>
<div class="row">
  <?php foreach ($category['products'] as $product) { ?>
  <div class="product-layout product-grid col-lg-2 col-md-2 col-sm-6 col-xs-12"style="padding:0 30px;">
               <?php if($product['special']) { ?>
					<div class="sale">-<?php echo round((preg_replace("/\D/", '', $product['price']) - preg_replace("/\D/", '', $product['special']))/(preg_replace("/\D/", '', $product['price'])/100)); ?>%</div>
               <?php } ?>
<!--  insert from down -->
               <div class="product-thumb">
			   <!-- by skam-->
				<div class="inf_"><h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
				<p class="des"><?php echo $product['description']; ?></p></div>	
			    
                  <div class="action">
                     <?php if($config->get('control_wishlist')== 1) { ?>
                <div class="wishlist">
                        <button class="no-btn" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button>
                     </div>
                <?php } ?>
                     <?php if($config->get('control_compare')== 1) { ?>
                     <div class="compare">
                        <button class="no-btn" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                     </div>
                     <?php } ?>
                     <?php if($config->get('control_quickview')== 1) { ?>
                     <div class="compare visible-md visible-lg">
                        <a rel="nofollow" href="index.php?route=product/quick_view&product_id=<?php echo $product['product_id']; ?>" class="quick-popup" data-toggle="tooltip" title="<?php echo $text_tooltip_quick; ?>"><i class="fa fa-external-link"></i></a>
                     </div>
                     <?php } ?>
                  </div>
				  <div class="image hover">
          <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
          </div>
                  <?php if (isset($product['thumb_swap'])) { ?>
              <div class="image "><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  <?php } else {?>
                  <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  <?php } ?>
              <?php if($config->get('control_quickview')== 2) { ?>
               <div class="contview">
               <a rel="nofollow" href="index.php?route=product/quick_view&product_id=<?php echo $product['product_id']; ?>" class="quick-popup">
               <div class="comparebtn visible-md visible-lg">
                  <?php echo $text_tooltip_quick; ?>
               </div>
               </a>
               </div>
               <?php } ?>
                  <div>
                     <div class="caption">
                        <?php if($config->get('control_stock')== 1) { ?><span class="stock" data-toggle="tooltip" title="<?php echo $product['stock']; ?>" style="background-color:<?php echo ($product['quantity'] > 0) ? '#9BD79B' : '#F07899'; ?>;"></span><?php } ?>



                        <!-- <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4> -->
                        <?php if ($product['rating']) { ?>
                        <div class="rating">
                           <?php for ($i = 1; $i <= 5; $i++) { ?>
                           <?php if ($product['rating'] < $i) { ?>
                           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                           <?php } else { ?>
                           <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                           <?php } ?>
                           <?php } ?>
                        </div>
                        <?php } ?>
                        <?php if ($product['price']) { ?>
                        <p class="price">
                           <?php if (!$product['special']) { ?>
                           <?php echo $product['price']; ?>
                           <?php } else { ?>
                           <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                           <?php } ?>
                           <?php if ($product['tax']) { ?>
                           <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                           <?php } ?>
                        </p>
                        <?php } ?>
						<p class="ad_c"><button class="no-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php if(!empty($product['minimum'])){echo $product['minimum'];}else{ echo 1;} ?>');">
                        <span class="cart-icon"><?php echo $button_cart; ?></span>
                        </button></p>
                     </div>
                  </div>
               </div>
               <?php if(isset($product['options']))foreach ($product['options'] as $option) { ?>
               <?php if ($option['type'] == 'radio') { ?>
               <?php $control_option_title = $config->get('control_option_title'); if ($option['name'] ==  $control_option_title[$lang] ) { ?>
               <div class="opsize">
                  <label class="control-label"><?php echo $option['name']; ?></label>
                  <div class="input-option">
                     <?php foreach ($option['product_option_value'] as $option_value) { ?>
                     <div class="opradio <?php if ($option_value['subtract']) { if ($option_value['quantity'] <= 0) echo 'out';} ?>" data-toggle="tooltip" title="<?php if ($option_value['subtract']) { if ($option_value['quantity'] > 0) echo ''.$text_scl.' '.$option_value['quantity'].' '.$text_pcs; else echo ' '.$text_out_of_stock.' ';} ?>"><?php echo $option_value['name']; ?></div>
                     <?php } ?>
                  </div>
               </div>
               <?php } ?>               
               <?php } ?>
               <?php } ?>
            </div>
  <?php } ?>
</div>
<?php } ?>
