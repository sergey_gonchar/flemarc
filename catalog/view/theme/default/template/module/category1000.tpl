<style>
ul.categories_1000 {
  display: block !important;
  list-style: none;
  padding: 0;
}
ul.categories_1000 ul {
  list-style: none;
  padding: 0;
}
ul.categories_1000 li a {
  padding: 8px 12px;
  color: #8F8B8B;
  display: block;
  border: 1px solid #DDDDDD;
  margin-bottom: -1px;
}
ul.categories_1000 li ul li a {
  padding-left: 30px;
}
ul.categories_1000 li ul li ul li a {
  padding-left: 50px;
}
ul.categories_1000 li a.active, ul.categories_1000 li a.active:hover, ul.categories_1000 li a:hover {
  color: #444444;
  background: #eeeeee;
  border: 1px solid #DDDDDD;
  text-shadow: 0 1px 0 #FFF;
}
</style>

<div class="box">
  <div class="box-heading"><h3><?php echo $heading_title ?></h3></div>
  <div class="box-content">
    <div class="box-category"><?php echo $category_accordion_menu ?></div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.expand-categ').click(function(e) {
		e.preventDefault();
		expandCategories($(this));
	})
	
	function expandCategories(categ) {
		var categ_id = $(categ).attr('category');
		var children = $('#children_' + categ_id);
		var path = $(categ).attr('path');
		if (!$(children).attr('loaded')) {
			
			$.post('<?php echo str_replace('&amp;', '&', $ajax_loader); ?>', { parent_id:categ_id, path:path }, function(data) {
				$(children).attr('loaded', 1);
				$(children).html(data);
				$(children).find('.expand-categ').click(function(e) {
					e.preventDefault();
					expandCategories($(this));
				})
			})
		}
		else {
			document.location.href = $(categ).attr('href');
		}
	}
	
});
</script>
