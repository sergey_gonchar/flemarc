<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
<h1><?php echo $text_faq; ?></h1>

<p>&nbsp;</p>

<?php if($questions) { ?> 

<div class="section">

<?php foreach($questions as $question) {  ?>
<div class="faq-single">
<div class="faq-heading"><?php echo $question['title']; ?></div> 
<div class="faq-answer"><?php echo $question['answer']; ?></div>
</div>
<?php } ?>

</div>

<script type="text/javascript">
$(document).ready(function(){
$('.hidden').next().remove();
$('.hidden').remove();
$(".faq-heading").click(function() {
$(this).toggleClass("active");
$(this).next(".faq-answer").slideToggle( 400, function() {
// Animation complete.
}); }); });
</script>

<style type="text/css">
.section {margin-bottom:35px;}
.section_title {font-size:24px;margin-bottom:13px;font-weight:normal;}
.faq-single {border:1px solid #dddddd;border-bottom:none;margin-bottom:5px;position:relative;border-radius:3px;}
.faq-heading {font-size:12px;font-weight:bold;padding:11px 25px 11px 12px; line-height:18px; border-bottom:1px solid #dddddd;cursor:pointer;transition:all 500ms ease; background-image: url('catalog/view/theme/default/image/toggle-icon.png');
background-position:100% -90px; background-repeat:no-repeat;}
.faq-heading:hover, .faq-heading.active {background-color:#f9f9f9;}
.faq-heading.active {background-position:100% 0px; background-repeat:no-repeat;}
.faq-answer {display:none;line-height:18px;padding:15px 15px 0 15px;border-bottom:1px solid #dddddd;}
</style>

<?php } ?>
      
<div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>      
      
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 