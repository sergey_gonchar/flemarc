<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
<h1><?php echo $text_faq; ?></h1>

<p>&nbsp;</p>
<?php if($questions) { ?>  

<style type="text/css">
.wrapper{
	width:98%;
}
.st-accordion{
    width:100%;
    min-width:270px;
    margin: 0 auto;
}
.st-accordion ul li{
    height: 100px;
    border-bottom: 1px solid #c7deef;
    border-top:1px solid #fff;
    overflow: hidden;
}
.st-accordion ul li:first-child{
    border-top:none;
}
.st-accordion ul li > a{
    font-family: 'Josefin Slab',Georgia, serif;
    text-shadow: 1px 1px 1px #fff;
    font-size: 20px;
    display: block;
	position: relative;
    line-height: 50px;
	outline:none;
    -webkit-transition:  color 0.2s ease-in-out;
	-moz-transition:  color 0.2s ease-in-out;
	-o-transition:  color 0.2s ease-in-out;
	-ms-transition:  color 0.2s ease-in-out;
	transition:  color 0.2s ease-in-out;
}
.st-accordion ul li > a:hover{
    color: #1693eb;
}
.st-accordion ul li > a:hover span{
	opacity:1;
	right: 10px;
}
.st-accordion ul li.st-open > a{
    color: #1693eb;
}
.st-accordion ul li.st-open > a span{
	-webkit-transform:rotate(180deg);
	-moz-transform:rotate(180deg);
    transform:rotate(180deg);
	right:10px;
	opacity:1;
}
.st-content{
    padding: 5px 0px 30px 0px;
}
.st-content p{
    font-size:  16px;
    font-family:  Georgia, serif;
    font-style: italic;
    line-height:  28px;
    padding: 0px 4px 15px 4px; 
}
.st-content img{
    width:125px;
    border-right:1px solid #fff;
    border-bottom:1px solid #fff;
}
@media screen and (max-width: 320px){
	.st-accordion ul li > a{
		font-size:36px;
	}
}
</style>

<style>
				.st-accordion ul li{
					height:auto;
				}
				.st-accordion ul li > a span{
					visibility:hidden;
				}
			</style>

  
<div class="wrapper">
				
                <div id="st-accordion" class="st-accordion">
                    <ul>
                    	<?php foreach($questions as $question) {  ?>
                        <li>
                            <a href="#"><?php echo $question['title']; ?></a>
                            <div class="st-content">
                                <?php echo $question['answer']; ?>
                            </div>
                        </li>
                        
                        <?php } ?>
                        
                    </ul>
                </div>
            </div>
            
        <script type="text/javascript" src="catalog/view/javascript/coc018/jquery.accordion.js"></script>
		<script type="text/javascript" src="catalog/view/javascript/coc018/jquery.easing.1.3.js"></script>
        <script type="text/javascript">
            $(function() {
			
				$('#st-accordion').accordion({
					oneOpenedItem	: true
				});
				
            });
        </script>

<?php } ?>
      
<div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>      
      
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 