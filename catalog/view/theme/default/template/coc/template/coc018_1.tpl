<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
    
<script type="text/javascript">
$(function(){
var n = 0;
	$('.dropdown h3').click(function(){
		$('.dropdown h3').removeClass('open');
		$('.dropdown p').slideUp('normal');
		if(n != $(this).index()){
			$(this).addClass('open');
			$(this).next('p').slideDown('normal');
			n = $(this).index();
		}else{
			n = 0;
		}
	});
});

</script>    

<style type="text/css">

.dropdown h2 {
    font-size: 18px;
    margin-bottom: 5px;
    margin-top: 15px;
}
.dropdown span {
}
.dropdown b {
    display: block;
    margin: 20px 0;
}
.dropdown h3 {
    background: linear-gradient(to bottom, #ffffff 0%, #f1f1f1 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: 1px solid #cccccc;
    border-radius: 5px;
    color: #494949;
    cursor: pointer;
    font-weight: normal;
    line-height: 32px;
    margin: 0 0 10px;
    padding: 2px 15px;
}
.dropdown h3.open {
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin: 0;
}
.dropdown p {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: none repeat scroll 0 0 #f9f9f9;
    border-color: -moz-use-text-color #ccc #ccc;
    border-image: none;
    border-radius: 0 0 5px 5px;
    border-right: 1px solid #ccc;
    border-style: none solid solid;
    border-width: 0 1px 1px;
    display: none;
    margin: 0 0 10px;
    padding: 10px;
}
</style>
      
      <h1><?php echo $text_faq; ?></h1>

<p>&nbsp;</p>

<div class="dropdown">

<?php

if($questions) {

	$i = 1;
	foreach($questions as $question) { 
		
        if($i == 1) {
?>

<h3 class="open"><?php echo $question['title']; ?></h3>

<p style="display: block;"><?php echo $question['answer']; ?></p>

<?php 	}else { ?>

<h3><?php echo $question['title']; ?></h3>

<p><?php echo $question['answer']; ?></p>

<?php 
		}
		$i++;
	}
}
?>



</div>
      
<div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>      
      
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 