<script src="catalog/view/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<?php $config = $this->registry->get('config');?>
<?php $lang = (int)$config->get('config_language_id');?>
<div id="quick" data-i32temscope_mdp data-i32temtype_mdp="http://schema.org/Product">
   <i class="fa fa-chevron-down arrowscroll"></i>
   <?php if (!empty($prevProduct)) { ?>
   <a href="<?php echo $prevProduct['href']; ?>">
      <div class="prevnext prev-product" data-toggle="popover" data-container="body" data-placement="right" data-content="<?php echo $prevProduct['name'];?>"><img src="<?php echo $prevProduct['image']; ?>" alt="<?php echo $prevProduct['name'];?>"></div>
   </a>
   <?php } ?>
   <?php if (!empty($nextProduct)) { ?>
   <a href="<?php echo $nextProduct['href']; ?>">
      <div class="prevnext next-product" data-toggle="popover" data-container="body" data-placement="left" data-content="<?php echo $nextProduct['name'];?>"><img src="<?php echo $nextProduct['image']; ?>" alt="<?php echo $nextProduct['name'];?>"></div>
   </a>
   <?php } ?>       
   <div class="quickleft">
      <div id="one-image" class="owl-carousel">
         <div class="item">
            <img src="<?php echo $thumb; ?>" id="image"  title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" data-i32temprop_mdp="image" />
            <a class="zoom2 thumbnails" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"></a>
         </div>
         <?php if ($images) { ?>
         <?php foreach ($images as $image) { ?>
         <div class="item">
            <img src="<?php echo $image['fix']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
            <a class="zoom2 thumbnails" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"></a>
         </div>
         <?php } ?>
         <?php } ?>
      </div>
      <script type="text/javascript">
         $(".zoom").on("click", function(){
         $(".active .zoom2").click();
         });							
      </script>
      <?php if ($images && $config->get('control_additional')== 3) { ?>
      <div id="image-additional" class="owl-carousel image-additional">
         <div class="item"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $fix2; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
         <?php foreach ($images as $image) { ?>
         <div class="item">
            <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
         </div>
         <?php } ?>
      </div>
      <?php } ?>
      <script>
         // product images owl
         var a = $("#one-image");
         var b = $("#image-additional");
         
         a.owlCarousel({
         singleItem: true,
         slideSpeed: 1000,
         navigation: true,
         navigationText: ['<i class="fa fa-angle-left fa-5x"></i>', '<i class="fa fa-angle-right fa-5x"></i>'],
         pagination: false,
         afterAction: syncPosition,
         responsiveRefreshRate: 200,
         addClassActive: true,
         });
         
         b.owlCarousel({
         items: 6,
         itemsDesktop: [1199, 6],
         itemsDesktopSmall: [979, 5],
         itemsTablet: [768, 4],
         itemsMobile: [479, 3],
         pagination: false,
         navigation: true,
         navigationText: ['<i class="fa fa-angle-left fa-5x"></i>', '<i class="fa fa-angle-right fa-5x"></i>'],
         responsiveRefreshRate: 100,
         afterInit: function(el) {
             el.find(".owl-item").eq(0).addClass("synced");
         }
         });
         
         function syncPosition(el) {
         var current = this.currentItem;
         $("#image-additional")
             .find(".owl-item")
             .removeClass("synced")
             .eq(current)
             .addClass("synced")
         if ($("#image-additional").data("owlCarousel") !== undefined) {
             center(current)
         }
         }
         
         $("#image-additional").on("click", ".owl-item", function(e) {
         e.preventDefault();
         var number = $(this).data("owlItem");
         a.trigger("owl.goTo", number);
         });
         
         function center(number) {
         var bvisible = b.data("owlCarousel").owl.visibleItems;
         var num = number;
         var found = false;
         for (var i in bvisible) {
             if (num === bvisible[i]) {
                 var found = true;
             }
         }
         
         if (found === false) {
             if (num > bvisible[bvisible.length - 1]) {
                 b.trigger("owl.goTo", num - bvisible.length + 2)
             } else {
                 if (num - 1 === -1) {
                     num = 0;
                 }
                 b.trigger("owl.goTo", num);
             }
         } else if (num === bvisible[bvisible.length - 1]) {
             b.trigger("owl.goTo", bvisible[1])
         } else if (num === bvisible[0]) {
             b.trigger("owl.goTo", num - 1)
         }
         }
      </script>
      <!--tab-->         
   </div>
   <div id="scrolli" class="product-info">
      <div class="scrollhide"></div>
      <div class="buttons">
         <div class="pull-left info-left col-sm-7 nopad">
            <h1 data-i32temprop_mdp="name"><?php echo $heading_title; ?></h1>
            <ul class="list-unstyled">
               <?php if ($manufacturer) { ?>
               <li><?php echo $text_manufacturer; ?> <a data-i32temprop_mdp="manufacturer" href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
               <?php } ?>
               <li><?php echo $text_model; ?> <span data-i32temprop_mdp="model"><?php echo $model; ?></span></li>
               <?php if ($reward) { ?>
               <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
               <?php } ?>
               <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
            </ul>
         </div>
         <?php if ($price) { ?>      
         <span data-i32temprop_mdp = "offers" data-i32temscope_mdp data-i32temtype_mdp = "http://schema.org/Offer">
            <meta data-i32temprop_mdp="price" content="<?php echo rtrim(preg_replace("/[^0-9\.]/", "", ($special ? $special : $price)), '.'); ?>" />
            <meta data-i32temprop_mdp="priceCurrency" content="<?php echo $priceCurrency; ?>" />
            <meta data-i32temprop_mdp="availability" content="<?php echo $stock; ?>" />
         </span>
         <div class="pull-right info-right col-sm-5 nopad">
            <ul class="list-unstyled">
               <?php if (!$special) { ?>
               <li>
                  <h2><span id="formated_price" price="<?php echo $price_value; ?>"><?php echo $price; ?></span></h2>
               </li>
               <?php } else { ?>  
               <li><span class="through"><span id="formated_price" price="<?php echo $price_value; ?>"><?php echo $price; ?></span></span></li>
               <li>
                  <h2><span id="formated_special" price="<?php echo $special_value; ?>"><?php echo $special; /**/ ?></span></h2>
               </li>
               <?php } ?>
               <?php if ($tax) { ?>
               <li><?php echo $text_tax; ?> <span id="formated_tax" price="<?php echo $tax_value; ?>"><?php echo $tax; ?></span></li>
               <?php } ?>
               <?php if ($points) { ?>
               <li><?php echo $text_points; ?> <span id="formated_points" points="<?php echo $points; /**/ ?>"><?php echo $points; /**/ ?></span></li>
               <?php } ?>
               <?php if ($discounts) { ?>
               <li>
                  <hr>
               </li>
               <?php foreach ($discounts as $discount) { ?>
               <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
               <?php } ?>
               <?php } ?>
            </ul>
         </div>
         <?php } ?>
      </div>
      <div id="product">
         <hr class="tophr">
         <?php if($config->get('control_description')== 1) { ?>  
         <div class="tab-content">
            <ul class="nav nav-tabs">
               <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
               <?php if ($attribute_groups) { ?>
               <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
               <?php } ?>
               <?php if($config->get('control_custom_tab_status')== 1) {
                  $control_title_tab = $config->get('control_title_tab');
                  $control_content_tab = $config->get('control_content_tab');    
                  if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
               <li><a href="#tab-custom" data-toggle="tab"><?php echo $control_title_tab[$lang]; ?></a></li>
               <?php } ?>
               <?php } ?>
			   				   <?php if($config->get('control_custom_tab4_status')== 1) {
                     $control_title_tab4 = $config->get('control_title_tab4');
                     $control_content_tab4 = $config->get('control_content_tab4');    
                     if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                  <li><a href="#tab-custom4" data-toggle="tab"><?php echo $control_title_tab4[$lang]; ?></a></li>
                  <?php } ?>
                  <?php } ?>
            </ul>
            <div class="tab-pane active" id="tab-description" data-i32temprop_mdp="description"><?php echo $description; ?></div>
            <?php if ($attribute_groups) { ?>
            <div class="tab-pane" id="tab-specification">
               <table class="table table-bordered">
                  <?php foreach ($attribute_groups as $attribute_group) { ?>
                  <thead>
                     <tr>
                        <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                     </tr>
                  </thead>
                  <tbody>
                     <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                     <tr>
                        <td><?php echo $attribute['name']; ?></td>
                        <td><?php echo $attribute['text']; ?></td>
                     </tr>
                     <?php } ?>
                  </tbody>
                  <?php } ?>
               </table>
            </div>
            <?php } ?>
            <?php if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
            <div class="tab-pane" id="tab-custom"><?php echo html_entity_decode($control_content_tab[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
            <?php } ?>
            <?php if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
            <div class="tab-pane" id="tab-custom4"><?php echo html_entity_decode($control_content_tab4[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
            <?php } ?>			
         </div>
         <hr>
         <?php } ?>
         <?php if ($options) { ?>
         <h3><?php echo $text_option; ?></h3>
         <?php foreach ($options as $option) { ?>
         <?php if ($option['type'] == 'select') { ?>
         <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control customselect">
               <option value=""><?php echo $text_select; ?></option>
               <?php foreach ($option['product_option_value'] as $option_value) { ?>
               <option value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>"><?php echo $option_value['name']; ?>
                  <?php if ($option_value['price']) { ?>
                  <?php
          if ($option_value['price_prefix'] == '*') {
            if ($option_value['price_value'] != 1.0)
              printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
          } else {
            echo "(".$option_value['price_prefix'].$option_value['price'].")"; 
          }
          ?>
                  <?php } ?>
               </option>
               <?php } ?>
            </select>
         </div>
         <?php } ?>
         <?php if ($option['type'] == 'radio') { ?>
         <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
            <label class="control-label"><?php echo $option['name']; ?></label>
            <div id="input-option<?php echo $option['product_option_id']; ?>">
               <?php foreach ($option['product_option_value'] as $option_value) { ?>
               <?php if ($option_value['subtract'] && !$option_value['quantity'])  { ?>
               <?php if($config->get('control_outstock')== 1) {?>
               <div class="radio" data-value="<?php echo $option['name']; ?>: <?php echo $option_value['name']; ?>">
                  <div data-toggle="tooltip" title="<?php echo $text_outstock; ?>">
                     <div class="panel-url outstock_canvas" data-action="order" data-spanel="#outstock"></div>
                  </div>
                  <?php } else { ?>
                  <div class="radio">
                     <?php } ?>
                     <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" disabled="disabled" id="<?php echo $option_value['product_option_value_id']; ?>" />
                     <?php } else { ?>
                     <div class="radio">
                        <input  type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" />
                        <?php } ?>
                        <label data-toggle="tooltip" title="<?php if ($option_value['subtract']) { if ($option_value['quantity'] > 0) echo ''.$text_scl.' '.$option_value['quantity'].' '.$text_pcs; else echo ' '.$text_out_of_stock.' ';} ?>" for="<?php echo $option_value['product_option_value_id']; ?>">
                        <?php echo $option_value['name']; ?>
                        </label>
                     </div>
                     <?php } ?>
                  </div>
               </div>
               <?php } ?>
               <?php if ($option['type'] == 'checkbox') { ?>
               <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                  <label class="control-label"><?php echo $option['name']; ?></label>
                  <div id="input-option<?php echo $option['product_option_id']; ?>">
                     <?php foreach ($option['product_option_value'] as $option_value) { ?>
                     <div class="checkbox">
                        <label>
                        <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" class="option-input"/>
                        <?php echo $option_value['name']; ?>
                        <?php if ($option_value['price']) { ?>
                        <?php
          if ($option_value['price_prefix'] == '*') {
            if ($option_value['price_value'] != 1.0)
              printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
          } else {
            echo "(".$option_value['price_prefix'].$option_value['price'].")"; 
          }
          ?>
                        <?php } ?>
                        </label>
                     </div>
                     <?php } ?>
                  </div>
               </div>
               <?php } ?>
               <?php if ($option['type'] == 'image') { ?>
                  <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> radioimg">
                     <label class="control-label"><?php echo $option['name']; ?></label>
                     <div id="input-option<?php echo $option['product_option_id']; ?>">
                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                        <div class="radio">                           
                           <label for="<?php echo $option_value['product_option_value_id']; ?>">
						   <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" />
                           <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> 
						   <div class="labeldiv">
						   <?php echo $option_value['name']; ?>                          
						   <?php if ($option_value['price']) { ?>
                           <?php
          if ($option_value['price_prefix'] == '*') {
            if ($option_value['price_value'] != 1.0)
              printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
          } else {
            echo "(".$option_value['price_prefix'].$option_value['price'].")"; 
          }
          ?>
                           <?php } ?>
						   </div>
                           </label>
                        </div>
                        <?php } ?>
                     </div>
                  </div>
                  <?php } ?>
               <?php if ($option['type'] == 'text') { ?>
               <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
               </div>
               <?php } ?>
               <?php if ($option['type'] == 'textarea') { ?>
               <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
               </div>
               <?php } ?>
               <?php if ($option['type'] == 'file') { ?>
               <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                  <label class="control-label"><?php echo $option['name']; ?></label>
                  <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                  <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
               </div>
               <?php } ?>
               <?php if ($option['type'] == 'date') { ?>
               <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <div class="input-group date">
                     <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                     <span class="input-group-btn">
                     <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                     </span>
                  </div>
               </div>
               <?php } ?>
               <?php if ($option['type'] == 'datetime') { ?>
               <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <div class="input-group datetime">
                     <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                     <span class="input-group-btn">
                     <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                     </span>
                  </div>
               </div>
               <?php } ?>
               <?php if ($option['type'] == 'time') { ?>
               <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                  <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                  <div class="input-group time">
                     <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                     <span class="input-group-btn">
                     <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                     </span>
                  </div>
               </div>
               <?php } ?>
               <?php } ?>
               <?php } ?>
               <?php if($variants && $config->get('variant_status')== 1) {?>
                  <div class="variant">
                     <h3><?php echo $variant_title; ?></h3>
                     <?php foreach($variants as $product){ ?>
                     <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="" /></a> 
                     <?php } ?>           
                  </div>
                  <?php } ?>
               <?php if ($recurrings) { ?>
               <hr>
               <h3><?php echo $text_payment_recurring ?></h3>
               <div class="form-group required">
                  <select name="recurring_id" class="form-control">
                     <option value=""><?php echo $text_select; ?></option>
                     <?php foreach ($recurrings as $recurring) { ?>
                     <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                     <?php } ?>
                  </select>
                  <div class="help-block" id="recurring-description"></div>
               </div>
               <?php } ?> 
               <div class="form-group orderbtn">
                  <?php if($config->get('control_quantity')== 1) {?>
                  <div class="quantity">
                     <input data-toggle="tooltip" title="<?php echo $entry_qty; ?>" type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                     <a class="plus" href="javascript:void(0);">+</a>
                     <a class="minus" href="javascript:void(0);">-</a>
                  </div>
                  <script type="text/javascript">  
                     $(".quantity a").click(function(){
                     if($(this).hasClass("plus")){
                     var qty = $("#input-quantity").val();
                     qty++;
                     $("#input-quantity").val(qty);
                     }else{
                     var qty = $("#input-quantity").val();
                     qty--;
                     if(qty>0){
                     $("#input-quantity").val(qty);
                     }
                     }
                     });
                  </script>
                  <?php } ?>
                  <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                  <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_cart; ?></button>
                  <a href="<?php echo $product_url; ?>" class="panel-url btn btn-default"><?php echo $text_more; ?></a>
               </div>
               <?php if ($minimum > 1) { ?>
               <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
               <?php } ?>
               <?php if($config->get('control_description')== 2) { ?>  
               <div class="tab-content">
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                     <?php if ($attribute_groups) { ?>
                     <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                     <?php } ?>
                     <?php if($config->get('control_custom_tab_status')== 1) {
                        $control_title_tab = $config->get('control_title_tab');
                        $control_content_tab = $config->get('control_content_tab');    
                        if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
                     <li><a href="#tab-custom" data-toggle="tab"><?php echo $control_title_tab[$lang]; ?></a></li>
                     <?php } ?>
                     <?php } ?>
					  <?php if($config->get('control_custom_tab4_status')== 1) {
                     $control_title_tab4 = $config->get('control_title_tab4');
                     $control_content_tab4 = $config->get('control_content_tab4');    
                     if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                  <li><a href="#tab-custom4" data-toggle="tab"><?php echo $control_title_tab4[$lang]; ?></a></li>
                  <?php } ?>
                  <?php } ?>
                  </ul>
                  <div class="tab-pane active" id="tab-description" data-i32temprop_mdp="description"><?php echo $description; ?></div>
                  <?php if ($attribute_groups) { ?>
                  <div class="tab-pane" id="tab-specification">
                     <table class="table table-bordered">
                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                        <thead>
                           <tr>
                              <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                           <tr>
                              <td><?php echo $attribute['name']; ?></td>
                              <td><?php echo $attribute['text']; ?></td>
                           </tr>
                           <?php } ?>
                        </tbody>
                        <?php } ?>
                     </table>
                  </div>
                  <?php } ?>
                  <?php if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
                  <div class="tab-pane" id="tab-custom"><?php echo html_entity_decode($control_content_tab[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
                  <?php } ?>
                  <?php if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                  <div class="tab-pane" id="tab-custom4"><?php echo html_entity_decode($control_content_tab4[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
                  <?php } ?>				  
               </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>
   <script type="text/javascript"><!--      
      $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
           $.ajax({
             url: 'index.php?route=product/product/getRecurringDescription',
             type: 'post',
             data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
             dataType: 'json',
             beforeSend: function() {
               $('#recurring-description').html('');
             },
             success: function(json) {
               $('.alert, .text-danger').remove();
         
               if (json['success']) {
                 $('#recurring-description').html(json['success']);
               }
             }
           });
         });
         
      
         $('#button-cart').on('click', function() {
            var qty_val=$('#qty').val();
          if(qty_val==0)
          {
             alert("Select quantity.");
            
          }
          else
          {
           $.ajax({
             url: 'index.php?route=checkout/cart/add',
             type: 'post',
             data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
             dataType: 'json',
             beforeSend: function() {
               $('#button-cart').button('loading');
             },
             complete: function() {
               $('#button-cart').button('reset');
             },
             success: function(json) {
               $('.alert, .text-danger').remove();
               $('.form-group').removeClass('has-error');
         
               if (json['error']) {
                 if (json['error']['option']) {
                   for (i in json['error']['option']) {
                     var element = $('#input-option' + i.replace('_', '-'));
         
                     if (element.parent().hasClass('input-group')) {
                       element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                     } else {
                       element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                     }
                   }
                 }
         
                 if (json['error']['recurring']) {
                   $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                 }
         
                 // Highlight any found errors
                 $('.text-danger').parent().addClass('has-error');
               }
         
               if (json['success']) {	
                       var out = json['total'].substr(0,json['total'].indexOf(' '));
                       // Need to set timeout otherwise it wont update the total
                       setTimeout(function() {
                           $('#cart > button').html('<span id="cart-total">' + out + '</span>');
                       }, 100);
      
                       $('#cart > ul').load('index.php?route=common/cart/info ul li');
         		
         		$.magnificPopup.close();
         		$("#carturl").click();
               }
             }
           });
         }
         });
        
      
         $('.date').datetimepicker({
           pickTime: false
         });
         
         $('.datetime').datetimepicker({
           pickDate: true,
           pickTime: true
         });
         
         $('.time').datetimepicker({
           pickDate: false
         });
         
         $('button[id^=\'button-upload\']').on('click', function() {
           var node = this;
         
           $('#form-upload').remove();
         
           $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');
         
           $('#form-upload input[name=\'file\']').trigger('click');
         
           if (typeof timer != 'undefined') {
               clearInterval(timer);
           }
         
           timer = setInterval(function() {
             if ($('#form-upload input[name=\'file\']').val() != '') {
               clearInterval(timer);
         
               $.ajax({
                 url: 'index.php?route=tool/upload',
                 type: 'post',
                 dataType: 'json',
                 data: new FormData($('#form-upload')[0]),
                 cache: false,
                 contentType: false,
                 processData: false,
                 beforeSend: function() {
                   $(node).button('loading');
                 },
                 complete: function() {
                   $(node).button('reset');
                 },
                 success: function(json) {
                   $('.text-danger').remove();
         
                   if (json['error']) {
                     $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                   }
         
                   if (json['success']) {
                     alert(json['success']);
         
                     $(node).parent().find('input').attr('value', json['code']);
                   }
                 },
                 error: function(xhr, ajaxOptions, thrownError) {
                   alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                 }
               });
             }
           }, 500);
         });
         
         $("#quick").niceScroll();
         //--></script>
		<?php if($config->get('control_autoprice')== 1) { ?> 
<script type="text/javascript"><!--

function price_format(n)
{ 
    c = <?php echo (empty($currency['decimals']) ? "0" : $currency['decimals'] ); ?>;
    d = '<?php echo $currency['decimal_point']; ?>'; // decimal separator
    t = '<?php echo $currency['thousand_point']; ?>'; // thousands separator
    s_left = '<?php echo $currency['symbol_left']; ?>';
    s_right = '<?php echo $currency['symbol_right']; ?>';
      
    n = n * <?php echo $currency['value']; ?>;

    //sign = (n < 0) ? '-' : '';

    //extracting the absolute value of the integer part of the number and converting to string
    i = parseInt(n = Math.abs(n).toFixed(c)) + ''; 

    j = ((j = i.length) > 3) ? j % 3 : 0; 
    return s_left + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '') + s_right; 
}

function calculate_tax(price)
{
    <?php // Process Tax Rates
      if (isset($tax_rates) && $tax) {
         foreach ($tax_rates as $tax_rate) {
           if ($tax_rate['type'] == 'F') {
             echo 'price += '.$tax_rate['rate'].';';
           } elseif ($tax_rate['type'] == 'P') {
             echo 'price += (price * '.$tax_rate['rate'].') / 100.0;';
           }
         }
      }
    ?>
    return price;
}

function process_discounts(price, quantity)
{
    <?php
      foreach ($dicounts_unf as $discount) {
        echo 'if ((quantity >= '.$discount['quantity'].') && ('.$discount['price'].' < price)) price = '.$discount['price'].';'."\n";
      }
    ?>
    return price;
}


animate_delay = 20;

main_price_final = calculate_tax(Number($('#formated_price').attr('price')));
main_price_start = calculate_tax(Number($('#formated_price').attr('price')));
main_step = 0;
main_timeout_id = 0;

function animateMainPrice_callback() {
    main_price_start += main_step;
    
    if ((main_step > 0) && (main_price_start > main_price_final)){
        main_price_start = main_price_final;
    } else if ((main_step < 0) && (main_price_start < main_price_final)) {
        main_price_start = main_price_final;
    } else if (main_step == 0) {
        main_price_start = main_price_final;
    }
    
    $('#formated_price').html( price_format(main_price_start) );
    
    if (main_price_start != main_price_final) {
        main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
    }
}

function animateMainPrice(price) {
    main_price_start = main_price_final;
    main_price_final = price;
    main_step = (main_price_final - main_price_start) / 10;
    
    clearTimeout(main_timeout_id);
    main_timeout_id = setTimeout(animateMainPrice_callback, animate_delay);
}


<?php if ($special) { ?>
special_price_final = calculate_tax(Number($('#formated_special').attr('price')));
special_price_start = calculate_tax(Number($('#formated_special').attr('price')));
special_step = 0;
special_timeout_id = 0;

function animateSpecialPrice_callback() {
    special_price_start += special_step;
    
    if ((special_step > 0) && (special_price_start > special_price_final)){
        special_price_start = special_price_final;
    } else if ((special_step < 0) && (special_price_start < special_price_final)) {
        special_price_start = special_price_final;
    } else if (special_step == 0) {
        special_price_start = special_price_final;
    }
    
    $('#formated_special').html( price_format(special_price_start) );
    
    if (special_price_start != special_price_final) {
        special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
    }
}

function animateSpecialPrice(price) {
    special_price_start = special_price_final;
    special_price_final = price;
    special_step = (special_price_final - special_price_start) / 10;
    
    clearTimeout(special_timeout_id);
    special_timeout_id = setTimeout(animateSpecialPrice_callback, animate_delay);
}
<?php } ?>


function recalculateprice()
{
    var main_price = Number($('#formated_price').attr('price'));
    var input_quantity = Number($('input[name="quantity"]').val());
    var special = Number($('#formated_special').attr('price'));
    var tax = 0;
    
    if (isNaN(input_quantity)) input_quantity = 0;
    
    // Process Discounts.
    <?php if ($special) { ?>
        special = process_discounts(special, input_quantity);
    <?php } else { ?>
        main_price = process_discounts(main_price, input_quantity);
    <?php } ?>
    tax = process_discounts(tax, input_quantity);
        
   <?php if ($points) { ?>
     var points = Number($('#formated_points').attr('points'));
     $('input:checked,option:selected').each(function() {
        if ($(this).attr('points')) points += Number($(this).attr('points'));
     });
     $('#formated_points').html(Number(points));
   <?php } ?>
    
    var option_price = 0;
    
    $('input:checked,option:selected').each(function() {
      if ($(this).attr('price_prefix') == '=') {
        option_price += Number($(this).attr('price'));
        main_price = 0;
        special = 0;
      }
    });
    
    $('input:checked,option:selected').each(function() {
      if ($(this).attr('price_prefix') == '+') {
        option_price += Number($(this).attr('price'));
      }
      if ($(this).attr('price_prefix') == '-') {
        option_price -= Number($(this).attr('price'));
      }
      if ($(this).attr('price_prefix') == 'u') {
        pcnt = 1.0 + (Number($(this).attr('price')) / 100.0);
        option_price *= pcnt;
        main_price *= pcnt;
        special *= pcnt;
      }
      if ($(this).attr('price_prefix') == '*') {
        option_price *= Number($(this).attr('price'));
        main_price *= Number($(this).attr('price'));
        special *= Number($(this).attr('price'));
      }
    });
    
    special += option_price;
    main_price += option_price;

    <?php if ($special) { ?>
      tax = special;
    <?php } else { ?>
      tax = main_price;
    <?php } ?>
    
    // Process TAX.
    main_price = calculate_tax(main_price);
    special = calculate_tax(special);
    
    // Раскомментировать, если нужен вывод цены с умножением на количество
    main_price *= input_quantity;
    special *= input_quantity;
    tax *= input_quantity;

	<?php if ($special) { ?>
      html = 'Вы экономите: <span class="price">' + price_format(main_price - special) + '</span>';
      $('.price-economy').html(html);
      
      //$('#formated_special').html( price_format(special) );
      animateSpecialPrice(special);
    <?php } ?>
	
    // Display Main Price
    //$('#formated_price').html( price_format(main_price) );
    animateMainPrice(main_price);
      
    <?php if ($special) { ?>
      //$('#formated_special').html( price_format(special) );
      animateSpecialPrice(special);
    <?php } ?>

    <?php if ($tax) { ?>
      $('#formated_tax').html( price_format(tax) );
    <?php } ?>
		
}

$(document).ready(function() {
    $('input[type="checkbox"]').bind('change', function() { recalculateprice(); });
    $('input[type="radio"]').bind('change', function() { recalculateprice(); });
    $('select').bind('change', function() { recalculateprice(); });
    
    $quantity = $('input[name="quantity"]');
    $quantity.data('val', $quantity.val());
    (function() {
        if ($quantity.val() != $quantity.data('val')){
            $quantity.data('val',$quantity.val());
            recalculateprice();
        }
        setTimeout(arguments.callee, 250);
    })();    
    
    recalculateprice();
});

//--></script>
<?php } ?>
<?php if($config->get('control_firstoption')== 1) { ?> 
<script type="text/javascript"><!--
$(document).ready(function() {
$('#product').each(function() {
$(this).find('.radio input:first, .radioimg input:first').trigger('click');
});
});
//--></script>
<?php } ?>
</div>