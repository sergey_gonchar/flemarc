<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<table class="table table-striped table-bordered" data-i32temprop_mdp="review" data-i32temscope_mdp data-i32temtype_mdp="http://schema.org/Review">
  <tr>
    <td style="width: 50%;"><strong data-i32temprop_mdp="author"><?php echo $review['author']; ?></strong></td>
    <td class="text-right" data-i32temprop_mdp="datePublished"><?php echo $review['date_added']; ?></td>
  </tr>  
  <tr>
    <td colspan="2"><p data-i32temprop_mdp="reviewBody"><?php echo $review['text']; ?></p>
	<span data-i32temprop_mdp="reviewRating" data-i32temscope_mdp data-i32temtype_mdp="http://schema.org/Rating">
					<meta data-i32temprop_mdp = "worstRating" content = "1" >
					<meta data-i32temprop_mdp = "ratingValue" content="<?php echo $review['rating']; ?>">
					<meta data-i32temprop_mdp = "bestRating" content="5">
				  </span>
      <?php for ($i = 1; $i <= 5; $i++) { ?>
      <?php if ($review['rating'] < $i) { ?>
      <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
      <?php } else { ?>
      <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
      <?php } ?>
      <?php } ?></td>
  </tr>
</table>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p class="empty"><?php 
// START HACK KERCH
//echo $text_no_reviews; 
// END HACK KERCH
?>
Please <a href="http://flemarc.com/index.php?route=account/login">login</a> or <a href="http://flemarc.com/index.php?route=account/register">register</a> to review
</p>
<?php } ?>
