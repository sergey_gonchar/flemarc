<a class="panel-url help_size" data-spanel="#size"><?php echo $url_size; ?></a>
<div id="size" class="spanel open-right">
   <div class="col-sm-12">
      <div class="panel-close"></div>
      <div id="close-size">
         <h3><?php echo $heading_title_size; ?></h3>
		 <p class="info"><?php echo $size_info; ?></p>
		 <div class="pblock">
		<ul class="nav nav-tabs">
               <li class="active"><a href="#size1" data-toggle="tab"><?php echo $text_woman; ?></a></li>
               <li><a href="#size2" data-toggle="tab"><?php echo $text_men; ?></a></li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active center" id="size1">
                  <img src="image/catalog/size/girl.png" title="<?php echo $text_woman; ?>" alt="<?php echo $text_woman; ?>">
               </div>
               <div class="tab-pane center" id="size2">			
                  <img src="image/catalog/size/men.png" title="<?php echo $text_men; ?>" alt="<?php echo $text_men; ?>">
               </div>
            </div>
			</div>
         <input type="hidden" name="size_product_name" value="<?php echo $name_product; ?>"/>
		 <input type="hidden" name="size_product_price" value="<?php if (!$special) { ?><?php echo $price; ?><?php } else { ?><?php echo $special; ?><?php } ?>"/>
		 <div class="form-group required">
            <input type="text" name="name" placeholder="<?php echo $entry_names; ?>" value="<?php echo $name_customer; ?>" id="size-name" class="form-control"/>
         </div>
         <div class="form-group required">
            <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value="<?php echo $email_customer; ?>" id="size-email" class="form-control"/>
         </div>
         <div class="form-group required">
            <textarea name="size" placeholder="<?php echo $entry_size; ?>" id="size-customer" class="form-control"></textarea>
         </div>
         <div class="form-group">
            <div class="fastorder"><input type="button" id="bsize" value="<?php echo $button_size; ?>" class="btn btn-center btn-primary"/></div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript"><!--
   $('#bsize').bind('click', function(){
   	$.ajax({
   		url: 'index.php?route=product/size/size',
   		type: 'post',
   		data: $('#size input[type=\'text\'], #size input[type=\'hidden\'], #size textarea').serializeArray(),
   		dataType: 'json',
   		success: function(json){
   			$('.error').remove();
   $('.text-danger').remove();
   			if (json['error']){
   				if (json['error']['name']){
   					$('#size-name').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_name'] + '</span>');
   					$('#size-name').closest('.form-group').addClass('has-error');
   				}
   				if (json['error']['email']){
   					$('#size-email').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_email'] + '</span>');
   					$('#size-email').closest('.form-group').addClass('has-error');
   				}
				if (json['error']['size']){
   					$('#size-customer').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_size'] + '</span>');
   					$('#size-customer').closest('.form-group').addClass('has-error');
   				}
   			}
   			if (json['success']) {
   				$('#close-size').hide().after('<div class="col-sm-12"><h3 class="success"><?php echo $title_success; ?></h3><p class="left"><?php echo $size_success; ?></p><b class="right"><?php echo $allbottom_success; ?>&nbsp;<?php echo $shop; ?></b><div class="time"</div></div>')
   			}
   		}
   	});
   });
   //--></script>		 