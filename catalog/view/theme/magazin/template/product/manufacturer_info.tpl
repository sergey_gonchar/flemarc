<?php echo $header; ?>
<?php $config = $this->registry->get('config');?>
<?php $lang = (int)$config->get('config_language_id');?>
<div class="container">
   <ul class="breadcrumb">
      <?php $breadlast = array_pop($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?> 
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?> 
      <li><span><?php echo $breadlast['text']; ?></span></li>
   </ul>
   <div class="row">
      <?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>">
         <?php echo $content_top; ?>         
         <div id="cat-row" class="row add_row">
		        <div class="col-md-10">
		        	<h1 class="pull-left"><?php echo $heading_title; ?></h1>
		        </div>         
	         	<div class="col-md-2">
	               <div class="btn-group">
	                  <?php if($config->get('control_view')== 1) { ?>
	                  <span type="button" id="list-view"></span>
	                  <span type="button" id="grid-view"></span>
	                  <?php } else { ?>
	                  <button type="button" id="list-view" class="hidden-xs no-btn list-view small-btn view" data-toggle="tooltip" title="<?php echo $button_list; ?>"></button>
	                  <button type="button" id="grid-view" class="hidden-xs no-btn grid-view small-btn view active" data-toggle="tooltip" title="<?php echo $button_grid; ?>"></button>
	                  <?php } ?>
	                  <?php if($config->get('control_compare')== 1) { ?>				  
	                  <a href="<?php echo $compare; ?>" id="button-compare" class="no-btn small-btn compare" data-toggle="tooltip" title="<?php echo $button_compare; ?>"><i class="fa fa-exchange"></i></a>
	                  <a href="<?php echo $compare; ?>" type="button" class="no-btn total" data-toggle="tooltip" title="<?php echo $text_compare; ?>"><span id="compare-total"><?php echo preg_replace('/\D/','',$text_compare); ?></span></a>
	                  <?php } ?>
	               </div>
	            </div>	            
            </div>            
         <?php if ($products) { ?>
         <div class="row">            
            <div class="col-md-4">
            <?php if ($thumb || $description) { ?>         
            <?php if ($thumb) { ?>
            <div class="col-md-4 col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-responsive" /></div>
            <?php } ?>
            <?php if (!empty($description)) { ?>
            <!-- START HACK KERCH -->
            <!--<div class="col-sm-10"><?php echo $description; ?></div>-->
            <!-- END HACK KERCH -->            
            <?php } ?>         
         <?php } ?>
            </div>
            
            <div class="col-md-2 flt col-md-offset-5 text-right width75">
               <select data-toggle="tooltip" title="<?php echo $text_sort; ?>" id="input-sort" class="customselect" onchange="location = this.value;">
                  <?php foreach ($sorts as $sorts) { ?>
                  <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                  <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                  <?php } ?>
                  <?php } ?>
               </select>
            </div>
            <div class="col-md-1 flt_c text-right width25">
               <select data-toggle="tooltip" title="<?php echo $text_limit; ?>" id="input-limit" class="customselect" onchange="location = this.value;">
                  <?php foreach ($limits as $limits) { ?>
                  <?php if ($limits['value'] == $limit) { ?>
                  <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                  <?php } ?>
                  <?php } ?>
               </select>
            </div>
         </div>
         <br />
         <div class="row">
            <?php foreach ($products as $product) { ?>
            <div class="product-layout product-list col-xs-12">
               <div class="product-thumb">
				<div class="inf_"><h4>
					<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
					<p class="des"><?php echo $product['description']; ?></p>
				</div>
					<?php if($product['special']) { ?>
					   <div class="sale">-<?php echo $product['sale']; ?>%</div>
				   <?php } ?>
                  <div class="action">
                     <?php if($config->get('control_wishlist')== 1) { ?>
					 <div class="wishlist">
                        <button class="no-btn" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button>
                     </div>
					 <?php } ?>
                     <?php if($config->get('control_compare')== 1) { ?>
                     <div class="compare">
                        <button class="no-btn" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
                     </div>
                     <?php } ?>
					 <?php if($config->get('control_quickview')== 1) { ?>
					 <div class="compare visible-md visible-lg">
					 <a rel="nofollow" href="index.php?route=product/quick_view&product_id=<?php echo $product['product_id']; ?>" class="quick-popup" data-toggle="tooltip" title="<?php echo $text_tooltip_quick; ?>"><i class="fa fa-external-link"></i></a>
					 </div>
					 <?php } ?>
                  </div>
				  <div class="image hover">
          <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
          </div>
                  <?php if ($product['thumb_swap']) { ?>
                  <div class="image "><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  <?php } else {?>
                  <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                  <?php } ?>
                  <?php if($config->get('control_quickview')== 2) { ?>
					<div class="contview">
					<a rel="nofollow" href="index.php?route=product/quick_view&product_id=<?php echo $product['product_id']; ?>" class="quick-popup">
					<div class="comparebtn visible-md visible-lg">
					   <?php echo $text_tooltip_quick; ?>
					</div>
					</a>
					</div>
					<?php } ?>
				  <div>
                     <div class="caption">
                        <?php if($config->get('control_stock')== 1) { ?><span class="stock" data-toggle="tooltip" title="<?php echo $product['stock']; ?>" style="background-color:<?php echo ($product['quantity'] > 0) ? '#9BD79B' : '#F07899'; ?>;"></span> <?php } ?>
                       <!--  <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4> -->
                        <?php if ($product['rating']) { ?>
                        <div class="rating">
                           <?php for ($i = 1; $i <= 5; $i++) { ?>
                           <?php if ($product['rating'] < $i) { ?>
                           <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                           <?php } else { ?>
                           <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                           <?php } ?>
                           <?php } ?>
                        </div>
                        <?php } ?>
                        <?php if ($product['price']) { ?>
                        <p class="price">
                           <?php if (!$product['special']) { ?>
                           <?php echo $product['price']; ?>
                           <?php } else { ?>
                           <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                           <?php } ?>
                           <?php if ($product['tax']) { ?>
                           <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                           <?php } ?>
                        </p>
                        <?php } ?>
						<div class="ad_c"><button class="no-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');">
                        <span class="cart-icon"><?php echo $button_cart; ?></span>
                        </button></div>
                     </div>
                  </div>
               </div>
               <?php foreach ($product['options'] as $option) { ?>
               <?php if ($option['type'] == 'radio') { ?>
               <?php $control_option_title = $config->get('control_option_title'); if ($option['name'] ==  $control_option_title[$lang] ) { ?>
               <div class="opsize">
                  <label class="control-label"><?php echo $option['name']; ?></label>
                  <div class="input-option">
                     <?php foreach ($option['product_option_value'] as $option_value) { ?>
                     <div class="opradio <?php if ($option_value['subtract']) { if ($option_value['quantity'] <= 0) echo 'out';} ?>"
                        data-toggle="tooltip" title="<?php if ($option_value['subtract']) {
                           if ($option_value['quantity'] > 0)
                             echo ''.$text_scl.' '.$option_value['quantity'].' '.$text_pcs;
                           else
                             echo ' '.$text_out_of_stock.' ';} ?>"><?php echo $option_value['name']; ?></div>
                     <?php } ?>
                  </div>
               </div>
               <?php } ?>               
               <?php } ?>
               <?php } ?>
            </div>
            <?php } ?>
         </div>
         <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
         </div>
         <?php } else { ?>
         <p class="empty"><?php echo $text_empty; ?></p>
         <div class="buttons">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
         </div>
         <?php } ?>
         <?php echo $content_bottom; ?>
      </div>
      <?php echo $column_right; ?>
   </div>
</div>
<?php echo $footer; ?>