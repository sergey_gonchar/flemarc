<div id="outstock" class="spanel open-right">
   <div class="col-sm-12">
      <div class="panel-close"></div>
      <div id="close-outstock">
         <h3><?php echo $heading_title_outstock; ?></h3>
         <p class="info"><?php echo $outstock_info; ?></p>
         <div class="pblock">
            <div class="img">
               <img src="<?php echo $thumb_fixed; ?>" title="<?php echo $name_product; ?>" alt="<?php echo $name_product; ?>" />
            </div>
            <div class="prinfo">
               <div class="dopproduct"><?php echo $name_product; ?></div>
               <form name="order">
                  <input type="hidden" class="text-option" name="value_name" value="">
               </form>
               <div id="outoption"></div>
               <?php if (!$special) { ?>
               <div class="bold"><?php echo $price; ?></div>
               <?php } else { ?>
               <div><span class="through"><?php echo $price; ?></span></div>
               <div class="bold"><?php echo $special; ?></div>
               <?php } ?>
            </div>
         </div>
         <input type="hidden" name="out_product_name" value="<?php echo $name_product; ?>"/>
         <input type="hidden" name="out_product_price" value="<?php if (!$special) { ?><?php echo $price; ?><?php } else { ?><?php echo $special; ?><?php } ?>"/>
         <div class="form-group required">
            <input type="text" name="name" placeholder="<?php echo $entry_names; ?>" value="<?php echo $name_customer; ?>" id="outstock-name" class="form-control"/>
         </div>
         <div class="form-group required">
            <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value="<?php echo $email_customer; ?>" id="outstock-email" class="form-control"/>
         </div>
         <div class="form-group">
            <textarea name="comment" placeholder="<?php echo $entry_comment; ?>" id="outstock-comment" class="form-control"></textarea>
         </div>
         <div class="form-group">
            <div class="outstock"><input type="button" id="boutstock" value="<?php echo $button_outstock; ?>" class="btn btn-center btn-primary"/></div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript"><!--
   $('#boutstock').bind('click', function(){
   	$.ajax({
   		url: 'index.php?route=product/outstock/outstock',
   		type: 'post',
   		data: $('#outstock input[type=\'text\'], #outstock input[type=\'hidden\'], #outstock textarea').serializeArray(),
   		dataType: 'json',
   		success: function(json){
   			$('.error').remove();
   $('.text-danger').remove();
   			if (json['error']){
   				if (json['error']['name']){
   					$('#outstock-name').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_name'] + '</span>');
   					$('#outstock-name').closest('.form-group').addClass('has-error');
   				}
   				if (json['error']['email']){
   					$('#outstock-email').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_email'] + '</span>');
   					$('#outstock-email').closest('.form-group').addClass('has-error');
   				}
   	if (json['error']['url']){
   					$('#outstock-url').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_url'] + '</span>');
   					$('#outstock-url').closest('.form-group').addClass('has-error');
   				}
   			}
   			if (json['success']) {
   				$('#close-outstock').hide().after('<div class="col-sm-12"><h3 class="success"><?php echo $title_success; ?></h3><p class="left"><?php echo $outstock_success; ?></p><b class="right"><?php echo $allbottom_success; ?>&nbsp;<?php echo $shop; ?></b><div class="time"</div></div>')
   			}
   		}
   	});
   });
   //--></script>