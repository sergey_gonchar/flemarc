<?php echo $header; ?>
<?php $config = $this->registry->get('config');?>
<?php $lang = (int)$config->get('config_language_id');?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 topcat">
            <ul class="breadcrumb pull-left">
                <?php $breadlast = array_pop($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
                <li><span><?php echo $breadlast['text']; ?></span></li>
            </ul>

        </div>
    </div>
    <div id="cat-row" class="row add_row">
        <div class="col-md-9">
            <h1 class="pull-left"><?php echo $heading_title; ?></h1>
        </div>
        <div class="col-md-3">
            <div class="btn-group">
                <?php if($config->get('control_view')== 1) { ?>
                <span type="button" id="list-view"></span>
                <span type="button" id="grid-view"></span>
                <?php } else { ?>
                <button type="button" id="list-view" class="hidden-xs no-btn list-view small-btn view" data-toggle="tooltip" title="<?php echo $button_list; ?>"></button>
                <button type="button" id="grid-view" class="hidden-xs no-btn grid-view small-btn view active" data-toggle="tooltip" title="<?php echo $button_grid; ?>"></button>
                <?php } ?>
                <?php if($config->get('control_compare')== 1) { ?>
                <a href="<?php echo $compare; ?>" id="button-compare" class="no-btn small-btn compare" data-toggle="tooltip" title="<?php echo $button_compare; ?>"><i class="fa fa-exchange"></i></a>
                <a href="<?php echo $compare; ?>" type="button" class="no-btn total" data-toggle="tooltip" title="<?php echo $text_compare; ?>"><span id="compare-total"><?php echo preg_replace('/\D/','',$text_compare); ?></span></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <div  class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <?php echo $content_top; ?>
            <!--          <?php if ($categories) { ?>
                     <div class="block-category">
                        <?php if ($config->get('control_imgcategory')==1) { ?>
                        <div class="owl-carousel category_carousel">
                           <?php foreach ($categories as $category) { ?>
                           <a class="category" href="<?php echo $category['href']; ?>">
                              <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" />
                              <div class="catname"><?php echo $category['name']; ?></div>
                           </a>
                           <?php } ?>
                        </div>
                        <script type="text/javascript">
                           $(document).ready(function() {
                              $("#content .category_carousel").owlCarousel({
                                 itemsCustom : [[0,3],[470,5],[760,5],[980,5],[1100,6]],
                                 navigation : true,
                                 navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
                                 scrollPerPage : true,
                                 pagination: false,
                           stopOnHover: true,
                           autoPlay:3000
                               });
                              });
                        </script>
                        <?php } else {?>
            <?php foreach ($categories as $category) { ?>
            <a class="category" href="<?php echo $category['href']; ?>">
            <span><?php echo $category['name']; ?></span>
            </a>
            <?php } ?>
                        <?php } ?>
                     </div>
                     <?php } ?> -->
            <?php if ($products) { ?>
            <div class="row">
                <div class="col-md-5 text-right">
                </div>
                <div class="fr3 col-md-1 flt_c text-right width25">
                    <select data-toggle="tooltip" title="<?php echo $text_limit; ?>" id="input-limit" class="form-control customselect" onchange="location = this.value;">
                        <?php foreach ($limits as $limits) { ?>
                        <?php if ($limits['value'] == $limit) { ?>
                        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-2 text-right width75">
                    <span id="button-filter2"  class="panel-url no-btn url-btn" data-toggle="tooltip" title="<?php echo $text_tooltip_filter; ?>" data-spanel="#filter"><?php echo $text_filter; ?></span>
                </div>
                <div class="fr2 col-md-1 flt text-right">
                    <select id="input-brand" class="form-control customselect" onchange="location = this.value;">
                        <?php if(isset($manufacturers))foreach ($manufacturers as $filter) { ?>
                        <?php if ($filter['value'] == $manufacturer) { ?>
                        <option value="<?php echo $filter['href']; ?>" selected="selected"><?php echo $filter['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $filter['href']; ?>"><?php echo $filter['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                    <!--      <div class="btn-group">
                            <?php if($config->get('control_view')== 1) { ?>
                            <span type="button" id="list-view"></span>
                            <span type="button" id="grid-view"></span>
                            <?php } else { ?>
                            <button type="button" id="list-view" class="hidden-xs no-btn list-view small-btn view" data-toggle="tooltip" title="<?php echo $button_list; ?>"></button>
                            <button type="button" id="grid-view" class="hidden-xs no-btn grid-view small-btn view active" data-toggle="tooltip" title="<?php echo $button_grid; ?>"></button>
                            <?php } ?>
                            <?php if($config->get('control_compare')== 1) { ?>
                            <a href="<?php echo $compare; ?>" class="no-btn small-btn compare" data-toggle="tooltip" title="<?php echo $text_compare; ?>"><i class="fa fa-exchange"></i></a>
                            <span id="compare-total"><?php echo preg_replace('/\D/','',$text_compare); ?></span>
                            <?php } ?>
                            <?php if($config->get('control_filter')== 1) { ?>
                            <span id="button-filter2"  class="panel-url no-btn url-btn" data-toggle="tooltip" title="<?php echo $text_tooltip_filter; ?>" data-spanel="#filter"><?php echo $text_filter; ?></span>
                            <button type="button" style="display: none;" id="button-clear2" class="no-btn fclear" data-toggle="tooltip" title="<?php echo $text_tooltip_clear_filter; ?>">Х</button>
                            <?php } ?>
                            <span style="display: none;" id="viewed-view" class="panel-url no-btn url-btn" data-spanel="#viewed"><?php echo $text_viewed; ?></span>
                         </div> -->
                </div>
                <div class="fr1 col-md-1 flt text-right width75">
                    <select data-toggle="tooltip" title="<?php echo $text_sort; ?>" id="input-sort" class="form-control customselect" onchange="location = this.value;">
                        <?php foreach ($sorts as $sorts) { ?>
                        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                        <?php } else { ?>
                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                        <?php } ?>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <br />
            <div class="row">
                <?php include('catalog/view/theme/magazin/template/common/products_list.tpl'); ?>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
            <div class="topfooter">
                <div class="container" style="width: 100%">
                    <?php echo $content_bottom; ?>
                    <?php echo $column_right; ?>
                </div>
            </div>
            <?php if ($description) { ?>
            <hr>
            <div class="row">
                <div class="col-sm-12">
                    <div class="cat-info"><?php echo $description; ?></div>
                </div>
            </div>
            <?php } ?>
            <?php } ?>
            <?php if (!$categories && !$products) { ?>
            <p class="empty"><?php echo $text_empty; ?></p>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
            <?php } ?>

        </div>

    </div>
</div>
<?php echo $footer; ?>