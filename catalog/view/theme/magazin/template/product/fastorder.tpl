<a class="panel-url btn btn-default" data-spanel="#fastorder"><?php echo $heading_title; ?></a>
<div id="fastorder" class="spanel open-right">
   <div class="col-sm-12">
      <div class="panel-close"></div>
      <div id="wrapper">
	  <div id="close-fastorder">
         <h3><?php echo $heading_title; ?></h3>
         <p class="info"><?php echo $fastorder_info; ?></p>
         <div class="pblock">
            <div class="img">
               <img src="<?php echo $thumb_fixed; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
            </div>
            <div class="prinfo">
               <div class="dopproduct"><?php echo $name_product; ?></div>
               <?php if (!$special) { ?>
               <div class="bold"><?php echo $price; ?></div>
               <?php } else { ?>
               <div><span class="through"><?php echo $price; ?></span></div>
               <div class="bold"><?php echo $special; ?></div>
               <?php } ?>
            </div>
         </div>
         <input type="hidden" name="order_product_name" value="<?php echo $name_product; ?>"/>
         <input type="hidden" name="order_product_price" value="<?php if (!$special) { ?><?php echo $price; ?><?php } else { ?><?php echo $special; ?><?php } ?>"/>
         <div class="form-group">
            <input type="text" name="order_name" placeholder="<?php echo $entry_names; ?>" value="<?php echo $name_customer; ?>" id="order-name" class="form-control"/>
         </div>
         <div class="form-group">
            <input type="text" name="order_phone" placeholder="<?php echo $entry_phone; ?>" value="<?php echo $phone_customer; ?>" id="order-phone" class="form-control"/>
         </div>
         <div class="form-group">
            <textarea name="order_comment" placeholder="<?php echo $entry_comment; ?>" id="order-comment" class="form-control"></textarea>
         </div>
         <div class="form-group">
            <div class="fastorder"><input type="button" id="bfastorder" value="<?php echo $button_fastorder; ?>" class="btn btn-center btn-primary"/></div>
         </div>
      </div>
	  </div>
   </div>
</div>
<script type="text/javascript"><!--
   $('#bfastorder').bind('click', function(){
   	$.ajax({
   		url: 'index.php?route=product/fastorder/fastorder',
   		type: 'post',
   		data: $('#fastorder input[type=\'text\'], #fastorder input[type=\'hidden\'], #fastorder textarea').serializeArray(),
   		dataType: 'json',
   		success: function(json){
   			$('.error').remove();
   $('.text-danger').remove();
   			if (json['error']){
   				if (json['error']['quick_name']){
   					$('#order-name').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_name'] + '</span>');
   					$('#order-name').closest('.form-group').addClass('has-error');
   				}
   				if (json['error']['quick_phone']){
   					$('#order-phone').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_telephone'] + '</span>');
   					$('#order-phone').closest('.form-group').addClass('has-error');
   				}
   			}
   			if (json['success']) {
   				$('#close-fastorder').hide().after("<div class='col-sm-12'><h3 class='success'><?php echo $order_success; ?></h3><p class='left'><?php echo $order_success_info; ?></p><b class='right'><?php echo $allbottom_success; ?>&nbsp;<?php echo $shop; ?></b><div class='time'</div></div>")
   			}
   		}
   	});
   });
   jQuery(function($){
   $("#order-phone").mask("+7 (999) 999-9999");
   });
   //--></script>