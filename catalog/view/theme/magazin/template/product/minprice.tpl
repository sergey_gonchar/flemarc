<p class="panel-url hminprice" data-spanel="#minprice"><?php echo $heading_title_minprice; ?></p>
<div id="minprice" class="spanel open-right">
   <div class="col-sm-12">
      <div class="panel-close"></div>
      <div id="close-minprice">
         <h3><?php echo $heading_title_minprice; ?></h3>
         <p class="info"><?php echo $minprice_info; ?></p>
         <div class="pblock">
            <div class="img">
               <img src="<?php echo $thumb_fixed; ?>" title="<?php echo $name_product; ?>" alt="<?php echo $name_product; ?>" />
            </div>
            <div class="prinfo">
               <div class="dopproduct"><?php echo $name_product; ?></div>
               <?php if (!$special) { ?>
               <div class="bold"><?php echo $price; ?></div>
               <?php } else { ?>
               <div><span class="through"><?php echo $price; ?></span></div>
               <div class="bold"><?php echo $special; ?></div>
               <?php } ?>
            </div>
         </div>
         <input type="hidden" name="min_product_name" value="<?php echo $name_product; ?>"/>
         <input type="hidden" name="min_product_price" value="<?php if (!$special) { ?><?php echo $price; ?><?php } else { ?><?php echo $special; ?><?php } ?>"/>
         <div class="form-group required">
            <input type="text" name="name" placeholder="<?php echo $entry_names; ?>" value="<?php echo $name_customer; ?>" id="minprice-name" class="form-control"/>
         </div>
         <div class="form-group required">
            <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value="<?php echo $email_customer; ?>" id="minprice-email" class="form-control"/>
         </div>
         <div class="form-group">
            <textarea name="comment" placeholder="<?php echo $entry_comment; ?>" id="minprice-comment" class="form-control"></textarea>
         </div>
         <div class="form-group required">
            <input type='text' name="url" placeholder="<?php echo $entry_url; ?>" class="form-control" id="minprice-url" />
         </div>
         <div class="form-group">
            <div class="fastorder"><input type="button" id="bminprice" value="<?php echo $button_minprice; ?>" class="btn btn-center btn-primary"/></div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript"><!--
   $('#bminprice').bind('click', function(){
   	$.ajax({
   		url: 'index.php?route=product/minprice/minprice',
   		type: 'post',
   		data: $('#minprice input[type=\'text\'], #minprice input[type=\'hidden\'], #minprice textarea').serializeArray(),
   		dataType: 'json',
   		success: function(json){
   			$('.error').remove();
   $('.text-danger').remove();
   			if (json['error']){
   				if (json['error']['name']){
   					$('#minprice-name').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_name'] + '</span>');
   					$('#minprice-name').closest('.form-group').addClass('has-error');
   				}
   				if (json['error']['email']){
   					$('#minprice-email').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_email'] + '</span>');
   					$('#minprice-email').closest('.form-group').addClass('has-error');
   				}
   	if (json['error']['url']){
   					$('#minprice-url').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_url'] + '</span>');
   					$('#minprice-url').closest('.form-group').addClass('has-error');
   				}
   			}
   			if (json['success']) {
   				$('#close-minprice').hide().after('<div class="col-sm-12"><h3 class="success"><?php echo $title_success; ?></h3><p class="left"><?php echo $minprice_success; ?></p><b class="right"><?php echo $allbottom_success; ?>&nbsp;<?php echo $shop; ?></b><div class="time"</div></div>')
   			}
   		}
   	});
   });
   //--></script>