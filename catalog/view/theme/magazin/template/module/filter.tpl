<?php global $config; if($config->get('control_filter')== 1) {	?>
<div id="filter" class="spanel open-left"> 
 <div class="block-filter">
      <div class="panel-close hidden-lg"></div>
	  <h3><?php echo $heading_title; ?></h3>
      <div class="filters-group">
         <?php foreach ($filter_groups as $filter_group) { ?>
         <div class="filter-heading" data-toggle="collapse" data-target="#filter-group<?php echo $filter_group['filter_group_id']; ?>"><?php echo $filter_group['name']; ?></div>
         <div id="filter-group<?php echo $filter_group['filter_group_id']; ?>" class="collapse in">
            <?php foreach ($filter_group['filter'] as $filter) { ?>
            <div class="checkbox">
          <label>
            <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
            <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" checked="checked" class="option-input"/>
            <?php echo $filter['name']; ?>
            <?php } else { ?>
            <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" class="option-input"/>
            <?php echo $filter['name']; ?>
            <?php } ?>
          </label>
        </div>
            <?php } ?>
         </div>
         <?php } ?>
         <div class="button-zone">
            <button type="button" id="button-filter" class="btn small-btn btn-default"><?php echo $button_filter; ?></button>
            <button type="button" style="display: none;" id="button-clear" class="no-btn small-btn btn-close"></button>
         </div>
      </div>
   </div>
 </div>  
<?php } else { ?>
<div class="block-filter">	  
      <div class="filters-group solid">
	  <h3><?php echo $heading_title; ?></h3>
         <?php foreach ($filter_groups as $filter_group) { ?>
         <div class="filter-heading" data-toggle="collapse" data-target="#filter-group<?php echo $filter_group['filter_group_id']; ?>"><?php echo $filter_group['name']; ?></div>
         <div id="filter-group<?php echo $filter_group['filter_group_id']; ?>" class="collapse in">
            <?php foreach ($filter_group['filter'] as $filter) { ?>
            <div class="checkbox">
          <label>
            <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
            <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" checked="checked" class="option-input"/>
            <?php echo $filter['name']; ?>
            <?php } else { ?>
            <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" class="option-input"/>
            <?php echo $filter['name']; ?>
            <?php } ?>
          </label>
        </div>
            <?php } ?>
         </div>
         <?php } ?>
         <div class="button-zone">
            <button type="button" id="button-filter" class="btn small-btn btn-default"><?php echo $button_filter; ?></button>
            <button type="button" style="display: none;" id="button-clear" class="no-btn small-btn btn-close"></button>
         </div>
      </div>
   </div>
<?php } ?>
 <script type="text/javascript"><!--
      $('#button-filter').on('click', function() {
      	filter = [];
      
      	$('input[name^=\'filter\']:checked').each(function(element) {
      		filter.push(this.value);
      	});
      
      	location = '<?php echo $action; ?>&filter=' + filter.join(',');
      });
      
      $('#button-clear').on('click', function() {
      	location = '<?php echo $action; ?>';
      });
      //--></script>