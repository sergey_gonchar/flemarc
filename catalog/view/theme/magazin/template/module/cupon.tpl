<div class="cuponmod">
   <div class="heading_cupon"><?php echo $heading_title; ?></div>
   <div class="gift"><?php echo $cupon_sale; ?></div>
   <div class="cupon"><?php echo $cupon_code; ?></div>
     <button class="no-btn panel-url copy" data-spanel="#cuponmod" onclick="CopyToClipboard('<?php echo $cupon_code; ?>')"><?php echo $text_copy; ?></button>
   <p class="cupon-info"><?php echo $cupon_info; ?></p>
</div>
<div id="cuponmod" class="spanel open-right">
   <div class="col-sm-12">
      <div class="panel-close hidden-lg hidden-md"></div>
      <h3 class="success"><?php echo $heading_success_cupon; ?></h3>
      <p class="left"><?php echo $info_success_cupon; ?></p>
      <div class="gift"><?php echo $cupon_sale; ?></div>
   </div>
</div>
<script>
   CopyToClipboard = function(text){
    var $t = $('<textarea />');
    $t.val(text).appendTo('body');
    $t.select();
        if (document.execCommand('copy')) {
            $t.remove();  
        }
    };   
    $('.copy').mousedown(function() {
     $(".copy").click();
    });
</script>
