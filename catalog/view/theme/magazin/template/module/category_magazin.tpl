<div id="magazincategory" class="list-group solid">
<h3><?php echo $heading_title; ?></h3>
   <ul>
      <?php foreach ($categories as $category) { ?>
      <?php if ($category['children']) { ?>
      <?php if ($category['category_id'] == $category_id) { ?>
      <li class="level1 headcat active">
         <style>.level1.active > a span{padding: 6px 14px;background: #EEEEEE;border-radius: 30px;}</style>
         <a href="<?php echo $category['href']; ?>" class="head active"><span><?php echo $category['name']; ?></span></a>
         <?php } else { ?>
      <li class="level1 headcat">
         <a href="<?php echo $category['href']; ?>" class="head"><span><?php echo $category['name']; ?></span></a>
         <?php } ?>
         <ul style="display:none">
            <?php foreach ($category['children'] as $child) { ?>
            <?php if($child['sister_id']){ ?>
            <?php if ($child['category_id'] == $child_id) { ?>
            <li class="level2 headcat active">
               <style>.level1.active > a span{padding: 0px;background: none;border-radius: 30px;}.level2.active > a span{padding: 6px 14px;background: #EEEEEE;border-radius: 30px;}</style>
               <a href="<?php echo $child['href']; ?>" class="active"><span>&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></span></a>
               <?php } else { ?>
            <li class="level2 headcat">
               <a href="<?php echo $child['href']; ?>"><span>&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></span></a>
               <?php } ?>
               <ul style="display:none">
                  <?php foreach($child['sister_id'] as $sisters) { ?>
                  <?php if ($sisters['xyi'] == $sisters_id) { ?>
                  <li class="level3 active">
                     <style>.level2.active > a span{padding: 0px;background: none;border-radius: 30px;}.level3.active > a span{padding: 6px 14px;background: #EEEEEE;border-radius: 30px;}</style>
                     <a href="<?php echo $sisters['href']; ?>" class="sub active" style="padding-left: 15px"><span> - <?php echo $sisters['name']; ?></span></a>
                     <?php } else { ?>
                  <li class="level3">
                     <a href="<?php echo $sisters['href']; ?>" class="sub <?php echo $sisters['xyi']; ?>" style="padding-left: 15px"><span> - <?php echo $sisters['name']; ?></span></a>
                     <?php } ?>
                  </li>
                  <?php } ?>
               </ul>
               <?php } else { ?>
               <?php if ($child['category_id'] == $child_id) { ?>
            <li class="level2 active">
               <style>.level1.active > a span{padding: 0px;background: none;border-radius: 30px;}.level2.active > a span{padding: 6px 14px;background: #EEEEEE;border-radius: 30px;}</style>
               <a href="<?php echo $child['href']; ?>" class="active"><span>&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></span></a>
               <?php } else { ?>
            <li class="level2">
               <a href="<?php echo $child['href']; ?>"><span>&nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></span></a>
               <?php } ?>
               <?php } ?>	
            </li>
            <?php } ?>
         </ul>
         <?php } else {?>
         <?php if ($category['category_id'] == $category_id) { ?>
      <li class="level1 active">
         <style>.level1.active > a span{padding: 6px 14px;background: #EEEEEE;border-radius: 30px;}</style>
         <a href="<?php echo $category['href']; ?>" class="active"><span><?php echo $category['name']; ?></span></a>
         <?php } else { ?>
      <li class="level1">
         <a href="<?php echo $category['href']; ?>"><span><?php echo $category['name']; ?></span></a>
         <?php } ?>
         <?php } ?>
      </li>
      <?php } ?>
   </ul>
</div>
<script type="text/javascript">
		
	$(document).ready(function(){	
		// menu
	$('#magazincategory li.active').addClass('open').children('ul').show();
	$('#magazincategory .headcat > a').on('click', function(){
	$(this).removeAttr('href');
		
	var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(400);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(400);
			element.siblings('li').children('ul').slideUp(400);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(400);
		}
	});
		
	});	
		
	</script>