<div class="newhead">
   <h2><?php echo $heading_title; ?></h2>
   <span><a class="border" href="<?php echo $all_href; ?>"><?php echo $all_title; ?></a></span> 
</div>
<div class="row block_news">
   <div class="owl-carousel news_carousel">
      <?php foreach ($all_news as $news) { ?>
      <div class="product-thumb">
         <div class="news">
            <?php if ($news['thumb']) { ?>
			<img src="<?php echo $news['thumb']; ?>" alt="<?php echo $news['title']; ?>" title="<?php echo $news['title']; ?>" class="img-news" />
            <?php } else { ?>
			<img src="image/no-img-news.jpg" alt="<?php echo $news['title']; ?>" title="<?php echo $news['title']; ?>" class="img-news" />
			<?php } ?>
			<div class="newsinfo">
               <div class="newdata"><?php echo $news['date_added']; ?></div>
               <a href="<?php echo $news['view']; ?>"><?php echo $news['title']; ?></a>
               <div class="newsds">
                  <?php echo $news['description']; ?>
               </div>
            </div>
         </div>
      </div>
      <?php } ?>
   </div>
</div>
<script type="text/javascript">
   $(document).ready(function() {
   	$("#content .news_carousel").owlCarousel({
   		itemsCustom : [[320, 2],[600, 2],[768, 2],[992, 2],[1170, 3]],											   
   		navigation : true,
   		navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
   		scrollPerPage : true,
   		pagination: false
       });
   	$('.newdata').each(function(){
    var $p = $(this);
    $p.html($p.html().replace(/^(\w+)/, '<span>$1</span>')); });
   
   });
</script>