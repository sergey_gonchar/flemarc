<form id="fsubscription" class="solid">
   <div id="close-subscription">
      <input type="hidden" name="language_id" id="language_id" value="<?php echo $language_id; ?>" />
      <input type="hidden" name="store_id" id="store_id" value="<?php echo $store_id; ?>" />
      <p class="sub-bl"><?php echo html_entity_decode($subscription_cupon[$lang], ENT_QUOTES, 'UTF-8'); ?></p>
      <div class="sub-bl">
         <input id="sname" class="input-sub" name="sname" value="<?php echo $name_customer; ?>" placeholder="<?php echo $entry_sname; ?>" type="text">
      </div>
      <div class="sub-bl">
         <input id="semail" class="input-sub" name="semail" value="<?php echo $email_customer; ?>" placeholder="<?php echo $entry_semail; ?>" type="text">
      </div>
      <div class="sub-bl">
         <input type="button" id="bsubscription" value="<?php echo $button_subscription; ?>" class="btn btn-primary">
      </div>
   </div>
</form>

<script type="text/javascript"><!--
   $('#bsubscription').bind('click', function(){
   	$.ajax({
   		url: 'index.php?route=module/subscription/subscribe',
   		type: 'post',
   		data: $('#fsubscription').serialize(),
   		dataType: 'json',
   		success: function(json){
   			$('.error').remove();
            $('.text-danger').remove();
   			if (json['error']){
   				if (json['error']['sname']){
   					$('#sname').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_sname'] + '</span>');
   					$('#sname').closest('.form-group').addClass('has-error');
   				}
   				if (json['error']['semail']){
   					$('#semail').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_semail'] + '</span>');
   					$('#semail').closest('.form-group').addClass('has-error');
   				}
				if (json['error']['sdemail']){
   					$('#semail').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_sdemail'] + '</span>');
   					$('#semail').closest('.form-group').addClass('has-error');
   				}
   			}
   			if (json['success']) {
				$('#close-subscription').hide().after('<div class="success" style="text-align: center;"><?php echo $subscription_success; ?></div>')
   			}
   		}
   	});
   });
   //--></script>