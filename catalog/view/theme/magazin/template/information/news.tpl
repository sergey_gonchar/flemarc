<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
      <?php $breadlast = array_pop($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?> 
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?> 
      <li><span><?php echo $breadlast['text']; ?></span></li>
   </ul>
   <div class="row">
      <?php echo $column_left; ?>
      <?php if ($column_left && $column_right) { ?>
      <?php $class = 'col-sm-6'; ?>
      <?php } elseif ($column_left || $column_right) { ?>
      <?php $class = 'col-sm-9'; ?>
      <?php } else { ?>
      <?php $class = 'col-sm-12'; ?>
      <?php } ?>
      <div id="content" class="<?php echo $class; ?>">
         <?php echo $content_top; ?>
         <h1><?php echo $heading_title; ?></h1>
         <div class="wbg">
            <?php if ($image) { ?>
            <div class="img-info">
               <img src="<?php echo $image; ?>" alt="<?php echo $heading_title; ?>" />
            </div>
            <?php } ?>
            <p><?php echo $description; ?></p>
         </div>
         <div class="col-sm-12">
            <div class="center">
               <p class="bold"><?php echo $text_soc; ?></p>
               <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>
               <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="small" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki" data-yashareTheme="counter"></div>
            </div>
         </div>
         <?php echo $content_bottom; ?>
      </div>
      <?php echo $column_right; ?>
   </div>
</div>
<?php echo $footer; ?> 