<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
      <?php $breadlast = array_pop($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?> 
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?> 
      <li><span><?php echo $breadlast['text']; ?></span></li>
   </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      
	  <div class="newhead">
  <h2><?php echo $heading_title; ?></h2>
  </div>
<div class="row block_news owl-item">

  <?php foreach ($all_news as $news) { ?>
  <div class="col-sm-4 nopad">
         <div class="product-thumb news">
            <?php if ($news['image']) { ?>
			<img src="<?php echo $news['image']; ?>" alt="<?php echo $news['title']; ?>" title="<?php echo $news['title']; ?>" class="img-news" />
            <?php } else { ?>
			<img src="image/no-img-news.jpg" alt="<?php echo $news['title']; ?>" title="<?php echo $news['title']; ?>" class="img-news" />
			<?php } ?>
			<div class="newsinfo">
               <div class="newdata"><?php echo $news['date_added']; ?></div>
               <a href="<?php echo $news['view']; ?>"><?php echo $news['title']; ?></a>
               <div class="newsds">
                  <?php echo $news['description']; ?>
               </div>
            </div>
         </div>
      </div>
 <?php } ?>
 
</div>
<script>
jQuery(document).ready(function(){
  $('.newdata').each(function(){
    var $p = $(this);
    $p.html($p.html().replace(/^(\w+)/, '<span>$1</span>')); });
});
</script>
	  <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
	  <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?> 