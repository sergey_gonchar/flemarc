<?php echo $header; ?>
<?php $config = $this->registry->get('config');?>
<?php $lang = (int)$config->get('config_language_id');?>
<div class="container">
  <ul class="breadcrumb">
      <?php $breadlast = array_pop($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?> 
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?> 
      <li><span><?php echo $breadlast['text']; ?></span></li>
   </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>	
	<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
        <h3><?php echo $text_location; ?></h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="contact-info col-sm-3">
                    <?php if ($image) { ?>
                        <div class="col-sm-12">
                            <img src="<?php echo $image; ?>" alt="<?php echo $store; ?>" title="<?php echo $store; ?>" class="img-thumbnail" />
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-6">
                            <strong><?php echo $store; ?></strong>
                        </div>
                        <div class="col-md-6">
                            <strong><?php echo $text_telephone; ?></strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <address>
                                <?php echo $address; ?>
                            </address>
                        </div>
                        <div class="col-md-6">
                            <?php echo $telephone; ?>
                            <?php if ($fax) { ?>
                                <br />
                                <strong><?php echo $text_fax; ?></strong>
                                <br />
                                <?php echo $fax; ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if ($open) { ?>
                                <strong><?php echo $text_open; ?></strong><br />
                                <?php echo $open; ?>
                                <br />
                                <br />
                            <?php } ?>
                            <?php if ($comment) { ?>
                            <strong><?php echo $text_comment; ?></strong><br />
                                <?php echo $comment; ?>
                            <?php } ?>
                        </div>   
                        <div class="col-sm-12">
                            <a id="reviewf" class="panel-url btn btn-primary" data-spanel="#review-form">написать нам</a>      
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <?php if ($locations) { ?>
      <h3><?php echo $text_store; ?></h3>
      <div class="panel-group" id="accordion">
        <?php foreach ($locations as $location) { ?>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title"><a href="#collapse-location<?php echo $location['location_id']; ?>" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"><?php echo $location['name']; ?> <i class="fa fa-caret-down"></i></a></h4>
          </div>
          <div class="panel-collapse collapse" id="collapse-location<?php echo $location['location_id']; ?>">
            <div class="panel-body">
              <div class="row">
                <?php if ($location['image']) { ?>
                <div class="col-sm-3"><img src="<?php echo $location['image']; ?>" alt="<?php echo $location['name']; ?>" title="<?php echo $location['name']; ?>" class="img-thumbnail" /></div>
                <?php } ?>
                <div class="col-sm-3"><strong><?php echo $location['name']; ?></strong><br />
                  <address>
                  <?php echo $location['address']; ?>
                  </address>
                  <?php if ($location['geocode']) { ?>
                  <a href="https://maps.google.com/maps?q=<?php echo urlencode($location['geocode']); ?>&hl=<?php echo $geocode_hl; ?>&t=m&z=15" target="_blank" class="btn btn-info"><i class="fa fa-map-marker"></i> <?php echo $button_map; ?></a>
                  <?php } ?>
                </div>
                <div class="col-sm-3"> <strong><?php echo $text_telephone; ?></strong><br>
                  <?php echo $location['telephone']; ?><br />
                  <br />
                  <?php if ($location['fax']) { ?>
                  <strong><?php echo $text_fax; ?></strong><br>
                  <?php echo $location['fax']; ?>
                  <?php } ?>
                </div>
                <div class="col-sm-3">
                  <?php if ($location['open']) { ?>
                  <strong><?php echo $text_open; ?></strong><br />
                  <?php echo $location['open']; ?><br />
                  <br />
                  <?php } ?>
                  <?php if ($location['comment']) { ?>
                  <strong><?php echo $text_comment; ?></strong><br />
                  <?php echo $location['comment']; ?>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="review-form" class="spanel open-right">
        <div class="col-sm-12">
		<div id="creview-form" class="panel-close"></div>
		<fieldset>
          <h3><?php echo $text_contact; ?></h3>
          <div class="form-group required">
              <input type="text" name="name" placeholder="<?php echo $entry_name; ?>" value="<?php echo $name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><i class="fa fa-arrow-up"></i><?php echo $error_name; ?></div>
              <?php } ?>
          </div>
          <div class="form-group required">
              <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value="<?php echo $email; ?>" id="input-email" class="form-control" />
              <?php if ($error_email) { ?>
              <div class="text-danger"><i class="fa fa-arrow-up"></i><?php echo $error_email; ?></div>
              <?php } ?>
          </div>
          <div class="form-group required">
              <textarea name="enquiry" placeholder="<?php echo $entry_enquiry; ?>" rows="10" id="input-enquiry" class="form-control"><?php echo $enquiry; ?></textarea>
              <?php if ($error_enquiry) { ?>
              <div class="text-danger"><i class="fa fa-arrow-up"></i><?php echo $error_enquiry; ?></div>
              <?php } ?>
          </div>
          <?php echo $captcha; ?>
        </fieldset>
        <div class="buttons">
          <div class="pull-right">
            <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
          </div>
        </div>
       </div>
	  </form>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
	<script type="text/javascript"><!--
   <?php if ($error_name || $error_email || $error_enquiry) { ?>
        $("html").addClass("open");
        $("body").addClass("open");
		$("#review-form").addClass("open");
		$("#block-close").fadeIn(1000);
  <?php } ?>
   --></script>
</div>
<?php echo $footer; ?>
