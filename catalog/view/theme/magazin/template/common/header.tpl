<!DOCTYPE html>
<?php
if(isset($_COOKIE["customer_id_cookie"]) && $_COOKIE["customer_id_cookie"]!=''){
$_SESSION['default']['customer_id']=$_COOKIE["customer_id_cookie"];
?>
<script>
    if( sessionStorage.reloadHeader ){
    }
    else{
        sessionStorage.reloadHeader = 1;
        location.reload();
    }
</script>
<?php
}
?>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8">
   <![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9">
   <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
        <base href="<?php echo $base; ?>" />
        <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
        <?php } ?>
        <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
        <?php } ?>
        <meta d32ata-property_mdp="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
        <meta d32ata-property_mdp="og:type" content="website" />
        <meta d32ata-property_mdp="og:url" content="<?php echo $og_url; ?>" />
        <?php if ($og_image) { ?>
        <meta d32ata-property_mdp="og:image" content="<?php echo $og_image; ?>" />
        <?php } else { ?>
        <meta d32ata-property_mdp="og:image" content="<?php echo $logo; ?>" />
        <?php } ?>
        <meta d32ata-property_mdp="og:site_name" content="<?php echo $name; ?>" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.32/owl.carousel.min.css" rel="stylesheet" media="screen" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.32/owl.carousel.min.js"></script>
        <link rel="stylesheet" type="text/css" href="catalog/view/theme/magazin/stylesheet/stylesheet<?php global $config; echo $config->get('control_thema'); ?>.css" />
        <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
        <?php } ?>
        <style><?php global $config; if ($config->get('control_custom_css') != '') { echo htmlspecialchars_decode($config->get('control_custom_css'));}?></style>
        <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
        <?php } ?>
        <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
        <?php } ?>
        <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
        <?php } ?>
        <?php $lang = (int)$config->get('config_language_id');?>
    </head>
    <body class="<?php echo $class; ?>">
        <div class="tophead"></div>
        <div id="block-close"></div>
        <?php if($config->get('control_promo')) {
        $control_promo = $config->get('control_promo');
        if(isset($control_promo[$lang]) && $control_promo[$lang] != '') { 	?>
        <div class="block-promo" style="display:none;">
            <div class="block-content">
                <a href="<?php echo $config->get('control_promo_url')?>">
                    <div class="promo-text active">
                        <p class="del2 blurFadeIn"><?php echo $control_promo[$lang]; ?></p>
                    </div>
                </a>
                <span class="close-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40" enable-background="new 0 0 40 40">
                    <line x1="15" y1="15" x2="25" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10"></line>
                    <line x1="25" y1="15" x2="15" y2="25" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10"></line>
                    <circle class="circle" cx="20" cy="20" r="19" opacity="0" stroke="#000" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10" fill="none"></circle>
                    <path d="M20 1c10.45 0 19 8.55 19 19s-8.55 19-19 19-19-8.55-19-19 8.55-19 19-19z" class="progress" stroke="#fff" stroke-width="2.5" stroke-linecap="round" stroke-miterlimit="10" fill="none"></path>
                    </svg>
                </span>
            </div>
            <script>
 $(
         document ).
         ready(
                 function (){
                     function get_cookie(
                             cookie_name ){
                         var results = document.cookie.match(
                                 '(^|;) ?'
                                 + cookie_name
                                 + '=([^;]*)(;|$)' );
                         if( results )
                             return ( unescape(
                                     results[2] ) );
                         else
                             return null;
                     }

                     function check_cookie(){
                         var textCookie = get_cookie(
                                 'messagetext' ),
                                 currentText =
                                 '<?php echo $control_promo[$lang]; ?>'
                         if( textCookie
                                 !== currentText ){
                             $(
                                     ".block-promo" ).
                                     slideDown(
                                             "slow" );
                         }
                     }

                     check_cookie();

                     $(
                             '.close-icon, .block-promo' ).
                             click(
                                     function (){
                                         var currentText =
                                                 '<?php echo $control_promo[$lang]; ?>',
                                                 date = new Date();
                                         date.setTime(
                                                 date.getTime()
                                                 + ( 365
                                                         * 24
                                                         * 60
                                                         * 60
                                                         * 1000 ) );
                                         document.cookie = "messagetext="
                                                 + currentText
                                                 + "; expires="
                                                 + date.toGMTString()
                                                 + "; path=/";
                                         $(
                                                 '.block-promo' ).
                                                 slideUp(
                                                         'slow' );
                                     } );

                 } );
            </script>
        </div>
        <?php } ?>
        <?php } ?>
        <?php $control_menu_top_urltitle1 = $config->get('control_menu_top_urltitle1'); $control_menu_top_url1 = $config->get('control_menu_top_url1'); if(isset($control_menu_top_urltitle1[$lang]) && ($control_menu_top_urltitle1[$lang] != '') ) { ?>
        <div class="topm">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 hidden-xs">
                        <ul class="left">
                            <li>
                                <?php echo ($config->get('config_email'))?>
                            </li>
                            <?php if($config->get('control_adress_menu')== 1) { ?>
                            <li>
                                <?php echo ($config->get('config_address'))?>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul class="right">

                            <li class="custom-link bgc1"><a href="<?php echo $control_menu_top_url1[$lang]; ?>">
                                    <?php echo $control_menu_top_urltitle1[$lang]; ?></a>
                            </li>
                            <?php $control_menu_top_urltitle2 = $config->get('control_menu_top_urltitle2'); $control_menu_top_url2 = $config->get('control_menu_top_url2'); if(isset($control_menu_top_urltitle2[$lang]) && ($control_menu_top_urltitle2[$lang] != '') ) { ?>
                            <li class="custom-link bgc2"><a href="<?php echo $control_menu_top_url2[$lang]; ?>">
                                    <?php echo $control_menu_top_urltitle2[$lang]; ?></a>
                            </li>
                            <?php } ?>
                            <?php $control_menu_top_urltitle3 = $config->get('control_menu_top_urltitle3'); $control_menu_top_url3 = $config->get('control_menu_top_url3'); if(isset($control_menu_top_urltitle3[$lang]) && ($control_menu_top_urltitle3[$lang] != '') ) { ?>
                            <li class="custom-link bgc3"><a href="<?php echo $control_menu_top_url3[$lang]; ?>">
                                    <?php echo $control_menu_top_urltitle3[$lang]; ?></a>
                            </li>
                            <?php } ?>
                            <?php $control_menu_top_urltitle4 = $config->get('control_menu_top_urltitle4'); $control_menu_top_url4 = $config->get('control_menu_top_url4'); if(isset($control_menu_top_urltitle4[$lang]) && ($control_menu_top_urltitle4[$lang] != '') ) { ?>
                            <li class="custom-link bgc4"><a href="<?php echo $control_menu_top_url4[$lang]; ?>">
                                    <?php echo $control_menu_top_urltitle4[$lang]; ?></a>
                            </li>
                            <?php } ?>
                            <?php if($config->get('control_curlang')== 1) { ?>
                            <!--  <div class="top-curlang">
                                <?php echo $language; ?>
                                <div class="cur">
                                   <?php echo $currency; ?>
                                </div>
                             </div> -->
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php echo $search; ?>
        <div class="white">
            <header>
                <?php if($config->get('control_callme')== 1) { echo $callme; ?><?php } ?>
                <div class="container">
                    <div class="row">
                        <?php if($config->get('control_logo')== 1) { ?>
                        <div class="col-sm-4 hidden-xs nopad">
                            <?php if($config->get('control_telephone')== 1) { ?>
                            <div class="col-sm-6">
                                <div class="head-phone">
                                    <div class="phone">
                                        <div><?php echo $telephone; ?></div>
                                    </div>
                                    <?php if($config->get('control_callme')== 1) { ?><a class="panel-url callme" data-spanel="#callme"><?php echo $heading_title_callme; ?></a><?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($config->get('config_open')) { ?>
                            <div class="col-sm-6">
                                <div class="regim">
                                    <div><?php echo $regim; ?></div>
                                </div>
                                <div><?php echo ($config->get('config_open'))?></div>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-sm-4">
                            <div id="logo">
                                <?php if ($logo) { ?>
                                <?php if ($home == $og_url) { ?>
                                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                                <?php } else { ?>
                                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                                <?php } ?>
                                <?php } else { ?>
                                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } else { ?>
                        <div class="col-sm-4">
                            <div id="logo">
                                <?php if ($logo) { ?>
                                <?php if ($home == $og_url) { ?>
                                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" style="margin-left: 0;"/>
                                <?php } else { ?>
                                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" style="margin-left: 0;"/></a>
                                <?php } ?>
                                <?php } else { ?>
                                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-4 hidden-xs nopad">
                            <?php if($config->get('control_telephone')== 1) { ?>
                            <div class="col-sm-6">
                                <div class="head-phone">
                                    <div class="phone">
                                        <div><?php echo $telephone; ?></div>
                                    </div>
                                    <?php if($config->get('control_callme')== 1) { ?><a class="panel-url callme" data-spanel="#callme"><?php echo $heading_title_callme; ?></a><?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($config->get('config_open')) { ?>
                            <div class="col-sm-6">
                                <div class="regim">
                                    <div><?php echo $regim; ?></div>
                                </div>
                                <div><?php echo ($config->get('config_open'))?></div>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                        <div id='headr-block' style="top: 25px;" class="headfix col-sm-4">
                            <div class="<?php if($config->get('control_menutext')== 1) { ?>menutext<?php } ?> head-block">
                                <div id="mobmenu" class="col hidden-lg hidden-md hidden-sm">
                                    <div class="log">
                                        <a href="#menu2">
                                            <p><?php if($config->get('control_menu_title')) { $control_menu_title = $config->get('control_menu_title');  echo $control_menu_title[$lang]; }?></p>
                                        </a>
                                    </div>
                                </div>
                                <span id="searchid" class="col"><?php echo $text_search_theme; ?></span>
                                <div class="col">
                                    <div class="log">
                                        <?php if($config->get('control_quicklogin')== 1) { ?>
                                        <?php if ($logged) { ?>
                                        <a class="panel-url quicklogin" data-spanel="#account">
                                            <p><?php echo $text_account; ?></p>
                                        </a>
                                        <div id="account" class="spanel open-right">
                                            <div class="panel-close"></div>
                                            <h3><?php echo $name_customer; ?></h3>
                                            <div class="panel-account">
                                                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                                                <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                                                <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                                                <li><a href="/system/storage/download/price_<?php echo $this->registry->get('config')->get('config_language'); ?><?php echo $this->registry->get('config')->get('config_store_id'); ?>.xlsx">xls price</a></li>
                                                <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                                                <?php if($config->get('control_custom_account_status')== 1) { $control_content_account = $config->get('control_content_account');
                                                echo html_entity_decode($control_content_account[$lang], ENT_QUOTES, 'UTF-8');
                                                } ?>
                                            </div>
                                        </div>
                                        <?php } else { ?>
                                        <?php echo $quicksignup; ?>
                                        <p class="panel-url quicklogin" data-spanel="#quicklogin"><?php echo $text_login ?></p>
                                        <?php } ?>
                                        <?php } else { ?>
                                        <?php if ($logged) { ?>
                                        <a href="<?php echo $account; ?>">
                                            <p><?php echo $text_account; ?></p>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $login; ?>">
                                            <p><?php echo $text_login ?></p>
                                        </a>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="mobcall panel-url" data-spanel="#callme"></div>
                                <!--  <div class="col cartfloat">
                                    <?php echo $cart; ?>
                                 </div> -->
                                <div class="top-curlang">
                                    <?php echo $language; ?>
                                    <div class="cur">
                                        <?php echo $currency; ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </header>
            <?php if ($categories) { ?>
            <nav id="menu" class="navbar hidden-xs <?php if($config->get('control_menu_title') && $config->get('control_menu_hidden')== 1) { echo "open"; }?>">
                 <?php if($config->get('control_menu_hidden')== 1) { ?>
                 <div class="menu_title">
                                                                                                                                            <i class="fa fa-navicon"></i><span><?php echo $control_menu_title[$lang]; ?></span>
                                                                                                                                        </div>
                                                                                                                                        <?php } ?>
                                                                                                                                        <div class="container">
                                                                                                                                            <div class="navbar-header"><span id="category" class="visible-xs"><?php echo $text_category; ?></span>
                                                                                                                                                <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                                                                                                                                            </div>
                                                                                                                                            <div class="collapse navbar-collapse navbar-ex1-collapse nopad">
                                                                                                                                                <ul class="nav navbar-nav">
                                                                                                                                                    <?php $cat = 0; foreach ($categories as $category) { $cat++; ?>
                                                                                                                                                    <?php if ($category['children'] && $config->get('control_lavel2')== 1) { ?>
                                                                                                                                                    <li class="dropdown cat-<?php echo $cat;?>">
                                                                                                                                                        <a class="arrow dropdown-toggle" href="<?php echo $category['href']; ?>" <?php if ($config->get('control_lavel1')== 2) { ?>data-toggle="dropdown"<?php } ?>><?php echo $category['name']; ?></a>
                                                                                                                                                        <div class="dropdown-menu">
                                                                                                                                                            <div class="container">
                                                                                                                                                                <div class="dropdown-inner">
                                                                                                                                                                    <?php $col = 0; foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { $col++; ?>
                                                                                                                                                                    <ul class="list-unstyled col-<?php echo $cat;?>-<?php echo $col;?>">
                                                                                                                                                                        <?php $ch = 0; foreach ($children as $child) { $ch++; ?>
                                                                                                                                                                        <li class="head-child-<?php echo $cat;?>-<?php echo $col;?>-<?php echo $ch;?>"><a class="liheader" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                                                                                                                                                        <?php if ($child['subchildren'] && $config->get('control_lavel3')== 1) { ?>
                                                                                                                                                                        <?php $sub = 0; foreach ($child['subchildren'] as $subchild) { $sub++; ?>
                                                                                                                                                                        <li class="child-<?php echo $cat;?>-<?php echo $col;?>-<?php echo $ch;?>-<?php echo $sub;?>"><a href="<?php echo $subchild['href']; ?>"><?php echo $subchild['name']; ?></a></li>
                                                                                                                                                                        <?php } ?>
                                                                                                                                                                        <?php } ?>
                                                                                                                                                                        <?php } ?>
                                                                                                                                                                    </ul>
                                                                                                                                                                    <?php } ?>
                                                                                                                                                                </div>
                                                                                                                                                                <a href="<?php echo $category['href']; ?>" class="see-all"><?php echo $text_all; ?> <?php echo $category['name']; ?></a>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </li>
                                                                                                                                                    <?php } else { ?>
                                                                                                                                                    <li class="bg<?php echo $cat;?>"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                                                                                                                                    <?php } ?>
                                                                                                                                                    <?php } ?>
                                                                                                                                                    <?php $control_menu_urltitle1 = $config->get('control_menu_urltitle1'); $control_menu_url1 = $config->get('control_menu_url1'); if(isset($control_menu_urltitle1[$lang]) && ($control_menu_urltitle1[$lang] != '') ) { ?>
                                                                                                                                                    <li class="custom-link bgc1"><a href="<?php echo $control_menu_url1[$lang]; ?>">
                                                                                                                                                            <?php echo $control_menu_urltitle1[$lang]; ?></a>
                                                                                                                                                    </li>
                                                                                                                                                    <?php } ?>
                                                                                                                                                    <?php $control_menu_urltitle2 = $config->get('control_menu_urltitle2'); $control_menu_url2 = $config->get('control_menu_url2'); if(isset($control_menu_urltitle2[$lang]) && ($control_menu_urltitle2[$lang] != '') ) { ?>
                                                                                                                                                    <li class="custom-link bgc2"><a href="<?php echo $control_menu_url2[$lang]; ?>">
                                                                                                                                                            <?php echo $control_menu_urltitle2[$lang]; ?></a>
                                                                                                                                                    </li>
                                                                                                                                                    <?php } ?>
                                                                                                                                                    <?php $control_menu_urltitle3 = $config->get('control_menu_urltitle3'); $control_menu_url3 = $config->get('control_menu_url3'); if(isset($control_menu_urltitle3[$lang]) && ($control_menu_urltitle3[$lang] != '') ) { ?>
                                                                                                                                                    <li class="custom-link bgc3"><a href="<?php echo $control_menu_url3[$lang]; ?>">
                                                                                                                                                            <?php echo $control_menu_urltitle3[$lang]; ?></a>
                                                                                                                                                    </li>
                                                                                                                                                    <?php } ?>
                                                                                                                                                    <?php $control_menu_urltitle4 = $config->get('control_menu_urltitle4'); $control_menu_url4 = $config->get('control_menu_url4'); if(isset($control_menu_urltitle4[$lang]) && ($control_menu_urltitle4[$lang] != '') ) { ?>
                                                                                                                                                    <li class="custom-link bgc4"><a href="<?php echo $control_menu_url4[$lang]; ?>">
                                                                                                                                                            <?php echo $control_menu_urltitle4[$lang]; ?></a>
                                                                                                                                                    </li>
                                                                                                                                                    <?php } ?>
                                                                                                                                                    <?php if($config->get('control_custom_info_status')== 1) { ?>
                                                                                                                                                    <li class="menu_html dropdown">
                                                                                                                                                        <a class="arrow"><?php $control_title_html = $config->get('control_title_html'); echo $control_title_html[$lang]; ?></a>
                                                                                                                                                        <div class="dropdown-menu">
                                                                                                                                                            <div class="container">
                                                                                                                                                                <?php $control_content_info = $config->get('control_content_info'); echo html_entity_decode($control_content_info[$lang], ENT_QUOTES, 'UTF-8'); ?>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </li>
                                                                                                                                                    <?php } ?>
                                                                                                                                                    <?php if($config->get('control_brand_menu')== 1) { ?>
                                                                                                                                                    <li class="menu_brands dropdown">
                                                                                                                                                        <a class="arrow" href="<?php echo $manufacturer; ?>"><?php echo $text_brand; ?></a>
                                                                                                                                                        <div class="dropdown-menu">
                                                                                                                                                            <?php if ($manufacturers) { ?>
                                                                                                                                                            <div class="container">
                                                                                                                                                                <div class="owl-carousel brand_carousel">
                                                                                                                                                                    <?php $counter = 0; foreach ($manufacturers as $manufacturer) { ?>
                                                                                                                                                                    <?php if ($manufacturer['image']) { ?>
                                                                                                                                                                    <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
                                                                                                                                                                        <a href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['image']; ?>" title="<?php echo $manufacturer['name']; ?>" alt="<?php echo $manufacturer['name']; ?>" /></a>
                                                                                                                                                                    </div>
                                                                                                                                                                    <?php } ?>
                                                                                                                                                                    <?php $counter++; } ?>
                                                                                                                                                                </div>
                                                                                                                                                            </div>
                                                                                                                                                            <?php } ?>
                                                                                                                                                        </div>
                                                                                                                                                    </li>
                                                                                                                                                    <?php } ?>
                                                                                                                                                    <li class="pull-right ">
                                                                                                                                                        <?php if($config->get('control_wishlist_menu')== 2) { ?>
                                                                                                                                                        <a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><i class="fa fa-heart"></i><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_wishlist; ?></span></a>
                                                                                                                                                        <?php } ?>
                                                                                                                                                        <?php if($config->get('control_wishlist_menu')== 1) { ?>
                                                                                                                                                        <a href="<?php echo $special; ?>" id="wishlist-total" title="<?php echo $text_special; ?>"><i class="fa fa-gift"></i><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_special; ?></span></a>
                                                                                                                                                        <?php } ?>
                                                                                                                                                        <?php echo $cart; ?>
                                                                                                                                                    </li>
                                                                                                                                                </ul>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </nav>
                                                                                                                                    <?php } ?>
            </div>
            <script type="text/javascript">
                /******** Menu Show Hide Sub Menu ********/
                /*           $('#menu .nav > li').mouseover(function() {

                 $(this).find('> div').stop(true, true).slideDown('fast');

                 $(this).bind('mouseleave', function() {

                 $(this).find('> div').stop(true, true).css('display', 'none');

                 });});*/

                $(
                        document ).
                        ready(
                                function (){
                                    $(
                                            "nav#menu2" ).
                                            mmenu(
                                                    {
                                                        extensions : [
                                                            'effect-slide-menu'
                                                            ,'pageshadow'
                                                        ],
                                                        counters : true,
                                                        navbar : {
                                                            title : '<?php echo $control_menu_title[$lang];?>'
                                                        },
                                                        navbars : [
                                                            {
                                                                position : 'top',
                                                                content : [
                                                                    'prev',
                                                                    'title',
                                                                    'close'
                                                                ]
                                                            }
                                                        ]
                                                    } );
                                } );



                $(
                        document ).
                        ready(
                                function (){
                                    $(
                                            ".brand_carousel" ).
                                            owlCarousel(
                                                    {
                                                        itemsCustom : [
                                                            [
                                                                320
                                                                ,2
                                                            ],
                                                            [
                                                                600
                                                                ,4
                                                            ],
                                                            [
                                                                768
                                                                ,5
                                                            ],
                                                            [
                                                                992
                                                                ,6
                                                            ],
                                                            [
                                                                1170
                                                                ,7
                                                            ]
                                                        ],
                                                        navigation : true,
                                                        navigationText : [
                                                            '<i class="fa fa-angle-left"></i>'
                                                            ,'<i class="fa fa-angle-right"></i>'
                                                        ],
                                                        scrollPerPage : true,
                                                        pagination : false
                                                    } );
                                } );
            </script>
            <nav id="menu2" class="fixload">
                <ul>
                    <?php if ($categories) { ?>
                    <?php foreach ($categories as $category) { ?>
                    <li>
                        <span class="name">
                            <?php if ($category['children']) { ?><span class="expander">-</span><?php } ?>
                            <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                        </span>
                        <?php if ($category['children']) { ?>
                        <ul>
                            <?php foreach ($category['children'] as $child) { ?>
                            <li>
                                <span class="name">
                                    <?php if ($child['subchildren']) { ?><span class="expander">-</span><?php } ?>
                                    <a href="<?php echo $child['href']; ?>">
                                        <span class="icon m-icon-dress"></span><?php echo $child['name']; ?>
                                    </a>
                                </span>
                                <?php if ($child['subchildren']) { ?>
                                <ul>
                                    <?php foreach ($child['subchildren'] as $subchild) { ?>
                                    <li><span class="name"><a href="<?php echo $subchild['href']; ?>"><?php echo $subchild['name']; ?></a></span></li>
                                    <?php } ?>
                                </ul>
                                <?php } ?>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                    </li>
                    <?php } ?>
                    <?php } ?>
                    <?php if(isset($control_menu_urltitle1[$lang]) && ($control_menu_urltitle1[$lang] != '') ) { ?>
                    <li class="custom-link bgc1"><a href="<?php echo $control_menu_url1[$lang]; ?>">
                            <?php echo $control_menu_urltitle1[$lang]; ?></a>
                    </li>
                    <?php } ?>
                    <?php if(isset($control_menu_urltitle2[$lang]) && ($control_menu_urltitle2[$lang] != '') ) { ?>
                    <li class="custom-link bgc2"><a href="<?php echo $control_menu_url2[$lang]; ?>">
                            <?php echo $control_menu_urltitle2[$lang]; ?></a>
                    </li>
                    <?php } ?>
                    <?php if(isset($control_menu_urltitle3[$lang]) && ($control_menu_urltitle3[$lang] != '') ) { ?>
                    <li class="custom-link bgc3"><a href="<?php echo $control_menu_url3[$lang]; ?>">
                            <?php echo $control_menu_urltitle3[$lang]; ?></a>
                    </li>
                    <?php } ?>
                    <?php if(isset($control_menu_urltitle4[$lang]) && ($control_menu_urltitle4[$lang] != '') ) { ?>
                    <li class="custom-link bgc4"><a href="<?php echo $control_menu_url4[$lang]; ?>">
                            <?php echo $control_menu_urltitle4[$lang]; ?></a>
                    </li>
                    <?php } ?>
                </ul>
            </nav>
            <?php echo $content_slide; ?>