<div id="quicklogin" class="spanel open-right">
   <div class="panel-close"></div>
   <ul class="nav nav-tabs">
      <li class="active"><a href="#tab1" data-toggle="tab"><?php echo $button_login; ?></a></li>
      <li><a href="#tab2" data-toggle="tab"><?php echo $button_register; ?></a></li>
   </ul>
   <div class="tab-content">
      <div class="tab-pane active" id="tab1">
         <div class="col-sm-12" id="quick-login">
            <div class="form-group required">
               <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value=""  id="input-email-login" class="form-control" />
            </div>
            <div class="form-group required">
               <input type="password" name="password" placeholder="<?php echo $entry_password; ?>" value="" id="input-password-login" class="form-control" />
            </div>
            <div class="form-group">
               <a class="href" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
            </div>
            <div class="form-group">
               <button type="button" class="btn btn-center btn-primary loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><?php echo $button_login ?></button>
            </div>
         </div>
      </div>
      <div class="tab-pane" id="tab2">
         <div class="col-sm-12" id="quick-register">
            <div class="form-group required">
               <input type="text" name="firstname" placeholder="<?php echo $entry_name; ?>" value="" id="input-firstname-reg" class="form-control" />
            </div>
			<?php if ($agree_lastname) { ?>
			<div class="form-group required">
               <input type="text" name="lastname" placeholder="<?php echo $entry_lastname; ?>" value="" id="input-lastname-reg" class="form-control" />
            </div>
			<?php } ?>
            <div class="form-group required">
               <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value="" id="input-email-reg" class="form-control" />
            </div>
            <div class="form-group required">
               <input type="text" name="telephone" placeholder="<?php echo $entry_telephone; ?>" value="" id="input-telephone" class="form-control" />
            </div>
            <div class="form-group required">
               <input type="password" name="password" placeholder="<?php echo $entry_password; ?>" value="" id="input-password-reg" class="form-control" />
            </div>
<div class="form-group">
<label for="input-remember">
<input type="checkbox" name="remember_me" id="input-remember" value="remember_checked" /> remember me
</label>
</div>			
            <?php if ($text_agree) { ?>
            <div class="form-group">
               <div class="checkbox">
			   <label for="agree">
			   <input type="checkbox" id="agree" class="option-input" name="agree" value="1" />
               &nbsp;<?php echo $text_agree; ?>
			   </label>
               
			   </div>
            </div>
            <button type="button" class="btn btn-center btn-primary createaccount"  data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_continue; ?></button>
            <?php }else{ ?>
            <button type="button" class="btn btn-center btn-primary createaccount" data-loading-text="<?php echo $text_loading; ?>" ><?php echo $button_continue; ?></button>
            <?php } ?>
            <p class="info"><?php echo $text_info; ?></p>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript"><!--
   $('#quick-register input').on('keydown', function(e) {
   	if (e.keyCode == 13) {
   		$('#quick-register .createaccount').trigger('click');
   	}
   });
   $('#quick-register .createaccount').click(function() {
   	$.ajax({
   		url: 'index.php?route=common/quicksignup/register',
   		type: 'post',
   		data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
   		dataType: 'json',
   		beforeSend: function() {
   			$('#quick-register .createaccount').button('loading');
   			$('#modal-quicksignup .alert-danger').remove();
   		},
   		complete: function() {
   			$('#quick-register .createaccount').button('reset');
   		},
   		success: function(json) {
   			$('#modal-quicksignup .form-group').removeClass('has-error');
   			
   			if(json['islogged']){
   				 window.location.href="index.php?route=account/account";
   			}
   			if (json['error_firstname']) {
   				$('#quick-register #input-firstname-reg').parent().addClass('has-error');
   				$('#quick-register #input-firstname-reg').focus();
   			}
			if (json['error_lastname']) {
   				$('#quick-register #input-lastname-reg').parent().addClass('has-error');
   				$('#quick-register #input-lastname-reg').focus();
   			}
   			if (json['error_email']) {
   				$('#quick-register #input-email-reg').parent().addClass('has-error');
   				$('#quick-register #input-email-reg').focus();
   			}
   			if (json['error_telephone']) {
   				$('#quick-register #input-telephone').parent().addClass('has-error');
   				$('#quick-register #input-telephone').focus();
   			}
   			if (json['error_password']) {
   				$('#quick-register #input-password-reg').parent().addClass('has-error');
   				$('#quick-register #input-password-reg').focus();
   			}
   			if (json['error']) {
   				$('#modal-quicksignup .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
   			}
   			
   			if (json['now_login']) {
   				$('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');
   				
   				$('.quick-login').remove();
   			}
   			if (json['success']) {
   				$('#modal-quicksignup .main-heading').html(json['heading_title']);
   				success = json['text_message'];
   				success += '<div class="buttons"><div class="text-right"><a onclick="loacation();" class="btn btn-primary">'+ json['button_continue'] +'</a></div></div>';
   				$('#modal-quicksignup .modal-body').html(success);
   				loacation();
   			}
   		}
   	});
   });

   $('#quick-login input').on('keydown', function(e) {
   	if (e.keyCode == 13) {
   		$('#quick-login .loginaccount').trigger('click');
   	}
   });
   $('#quick-login .loginaccount').click(function() {
   	$.ajax({
   		url: 'index.php?route=common/quicksignup/login',
   		type: 'post',
   		data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
   		dataType: 'json',
   		beforeSend: function() {
   			$('#quick-login .loginaccount').button('loading');
   			$('#modal-quicksignup .alert-danger').remove();
   		},
   		complete: function() {
   			$('#quick-login .loginaccount').button('reset');
   		},
   		success: function(json) {
   			$('#modal-quicksignup .form-group').removeClass('has-error');
   			if(json['islogged']){
   				 window.location.href="index.php?route=account/account";
   			}
   			
   			if (json['error']) {
   				$('#modal-quicksignup .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
   				$('#quick-login #input-email-login').parent().addClass('has-error');
   				$('#quick-login #input-password-login').parent().addClass('has-error');
   				$('#quick-login #input-email-login').focus();
   			}
   			if(json['success']){
   				loacation();
   				$('#modal-quicksignup').modal('hide');
   			}   			
   		}
   	});
   });
   function loacation() {
   	location.reload();
   }
   $(document).ready(function() {
   $("#input-telephone").mask("(099)-999-9999");
   });
   //--></script>
