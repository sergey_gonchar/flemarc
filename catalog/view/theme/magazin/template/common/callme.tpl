<div id="callme" class="spanel open-left">
   <div class="col-sm-12">
      <div class="panel-close"></div>
      <div id="close-callme">
         <h3><?php echo $heading_title_callme; ?></h3>
         <div class="form-group required">
            <input type="text" name="name" placeholder="<?php echo $entry_names; ?>" value="<?php echo $name_customer; ?>" id="callme-name" class="form-control"/>
         </div>
         <div class="form-group required">
            <input type="text" name="phone" placeholder="<?php echo $entry_phone; ?>" value="<?php echo $phone_customer; ?>" id="callme-phone" class="form-control"/>
         </div>
         <div class="form-group">
            <textarea name="comment" placeholder="<?php echo $entry_comment; ?>" id="callme-comment" class="form-control"></textarea>
         </div>
         <div class="form-group">
            <input type='text' name="time" placeholder="<?php echo $entry_time; ?>" class="form-control" id="callme-time" />
         </div>
         <div class="form-group">
            <div class="callme"><input type="button" id="bcallme" value="<?php echo $heading_title_callme; ?>" class="btn btn-center btn-primary"/></div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript"><!--
   $('#bcallme').bind('click', function(){
   	$.ajax({
   		url: 'index.php?route=common/callme/callme',
   		type: 'post',
   		data: $('#callme input[type=\'text\'], #callme textarea').serializeArray(),
   		dataType: 'json',
   		success: function(json){
   			$('.error').remove();
   $('.text-danger').remove();
   			if (json['error']){
   				if (json['error']['name']){
   					$('#callme-name').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_name'] + '</span>');
   					$('#callme-name').closest('.form-group').addClass('has-error');
   				}
   				if (json['error']['phone']){
   					$('#callme-phone').after('<span class="text-danger"><i class="fa fa-arrow-up"></i>' + json['error_telephone'] + '</span>');
   					$('#callme-phone').closest('.form-group').addClass('has-error');
   				}
   			}
   			if (json['success']) {
   				$('#close-callme').hide().after('<div class="col-sm-12"><h3 class="success"><?php echo $title_success; ?></h3><p class="left"><?php echo $callme_success; ?></p><b class="right"><?php echo $allbottom_success; ?>&nbsp;<?php echo $shop; ?></b><div class="time"</div></div>')
   			}
   		}
   	});
   });
   $(document).ready(function() {
   $("#callme-phone").mask("(099)-999-9999");
   });
   //--></script>
   