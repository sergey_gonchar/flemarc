<!-- Search. Открываемое меню поиска  -->
<!-- http://flemarc.com/index.php?route=product/search&search=ghfhfgfghfh  -->
<!-- в текущей версии используется этот файл -->

<style>
    .topsearch__btn-link {
        font-weight: bolder;
        vertical-align: middle;
    }
    .topsearch__btn-link--search {
        line-height: 45px;
        font-size: 8.5pt;
    }
    .topsearch__btn-link--reset {
        line-height: 40px;
        font-size: 12pt !important;
    }    
</style>
<div id="topsearch">
    <div class="container">
        <form name="search-popup">
            <div id="search" class="input-group">
                <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
                <span class="input-group-btn">
                    <button type="button" class="btn-link topsearch__btn-link topsearch__btn-link--search"><i class="fa fa-search"></i></button>
                    <span type="reset" class="btn-lg btn-link btn-reset topsearch__btn-link topsearch__btn-link--reset" name="reset" onClick="return false;">X</span>
                </span>
            </div>
        </form>
    </div>
</div>

