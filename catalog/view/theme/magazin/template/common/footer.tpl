<?php echo $content_footer; ?>
<footer>

		<div class="col-sm-12 footer-links">	
			<ul class="line1">		
					<?php foreach ($informations as $information) { ?>
					<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
					<?php } ?>
					<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
					<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
					<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
					<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
					<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
					<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
					<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
					<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
					<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
					<li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
			</ul>
		</div>
  <div class="container">
    <div class="row">
		<div class="col-lg-3 col-sm-8">
			<a href="#" title="Universal" class="footer-logo"><span>Universal</span></a>
			<p>
		<?php global $config; $lang = (int)$config->get('config_language_id');?>
		<?php if($config->get('control_info_shop')) { $control_info_shop = $config->get('control_info_shop');  echo $control_info_shop[$lang]; }?>	 
			</p>
			<div class="data-footer">
				<p  class="df"><i class="fa fa-phone"></i> <?php echo $telephone; ?></p>
				<p  class="df"><i class="fa fa-globe"></i> <?php echo $address; ?></p>
				<p  class="df"><a target="_blank" href="mailto:kenan78@gmail.com"><i class="fa fa-envelope"></i>kenan78@gmail.com</a></p>
			</div>
		</div>
		<div class="col-sm-3">
			<h2 class="footer-block-title">Newsletter</h2>
			<form class="solid">
				<div id="close-subscription">
				  <input type="hidden" name="language_id" id="language_id" value="" />
				  <input type="hidden" name="store_id" id="store_id" value="" />
				  <label for="newsletter-296">Subscribe to the Universal mailing list to receive updates on new arrivals, offers and other discount information.</label>
				  <div class="sub-bl">
					 <input id="semail" class="input-sub" name="semail" value="" placeholder="E-Mail" type="text">
				  </div>
				  <div class="sub-bl">
					 <input type="button" id="bsubscription1" value="Подписаться" class="btn btn-primary">
				  </div>
				</div>
			</form>
		</div>
	  
		<div class="col-lg-2 col-sm-4 footer-block">
			<h2 class="footer-block-title">Quick links</h2>
			<ul class="list-unstyled line">
				  <?php foreach ($informations as $information) { ?>
				  <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
				  <?php } ?>
				  <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
			</ul>
		</div>
		<div class="col-lg-2 col-sm-4 footer-block">
			<h2 class="footer-block-title">Accessories</h2>
			<ul class="list-unstyled line">
				<?php foreach ($informations as $information) { ?>
				<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
				<?php } ?>
				<li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
			</ul>
		</div>
		<div class="col-lg-2 col-sm-4 footer-block">
			<h2 class="footer-block-title">Openning hours</h2>
			<ul class="list-unstyled line">
				<?php foreach ($informations as $information) { ?>
				<li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
				<?php } ?>
				<li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
				<li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
			</ul>
		</div>
	</div>
  </div>
  <div class="copyright"><?php echo $powered; ?></div>
  <?php if($config->get('control_callmebtn')== 1 && $config->get('control_callme')== 1) { ?>
  <div class="callmebtn panel-url hidden-xs" data-spanel="#callme">
    <div class="toolhover">
        <div class="callbtn">Перезвоним</div>
		<div class="callicon"></div>
        <div class="callfon"></div>
    <div class="toolcall">Перезвоним бесплатно</div>
	</div>   
</div>
 <?php } ?> 
</footer>
<?php if($config->get('control_back_to_top')== 1) {?> 
<div class="scrollup hidden-xs">
   <div class="sup"></div>
</div>
<?php } ?>
<script src="catalog/view/theme/magazin/js/common.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js" type="text/javascript"></script>
<link href="catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet" type='text/css'>
<script src="catalog/view/theme/magazin/js/jquery.mmenu.min.all.js" type="text/javascript"></script>
<script src="catalog/view/theme/magazin/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="catalog/view/theme/magazin/js/jquery.elevatezoom.js" type="text/javascript"></script>
<link href="catalog/view/theme/magazin/stylesheet/jquery.mmenu.all.css" rel="stylesheet" type='text/css'>
<div id="id32" style="display:none;">
<div class="form-group">
<label for="input-remember">
<input type="checkbox" name="remember_me" id="input-remember" value="remember_checked" /> remember me
</label>
</div>
</div>
<script>$('#quicklogin #quick-login .form-group:nth-child(3)').after($('#id32').html());</script>

</body></html>