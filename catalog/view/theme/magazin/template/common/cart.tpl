<a data-spanel="#sidecart" id="wishlist-total" class="panel-url" title="<?php echo $text_theme_cart; ?>">
   <i class="fa fa-gift"></i><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_theme_cart; ?></span>
</a>       
<div id="cart">
   <ul id="sidecart" class="spanel open-right">
      <li id="sidecart-close" class="panel-close"></li>
      <li class="form-group closss" style="display:none;">
         <input type="button" value="<?php echo $quick_order_cart; ?>" class="procart btn btn-center btn-primary" onclick="order_form();">
      </li>
      <li class="closss">
         <h3><?php echo $text_cart; ?></h3>
      </li>
      <?php if ($products || $vouchers) { ?>
      <li class="closss">
         <table class="table table-items">
            <?php foreach ($products as $product) { ?>
            <tr>
               <td class="text-center" width="25%"><?php if ($product['thumb']) { ?>
                  <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="" /></a>
                  <?php } ?>
               </td>
               <td class="text-left" width="60%"><span class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></span>
                  <?php if ($product['option']) { ?>
                  <?php foreach ($product['option'] as $option) { ?>
                  <br />
                  - <small><?php echo $option['name']; ?> <?php echo $option['value']; ?></small>
                  <?php } ?>
                  <?php } ?>
                  <?php if ($product['recurring']) { ?>
                  <br />
                  - <small><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></small>
                  <?php } ?>
                  <br><?php echo $product['total']; ?> x <?php echo $product['quantity']; ?>
               </td>
               <td class="text-center" width="15%"><button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" class="no-btn btn-xs"><i class="fa fa-trash-o"></i></button></td>
            </tr>
            <?php } ?>
            <?php foreach ($vouchers as $voucher) { ?>
            <tr>
               <td class="text-center"></td>
               <td class="text-left"><?php echo $voucher['description']; ?> <br> x&nbsp;1</td>
               <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="no-btn btn-xs"><i class="fa fa-trash-o"></i></button></td>
            </tr>
            <?php } ?>
         </table>
      </li>
      <li class="closss">
         <table class="table">
            <?php foreach ($totals as $total) { ?>
            <tr>
               <td class="text-right"><strong><?php echo $total['title']; ?></strong></td>
               <td class="text-right"><?php echo $total['text']; ?></td>
            </tr>
            <?php } ?>
         </table>
         <div class="form-group">
            <a href="<?php echo $checkout; ?>" class="btn btn-center btn-primary"><?php echo $text_checkout; ?></a>
         </div>
         <div class="smoll"><?php echo $text_or; ?></div>
         <div class="form-group">
            <input type="button" value="<?php echo $quick_order; ?>" class="btn btn-center btn-default" onclick="order_form();" />						
         </div>
      </li>
      <li>
         <div id="order-form" style="display:none;">
            <div class="col-sm-12" id="quick-order">
               <?php if ($name_customer) { ?>
               <div class="form-group required">
                  <input type="text" name="name" placeholder="<?php echo $entry_name; ?>" value="<?php echo $name_customer; ?>" readonly="readonly" id="input-name-order" class="form-control" />
               </div>
               <div class="form-group required">
                  <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value="<?php echo $email_customer; ?>" readonly="readonly" id="input-email-order" class="form-control" />
               </div>
               <div class="form-group required">
                  <input type="text" name="telephone" placeholder="<?php echo $entry_telephone; ?>" value="<?php echo $telephone_customer; ?>" readonly="readonly" id="input-telephone-order" class="form-control" />
               </div>
               <?php } else { ?>
               <div class="form-group required">
                  <input type="text" name="name" placeholder="<?php echo $entry_name; ?>" value="<?php echo $name_customer; ?>" id="input-name-order" class="form-control" />
               </div>
               <div class="form-group required">
                  <input type="text" name="email" placeholder="<?php echo $entry_email; ?>" value="<?php echo $email_customer; ?>" id="input-email-order" class="form-control" />
               </div>
               <div class="form-group required">
                  <input type="text" name="telephone" placeholder="<?php echo $entry_telephone; ?>" value="<?php echo $telephone_customer; ?>" id="input-telephone-order" class="form-control" />
               </div>
               <?php } ?>
               <div class="form-group">			   
                  <textarea name="comment" placeholder="<?php echo $entry_comment; ?>" id="order-comment" class="form-control"></textarea>
               </div>
            </div>
            <table class="table">
               <?php foreach ($totals as $total) { ?>
               <tr>
                  <td class="text-right"><strong><?php echo $total['title']; ?></strong></td>
                  <td class="text-right"><?php echo $total['text']; ?></td>
               </tr>
               <?php } ?>
            </table>
            <div class="form-group">
               <input type="button" value="<?php echo $text_checkout; ?>" class="btn btn-center btn-primary" onclick="add_quick_order();">
            </div>
         </div>
      </li>
      <?php } else { ?>
      <li>
         <p class="text-center">
            <i class="fa fa-shopping-cart fa-5x"></i><br>
            <span class="empty-cart"><?php echo $text_empty; ?></span>
         </p>
      </li>
      <?php } ?>
   </ul>
</div>