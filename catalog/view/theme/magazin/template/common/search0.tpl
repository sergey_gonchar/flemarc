<div id="topsearch">
   <span id="sclose">x</span>
   <div class="container">
      <div id="search" class="input-group">
         <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" />
         <span class="input-group-btn">
         <button type="button" class="no-btn btn-lg"><i class="fa fa-search"></i></button>
         </span>
      </div>
   </div>
</div>