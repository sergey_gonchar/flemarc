<?php
class ControllerCommonCallme extends Controller {
	public function index() {
		$this->load->language('product/fastorder');
        $data['heading_title_callme'] = $this->language->get('heading_title_callme');
		$data['callme'] = $this->language->get('callme');
		$data['entry_names'] = $this->language->get('entry_names');
		$data['entry_phone'] = $this->language->get('entry_phone');
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_time'] = $this->language->get('entry_time');
		$data['button_time'] = $this->language->get('button_time');
        $data['title_success'] = $this->language->get('title_success');
		$data['callme_success'] = $this->language->get('callme_success');
		$data['allbottom_success'] = $this->language->get('allbottom_success');
		$data['shop'] = $this->config->get('config_name');
		$data['lang'] = $this->config->get('config_language_id');
	    $data['control_phone_mask'] = $this->config->get('control_phone_mask');
			   		
		if (isset($this->request->post['name'])) {
			$data['name_customer'] = $this->request->post['name'];
		} else {
			$data['name_customer'] = $this->customer->getFirstName();
			}
			if (isset($this->request->post['phone'])) {
			$data['phone_customer'] = $this->request->post['telephone'];
		} else {
			$data['phone_customer'] = $this->customer->getTelephone();
			}	

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/callme.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/callme.tpl', $data);
		} else {
			return $this->load->view('default/template/common/callme.tpl', $data);
		}
		
	}

              public function callme(){
			    $this->load->language('product/fastorder');
				$json = array();
				$json['error_name'] = $this->language->get('error_name');
                $json['error_telephone'] = $this->language->get('error_telephone');			

				
				if((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 35)){
					$json['error']['name'] = true;
					
				}
				if((utf8_strlen($this->request->post['phone']) < 1) || (utf8_strlen($this->request->post['phone']) > 32)){
					$json['error']['phone'] = true;
				}
				
				if(empty($json['error'])){

					$run_query = $this->db->query("INSERT INTO `" . DB_PREFIX . "callme` (customer_phone, customer_name, customer_comment, time, date_created, language_id, store_id) VALUES ('".$_POST['phone']."', '".$_POST['name']."', '".$_POST['comment']."', '".$_POST['time']."', NOW(), '".$this->config->get('config_language')."', '".$this->config->get('config_name')."')");
					
					$subject = sprintf($this->language->get('heading_title_minprice'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
                    $message  = $this->language->get('callme')."\n";
					$message .= $this->language->get('entry_names').":".$this->request->post['name']."\n";
					$message .= $this->language->get('entry_phone').":".$this->request->post['phone']."\n";
					if($this->request->post['comment'])
					$message .= $this->language->get('entry_comment').":".$this->request->post['comment']."\n";
					if($this->request->post['time'])
					$message .= $this->language->get('entry_time').":".$this->request->post['time'];	
					$mail = new Mail();
					$mail->protocol = $this->config->get('config_mail_protocol');
					$mail->parameter = $this->config->get('config_mail_parameter');
					$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
					$mail->smtp_username = $this->config->get('config_mail_smtp_username');
					$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
					$mail->smtp_port = $this->config->get('config_mail_smtp_port');
					$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
					$mail->setTo($this->config->get('config_email'));
					$mail->setFrom($this->config->get('config_email'));
					$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
					$mail->setSubject($subject);
					$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
					$mail->send();
					
					$json['success'] = true;
				}
				$this->response->addHeader('Content-Type: application/json');
				$this->response->setOutput(json_encode($json));
			}
			}