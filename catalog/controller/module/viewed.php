<?php
class ControllerModuleViewed extends Controller {
	public function index($setting) {
        $this->load->language('theme');
		$data['heading_title_viewe'] = $this->language->get('heading_title_viewe');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');	
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_scl'] = $this->language->get('text_scl');
		$data['text_pcs'] = $this->language->get('text_pcs');
		$data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
		$data['text_clear_viewe'] = $this->language->get('text_clear_viewe');
		$data['text_toggle_viewe'] = $this->language->get('text_toggle_viewe');
		$data['text_tooltip_quick'] = $this->language->get('text_tooltip_quick');		
		$data['domen'] = $this->request->server['HTTP_HOST'];	
		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

        $products = array();
		


        if (isset($this->request->cookie['viewed'])) {
            $products = explode(',', $this->request->cookie['viewed']);
        } else if (isset($this->session->data['viewed'])) {
            $products = $this->session->data['viewed'];
        }



		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}

		$products = array_slice($products, 0, (int)$setting['limit']);
		
		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
                
				$options = $this->model_catalog_product->getProductOptions($product_info['product_id']);
				$swapimages = $this->model_catalog_product->getProductImages($product_info['product_id']);
			    if ($swapimages) {
				    $swapimage = $this->model_tool_image->resize($swapimages[0]['image'], $setting['width'], $setting['height']);
			    } else {
				    $swapimage = false;
			    }
				
				$data['products'][] = array(
					'thumb_swap'  => $swapimage,
					'options'     => $options,
					'quantity' => $product_info['quantity'],
				    'stock'=> ($product_info['quantity']<=0) ? $product_info['stock_status'] : $this->language->get('text_instock'),
				    'sale'        => $product_info['price'] == 0 ? 100 : round((($product_info['price'] - $product_info['special'])/$product_info['price'])*100, 0),
					'product_id'  => $product_info['product_id'],
					'thumb'       => $image,
					'name'        => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
				);
			}
		}

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/viewed.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/viewed.tpl', $data);
			} else {
				return $this->load->view('default/template/module/viewed.tpl', $data);
			}
		}
	}
	
	public function delite() {
	
	$json = array();
	
	 $products = array();
			
		
        if (isset($this->request->cookie['viewed'])) {
            $products = explode(',', $this->request->cookie['viewed']);
        } else if (isset($this->session->data['viewed'])) {
            $products = $this->session->data['viewed'];
        }



		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}
		
        if (isset($this->request->post['product_id'])) {
		$var = $this->request->post['product_id'];
        $products = array_diff($products, explode(' ', $var));
		$json['success'] = true;
		
		}

		$products = array_slice($products, 0, (int)$setting['limit']);

		setcookie('viewed', implode(',',$products), time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
		
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		}
		
		
}