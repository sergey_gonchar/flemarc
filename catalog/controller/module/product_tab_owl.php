<?php
class ControllerModuleProductTabOwl extends Controller {
	public function index($setting) {
		$this->load->language('module/product_tab_owl');
		$this->load->language('product/product');
	    $this->load->language('theme');
			
		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['tab_latest'] = $this->language->get('tab_latest');
      	$data['tab_featured'] = $this->language->get('tab_featured');
      	$data['tab_bestseller'] = $this->language->get('tab_bestseller');
      	$data['tab_special'] = $this->language->get('tab_special');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_scl'] = $this->language->get('text_scl');
		$data['text_pcs'] = $this->language->get('text_pcs');
		$data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
		$data['text_filter'] = $this->language->get('text_filter');
		$data['text_clear_filter'] = $this->language->get('text_clear_filter');
		$data['text_tooltip_filter'] = $this->language->get('text_tooltip_filter');
		$data['text_tooltip_clear_filter'] = $this->language->get('text_tooltip_clear_filter');
        $data['text_tooltip_quick'] = $this->language->get('text_tooltip_quick');				

		$this->load->model('catalog/product');

		$this->load->model('tool/image');
        
		//Latest Products
		
		$data['latest_products'] = array();

		$filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$latest_results = $this->model_catalog_product->getProducts($filter_data);

		if ($latest_results) {
			foreach ($latest_results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}
				
				 $swapimages = $this->model_catalog_product->getProductImages($result['product_id']);
			    if ($swapimages) {
				    $swapimage = $this->model_tool_image->resize($swapimages[0]['image'], $setting['width'], $setting['height']);
			    } else {
				    $swapimage = false;
			    }

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
                
				if($result['quantity'] <= 0){
					$result['stock'] = $result['stock_status'];
				}
				elseif($this->config->get('config_stock_display')){
					$result['stock'] = $result['quantity'];
				}
				else{
					$result['stock'] = $this->language->get('text_instock');
				}
				
                $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				
				$data['latest_products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'thumb_swap'  => $swapimage,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'sale'        => $result['price'] == 0 ? 100 : round((($result['price'] - $result['special'])/$result['price'])*100, 0),
					'rating'      => $rating,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'quantity'    => $result['quantity'],
				    'stock'       => $result['stock'],
					'options'     => $options,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}

		}
		
		//Specials product

		$data['special_products'] = array();

		$filter_data = array(
			'sort'  => 'pd.name',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$special_results = $this->model_catalog_product->getProductSpecials($filter_data);

		if ($special_results) {
			foreach ($special_results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}
				
				 $swapimages = $this->model_catalog_product->getProductImages($result['product_id']);
			    if ($swapimages) {
				    $swapimage = $this->model_tool_image->resize($swapimages[0]['image'], $setting['width'], $setting['height']);
			    } else {
				    $swapimage = false;
			    }

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
                
				if($result['quantity'] <= 0){
					$result['stock'] = $result['stock_status'];
				}
				elseif($this->config->get('config_stock_display')){
					$result['stock'] = $result['quantity'];
				}
				else{
					$result['stock'] = $this->language->get('text_instock');
				}
				
                $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				
				$data['special_products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'thumb_swap'  => $swapimage,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'sale'        => $result['price'] == 0 ? 100 : round((($result['price'] - $result['special'])/$result['price'])*100, 0),
					'rating'      => $rating,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'quantity'    => $result['quantity'],
				    'stock'       => $result['stock'],
					'options'     => $options,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
			
		}
			
		//BestSeller
		$data['bestseller_products'] = array();

		$bestseller_results = $this->model_catalog_product->getBestSellerProducts($setting['limit']);

		if ($bestseller_results) {
			foreach ($bestseller_results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}
				
				 $swapimages = $this->model_catalog_product->getProductImages($result['product_id']);
			    if ($swapimages) {
				    $swapimage = $this->model_tool_image->resize($swapimages[0]['image'], $setting['width'], $setting['height']);
			    } else {
				    $swapimage = false;
			    }

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
                
				if($result['quantity'] <= 0){
					$result['stock'] = $result['stock_status'];
				}
				elseif($this->config->get('config_stock_display')){
					$result['stock'] = $result['quantity'];
				}
				else{
					$result['stock'] = $this->language->get('text_instock');
				}
				
                $options = $this->model_catalog_product->getProductOptions($result['product_id']);
				
				$data['bestseller_products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'thumb_swap'  => $swapimage,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'sale'        => $result['price'] == 0 ? 100 : round((($result['price'] - $result['special'])/$result['price'])*100, 0),
					'rating'      => $rating,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'quantity'    => $result['quantity'],
				    'stock'       => $result['stock'],
					'options'     => $options,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
			
		}

		//Featured
		$data['featured_products'] = array();
		$products = array();
			
		$feautured_module_info = $this->model_extension_module->getModule($setting['active_module']);
		if($feautured_module_info['status']){
		$products = $feautured_module_info['product'];
		if (empty($setting['limit'])) {
			$setting['limit'] = 4;
		}

		$products = array_slice($products, 0, (int)$setting['limit']);

		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);

			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}
				
                $swapimages = $this->model_catalog_product->getProductImages($result['product_id']);
			    if ($swapimages) {
				    $swapimage = $this->model_tool_image->resize($swapimages[0]['image'], $setting['width'], $setting['height']);
			    } else {
				    $swapimage = false;
			    }

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
				
                $options = $this->model_catalog_product->getProductOptions($product_info['product_id']);
				
				$data['featured_products'][] = array(
					'product_id'  => $product_info['product_id'],
					'thumb'       => $image,
					'thumb_swap'  => $swapimage,
					'name'        => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'sale'        => $result['price'] == 0 ? 100 : round((($result['price'] - $result['special'])/$result['price'])*100, 0),
					'rating'      => $rating,
					'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'quantity'    => $result['quantity'],
				    'stock'=> ($result['quantity']<=0) ? $result['stock_status'] : $this->language->get('text_instock'),
					'options'     => $options,
					'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
				);
			}
		}
		}
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/product_tab_owl.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/product_tab_owl.tpl', $data);
			} else {
				return $this->load->view('default/template/module/product_tab_owl.tpl', $data);
			}
	}
}