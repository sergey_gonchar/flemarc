<?php
class ControllerModuleCupon extends Controller {
	public function index() {
		$this->load->language('module/cupon');
		$data['heading_title'] = $this->language->get('heading_title');
        $data['text_copy'] = $this->language->get('text_copy');		
		$data['cupon_sale'] = $this->config->get('cupon_sale');
		$data['cupon_info'] = $this->config->get('cupon_info');
		$data['cupon_code'] = $this->config->get('cupon_code');
        $data['heading_success_cupon'] = $this->language->get('heading_success_cupon');
        $data['info_success_cupon'] = $this->language->get('info_success_cupon');
		$data['heading_title'] = $this->language->get('heading_title');

		if ($this->request->server['HTTPS']) {
			$data['code'] = str_replace('http', 'https', html_entity_decode($this->config->get('cupon_code')));
		} else {
			$data['code'] = html_entity_decode($this->config->get('cupon_code'));
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/cupon.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/cupon.tpl', $data);
		} else {
			return $this->load->view('default/template/module/cupon.tpl', $data);
		}
	}
}