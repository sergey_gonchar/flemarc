<?php
class ControllerModuleFaqCategory extends Controller {
	public function index() {
		$this->load->language('module/faq_category');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['faq_category_id'] = $parts[0];
		} else {
			$data['faq_category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

		$this->load->model('coc/faq_category');

		$this->load->model('coc/question');

		$data['faq_categories'] = array();

		$faq_categories = $this->model_coc_faq_category->getFaqCategories(0);
		
		foreach ($faq_categories as $faq_category) {
			$children_data = array();

			if ($faq_category['faq_category_id'] == $data['faq_category_id']) {
				$children = $this->model_coc_faq_category->getFaqCategories($faq_category['faq_category_id']);

				foreach($children as $child) {
					$filter_data = array('filter_faq_category_id' => $child['faq_category_id'], 'filter_sub_faq_category' => true);

					$children_data[] = array(
						'faq_category_id' => $child['faq_category_id'], 
						'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_coc_question->getTotalQuestions($filter_data) . ')' : ''), 
						'href' => $this->url->link('coc/faq_category', 'path=' . $faq_category['faq_category_id'] . '_' . $child['faq_category_id'])
					);
				}
			}

			$filter_data = array(
				'filter_faq_category_id'  => $faq_category['faq_category_id'],
				'filter_sub_faq_category' => true
			);

			$data['faq_categories'][] = array(
				'faq_category_id' => $faq_category['faq_category_id'],
				'name'        => $faq_category['name'] . ($this->config->get('config_question_count') ? ' (' . $this->model_coc_question->getTotalQuestions($filter_data) . ')' : ''),
				'children'    => $children_data,
				'href'        => $this->url->link('coc/faq_category', 'path=' . $faq_category['faq_category_id'])
			);
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/faq_category.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/faq_category.tpl', $data);
		} else {
			return $this->load->view('default/template/module/faq_category.tpl', $data);
		}
	}
}