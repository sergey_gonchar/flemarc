<?php  
class ControllerModuleNews extends Controller {
	public function index() {
		$this->load->language('module/news');
		$this->load->model('extension/news');
		$this->load->model('tool/image');
		$filter_data = array(
			'page' => 1,
			'limit' => 5,
			'start' => 0,
		);
	 
		$data['heading_title'] = $this->language->get('heading_title');
		$data['all_title'] = $this->language->get('all_title');
		$data['text_soc'] = $this->language->get('text_soc');
	 
		$all_news = $this->model_extension_news->getAllNews($filter_data);
	 
		$data['all_news'] = array();
		$data['all_href'] = $this->url->link('information/news');
	    $replace = array(
			'January'=>'Января',
			'February'=>'Февраля',
			'March'=>'Марта',
			'April'=>'Апреля',
			'May'=>'Мая',
			'June'=>'Июня',
			'July'=>'Июля',
			'August'=>'Августа',
			'September'=>'Сентября',
			'October'=>'Октября',
			'November'=>'Ноября',
			'December'=>'Декабря'
		);
		
		foreach ($all_news as $news) {		
		if ($news['image']) {
						$image = $this->model_tool_image->resize($news['image'], 100, 100);
					} else {
						$image = false;
					}
		
			$data['all_news'][] = array (
				'title' 		=> html_entity_decode($news['title'], ENT_QUOTES),
				'thumb'         => $image,
				'description' 	=> (strlen(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))) > 200 ? substr(strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES)), 0, 200) . '...' : strip_tags(html_entity_decode($news['short_description'], ENT_QUOTES))),
				'view' 			=> $this->url->link('information/news/news', 'news_id=' . $news['news_id']),
				'date_added' 	=> strtr(date($this->language->get('date_format_short'), strtotime($news['date_added'])), $replace)
			);
		}
	 
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/news.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/news.tpl', $data);
		} else {
			return $this->load->view('default/template/module/news.tpl', $data);
		}
	}
}