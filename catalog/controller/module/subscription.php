<?php  
class ControllerModuleSubscription extends Controller {
	private $data = array();
	public function index() {
		$this->load->model('setting/setting');
		$this->language->load('module/subscription');

		$this->data['entry_sname'] = $this->language->get('entry_sname');
      	$this->data['entry_semail'] = $this->language->get('entry_semail');
		$this->data['button_subscription'] = $this->language->get('button_subscription');
		$this->data['subscription_success'] = $this->language->get('subscription_success');

		$this->data['currenttemplate'] = $this->config->get('config_template');
		$this->data['language_id'] = $this->config->get('config_language_id');
		$this->data['store_id'] = $this->config->get('config_name');
        $this->data['lang'] = $this->config->get('config_language_id');		
        $this->data['subscription_cupon'] = $this->config->get('subscription_cupon');
        
				
		if (isset($this->request->post['sname'])) {
			$this->data['name_customer'] = $this->request->post['sname'];
		} else {
			$this->data['name_customer'] = $this->customer->getFirstName();
			}
			if (isset($this->request->post['semail'])) {
			$this->data['email_customer'] = $this->request->post['semail'];
		} else {
			$this->data['email_customer'] = $this->customer->getEmail();
			}	
		
		if(file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/subscription.tpl')) {
			return $this->load->view($this->config->get('config_template').'/template/module/subscription.tpl', $this->data);
		} else {
			return $this->load->view('default/template/module/subscription.tpl', $this->data);
		}
	}
	
	public function subscribe() { 
	    $this->language->load('module/subscription');
		$this->data['lang'] = $this->config->get('config_language_id');
		$this->data['subscription_content_tab'] = $this->config->get('subscription_content_tab');
		if (isset($this->request->post['semail'])) {
			$this->data['email_customer'] = $this->request->post['semail'];
		} else {
			$this->data['email_customer'] = $this->customer->getEmail();
			}
		$json = array();
		$json['error_sname'] = $this->language->get('error_sname');
        $json['error_semail'] = $this->language->get('error_semail');
		$json['error_sdemail'] = $this->language->get('error_sdemail');
		if((utf8_strlen($this->request->post['sname']) < 1) || (utf8_strlen($this->request->post['sname']) > 35)){
			$json['error']['sname'] = true;	
		}
		if ((utf8_strlen($this->request->post['semail']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['semail'])) {
			$json['error']['semail'] = true;
		} else {
		
		$result = $this->db->query("SELECT * FROM `" . DB_PREFIX . "subscription` WHERE `customer_email`='".$_POST['semail']."'");
		
			if ($result->row) {
				$json['error']['sdemail'] = true;
			}
		
		}
		
		if(empty($json['error'])){
			
			$run_query = $this->db->query("INSERT INTO `" . DB_PREFIX . "subscription` (customer_email, customer_name, date_created, language_id, store_id) VALUES ('".$_POST['semail']."', '".$_POST['sname']."', NOW(), '".$_POST['language_id']."', '".$_POST['store_id']."')");
						
			$message  = '<html dir="ltr" lang="en">' . "\n";
			$message .= '  <head>' . "\n";
			$message .= '    <title>' . $this->language->get('theme_subscription') . '</title>' . "\n";
			$message .= '    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">' . "\n";
			$message .= '  </head>' . "\n";
			$message .= '  <body>' . html_entity_decode($this->data['subscription_content_tab'][$this->config->get('config_language_id')], ENT_QUOTES, 'UTF-8') . '</body>' . "\n";
			$message .= '</html>' . "\n";
			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->setTo($this->data['email_customer']);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('theme_subscription')));
			$mail->setHtml(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
						
			$json['success'] = true;
		}
				
			$this->response->addHeader('Content-Type: application/json');
		    $this->response->setOutput(json_encode($json));				
	}	
}
?>