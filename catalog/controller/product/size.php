<?php
class ControllerProductSize extends Controller {
	public function index() {
		$this->load->language('product/fastorder');
		$data['heading_title_size'] = $this->language->get('heading_title_size');
		$data['entry_names'] = $this->language->get('entry_names');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_size'] = $this->language->get('entry_size');
        $data['text_woman'] = $this->language->get('text_woman');
        $data['text_men'] = $this->language->get('text_men');
		$data['button_size'] = $this->language->get('button_size');
		$data['title_success'] = $this->language->get('title_success');
		$data['size_success'] = $this->language->get('size_success');
		$data['size_info'] = $this->language->get('size_info');
		$data['url_size'] = $this->language->get('url_size');
		$data['allbottom_success'] = $this->language->get('allbottom_success');
		$data['shop'] = $this->config->get('config_name');
	    
        if (isset($this->request->get['product_id'])) { $product_id = (int)$this->request->get['product_id']; } else { $product_id = 0; }
		$product_info = $this->model_catalog_product->getProduct( $product_id );
		$data['name_product'] = $product_info['name'];
	
        // product price

        if ( ( $this->config->get( 'config_customer_price' ) && $this->customer->isLogged() ) || !$this->config->get( 'config_customer_price' ) ) {
            $data['price'] = $this->currency->format( $this->tax->calculate( $product_info['price'], $product_info['tax_class_id'], $this->config->get( 'config_tax' ) ) );
        } else {
            $data['price'] = false;
        }

        // product special

        if ( (float) $product_info['special'] ) {
            $data['special'] = $this->currency->format( $this->tax->calculate( $product_info['special'], $product_info['tax_class_id'], $this->config->get( 'config_tax' ) ) );
        } else {
            $data['special'] = false;
        }
		
		
		if (isset($this->request->post['name'])) {
			$data['name_customer'] = $this->request->post['name'];
		} else {
			$data['name_customer'] = $this->customer->getFirstName();
			}
			if (isset($this->request->post['name'])) {
			$data['email_customer'] = $this->request->post['email'];
		} else {
			$data['email_customer'] = $this->customer->getEmail();
			}	

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/size.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/product/size.tpl', $data);
		} else {
			return $this->load->view('default/template/product/size.tpl', $data);
		}
		
	}

	public function size(){
		$this->load->language('product/fastorder');
		$json = array();
		$json['error_name'] = $this->language->get('error_name');
		$json['error_email'] = $this->language->get('error_email');			
		$json['error_size'] = $this->language->get('error_size');
		
		if((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 35)){
			$json['error']['name'] = true;
			
		}
		if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$json['error']['email'] = true;
		}
		if(utf8_strlen($this->request->post['size']) < 6){
			$json['error']['size'] = true;
		}
		
		if(empty($json['error'])){
			$subject = sprintf($this->language->get('heading_title_size'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$message  = $this->language->get('heading_title_size')."\n";
			$message .= $this->language->get('entry_names').":".$this->request->post['name']."\n";
			$message .= $this->language->get('entry_email').":".$this->request->post['email']."\n";
			$message .= $this->language->get('entry_size').":".$this->request->post['size']."\n";
			$message .= $this->language->get('entry_product_name').":".$this->request->post['size_product_name']."\n";
			$message .= $this->language->get('entry_product_price').":".$this->request->post['size_product_price']."\n";	

			
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject($subject);
			$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
			$mail->send();
			
			$json['success'] = true;
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}	
}			
			