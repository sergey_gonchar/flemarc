<?php
class ControllerProductFastorder extends Controller {
	public function index() {
		$this->load->language('product/fastorder');
        $data['heading_title'] = $this->language->get('heading_title');
		$data['fastorder_info'] = $this->language->get('fastorder_info');
		$data['entry_names'] = $this->language->get('entry_names');
		$data['entry_phone'] = $this->language->get('entry_phone');
		$data['entry_comment'] = $this->language->get('entry_comment');
		$data['entry_time'] = $this->language->get('entry_time');
		$data['button_fastorder'] = $this->language->get('button_fastorder');
		$data['order_success'] = $this->language->get('order_success');
		$data['order_success_info'] = $this->language->get('order_success_info');
        $data['allbottom_success'] = $this->language->get('allbottom_success');
		$data['shop'] = $this->config->get('config_name');		
	    
        if (isset($this->request->get['product_id'])) { $product_id = (int)$this->request->get['product_id']; } else { $product_id = 0; }
		$product_info = $this->model_catalog_product->getProduct( $product_id );
		$data['name_product'] = $product_info['name'];
		
		if ($product_info['image']) {
				$data['thumb_fixed'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
			} else {
				$data['thumb_fixed'] = '';
			}		
		
        // product price

        if ( ( $this->config->get( 'config_customer_price' ) && $this->customer->isLogged() ) || !$this->config->get( 'config_customer_price' ) ) {
            $data['price'] = $this->currency->format( $this->tax->calculate( $product_info['price'], $product_info['tax_class_id'], $this->config->get( 'config_tax' ) ) );
        } else {
            $data['price'] = false;
        }

        // product special

        if ( (float) $product_info['special'] ) {
            $data['special'] = $this->currency->format( $this->tax->calculate( $product_info['special'], $product_info['tax_class_id'], $this->config->get( 'config_tax' ) ) );
        } else {
            $data['special'] = false;
        }
		
		
		if (isset($this->request->post['name'])) {
			$data['name_customer'] = $this->request->post['name'];
		} else {
			$data['name_customer'] = $this->customer->getFirstName();
			}
			if (isset($this->request->post['name'])) {
			$data['phone_customer'] = $this->request->post['telephone'];
		} else {
			$data['phone_customer'] = $this->customer->getTelephone();
			}	

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/fastorder.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/product/fastorder.tpl', $data);
		} else {
			return $this->load->view('default/template/product/fastorder.tpl', $data);
		}
		
	}

    public function fastorder(){
	$this->load->language('product/fastorder');

	$json = array();
	$json['error_name'] = $this->language->get('error_name');
	$json['error_telephone'] = $this->language->get('error_telephone');			

	
	if((utf8_strlen($this->request->post['order_name']) < 1) || (utf8_strlen($this->request->post['order_name']) > 35)){
		$json['error']['quick_name'] = true;
		
	}
	if((utf8_strlen($this->request->post['order_phone']) < 1) || (utf8_strlen($this->request->post['order_phone']) > 32)){
		$json['error']['quick_phone'] = true;
	}
	
	if(empty($json['error'])){
		$subject = sprintf($this->language->get('heading_title'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$message  = $this->language->get('heading_title')."\n";
		$message .= $this->language->get('entry_product_name').":".$this->request->post['order_product_name']."\n";
		$message .= $this->language->get('entry_product_price').":".$this->request->post['order_product_price']."\n";
		$message .= $this->language->get('entry_names').":".$this->request->post['order_name']."\n";
		$message .= $this->language->get('entry_phone').":".$this->request->post['order_phone']."\n";
		if($this->request->post['order_comment'])
			$message .= $this->language->get('entry_comment').":".$this->request->post['order_comment']."\n";
		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');
		$mail->setTo($this->config->get('config_email'));
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
		$mail->send();
		
		$json['success'] = true;
	}
	$this->response->addHeader('Content-Type: application/json');
	$this->response->setOutput(json_encode($json));
    }						
}