<?php
class ControllerCocFaqCategory extends Controller {
	public function index() {
		$this->load->language('coc/faq_category');

		$this->load->model('coc/faq_category');

		$this->load->model('coc/question');

		$this->load->model('tool/image');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$limit = $this->config->get('config_product_limit');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_faq'),
			'href' => $this->url->link('coc/faq')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$faq_category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$faq_category_info = $this->model_coc_faq_category->getFaqCategory($path_id);

				if ($faq_category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $faq_category_info['name'],
						'href' => $this->url->link('coc/faq_category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$faq_category_id = 0;
		}
		

		$faq_category_info = $this->model_coc_faq_category->getFaqCategory($faq_category_id);

		if ($faq_category_info) {
			$this->document->setTitle($faq_category_info['meta_title']);
			$this->document->setDescription($faq_category_info['meta_description']);
			$this->document->setKeywords($faq_category_info['meta_keyword']);
			$this->document->addLink($this->url->link('coc/faq_category', 'path=' . $this->request->get['path']), 'canonical');

			$data['heading_title'] = $faq_category_info['name'];

			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_faq'] = $this->language->get('text_faq');

			$data['button_continue'] = $this->language->get('button_continue');

			// Set the last faq_category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $faq_category_info['name'],
				'href' => $this->url->link('coc/faq_category', 'path=' . $this->request->get['path'])
			);


			$url = '';


			$data['faq_categories'] = array();

			$results = $this->model_coc_faq_category->getFaqCategories($faq_category_id);
			
	

			foreach ($results as $result) {
				$filter_data = array(
					'filter_faq_category_id'  => $result['faq_category_id'],
					'filter_sub_faq_category' => true
				);

				$data['faq_categories'][] = array(
					'name'  => $result['name'] . ($this->config->get('config_question_count') ? ' (' . $this->model_coc_question->getTotalQuestions($filter_data) . ')' : ''),
					'href'  => $this->url->link('coc/faq_category', 'path=' . $this->request->get['path'] . '_' . $result['faq_category_id'] . $url)
				);
			}

			$data['questions'] = array();

			$filter_data = array(
				'filter_faq_category_id' => $faq_category_id,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);

			$question_total = $this->model_coc_question->getTotalQuestions($filter_data);

			$results = $this->model_coc_question->getQuestions($filter_data);

			foreach ($results as $result) {

				if($this->config->get('coc_faq_template') == 'default') {
					$data['questions'][] = array(
						'question_id'  => $result['question_id'],
						'title'        => html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'),
						'answer' 	   => html_entity_decode($result['answer'], ENT_QUOTES, 'UTF-8'),
					);
				}elseif($this->config->get('coc_faq_template') == 'coc018_1'){
					$data['questions'][] = array(
						'question_id'  => $result['question_id'],
						'title'        => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8')),
						'answer' 	   => strip_tags(html_entity_decode($result['answer'], ENT_QUOTES, 'UTF-8')),
					);
				}else{
					
					$data['questions'][] = array(
						'question_id'  => $result['question_id'],
						'title'        => html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'),
						'answer' 	   => html_entity_decode($result['answer'], ENT_QUOTES, 'UTF-8'),
					);
					
				}
				
			}

			$url = '';



			$pagination = new Pagination();
			$pagination->total = $question_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('coc/faq_category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($question_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($question_total - $limit)) ? $question_total : ((($page - 1) * $limit) + $limit), $question_total, ceil($question_total / $limit));


			$data['continue'] = $this->url->link('common/home');
			
			$template = $this->config->get('coc_faq_template');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/coc/template/'.$template.'.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/coc/template/'.$template.'.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/coc/template/'.$template.'.tpl', $data));
			}
		} else {
			$url = '';


			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('coc/faq_category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
}