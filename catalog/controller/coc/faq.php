<?php
class ControllerCocFaq extends Controller {
	public function index() {
		$this->load->language('coc/faq');

		$this->load->model('coc/question');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$limit = $this->config->get('config_product_limit');
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		
		$coc_faq_description = $this->config->get('coc_faq_description');
		

		$this->document->setTitle($coc_faq_description[(int)$this->config->get('config_language_id')]['meta_title']);
		$this->document->setDescription($coc_faq_description[(int)$this->config->get('config_language_id')]['meta_description']);
		$this->document->setKeywords($coc_faq_description[(int)$this->config->get('config_language_id')]['meta_keyword']);
		$this->document->addLink($this->url->link('coc/faq', ''), 'canonical');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_faq'] = $this->language->get('text_faq');

		$data['button_continue'] = $this->language->get('button_continue');

		// Set the last category breadcrumb
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_faq'),
			'href' => $this->url->link('coc/faq', '')
		);


		$data['questions'] = array();

		$filter_data = array(
			'start'              => ($page - 1) * $limit,
			'limit'              => $limit
		);

		$question_total = $this->model_coc_question->getTotalQuestions($filter_data);

		$results = $this->model_coc_question->getQuestions($filter_data);

		foreach ($results as $result) {

				if($this->config->get('coc_faq_template') == 'default') {
					$data['questions'][] = array(
						'question_id'  => $result['question_id'],
						'title'        => html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'),
						'answer' 	   => html_entity_decode($result['answer'], ENT_QUOTES, 'UTF-8'),
					);
				}elseif($this->config->get('coc_faq_template') == 'coc018_1'){
					$data['questions'][] = array(
						'question_id'  => $result['question_id'],
						'title'        => strip_tags(html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8')),
						'answer' 	   => strip_tags(html_entity_decode($result['answer'], ENT_QUOTES, 'UTF-8')),
					);
				}else{
					
					$data['questions'][] = array(
						'question_id'  => $result['question_id'],
						'title'        => html_entity_decode($result['title'], ENT_QUOTES, 'UTF-8'),
						'answer' 	   => html_entity_decode($result['answer'], ENT_QUOTES, 'UTF-8'),
					);
					
				}
				
			}


		$pagination = new Pagination();
		$pagination->total = $question_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('coc/faq', 'page={page}');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($question_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($question_total - $limit)) ? $question_total : ((($page - 1) * $limit) + $limit), $question_total, ceil($question_total / $limit));

		$data['continue'] = $this->url->link('common/home');
		
		$template = $this->config->get('coc_faq_template');
		

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/coc/template/'.$template.'.tpl')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/coc/template/'.$template.'.tpl', $data));
		} else {
			$this->response->setOutput($this->load->view('default/template/coc/template/'.$template.'.tpl', $data));
		}
		
	}
}