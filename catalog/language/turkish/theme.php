<?php

// Text
$_['text_quantity']             = 'Количество:';
$_['text_scl']                  = 'в наличии';
$_['text_pcs']                  = 'шт.';
$_['text_out_of_stock']         = 'нет в наличии';
$_['text_outstock']             = 'доступен предзаказ для этого кликните на опцию';
$_['text_search_theme']         = 'поиск';
$_['text_regim']                = 'режим работы';
$_['text_filter']               = 'фильтр';
$_['text_clear_filter']         = 'очистить';
$_['text_tooltip_filter']       = 'фильтровать товары';
$_['text_tooltip_clear_filter'] = 'очистить фильтр';
$_['text_viewed']               = 'вы смотрели';
$_['text_or']                   = 'или';
$_['text_theme_cart']           = 'cart';
$_['text_brand']                = 'бренды';

//Quick_viewe
$_['text_tooltip_quick']        = 'Быстрый просмотр';
$_['text_more']                 = 'Подробнее';

//Viewed
$_['heading_title_viewe']       = 'Вы смотрели';
$_['text_clear_viewe']          = 'Очистить';
$_['text_toggle_viewe']         = 'Свернуть';