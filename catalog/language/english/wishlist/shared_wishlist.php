<?php
// Heading 
$_['heading_title'] = 'Shared Wish List';
$_['page_title'] = 'Wish List for %s';
// Text
$_['text_instock']  = 'In Stock';
$_['text_empty']    = 'This wish list is empty.';

// Column
$_['column_image']  = 'Image';
$_['column_name']   = 'Product Name';
$_['column_model']  = 'Model';
$_['column_stock']  = 'Stock';
$_['column_price']  = 'Unit Price';
$_['column_action'] = 'Action';
?>