<?php
$_['entry_email'] = 'Email';
$_['entry_name'] = 'Name';
$_['entry_password'] = 'Password';
$_['entry_telephone'] = 'Telephone';
$_['text_forgotten'] = 'Forgotten Password';
$_['text_or'] = 'or';
$_['text_info'] = 'info';
$_['text_agree'] = 'I have read and agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['button_login'] = 'Login';
$_['button_register'] = 'Register';
$_['entry_lastname'] = 'Surname';
?>