<?php
// Heading
$_['heading_title']              = 'Fast order';
$_['heading_title_minprice']     = 'cheaper?';
$_['heading_title_callme']       = 'call back';
$_['heading_title_outstock']     = 'Report will appear when required option';
$_['heading_title_size']         = 'What size?';

// Text
$_['text_woman']                 = 'Woman';
$_['text_men']                   = 'Man';


$_['entry_product_name']         = 'Product';
$_['entry_product_price']        = 'Price';
$_['entry_product_option']       = 'Required option';
$_['callme']                     = 'Call back';
$_['size']                       = 'Help with size';		
$_['error_name']                 = 'First Name must be between 1 and 32 characters!';
$_['error_telephone']            = 'Warning: Telephone is already registered!';
$_['error_email']                = 'Warning: E-Mail Address is already registered!';

$_['error_url']                  = 'The link must be greater than 6 characters';
$_['error_size']                 = 'Fill in your size';
$_['entry_names']                = 'Name';
$_['entry_phone']                = 'Phone';
$_['entry_comment']              = 'Comment';
$_['entry_email']                = 'E-mail';
$_['entry_time']                 = 'Time to call';
$_['entry_url']                  = 'Link to cheap goods';
$_['entry_value']                = 'Missing option:';
$_['entry_size']                 = 'your size (example:1-90;2-60;3-90)';
$_['button_fastorder']           = 'Order';
$_['button_minprice']            = 'Notify';
$_['button_outstock']            = 'Notify';
$_['button_size']                = 'Notify';
$_['url_size']                   = 'Notify';

$_['size_info']                  = 'Measure your measurements and send us. Our experts will select for You the suitable size for this product. Recommendations we will send to Your mail.';
$_['minprice_info']              = 'Fill in the form and possibly we will reduce our price specially for You';
$_['fastorder_info']             = 'Just fill in your details and the order will be processed.';
$_['title_success']              = 'Thank you for your interest in our product';
$_['order_success']              = 'Thank you. The order is successfully submitted';
$_['callme_success']             = 'Very soon we will call You';
$_['order_success_info']         = 'Soon we will contact with You and clarify the details of the order';
$_['outstock_info']              = 'If the product remained with the supplier and not discontinued, we specially for You will book it. As soon as the product arrives to us, we will notify You by mail';


$_['minprice_success']           = 'We will consider giving discounts especially for You. Very soon You will be informed via email.';
$_['outstock_success']           = 'We are already working on your pre-order. Very soon we will notify You of the outcome by mail';
$_['size_success']               = 'Our experts, based on your measurements, already select required size this product. Very soon we will send recommendations on mail';
$_['allbottom_success']           = 'Sincerely, <br> team';



