<?php
// Text
$_['heading_title']  = 'Frequently Asked Questions';
$_['text_faq']      	= 'Frequently Asked Questions';
$_['text_question']     = 'FAQ';
$_['text_error']        = 'FAQ Category not found!';
$_['text_empty']        = 'There are no questions to list in this faq category.';
