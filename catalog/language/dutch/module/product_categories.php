<?php
// Heading
$_['heading_title']    		= 'Products by Category';

// Text
$_['text_license'] 			= 'The license is not valid';
$_['text_manufacturer']    	= 'Brand:';
$_['text_model']    		= 'Product Code:';
$_['text_sku']    			= 'SKU:';
$_['text_weight']    		= 'Weight:';
$_['text_reward']    		= 'Reward Points:';
$_['text_stock']    		= 'Availability:';
$_['text_instock']    		= 'In Stock';
$_['text_tax']              = 'Ex Tax:'; 
$_['text_points']           = 'Price in reward points:';
$_['text_discount']         = ' or more ';

// Button
$_['button_quick_view']    	= 'Quick View';
$_['button_more']    		= 'More';