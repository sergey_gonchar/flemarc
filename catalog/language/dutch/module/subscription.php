<?php
$_['theme_subscription']   = 'Купон за подписку';
$_['entry_sname']          = 'Ваше имя';
$_['entry_semail']         = 'Ваш email';
$_['button_subscription']  = 'Подписаться';
$_['error_sname']          = 'Имя должно быть от 1 до 32 символов!';
$_['error_semail']         = 'Неверный email';
$_['error_sdemail']        = 'Этот email уже подписан';
$_['subscription_success'] = '<span style="font-size: 18px;">Спасибо за подписку.</span><br>Купон мы выслали Вам на почту.';
?>