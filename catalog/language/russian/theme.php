<?php

// Text
$_['text_quantity']             = 'Количество:';
$_['text_scl']                  = 'в наличии';
$_['text_pcs']                  = 'шт.';
$_['text_out_of_stock']         = 'нет в наличии';
$_['text_outstock']             = 'доступен предзаказ для этого кликните на опцию';
$_['text_search_theme']         = 'поиск';
$_['text_regim']                = 'режим работы';
$_['text_filter']               = 'фильтр';
$_['text_clear_filter']         = 'очистить';
$_['text_tooltip_filter']       = 'фильтровать товары';
$_['text_tooltip_clear_filter'] = 'очистить фильтр';
$_['text_viewed']               = 'вы смотрели';
$_['text_or']                   = 'или';
$_['text_theme_cart']           = 'корзина';
$_['text_brand']                = 'бренды';
$_['text_size']                 = 'Таблица размеров';
$_['text_econom']               = 'Экономия';

//Quick_order

$_['entry_name']                 = 'Имя';
$_['entry_email']                = 'Почта';
$_['entry_telephone']            = 'Телефон';
$_['entry_comment']              = 'Комментарий';
$_['quick_order']                = 'Быстрый заказ';
$_['quick_order_cart']           = '↑ Товары в корзине';
$_['quick_order_close']          = 'Продолжить покупки';

$_['text_customer_order']        = '<h3 class="success">Спасибо за заказ</h3><p class="left">Скоро наш менеджер свяжится с Вами для уточнения заказа.<br>Вы можете просматривать историю заказов в <a class="href" href="%s">Личном кабинете</a> открыв <a class="href" href="%s">Историю заказов</a><br>Если у вас возникнут какие либо вопросы, пожалуйста, <a class="href" href="%s">свяжитесь с нами.</a></p><b class="right">C уважением, <br>%s</b>';
$_['text_guest_order']           = '<h3 class="success">Спасибо за заказ</h3><p class="left">Скоро наш менеджер свяжится с Вами для уточнения заказа.<br>Номер Вашего заказа  #%s. <br>Если у вас возникнут какие либо вопросы, пожалуйста, <a class="href" href="%s">свяжитесь с нами.</a></p><b class="right">C уважением, <br>%s</b>';




//Quick_viewe
$_['text_tooltip_quick']        = 'Быстрый просмотр';
$_['text_more']                 = 'Подробнее';

//Viewed
$_['heading_title_viewe']       = 'Вы смотрели';
$_['text_clear_viewe']          = 'Очистить';
$_['text_toggle_viewe']         = 'Свернуть';