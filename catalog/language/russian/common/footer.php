<?php
$_['text_information'] = 'Информация';
$_['text_service'] = 'Служба поддержки';
$_['text_extra'] = 'Дополнительно';
$_['text_contact'] = 'Связаться с нами';
$_['text_return'] = 'Возврат товара';
$_['text_sitemap'] = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher'] = 'Подарочные сертификаты';
$_['text_affiliate'] = 'Партнёры';
$_['text_special'] = 'Скидки';
$_['text_account'] = 'Личный Кабинет';
$_['text_order'] = 'История заказов';
$_['text_wishlist'] = 'Мои Закладки';
$_['text_newsletter'] = 'Рассылка новостей';
$_['text_powered'] = 'Работает на <a target="_blank" href="http://myopencart.com/">ocStore</a><br /> %s &copy; %s';
$_['entry_cookie_more'] = 'Подробнее';
$_['entry_cookie_accept'] = 'Согласен!';
$_['entry_cookie_msg'] = 'Этот сайт собирает статистику.';
?>