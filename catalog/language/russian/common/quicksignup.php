<?php
// Text
$_['entry_email']             = 'Email';
$_['entry_name']              = 'Имя';
$_['entry_lastname']          = 'Фамилия';
$_['entry_password']          = 'Пароль';
$_['entry_telephone']         = 'Телефон';
$_['text_forgotten']          = 'Забыли пароль?';
$_['text_or']                 = 'или';
$_['text_info']               = '*Зарегистрированные пользователи могут быстрее оформлять заказы, отслеживать их статус и просматривать историю покупок.';
$_['text_agree']              = 'Я прочел и согласен с <a href="%s" class="agree"><b>%s</b></a>';

//Button
$_['button_login']            = 'Войти';
$_['button_register']         = 'Зарегистрироваться';
