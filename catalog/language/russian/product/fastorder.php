<?php
// Heading
$_['heading_title']              = 'Быстрый заказ';
$_['heading_title_minprice']     = 'нашли дешевле?';
$_['heading_title_callme']       = 'перезвонить';
$_['heading_title_outstock']     = 'Сообщить когда появится необходимая опция';
$_['heading_title_size']         = 'Сомневаетесь в размере?';

// Text
$_['text_woman']                 = 'Женщина';
$_['text_men']                   = 'Мужчина';


$_['entry_product_name']         = 'Товар';
$_['entry_product_price']        = 'Цена';
$_['entry_product_option']       = 'Отсутствующая опция';
$_['callme']                     = 'Заказ звонка';
$_['size']                       = 'Помощь с размером';		
$_['error_name']                 = 'Имя должно быть от 1 до 32 символов!';
$_['error_telephone']            = 'Введите телефон';
$_['error_email']                = 'Введите email';

$_['error_url']                  = 'Ссылка должна быть больше 6 символов';
$_['error_size']                 = 'Заполните ваши измерения';
$_['entry_names']                = 'Имя';
$_['entry_phone']                = 'Телефон';
$_['entry_comment']              = 'Комментарий';
$_['entry_email']                = 'Почта';
$_['entry_time']                 = 'Удобное время для звонка';
$_['entry_url']                  = 'ссылка на дешевый товар';
$_['entry_value']                = 'Желаемая опция:';
$_['entry_size']                 = 'Ваша мерка (например:1-90;2-60;3-90)';
$_['button_fastorder']           = 'Заказать';
$_['button_minprice']            = 'Сообщить';
$_['button_outstock']            = 'Сообщить';
$_['button_size']                = 'Подсказать';
$_['url_size']                   = 'Определить размер';

$_['size_info']                  = 'Снимите необходимые мерки и пошлите нам. Наши специалисты подберут для Вас подходящий размер для этого товара. Рекомендации мы вышлем на Вашу почту.';
$_['minprice_info']              = 'Заполните форму и возможно мы пересмотрим нашу цену специально для Вас';
$_['fastorder_info']             = 'Просто заполните свои данные и заказ будет оформлен. Внимание! При данном заказе бонусы автоматически не начисляются';
$_['title_success']              = 'Спасибо за проявленный интерес к нашему товару';
$_['order_success']              = 'Спасибо. Заказ успешно оформлен';
$_['callme_success']             = 'Совсем скоро мы перезвоним Вам';
$_['order_success_info']         = 'Скоро мы свяжемся с Вами и уточним детали заказа';
$_['outstock_info']              = 'Если товар остался у поставщика и не снят с производства, то мы специально для Вас закажем его. Как только товар поступит к нам, мы уведомим Вас по почте';


$_['minprice_success']           = 'Мы рассмотрим возможность предоставления скидки специально для Вас. Совсем скоро мы оповестим Вас по почте.';
$_['outstock_success']           = 'Мы уже работаем по вашему предзаказу. Совсем скоро мы уведомим Вас о результате по почте';
$_['size_success']               = 'Наши специалисты, исходя из ваших мерок, уже подбирают необходимый размер данного товара. Совсем скоро мы вышлем рекомендации на почту';
$_['allbottom_success']          = 'C уважением, <br> команда';



