<?php
// Heading
$_['heading_title']     = 'Наши Бренды';

// Text
$_['text_brand']        = 'Бренды';
$_['text_index']        = 'Алфавитный указатель:';
$_['text_error']        = 'Бренд не найден!';
$_['text_empty']        = 'Этот лист пуст.';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Бренды:';
$_['text_model']        = 'Код товара:';
$_['text_points']       = 'Бонусы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без налога:';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировать:';
$_['text_default']      = 'По умолчанию';
$_['text_name_asc']     = 'Название (A - Я)';
$_['text_name_desc']    = 'Название (Я - A)';
$_['text_price_asc']    = 'Цена по возрастанию';
$_['text_price_desc']   = 'Цена по убыванию';
$_['text_rating_asc']   = 'Рейтинг по возрастанию';
$_['text_rating_desc']  = 'Рейтинг по убыванию';
$_['text_model_asc']    = 'Модель (A - Z)';
$_['text_model_desc']   = 'Модель (Z - A)';
$_['text_limit']        = 'Показывать:';
