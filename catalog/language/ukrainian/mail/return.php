<?php
$_['text_date'] = 'Дата:';
$_['text_model'] = 'Модель:';
$_['text_opened'] = 'Открыто:';
$_['text_product'] = 'Товар:';
$_['text_quantity'] = 'Количество:';
$_['text_reason'] = 'Причина:';
$_['text_return'] = 'Информация о возврате:';
$_['text_return_date'] = 'Детали возврата:';
$_['text_return_detail'] = 'Детали возврата:';
$_['text_telephone'] = 'Телефон:';
$_['text_return_id'] = 'ID возврата:';
$_['text_subject'] = 'Новый возврат: {$store_name}';
$_['text_order_id'] = 'Номер заказа:';
$_['text_order'] = 'Информация о заказе';
$_['text_order_date'] = 'Дата заказа:';
?>