<?php
$_['text_product'] = 'Товары';
$_['text_error'] = 'Категория не найдена';
$_['text_quantity'] = 'Кол:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_price'] = 'Цена:';
$_['text_tax'] = 'Без НДС:';
$_['text_compare'] = 'Сравнить (%s)';
$_['text_limit'] = 'Показать:';
$_['text_model_desc'] = 'Модель (Я-А)';
$_['text_model_asc'] = 'Модель (А-Я)';
$_['text_empty'] = 'Продукты не найдены';
$_['text_model'] = 'Код:';
$_['text_sort'] = 'Сортировать:';
$_['text_default'] = 'По умолчанию';
$_['text_name_asc'] = 'Название (А-Я)';
$_['text_name_desc'] = 'Название (Я-А)';
$_['text_price_asc'] = 'Цена (Низкая > Высокая)';
$_['text_price_desc'] = 'Цена (Высокая > Низкая)';
$_['text_rating_asc'] = 'Рейтинг (Низкий)';
$_['text_rating_desc'] = 'Рейтинг (Высокий)';
$_['text_refine'] = 'Поиск';
$_['text_points'] = 'Баллы';
?>