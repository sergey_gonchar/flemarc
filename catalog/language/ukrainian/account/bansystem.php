<?php
/*------------------------------------------------------------------------
# Ban System
# ------------------------------------------------------------------------
# The Krotek
# Copyright (C) 2011-2015 thekrotek.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://thekrotek.com
# Support: support@thekrotek.com
-------------------------------------------------------------------------*/

$_['error_banned'] = "Warning: Operation aborted. Please, contact administrator.";

?>