<?php
$_['text_administrator'] = 'Администратор';
$_['button_clear'] = 'Очистить';
$_['entry_message'] = 'Сообщение';
$_['button_send'] = 'Отправить';
$_['text_confirm'] = 'Вы уверены?';
$_['text_no_message'] = 'Сообщения отстутствуют';
$_['text_message_system'] = 'Сообщения';
$_['heading_title'] = 'Сообщения';
$_['text_account'] = 'Учетная запись';
$_['text_success'] = 'Сообщение отпралено!';
$_['error_message'] = 'Впишите сообщение!';
?>