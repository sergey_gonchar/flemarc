<?php
$_['heading_title'] = 'Карта сайта';
$_['text_special'] = 'Скидки';
$_['text_account'] = 'Личный Кабинет';
$_['text_edit'] = 'Учетная запись';
$_['text_password'] = 'Смена пароля';
$_['text_address'] = 'Список адресов доставки';
$_['text_history'] = 'История заказов';
$_['text_download'] = 'Файлы для скачивания';
$_['text_cart'] = 'Корзина покупок';
$_['text_checkout'] = 'Оформление заказа';
$_['text_search'] = 'Поиск';
$_['text_information'] = 'Информация';
$_['text_contact'] = 'Связаться с нами';
?>