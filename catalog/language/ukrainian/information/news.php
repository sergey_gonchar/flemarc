<?php
// Heading
$_['heading_title'] 	= 'Новости';
 
// Text
$_['text_title'] 		= 'Заголовок';
$_['text_description'] 	= 'Описание';
$_['text_date'] 		= 'Дата добавления';
$_['text_view'] 		= 'Подробнее';
$_['text_soc'] 		    = 'Расскажи друзьям';
$_['text_error'] 		= 'Страницы не существует!';

