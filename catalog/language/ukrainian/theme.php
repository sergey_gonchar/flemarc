<?php

// Text
$_['text_quantity']             = 'Кількість:';
$_['text_scl']                  = 'В наявності';
$_['text_pcs']                  = 'шт.';
$_['text_out_of_stock']         = 'немає в наявності';
$_['text_outstock']             = 'передзамовлення доступний для цього натисніть на опцію';
$_['text_search_theme']         = 'пошук';
$_['text_regim']                = 'режим роботи';
$_['text_filter']               = 'фільтр';
$_['text_clear_filter']         = 'очистити';
$_['text_tooltip_filter']       = 'фільтрувати товари';
$_['text_tooltip_clear_filter'] = 'очистити фільтр';
$_['text_viewed']               = 'ви дивилися';
$_['text_or']                   = 'або';
$_['text_theme_cart']           = 'кошик';
$_['text_brand']                = 'бренди';

//Quick_viewe
$_['text_tooltip_quick']        = 'Швидкий перегляд';
$_['text_more']                 = 'Детальніше';

//Viewed
$_['heading_title_viewe']       = 'Ви дивилися';
$_['text_clear_viewe']          = 'Очистити';
$_['text_toggle_viewe']         = 'Згорнути';