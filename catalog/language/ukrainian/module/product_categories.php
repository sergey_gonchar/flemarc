<?php
// Heading
$_['heading_title']    		= 'Товары по категориям';

// Text
$_['text_license'] 			= 'Лицензия недействительна';
$_['text_manufacturer']    	= 'Производитель:';
$_['text_model']    		= 'Модель:';
$_['text_sku']    			= 'Артикул:';
$_['text_weight']    		= 'Вес:';
$_['text_reward']    		= 'Бонусные баллы:';
$_['text_stock']    		= 'Наличие:';
$_['text_instock']    		= 'В наличии';
$_['text_tax']              = 'Без НДС:'; 
$_['text_points']           = 'Цена в бонусных баллах:';
$_['text_discount']         = ' шт. или более по цене за 1шт. - ';

// Button
$_['button_quick_view']    	= 'Быстрый просмотр';
$_['button_more']    		= 'Подробнее';