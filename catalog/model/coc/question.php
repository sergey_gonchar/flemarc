<?php
class ModelCocQuestion extends Model {

	public function getQuestion($question_id) {
		$query = $this->db->query("SELECT DISTINCT *, pd.title AS title,    p.sort_order FROM " . DB_PREFIX . "question p LEFT JOIN " . DB_PREFIX . "question_description pd ON (p.question_id = pd.question_id) LEFT JOIN " . DB_PREFIX . "question_to_store p2s ON (p.question_id = p2s.question_id) WHERE p.question_id = '" . (int)$question_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return array(
				'question_id'      => $query->row['question_id'],
				'title'            => $query->row['title'],
				'answer'      	   => $query->row['answer'],
				'status'           => $query->row['status'],
				'date_added'       => $query->row['date_added'],
				'date_modified'    => $query->row['date_modified'],
			);
		} else {
			return false;
		}
	}

	public function getQuestions($data = array()) {
		$sql = "SELECT p.question_id ";

		
		if (!empty($data['filter_faq_category_id'])) {
			if (!empty($data['filter_sub_faq_category'])) {
				$sql .= " FROM " . DB_PREFIX . "faq_category_path cp LEFT JOIN " . DB_PREFIX . "question_to_faq_category p2c ON (cp.faq_category_id = p2c.faq_category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "question_to_faq_category p2c";
			}

			
			$sql .= " LEFT JOIN " . DB_PREFIX . "question p ON (p2c.question_id = p.question_id)";
			
		} else {
			$sql .= " FROM " . DB_PREFIX . "question p";
		}
		

		$sql .= " LEFT JOIN " . DB_PREFIX . "question_description pd ON (p.question_id = pd.question_id) LEFT JOIN " . DB_PREFIX . "question_to_store p2s ON (p.question_id = p2s.question_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1'  AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

		if (!empty($data['filter_faq_category_id'])) {
			if (!empty($data['filter_sub_faq_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_faq_category_id'] . "'";
			} else {
				$sql .= " AND p2c.faq_category_id = '" . (int)$data['filter_faq_category_id'] . "'";
			}
			

		}
		


		$sql .= " GROUP BY p.question_id";

		$sort_data = array(
			'pd.title',
			'p.sort_order',
			'p.date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			if ($data['sort'] == 'pd.title') {
				$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
			} else {
				$sql .= " ORDER BY " . $data['sort'];
			}
		} else {
			$sql .= " ORDER BY p.sort_order";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC, LCASE(pd.title) DESC";
		} else {
			$sql .= " ASC, LCASE(pd.title) ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$question_data = array();

		$query = $this->db->query($sql);

		foreach ($query->rows as $result) {
			$question_data[$result['question_id']] = $this->getQuestion($result['question_id']);
		}

		return $question_data;
	}


	public function getQuestionLayoutId($question_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "question_to_layout WHERE question_id = '" . (int)$question_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getFaqCategories($question_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "question_to_faq_category WHERE question_id = '" . (int)$question_id . "'");

		return $query->rows;
	}

	public function getTotalQuestions($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.question_id) AS total";

		if (!empty($data['filter_faq_category_id'])) {
			if (!empty($data['filter_sub_faq_category'])) {
				$sql .= " FROM " . DB_PREFIX . "faq_category_path cp LEFT JOIN " . DB_PREFIX . "question_to_faq_category p2c ON (cp.faq_category_id = p2c.faq_category_id)";
			} else {
				$sql .= " FROM " . DB_PREFIX . "question_to_faq_category p2c";
			}
			
			$sql .= " LEFT JOIN " . DB_PREFIX . "question p ON (p2c.question_id = p.question_id)";

		} else {
			$sql .= " FROM " . DB_PREFIX . "question p";
		}

		$sql .= " LEFT JOIN " . DB_PREFIX . "question_description pd ON (p.question_id = pd.question_id) LEFT JOIN " . DB_PREFIX . "question_to_store p2s ON (p.question_id = p2s.question_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1'  AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";

		if (!empty($data['filter_faq_category_id'])) {
			if (!empty($data['filter_sub_faq_category'])) {
				$sql .= " AND cp.path_id = '" . (int)$data['filter_faq_category_id'] . "'";
			} else {
				$sql .= " AND p2c.faq_category_id = '" . (int)$data['filter_faq_category_id'] . "'";
			}

		}


		$query = $this->db->query($sql);

		return $query->row['total'];
	}

}
