<?php

/************copyright**************/
/*                                 */
/*   site:  http://microdata.pro   */
/*   email: info@microdata.pro     */
/*                                 */
/************copyright**************/

class MicrodataPro {

private $data = array();

	public function opencart_version($d){
		
		$opencart_version = explode(".", VERSION);
		
		return $opencart_version[$d];
	}
	
	public function clear_array($data_array) {
		if(isset($data_array) and is_array($data_array)){
			foreach($data_array as $key => $value){
				$data_array[$key] = $this->clear($value);
			}
		}
		
		return $data_array; 
	}
	
	public function clear($text = '', $decode = false) {
		if(is_string($text)){
			if($decode) $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
			$text = str_replace("><","> <",$text);
			if($text) $text = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', ' ', strip_tags((string)$text));
			$text = str_replace(array(PHP_EOL, "\r\n", "\r", "\n", "\t", '  ', '    ', '    ', '&nbsp;'), ' ', $text);
			$text = str_replace('"', "'", $text);
			$text = str_replace("'", " ", $text);
			$text = str_replace("\\", " ", $text);
		}
		return $text;
	}
	
	public function license($key){ // :)
		$license = false; 
		
		if(isset($key) && !empty($key)){
			$key = substr($key, 0, -7);
			$decode_key = base64_decode(strrev($key));
			$key_array = explode("327450", $decode_key);
			if($key_array[0] == base64_encode($this->module_info('main_host')) && $key_array[1] == base64_encode($this->module_info('sale_link'))){
				$license = true;
			}
		}	
		  
		return $license;
	}
	
	public function request() {
		$url = "http://microdata.pro/index.php?route=sale/sale/request";	
		$prepare_data = array(
			'site' 	    => $_SERVER['HTTP_HOST'],
		);		
		if($curl = curl_init()) { //POST CURL
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $prepare_data);
			$request = curl_exec($curl);
			curl_close($curl);
			return $request;
		}
	}	
	
	public function getKey() {
		$url = "http://microdata.pro/index.php?route=sale/sale/getKey";	
		$prepare_data = array(
			'site' 	    => $_SERVER['HTTP_HOST'],
		);		
		if($curl = curl_init()) { //POST CURL
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $prepare_data);
			$register_number = curl_exec($curl);
			curl_close($curl);
			return $register_number;
		}
	}	
	
	public function sendToServer($send_data) {
		$url = "http://microdata.pro/index.php?route=sale/sale";
		//$xdomain =  parse_url($_SERVER['SCRIPT_URI']);		
		$prepare_data = array(
			'email'     => $send_data['email'],
			'module'    => $this->module_info('module') . " " . $this->module_info('version'),
			'nikname'   => $send_data['nikname'],
			'site' 	    => $_SERVER['HTTP_HOST'],
			'shop_link' => $this->module_info('sale_link'),
			'sec_token' => "3274507573",
			'method'	=> 'POST',
			'lang'		=> $send_data['lang'],
			'engine'	=> $this->module_info('engine'),
			'date'		=> date("Y-m-d H:i:s")
		);		
		if($curl = curl_init()) { //POST CURL
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $prepare_data);
			$register_number = curl_exec($curl);
			curl_close($curl);
			return $register_number;
		}else{ //GET HTTP
			$url .= "&email=" . $prepare_data['email'];
			$url .= "&module=" . $prepare_data['module'];
			$url .= "&nikname=" . $prepare_data['nikname'];
			$url .= "&site=" . $prepare_data['site'];
			$url .= "&shop_link=" . $prepare_data['shop_link'];
			$url .= "&sec_token=" . $prepare_data['sec_token'];
			$url .= "&method=GET";
			$url .= "&lang=" . $prepare_data['lang'];
			$url .= "&engine=" . $prepare_data['engine'];
			$url .= "&date=" . $prepare_data['date'];
			return header("Location: " . $url);
		}
	}

	public function module_info($key){
		//$xdomain =  parse_url($_SERVER['HTTP_HOST']);
			$information = array(
			'host'		=> $_SERVER['HTTP_HOST'],
			'main_host'	=> $_SERVER['HTTP_HOST'],
			'engine' 	=> VERSION,
			'version' 	=> '3.1',
			'module' 	=> 'MicrodataPro',
			'sale_link' => 'https://opencartforum.com/files/file/2859-микроразметка-applicationldjson-pro/',
			'sys_key'	=> '327450'
		);

		return $information[$key];
	}
	
}
?>