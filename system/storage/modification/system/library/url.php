<?php
class Url {
	private $domain;
	private $ssl;
	private $rewrite = array();

	public function __construct($domain, $ssl = '') {
		$this->domain = $domain;
		$this->ssl = $ssl;
	}

	public function addRewrite($rewrite) {
		$this->rewrite[] = $rewrite;
	}


	public function nofollow($content, $server = null, $secure = false) {
		if (!$secure) {
			$server = $this->domain;
		} else {
			$server = $this->ssl;
		}

		return preg_replace_callback(
			"#(<a[^>]+?)>#is", function ($mach) use ($server) {
				return (
					!($server && strpos($mach[1], $server) !== false) && strpos($mach[1], 'rel=') === false
				) ? $mach[1] . ' rel="nofollow">' : $mach[0];
			}, $content
		);
	}
	public function link($route, $args = '', $secure = false) {
		if (!$secure) {
			$url = $this->domain;
		} else {
			$url = $this->ssl;
		}

		$url .= 'index.php?route=' . $route;

		if ($args) {
			if (is_array($args)) {
				$url .= '&amp;' . http_build_query($args);
			} else {
				$url .= str_replace('&', '&amp;', '&' . ltrim($args, '&'));
			}
		}

		foreach ($this->rewrite as $rewrite) {
			$url = $rewrite->rewrite($url);
		}

		return $url;
	}
}
