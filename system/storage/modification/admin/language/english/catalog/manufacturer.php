<?php

			// BrandManager
				$_['txt_clear']       = 'Clear';
				$_['txt_filter']      = 'Filter';
				$_['text_undefined']  = '-- Undefined --';
			// BrandManager end
			
// Heading
$_['heading_title']      = 'Manufacturers';

// Text
$_['text_success']       = 'Success: You have modified manufacturers!';
$_['text_list']          = 'Manufacturer List';
$_['text_add']           = 'Add Manufacturer';
$_['text_edit']          = 'Edit Manufacturer';
$_['text_default']       = 'Default';
$_['text_percent']       = 'Percentage';
$_['text_amount']        = 'Fixed Amount';

// Column
$_['column_name']        = 'Manufacturer Name';
$_['column_sort_order']  = 'Sort Order';
$_['column_action']      = 'Action';
$_['column_status']      = 'Status';//coc

// Entry
$_['entry_name']         = 'Manufacturer Name';
$_['entry_store']        = 'Stores';
$_['entry_keyword']      = 'SEO Keyword';
$_['entry_image']        = 'Image';
$_['entry_sort_order']   = 'Sort Order';
$_['entry_type']         = 'Type';
$_['entry_status']       = 'Status';//coc

// Help
$_['help_keyword']       = 'Do not use spaces, instead replace spaces with - and make sure the keyword is globally unique.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify manufacturers!';
$_['error_name']         = 'Manufacturer Name must be between 2 and 64 characters!';
$_['error_keyword']      = 'SEO keyword already in use!';
$_['error_product']      = 'Warning: This manufacturer cannot be deleted as it is currently assigned to %s products!';