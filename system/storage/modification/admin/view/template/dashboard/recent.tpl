<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-shopping-cart"></i> <?php echo $heading_title; ?></h3>
  </div>
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <td class="text-right"><?php echo $column_order_id; ?></td>
          <td><?php echo $column_customer; ?></td>
 
			 <td>Order Details</td>
				
          <td><?php echo $column_status; ?></td>
          <td><?php echo $column_date_added; ?></td>
          <td class="text-right"><?php echo $column_total; ?></td>
          <td class="text-right"><?php echo $column_action; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php if ($orders) { ?>
        <?php foreach ($orders as $order) { ?>
        <tr>
          <td class="text-right"><?php echo $order['order_id']; ?></td>
          <td><?php echo $order['customer']; ?></td>
 
		
			 
		    <td class="text-left">

 <span class=" previeworder" data-toggle="popover" data-placement="top" data-trigger="hover" data-html="true" title="<center>Viewing <b><?php echo count($order['products']);?></b> Products in Order# <?php echo $order['order_id']; ?></center>"
			data-content='
			
			
	 <table class="table">
      <thead>
            <tr>
              <td class="center" style="width:126px;">Image</td>
              <td class="left"><?php echo $column_product; ?></td>
              <td class="left"><?php echo $column_model; ?></td>
              <td class="right" style="text-align:right;"><?php echo $column_quantity; ?></td>
           
              
            </tr>
          
  <br>



<?php $int=0;$countit=1;foreach ($order['products'] as $product) { ?>
           
           <tr>
              <td style="text-align:center;"><img src="<?php echo $product['image'];?>"></td>
              <td class="left" style="font-weight:normal;font-size:12px;"> <span style="color: #1e91cf;"><span style="font-size:14px;"><?php echo $countit;?>.</span> <b><?php echo htmlentities($product['name'],ENT_QUOTES); ?></b></span>
                <?php foreach ($product['option'] as $option) { ?>
                <br />
                <?php if ($option['type'] != 'file') { ?>
                &nbsp;<span style="font-size:12px;"> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></span>
                <?php } else { ?>
                &nbsp;<span style="font-size:12px;"> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></span>
                <?php } ?>
                <?php } ?></td>
              <td class="left" style="font-weight:normal;font-size:12px;"><?php echo $product['model']; ?></td>
              <td style="text-align:right;">
            <span style="color: #1e91cf;font-weight:normal;"> <?php echo $product['quantity']; $int = $int + (int)$product['quantity']; ?></span></td>
             
            </tr>
           
            <?php $countit++; } ?>
            
            <tr><td style="text-align:left;font-size:12px;"><b style="color: #39b3d7;">PAYMENT METHOD</b></td><td></td><td></td><td style="text-align:right;"><b style="color: #39b3d7;font-size:12px;">TOTAL</b></td></tr>
            
             <tr><td style="text-align:left;font-size:12px;"><?php echo $order['payment']; ?></td><td></td><td></td><td style="text-align:right;font-size:12px;"><?php echo $int;?></td></tr>
            </table>
            '>   
            
<span style="color: #39b3d7;"><i class="fa fa-eye fa-lg"></i> <b><?php echo count($order['products']);?></b> product<?php if(count($order['products'])>1) echo 's';?> - <?php $int=0;foreach ($order['products'] as $product) {$int = $int + (int)$product['quantity']; ?>    <?php  } ?><b><?php echo $int;?></b> Total</span></span>
    
        <br>
 
<span style="color: #1e91cf;"><i class="fa fa-truck fa-lg"></i> <?php  echo $order['shipping']; ?></span>




  

<?php if (!empty($order['comment'])){ ?><br> <div style="text-align: justify;color: #f24545;max-width: 345px;line-height: 15px;"><i class="fa fa-comment fa-lg"></i> <?php echo $order['comment']; ?></div><?php } ?>


    
     
    </td>
				
          <td><?php echo $order['status']; ?></td>
          <td><?php echo $order['date_added']; ?></td>
          <td class="text-right"><?php echo $order['total']; ?></td>
          <td class="text-right"><a href="<?php echo $order['view']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a></td>
        </tr>
        <?php } ?>
        <?php } else { ?>
        <tr>
          <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
