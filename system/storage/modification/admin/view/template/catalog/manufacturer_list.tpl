<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      
<!-- BrandManager -->
      <div class="pull-right">
		<span style="padding-right:20px;">
		<a href="https://opencartforum.com/index.php?app=core&module=search&do=user_activity&search_app=downloads&mid=688391" target="_blank" data-toggle="tooltip" title="Другие дополнения" class="btn btn-info"><i class="fa fa-download"></i></a></span>
<!-- BrandManager -->
			<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-manufacturer').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1>
                  BrandManager
			</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">

<!-- BrandManager -->
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-id">ID</label>
                <input type="text" name="filter_id" value="<?php echo $filter_id; ?>" placeholder="ID" id="input-id" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-image"><?php echo $entry_image; ?></label>
                <select name="filter_image" id="input-image" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_image == 1) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <?php } ?>
                  <?php if ($filter_image == 2) { ?>
                  <option value="2" selected="selected"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="2"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-seo"><?php echo $entry_keyword; ?></label>
                <select name="filter_seo" id="input-seo" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_seo == 1) { ?>
                  <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_yes; ?></option>
                  <?php } ?>
                  <?php if ($filter_seo == 2) { ?>
                  <option value="2" selected="selected"><?php echo $text_no; ?></option>
                  <?php } else { ?>
                  <option value="2"><?php echo $text_no; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-4">
			  <div class="form-group">
				<label class="control-label" for="input-store"><?php echo $entry_store; ?></label>
                <select name="filter_store" id="input-store" class="form-control">
                  <option value="*"></option>

                  <?php if ($filter_store == '**') { ?>
                  <option value="**" selected="selected"><?php echo $text_undefined; ?></option>
                  <?php } else { ?>
                  <option value="**"><?php echo $text_undefined; ?></option>
                  <?php } ?>

                  <?php if ($filter_store == '0') { ?>
                  <option value="0" selected="selected"><?php echo $text_default; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_default; ?></option>
                  <?php } ?>

                  <?php foreach ($stores as $store) { ?>
                  <?php if ($store['store_id'] == $filter_store) { ?>
                  <option value="<?php echo $store['store_id']; ?>" selected="selected"><?php echo $store['name']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
			  <div class="form-group">
				<div class="pull-right">
					<button type="button" id="button-clear" class="btn btn-info"><i class="fa fa-eraser"></i> <?php echo $txt_clear; ?></button>
					<button type="button" id="button-filter" class="btn btn-primary"><i class="fa fa-search"></i>  <?php echo $txt_filter; ?></button>
				</div>
			  </div>
            </div>
          </div>
        </div>

		<div class="row" style="padding-bottom:10px;">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
<!-- BrandManager -->
			
        
        <div class="well">
          <div class="row">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_status && !is_null($filter_status)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-4">
              <br />
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>

<!-- BrandManager -->
              <td class="text-left" width="80px"><?php if ($sort == 'manufacturer_id') { ?>
                <a href="<?php echo $sort_id; ?>" class="<?php echo strtolower($order); ?>">ID</a>
                <?php } else { ?>
                <a href="<?php echo $sort_id; ?>">ID</a>
                <?php } ?></td>
			  <td class="text-center"><?php echo $entry_image ?></td>
<!-- BrandManager -->
			
                  <td class="text-left"><?php if ($sort == 'name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>

<!-- BrandManager -->
              <td class="text-left"><?php echo $entry_keyword ?></td>
              <td class="text-left"><?php echo $entry_store ?></td>
<!-- BrandManager -->
			
                  <td class="text-right"><?php if ($sort == 'sort_order') { ?>
                    <a href="<?php echo $sort_sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sort_order; ?>"><?php echo $column_sort_order; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($manufacturers) { ?>
                <?php foreach ($manufacturers as $manufacturer) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($manufacturer['manufacturer_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />
                    <?php } ?></td>

<!-- BrandManager -->
              <td class="text-left"><?php echo $manufacturer['manufacturer_id']; ?></td>
                  <td class="text-center"><?php if ($manufacturer['image']) { ?>
                    <img src="<?php echo $manufacturer['image']; ?>" alt="<?php echo $manufacturer['name']; ?>" class="img-thumbnail" />
                    <?php } else { ?>
                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                    <?php } ?></td>
<!-- BrandManager -->
			
                  <td class="text-left"><?php echo $manufacturer['name']; ?></td>
                  <td class="text-left"><?php echo $manufacturer['status']; ?></td>

<!-- BrandManager -->
              <td class="text-left"><?php echo $manufacturer['keyword']; ?></td>

              <td class="text-left">
                    <?php if (in_array(0, $manufacturer['stores'])) { ?>
                    <?php echo '> '.$text_default; ?></br>
                    <?php } ?>

                <?php foreach ($stores as $store) { ?>
                    <?php if (in_array($store['store_id'], $manufacturer['stores'])) { ?>
                    <?php echo '> '.$store['name']; ?></br>
                    <?php } ?>
                <?php } ?>
			  </td>
<!-- BrandManager -->
			
                  <td class="text-right"><?php echo $manufacturer['sort_order']; ?></td>
                  <td class="text-right"><a href="<?php echo $manufacturer['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="5"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/manufacturer&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

	location = url;
});
//--></script> 
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['manufacturer_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});
//--></script>
</div>

<!-- BrandManager -->
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=catalog/manufacturer&token=<?php echo $token; ?>';

	var filter_id = $('input[name=\'filter_id\']').val();
	
	if (filter_id) {
		url += '&filter_id=' + encodeURIComponent(filter_id);
	}

	var filter_store = $('select[name=\'filter_store\']').val();
	
	if (filter_store !='*') {
		url += '&filter_store=' + encodeURIComponent(filter_store);
	}

	var filter_seo = $('select[name=\'filter_seo\']').val();
	
	if (filter_seo !='*') {
		url += '&filter_seo=' + encodeURIComponent(filter_seo);
	}

	var filter_name = $('input[name=\'filter_name\']').val();
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_image = $('select[name=\'filter_image\']').val();
	
	if (filter_image !='*') {
		url += '&filter_image=' + encodeURIComponent(filter_image);
	}

	location = url;
}

function clear_filter() {
	$('select[class=\'form-control\']').val('*');
	$('input[class=\'form-control\']').val('');
	filter();
	return false;
}
//--></script>  
<script type="text/javascript"><!--
$('input[class=\'form-control\']').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

$('#button-filter').on('click', function() {
		filter();
});

$('#button-clear').on('click', function() {
		clear_filter();
});
//--></script>
<!-- BrandManager -->
			
<?php echo $footer; ?>