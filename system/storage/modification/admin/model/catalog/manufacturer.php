<?php
class ModelCatalogManufacturer extends Model {
	public function addManufacturer($data) {
		$this->event->trigger('pre.admin.manufacturer.add', $data);

		$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$manufacturer_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		}

		if (isset($data['manufacturer_store'])) {
			foreach ($data['manufacturer_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");//modify coc
		}

		$this->cache->delete('manufacturer');

		$this->event->trigger('post.admin.manufacturer.add', $manufacturer_id);

		return $manufacturer_id;
	}

	public function editManufacturer($manufacturer_id, $data) {
		$this->event->trigger('pre.admin.manufacturer.edit', $data);

		$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");//modify coc

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		if (isset($data['manufacturer_store'])) {
			foreach ($data['manufacturer_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

		if ($data['keyword']) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}

		$this->cache->delete('manufacturer');

		$this->event->trigger('post.admin.manufacturer.edit');
	}

	public function deleteManufacturer($manufacturer_id) {
		$this->event->trigger('pre.admin.manufacturer.delete', $manufacturer_id);

		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "'");

		$this->cache->delete('manufacturer');

		$this->event->trigger('post.admin.manufacturer.delete', $manufacturer_id);
	}

	public function getManufacturer($manufacturer_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'manufacturer_id=" . (int)$manufacturer_id . "') AS keyword FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row;
	}

	public function getManufacturers($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "manufacturer WHERE manufacturer_id != 0";//modify coc

		if (!empty($data['filter_name'])) {
			$sql .= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";//modify coc
		}
		
		//coc
		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND status = '" . (int)$data['filter_status'] . "'";
		}

		$sort_data = array(
			'name',
			'status',//coc
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getManufacturerStores($manufacturer_id) {
		$manufacturer_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "manufacturer_to_store WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		foreach ($query->rows as $result) {
			$manufacturer_store_data[] = $result['store_id'];
		}

		return $manufacturer_store_data;
	}


	// BrandManager
	public function getManufacturersBM($data = array()) {
		if (isset($data['filter_seo'])) {
		$sql = "SELECT * FROM " . DB_PREFIX . "url_alias WHERE `keyword` != '' AND `query` LIKE 'manufacturer_id=%' ";
		$query = $this->db->query($sql);

			$seoman = array();
				foreach ($query->rows as $result) {
					$seoman[] = str_replace('manufacturer_id=', '', $result['query']);
				}
			$seoman = implode(',', $seoman);
		}

		$sql = "SELECT c.manufacturer_id, c.name, c.sort_order FROM " . DB_PREFIX . "manufacturer c WHERE 1 ";

		if (isset($data['filter_id'])) {
			$sql .= " AND c.manufacturer_id LIKE '%" . (int)$data['filter_id'] . "%'";
		}

		if (isset($data['filter_store'])) {
			if (($data['filter_store']) <> '**') {
				$sql .= " AND c.manufacturer_id IN (SELECT m2s.manufacturer_id FROM " . DB_PREFIX . "manufacturer_to_store m2s WHERE c.manufacturer_id=m2s.manufacturer_id AND m2s.store_id = '" . (int)$data['filter_store'] . "' ) ";
			} else {
				$sql .= " AND c.manufacturer_id NOT IN (SELECT m2s.manufacturer_id FROM " . DB_PREFIX . "manufacturer_to_store m2s GROUP BY m2s.manufacturer_id) ";
			}
		}

		if (isset($data['filter_seo'])) {
			if (($data['filter_seo']) == 1) {
				$sql .= " AND (c.manufacturer_id IN (".$seoman.")) ";
			} else {
				$sql .= " AND (c.manufacturer_id NOT IN (".$seoman.")) ";
			}
		}

		if (isset($data['filter_image']) ) {
			if ($data['filter_image'] == 1) {
				$sql .= " AND ((c.image) <> '' AND (c.image) <> 'no_image.png' AND (c.image) IS NOT NULL)";
			} else {
				$sql .= " AND ((c.image) = '' OR (c.image) = 'no_image.png' OR (c.image) IS NULL)";
			}
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(c.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		$sort_data = array(
			'manufacturer_id',
			'name',
			'sort_order'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

		public function getTotalBrandManager($data = array()) {

		if (isset($data['filter_seo'])) {
		$sql = "SELECT * FROM " . DB_PREFIX . "url_alias WHERE `keyword` != '' AND `query` LIKE 'manufacturer_id=%' ";
		$query = $this->db->query($sql);

			$seoman = array();
				foreach ($query->rows as $result) {
					$seoman[] = str_replace('manufacturer_id=', '', $result['query']);
				}
			$seoman = implode(',', $seoman);
		}
			$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "manufacturer WHERE 1 ";

		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (isset($data['filter_id'])) {
			$sql .= " AND manufacturer_id LIKE '%" . (int)$data['filter_id'] . "%'";
		}

		if (isset($data['filter_store'])) {
			if (($data['filter_store']) <> '**') {
				$sql .= " AND manufacturer_id IN (SELECT m2s.manufacturer_id FROM " . DB_PREFIX . "manufacturer_to_store m2s WHERE manufacturer_id=m2s.manufacturer_id AND m2s.store_id = '" . (int)$data['filter_store'] . "' ) ";
			} else {
				$sql .= " AND manufacturer_id NOT IN (SELECT m2s.manufacturer_id FROM " . DB_PREFIX . "manufacturer_to_store m2s GROUP BY m2s.manufacturer_id) ";
			}
		}

		if (isset($data['filter_seo'])) {
			if (($data['filter_seo']) == 1) {
				$sql .= " AND (manufacturer_id IN (".$seoman.")) ";
			} else {
				$sql .= " AND (manufacturer_id NOT IN (".$seoman.")) ";
			}
		}

		if (isset($data['filter_image']) ) {
			if ($data['filter_image'] == 1) {
				$sql .= " AND ((image) <> '' AND (image) <> 'no_image.png' AND (image) IS NOT NULL)";
			} else {
				$sql .= " AND ((image) = '' OR (image) = 'no_image.png' OR (image) IS NULL)";
			}
		}

			$query = $this->db->query($sql);
			return $query->row['total'];
		}
	// BrandManager end
			
	public function getTotalManufacturers() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "manufacturer");

		return $query->row['total'];
	}
}