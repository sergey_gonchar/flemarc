<?php
class ControllerModuleSpecial extends Controller {
	public function index($setting) {
		$this->load->language('module/special');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
$this->load->language('product/product');
				$this->load->language('theme');
				$data['text_quantity'] = $this->language->get('text_quantity');
				$data['text_scl'] = $this->language->get('text_scl');
		        $data['text_pcs'] = $this->language->get('text_pcs');
		        $data['text_out_of_stock'] = $this->language->get('text_out_of_stock');
                $data['text_filter'] = $this->language->get('text_filter');
				$data['text_viewed'] = $this->language->get('text_viewed');
				$data['text_clear_filter'] = $this->language->get('text_clear_filter');
                $data['text_tooltip_filter'] = $this->language->get('text_tooltip_filter');
                $data['text_tooltip_clear_filter'] = $this->language->get('text_tooltip_clear_filter');
				$data['text_tooltip_quick'] = $this->language->get('text_tooltip_quick');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		$filter_data = array(
			'sort'  => 'pd.name',
			'order' => 'ASC',
			'start' => 0,
			'limit' => $setting['limit']
		);

		$results = $this->model_catalog_product->getProductSpecials($filter_data);

		if ($results) {
			foreach ($results as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				
		 $options = $this->model_catalog_product->getProductOptions($result['product_id']);
		 $swapimages = $this->model_catalog_product->getProductImages($result['product_id']);
			    if ($swapimages) {
				    $swapimage = $this->model_tool_image->resize($swapimages[0]['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			    } else {
				    $swapimage = false;
			    }
				$data['products'][] = array(
				'thumb_swap'  => $swapimage,
				'options'     => $options,
				'quantity' => $result['quantity'],
				'stock'=> ($result['quantity']<=0) ? $result['stock_status'] : $this->language->get('text_instock'),
				'sale'        => $result['price'] == 0 ? 100 : round((($result['price'] - $result['special'])/$result['price'])*100, 0),
				
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/special.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/special.tpl', $data);
			} else {
				return $this->load->view('default/template/module/special.tpl', $data);
			}
		}
	}
}