<?php echo $header; ?>
<?php $config = $this->registry->get('config');?>
<?php $lang = (int)$config->get('config_language_id');?>
<!-- START STYLES -->
<style>

    .row.control+.row.control {
        margin-top: 10px;
    }
    .control {
        margin: 0;
        padding: 0;
        clear: both;
        font-size: 100%;
    }
    .control .btn-cart {
        min-width: 100% !important;
        margin-top: 10px;
    }
    .input-group--quantity {
        width: 100%;
    }
    .input-group--quantity .quantity__btn{
        width: 25%;
        cursor: pointer;
    }
    .input-group--quantity .quantity__counter {
        width: 100% !important;
    }

    /* Common button styles */
    .button {
        box-sizing: border-box ;
        display: block;
        vertical-align: middle;
        position: relative;
        z-index: 1;
        border: 1px solid #3f51b5 !important;
    }
    .button:focus {
        outline: none;
    }

    .button > span {
        vertical-align: middle;
    }

    /* Individual button styles */

    /* Winona */
    .button--winona {
        overflow: hidden;
        padding: 0;
        -webkit-transition: border-color 0.3s, background-color 0.3s;
        transition: border-color 0.3s, background-color 0.3s;
        -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
    }
    .button--winona::after {
        content: attr(data-text);
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        opacity: 0;
        color: #3f51b5;
        -webkit-transform: translate3d(0, 25%, 0);
        transform: translate3d(0, 25%, 0);
    }
    .button--winona > span {
        display: block;
    }
    .button--winona.button--inverted {
        color: #7986cb;
    }
    .button--winona.button--inverted:after {
        color: #fff;
    }
    .button--winona::after,
    .button--winona > span {
        padding: 1em 2em;
        -webkit-transition: -webkit-transform 0.3s, opacity 0.3s;
        transition: transform 0.3s, opacity 0.3s;
        -webkit-transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
        transition-timing-function: cubic-bezier(0.2, 1, 0.3, 1);
    }
    .button--winona:hover {
        border-color: #3f51b5;
        background-color: rgba(63, 81, 181, 0.1);
    }
    .button--winona.button--inverted:hover {
        border-color: #21333C;
        background-color: #21333C;
    }
    .button--winona:hover::after {
        opacity: 1;
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }
    .button--winona:hover > span {
        opacity: 0;
        -webkit-transform: translate3d(0, -25%, 0);
        transform: translate3d(0, -25%, 0);
    }
</style>
<!-- END STYLES -->

<div class="container">
    <ul class="breadcrumb">
        <?php $breadlast = array_pop($breadcrumbs); foreach ($breadcrumbs as $breadcrumb) { ?>
        <li data-t32ypeof_mdp="mdp_v:Breadcrumb"><a href="<?php echo $breadcrumb['href']; ?>" data-r32el_mdp="mdp_v:url" data-p32roperty_mdp="mdp_v:title"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
        <li data-t32ypeof_mdp="mdp_v:Breadcrumb"><span data-p32roperty_mdp="mdp_v:title"><?php echo $breadlast['text']; ?></span></li>
    </ul>
    <div class="row">
        <?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <?php if (!empty($prevProduct)) { ?>
        <a href="<?php echo $prevProduct['href']; ?>">
            <div class="prevnext prev-product" data-toggle="popover" data-container="body" data-placement="right" data-content="<?php echo $prevProduct['name'];?>"><img src="<?php echo $prevProduct['image']; ?>" alt="<?php echo $prevProduct['name'];?>"></div>
        </a>
        <?php } ?>
        <?php if (!empty($nextProduct)) { ?>
        <a href="<?php echo $nextProduct['href']; ?>">
            <div class="prevnext next-product" data-toggle="popover" data-container="body" data-placement="left" data-content="<?php echo $nextProduct['name'];?>"><img src="<?php echo $nextProduct['image']; ?>" alt="<?php echo $nextProduct['name'];?>"></div>
        </a>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>" data-i32temscope_mdp data-i32temtype_mdp="http://schema.org/Product">
            <?php echo $content_top; ?>
            <div class="row">
                <?php if ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } else { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?><?php if($config->get('control_zoom_image')== 1) { ?> zoom-image<?php } ?>">
                    <div class="zoom">+</div>
                    <div id="one-image" class="owl-carousel">
                        <div class="item">
                            <img src="<?php echo $thumb; ?>" data-zoom-image="<?php echo $popup; ?>" id="image"  title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" data-i32temprop_mdp="image" />
                            <a class="zoom2 thumbnails" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"></a>
                        </div>
                        <?php if ($images) { ?>
                        <?php foreach ($images as $image) { ?>
                        <div class="item">
                            <img src="<?php echo $image['fix']; ?>" data-zoom-image="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
                            <a class="zoom2 thumbnails" href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>"></a>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <script type="text/javascript">
                        $( ".zoom" ).on( "click",function( ){
                        $( ".active .zoom2" ).click( );
                        } );
                    </script>
                    <?php if ($images && $config->get('control_additional')== 1) { ?>
                    <div id="image-additional" class="owl-carousel image-additional">
                        <div class="item"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>"><img src="<?php echo $fix2; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>
                        <?php foreach ($images as $image) { ?>
                        <div class="item">
                            <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>">
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <script type="text/javascript" src="//yastatic.net/share/share.js" charset="utf-8"></script>

                    <!-- // START HACK KERCH -->
                    <div class="tags"><b><?php echo $text_tags; ?></b>
                        <?php foreach ($catprod as $catp) { ?> <a href="<?php echo $catp['href']; ?>"><?php echo $catp['name']; ?></a>,<?php } ?><br />
                    </div>

                    <?php if ($tags) { ?>
                    <p><?php echo $text_tags; ?>
                        <?php for ($i = 0; $i < count($tags); $i++) { ?>
                        <?php if ($i < (count($tags) - 1)) { ?>
                        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                        <?php } else { ?>
                        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                        <?php } ?>
                        <?php } ?>
                    </p>
                    <?php } ?>
                    <!-- // END HACK KERCH -->

                    <script>
                        // product images owl
                        var a = $( "#one-image" );
                        var b = $( "#image-additional" );
                        a.owlCarousel( {
                        singleItem: true,
                                slideSpeed: 1000,
                                navigation: true,
                                navigationText: ['<i class="fa fa-angle-left fa-5x"></i>','<i class="fa fa-angle-right fa-5x"></i>'],
                                pagination: false,
                                afterAction: syncPosition,
                                responsiveRefreshRate: 200,
                                addClassActive: true,
                        } );
                        b.owlCarousel( {
                        items: 6,
                                itemsDesktop: [1199,6],
                                itemsDesktopSmall: [979,5],
                                itemsTablet: [768,4],
                                itemsMobile: [479,3],
                                pagination: false,
                                navigation: true,
                                navigationText: ['<i class="fa fa-angle-left fa-5x"></i>','<i class="fa fa-angle-right fa-5x"></i>'],
                                responsiveRefreshRate: 100,
                                afterInit: function( el ) {
                                el.find( ".owl-item" ).eq( 0 ).addClass( "synced" );
                                }
                        } );
                        function syncPosition( el ) {
                        var current = this.currentItem;
                        $( "#image-additional" )
                                .find( ".owl-item" )
                                .removeClass( "synced" )
                                .eq( current )
                                .addClass( "synced" )
                                if( $( "#image-additional" ).data( "owlCarousel" )
                                        !== undefined ) {
                        center( current )
                        }
                        }

                        $( "#image-additional" ).on( "click",".owl-item",function( e ) {
                        e.preventDefault( );
                        var number = $( this ).data( "owlItem" );
                        a.trigger( "owl.goTo",number );
                        } );
                        function center( number ) {
                        var bvisible =
                                b.data( "owlCarousel" ).owl.visibleItems;
                        var num = number;
                        var found = false;
                        for( var i in bvisible ) {
                        if( num
                                === bvisible[i] ) {
                        var found = true;
                        }
                        }

                        if( found
                                === false ) {
                        if( num
                                > bvisible[bvisible.length
                                        - 1] ) {
                        b.trigger( "owl.goTo",num
                                - bvisible.length
                                + 2 )
                        }
                        else {
                        if( num
                                - 1
                                ===
                                - 1 ) {
                        num = 0;
                        }
                        b.trigger( "owl.goTo",num );
                        }
                        }
                        else if( num
                                === bvisible[bvisible.length
                                        - 1] ) {
                        b.trigger( "owl.goTo",bvisible[1] )
                        }
                        else if( num
                                === bvisible[0] ) {
                        b.trigger( "owl.goTo",num
                                - 1 )
                        }
                        }
                    </script>
                    <!--tab-->
                </div>
                <?php if ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } else { ?>
                <?php $class = 'col-sm-6'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?> product-info">
                    <div class="buttons">
                        <div class="pull-left info-left col-sm-7 nopad">
                            <h1 data-i32temprop_mdp="name"><?php echo $heading_title; ?></h1>
                            <ul class="list-unstyled">
                                <?php if ($manufacturer) { ?>
                                <li><img src="<?php echo $manufacturerimg; ?>"></li>
                                <li><?php echo $text_manufacturer; ?> <a data-i32temprop_mdp="manufacturer" href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></li>
                                <?php } ?>
                                <li><?php echo $text_model; ?> <span data-i32temprop_mdp="model"><?php echo $model; ?></span></li>

                                <li><?php echo $text_weight; ?> <?php echo $weight . ' ' . $weightunit; ?></li>

                                <?php if ($reward) { ?>
                                <li><?php echo $text_reward; ?> <?php echo $reward; ?></li>
                                <?php } ?>
                                <li><?php echo $text_stock; ?> <?php echo $stock; ?></li>
                            </ul>
                            <?php if ($econom) { ?><div class="econom"><?php echo $text_econom; ?> <?php echo $econom; ?></div><?php } ?>
                            <?php if ($jan) { ?><div class="econom"><?php echo $jan; ?></div><?php } ?>
                            <?php if ($isbn) { ?><div class="econom"><?php echo $isbn; ?></div><?php } ?>
                            <?php if ($mpn) { ?><div class="econom"><?php echo $mpn; ?></div><?php } ?>
                        </div>
                        <?php if ($price) { ?>
                        <span data-i32temprop_mdp = "offers" data-i32temscope_mdp data-i32temtype_mdp = "http://schema.org/Offer">
                            <meta data-i32temprop_mdp="price" content="<?php echo rtrim(preg_replace("/[^0-9\.]/", "", ($special ? $special : $price)), '.'); ?>" />
                                  <meta data-i32temprop_mdp="priceCurrency" content="<?php echo $priceCurrency; ?>" />
                            <meta data-i32temprop_mdp="availability" content="<?php echo strip_tags($stock); ?>" />
                        </span>
                        <div class="pull-right info-right col-sm-5 nopad">
                            <ul class="list-unstyled">
                                <?php if (!$special) { ?>
                                <li>
                                    <h2><span id="formated_price" price="<?php echo $price_value; ?>"><?php echo $price; ?></span></h2>
                                    <?php if($config->get('control_minprice')== 1) { ?><?php echo $minprice; ?><?php } ?>
                                </li>
                                <?php } else { ?>
                                <li><span class="through"><span id="formated_price" price="<?php echo $price_value; ?>"><?php echo $price; ?></span></span></li>
                                <li>
                                    <h2><span id="formated_special" price="<?php echo $special_value; ?>"><?php echo $special; /**/ ?></span></h2>
                                    <?php if($config->get('control_minprice')== 1) {?><?php echo $minprice; ?><?php } ?>
                                </li>
                                <?php } ?>
                                <?php if ($tax) { ?>
                                <li><?php echo $text_tax; ?> <span id="formated_tax" price="<?php echo $tax_value; ?>"><?php echo $tax; ?></span></li>
                                <?php } ?>
                                <?php if ($points) { ?>
                                <li><?php echo $text_points; ?> <span id="formated_points" points="<?php echo $points; /**/ ?>"><?php echo $points; /**/ ?></span></li>
                                <?php } ?>
                                <?php if ($discounts) { ?>
                                <li>
                                    <hr>
                                </li>
                                <?php foreach ($discounts as $discount) { ?>
                                <li><?php echo $discount['quantity']; ?><?php echo $text_discount; ?><?php echo $discount['price']; ?></li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                            <?php if ($review_status && $config->get('control_review')== 1) { ?>
                            <div class="rating">
                                <p>
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <?php if ($rating < $i) { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                    <?php } else { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                    <?php } ?>
                                    <?php } ?>
                                    <span class="hrefr"><a class="scrollto">(<?php echo rtrim(preg_replace("/[^0-9\.]/", "", ($reviews)), '.'); ?>)</a><br><a class="scrollto"><?php echo $text_write; ?></a></span>
                                    <?php if($rating) { ?>
                                    <span data-i32temprop_mdp="aggregateRating" data-i32temscope_mdp data-i32temtype_mdp="http://schema.org/AggregateRating">
                                        <meta data-i32temprop_mdp="ratingValue" content="<?php echo $rating; ?>" />
                                        <meta data-i32temprop_mdp="reviewCount" content="<?php echo rtrim(preg_replace("/[^0-9\.]/", "", ($reviews)), '.'); ?>" />
                                              <meta data-i32temprop_mdp="bestRating" content="5" />
                                        <meta data-i32temprop_mdp="worstRating" content="1" />
                                    </span>
                                    <?php } ?>
                                </p>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div id="product">
                        <hr class="tophr">
                        <?php if($config->get('control_description')== 1) { ?>
                        <div class="tab-content">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                                <?php if ($attribute_groups) { ?>
                                <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                                <?php } ?>
                                <?php if($config->get('control_custom_tab_status')== 1) {
                                $control_title_tab = $config->get('control_title_tab');
                                $control_content_tab = $config->get('control_content_tab');
                                if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
                                <li><a href="#tab-custom" data-toggle="tab"><?php echo $control_title_tab[$lang]; ?></a></li>
                                <?php } ?>
                                <?php } ?>
                                <?php if($config->get('control_custom_tab4_status')== 1) {
                                $control_title_tab4 = $config->get('control_title_tab4');
                                $control_content_tab4 = $config->get('control_content_tab4');
                                if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                                <li><a href="#tab-custom4" data-toggle="tab"><?php echo $control_title_tab4[$lang]; ?></a></li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                            <div class="tab-pane active" id="tab-description" data-i32temprop_mdp="description"><?php echo $description; ?></div>
                            <div class="tab-pane tab-content" id="tab-documentation">
                                <?php if ($downloads){ ?>
                                <ul style="list-style:none;">
                                    <?php foreach($downloads as $download){ ?>
                                    <li><i class="<?php echo $download['icon']; ?>"></i> <a href="<?php echo $download['href']; ?>" title="<?php echo $download['name']; ?>" <?php echo (!$download['size'])?'target="_blank"':''; ?> ><?php echo $download['name']; ?><?php echo ($download['size'])?" (". $download['size'] .")":'';?></a></li>
                                    <?php } ?>
                                </ul>
                                <?php } ?>
                            </div>
                            <?php if ($attribute_groups) { ?>
                            <div class="tab-pane" id="tab-specification">
                                <table class="table table-bordered">
                                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <thead>
                                        <tr>
                                            <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                        <tr>
                                            <td><?php echo $attribute['name']; ?></td>
                                            <td><?php echo $attribute['text']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <?php } ?>
                                </table>
                            </div>
                            <?php } ?>
                            <?php if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
                            <div class="tab-pane" id="tab-custom"><?php echo html_entity_decode($control_content_tab[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
                            <?php } ?>
                            <?php if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                            <div class="tab-pane" id="tab-custom4"><?php echo html_entity_decode($control_content_tab4[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
                            <?php } ?>
                        </div>
                        <hr>
                        <?php } ?>
                        <?php if ($options) { ?>
                        <h3><?php echo $text_option; ?></h3>
                        <?php foreach ($options as $option) { ?>
                        <?php if ($option['type'] == 'select') { ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control customselect">
                                <option value=""><?php echo $text_select; ?></option>
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                <option value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>"><?php echo $option_value['name']; ?>
                                    <?php if ($option_value['price']) { ?>
                                    <?php
                                    if ($option_value['price_prefix'] == '*') {
                                    if ($option_value['price_value'] != 1.0)
                                    printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
                                    } else {
                                    echo "(".$option_value['price_prefix'].$option_value['price'].")";
                                    }
                                    ?>
                                    <?php } ?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php } ?>
                        <?php if ($option['type'] == 'radio') { ?>
                        <?php if($config->get('control_outstock')== 1) {?><?php echo $outstock; ?><?php } ?>
                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                            <label class="control-label"><?php echo $option['name']; ?></label>
                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                <?php if ($option_value['subtract'] && !$option_value['quantity'])  { ?>
                                <?php if($config->get('control_outstock')== 1) {?>
                  <div class="radio" data-value="<?php echo $option['name']; ?>: <?php echo $option_value['name']; ?>">
                     <div data-toggle="tooltip" title="<?php echo $text_outstock; ?>">
                        <div class="panel-url outstock_canvas" data-action="order" data-spanel="#outstock"></div>
                     </div>
                     <?php } else { ?>
                                <div class="radio">
                                    <?php } ?>
                                    <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" disabled="disabled" id="<?php echo $option_value['product_option_value_id']; ?>" />
                                    <?php } else { ?>
                                    <div class="radio">
                                        <input  type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" />
                                        <?php } ?>
                                        <label data-toggle="tooltip" title="<?php if ($option_value['subtract']) { if ($option_value['quantity'] > 0) echo ''.$text_scl.' '.$option_value['quantity'].' '.$text_pcs; else echo ' '.$text_out_of_stock.' ';} ?>" for="<?php echo $option_value['product_option_value_id']; ?>">
                                            <?php echo $option_value['name']; ?>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php $control_size_title = $config->get('control_size_title'); if ($option['name'] ==  $control_size_title[$lang] ) { ?>
                                <?php echo $size; ?>
                                <?php } ?>
                            </div>
                            <script>
                                $( function ( ) {
                                var value_name =
                                        $( document.order.value_name );
                                $( document ).on( 'click','[data-action="order"]',function ( e ) {

                                e.preventDefault( );
                                var set = value_name.val( ),
                                        value =
                                        $( this ).closest( '[data-value]' ).data( 'value' ).toString( );
                                value_name.val( value );
                                $( '#outoption' ).text( $( ".text-option" ).val( ) );
                                } );
                                } );
                            </script>
                            <?php } ?>
                            <?php if ($option['type'] == 'checkbox') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" class="option-input"/>
                                            <?php echo $option_value['name']; ?>
                                            <?php if ($option_value['price']) { ?>
                                            <?php
                                            if ($option_value['price_prefix'] == '*') {
                                            if ($option_value['price_value'] != 1.0)
                                            printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
                                            } else {
                                            echo "(".$option_value['price_prefix'].$option_value['price'].")";
                                            }
                                            ?>
                                            <?php } ?>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'image') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?> radioimg">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                    <div class="radio">
                                        <label for="<?php echo $option_value['product_option_value_id']; ?>">
                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"  points="<?php echo (isset($option_value['points_value']) ? $option_value['points_value'] : 0); ?>" price_prefix="<?php echo $option_value['price_prefix']; ?>" price="<?php echo $option_value['price_value']; ?>" id="<?php echo $option_value['product_option_value_id']; ?>" />
                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                                            <div class="labeldiv">
                                                <?php echo $option_value['name']; ?>
                                                <?php if ($option_value['price']) { ?>
                                                <?php
                                                if ($option_value['price_prefix'] == '*') {
                                                if ($option_value['price_value'] != 1.0)
                                                printf("(%+d%%)", round(($option_value['price_value'] * 100) - 100) );
                                                } else {
                                                echo "(".$option_value['price_prefix'].$option_value['price'].")";
                                                }
                                                ?>
                                                <?php } ?>
                                            </div>
                                        </label>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'text') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'textarea') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'file') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label"><?php echo $option['name']; ?></label>
                                <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'date') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group date">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="MM-DD-YYYY" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'datetime') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group datetime">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="MM-DD-YYYY HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($option['type'] == 'time') { ?>
                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                <div class="input-group time">
                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                            <?php } ?>
                            <?php } ?>
                            <?php } ?>
                            <?php if(isset($size_description) && $config->get('size_status')== 1) {?>
                  <a href="#test-modal" class="size-table">
                  <i class="fa fa-calendar" style="margin-right: 6px;color: #6D6D6D;"></i><?php echo $text_size; ?>
                  </a>
                  <div id="test-modal" class="white-popup-block mfp-hide">
                     <?php echo html_entity_decode($size_description, ENT_QUOTES, 'UTF-8'); ?>
                  </div>
                  <?php } ?>
                            <?php if($config->get('variant_status')== 1) {?>
				  <?php if($variants) {?>
                  <div class="variant">
                     <h3><?php echo $variant_title; ?></h3>
                     <?php foreach($variants as $product){ ?>
                     <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
                     <?php } ?>
                  </div>
                  <?php } ?>
                            <?php } ?>
                            <?php if ($recurrings) { ?>
                            <hr>
                            <h3><?php echo $text_payment_recurring ?></h3>
                            <div class="form-group required">
                                <select name="recurring_id" class="form-control">
                                    <option value=""><?php echo $text_select; ?></option>
                                    <?php foreach ($recurrings as $recurring) { ?>
                                    <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                    <?php } ?>
                                </select>
                                <div class="help-block" id="recurring-description"></div>
                            </div>
                            <?php } ?>

                            <!-- START CONTROLS -->

                            <!-- START QUANTITY -->
                            <?php if($config->get('control_quantity')== 1) : ?>
                            <div class="row control"> <!-- quantity -->
                                <div class="input-group input-group--quantity">
                                    <!-- PLUS BUTTON -->
                                    <span class="input-group-addon quantity__btn plus"
                                          id="sizing-addon2" data-action="+">
                                        <a class="plus" href="javascript:void(0);">+</a>
                                    </span>

                                    <!-- MANUAL INPUT AND COUNTER -->
                                    <input data-toggle="tooltip"
                                           title="<?php echo $entry_qty; ?>"
                                           type="text"
                                           name="quantity"
                                           value="<?php echo $minimum; ?>"
                                           id="input-quantity"
                                           class="form-control quantity__counter" />

                                    <!-- MINUS BUTTON -->
                                    <span class="input-group-addon quantity__btn minus"
                                          id="sizing-addon2"
                                          data-action="-">
                                        <a class="minus" href="javascript:void(0);">-</a>
                                    </span>


                                </div>
                            </div>
                            <?php endif; ?>

                            <script>
                                /**
                                 * increaseProductNumber
                                 */
                                var increaseProductNumber = function( ) {
                                console.log( 'Increase Product Number' );
                                $( '.plus' ).trigger( 'click' );
                                };
                                /**
                                 * reduceProductNumber
                                 */
                                var reduceProductNumber = function( ) {
                                console.log( 'Reduce Product Number' );
                                $( '.minus' ).trigger( 'click' );
                                };
                                /**
                                 * Quantity buttons Event Listener
                                 */
                                $( '.input-group--quantity .quantity__btn' ).click( function( event ){
                                event.stopPropagation( );
                                event.preventDefault( );
                                var $pressedButton = $( event.target ),
                                        action =
                                        $pressedButton.data( 'action' );
                                // Check plus or minus
                                switch( action ) {
                                case '+':
                                        // If plus increaseProductNumber
                                        increaseProductNumber( );
                                break;
                                case '-':
                                        // Else If minus reduceProductNumber
                                        reduceProductNumber( );
                                break;
                                default:
                                        //
                                        // If requested action not defined in switch|case
                                        console.log( 'Action: '
                                                + action
                                                + ' not defined' );
                                break;
                                };
                                return false;
                                } );
                            </script>
                            <!-- END QUANTITY -->

                            <!-- START ADD TO CART -->
                            <div class="row control">
                                <button type="button"
                                        id="button-cart"
                                        data-loading-text="<?php echo $text_loading; ?>"
                                        data-text="<?php echo $button_cart; ?>"
                                        class="btn btn-primary btn-cart button button--winona">
                                    <span><?php echo $button_cart; ?></span>

                                </button>
                            </div>
                            <!-- END ADD TO CART -->

                            <!-- START SOCIAL, COMPARE AND WISHLIST BUTTONS -->
                            <div class="row control">
                                <!-- START SOCIAL WIDGET -->
                                <div class="yashare-auto-init pull-left"
                                     data-yashareL10n="ru"
                                     data-yashareType="small"
                                     data-yashareQuickServices="vkontakte,odnoklassniki,twitter,facebook"
                                     data-yashareTheme="counter">
                                    &nbsp;
                                </div>
                                <!-- END SOCIAL WIDGET -->

                                <!-- START PRODUCT ACTIONS -->

                                <!-- WISHLIST BUTTON -->
                                <button type="button"
                                        data-toggle="tooltip"
                                        class="no-btn btn-rad pull-right"
                                        title="<?php echo $button_wishlist; ?>"
                                        onclick="wishlist.add( '<?php echo $product_id; ?>' );">
                                    <i class="fa fa-heart"></i>
                                </button>

                                <!-- COMPARE BUTTON -->
                                <?php if($config->get('control_compare')== 1) : ?>
                                <button type="button"
                                        data-toggle="tooltip"
                                        class="no-btn btn-rad pull-right"
                                        title="<?php echo $button_compare; ?>"
                                        onclick="compare.add( '<?php echo $product_id; ?>' );">
                                    <i class="fa fa-exchange"></i>
                                </button>
                                <?php endif; ?>

                                <!-- END PRODUCT ACTIONS -->
                            </div>
                            <!-- END SOCIAL, COMPARE AND WISHLIST BUTTONS -->

                            <!-- START DATA INPUTS -->
                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                            <!-- END DATA INPUTS -->

                            <!-- START RATING -->
                            <?php if ($review_status && $config->get('control_review')== 2) : ?>
                            <div class="row control">
                                <div class="rating">
                                    <p>
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <?php if ($rating < $i) { ?>
                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <?php } else { ?>
                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                        <?php } ?>
                                        <?php } ?>
                                        <span class="hrefr"><a class="scrollto"><?php echo $reviews; ?></a> / <a class="scrollto"><?php echo $text_write; ?></a></span>
                                        <?php if($rating) { ?>
                                        <span data-i32temprop_mdp="aggregateRating" data-i32temscope_mdp data-i32temtype_mdp="http://schema.org/AggregateRating">
                                            <meta data-i32temprop_mdp="ratingValue" content="<?php echo $rating; ?>" />
                                            <meta data-i32temprop_mdp="reviewCount" content="<?php echo rtrim(preg_replace("/[^0-9\.]/", "", ($reviews)), '.'); ?>" />
                                                  <meta data-i32temprop_mdp="bestRating" content="5" />
                                            <meta data-i32temprop_mdp="worstRating" content="1" />
                                        </span>
                                        <?php } ?>
                                    </p>
                                </div>
                            </div>
                            <?php endif; ?>
                            <!-- END RATING -->

                            <!-- END CONTROLS -->

                            <div class="form-group orderbtn hidden">
                                <?php if($config->get('control_quantity')== 1) {?>
                                <div class="quantity">
                                   <input data-toggle="tooltip" title="<?php echo $entry_qty; ?>" type="text" name="quantity" value="<?php echo $minimum; ?>" size="2" id="input-quantity" class="form-control" />
                                   <a class="plus" href="javascript:void(0);">+</a>
                                   <a class="minus" href="javascript:void(0);">-</a>
                                </div>
                                <?php } ?>
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_cart; ?></button>
                                <div class="btn-group" style="vertical-align: top;">
                                    <button type="button" data-toggle="tooltip" class="no-btn btn-rad" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add( '<?php echo $product_id; ?>' );"><i class="fa fa-heart"></i></button>
                                    <?php if($config->get('control_compare')== 1) { ?>
                                    <button type="button" data-toggle="tooltip" class="no-btn btn-rad" title="<?php echo $button_compare; ?>" onclick="compare.add( '<?php echo $product_id; ?>' );"><i class="fa fa-exchange"></i></button>
                                    <?php } ?>
                                </div>
                            </div>
                            <!-- // START HACK KERCH -->
                            <div class="yashare-auto-init hidden"
                                 data-yashareL10n="ru"
                                 data-yashareType="small"
                                 data-yashareQuickServices="vkontakte,odnoklassniki,twitter,facebook"
                                 data-yashareTheme="counter">
                                &nbsp;
                            </div>
                            <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
                            <!-- // END HACK KERCH -->
                            <?php if ($minimum > 1) { ?>
                            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                            <?php } ?>
                            <?php if($config->get('control_description')== 2) { ?>
                            <div class="tab-content">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                                    <?php if ($attribute_groups) { ?>
                                    <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                                    <?php } ?>
                                    <?php if($config->get('control_custom_tab_status')== 1) {
                                    $control_title_tab = $config->get('control_title_tab');
                                    $control_content_tab = $config->get('control_content_tab');
                                    if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
                                    <li><a href="#tab-custom" data-toggle="tab"><?php echo $control_title_tab[$lang]; ?></a></li>
                                    <?php } ?>
                                    <?php } ?>
                                    <?php if($config->get('control_custom_tab4_status')== 1) {
                                    $control_title_tab4 = $config->get('control_title_tab4');
                                    $control_content_tab4 = $config->get('control_content_tab4');
                                    if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                                    <li><a href="#tab-custom4" data-toggle="tab"><?php echo $control_title_tab4[$lang]; ?></a></li>
                                    <?php } ?>
                                    <?php } ?>
                                </ul>
                                <div class="tab-pane active" id="tab-description" data-i32temprop_mdp="description"><?php echo $description; ?></div>
                                <?php if ($attribute_groups) { ?>
                                <div class="tab-pane" id="tab-specification">
                                    <table class="table table-bordered">
                                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                                        <thead>
                                            <tr>
                                                <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                            <tr>
                                                <td><?php echo $attribute['name']; ?></td>
                                                <td><?php echo $attribute['text']; ?></td>
                                            </tr>
                                            <?php } ?>
                                        </tbody>
                                        <?php } ?>
                                    </table>
                                </div>
                                <?php } ?>
                                <?php if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
                                <div class="tab-pane" id="tab-custom"><?php echo html_entity_decode($control_content_tab[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
                                <?php } ?>
                                <?php if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                                <div class="tab-pane" id="tab-custom4"><?php echo html_entity_decode($control_content_tab4[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                        <?php if ($review_status && $config->get('control_review')== 2) { ?>
                        <div class="rating hidden">
                            <p>
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($rating < $i) { ?>
                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                <?php } else { ?>
                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                <?php } ?>
                                <?php } ?>
                                <span class="hrefr"><a class="scrollto"><?php echo $reviews; ?></a> / <a class="scrollto"><?php echo $text_write; ?></a></span>
                                <?php if($rating) { ?>
                                <span data-i32temprop_mdp="aggregateRating" data-i32temscope_mdp data-i32temtype_mdp="http://schema.org/AggregateRating">
                                    <meta data-i32temprop_mdp="ratingValue" content="<?php echo $rating; ?>" />
                                    <meta data-i32temprop_mdp="reviewCount" content="<?php echo rtrim(preg_replace("/[^0-9\.]/", "", ($reviews)), '.'); ?>" />
                                          <meta data-i32temprop_mdp="bestRating" content="5" />
                                    <meta data-i32temprop_mdp="worstRating" content="1" />
                                </span>
                                <?php } ?>
                            </p>
                            <hr>
                            <!-- AddThis Button BEGIN -->
                            <!-- AddThis Button END -->
                        </div>
                        <?php } ?>
                    </div>


                    <div class="col-sm-12">
                        <?php if($config->get('control_description')== 3) { ?>
                        <div class="tab-content">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                                <?php if ($attribute_groups) { ?>
                                <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                                <?php } ?>
                                <?php if($config->get('control_custom_tab_status')== 1) {
                                $control_title_tab = $config->get('control_title_tab');
                                $control_content_tab = $config->get('control_content_tab');
                                if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
                                <li><a href="#tab-custom" data-toggle="tab"><?php echo $control_title_tab[$lang]; ?></a></li>
                                <?php } ?>
                                <?php } ?>
                                <?php if($config->get('control_custom_tab4_status')== 1) {
                                $control_title_tab4 = $config->get('control_title_tab4');
                                $control_content_tab4 = $config->get('control_content_tab4');
                                if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                                <li><a href="#tab-custom4" data-toggle="tab"><?php echo $control_title_tab4[$lang]; ?></a></li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                            <div class="tab-pane active" id="tab-description" data-i32temprop_mdp="description"><?php echo $description; ?></div>
                            <?php if ($attribute_groups) { ?>
                            <div class="tab-pane" id="tab-specification">
                                <table class="table table-bordered">
                                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                                    <thead>
                                        <tr>
                                            <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                        <tr>
                                            <td><?php echo $attribute['name']; ?></td>
                                            <td><?php echo $attribute['text']; ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                    <?php } ?>
                                </table>
                            </div>
                            <?php } ?>
                            <?php if(isset($control_title_tab[$lang]) && $control_title_tab[$lang]!= '') { ?>
                            <div class="tab-pane" id="tab-custom"><?php echo html_entity_decode($control_content_tab[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
                            <?php } ?>
                            <?php if(isset($control_title_tab4[$lang]) && $control_title_tab4[$lang]!= '') { ?>
                            <div class="tab-pane" id="tab-custom4"><?php echo html_entity_decode($control_content_tab4[$lang], ENT_QUOTES, 'UTF-8'); ?></div>
                            <?php } ?>
                        </div>
                        <hr>
                        <?php } ?>
                        <?php if (@$downloads) { ?>
                        <li><a href="#tab-documentation" data-toggle="tab"><?php echo $tab_documentation; ?></a></li>
                        <?php } ?>
                        <?php if ($review_status) { ?>
                        <div id="tab-review">
                            <form class="row form-horizontal" id="form-review">
                                <div id="review"></div>
                                <?php
                                //echo '<pre>';
                                //print_r($this->customer->getId());
                                //echo '</pre>';
                                //if(/$this->customer->isLogged()) {

                                //}
                                ?>
                                <div class="col-sm-6">
                                    <?php if ($review_guest) { ?>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                                            <input type="text" name="name" value="" id="input-name" class="form-control" />
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                            <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>

                                            <!--// BOF dymago extension: reviews text limit + counter //-->
                                            <div id="textleft" style="text-align:right;"><?php echo $text_charuse; ?></div>
                                            <!--// EOF dymago extension: reviews text limit + counter //-->

                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <div class="rating pull-left">
                                                <div class="help-block"><?php echo $text_note; ?></div>
                                                <div class="radio-div">
                                                    <div class="st1 icon-star"><i class="iconio fa fa-star"></i></div>
                                                    <input class="radio-star" type="radio" name="rating" value="1" />
                                                    <div class="st2 icon-star"><i class="iconio fa fa-star"></i></div>
                                                    <input class="radio-star" type="radio" name="rating" value="2" />
                                                    <div class="st3 icon-star"><i class="iconio fa fa-star"></i></div>
                                                    <input class="radio-star" type="radio" name="rating" value="3" />
                                                    <div class="st4 icon-star"><i class="iconio fa fa-star"></i></div>
                                                    <input class="radio-star" type="radio" name="rating" value="4" />
                                                    <div class="st5 icon-star"><i class="iconio fa fa-star"></i></div>
                                                    <input class="radio-star" type="radio" name="rating" value="5" />
                                                </div>
                                                <div class="star-div">0/5</div>
                                            </div>
                                            <script>
                                        jQuery( '.radio-star' ).hover( function( ){var stars = jQuery(this).val();
                              jQuery('.star-div').html(''+ stars +'/5');
                              jQuery('.icon-star').html('<i class="iconio sta'+ stars +' fa fa-star"></i>');
                                                },
                                                function( ){var start = jQuery('input:radio[name=rating]:checked').val();if(typeof  start == 'undefined' ){start = 0;}
                              jQuery('.star-div').html(''+ start +'/5');
                              jQuery('.icon-star').html('<i class="iconio sta'+ start +' fa fa-star"></i>');
                                                } );
                                                jQuery( '.radio-star' ).click( function( ){
                                                jQuery( this ).each( function( ){
                                                if( jQuery( this ).attr( "checked" )
                                                        == "checked" ){var s = jQuery(this).val();
                              jQuery('.star-div').stop().html(''+ s +'/5');
                              jQuery('.icon-star').stop().html('<i id="settext-'+ s +'"></i>');
                                                }} );
                                                } );
                                                $( '.scrollto' ).click( function( ) {
                                                $( 'html, body' ).animate( { scrollTop: $( '#review' ).offset( ).top },500 );
                                                } );
                                            </script>
                                            <div class="pull-right">
                                                <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                    <?php echo $captcha; ?>
                                    <?php } else { ?>
                                    <?php
                                    // START HACK KERCH
                                    // echo $text_login;
                                    // END HACK KERCH
                                    ?>
                                    <?php } ?>
                                </div>

                                <?php if($config->get('control_custom_tab2_status')== 1) {
                                $control_title_tab2 = $config->get('control_title_tab2');
                                $control_content_tab2 = $config->get('control_content_tab2');
                                if(isset($control_title_tab2[$lang]) && $control_title_tab2[$lang]!= '') { ?>
                                <div class="col-sm-6">
                                    <div>
                                        <h3><?php echo $control_title_tab2[$lang]; ?></h3>
                                        <p><?php echo html_entity_decode($control_content_tab2[$lang], ENT_QUOTES, 'UTF-8'); ?></p>
                                    </div>
                                </div>
                                <?php } ?> <?php } ?>
                            </form>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <?php if ($products) { ?>
                <h3 class="rel"><?php echo $text_related; ?></h3>
                <div class="row">
                    <div class="owl-hidden">
                        <div class="owl-carousel product_related">
                            <?php $i = 0; ?>
                            <?php foreach ($products as $product) { ?>
                            <div class="option-hover">
                                <?php if($product['special']) { ?>
                                <div class="sale">-<?php echo $product['sale']; ?>%</div>
                                <?php } ?>
                                <div class="product-thumb">
                                    <div class="action">
                                        <div class="wishlist">
                                            <button class="no-btn" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add( '<?php echo $product['product_id']; ?>' );"><i class="fa fa-heart-o"></i></button>
                                        </div>
                                        <?php if($config->get('control_compare')== 1) { ?>
                                        <div class="compare">
                                            <button class="no-btn" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add( '<?php echo $product['product_id']; ?>' );"><i class="fa fa-exchange"></i></button>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php if ($product['thumb_swap']) { ?>
                                    <div class="image hover"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                                    <div class="image "><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb_swap']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                                    <?php } else {?>
                        <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
                        <?php } ?>
                                    <div>
                                        <div class="caption">
                                            <?php if($config->get('control_stock')== 1) { ?><span class="stock" data-toggle="tooltip" title="<?php echo $product['stock']; ?>" style="background-color:<?php echo ($product['quantity'] > 0) ? '#9BD79B' : '#F07899'; ?>;"></span><?php } ?>
                                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                            <?php if ($product['rating']) { ?>
                                            <div class="rating">
                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                <?php if ($product['rating'] < $i) { ?>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <?php } else { ?>
                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                                <?php } ?>
                                                <?php } ?>
                                            </div>
                                            <?php } ?>
                                            <button class="no-btn" type="button" onclick="cart.add( '<?php echo $product['product_id']; ?>','<?php echo $product['minimum']; ?>' );">
                                                <span class="cart-icon"><?php echo $button_cart; ?></span>
                                            </button>
                                            <?php if ($product['price']) { ?>
                                            <p class="price">
                                                <?php if (!$product['special']) { ?>
                                                <?php echo $product['price']; ?>
                                                <?php } else { ?>
                                                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                                <?php } ?>
                                                <?php if ($product['tax']) { ?>
                                                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                                                <?php } ?>
                                            </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($product['options'] as $option) { ?>
                                <?php if ($option['type'] == 'radio') { ?>
                                <?php $control_option_title = $config->get('control_option_title'); if ($option['name'] ==  $control_option_title[$lang] ) { ?>
                                <div class="opsize">
                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                    <div id="input-option">
                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                        <div class="opradio <?php if ($option_value['subtract']) { if ($option_value['quantity'] <= 0) echo 'out';} ?>" data-toggle="tooltip" title="<?php if ($option_value['subtract']) { if ($option_value['quantity'] > 0) echo ''.$text_scl.' '.$option_value['quantity'].' '.$text_pcs; else echo ' '.$text_out_of_stock.' ';} ?>"><?php echo $option_value['name']; ?></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <?php } ?>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <script src="//cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.32/owl.carousel.min.js"></script>
                    <script type="text/javascript" src="/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>
                    <script type="text/javascript">
                                                        $( document ).ready( function( ) {
                                                        $( ".product_related" ).owlCarousel( {
                                                        items : 6,
                                                                //itemsCustom : [[320, 1],[600, 2],[768, 3],[992, 4],[1170, 4]],
                                                                navigation : true,
                                                                navigationText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
                                                                scrollPerPage : true,
                                                                pagination: false
                                                        } );
                                                        } );
                    </script>
                </div>
                <?php } ?>

                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?>
        </div>
    </div>
    <script type="text/javascript" src="/catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <script type="text/javascript">
                $( 'select[name=\'recurring_id\'], input[name="quantity"]' ).change( function( ){
                $.ajax( {
                url: 'index.php?route=product/product/getRecurringDescription',
                        type: 'post',
                        data: $( 'input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']' ),
                        dataType: 'json',
                        beforeSend: function( ) {
                        $( '#recurring-description' ).html( '' );
                        },
                        success: function( json ) {
                        $( '.alert, .text-danger' ).remove( );
                        if( json['success'] ) {
                        $( '#recurring-description' ).html( json['success'] );
                        }
                        }
                } );
                } );
    </script>
    <script type="text/javascript">
                $( '#button-cart' ).on( 'click',function( ) {
                $.ajax( {
                url: 'index.php?route=checkout/cart/add',
                        type: 'post',
                        data: $( '#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea' ),
                        dataType: 'json',
                        beforeSend: function( ) {
                        //$('#button-cart').button('loading');
                        },
                        complete: function( ) {
                        //$('#button-cart').button('reset');
                        },
                        success: function( json ) {
                        $( '.alert, .text-danger' ).remove( );
                        $( '.form-group' ).removeClass( 'has-error' );
                        if( json['error'] ) {
                        if( json['error']['option'] ) {
                        for( i in json['error']['option'] ) {
                        var element = $( '#input-option'
                                + i.replace( '_','-' ) );
                        if( element.parent( ).hasClass( 'input-group' ) ) {
                        element.parent( ).after( '<div class="text-danger"><i class="fa fa-arrow-up"></i>'
                                + json['error']['option'][i]
                                + '</div>' );
                        }
                        else {
                        element.after( '<div class="text-danger"><i class="fa fa-arrow-up"></i>'
                                + json['error']['option'][i]
                                + '</div>' );
                        }
                        }
                        }

                        if( json['error']['recurring'] ) {
                        $( 'select[name=\'recurring_id\']' ).after( '<div class="text-danger"><i class="fa fa-arrow-up"></i>'
                                + json['error']['recurring']
                                + '</div>' );
                        }

                        // Highlight any found errors
                        $( '.text-danger' ).parent( ).addClass( 'has-error' );
                        }

                        if( json['success'] ) {
                        $( ".nav .panel-url" ).click( );
                        var out =
                                json['total'].substr( 0,json['total'].indexOf( ' ' ) );
                        $( '#cart > button' ).html( '<span id="cart-total">'
                                + out
                                + '</span>' );
                        $( '#cart > ul' ).load( 'index.php?route=common/cart/info ul li' );
                        }
                        },
                        error: function( xhr,ajaxOptions,thrownError ) {
                        alert( thrownError
                                + "\r\n"
                                + xhr.statusText
                                + "\r\n"
                                + xhr.responseText );
                        }
                } );
                } );
    </script>
    <script type="text/javascript">
                $( 'button[id^=\'button-upload\']' ).on( 'click',function( ) {
                var node = this;
                $( '#form-upload' ).remove( );
                $( 'body' ).prepend( '<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>' );
                $( '#form-upload input[name=\'file\']' ).trigger( 'click' );
                if( typeof timer
                        != 'undefined' ) {
                clearInterval( timer );
                }

                timer = setInterval( function( ) {
                if( $( '#form-upload input[name=\'file\']' ).val( )
                        != '' ) {
                clearInterval( timer );
                $.ajax( {
                url: 'index.php?route=tool/upload',
                        type: 'post',
                        dataType: 'json',
                        data: new FormData( $( '#form-upload' )[0] ),
                        cache: false,
                        contentType: false,
                        processData: false,
                        beforeSend: function( ) {
                        $( node ).button( 'loading' );
                        },
                        complete: function( ) {
                        $( node ).button( 'reset' );
                        },
                        success: function( json ) {
                        $( '.text-danger' ).remove( );
                        if( json['error'] ) {
                        $( node ).parent( ).find( 'input' ).after( '<div class="text-danger">'
                                + json['error']
                                + '</div>' );
                        }

                        if( json['success'] ) {
                        alert( json['success'] );
                        $( node ).parent( ).find( 'input' ).attr( 'value',json['code'] );
                        }
                        },
                        error: function( xhr,ajaxOptions,thrownError ) {
                        alert( thrownError
                                + "\r\n"
                                + xhr.statusText
                                + "\r\n"
                                + xhr.responseText );
                        }
                } );
                }
                },500 );
                } );
    </script>
    <script type="text/javascript">
                $( '#review' ).delegate( '.pagination a','click',function( e ) {
                e.preventDefault( );
                $( '#review' ).fadeOut( 'slow' );
                $( '#review' ).load( this.href );
                $( '#review' ).fadeIn( 'slow' );
                } );
                $( '#review' ).load( 'index.php?route=product/product/review&product_id=<?php echo $product_id; ?>' );
                $( '#button-review' ).on( 'click',function( ) {
                $.ajax( {
                url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
                        type: 'post',
                        dataType: 'json',
                        data: $( "#form-review" ).serialize( ),
                        beforeSend: function( ) {
                        $( '#button-review' ).button( 'loading' );
                        },
                        complete: function( ) {
                        $( '#button-review' ).button( 'reset' );
                        },
                        success: function( json ) {
                        $( '.alert-success, .alert-danger' ).remove( );
                        if( json['error'] ) {
                        $( '#review' ).after( '<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> '
                                + json['error']
                                + '</div>' );
                        }

                        if( json['success'] ) {
                        $( '#review' ).after( '<div class="alert alert-success"><i class="fa fa-check-circle"></i> '
                                + json['success']
                                + '</div>' );
                        $( 'input[name=\'name\']' ).val( '' );
                        $( 'textarea[name=\'text\']' ).val( '' );
                        $( 'input[name=\'rating\']:checked' ).prop( 'checked',false );
                        }
                        }
                } );
                } );
                $( document ).ready( function( ) {
                $( '.thumbnails' ).magnificPopup( {
                type:'image',
                        gallery: {
                        enabled:true
                        }
                } );
                $( '.date' ).datetimepicker( {
                pickTime: false
                } );
                $( '.datetime' ).datetimepicker( {
                pickDate: true,
                        pickTime: true
                } );
                $( '.time' ).datetimepicker( {
                pickDate: false
                } );
                } );
    </script>

    <?php if($config->get('control_autoprice')== 1) { ?>
    <script type="text/javascript"><!--

                function price_format( n )
                {
                c =
                        < ?php echo ( empty( $currency['decimals'] ) ? "0" : $currency['decimals'] ); ?
                        > ;
                d =
                        '<?php echo $currency['decimal_point']; ?>'; // decimal separator
                t =
                        '<?php echo $currency['thousand_point']; ?>'; // thousands separator
                s_left = '<?php echo $currency['symbol_left']; ?>';
                s_right = '<?php echo $currency['symbol_right']; ?>';
                n = n
                        *
                        < ?php echo $currency['value']; ?
                        > ;
                //sign = (n < 0) ? '-' : '';

                //extracting the absolute value of the integer part of the number and converting to string
                i = parseInt( n = Math.abs( n ).toFixed( c ) )
                        + '';
                j = ( ( j = i.length )
                        > 3 ) ? j
                        % 3 : 0;
                return s_left
                        + ( j ? i.substr( 0,j )
                                + t : '' )
                        + i.substr( j ).replace( /(\d{3})(?=\d)/g,"$1"
                        + t )
                        + ( c ? d
                                + Math.abs( n
                                        - i ).toFixed( c ).slice( 2 ) : '' )
                        + s_right;
                }

                function calculate_tax( price )
                {
                < ?php // Process Tax Rates
                        if( isset( $tax_rates )
                                && $tax ) {
                foreach ( $tax_rates as $tax_rate ) {
                if( $tax_rate['type']
                        == 'F' ) {
                echo 'price += '.$tax_rate['rate'].';';
                } elseif ( $tax_rate['type']
                        == 'P' ) {
                echo 'price += (price * '.$tax_rate['rate'].') / 100.0;';
                }
                }
                }
                ?
                        >
                        return price;
                }

                function process_discounts( price,quantity )
                {
                < ?php
                        foreach ( $dicounts_unf as $discount ) {
                echo 'if ((quantity >= '.$discount['quantity'].') && ('.$discount['price'].' < price)) price = '.$discount['price'].';'."\n";
                }
                ?
                        >
                        return price;
                }


                animate_delay = 20;
                main_price_final =
                        calculate_tax( Number( $( '#formated_price' ).attr( 'price' ) ) );
                main_price_start =
                        calculate_tax( Number( $( '#formated_price' ).attr( 'price' ) ) );
                main_step = 0;
                main_timeout_id = 0;
                function animateMainPrice_callback( ) {
                main_price_start += main_step;
                if( ( main_step
                        > 0 )
                        && ( main_price_start
                                > main_price_final ) ){
                main_price_start = main_price_final;
                }
                else if( ( main_step
                        < 0 )
                        && ( main_price_start
                                < main_price_final ) ) {
                main_price_start = main_price_final;
                }
                else if( main_step
                        == 0 ) {
                main_price_start = main_price_final;
                }

                $( '#formated_price' ).html( price_format( main_price_start ) );
                if( main_price_start
                        != main_price_final ) {
                main_timeout_id =
                        setTimeout( animateMainPrice_callback,animate_delay );
                }
                }

                function animateMainPrice( price ) {
                main_price_start = main_price_final;
                main_price_final = price;
                main_step = ( main_price_final
                        - main_price_start )
                        / 10;
                        clearTimeout( main_timeout_id );
                main_timeout_id =
                        setTimeout( animateMainPrice_callback,animate_delay );
                }


                < ?php if( $special ) { ?
                        >
                        special_price_final =
                        calculate_tax( Number( $( '#formated_special' ).attr( 'price' ) ) );
                special_price_start =
                        calculate_tax( Number( $( '#formated_special' ).attr( 'price' ) ) );
                special_step = 0;
                special_timeout_id = 0;
                function animateSpecialPrice_callback( ) {
                special_price_start += special_step;
                if( ( special_step
                        > 0 )
                        && ( special_price_start
                                > special_price_final ) ){
                special_price_start = special_price_final;
                }
                else if( ( special_step
                        < 0 )
                        && ( special_price_start
                                < special_price_final ) ) {
                special_price_start = special_price_final;
                }
                else if( special_step
                        == 0 ) {
                special_price_start = special_price_final;
                }

                $( '#formated_special' ).html( price_format( special_price_start ) );
                if( special_price_start
                        != special_price_final ) {
                special_timeout_id =
                        setTimeout( animateSpecialPrice_callback,animate_delay );
                }
                }

                function animateSpecialPrice( price ) {
                special_price_start = special_price_final;
                special_price_final = price;
                special_step = ( special_price_final
                        - special_price_start )
                        / 10;
                        clearTimeout( special_timeout_id );
                special_timeout_id =
                        setTimeout( animateSpecialPrice_callback,animate_delay );
                }
                < ?php } ?
                        >
                        function recalculateprice( )
                        {
                        var main_price =
                                Number( $( '#formated_price' ).attr( 'price' ) );
                        var input_quantity =
                                Number( $( 'input[name="quantity"]' ).val( ) );
                        var special =
                                Number( $( '#formated_special' ).attr( 'price' ) );
                        var tax = 0;
                        if( isNaN( input_quantity ) ) input_quantity = 0;
                        // Process Discounts.
                        < ?php if( $special ) { ?
                                >
                                special =
                                process_discounts( special,input_quantity );
                        < ?php }
                        else { ?
                                >
                                main_price =
                                process_discounts( main_price,input_quantity );
                        < ?php } ?
                                >
                                tax = process_discounts( tax,input_quantity );
                        < ?php if( $points ) { ?
                                >
                                var points =
                                Number( $( '#formated_points' ).attr( 'points' ) );
                        $( 'input:checked,option:selected' ).each( function( ) {
                        if( $( this ).attr( 'points' ) ) points +=
                                Number( $( this ).attr( 'points' ) );
                        } );
                        $( '#formated_points' ).html( Number( points ) );
                        < ?php } ?
                                >
                                var option_price = 0;
                        $( 'input:checked,option:selected' ).each( function( ) {
                        if( $( this ).attr( 'price_prefix' )
                                == '=' ) {
                        option_price += Number( $( this ).attr( 'price' ) );
                        main_price = 0;
                        special = 0;
                        }
                        } );
                        $( 'input:checked,option:selected' ).each( function( ) {
                        if( $( this ).attr( 'price_prefix' )
                                == '+' ) {
                        option_price += Number( $( this ).attr( 'price' ) );
                        }
                        if( $( this ).attr( 'price_prefix' )
                                == '-' ) {
                        option_price -= Number( $( this ).attr( 'price' ) );
                        }
                        if( $( this ).attr( 'price_prefix' )
                                == 'u' ) {
                        pcnt = 1.0
                                + ( Number( $( this ).attr( 'price' ) )
                                        / 100.0 );
                                        option_price *= pcnt;
                                main_price *= pcnt;
                                special *= pcnt;
                                }
                                if( $( this ).attr( 'price_prefix' )
                                        == '*' ) {
                                option_price *=
                                        Number( $( this ).attr( 'price' ) );
                                main_price *=
                                        Number( $( this ).attr( 'price' ) );
                                special *= Number( $( this ).attr( 'price' ) );
                                }
                                } );
                        special += option_price;
                        main_price += option_price;
                        < ?php if( $special ) { ?
                                >
                                tax = special;
                        < ?php }
                        else { ?
                                >
                                tax = main_price;
                        < ?php } ?
                                >
                                // Process TAX.
                                main_price = calculate_tax( main_price );
                        special = calculate_tax( special );
                        // Раскомментировать, если нужен вывод цены с умножением на количество
                        main_price *= input_quantity;
                        special *= input_quantity;
                        tax *= input_quantity;
                        < ?php if( $special ) { ?
                                >
                                html = 'Вы экономите: <span class="price">'
                                + price_format( main_price
                                        - special )
                                + '</span>';
                        $( '.price-economy' ).html( html );
                        //$('#formated_special').html( price_format(special) );
                        animateSpecialPrice( special );
                        < ?php } ?
                                >
                                // Display Main Price
                                //$('#formated_price').html( price_format(main_price) );
                                animateMainPrice( main_price );
                        < ?php if( $special ) { ?
                                >
                                //$('#formated_special').html( price_format(special) );
                                animateSpecialPrice( special );
                        < ?php } ?
                                >
                                < ?php if( $tax ) { ?
                                >
                                $( '#formated_tax' ).html( price_format( tax ) );
                        < ?php } ?
                                >
                        }

                        $( document ).ready( function( ) {
                        $( 'input[type="checkbox"]' ).bind( 'change',function( ) { recalculateprice( ); } );
                        $( 'input[type="radio"]' ).bind( 'change',function( ) { recalculateprice( ); } );
                        $( 'select' ).bind( 'change',function( ) { recalculateprice( ); } );
                        $quantity = $( 'input[name="quantity"]' );
                        $quantity.data( 'val',$quantity.val( ) );
                        ( function( ) {
                        if( $quantity.val( )
                                != $quantity.data( 'val' ) ){
                        $quantity.data( 'val',$quantity.val( ) );
                        recalculateprice( );
                        }
                        setTimeout( arguments.callee,250 );
                        } )( );
                        recalculateprice( );
                        } );
                        //--></script>
    <?php } ?>
    <?php if($config->get('control_firstoption')== 1) { ?>
    <script type="text/javascript"><!--
                    $( document ).ready( function( ) {

                        $( '#product' ).each( function( ) {
                        $( this ).find( '.radio input:first, .radioimg input:first' ).trigger( 'click' );
                        } );
                        } );
                        //--></script>
    <?php } ?>

    <script type="text/javascript"><!--
    $( document ).ready( function ( ) {

                        $( '#input-review' ).keypress( function ( event ) {
                        var max = 3000;
                        var len = $( this ).val( ).length;
                        if( event.which
                                < 0x20 ) {
                        // e.which < 0x20, then it's not a printable character
                        // e.which === 0 - Not a character
                        return; // Do nothing
                        }

                        if( len
                                >= max ) {
                        event.preventDefault( );
                        }

                        } );
                        $( '#input-review' ).keyup( function ( event ) {
                        var max = 3000;
                        var len = $( this ).val( ).length;
                        var char = max
                                - len;
                        $( '#textleft' ).text( '<?php echo $text_startchar; ?> '
                                + char
                                + ' <?php echo $text_charleft; ?>' );
                        } );
                        } );
                        //--></script>

    <?php echo $footer; ?>