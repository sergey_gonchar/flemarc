<?php
// Text
$_['text_information']  = 'Information';

				$_['text_faq'] = 'All FAQ`s';
			
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = 'Powered By <a target="_blank" href="http://myopencart.com/">ocStore</a><br /> %s &copy; %s';
			//Cookie
			$_['entry_cookie_link']            = 'https://en.wikipedia.org/wiki/HTTP_cookie';
			 $_['entry_cookie_more']           = 'More info';
             $_['entry_cookie_accept']         = 'Got it!';
			 $_['entry_cookie_msg']            = 'This website uses cookies to ensure you get the best experience on our website - ';