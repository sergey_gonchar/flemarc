<?php

$_['text_affiliate_subject'] = '{$store_name} - Forgotten Password';
$_['text_customer_subject']  = '{$store_name} - Forgotten Password';
$_['text_heading']           = 'Forgotten Password';
$_['text_login']             = 'Log in by visiting:';
$_['text_new_password']      = 'Your new password is:';
// Text
$_['text_subject']  = '%s - New Password';
$_['text_greeting'] = 'A new password was requested from %s.';
$_['text_password'] = 'Your new password is:';
