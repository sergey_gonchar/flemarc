<?php
$_['heading_title'] = 'Your Account Has Been Created!';
$_['heading_unsubscribe']  = 'To unsubscribe from the mailing list';
$_['text_success_unsubscribe']   = 'You have successfully unsubscribed from the newsletter!';
$_['text_error_unsubscribe']     = 'Account are not correct!';
$_['text_message'] = '<p>Congratulations! Your new account has been successfully created!</p><p>A confirmation has been sent to the provided e-mail address. If you have not received it within the hour, please <a href="%s">contact us</a>.</p>';
$_['text_approval'] = '<p>Thank you for registering with %s!</p><p>You will be notified by e-mail once your account has been activated by the store owner.</p><p>If you have ANY questions about the operation of this online shop, please <a href="%s">contact the store owner</a>.</p>';
$_['text_account'] = 'Account';
$_['text_success'] = 'Success';
?>