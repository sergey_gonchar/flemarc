<?php
$_['heading_title'] = 'Учётная запись успешно создана!';
$_['heading_unsubscribe']  = 'Отписаться от рассылки';
$_['text_success_unsubscribe']   = 'Вы успешно отписались от рассылки!';
$_['text_error_unsubscribe']     = 'Учетные данные не верны!';
$_['text_message'] = '<p>Поздравляем! Ваша Учётная запись была успешно создана.</p><p>Письмо с данными о регистрации было отправлено на указанный E-mail. Если Вы не получили письмо, пожалуйста <a href="%s">свяжитесь с нами</a>.</p>';
$_['text_approval'] = '<p>Спасибо за регистрацию %s!</p><p>Вы будете уведомлены по электронной почте, как только Ваш Личный Кабинет будет активирован администрацией магазина.</p><p>Если у Вас есть какие-то вопросы, пожалуйста <a href="%s">свяжитесь с администратором</a>.</p>';
$_['text_account'] = 'Личный Кабинет';
$_['text_success'] = 'Учётная запись успешно создана!';
?>