<?php
require_once('../config.php');

if( $curl = curl_init() ) {
    curl_setopt($curl, CURLOPT_URL, HTTP_SERVER.'index.php?route=xls/xls_pricelist');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, "action=generate");
    $out = curl_exec($curl);
    echo $out;
    curl_close($curl);
}

?>