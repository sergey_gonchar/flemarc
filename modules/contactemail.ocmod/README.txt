Description
===================================================

Simple extension for define email address to send contact form.


Installation
===================================================

Please find the file install.ocmod.xml and upload it in the Extension Installer
of your OpenCart. Then go to Extensions > Modifications and hit the blue "Refresh"
button in the upper right corner under the notification icon.

Edit field in System > Setting > Store > Tab store

Support
===================================================
​Ing. Roberto I Ramírez N
Senior Web Developer
robertoiran@gmail.com
Skype: iBet7o
