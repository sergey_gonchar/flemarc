﻿

Инструкция по установке версия 2.Х:
1. Админ панель - Установка расширений - добавляем архив tags2.ocmod.zip
2. Админ панель - Модификаторы - Обновляем
3. Должен появиться Tags 2.0
4. Пользователи - Группы пользователей нужно для тех пользователей которые будут пользоваться режимом поставить галочки напротив catalog/tags

Инструкция по установке версия 1.5.Х:
1. Обязательное наличие vqmod
2. Скопировать содержимое архива tags1_5_X.zip в корень сайта.
3. Пользователи - Группы пользователей нужно для тех пользователей которые будут пользоваться режимом поставить галочки напротив catalog/tags

Инструкция по работе:
1. Добавление меток в режиме Метки 2.0 (Tags 2.0)
2. На странице меток также есть кнопка настроек. Доступны следующий настройки:
- Описание вверху страницы - выводить ли описание на странице метки верхнее
- Описание внизу страницы - выводить ли описание на странице метки нижнее
- Только 1 страница - выводить описания только на первой странице
- Ajax навигация - постраничная ajax навигация без перезагрузки страницы.
- Показывать в категории - отображение меток на странице категории
- Показывать количество товаров - отображение количества товаров для метки
- Похожие метки - отображение похожих меток.


3. После добавления тегов, в режиме редактирования товара на вкладке Связи появляеться режим Метки 2.0(Tags 2.0). Автозаполнение.
4. Если добавилась метка на товар, то на странице товара должны выводиться метки данного модуля вместо меток по умолчанию.


По любим вопросам пишите на мой Email - ceskf@mail.ru.
