﻿

Installation Instructions:
1. Admin panels - extensions - add archive tags.ocmod.zip
2. Admin panel - Modifiers - Update
3. You should see Tags 2.0
4. Members - Group members need for those users who will benefit from the regime to put a checkmark in the catalog/tags

How to work:
1. Adding tags Tags 2.0
2. On the label also has a button configuration. Following settings are available:
- Description top of the page - whether to display the description of the mark on the page top
- Description of the bottom of the page - whether to display the description of the mark on the page bottom
- Only one page - only displays the first page
- Ajax Navigation - ajax page-navigation without reloading the page.

3. After you have added the tag mode editing goods on the Communication tab shows mode Tags 2.0. AutoComplete.
4. If you add tags to the items, the page should be displayed зкщвгсе mark this module instead of the default labels.

In any questions write to my Email - ceskf@mail.ru.

Метки это еще один способ группировки товаров в opencart. Модуль расширяет семантическое ядро и увеличивает количевство посадочных страниц.
Будет полезен для продвижения сайта.
В Opencart есть стандартный механизм меток, но как продвигать эти страницы меток, ведь им нельзя выставить мета теги, урл и т.д.

Данный модуль дает возможность создавать метки как полноценную страницу со свои урлом, а также:
- Описания (вверху и внизу) настраиваемые
- мета теги(title, description,keywords)
- Тег н1, можно изменять прямо на странице тегов, это не будет стандартный тег который равняеться названию метки

Данный модуль полностью заменяет стандартный модуль меток.

Разрабатывался данный модуль по причине того, что когда я сделал на один магазин клиенту такой функционал, клиент захотил еще на 7 магазинов аналогичный модуль.


Также работает с SeoUrl и SeoPro.
Не заменяет никаких файлов.

Возможности:
1. Создание меток
2. Назначение меток для товаров
3. Страница метки со всеми мета-тегами
4. AJAX - постраничная навигация.
5. Возможность назначать категорию для метки, которая используеться в url и хлебных крошках
6. Вывод похожих меток на странице метки(выводяться метки которые назначены товарам на текущей странице)
7. Выгрузка страниц меток в sitemap.xml 